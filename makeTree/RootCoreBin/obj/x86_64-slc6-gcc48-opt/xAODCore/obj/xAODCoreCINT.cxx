#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODCoredIobjdIxAODCoreCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "xAODCore/tools/ReadStats.h"
#include "xAODCore/tools/PerfStats.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_xAODcLcLBranchStats(void *p = 0);
   static void *newArray_xAODcLcLBranchStats(Long_t size, void *p);
   static void delete_xAODcLcLBranchStats(void *p);
   static void deleteArray_xAODcLcLBranchStats(void *p);
   static void destruct_xAODcLcLBranchStats(void *p);
   static Long64_t merge_xAODcLcLBranchStats(void *obj, TCollection *coll,TFileMergeInfo *info);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BranchStats*)
   {
      ::xAOD::BranchStats *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::BranchStats >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BranchStats", ::xAOD::BranchStats::Class_Version(), "xAODCore/tools/ReadStats.h", 38,
                  typeid(::xAOD::BranchStats), DefineBehavior(ptr, ptr),
                  &::xAOD::BranchStats::Dictionary, isa_proxy, 4,
                  sizeof(::xAOD::BranchStats) );
      instance.SetNew(&new_xAODcLcLBranchStats);
      instance.SetNewArray(&newArray_xAODcLcLBranchStats);
      instance.SetDelete(&delete_xAODcLcLBranchStats);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBranchStats);
      instance.SetDestructor(&destruct_xAODcLcLBranchStats);
      instance.SetMerge(&merge_xAODcLcLBranchStats);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BranchStats*)
   {
      return GenerateInitInstanceLocal((::xAOD::BranchStats*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BranchStats*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_xAODcLcLReadStats(void *p = 0);
   static void *newArray_xAODcLcLReadStats(Long_t size, void *p);
   static void delete_xAODcLcLReadStats(void *p);
   static void deleteArray_xAODcLcLReadStats(void *p);
   static void destruct_xAODcLcLReadStats(void *p);
   static Long64_t merge_xAODcLcLReadStats(void *obj, TCollection *coll,TFileMergeInfo *info);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::ReadStats*)
   {
      ::xAOD::ReadStats *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::ReadStats >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::ReadStats", ::xAOD::ReadStats::Class_Version(), "xAODCore/tools/ReadStats.h", 118,
                  typeid(::xAOD::ReadStats), DefineBehavior(ptr, ptr),
                  &::xAOD::ReadStats::Dictionary, isa_proxy, 4,
                  sizeof(::xAOD::ReadStats) );
      instance.SetNew(&new_xAODcLcLReadStats);
      instance.SetNewArray(&newArray_xAODcLcLReadStats);
      instance.SetDelete(&delete_xAODcLcLReadStats);
      instance.SetDeleteArray(&deleteArray_xAODcLcLReadStats);
      instance.SetDestructor(&destruct_xAODcLcLReadStats);
      instance.SetMerge(&merge_xAODcLcLReadStats);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::ReadStats*)
   {
      return GenerateInitInstanceLocal((::xAOD::ReadStats*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::ReadStats*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEstringcOxAODcLcLBranchStatsgR_Dictionary();
   static void pairlEstringcOxAODcLcLBranchStatsgR_TClassManip(TClass*);
   static void *new_pairlEstringcOxAODcLcLBranchStatsgR(void *p = 0);
   static void *newArray_pairlEstringcOxAODcLcLBranchStatsgR(Long_t size, void *p);
   static void delete_pairlEstringcOxAODcLcLBranchStatsgR(void *p);
   static void deleteArray_pairlEstringcOxAODcLcLBranchStatsgR(void *p);
   static void destruct_pairlEstringcOxAODcLcLBranchStatsgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<string,xAOD::BranchStats>*)
   {
      pair<string,xAOD::BranchStats> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<string,xAOD::BranchStats>));
      static ::ROOT::TGenericClassInfo 
         instance("pair<string,xAOD::BranchStats>", "string", 96,
                  typeid(pair<string,xAOD::BranchStats>), DefineBehavior(ptr, ptr),
                  &pairlEstringcOxAODcLcLBranchStatsgR_Dictionary, isa_proxy, 4,
                  sizeof(pair<string,xAOD::BranchStats>) );
      instance.SetNew(&new_pairlEstringcOxAODcLcLBranchStatsgR);
      instance.SetNewArray(&newArray_pairlEstringcOxAODcLcLBranchStatsgR);
      instance.SetDelete(&delete_pairlEstringcOxAODcLcLBranchStatsgR);
      instance.SetDeleteArray(&deleteArray_pairlEstringcOxAODcLcLBranchStatsgR);
      instance.SetDestructor(&destruct_pairlEstringcOxAODcLcLBranchStatsgR);
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<string,xAOD::BranchStats>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEstringcOxAODcLcLBranchStatsgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<string,xAOD::BranchStats>*)0x0)->GetClass();
      pairlEstringcOxAODcLcLBranchStatsgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEstringcOxAODcLcLBranchStatsgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void delete_xAODcLcLPerfStats(void *p);
   static void deleteArray_xAODcLcLPerfStats(void *p);
   static void destruct_xAODcLcLPerfStats(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::PerfStats*)
   {
      ::xAOD::PerfStats *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::PerfStats >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::PerfStats", ::xAOD::PerfStats::Class_Version(), "xAODCore/tools/PerfStats.h", 25,
                  typeid(::xAOD::PerfStats), DefineBehavior(ptr, ptr),
                  &::xAOD::PerfStats::Dictionary, isa_proxy, 4,
                  sizeof(::xAOD::PerfStats) );
      instance.SetDelete(&delete_xAODcLcLPerfStats);
      instance.SetDeleteArray(&deleteArray_xAODcLcLPerfStats);
      instance.SetDestructor(&destruct_xAODcLcLPerfStats);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::PerfStats*)
   {
      return GenerateInitInstanceLocal((::xAOD::PerfStats*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::PerfStats*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr BranchStats::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *BranchStats::Class_Name()
{
   return "xAOD::BranchStats";
}

//______________________________________________________________________________
const char *BranchStats::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::BranchStats*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int BranchStats::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::BranchStats*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *BranchStats::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::BranchStats*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *BranchStats::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::BranchStats*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr ReadStats::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ReadStats::Class_Name()
{
   return "xAOD::ReadStats";
}

//______________________________________________________________________________
const char *ReadStats::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::ReadStats*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ReadStats::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::ReadStats*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ReadStats::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::ReadStats*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ReadStats::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::ReadStats*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr PerfStats::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *PerfStats::Class_Name()
{
   return "xAOD::PerfStats";
}

//______________________________________________________________________________
const char *PerfStats::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::PerfStats*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int PerfStats::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::PerfStats*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *PerfStats::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::PerfStats*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *PerfStats::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::PerfStats*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
void BranchStats::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::BranchStats.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(xAOD::BranchStats::Class(),this);
   } else {
      R__b.WriteClassBuffer(xAOD::BranchStats::Class(),this);
   }
}

} // namespace xAOD
namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBranchStats(void *p) {
      return  p ? new(p) ::xAOD::BranchStats : new ::xAOD::BranchStats;
   }
   static void *newArray_xAODcLcLBranchStats(Long_t nElements, void *p) {
      return p ? new(p) ::xAOD::BranchStats[nElements] : new ::xAOD::BranchStats[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBranchStats(void *p) {
      delete ((::xAOD::BranchStats*)p);
   }
   static void deleteArray_xAODcLcLBranchStats(void *p) {
      delete [] ((::xAOD::BranchStats*)p);
   }
   static void destruct_xAODcLcLBranchStats(void *p) {
      typedef ::xAOD::BranchStats current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around the merge function.
   static Long64_t  merge_xAODcLcLBranchStats(void *obj,TCollection *coll,TFileMergeInfo *) {
      return ((::xAOD::BranchStats*)obj)->Merge(coll);
   }
} // end of namespace ROOT for class ::xAOD::BranchStats

namespace xAOD {
//______________________________________________________________________________
void ReadStats::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::ReadStats.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(xAOD::ReadStats::Class(),this);
   } else {
      R__b.WriteClassBuffer(xAOD::ReadStats::Class(),this);
   }
}

} // namespace xAOD
namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLReadStats(void *p) {
      return  p ? new(p) ::xAOD::ReadStats : new ::xAOD::ReadStats;
   }
   static void *newArray_xAODcLcLReadStats(Long_t nElements, void *p) {
      return p ? new(p) ::xAOD::ReadStats[nElements] : new ::xAOD::ReadStats[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLReadStats(void *p) {
      delete ((::xAOD::ReadStats*)p);
   }
   static void deleteArray_xAODcLcLReadStats(void *p) {
      delete [] ((::xAOD::ReadStats*)p);
   }
   static void destruct_xAODcLcLReadStats(void *p) {
      typedef ::xAOD::ReadStats current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around the merge function.
   static Long64_t  merge_xAODcLcLReadStats(void *obj,TCollection *coll,TFileMergeInfo *) {
      return ((::xAOD::ReadStats*)obj)->Merge(coll);
   }
} // end of namespace ROOT for class ::xAOD::ReadStats

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEstringcOxAODcLcLBranchStatsgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,xAOD::BranchStats> : new pair<string,xAOD::BranchStats>;
   }
   static void *newArray_pairlEstringcOxAODcLcLBranchStatsgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,xAOD::BranchStats>[nElements] : new pair<string,xAOD::BranchStats>[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEstringcOxAODcLcLBranchStatsgR(void *p) {
      delete ((pair<string,xAOD::BranchStats>*)p);
   }
   static void deleteArray_pairlEstringcOxAODcLcLBranchStatsgR(void *p) {
      delete [] ((pair<string,xAOD::BranchStats>*)p);
   }
   static void destruct_pairlEstringcOxAODcLcLBranchStatsgR(void *p) {
      typedef pair<string,xAOD::BranchStats> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<string,xAOD::BranchStats>

namespace xAOD {
//______________________________________________________________________________
void PerfStats::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::PerfStats.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(xAOD::PerfStats::Class(),this);
   } else {
      R__b.WriteClassBuffer(xAOD::PerfStats::Class(),this);
   }
}

} // namespace xAOD
namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLPerfStats(void *p) {
      delete ((::xAOD::PerfStats*)p);
   }
   static void deleteArray_xAODcLcLPerfStats(void *p) {
      delete [] ((::xAOD::PerfStats*)p);
   }
   static void destruct_xAODcLcLPerfStats(void *p) {
      typedef ::xAOD::PerfStats current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::PerfStats

namespace ROOT {
   static TClass *vectorlExAODcLcLBranchStatsmUgR_Dictionary();
   static void vectorlExAODcLcLBranchStatsmUgR_TClassManip(TClass*);
   static void *new_vectorlExAODcLcLBranchStatsmUgR(void *p = 0);
   static void *newArray_vectorlExAODcLcLBranchStatsmUgR(Long_t size, void *p);
   static void delete_vectorlExAODcLcLBranchStatsmUgR(void *p);
   static void deleteArray_vectorlExAODcLcLBranchStatsmUgR(void *p);
   static void destruct_vectorlExAODcLcLBranchStatsmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<xAOD::BranchStats*>*)
   {
      vector<xAOD::BranchStats*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<xAOD::BranchStats*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<xAOD::BranchStats*>", -2, "vector", 210,
                  typeid(vector<xAOD::BranchStats*>), DefineBehavior(ptr, ptr),
                  &vectorlExAODcLcLBranchStatsmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<xAOD::BranchStats*>) );
      instance.SetNew(&new_vectorlExAODcLcLBranchStatsmUgR);
      instance.SetNewArray(&newArray_vectorlExAODcLcLBranchStatsmUgR);
      instance.SetDelete(&delete_vectorlExAODcLcLBranchStatsmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlExAODcLcLBranchStatsmUgR);
      instance.SetDestructor(&destruct_vectorlExAODcLcLBranchStatsmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<xAOD::BranchStats*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<xAOD::BranchStats*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlExAODcLcLBranchStatsmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<xAOD::BranchStats*>*)0x0)->GetClass();
      vectorlExAODcLcLBranchStatsmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlExAODcLcLBranchStatsmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlExAODcLcLBranchStatsmUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<xAOD::BranchStats*> : new vector<xAOD::BranchStats*>;
   }
   static void *newArray_vectorlExAODcLcLBranchStatsmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<xAOD::BranchStats*>[nElements] : new vector<xAOD::BranchStats*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlExAODcLcLBranchStatsmUgR(void *p) {
      delete ((vector<xAOD::BranchStats*>*)p);
   }
   static void deleteArray_vectorlExAODcLcLBranchStatsmUgR(void *p) {
      delete [] ((vector<xAOD::BranchStats*>*)p);
   }
   static void destruct_vectorlExAODcLcLBranchStatsmUgR(void *p) {
      typedef vector<xAOD::BranchStats*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<xAOD::BranchStats*>

namespace ROOT {
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary();
   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<int> >*)
   {
      vector<vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<int> >", -2, "vector", 210,
                  typeid(vector<vector<int> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<int> >) );
      instance.SetNew(&new_vectorlEvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<int> >*)0x0)->GetClass();
      vectorlEvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<int> > : new vector<vector<int> >;
   }
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<int> >[nElements] : new vector<vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEintgRsPgR(void *p) {
      delete ((vector<vector<int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p) {
      delete [] ((vector<vector<int> >*)p);
   }
   static void destruct_vectorlEvectorlEintgRsPgR(void *p) {
      typedef vector<vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<int> >

namespace ROOT {
   static TClass *vectorlEvectorlEfloatgRsPgR_Dictionary();
   static void vectorlEvectorlEfloatgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEfloatgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEfloatgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEfloatgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEfloatgRsPgR(void *p);
   static void destruct_vectorlEvectorlEfloatgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<float> >*)
   {
      vector<vector<float> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<float> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<float> >", -2, "vector", 210,
                  typeid(vector<vector<float> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEfloatgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<float> >) );
      instance.SetNew(&new_vectorlEvectorlEfloatgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEfloatgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEfloatgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEfloatgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEfloatgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<float> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<float> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEfloatgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<float> >*)0x0)->GetClass();
      vectorlEvectorlEfloatgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEfloatgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEfloatgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<float> > : new vector<vector<float> >;
   }
   static void *newArray_vectorlEvectorlEfloatgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<float> >[nElements] : new vector<vector<float> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEfloatgRsPgR(void *p) {
      delete ((vector<vector<float> >*)p);
   }
   static void deleteArray_vectorlEvectorlEfloatgRsPgR(void *p) {
      delete [] ((vector<vector<float> >*)p);
   }
   static void destruct_vectorlEvectorlEfloatgRsPgR(void *p) {
      typedef vector<vector<float> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<float> >

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 210,
                  typeid(vector<int>), DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 210,
                  typeid(vector<float>), DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *maplEstringcOxAODcLcLBranchStatsgR_Dictionary();
   static void maplEstringcOxAODcLcLBranchStatsgR_TClassManip(TClass*);
   static void *new_maplEstringcOxAODcLcLBranchStatsgR(void *p = 0);
   static void *newArray_maplEstringcOxAODcLcLBranchStatsgR(Long_t size, void *p);
   static void delete_maplEstringcOxAODcLcLBranchStatsgR(void *p);
   static void deleteArray_maplEstringcOxAODcLcLBranchStatsgR(void *p);
   static void destruct_maplEstringcOxAODcLcLBranchStatsgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,xAOD::BranchStats>*)
   {
      map<string,xAOD::BranchStats> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,xAOD::BranchStats>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,xAOD::BranchStats>", -2, "map", 96,
                  typeid(map<string,xAOD::BranchStats>), DefineBehavior(ptr, ptr),
                  &maplEstringcOxAODcLcLBranchStatsgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,xAOD::BranchStats>) );
      instance.SetNew(&new_maplEstringcOxAODcLcLBranchStatsgR);
      instance.SetNewArray(&newArray_maplEstringcOxAODcLcLBranchStatsgR);
      instance.SetDelete(&delete_maplEstringcOxAODcLcLBranchStatsgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOxAODcLcLBranchStatsgR);
      instance.SetDestructor(&destruct_maplEstringcOxAODcLcLBranchStatsgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,xAOD::BranchStats> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<string,xAOD::BranchStats>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOxAODcLcLBranchStatsgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,xAOD::BranchStats>*)0x0)->GetClass();
      maplEstringcOxAODcLcLBranchStatsgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOxAODcLcLBranchStatsgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOxAODcLcLBranchStatsgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,xAOD::BranchStats> : new map<string,xAOD::BranchStats>;
   }
   static void *newArray_maplEstringcOxAODcLcLBranchStatsgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,xAOD::BranchStats>[nElements] : new map<string,xAOD::BranchStats>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOxAODcLcLBranchStatsgR(void *p) {
      delete ((map<string,xAOD::BranchStats>*)p);
   }
   static void deleteArray_maplEstringcOxAODcLcLBranchStatsgR(void *p) {
      delete [] ((map<string,xAOD::BranchStats>*)p);
   }
   static void destruct_maplEstringcOxAODcLcLBranchStatsgR(void *p) {
      typedef map<string,xAOD::BranchStats> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,xAOD::BranchStats>

namespace ROOT {
   static TClass *maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR_Dictionary();
   static void maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<xAOD::BranchStats*> >*)
   {
      map<string,vector<xAOD::BranchStats*> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<xAOD::BranchStats*> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<xAOD::BranchStats*> >", -2, "map", 96,
                  typeid(map<string,vector<xAOD::BranchStats*> >), DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<xAOD::BranchStats*> >) );
      instance.SetNew(&new_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<xAOD::BranchStats*> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<string,vector<xAOD::BranchStats*> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<xAOD::BranchStats*> >*)0x0)->GetClass();
      maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<xAOD::BranchStats*> > : new map<string,vector<xAOD::BranchStats*> >;
   }
   static void *newArray_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<xAOD::BranchStats*> >[nElements] : new map<string,vector<xAOD::BranchStats*> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(void *p) {
      delete ((map<string,vector<xAOD::BranchStats*> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(void *p) {
      delete [] ((map<string,vector<xAOD::BranchStats*> >*)p);
   }
   static void destruct_maplEstringcOvectorlExAODcLcLBranchStatsmUgRsPgR(void *p) {
      typedef map<string,vector<xAOD::BranchStats*> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<xAOD::BranchStats*> >

namespace {
  void TriggerDictionaryInitialization_xAODCoreCINT_Impl() {
    static const char* headers[] = {
"xAODCore/tools/ReadStats.h",
"xAODCore/tools/PerfStats.h",
0
    };
    static const char* includePaths[] = {
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCore/Root",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCore",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCore/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCore/Root/LinkDef.h")))  BranchStats;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCore/Root/LinkDef.h")))  ReadStats;}
namespace std{template <class _CharT> struct __attribute__((annotate("$clingAutoload$string")))  char_traits;
}
namespace std{template <typename > class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCore/Root/LinkDef.h")))  PerfStats;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODCore"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "xAODCore/tools/ReadStats.h"
#include "xAODCore/tools/PerfStats.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"map<string,xAOD::BranchStats>", payloadCode, "@",
"pair<string,xAOD::BranchStats>", payloadCode, "@",
"vector<vector<float> >", payloadCode, "@",
"vector<vector<int> >", payloadCode, "@",
"xAOD::BranchStats", payloadCode, "@",
"xAOD::PerfStats", payloadCode, "@",
"xAOD::ReadStats", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODCoreCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODCoreCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODCoreCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODCoreCINT() {
  TriggerDictionaryInitialization_xAODCoreCINT_Impl();
}

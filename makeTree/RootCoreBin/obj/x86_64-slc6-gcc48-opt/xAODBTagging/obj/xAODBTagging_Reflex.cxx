// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODBTaggingdIobjdIxAODBTagging_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODBTagging/xAODBTagging/xAODBTaggingDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLBTagging_v1_Dictionary();
   static void xAODcLcLBTagging_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBTagging_v1(void *p = 0);
   static void *newArray_xAODcLcLBTagging_v1(Long_t size, void *p);
   static void delete_xAODcLcLBTagging_v1(void *p);
   static void deleteArray_xAODcLcLBTagging_v1(void *p);
   static void destruct_xAODcLcLBTagging_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BTagging_v1*)
   {
      ::xAOD::BTagging_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BTagging_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BTagging_v1", "xAODBTagging/versions/BTagging_v1.h", 32,
                  typeid(::xAOD::BTagging_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBTagging_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BTagging_v1) );
      instance.SetNew(&new_xAODcLcLBTagging_v1);
      instance.SetNewArray(&newArray_xAODcLcLBTagging_v1);
      instance.SetDelete(&delete_xAODcLcLBTagging_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBTagging_v1);
      instance.SetDestructor(&destruct_xAODcLcLBTagging_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BTagging_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BTagging_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BTagging_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBTagging_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BTagging_v1*)0x0)->GetClass();
      xAODcLcLBTagging_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBTagging_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLBTagging_v1gR_Dictionary();
   static void DataVectorlExAODcLcLBTagging_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLBTagging_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLBTagging_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLBTagging_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLBTagging_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLBTagging_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::BTagging_v1>*)
   {
      ::DataVector<xAOD::BTagging_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::BTagging_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::BTagging_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::BTagging_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLBTagging_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::BTagging_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLBTagging_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLBTagging_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLBTagging_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLBTagging_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLBTagging_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::BTagging_v1>","xAOD::BTaggingContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::BTagging_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::BTagging_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::BTagging_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLBTagging_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::BTagging_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLBTagging_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLBTagging_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","5D19F171-E07E-4F65-BC1F-FC4F4099E9F1");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLBTaggingAuxContainer_v1_Dictionary();
   static void xAODcLcLBTaggingAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBTaggingAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLBTaggingAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLBTaggingAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLBTaggingAuxContainer_v1(void *p);
   static void destruct_xAODcLcLBTaggingAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BTaggingAuxContainer_v1*)
   {
      ::xAOD::BTaggingAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BTaggingAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BTaggingAuxContainer_v1", "xAODBTagging/versions/BTaggingAuxContainer_v1.h", 27,
                  typeid(::xAOD::BTaggingAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBTaggingAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BTaggingAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLBTaggingAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLBTaggingAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLBTaggingAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBTaggingAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLBTaggingAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BTaggingAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BTaggingAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BTaggingAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBTaggingAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BTaggingAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLBTaggingAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBTaggingAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","5E1973D2-D860-4BB1-B8EF-C9AD8E6C66A2");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::BTagging_v1> >*)
   {
      ::DataLink<DataVector<xAOD::BTagging_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::BTagging_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::BTagging_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::BTagging_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::BTagging_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::BTagging_v1> >","DataLink<xAOD::BTaggingContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::BTagging_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::BTagging_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::BTagging_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::BTagging_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::BTagging_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::BTagging_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::BTagging_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::BTagging_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::BTagging_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::BTagging_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::BTagging_v1> >","ElementLink<xAOD::BTaggingContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::BTagging_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::BTagging_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::BTagging_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::BTagging_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLBTagVertex_v1_Dictionary();
   static void xAODcLcLBTagVertex_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBTagVertex_v1(void *p = 0);
   static void *newArray_xAODcLcLBTagVertex_v1(Long_t size, void *p);
   static void delete_xAODcLcLBTagVertex_v1(void *p);
   static void deleteArray_xAODcLcLBTagVertex_v1(void *p);
   static void destruct_xAODcLcLBTagVertex_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BTagVertex_v1*)
   {
      ::xAOD::BTagVertex_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BTagVertex_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BTagVertex_v1", "xAODBTagging/versions/BTagVertex_v1.h", 28,
                  typeid(::xAOD::BTagVertex_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBTagVertex_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BTagVertex_v1) );
      instance.SetNew(&new_xAODcLcLBTagVertex_v1);
      instance.SetNewArray(&newArray_xAODcLcLBTagVertex_v1);
      instance.SetDelete(&delete_xAODcLcLBTagVertex_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBTagVertex_v1);
      instance.SetDestructor(&destruct_xAODcLcLBTagVertex_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BTagVertex_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BTagVertex_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BTagVertex_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBTagVertex_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BTagVertex_v1*)0x0)->GetClass();
      xAODcLcLBTagVertex_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBTagVertex_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLBTagVertex_v1gR_Dictionary();
   static void DataVectorlExAODcLcLBTagVertex_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLBTagVertex_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLBTagVertex_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLBTagVertex_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLBTagVertex_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLBTagVertex_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::BTagVertex_v1>*)
   {
      ::DataVector<xAOD::BTagVertex_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::BTagVertex_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::BTagVertex_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::BTagVertex_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLBTagVertex_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::BTagVertex_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLBTagVertex_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLBTagVertex_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLBTagVertex_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLBTagVertex_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLBTagVertex_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::BTagVertex_v1>","xAOD::BTagVertexContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::BTagVertex_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::BTagVertex_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::BTagVertex_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLBTagVertex_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::BTagVertex_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLBTagVertex_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLBTagVertex_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E225A9EC-9782-43F7-B7BF-1B215187C11C");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLBTagVertexAuxContainer_v1_Dictionary();
   static void xAODcLcLBTagVertexAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBTagVertexAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLBTagVertexAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLBTagVertexAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLBTagVertexAuxContainer_v1(void *p);
   static void destruct_xAODcLcLBTagVertexAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BTagVertexAuxContainer_v1*)
   {
      ::xAOD::BTagVertexAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BTagVertexAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BTagVertexAuxContainer_v1", "xAODBTagging/versions/BTagVertexAuxContainer_v1.h", 29,
                  typeid(::xAOD::BTagVertexAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBTagVertexAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BTagVertexAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLBTagVertexAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLBTagVertexAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLBTagVertexAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBTagVertexAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLBTagVertexAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BTagVertexAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BTagVertexAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BTagVertexAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBTagVertexAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BTagVertexAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLBTagVertexAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBTagVertexAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","09CD44BA-0F40-4FDB-BB30-2F4226FF3E18");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::BTagVertex_v1> >*)
   {
      ::DataLink<DataVector<xAOD::BTagVertex_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::BTagVertex_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::BTagVertex_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::BTagVertex_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::BTagVertex_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::BTagVertex_v1> >","DataLink<xAOD::BTagVertexContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::BTagVertex_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::BTagVertex_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::BTagVertex_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::BTagVertex_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::BTagVertex_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::BTagVertex_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::BTagVertex_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::BTagVertex_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::BTagVertex_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::BTagVertex_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::BTagVertex_v1> >","ElementLink<xAOD::BTagVertexContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::BTagVertex_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::BTagVertex_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::BTagVertex_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::BTagVertex_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBTagging_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BTagging_v1 : new ::xAOD::BTagging_v1;
   }
   static void *newArray_xAODcLcLBTagging_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BTagging_v1[nElements] : new ::xAOD::BTagging_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBTagging_v1(void *p) {
      delete ((::xAOD::BTagging_v1*)p);
   }
   static void deleteArray_xAODcLcLBTagging_v1(void *p) {
      delete [] ((::xAOD::BTagging_v1*)p);
   }
   static void destruct_xAODcLcLBTagging_v1(void *p) {
      typedef ::xAOD::BTagging_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BTagging_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLBTagging_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::BTagging_v1> : new ::DataVector<xAOD::BTagging_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLBTagging_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::BTagging_v1>[nElements] : new ::DataVector<xAOD::BTagging_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLBTagging_v1gR(void *p) {
      delete ((::DataVector<xAOD::BTagging_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLBTagging_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::BTagging_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLBTagging_v1gR(void *p) {
      typedef ::DataVector<xAOD::BTagging_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::BTagging_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBTaggingAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BTaggingAuxContainer_v1 : new ::xAOD::BTaggingAuxContainer_v1;
   }
   static void *newArray_xAODcLcLBTaggingAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BTaggingAuxContainer_v1[nElements] : new ::xAOD::BTaggingAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBTaggingAuxContainer_v1(void *p) {
      delete ((::xAOD::BTaggingAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLBTaggingAuxContainer_v1(void *p) {
      delete [] ((::xAOD::BTaggingAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLBTaggingAuxContainer_v1(void *p) {
      typedef ::xAOD::BTaggingAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BTaggingAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::BTagging_v1> > : new ::DataLink<DataVector<xAOD::BTagging_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::BTagging_v1> >[nElements] : new ::DataLink<DataVector<xAOD::BTagging_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::BTagging_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::BTagging_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::BTagging_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::BTagging_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::BTagging_v1> > : new ::ElementLink<DataVector<xAOD::BTagging_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::BTagging_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::BTagging_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::BTagging_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::BTagging_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::BTagging_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::BTagging_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBTagVertex_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BTagVertex_v1 : new ::xAOD::BTagVertex_v1;
   }
   static void *newArray_xAODcLcLBTagVertex_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BTagVertex_v1[nElements] : new ::xAOD::BTagVertex_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBTagVertex_v1(void *p) {
      delete ((::xAOD::BTagVertex_v1*)p);
   }
   static void deleteArray_xAODcLcLBTagVertex_v1(void *p) {
      delete [] ((::xAOD::BTagVertex_v1*)p);
   }
   static void destruct_xAODcLcLBTagVertex_v1(void *p) {
      typedef ::xAOD::BTagVertex_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BTagVertex_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLBTagVertex_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::BTagVertex_v1> : new ::DataVector<xAOD::BTagVertex_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLBTagVertex_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::BTagVertex_v1>[nElements] : new ::DataVector<xAOD::BTagVertex_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLBTagVertex_v1gR(void *p) {
      delete ((::DataVector<xAOD::BTagVertex_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLBTagVertex_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::BTagVertex_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLBTagVertex_v1gR(void *p) {
      typedef ::DataVector<xAOD::BTagVertex_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::BTagVertex_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBTagVertexAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BTagVertexAuxContainer_v1 : new ::xAOD::BTagVertexAuxContainer_v1;
   }
   static void *newArray_xAODcLcLBTagVertexAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BTagVertexAuxContainer_v1[nElements] : new ::xAOD::BTagVertexAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBTagVertexAuxContainer_v1(void *p) {
      delete ((::xAOD::BTagVertexAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLBTagVertexAuxContainer_v1(void *p) {
      delete [] ((::xAOD::BTagVertexAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLBTagVertexAuxContainer_v1(void *p) {
      typedef ::xAOD::BTagVertexAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BTagVertexAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::BTagVertex_v1> > : new ::DataLink<DataVector<xAOD::BTagVertex_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::BTagVertex_v1> >[nElements] : new ::DataLink<DataVector<xAOD::BTagVertex_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::BTagVertex_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::BTagVertex_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::BTagVertex_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::BTagVertex_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::BTagVertex_v1> > : new ::ElementLink<DataVector<xAOD::BTagVertex_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::BTagVertex_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::BTagVertex_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::BTagVertex_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::BTagVertex_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::BTagVertex_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::BTagVertex_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary();
   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<int> >*)
   {
      vector<vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<int> >", -2, "vector", 210,
                  typeid(vector<vector<int> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<int> >) );
      instance.SetNew(&new_vectorlEvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<int> >*)0x0)->GetClass();
      vectorlEvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<int> > : new vector<vector<int> >;
   }
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<int> >[nElements] : new vector<vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEintgRsPgR(void *p) {
      delete ((vector<vector<int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p) {
      delete [] ((vector<vector<int> >*)p);
   }
   static void destruct_vectorlEvectorlEintgRsPgR(void *p) {
      typedef vector<vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<int> >

namespace ROOT {
   static TClass *vectorlEvectorlEboolgRsPgR_Dictionary();
   static void vectorlEvectorlEboolgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEboolgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEboolgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEboolgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEboolgRsPgR(void *p);
   static void destruct_vectorlEvectorlEboolgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<bool> >*)
   {
      vector<vector<bool> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<bool> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<bool> >", -2, "vector", 210,
                  typeid(vector<vector<bool> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEboolgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<bool> >) );
      instance.SetNew(&new_vectorlEvectorlEboolgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEboolgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEboolgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEboolgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEboolgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<bool> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<bool> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEboolgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<bool> >*)0x0)->GetClass();
      vectorlEvectorlEboolgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEboolgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEboolgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<bool> > : new vector<vector<bool> >;
   }
   static void *newArray_vectorlEvectorlEboolgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<bool> >[nElements] : new vector<vector<bool> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEboolgRsPgR(void *p) {
      delete ((vector<vector<bool> >*)p);
   }
   static void deleteArray_vectorlEvectorlEboolgRsPgR(void *p) {
      delete [] ((vector<vector<bool> >*)p);
   }
   static void destruct_vectorlEvectorlEboolgRsPgR(void *p) {
      typedef vector<vector<bool> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<bool> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 210,
                  typeid(vector<int>), DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEboolgR_Dictionary();
   static void vectorlEboolgR_TClassManip(TClass*);
   static void *new_vectorlEboolgR(void *p = 0);
   static void *newArray_vectorlEboolgR(Long_t size, void *p);
   static void delete_vectorlEboolgR(void *p);
   static void deleteArray_vectorlEboolgR(void *p);
   static void destruct_vectorlEboolgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<bool>*)
   {
      vector<bool> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<bool>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<bool>", -2, "vector", 518,
                  typeid(vector<bool>), DefineBehavior(ptr, ptr),
                  &vectorlEboolgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<bool>) );
      instance.SetNew(&new_vectorlEboolgR);
      instance.SetNewArray(&newArray_vectorlEboolgR);
      instance.SetDelete(&delete_vectorlEboolgR);
      instance.SetDeleteArray(&deleteArray_vectorlEboolgR);
      instance.SetDestructor(&destruct_vectorlEboolgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<bool> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<bool>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEboolgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<bool>*)0x0)->GetClass();
      vectorlEboolgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEboolgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEboolgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<bool> : new vector<bool>;
   }
   static void *newArray_vectorlEboolgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<bool>[nElements] : new vector<bool>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEboolgR(void *p) {
      delete ((vector<bool>*)p);
   }
   static void deleteArray_vectorlEboolgR(void *p) {
      delete [] ((vector<bool>*)p);
   }
   static void destruct_vectorlEboolgR(void *p) {
      typedef vector<bool> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<bool>

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::BTagging_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::BTagging_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::BTagging_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::BTagging_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::BTagging_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::BTagging_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::BTagging_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::BTagging_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::BTagging_v1> > > : new vector<ElementLink<DataVector<xAOD::BTagging_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::BTagging_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::BTagging_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::BTagging_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::BTagging_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::BTagging_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::BTagging_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > : new vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::BTagging_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::BTagging_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::BTagging_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::BTagging_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::BTagging_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::BTagging_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::BTagging_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::BTagging_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::BTagging_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::BTagging_v1> > > : new vector<DataLink<DataVector<xAOD::BTagging_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::BTagging_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::BTagging_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::BTagging_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::BTagging_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLBTagging_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::BTagging_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::BTagging_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::BTagVertex_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::BTagVertex_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::BTagVertex_v1> > > : new vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLBTagVertex_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::BTagVertex_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODBTagging_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODBTagging/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODBTagging/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODBTagging/BTaggingContainer.h")))  BTagging_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@5E1973D2-D860-4BB1-B8EF-C9AD8E6C66A2)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODBTagging/BTaggingAuxContainer.h")))  BTaggingAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODBTagging/BTaggingContainer.h")))  BTagVertex_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@09CD44BA-0F40-4FDB-BB30-2F4226FF3E18)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODBTagging/BTagVertexAuxContainer.h")))  BTagVertexAuxContainer_v1;}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODBTagging"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODBTaggingDict.h 631475 2014-11-27 11:53:05Z filthaut $
#ifndef XAODBTAGGING_XAODBTAGGINGDICT_H
#define XAODBTAGGING_XAODBTAGGINGDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODBTagging/BTaggingContainer.h"
#include "xAODBTagging/BTaggingAuxContainer.h"
#include "xAODBTagging/BTagVertexContainer.h" 
#include "xAODBTagging/BTagVertexAuxContainer.h"
// #include "xAODBTagging/versions/BTaggingContainer_v1.h"
// #include "xAODBTagging/versions/BTaggingAuxContainer_v1.h"
// #include "xAODBTagging/versions/BTagVertexContainer_v1.h" 
// #include "xAODBTagging/versions/BTagVertexAuxContainer_v1.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODBTAGGING {
      xAOD::BTaggingContainer_v1 c1;
      DataLink< xAOD::BTaggingContainer_v1 > dl1;
      std::vector< DataLink< xAOD::BTaggingContainer_v1 > > dl2;
      ElementLink< xAOD::BTaggingContainer_v1 > el1;
      std::vector< ElementLink< xAOD::BTaggingContainer_v1 > > el2;
      std::vector< std::vector< ElementLink< xAOD::BTaggingContainer_v1 > > > el3;


      std::vector< std::vector< bool > > atmp;
      std::vector< std::vector< int > > anothertmp;

      
      xAOD::BTagVertexContainer_v1 cx1;
      DataLink< xAOD::BTagVertexContainer_v1 > dlx1;
      std::vector< DataLink< xAOD::BTagVertexContainer_v1 > > dlx2;
      ElementLink< xAOD::BTagVertexContainer_v1 > elx1;
      std::vector< ElementLink< xAOD::BTagVertexContainer_v1 > > elx2;
      std::vector< std::vector< ElementLink< xAOD::BTagVertexContainer_v1 > > > elx3;

     ElementLink< xAOD::TrackParticleContainer > auxlink1;
     std::vector<ElementLink< xAOD::TrackParticleContainer > > auxlink2;
   };
}

#endif // XAODBTAGGING_XAODBTAGGINGDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::BTagVertex_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::BTagging_v1> >", payloadCode, "@",
"DataLink<xAOD::BTagVertexContainer_v1>", payloadCode, "@",
"DataLink<xAOD::BTaggingContainer_v1>", payloadCode, "@",
"DataVector<xAOD::BTagVertex_v1>", payloadCode, "@",
"DataVector<xAOD::BTagging_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::BTagVertex_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::BTagging_v1> >", payloadCode, "@",
"ElementLink<xAOD::BTagVertexContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::BTaggingContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::BTagVertex_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::BTagging_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::BTagVertexContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::BTaggingContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::BTagging_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::BTagVertexContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::BTaggingContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::BTagVertexContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::BTaggingContainer_v1> > >", payloadCode, "@",
"vector<std::vector<bool> >", payloadCode, "@",
"vector<std::vector<int> >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::BTagVertex_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::BTagging_v1> > > >", payloadCode, "@",
"vector<vector<bool> >", payloadCode, "@",
"vector<vector<int> >", payloadCode, "@",
"xAOD::BTagVertexAuxContainer_v1", payloadCode, "@",
"xAOD::BTagVertexContainer_v1", payloadCode, "@",
"xAOD::BTagVertex_v1", payloadCode, "@",
"xAOD::BTaggingAuxContainer_v1", payloadCode, "@",
"xAOD::BTaggingContainer_v1", payloadCode, "@",
"xAOD::BTagging_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODBTagging_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODBTagging_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODBTagging_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODBTagging_Reflex() {
  TriggerDictionaryInitialization_xAODBTagging_Reflex_Impl();
}

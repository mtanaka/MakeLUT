// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODAssociationsdIobjdIxAODAssociations_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODAssociations/xAODAssociations/xAODAssociationsDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTrackParticleClusterAssociation_v1_Dictionary();
   static void xAODcLcLTrackParticleClusterAssociation_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrackParticleClusterAssociation_v1(void *p = 0);
   static void *newArray_xAODcLcLTrackParticleClusterAssociation_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrackParticleClusterAssociation_v1(void *p);
   static void deleteArray_xAODcLcLTrackParticleClusterAssociation_v1(void *p);
   static void destruct_xAODcLcLTrackParticleClusterAssociation_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackParticleClusterAssociation_v1*)
   {
      ::xAOD::TrackParticleClusterAssociation_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackParticleClusterAssociation_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackParticleClusterAssociation_v1", "xAODAssociations/versions/TrackParticleClusterAssociation_v1.h", 21,
                  typeid(::xAOD::TrackParticleClusterAssociation_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackParticleClusterAssociation_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackParticleClusterAssociation_v1) );
      instance.SetNew(&new_xAODcLcLTrackParticleClusterAssociation_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrackParticleClusterAssociation_v1);
      instance.SetDelete(&delete_xAODcLcLTrackParticleClusterAssociation_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackParticleClusterAssociation_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrackParticleClusterAssociation_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackParticleClusterAssociation_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackParticleClusterAssociation_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackParticleClusterAssociation_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackParticleClusterAssociation_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackParticleClusterAssociation_v1*)0x0)->GetClass();
      xAODcLcLTrackParticleClusterAssociation_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackParticleClusterAssociation_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrackParticleClusterAssociation_v1>*)
   {
      ::DataVector<xAOD::TrackParticleClusterAssociation_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrackParticleClusterAssociation_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrackParticleClusterAssociation_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrackParticleClusterAssociation_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrackParticleClusterAssociation_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrackParticleClusterAssociation_v1>","xAOD::TrackParticleClusterAssociationContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrackParticleClusterAssociation_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrackParticleClusterAssociation_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrackParticleClusterAssociation_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrackParticleClusterAssociation_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F2BC2005-C793-49E4-B79F-7848E732B284");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrackParticleClusterAssociationAuxContainer_v1_Dictionary();
   static void xAODcLcLTrackParticleClusterAssociationAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackParticleClusterAssociationAuxContainer_v1*)
   {
      ::xAOD::TrackParticleClusterAssociationAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackParticleClusterAssociationAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackParticleClusterAssociationAuxContainer_v1", "xAODAssociations/versions/TrackParticleClusterAssociationAuxContainer_v1.h", 24,
                  typeid(::xAOD::TrackParticleClusterAssociationAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackParticleClusterAssociationAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackParticleClusterAssociationAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackParticleClusterAssociationAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackParticleClusterAssociationAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackParticleClusterAssociationAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackParticleClusterAssociationAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackParticleClusterAssociationAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrackParticleClusterAssociationAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackParticleClusterAssociationAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","1E0569F0-C3A0-4EB2-9264-85B8F1C10D26");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >","DataLink<xAOD::TrackParticleClusterAssociationContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >","ElementLink<xAOD::TrackParticleClusterAssociationContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackParticleClusterAssociation_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticleClusterAssociation_v1 : new ::xAOD::TrackParticleClusterAssociation_v1;
   }
   static void *newArray_xAODcLcLTrackParticleClusterAssociation_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticleClusterAssociation_v1[nElements] : new ::xAOD::TrackParticleClusterAssociation_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackParticleClusterAssociation_v1(void *p) {
      delete ((::xAOD::TrackParticleClusterAssociation_v1*)p);
   }
   static void deleteArray_xAODcLcLTrackParticleClusterAssociation_v1(void *p) {
      delete [] ((::xAOD::TrackParticleClusterAssociation_v1*)p);
   }
   static void destruct_xAODcLcLTrackParticleClusterAssociation_v1(void *p) {
      typedef ::xAOD::TrackParticleClusterAssociation_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackParticleClusterAssociation_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrackParticleClusterAssociation_v1> : new ::DataVector<xAOD::TrackParticleClusterAssociation_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrackParticleClusterAssociation_v1>[nElements] : new ::DataVector<xAOD::TrackParticleClusterAssociation_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrackParticleClusterAssociation_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrackParticleClusterAssociation_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrackParticleClusterAssociation_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrackParticleClusterAssociation_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrackParticleClusterAssociation_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticleClusterAssociationAuxContainer_v1 : new ::xAOD::TrackParticleClusterAssociationAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticleClusterAssociationAuxContainer_v1[nElements] : new ::xAOD::TrackParticleClusterAssociationAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(void *p) {
      delete ((::xAOD::TrackParticleClusterAssociationAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrackParticleClusterAssociationAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrackParticleClusterAssociationAuxContainer_v1(void *p) {
      typedef ::xAOD::TrackParticleClusterAssociationAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackParticleClusterAssociationAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > : new ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > : new ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > : new vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > : new vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticleClusterAssociation_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODAssociations_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODAssociations/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODAssociations",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODAssociations/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODAssociations/TrackParticleClusterAssociationContainer.h")))  TrackParticleClusterAssociation_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@1E0569F0-C3A0-4EB2-9264-85B8F1C10D26)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODAssociations/TrackParticleClusterAssociationAuxContainer.h")))  TrackParticleClusterAssociationAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODAssociations"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
#ifndef XAODASSOCIATIONS_XAODASSOCIATIONSDICT_H
#define XAODASSOCIATIONS_XAODASSOCIATIONSDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
# ifndef EIGEN_DONT_VECTORIZE
#   define EIGEN_DONT_VECTORIZE
# endif
#endif // __GCCXML__
 
// Local include(s):
#include "xAODAssociations/TrackParticleClusterAssociationContainer.h"
#include "xAODAssociations/TrackParticleClusterAssociationAuxContainer.h"

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODASSOCIATIONS {
      // Classes in this package
     xAOD::TrackParticleClusterAssociation                                                        o1;
     xAOD::TrackParticleClusterAssociationContainer                                               c1;
      // Links for this package
      DataLink< xAOD::TrackParticleClusterAssociationContainer >                                   l1;
      ElementLink< xAOD::TrackParticleClusterAssociationContainer >                                l2;
      std::vector< DataLink< xAOD::TrackParticleClusterAssociationContainer > >                    l3;
      std::vector< ElementLink< xAOD::TrackParticleClusterAssociationContainer > >                 l4;
      std::vector< std::vector< ElementLink< xAOD::TrackParticleClusterAssociationContainer > > >  l5;
      // Instantiations of links used by this package
      ElementLink< xAOD::TrackParticleContainer >                                                  i1;
      ElementLink< xAOD::CaloClusterContainer >                                                    i2;
      std::vector< ElementLink< xAOD::CaloClusterContainer > >                                     i3;
   };
}

#endif // XAODASSOCIATIONS_XAODASSOCIATIONSDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >", payloadCode, "@",
"DataLink<xAOD::TrackParticleClusterAssociationContainer_v1>", payloadCode, "@",
"DataVector<xAOD::TrackParticleClusterAssociation_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> >", payloadCode, "@",
"ElementLink<xAOD::TrackParticleClusterAssociationContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::TrackParticleClusterAssociationContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::TrackParticleClusterAssociationContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrackParticleClusterAssociationContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrackParticleClusterAssociation_v1> > > >", payloadCode, "@",
"xAOD::TrackParticleClusterAssociationAuxContainer_v1", payloadCode, "@",
"xAOD::TrackParticleClusterAssociationContainer_v1", payloadCode, "@",
"xAOD::TrackParticleClusterAssociation_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODAssociations_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODAssociations_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODAssociations_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODAssociations_Reflex() {
  TriggerDictionaryInitialization_xAODAssociations_Reflex_Impl();
}

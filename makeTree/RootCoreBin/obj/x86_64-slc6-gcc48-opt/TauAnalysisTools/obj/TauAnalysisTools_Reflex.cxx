// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdITauAnalysisToolsdIobjdITauAnalysisTools_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TauAnalysisTools/TauAnalysisTools/TauAnalysisToolsDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *TauAnalysisToolscLcLTauEfficiencyCorrectionsTool_Dictionary();
   static void TauAnalysisToolscLcLTauEfficiencyCorrectionsTool_TClassManip(TClass*);
   static void delete_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool(void *p);
   static void deleteArray_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool(void *p);
   static void destruct_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TauAnalysisTools::TauEfficiencyCorrectionsTool*)
   {
      ::TauAnalysisTools::TauEfficiencyCorrectionsTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TauAnalysisTools::TauEfficiencyCorrectionsTool));
      static ::ROOT::TGenericClassInfo 
         instance("TauAnalysisTools::TauEfficiencyCorrectionsTool", "TauAnalysisTools/TauEfficiencyCorrectionsTool.h", 31,
                  typeid(::TauAnalysisTools::TauEfficiencyCorrectionsTool), DefineBehavior(ptr, ptr),
                  &TauAnalysisToolscLcLTauEfficiencyCorrectionsTool_Dictionary, isa_proxy, 0,
                  sizeof(::TauAnalysisTools::TauEfficiencyCorrectionsTool) );
      instance.SetDelete(&delete_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool);
      instance.SetDeleteArray(&deleteArray_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool);
      instance.SetDestructor(&destruct_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TauAnalysisTools::TauEfficiencyCorrectionsTool*)
   {
      return GenerateInitInstanceLocal((::TauAnalysisTools::TauEfficiencyCorrectionsTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TauAnalysisTools::TauEfficiencyCorrectionsTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TauAnalysisToolscLcLTauEfficiencyCorrectionsTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TauAnalysisTools::TauEfficiencyCorrectionsTool*)0x0)->GetClass();
      TauAnalysisToolscLcLTauEfficiencyCorrectionsTool_TClassManip(theClass);
   return theClass;
   }

   static void TauAnalysisToolscLcLTauEfficiencyCorrectionsTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TauAnalysisToolscLcLTauSelectionTool_Dictionary();
   static void TauAnalysisToolscLcLTauSelectionTool_TClassManip(TClass*);
   static void delete_TauAnalysisToolscLcLTauSelectionTool(void *p);
   static void deleteArray_TauAnalysisToolscLcLTauSelectionTool(void *p);
   static void destruct_TauAnalysisToolscLcLTauSelectionTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TauAnalysisTools::TauSelectionTool*)
   {
      ::TauAnalysisTools::TauSelectionTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TauAnalysisTools::TauSelectionTool));
      static ::ROOT::TGenericClassInfo 
         instance("TauAnalysisTools::TauSelectionTool", "TauAnalysisTools/TauSelectionTool.h", 69,
                  typeid(::TauAnalysisTools::TauSelectionTool), DefineBehavior(ptr, ptr),
                  &TauAnalysisToolscLcLTauSelectionTool_Dictionary, isa_proxy, 0,
                  sizeof(::TauAnalysisTools::TauSelectionTool) );
      instance.SetDelete(&delete_TauAnalysisToolscLcLTauSelectionTool);
      instance.SetDeleteArray(&deleteArray_TauAnalysisToolscLcLTauSelectionTool);
      instance.SetDestructor(&destruct_TauAnalysisToolscLcLTauSelectionTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TauAnalysisTools::TauSelectionTool*)
   {
      return GenerateInitInstanceLocal((::TauAnalysisTools::TauSelectionTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TauAnalysisTools::TauSelectionTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TauAnalysisToolscLcLTauSelectionTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TauAnalysisTools::TauSelectionTool*)0x0)->GetClass();
      TauAnalysisToolscLcLTauSelectionTool_TClassManip(theClass);
   return theClass;
   }

   static void TauAnalysisToolscLcLTauSelectionTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TauAnalysisToolscLcLTauSmearingTool_Dictionary();
   static void TauAnalysisToolscLcLTauSmearingTool_TClassManip(TClass*);
   static void delete_TauAnalysisToolscLcLTauSmearingTool(void *p);
   static void deleteArray_TauAnalysisToolscLcLTauSmearingTool(void *p);
   static void destruct_TauAnalysisToolscLcLTauSmearingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TauAnalysisTools::TauSmearingTool*)
   {
      ::TauAnalysisTools::TauSmearingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TauAnalysisTools::TauSmearingTool));
      static ::ROOT::TGenericClassInfo 
         instance("TauAnalysisTools::TauSmearingTool", "TauAnalysisTools/TauSmearingTool.h", 33,
                  typeid(::TauAnalysisTools::TauSmearingTool), DefineBehavior(ptr, ptr),
                  &TauAnalysisToolscLcLTauSmearingTool_Dictionary, isa_proxy, 0,
                  sizeof(::TauAnalysisTools::TauSmearingTool) );
      instance.SetDelete(&delete_TauAnalysisToolscLcLTauSmearingTool);
      instance.SetDeleteArray(&deleteArray_TauAnalysisToolscLcLTauSmearingTool);
      instance.SetDestructor(&destruct_TauAnalysisToolscLcLTauSmearingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TauAnalysisTools::TauSmearingTool*)
   {
      return GenerateInitInstanceLocal((::TauAnalysisTools::TauSmearingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TauAnalysisTools::TauSmearingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TauAnalysisToolscLcLTauSmearingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TauAnalysisTools::TauSmearingTool*)0x0)->GetClass();
      TauAnalysisToolscLcLTauSmearingTool_TClassManip(theClass);
   return theClass;
   }

   static void TauAnalysisToolscLcLTauSmearingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TauAnalysisToolscLcLTauTruthMatchingTool_Dictionary();
   static void TauAnalysisToolscLcLTauTruthMatchingTool_TClassManip(TClass*);
   static void delete_TauAnalysisToolscLcLTauTruthMatchingTool(void *p);
   static void deleteArray_TauAnalysisToolscLcLTauTruthMatchingTool(void *p);
   static void destruct_TauAnalysisToolscLcLTauTruthMatchingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TauAnalysisTools::TauTruthMatchingTool*)
   {
      ::TauAnalysisTools::TauTruthMatchingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TauAnalysisTools::TauTruthMatchingTool));
      static ::ROOT::TGenericClassInfo 
         instance("TauAnalysisTools::TauTruthMatchingTool", "TauAnalysisTools/TauTruthMatchingTool.h", 46,
                  typeid(::TauAnalysisTools::TauTruthMatchingTool), DefineBehavior(ptr, ptr),
                  &TauAnalysisToolscLcLTauTruthMatchingTool_Dictionary, isa_proxy, 0,
                  sizeof(::TauAnalysisTools::TauTruthMatchingTool) );
      instance.SetDelete(&delete_TauAnalysisToolscLcLTauTruthMatchingTool);
      instance.SetDeleteArray(&deleteArray_TauAnalysisToolscLcLTauTruthMatchingTool);
      instance.SetDestructor(&destruct_TauAnalysisToolscLcLTauTruthMatchingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TauAnalysisTools::TauTruthMatchingTool*)
   {
      return GenerateInitInstanceLocal((::TauAnalysisTools::TauTruthMatchingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TauAnalysisTools::TauTruthMatchingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TauAnalysisToolscLcLTauTruthMatchingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TauAnalysisTools::TauTruthMatchingTool*)0x0)->GetClass();
      TauAnalysisToolscLcLTauTruthMatchingTool_TClassManip(theClass);
   return theClass;
   }

   static void TauAnalysisToolscLcLTauTruthMatchingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TauAnalysisToolscLcLTauTruthTrackMatchingTool_Dictionary();
   static void TauAnalysisToolscLcLTauTruthTrackMatchingTool_TClassManip(TClass*);
   static void delete_TauAnalysisToolscLcLTauTruthTrackMatchingTool(void *p);
   static void deleteArray_TauAnalysisToolscLcLTauTruthTrackMatchingTool(void *p);
   static void destruct_TauAnalysisToolscLcLTauTruthTrackMatchingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TauAnalysisTools::TauTruthTrackMatchingTool*)
   {
      ::TauAnalysisTools::TauTruthTrackMatchingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TauAnalysisTools::TauTruthTrackMatchingTool));
      static ::ROOT::TGenericClassInfo 
         instance("TauAnalysisTools::TauTruthTrackMatchingTool", "TauAnalysisTools/TauTruthTrackMatchingTool.h", 35,
                  typeid(::TauAnalysisTools::TauTruthTrackMatchingTool), DefineBehavior(ptr, ptr),
                  &TauAnalysisToolscLcLTauTruthTrackMatchingTool_Dictionary, isa_proxy, 0,
                  sizeof(::TauAnalysisTools::TauTruthTrackMatchingTool) );
      instance.SetDelete(&delete_TauAnalysisToolscLcLTauTruthTrackMatchingTool);
      instance.SetDeleteArray(&deleteArray_TauAnalysisToolscLcLTauTruthTrackMatchingTool);
      instance.SetDestructor(&destruct_TauAnalysisToolscLcLTauTruthTrackMatchingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TauAnalysisTools::TauTruthTrackMatchingTool*)
   {
      return GenerateInitInstanceLocal((::TauAnalysisTools::TauTruthTrackMatchingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TauAnalysisTools::TauTruthTrackMatchingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TauAnalysisToolscLcLTauTruthTrackMatchingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TauAnalysisTools::TauTruthTrackMatchingTool*)0x0)->GetClass();
      TauAnalysisToolscLcLTauTruthTrackMatchingTool_TClassManip(theClass);
   return theClass;
   }

   static void TauAnalysisToolscLcLTauTruthTrackMatchingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator_Dictionary();
   static void TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator_TClassManip(TClass*);
   static void delete_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator(void *p);
   static void deleteArray_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator(void *p);
   static void destruct_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TauAnalysisTools::TauOverlappingElectronLLHDecorator*)
   {
      ::TauAnalysisTools::TauOverlappingElectronLLHDecorator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TauAnalysisTools::TauOverlappingElectronLLHDecorator));
      static ::ROOT::TGenericClassInfo 
         instance("TauAnalysisTools::TauOverlappingElectronLLHDecorator", "TauAnalysisTools/TauOverlappingElectronLLHDecorator.h", 27,
                  typeid(::TauAnalysisTools::TauOverlappingElectronLLHDecorator), DefineBehavior(ptr, ptr),
                  &TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator_Dictionary, isa_proxy, 0,
                  sizeof(::TauAnalysisTools::TauOverlappingElectronLLHDecorator) );
      instance.SetDelete(&delete_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator);
      instance.SetDeleteArray(&deleteArray_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator);
      instance.SetDestructor(&destruct_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TauAnalysisTools::TauOverlappingElectronLLHDecorator*)
   {
      return GenerateInitInstanceLocal((::TauAnalysisTools::TauOverlappingElectronLLHDecorator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TauAnalysisTools::TauOverlappingElectronLLHDecorator*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TauAnalysisTools::TauOverlappingElectronLLHDecorator*)0x0)->GetClass();
      TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator_TClassManip(theClass);
   return theClass;
   }

   static void TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool(void *p) {
      delete ((::TauAnalysisTools::TauEfficiencyCorrectionsTool*)p);
   }
   static void deleteArray_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool(void *p) {
      delete [] ((::TauAnalysisTools::TauEfficiencyCorrectionsTool*)p);
   }
   static void destruct_TauAnalysisToolscLcLTauEfficiencyCorrectionsTool(void *p) {
      typedef ::TauAnalysisTools::TauEfficiencyCorrectionsTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TauAnalysisTools::TauEfficiencyCorrectionsTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TauAnalysisToolscLcLTauSelectionTool(void *p) {
      delete ((::TauAnalysisTools::TauSelectionTool*)p);
   }
   static void deleteArray_TauAnalysisToolscLcLTauSelectionTool(void *p) {
      delete [] ((::TauAnalysisTools::TauSelectionTool*)p);
   }
   static void destruct_TauAnalysisToolscLcLTauSelectionTool(void *p) {
      typedef ::TauAnalysisTools::TauSelectionTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TauAnalysisTools::TauSelectionTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TauAnalysisToolscLcLTauSmearingTool(void *p) {
      delete ((::TauAnalysisTools::TauSmearingTool*)p);
   }
   static void deleteArray_TauAnalysisToolscLcLTauSmearingTool(void *p) {
      delete [] ((::TauAnalysisTools::TauSmearingTool*)p);
   }
   static void destruct_TauAnalysisToolscLcLTauSmearingTool(void *p) {
      typedef ::TauAnalysisTools::TauSmearingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TauAnalysisTools::TauSmearingTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TauAnalysisToolscLcLTauTruthMatchingTool(void *p) {
      delete ((::TauAnalysisTools::TauTruthMatchingTool*)p);
   }
   static void deleteArray_TauAnalysisToolscLcLTauTruthMatchingTool(void *p) {
      delete [] ((::TauAnalysisTools::TauTruthMatchingTool*)p);
   }
   static void destruct_TauAnalysisToolscLcLTauTruthMatchingTool(void *p) {
      typedef ::TauAnalysisTools::TauTruthMatchingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TauAnalysisTools::TauTruthMatchingTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TauAnalysisToolscLcLTauTruthTrackMatchingTool(void *p) {
      delete ((::TauAnalysisTools::TauTruthTrackMatchingTool*)p);
   }
   static void deleteArray_TauAnalysisToolscLcLTauTruthTrackMatchingTool(void *p) {
      delete [] ((::TauAnalysisTools::TauTruthTrackMatchingTool*)p);
   }
   static void destruct_TauAnalysisToolscLcLTauTruthTrackMatchingTool(void *p) {
      typedef ::TauAnalysisTools::TauTruthTrackMatchingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TauAnalysisTools::TauTruthTrackMatchingTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator(void *p) {
      delete ((::TauAnalysisTools::TauOverlappingElectronLLHDecorator*)p);
   }
   static void deleteArray_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator(void *p) {
      delete [] ((::TauAnalysisTools::TauOverlappingElectronLLHDecorator*)p);
   }
   static void destruct_TauAnalysisToolscLcLTauOverlappingElectronLLHDecorator(void *p) {
      typedef ::TauAnalysisTools::TauOverlappingElectronLLHDecorator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TauAnalysisTools::TauOverlappingElectronLLHDecorator

namespace {
  void TriggerDictionaryInitialization_TauAnalysisTools_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TauAnalysisTools/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TauAnalysisTools",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TauAnalysisTools/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace TauAnalysisTools{class __attribute__((annotate("$clingAutoload$TauAnalysisTools/TauEfficiencyCorrectionsTool.h")))  TauEfficiencyCorrectionsTool;}
namespace TauAnalysisTools{class __attribute__((annotate("$clingAutoload$TauAnalysisTools/TauEfficiencyCorrectionsTool.h")))  TauSelectionTool;}
namespace TauAnalysisTools{class __attribute__((annotate("$clingAutoload$TauAnalysisTools/TauSmearingTool.h")))  TauSmearingTool;}
namespace TauAnalysisTools{class __attribute__((annotate("$clingAutoload$TauAnalysisTools/TauTruthMatchingTool.h")))  TauTruthMatchingTool;}
namespace TauAnalysisTools{class __attribute__((annotate("$clingAutoload$TauAnalysisTools/TauTruthTrackMatchingTool.h")))  TauTruthTrackMatchingTool;}
namespace TauAnalysisTools{class __attribute__((annotate("$clingAutoload$TauAnalysisTools/TauEfficiencyCorrectionsTool.h")))  TauOverlappingElectronLLHDecorator;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "TauAnalysisTools"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef TAUANALYSISTOOLDICT_H
#define TAUANALYSISTOOLDICT_H

/*
  author: Dirk Duschinger
  mail: dirk.duschinger@cern.ch
  documentation in: ../README.rst
                    or
                    https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/TauID/TauAnalysisTools/tags/TauAnalysisTools-<tag>/README.rst
		    or
                    https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/TauID/TauAnalysisTools/trunk/README.rst
  report any issues on JIRA: https://its.cern.ch/jira/browse/TAUAT/?selectedTab=com.atlassian.jira.jira-projects-plugin:issues-panel
*/

#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

#include "TauAnalysisTools/ConfigHelper.h"
#include "TauAnalysisTools/TauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/TauSmearingTool.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "TauAnalysisTools/TauTruthTrackMatchingTool.h"

#endif // not TAUANALYSISTOOLDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"TauAnalysisTools::TauEfficiencyCorrectionsTool", payloadCode, "@",
"TauAnalysisTools::TauOverlappingElectronLLHDecorator", payloadCode, "@",
"TauAnalysisTools::TauSelectionTool", payloadCode, "@",
"TauAnalysisTools::TauSmearingTool", payloadCode, "@",
"TauAnalysisTools::TauTruthMatchingTool", payloadCode, "@",
"TauAnalysisTools::TauTruthTrackMatchingTool", payloadCode, "@",
"TauAnalysisTools::split", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TauAnalysisTools_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TauAnalysisTools_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TauAnalysisTools_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TauAnalysisTools_Reflex() {
  TriggerDictionaryInitialization_TauAnalysisTools_Reflex_Impl();
}

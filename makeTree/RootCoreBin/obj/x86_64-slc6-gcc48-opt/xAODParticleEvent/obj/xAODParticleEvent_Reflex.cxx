// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODParticleEventdIobjdIxAODParticleEvent_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODParticleEvent/xAODParticleEvent/xAODParticleEventDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLParticle_v1_Dictionary();
   static void xAODcLcLParticle_v1_TClassManip(TClass*);
   static void *new_xAODcLcLParticle_v1(void *p = 0);
   static void *newArray_xAODcLcLParticle_v1(Long_t size, void *p);
   static void delete_xAODcLcLParticle_v1(void *p);
   static void deleteArray_xAODcLcLParticle_v1(void *p);
   static void destruct_xAODcLcLParticle_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLParticle_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::Particle_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::Particle_v1* newObj = (xAOD::Particle_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
	      m_p4Cached = false;
	    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::Particle_v1*)
   {
      ::xAOD::Particle_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::Particle_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::Particle_v1", "xAODParticleEvent/versions/Particle_v1.h", 24,
                  typeid(::xAOD::Particle_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLParticle_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::Particle_v1) );
      instance.SetNew(&new_xAODcLcLParticle_v1);
      instance.SetNewArray(&newArray_xAODcLcLParticle_v1);
      instance.SetDelete(&delete_xAODcLcLParticle_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLParticle_v1);
      instance.SetDestructor(&destruct_xAODcLcLParticle_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::Particle_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLParticle_v1_0);
      rule->fCode        = "\n	      m_p4Cached = false;\n	    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::Particle_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::Particle_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::Particle_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLParticle_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::Particle_v1*)0x0)->GetClass();
      xAODcLcLParticle_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLParticle_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLParticle_v1gR_Dictionary();
   static void DataVectorlExAODcLcLParticle_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLParticle_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLParticle_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLParticle_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLParticle_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLParticle_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::Particle_v1>*)
   {
      ::DataVector<xAOD::Particle_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::Particle_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::Particle_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::Particle_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLParticle_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::Particle_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLParticle_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLParticle_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLParticle_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLParticle_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLParticle_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::Particle_v1>","xAOD::ParticleContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::Particle_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::Particle_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::Particle_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLParticle_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::Particle_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLParticle_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLParticle_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","CE9D717A-A6DD-4BCA-A946-63A730E0EA3B");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLParticleAuxContainer_v1_Dictionary();
   static void xAODcLcLParticleAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLParticleAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLParticleAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLParticleAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLParticleAuxContainer_v1(void *p);
   static void destruct_xAODcLcLParticleAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::ParticleAuxContainer_v1*)
   {
      ::xAOD::ParticleAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::ParticleAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::ParticleAuxContainer_v1", "xAODParticleEvent/versions/ParticleAuxContainer_v1.h", 26,
                  typeid(::xAOD::ParticleAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLParticleAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::ParticleAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLParticleAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLParticleAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLParticleAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLParticleAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLParticleAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::ParticleAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::ParticleAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::ParticleAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLParticleAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::ParticleAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLParticleAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLParticleAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B53E64D2-C5EA-4B93-9B3C-F4506C823708");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::Particle_v1> >*)
   {
      ::DataLink<DataVector<xAOD::Particle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::Particle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::Particle_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::Particle_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::Particle_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::Particle_v1> >","DataLink<xAOD::ParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::Particle_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::Particle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Particle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Particle_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::Particle_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::Particle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::Particle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::Particle_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::Particle_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::Particle_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::Particle_v1> >","ElementLink<xAOD::ParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::Particle_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::Particle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Particle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Particle_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary();
   static void ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLinkVector<DataVector<xAOD::Particle_v1> >*)
   {
      ::ElementLinkVector<DataVector<xAOD::Particle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLinkVector<DataVector<xAOD::Particle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLinkVector<DataVector<xAOD::Particle_v1> >", "AthLinks/ElementLinkVector.h", 34,
                  typeid(::ElementLinkVector<DataVector<xAOD::Particle_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLinkVector<DataVector<xAOD::Particle_v1> >) );
      instance.SetNew(&new_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLinkVector<DataVector<xAOD::Particle_v1> >","ElementLinkVector<xAOD::ParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLinkVector<DataVector<xAOD::Particle_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLinkVector<DataVector<xAOD::Particle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Particle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Particle_v1> >*)0x0)->GetClass();
      ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCompositeParticle_v1_Dictionary();
   static void xAODcLcLCompositeParticle_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCompositeParticle_v1(void *p = 0);
   static void *newArray_xAODcLcLCompositeParticle_v1(Long_t size, void *p);
   static void delete_xAODcLcLCompositeParticle_v1(void *p);
   static void deleteArray_xAODcLcLCompositeParticle_v1(void *p);
   static void destruct_xAODcLcLCompositeParticle_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLCompositeParticle_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::CompositeParticle_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::CompositeParticle_v1* newObj = (xAOD::CompositeParticle_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
	      m_p4Cached = false;
	    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CompositeParticle_v1*)
   {
      ::xAOD::CompositeParticle_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CompositeParticle_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CompositeParticle_v1", "xAODParticleEvent/versions/CompositeParticle_v1.h", 39,
                  typeid(::xAOD::CompositeParticle_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCompositeParticle_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CompositeParticle_v1) );
      instance.SetNew(&new_xAODcLcLCompositeParticle_v1);
      instance.SetNewArray(&newArray_xAODcLcLCompositeParticle_v1);
      instance.SetDelete(&delete_xAODcLcLCompositeParticle_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCompositeParticle_v1);
      instance.SetDestructor(&destruct_xAODcLcLCompositeParticle_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::CompositeParticle_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLCompositeParticle_v1_0);
      rule->fCode        = "\n	      m_p4Cached = false;\n	    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CompositeParticle_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CompositeParticle_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CompositeParticle_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCompositeParticle_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CompositeParticle_v1*)0x0)->GetClass();
      xAODcLcLCompositeParticle_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCompositeParticle_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCompositeParticle_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCompositeParticle_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCompositeParticle_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCompositeParticle_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCompositeParticle_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCompositeParticle_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCompositeParticle_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CompositeParticle_v1>*)
   {
      ::DataVector<xAOD::CompositeParticle_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CompositeParticle_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CompositeParticle_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::CompositeParticle_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCompositeParticle_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CompositeParticle_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCompositeParticle_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCompositeParticle_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCompositeParticle_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCompositeParticle_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCompositeParticle_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CompositeParticle_v1>","xAOD::CompositeParticleContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CompositeParticle_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CompositeParticle_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CompositeParticle_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCompositeParticle_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CompositeParticle_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCompositeParticle_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCompositeParticle_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C65FFC20-CC41-4C6D-BEDF-B10E935EBFCC");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCompositeParticleAuxContainer_v1_Dictionary();
   static void xAODcLcLCompositeParticleAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCompositeParticleAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCompositeParticleAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCompositeParticleAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCompositeParticleAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCompositeParticleAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CompositeParticleAuxContainer_v1*)
   {
      ::xAOD::CompositeParticleAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CompositeParticleAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CompositeParticleAuxContainer_v1", "xAODParticleEvent/versions/CompositeParticleAuxContainer_v1.h", 27,
                  typeid(::xAOD::CompositeParticleAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCompositeParticleAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CompositeParticleAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCompositeParticleAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCompositeParticleAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCompositeParticleAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCompositeParticleAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCompositeParticleAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CompositeParticleAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CompositeParticleAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CompositeParticleAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCompositeParticleAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CompositeParticleAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCompositeParticleAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCompositeParticleAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E43BBAC2-214D-4AFC-927D-AA0EE2C9217C");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::CompositeParticle_v1> >*)
   {
      ::DataLink<DataVector<xAOD::CompositeParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::CompositeParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::CompositeParticle_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::CompositeParticle_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::CompositeParticle_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::CompositeParticle_v1> >","DataLink<xAOD::CompositeParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::CompositeParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::CompositeParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::CompositeParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::CompositeParticle_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::CompositeParticle_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::CompositeParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::CompositeParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::CompositeParticle_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::CompositeParticle_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::CompositeParticle_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::CompositeParticle_v1> >","ElementLink<xAOD::CompositeParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::CompositeParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::CompositeParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::CompositeParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::CompositeParticle_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary();
   static void ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >*)
   {
      ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >", "AthLinks/ElementLinkVector.h", 34,
                  typeid(::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >) );
      instance.SetNew(&new_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >","ElementLinkVector<xAOD::CompositeParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >*)0x0)->GetClass();
      ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLParticle_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Particle_v1 : new ::xAOD::Particle_v1;
   }
   static void *newArray_xAODcLcLParticle_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Particle_v1[nElements] : new ::xAOD::Particle_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLParticle_v1(void *p) {
      delete ((::xAOD::Particle_v1*)p);
   }
   static void deleteArray_xAODcLcLParticle_v1(void *p) {
      delete [] ((::xAOD::Particle_v1*)p);
   }
   static void destruct_xAODcLcLParticle_v1(void *p) {
      typedef ::xAOD::Particle_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::Particle_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLParticle_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::Particle_v1> : new ::DataVector<xAOD::Particle_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLParticle_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::Particle_v1>[nElements] : new ::DataVector<xAOD::Particle_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLParticle_v1gR(void *p) {
      delete ((::DataVector<xAOD::Particle_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLParticle_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::Particle_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLParticle_v1gR(void *p) {
      typedef ::DataVector<xAOD::Particle_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::Particle_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLParticleAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ParticleAuxContainer_v1 : new ::xAOD::ParticleAuxContainer_v1;
   }
   static void *newArray_xAODcLcLParticleAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ParticleAuxContainer_v1[nElements] : new ::xAOD::ParticleAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLParticleAuxContainer_v1(void *p) {
      delete ((::xAOD::ParticleAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLParticleAuxContainer_v1(void *p) {
      delete [] ((::xAOD::ParticleAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLParticleAuxContainer_v1(void *p) {
      typedef ::xAOD::ParticleAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::ParticleAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::Particle_v1> > : new ::DataLink<DataVector<xAOD::Particle_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::Particle_v1> >[nElements] : new ::DataLink<DataVector<xAOD::Particle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::Particle_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::Particle_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::Particle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::Particle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::Particle_v1> > : new ::ElementLink<DataVector<xAOD::Particle_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::Particle_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::Particle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::Particle_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::Particle_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::Particle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::Particle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLinkVector<DataVector<xAOD::Particle_v1> > : new ::ElementLinkVector<DataVector<xAOD::Particle_v1> >;
   }
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLinkVector<DataVector<xAOD::Particle_v1> >[nElements] : new ::ElementLinkVector<DataVector<xAOD::Particle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      delete ((::ElementLinkVector<DataVector<xAOD::Particle_v1> >*)p);
   }
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      delete [] ((::ElementLinkVector<DataVector<xAOD::Particle_v1> >*)p);
   }
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgR(void *p) {
      typedef ::ElementLinkVector<DataVector<xAOD::Particle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLinkVector<DataVector<xAOD::Particle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCompositeParticle_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CompositeParticle_v1 : new ::xAOD::CompositeParticle_v1;
   }
   static void *newArray_xAODcLcLCompositeParticle_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CompositeParticle_v1[nElements] : new ::xAOD::CompositeParticle_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCompositeParticle_v1(void *p) {
      delete ((::xAOD::CompositeParticle_v1*)p);
   }
   static void deleteArray_xAODcLcLCompositeParticle_v1(void *p) {
      delete [] ((::xAOD::CompositeParticle_v1*)p);
   }
   static void destruct_xAODcLcLCompositeParticle_v1(void *p) {
      typedef ::xAOD::CompositeParticle_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CompositeParticle_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCompositeParticle_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CompositeParticle_v1> : new ::DataVector<xAOD::CompositeParticle_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCompositeParticle_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CompositeParticle_v1>[nElements] : new ::DataVector<xAOD::CompositeParticle_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCompositeParticle_v1gR(void *p) {
      delete ((::DataVector<xAOD::CompositeParticle_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCompositeParticle_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CompositeParticle_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCompositeParticle_v1gR(void *p) {
      typedef ::DataVector<xAOD::CompositeParticle_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CompositeParticle_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCompositeParticleAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CompositeParticleAuxContainer_v1 : new ::xAOD::CompositeParticleAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCompositeParticleAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CompositeParticleAuxContainer_v1[nElements] : new ::xAOD::CompositeParticleAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCompositeParticleAuxContainer_v1(void *p) {
      delete ((::xAOD::CompositeParticleAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCompositeParticleAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CompositeParticleAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCompositeParticleAuxContainer_v1(void *p) {
      typedef ::xAOD::CompositeParticleAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CompositeParticleAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::CompositeParticle_v1> > : new ::DataLink<DataVector<xAOD::CompositeParticle_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::CompositeParticle_v1> >[nElements] : new ::DataLink<DataVector<xAOD::CompositeParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::CompositeParticle_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::CompositeParticle_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::CompositeParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::CompositeParticle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::CompositeParticle_v1> > : new ::ElementLink<DataVector<xAOD::CompositeParticle_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::CompositeParticle_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::CompositeParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::CompositeParticle_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::CompositeParticle_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::CompositeParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::CompositeParticle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > : new ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >;
   }
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >[nElements] : new ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      delete ((::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >*)p);
   }
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      delete [] ((::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >*)p);
   }
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgR(void *p) {
      typedef ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >

namespace ROOT {
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >*)
   {
      vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >*)0x0)->GetClass();
      vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > > : new vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >;
   }
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >[nElements] : new vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >*)
   {
      vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >*)0x0)->GetClass();
      vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > > : new vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >;
   }
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >[nElements] : new vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::Particle_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::Particle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::Particle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::Particle_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::Particle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::Particle_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::Particle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Particle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Particle_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Particle_v1> > > : new vector<ElementLink<DataVector<xAOD::Particle_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Particle_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::Particle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::Particle_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::Particle_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::Particle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::Particle_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::IParticle> > >*)
   {
      vector<ElementLink<DataVector<xAOD::IParticle> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::IParticle> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::IParticle> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::IParticle> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::IParticle> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::IParticle> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::IParticle> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::IParticle> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E3A75D69-B84E-41DD-AB67-B5FF6ACBC243");
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::IParticle> > > : new vector<ElementLink<DataVector<xAOD::IParticle> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::IParticle> > >[nElements] : new vector<ElementLink<DataVector<xAOD::IParticle> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::IParticle> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::IParticle> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::IParticle> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::IParticle> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > > : new vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::Particle_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::Particle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::Particle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::Particle_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::Particle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::Particle_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::Particle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Particle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Particle_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Particle_v1> > > : new vector<DataLink<DataVector<xAOD::Particle_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Particle_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::Particle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::Particle_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::Particle_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::Particle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::Particle_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > > : new vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLCompositeParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODParticleEvent_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODParticleEvent/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODParticleEvent",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODParticleEvent/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODParticleEvent/IParticleLinkContainer.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODParticleEvent/ParticleContainer.h")))  Particle_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B53E64D2-C5EA-4B93-9B3C-F4506C823708)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODParticleEvent/ParticleAuxContainer.h")))  ParticleAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODParticleEvent/CompositeParticleContainer.h")))  CompositeParticle_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@E43BBAC2-214D-4AFC-927D-AA0EE2C9217C)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODParticleEvent/CompositeParticleAuxContainer.h")))  CompositeParticleAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODParticleEvent"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODParticleEventDict.h 649922 2015-02-26 12:20:50Z kkoeneke $
#ifndef XAODPARTICLEEVENT_XAODPARTICLEEVENTDICT_H
#define XAODPARTICLEEVENT_XAODPARTICLEEVENTDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__


// STL
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
#include "xAODParticleEvent/IParticleLinkContainer.h"
#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"
#include "xAODParticleEvent/CompositeParticleContainer.h"
#include "xAODParticleEvent/CompositeParticleAuxContainer.h"


namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODPARTICLEEVENT {
    xAOD::IParticleLinkContainer_v1                                              ipl_c1;
    DataLink< xAOD::IParticleLinkContainer_v1 >                                  ipl_l1;
    ElementLink< xAOD::IParticleLinkContainer_v1 >                               ipl_l2;
    ElementLinkVector< xAOD::IParticleLinkContainer_v1 >                         ipl_l3;
    std::vector< DataLink< xAOD::IParticleLinkContainer_v1 > >                   ipl_l4;
    std::vector< ElementLink< xAOD::IParticleLinkContainer_v1 > >                ipl_l5;
    std::vector< ElementLinkVector< xAOD::IParticleLinkContainer_v1 > >          ipl_l6;
    std::vector< std::vector< ElementLink< xAOD::IParticleLinkContainer_v1 > > > ipl_l7;

    xAOD::ParticleContainer_v1                                              p_c1;
    DataLink< xAOD::ParticleContainer_v1 >                                  p_l1;
    ElementLink< xAOD::ParticleContainer_v1 >                               p_l2;
    ElementLinkVector< xAOD::ParticleContainer_v1 >                         p_l3;
    std::vector< DataLink< xAOD::ParticleContainer_v1 > >                   p_l4;
    std::vector< ElementLink< xAOD::ParticleContainer_v1 > >                p_l5;
    std::vector< ElementLinkVector< xAOD::ParticleContainer_v1 > >          p_l6;
    std::vector< std::vector< ElementLink< xAOD::ParticleContainer_v1 > > > p_l7;

    xAOD::CompositeParticleContainer_v1                                              cp_c1;
    DataLink< xAOD::CompositeParticleContainer_v1 >                                  cp_l1;
    ElementLink< xAOD::CompositeParticleContainer_v1 >                               cp_l2;
    ElementLinkVector< xAOD::CompositeParticleContainer_v1 >                         cp_l3;
    std::vector< DataLink< xAOD::CompositeParticleContainer_v1 > >                   cp_l4;
    std::vector< ElementLink< xAOD::CompositeParticleContainer_v1 > >                cp_l5;
    std::vector< ElementLinkVector< xAOD::CompositeParticleContainer_v1 > >          cp_l6;
    std::vector< std::vector< ElementLink< xAOD::CompositeParticleContainer_v1 > > > cp_l7;

    // // Smart pointers needed for the correct generation of the auxiliary
    // // class dictionaries:
    // std::vector< xAOD::IParticleLinkContainer > el1;
    // ElementLink< xAOD::IParticleContainer > el7;
    // std::vector< ElementLink< xAOD::IParticleContainer > > el8;
    // std::vector< std::vector< ElementLink< xAOD::IParticleContainer > > > el9;

  };
}

#endif // XAODPARTICLEEVENT_XAODPARTICLEEVENTDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::CompositeParticle_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::Particle_v1> >", payloadCode, "@",
"DataLink<xAOD::CompositeParticleContainer_v1>", payloadCode, "@",
"DataLink<xAOD::ParticleContainer_v1>", payloadCode, "@",
"DataVector<xAOD::CompositeParticle_v1>", payloadCode, "@",
"DataVector<xAOD::Particle_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::CompositeParticle_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::Particle_v1> >", payloadCode, "@",
"ElementLink<xAOD::CompositeParticleContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::ParticleContainer_v1>", payloadCode, "@",
"ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> >", payloadCode, "@",
"ElementLinkVector<DataVector<xAOD::Particle_v1> >", payloadCode, "@",
"ElementLinkVector<xAOD::CompositeParticleContainer_v1>", payloadCode, "@",
"ElementLinkVector<xAOD::ParticleContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::CompositeParticle_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::Particle_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::CompositeParticleContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::ParticleContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::CompositeParticle_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::IParticle> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::Particle_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::CompositeParticleContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::ParticleContainer_v1> >", payloadCode, "@",
"vector<ElementLinkVector<DataVector<xAOD::CompositeParticle_v1> > >", payloadCode, "@",
"vector<ElementLinkVector<DataVector<xAOD::Particle_v1> > >", payloadCode, "@",
"vector<ElementLinkVector<xAOD::CompositeParticleContainer_v1> >", payloadCode, "@",
"vector<ElementLinkVector<xAOD::ParticleContainer_v1> >", payloadCode, "@",
"xAOD::CompositeParticleAuxContainer_v1", payloadCode, "@",
"xAOD::CompositeParticleContainer_v1", payloadCode, "@",
"xAOD::CompositeParticle_v1", payloadCode, "@",
"xAOD::IParticleLinkContainer_v1", payloadCode, "@",
"xAOD::ParticleAuxContainer_v1", payloadCode, "@",
"xAOD::ParticleContainer_v1", payloadCode, "@",
"xAOD::Particle_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODParticleEvent_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODParticleEvent_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODParticleEvent_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODParticleEvent_Reflex() {
  TriggerDictionaryInitialization_xAODParticleEvent_Reflex_Impl();
}

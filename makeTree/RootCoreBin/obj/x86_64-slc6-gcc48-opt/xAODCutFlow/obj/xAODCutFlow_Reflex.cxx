// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODCutFlowdIobjdIxAODCutFlow_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODCutFlow/xAODCutFlow/xAODCutFlowDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLCutBookkeeper_v1_Dictionary();
   static void xAODcLcLCutBookkeeper_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCutBookkeeper_v1(void *p = 0);
   static void *newArray_xAODcLcLCutBookkeeper_v1(Long_t size, void *p);
   static void delete_xAODcLcLCutBookkeeper_v1(void *p);
   static void deleteArray_xAODcLcLCutBookkeeper_v1(void *p);
   static void destruct_xAODcLcLCutBookkeeper_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CutBookkeeper_v1*)
   {
      ::xAOD::CutBookkeeper_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CutBookkeeper_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CutBookkeeper_v1", "xAODCutFlow/versions/CutBookkeeper_v1.h", 25,
                  typeid(::xAOD::CutBookkeeper_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCutBookkeeper_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CutBookkeeper_v1) );
      instance.SetNew(&new_xAODcLcLCutBookkeeper_v1);
      instance.SetNewArray(&newArray_xAODcLcLCutBookkeeper_v1);
      instance.SetDelete(&delete_xAODcLcLCutBookkeeper_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCutBookkeeper_v1);
      instance.SetDestructor(&destruct_xAODcLcLCutBookkeeper_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CutBookkeeper_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CutBookkeeper_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CutBookkeeper_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCutBookkeeper_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CutBookkeeper_v1*)0x0)->GetClass();
      xAODcLcLCutBookkeeper_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCutBookkeeper_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCutBookkeeper_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCutBookkeeper_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCutBookkeeper_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCutBookkeeper_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCutBookkeeper_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCutBookkeeper_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCutBookkeeper_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CutBookkeeper_v1>*)
   {
      ::DataVector<xAOD::CutBookkeeper_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CutBookkeeper_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CutBookkeeper_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CutBookkeeper_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCutBookkeeper_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CutBookkeeper_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCutBookkeeper_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCutBookkeeper_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCutBookkeeper_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCutBookkeeper_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCutBookkeeper_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CutBookkeeper_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CutBookkeeper_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CutBookkeeper_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCutBookkeeper_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CutBookkeeper_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCutBookkeeper_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCutBookkeeper_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCutBookkeeperContainer_v1_Dictionary();
   static void xAODcLcLCutBookkeeperContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCutBookkeeperContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCutBookkeeperContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCutBookkeeperContainer_v1(void *p);
   static void deleteArray_xAODcLcLCutBookkeeperContainer_v1(void *p);
   static void destruct_xAODcLcLCutBookkeeperContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CutBookkeeperContainer_v1*)
   {
      ::xAOD::CutBookkeeperContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CutBookkeeperContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CutBookkeeperContainer_v1", "xAODCutFlow/versions/CutBookkeeperContainer_v1.h", 21,
                  typeid(::xAOD::CutBookkeeperContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCutBookkeeperContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CutBookkeeperContainer_v1) );
      instance.SetNew(&new_xAODcLcLCutBookkeeperContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCutBookkeeperContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCutBookkeeperContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCutBookkeeperContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCutBookkeeperContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CutBookkeeperContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CutBookkeeperContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CutBookkeeperContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCutBookkeeperContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CutBookkeeperContainer_v1*)0x0)->GetClass();
      xAODcLcLCutBookkeeperContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCutBookkeeperContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F2F90B2F-B879-43B8-AF9B-0F843E299A87");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCutBookkeeperAuxContainer_v1_Dictionary();
   static void xAODcLcLCutBookkeeperAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCutBookkeeperAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCutBookkeeperAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCutBookkeeperAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCutBookkeeperAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCutBookkeeperAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CutBookkeeperAuxContainer_v1*)
   {
      ::xAOD::CutBookkeeperAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CutBookkeeperAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CutBookkeeperAuxContainer_v1", "xAODCutFlow/versions/CutBookkeeperAuxContainer_v1.h", 29,
                  typeid(::xAOD::CutBookkeeperAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCutBookkeeperAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CutBookkeeperAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCutBookkeeperAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCutBookkeeperAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCutBookkeeperAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCutBookkeeperAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCutBookkeeperAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CutBookkeeperAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CutBookkeeperAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CutBookkeeperAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCutBookkeeperAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CutBookkeeperAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCutBookkeeperAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCutBookkeeperAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","AF612BAA-20B8-40A3-A418-894A9FB8A61B");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklExAODcLcLCutBookkeeperContainer_v1gR_Dictionary();
   static void DataLinklExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(TClass*);
   static void *new_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p = 0);
   static void *newArray_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(Long_t size, void *p);
   static void delete_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p);
   static void deleteArray_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p);
   static void destruct_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<xAOD::CutBookkeeperContainer_v1>*)
   {
      ::DataLink<xAOD::CutBookkeeperContainer_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<xAOD::CutBookkeeperContainer_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<xAOD::CutBookkeeperContainer_v1>", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<xAOD::CutBookkeeperContainer_v1>), DefineBehavior(ptr, ptr),
                  &DataLinklExAODcLcLCutBookkeeperContainer_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<xAOD::CutBookkeeperContainer_v1>) );
      instance.SetNew(&new_DataLinklExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetNewArray(&newArray_DataLinklExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDelete(&delete_DataLinklExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDeleteArray(&deleteArray_DataLinklExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDestructor(&destruct_DataLinklExAODcLcLCutBookkeeperContainer_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<xAOD::CutBookkeeperContainer_v1>*)
   {
      return GenerateInitInstanceLocal((::DataLink<xAOD::CutBookkeeperContainer_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<xAOD::CutBookkeeperContainer_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklExAODcLcLCutBookkeeperContainer_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<xAOD::CutBookkeeperContainer_v1>*)0x0)->GetClass();
      DataLinklExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklExAODcLcLCutBookkeeperContainer_v1gR_Dictionary();
   static void ElementLinklExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(TClass*);
   static void *new_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p = 0);
   static void *newArray_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(Long_t size, void *p);
   static void delete_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p);
   static void deleteArray_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p);
   static void destruct_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<xAOD::CutBookkeeperContainer_v1>*)
   {
      ::ElementLink<xAOD::CutBookkeeperContainer_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<xAOD::CutBookkeeperContainer_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<xAOD::CutBookkeeperContainer_v1>", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<xAOD::CutBookkeeperContainer_v1>), DefineBehavior(ptr, ptr),
                  &ElementLinklExAODcLcLCutBookkeeperContainer_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<xAOD::CutBookkeeperContainer_v1>) );
      instance.SetNew(&new_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetNewArray(&newArray_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDelete(&delete_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDeleteArray(&deleteArray_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDestructor(&destruct_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<xAOD::CutBookkeeperContainer_v1>*)
   {
      return GenerateInitInstanceLocal((::ElementLink<xAOD::CutBookkeeperContainer_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<xAOD::CutBookkeeperContainer_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklExAODcLcLCutBookkeeperContainer_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<xAOD::CutBookkeeperContainer_v1>*)0x0)->GetClass();
      ElementLinklExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR_Dictionary();
   static void ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(TClass*);
   static void *new_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(void *p = 0);
   static void *newArray_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(Long_t size, void *p);
   static void delete_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(void *p);
   static void deleteArray_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(void *p);
   static void destruct_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>*)
   {
      ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLinkVector<xAOD::CutBookkeeperContainer_v1>", "AthLinks/ElementLinkVector.h", 34,
                  typeid(::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>), DefineBehavior(ptr, ptr),
                  &ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>) );
      instance.SetNew(&new_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetNewArray(&newArray_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDelete(&delete_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDeleteArray(&deleteArray_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR);
      instance.SetDestructor(&destruct_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>*)
   {
      return GenerateInitInstanceLocal((::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>*)0x0)->GetClass();
      ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCutBookkeeper_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CutBookkeeper_v1 : new ::xAOD::CutBookkeeper_v1;
   }
   static void *newArray_xAODcLcLCutBookkeeper_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CutBookkeeper_v1[nElements] : new ::xAOD::CutBookkeeper_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCutBookkeeper_v1(void *p) {
      delete ((::xAOD::CutBookkeeper_v1*)p);
   }
   static void deleteArray_xAODcLcLCutBookkeeper_v1(void *p) {
      delete [] ((::xAOD::CutBookkeeper_v1*)p);
   }
   static void destruct_xAODcLcLCutBookkeeper_v1(void *p) {
      typedef ::xAOD::CutBookkeeper_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CutBookkeeper_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCutBookkeeper_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CutBookkeeper_v1> : new ::DataVector<xAOD::CutBookkeeper_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCutBookkeeper_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CutBookkeeper_v1>[nElements] : new ::DataVector<xAOD::CutBookkeeper_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCutBookkeeper_v1gR(void *p) {
      delete ((::DataVector<xAOD::CutBookkeeper_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCutBookkeeper_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CutBookkeeper_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCutBookkeeper_v1gR(void *p) {
      typedef ::DataVector<xAOD::CutBookkeeper_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CutBookkeeper_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCutBookkeeperContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CutBookkeeperContainer_v1 : new ::xAOD::CutBookkeeperContainer_v1;
   }
   static void *newArray_xAODcLcLCutBookkeeperContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CutBookkeeperContainer_v1[nElements] : new ::xAOD::CutBookkeeperContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCutBookkeeperContainer_v1(void *p) {
      delete ((::xAOD::CutBookkeeperContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCutBookkeeperContainer_v1(void *p) {
      delete [] ((::xAOD::CutBookkeeperContainer_v1*)p);
   }
   static void destruct_xAODcLcLCutBookkeeperContainer_v1(void *p) {
      typedef ::xAOD::CutBookkeeperContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CutBookkeeperContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCutBookkeeperAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CutBookkeeperAuxContainer_v1 : new ::xAOD::CutBookkeeperAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCutBookkeeperAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CutBookkeeperAuxContainer_v1[nElements] : new ::xAOD::CutBookkeeperAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCutBookkeeperAuxContainer_v1(void *p) {
      delete ((::xAOD::CutBookkeeperAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCutBookkeeperAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CutBookkeeperAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCutBookkeeperAuxContainer_v1(void *p) {
      typedef ::xAOD::CutBookkeeperAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CutBookkeeperAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      return  p ? new(p) ::DataLink<xAOD::CutBookkeeperContainer_v1> : new ::DataLink<xAOD::CutBookkeeperContainer_v1>;
   }
   static void *newArray_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<xAOD::CutBookkeeperContainer_v1>[nElements] : new ::DataLink<xAOD::CutBookkeeperContainer_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      delete ((::DataLink<xAOD::CutBookkeeperContainer_v1>*)p);
   }
   static void deleteArray_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      delete [] ((::DataLink<xAOD::CutBookkeeperContainer_v1>*)p);
   }
   static void destruct_DataLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      typedef ::DataLink<xAOD::CutBookkeeperContainer_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<xAOD::CutBookkeeperContainer_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      return  p ? new(p) ::ElementLink<xAOD::CutBookkeeperContainer_v1> : new ::ElementLink<xAOD::CutBookkeeperContainer_v1>;
   }
   static void *newArray_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<xAOD::CutBookkeeperContainer_v1>[nElements] : new ::ElementLink<xAOD::CutBookkeeperContainer_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      delete ((::ElementLink<xAOD::CutBookkeeperContainer_v1>*)p);
   }
   static void deleteArray_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      delete [] ((::ElementLink<xAOD::CutBookkeeperContainer_v1>*)p);
   }
   static void destruct_ElementLinklExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      typedef ::ElementLink<xAOD::CutBookkeeperContainer_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<xAOD::CutBookkeeperContainer_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      return  p ? new(p) ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1> : new ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>;
   }
   static void *newArray_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>[nElements] : new ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      delete ((::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>*)p);
   }
   static void deleteArray_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      delete [] ((::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>*)p);
   }
   static void destruct_ElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gR(void *p) {
      typedef ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLinkVector<xAOD::CutBookkeeperContainer_v1>

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >*)
   {
      vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > > : new vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >[nElements] : new vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary();
   static void vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);
   static void deleteArray_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);
   static void destruct_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >*)
   {
      vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >", -2, "vector", 210,
                  typeid(vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >) );
      instance.SetNew(&new_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >*)0x0)->GetClass();
      vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> > : new vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >;
   }
   static void *newArray_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >[nElements] : new vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      delete ((vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >*)p);
   }
   static void deleteArray_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      delete [] ((vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >*)p);
   }
   static void destruct_vectorlEElementLinkVectorlExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      typedef vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >

namespace ROOT {
   static TClass *vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary();
   static void vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);
   static void destruct_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >*)
   {
      vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >", -2, "vector", 210,
                  typeid(vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >) );
      instance.SetNew(&new_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >*)0x0)->GetClass();
      vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > : new vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >;
   }
   static void *newArray_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >[nElements] : new vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      delete ((vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >*)p);
   }
   static void deleteArray_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      delete [] ((vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >*)p);
   }
   static void destruct_vectorlEElementLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      typedef vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >

namespace ROOT {
   static TClass *vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary();
   static void vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);
   static void destruct_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<xAOD::CutBookkeeperContainer_v1> >*)
   {
      vector<DataLink<xAOD::CutBookkeeperContainer_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<xAOD::CutBookkeeperContainer_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<xAOD::CutBookkeeperContainer_v1> >", -2, "vector", 210,
                  typeid(vector<DataLink<xAOD::CutBookkeeperContainer_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<xAOD::CutBookkeeperContainer_v1> >) );
      instance.SetNew(&new_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<xAOD::CutBookkeeperContainer_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<xAOD::CutBookkeeperContainer_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<xAOD::CutBookkeeperContainer_v1> >*)0x0)->GetClass();
      vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::CutBookkeeperContainer_v1> > : new vector<DataLink<xAOD::CutBookkeeperContainer_v1> >;
   }
   static void *newArray_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::CutBookkeeperContainer_v1> >[nElements] : new vector<DataLink<xAOD::CutBookkeeperContainer_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      delete ((vector<DataLink<xAOD::CutBookkeeperContainer_v1> >*)p);
   }
   static void deleteArray_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      delete [] ((vector<DataLink<xAOD::CutBookkeeperContainer_v1> >*)p);
   }
   static void destruct_vectorlEDataLinklExAODcLcLCutBookkeeperContainer_v1gRsPgR(void *p) {
      typedef vector<DataLink<xAOD::CutBookkeeperContainer_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<xAOD::CutBookkeeperContainer_v1> >

namespace {
  void TriggerDictionaryInitialization_xAODCutFlow_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODCutFlow/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODCutFlow",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODCutFlow/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCutFlow/CutBookkeeperContainer.h")))  CutBookkeeper_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@F2F90B2F-B879-43B8-AF9B-0F843E299A87)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCutFlow/CutBookkeeperContainer.h")))  CutBookkeeperContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@AF612BAA-20B8-40A3-A418-894A9FB8A61B)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCutFlow/CutBookkeeperAuxContainer.h")))  CutBookkeeperAuxContainer_v1;}
template <typename STORABLE> class __attribute__((annotate("$clingAutoload$AthLinks/DataLink.h")))  DataLink;

namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
template <typename STORABLE> class __attribute__((annotate("$clingAutoload$AthLinks/ElementLink.h")))  ElementLink;

template <class CONTAINER> class __attribute__((annotate("$clingAutoload$AthLinks/ElementLinkVector.h")))  ElementLinkVector;

)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODCutFlow"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id$
#ifndef XAODCUTFLOW_XAODCUTFLOWDICT_H
#define XAODCUTFLOW_XAODCUTFLOWDICT_H


// STL
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
// #include "xAODCutFlow/versions/CutBookkeeperContainer_v1.h"
// #include "xAODCutFlow/versions/CutBookkeeperAuxContainer_v1.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeperAuxContainer.h"



namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODCUTFLOW {
    xAOD::CutBookkeeperContainer_v1                                              ebkl_c1;
    DataLink< xAOD::CutBookkeeperContainer_v1 >                                  ebkl_l1;
    ElementLink< xAOD::CutBookkeeperContainer_v1 >                               ebkl_l2;
    ElementLinkVector< xAOD::CutBookkeeperContainer_v1 >                         ebkl_l3;
    std::vector< DataLink< xAOD::CutBookkeeperContainer_v1 > >                   ebkl_l4;
    std::vector< ElementLink< xAOD::CutBookkeeperContainer_v1 > >                ebkl_l5;
    std::vector< ElementLinkVector< xAOD::CutBookkeeperContainer_v1 > >          ebkl_l6;
    std::vector< std::vector< ElementLink< xAOD::CutBookkeeperContainer_v1 > > > ebkl_l7;

  };
}

#endif // XAODCUTFLOW_XAODCUTFLOWDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<xAOD::CutBookkeeperContainer_v1>", payloadCode, "@",
"DataVector<xAOD::CutBookkeeper_v1>", payloadCode, "@",
"ElementLink<xAOD::CutBookkeeperContainer_v1>", payloadCode, "@",
"ElementLinkVector<xAOD::CutBookkeeperContainer_v1>", payloadCode, "@",
"vector<DataLink<xAOD::CutBookkeeperContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::CutBookkeeperContainer_v1> >", payloadCode, "@",
"vector<ElementLinkVector<xAOD::CutBookkeeperContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<xAOD::CutBookkeeperContainer_v1> > >", payloadCode, "@",
"xAOD::CutBookkeeperAuxContainer_v1", payloadCode, "@",
"xAOD::CutBookkeeperContainer_v1", payloadCode, "@",
"xAOD::CutBookkeeper_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODCutFlow_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODCutFlow_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODCutFlow_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODCutFlow_Reflex() {
  TriggerDictionaryInitialization_xAODCutFlow_Reflex_Impl();
}

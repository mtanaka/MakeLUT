// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIPileupReweightingdIobjdIPileupReweighting_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PileupReweighting/PileupReweighting/PileupReweightingDict.h"

// Header files passed via #pragma extra_include

namespace CP {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *CP_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("CP", 0 /*version*/, "PATInterfaces/SystematicCode.h", 14,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &CP_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *CP_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *CPcLcLPileupReweightingTool_Dictionary();
   static void CPcLcLPileupReweightingTool_TClassManip(TClass*);
   static void delete_CPcLcLPileupReweightingTool(void *p);
   static void deleteArray_CPcLcLPileupReweightingTool(void *p);
   static void destruct_CPcLcLPileupReweightingTool(void *p);
   static Long64_t merge_CPcLcLPileupReweightingTool(void *obj, TCollection *coll,TFileMergeInfo *info);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::PileupReweightingTool*)
   {
      ::CP::PileupReweightingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CP::PileupReweightingTool));
      static ::ROOT::TGenericClassInfo 
         instance("CP::PileupReweightingTool", ::CP::PileupReweightingTool::Class_Version(), "PileupReweighting/PileupReweightingTool.h", 20,
                  typeid(::CP::PileupReweightingTool), DefineBehavior(ptr, ptr),
                  &CPcLcLPileupReweightingTool_Dictionary, isa_proxy, 0,
                  sizeof(::CP::PileupReweightingTool) );
      instance.SetDelete(&delete_CPcLcLPileupReweightingTool);
      instance.SetDeleteArray(&deleteArray_CPcLcLPileupReweightingTool);
      instance.SetDestructor(&destruct_CPcLcLPileupReweightingTool);
      instance.SetMerge(&merge_CPcLcLPileupReweightingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::PileupReweightingTool*)
   {
      return GenerateInitInstanceLocal((::CP::PileupReweightingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CP::PileupReweightingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CPcLcLPileupReweightingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CP::PileupReweightingTool*)0x0)->GetClass();
      CPcLcLPileupReweightingTool_TClassManip(theClass);
   return theClass;
   }

   static void CPcLcLPileupReweightingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CPcLcLIPileupReweightingTool_Dictionary();
   static void CPcLcLIPileupReweightingTool_TClassManip(TClass*);
   static void delete_CPcLcLIPileupReweightingTool(void *p);
   static void deleteArray_CPcLcLIPileupReweightingTool(void *p);
   static void destruct_CPcLcLIPileupReweightingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::IPileupReweightingTool*)
   {
      ::CP::IPileupReweightingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CP::IPileupReweightingTool));
      static ::ROOT::TGenericClassInfo 
         instance("CP::IPileupReweightingTool", "PileupReweighting/IPileupReweightingTool.h", 18,
                  typeid(::CP::IPileupReweightingTool), DefineBehavior(ptr, ptr),
                  &CPcLcLIPileupReweightingTool_Dictionary, isa_proxy, 0,
                  sizeof(::CP::IPileupReweightingTool) );
      instance.SetDelete(&delete_CPcLcLIPileupReweightingTool);
      instance.SetDeleteArray(&deleteArray_CPcLcLIPileupReweightingTool);
      instance.SetDestructor(&destruct_CPcLcLIPileupReweightingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::IPileupReweightingTool*)
   {
      return GenerateInitInstanceLocal((::CP::IPileupReweightingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CP::IPileupReweightingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CPcLcLIPileupReweightingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CP::IPileupReweightingTool*)0x0)->GetClass();
      CPcLcLIPileupReweightingTool_TClassManip(theClass);
   return theClass;
   }

   static void CPcLcLIPileupReweightingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CPcLcLPileupReweightingTool(void *p) {
      delete ((::CP::PileupReweightingTool*)p);
   }
   static void deleteArray_CPcLcLPileupReweightingTool(void *p) {
      delete [] ((::CP::PileupReweightingTool*)p);
   }
   static void destruct_CPcLcLPileupReweightingTool(void *p) {
      typedef ::CP::PileupReweightingTool current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around the merge function.
   static Long64_t  merge_CPcLcLPileupReweightingTool(void *obj,TCollection *coll,TFileMergeInfo *) {
      return ((::CP::PileupReweightingTool*)obj)->Merge(coll);
   }
} // end of namespace ROOT for class ::CP::PileupReweightingTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CPcLcLIPileupReweightingTool(void *p) {
      delete ((::CP::IPileupReweightingTool*)p);
   }
   static void deleteArray_CPcLcLIPileupReweightingTool(void *p) {
      delete [] ((::CP::IPileupReweightingTool*)p);
   }
   static void destruct_CPcLcLIPileupReweightingTool(void *p) {
      typedef ::CP::IPileupReweightingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CP::IPileupReweightingTool

namespace {
  void TriggerDictionaryInitialization_PileupReweighting_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PileupReweighting/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PileupReweighting/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace CP{class __attribute__((annotate("$clingAutoload$PileupReweighting/PileupReweightingTool.h")))  PileupReweightingTool;}
namespace CP{class __attribute__((annotate("$clingAutoload$PileupReweighting/PileupReweightingTool.h")))  IPileupReweightingTool;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "PileupReweighting"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef PILEUPREWEIGHTINGDICT_H
#define PILEUPREWEIGHTINGDICT_H

#include "PileupReweighting/PileupReweightingTool.h"

#endif 

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"CP::IPileupReweightingTool", payloadCode, "@",
"CP::PileupReweightingTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("PileupReweighting_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_PileupReweighting_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_PileupReweighting_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_PileupReweighting_Reflex() {
  TriggerDictionaryInitialization_PileupReweighting_Reflex_Impl();
}

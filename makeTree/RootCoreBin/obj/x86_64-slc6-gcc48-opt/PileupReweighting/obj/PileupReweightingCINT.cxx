#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIPileupReweightingdIobjdIPileupReweightingCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "PileupReweighting/TPileupReweighting.h"

// Header files passed via #pragma extra_include

namespace CP {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *CP_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("CP", 0 /*version*/, "PileupReweighting/TPileupReweighting.h", 44,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &CP_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *CP_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static void *new_CPcLcLTPileupReweighting(void *p = 0);
   static void *newArray_CPcLcLTPileupReweighting(Long_t size, void *p);
   static void delete_CPcLcLTPileupReweighting(void *p);
   static void deleteArray_CPcLcLTPileupReweighting(void *p);
   static void destruct_CPcLcLTPileupReweighting(void *p);
   static Long64_t merge_CPcLcLTPileupReweighting(void *obj, TCollection *coll,TFileMergeInfo *info);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::TPileupReweighting*)
   {
      ::CP::TPileupReweighting *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::CP::TPileupReweighting >(0);
      static ::ROOT::TGenericClassInfo 
         instance("CP::TPileupReweighting", ::CP::TPileupReweighting::Class_Version(), "PileupReweighting/TPileupReweighting.h", 45,
                  typeid(::CP::TPileupReweighting), DefineBehavior(ptr, ptr),
                  &::CP::TPileupReweighting::Dictionary, isa_proxy, 4,
                  sizeof(::CP::TPileupReweighting) );
      instance.SetNew(&new_CPcLcLTPileupReweighting);
      instance.SetNewArray(&newArray_CPcLcLTPileupReweighting);
      instance.SetDelete(&delete_CPcLcLTPileupReweighting);
      instance.SetDeleteArray(&deleteArray_CPcLcLTPileupReweighting);
      instance.SetDestructor(&destruct_CPcLcLTPileupReweighting);
      instance.SetMerge(&merge_CPcLcLTPileupReweighting);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::TPileupReweighting*)
   {
      return GenerateInitInstanceLocal((::CP::TPileupReweighting*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CP::TPileupReweighting*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace CP {
//______________________________________________________________________________
atomic_TClass_ptr TPileupReweighting::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TPileupReweighting::Class_Name()
{
   return "CP::TPileupReweighting";
}

//______________________________________________________________________________
const char *TPileupReweighting::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::CP::TPileupReweighting*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TPileupReweighting::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::CP::TPileupReweighting*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TPileupReweighting::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::CP::TPileupReweighting*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TPileupReweighting::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::CP::TPileupReweighting*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace CP
namespace CP {
//______________________________________________________________________________
void TPileupReweighting::Streamer(TBuffer &R__b)
{
   // Stream an object of class CP::TPileupReweighting.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(CP::TPileupReweighting::Class(),this);
   } else {
      R__b.WriteClassBuffer(CP::TPileupReweighting::Class(),this);
   }
}

} // namespace CP
namespace ROOT {
   // Wrappers around operator new
   static void *new_CPcLcLTPileupReweighting(void *p) {
      return  p ? new(p) ::CP::TPileupReweighting : new ::CP::TPileupReweighting;
   }
   static void *newArray_CPcLcLTPileupReweighting(Long_t nElements, void *p) {
      return p ? new(p) ::CP::TPileupReweighting[nElements] : new ::CP::TPileupReweighting[nElements];
   }
   // Wrapper around operator delete
   static void delete_CPcLcLTPileupReweighting(void *p) {
      delete ((::CP::TPileupReweighting*)p);
   }
   static void deleteArray_CPcLcLTPileupReweighting(void *p) {
      delete [] ((::CP::TPileupReweighting*)p);
   }
   static void destruct_CPcLcLTPileupReweighting(void *p) {
      typedef ::CP::TPileupReweighting current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around the merge function.
   static Long64_t  merge_CPcLcLTPileupReweighting(void *obj,TCollection *coll,TFileMergeInfo *) {
      return ((::CP::TPileupReweighting*)obj)->Merge(coll);
   }
} // end of namespace ROOT for class ::CP::TPileupReweighting

namespace {
  void TriggerDictionaryInitialization_PileupReweightingCINT_Impl() {
    static const char* headers[] = {
"PileupReweighting/TPileupReweighting.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PileupReweighting/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PileupReweighting/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace CP{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PileupReweighting/Root/LinkDef.h")))  TPileupReweighting;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "PileupReweighting"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "PileupReweighting/TPileupReweighting.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"CP::TPileupReweighting", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("PileupReweightingCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_PileupReweightingCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_PileupReweightingCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_PileupReweightingCINT() {
  TriggerDictionaryInitialization_PileupReweightingCINT_Impl();
}

// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIJetSelectorToolsdIobjdIJetSelectorTools_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetSelectorTools/JetSelectorTools/JetSelectorToolsDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *JetCleaningTool_Dictionary();
   static void JetCleaningTool_TClassManip(TClass*);
   static void *new_JetCleaningTool(void *p = 0);
   static void *newArray_JetCleaningTool(Long_t size, void *p);
   static void delete_JetCleaningTool(void *p);
   static void deleteArray_JetCleaningTool(void *p);
   static void destruct_JetCleaningTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetCleaningTool*)
   {
      ::JetCleaningTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JetCleaningTool));
      static ::ROOT::TGenericClassInfo 
         instance("JetCleaningTool", "JetSelectorTools/JetCleaningTool.h", 29,
                  typeid(::JetCleaningTool), DefineBehavior(ptr, ptr),
                  &JetCleaningTool_Dictionary, isa_proxy, 0,
                  sizeof(::JetCleaningTool) );
      instance.SetNew(&new_JetCleaningTool);
      instance.SetNewArray(&newArray_JetCleaningTool);
      instance.SetDelete(&delete_JetCleaningTool);
      instance.SetDeleteArray(&deleteArray_JetCleaningTool);
      instance.SetDestructor(&destruct_JetCleaningTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetCleaningTool*)
   {
      return GenerateInitInstanceLocal((::JetCleaningTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetCleaningTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JetCleaningTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JetCleaningTool*)0x0)->GetClass();
      JetCleaningTool_TClassManip(theClass);
   return theClass;
   }

   static void JetCleaningTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *JetAttributeSelector_Dictionary();
   static void JetAttributeSelector_TClassManip(TClass*);
   static void delete_JetAttributeSelector(void *p);
   static void deleteArray_JetAttributeSelector(void *p);
   static void destruct_JetAttributeSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetAttributeSelector*)
   {
      ::JetAttributeSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JetAttributeSelector));
      static ::ROOT::TGenericClassInfo 
         instance("JetAttributeSelector", "JetSelectorTools/JetAttributeSelector.h", 34,
                  typeid(::JetAttributeSelector), DefineBehavior(ptr, ptr),
                  &JetAttributeSelector_Dictionary, isa_proxy, 0,
                  sizeof(::JetAttributeSelector) );
      instance.SetDelete(&delete_JetAttributeSelector);
      instance.SetDeleteArray(&deleteArray_JetAttributeSelector);
      instance.SetDestructor(&destruct_JetAttributeSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetAttributeSelector*)
   {
      return GenerateInitInstanceLocal((::JetAttributeSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetAttributeSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JetAttributeSelector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JetAttributeSelector*)0x0)->GetClass();
      JetAttributeSelector_TClassManip(theClass);
   return theClass;
   }

   static void JetAttributeSelector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *JetAttributeRatioSelector_Dictionary();
   static void JetAttributeRatioSelector_TClassManip(TClass*);
   static void delete_JetAttributeRatioSelector(void *p);
   static void deleteArray_JetAttributeRatioSelector(void *p);
   static void destruct_JetAttributeRatioSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetAttributeRatioSelector*)
   {
      ::JetAttributeRatioSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JetAttributeRatioSelector));
      static ::ROOT::TGenericClassInfo 
         instance("JetAttributeRatioSelector", "JetSelectorTools/JetAttributeSelector.h", 77,
                  typeid(::JetAttributeRatioSelector), DefineBehavior(ptr, ptr),
                  &JetAttributeRatioSelector_Dictionary, isa_proxy, 0,
                  sizeof(::JetAttributeRatioSelector) );
      instance.SetDelete(&delete_JetAttributeRatioSelector);
      instance.SetDeleteArray(&deleteArray_JetAttributeRatioSelector);
      instance.SetDestructor(&destruct_JetAttributeRatioSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetAttributeRatioSelector*)
   {
      return GenerateInitInstanceLocal((::JetAttributeRatioSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetAttributeRatioSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JetAttributeRatioSelector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JetAttributeRatioSelector*)0x0)->GetClass();
      JetAttributeRatioSelector_TClassManip(theClass);
   return theClass;
   }

   static void JetAttributeRatioSelector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *JetAbsAttributeSelector_Dictionary();
   static void JetAbsAttributeSelector_TClassManip(TClass*);
   static void delete_JetAbsAttributeSelector(void *p);
   static void deleteArray_JetAbsAttributeSelector(void *p);
   static void destruct_JetAbsAttributeSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetAbsAttributeSelector*)
   {
      ::JetAbsAttributeSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JetAbsAttributeSelector));
      static ::ROOT::TGenericClassInfo 
         instance("JetAbsAttributeSelector", "JetSelectorTools/JetAttributeSelector.h", 68,
                  typeid(::JetAbsAttributeSelector), DefineBehavior(ptr, ptr),
                  &JetAbsAttributeSelector_Dictionary, isa_proxy, 0,
                  sizeof(::JetAbsAttributeSelector) );
      instance.SetDelete(&delete_JetAbsAttributeSelector);
      instance.SetDeleteArray(&deleteArray_JetAbsAttributeSelector);
      instance.SetDestructor(&destruct_JetAbsAttributeSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetAbsAttributeSelector*)
   {
      return GenerateInitInstanceLocal((::JetAbsAttributeSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetAbsAttributeSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JetAbsAttributeSelector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JetAbsAttributeSelector*)0x0)->GetClass();
      JetAbsAttributeSelector_TClassManip(theClass);
   return theClass;
   }

   static void JetAbsAttributeSelector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetCleaningTool(void *p) {
      return  p ? new(p) ::JetCleaningTool : new ::JetCleaningTool;
   }
   static void *newArray_JetCleaningTool(Long_t nElements, void *p) {
      return p ? new(p) ::JetCleaningTool[nElements] : new ::JetCleaningTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetCleaningTool(void *p) {
      delete ((::JetCleaningTool*)p);
   }
   static void deleteArray_JetCleaningTool(void *p) {
      delete [] ((::JetCleaningTool*)p);
   }
   static void destruct_JetCleaningTool(void *p) {
      typedef ::JetCleaningTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetCleaningTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JetAttributeSelector(void *p) {
      delete ((::JetAttributeSelector*)p);
   }
   static void deleteArray_JetAttributeSelector(void *p) {
      delete [] ((::JetAttributeSelector*)p);
   }
   static void destruct_JetAttributeSelector(void *p) {
      typedef ::JetAttributeSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetAttributeSelector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JetAttributeRatioSelector(void *p) {
      delete ((::JetAttributeRatioSelector*)p);
   }
   static void deleteArray_JetAttributeRatioSelector(void *p) {
      delete [] ((::JetAttributeRatioSelector*)p);
   }
   static void destruct_JetAttributeRatioSelector(void *p) {
      typedef ::JetAttributeRatioSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetAttributeRatioSelector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JetAbsAttributeSelector(void *p) {
      delete ((::JetAbsAttributeSelector*)p);
   }
   static void deleteArray_JetAbsAttributeSelector(void *p) {
      delete [] ((::JetAbsAttributeSelector*)p);
   }
   static void destruct_JetAbsAttributeSelector(void *p) {
      typedef ::JetAbsAttributeSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetAbsAttributeSelector

namespace {
  void TriggerDictionaryInitialization_JetSelectorTools_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetSelectorTools/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetSelectorTools",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetSelectorTools/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$JetSelectorTools/JetCleaningTool.h")))  JetCleaningTool;
class __attribute__((annotate("$clingAutoload$JetSelectorTools/JetAttributeSelector.h")))  JetAttributeSelector;
class __attribute__((annotate("$clingAutoload$JetSelectorTools/JetAttributeSelector.h")))  JetAttributeRatioSelector;
class __attribute__((annotate("$clingAutoload$JetSelectorTools/JetAttributeSelector.h")))  JetAbsAttributeSelector;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "JetSelectorTools"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef JETSELECTORTOOLS_JETSELECTORTOOLSDICT_H
#define JETSELECTORTOOLS_JETSELECTORTOOLSDICT_H

#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// Includes for the dictionary generation:
#include "JetSelectorTools/JetCleaningTool.h"

#include "JetSelectorTools/JetAttributeSelector.h"

#endif // JETSELECTORTOOLS_JETSELECTORTOOLSDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"JetAbsAttributeSelector", payloadCode, "@",
"JetAttributeRatioSelector", payloadCode, "@",
"JetAttributeSelector", payloadCode, "@",
"JetCleaningTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("JetSelectorTools_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_JetSelectorTools_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_JetSelectorTools_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_JetSelectorTools_Reflex() {
  TriggerDictionaryInitialization_JetSelectorTools_Reflex_Impl();
}

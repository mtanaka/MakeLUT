// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODEventInfodIobjdIxAODEventInfo_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventInfo/xAODEventInfo/xAODEventInfoDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLEventInfo_v1_Dictionary();
   static void xAODcLcLEventInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEventInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLEventInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLEventInfo_v1(void *p);
   static void deleteArray_xAODcLcLEventInfo_v1(void *p);
   static void destruct_xAODcLcLEventInfo_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLEventInfo_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::EventInfo_v1* newObj = (xAOD::EventInfo_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       newObj->setStore( ( SG::IAuxStore* ) 0 );
     
   }
   static void read_xAODcLcLEventInfo_v1_1( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::EventInfo_v1");
      static Long_t offset_m_updateStreamTags = cls->GetDataMemberOffset("m_updateStreamTags");
      bool& m_updateStreamTags = *(bool*)(target+offset_m_updateStreamTags);
      xAOD::EventInfo_v1* newObj = (xAOD::EventInfo_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_updateStreamTags = true;
     
   }
   static void read_xAODcLcLEventInfo_v1_2( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::EventInfo_v1");
      static Long_t offset_m_updateSubEvents = cls->GetDataMemberOffset("m_updateSubEvents");
      bool& m_updateSubEvents = *(bool*)(target+offset_m_updateSubEvents);
      xAOD::EventInfo_v1* newObj = (xAOD::EventInfo_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_updateSubEvents = true;
     
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EventInfo_v1*)
   {
      ::xAOD::EventInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EventInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EventInfo_v1", "xAODEventInfo/versions/EventInfo_v1.h", 39,
                  typeid(::xAOD::EventInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEventInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EventInfo_v1) );
      instance.SetNew(&new_xAODcLcLEventInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLEventInfo_v1);
      instance.SetDelete(&delete_xAODcLcLEventInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEventInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLEventInfo_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(3);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::EventInfo_v1";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLEventInfo_v1_0);
      rule->fCode        = "\n       newObj->setStore( ( SG::IAuxStore* ) 0 );\n     ";
      rule->fVersion     = "[1-]";
      rule = &readrules[1];
      rule->fSourceClass = "xAOD::EventInfo_v1";
      rule->fTarget      = "m_updateStreamTags";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLEventInfo_v1_1);
      rule->fCode        = "\n       m_updateStreamTags = true;\n     ";
      rule->fVersion     = "[1-]";
      rule = &readrules[2];
      rule->fSourceClass = "xAOD::EventInfo_v1";
      rule->fTarget      = "m_updateSubEvents";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLEventInfo_v1_2);
      rule->fCode        = "\n       m_updateSubEvents = true;\n     ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EventInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EventInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EventInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEventInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EventInfo_v1*)0x0)->GetClass();
      xAODcLcLEventInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEventInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","AE8BED6D-1D41-4CAF-994B-42613FC91A0A");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEventAuxInfo_v1_Dictionary();
   static void xAODcLcLEventAuxInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEventAuxInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLEventAuxInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLEventAuxInfo_v1(void *p);
   static void deleteArray_xAODcLcLEventAuxInfo_v1(void *p);
   static void destruct_xAODcLcLEventAuxInfo_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EventAuxInfo_v1*)
   {
      ::xAOD::EventAuxInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EventAuxInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EventAuxInfo_v1", "xAODEventInfo/versions/EventAuxInfo_v1.h", 28,
                  typeid(::xAOD::EventAuxInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEventAuxInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EventAuxInfo_v1) );
      instance.SetNew(&new_xAODcLcLEventAuxInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLEventAuxInfo_v1);
      instance.SetDelete(&delete_xAODcLcLEventAuxInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEventAuxInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLEventAuxInfo_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EventAuxInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EventAuxInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EventAuxInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEventAuxInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EventAuxInfo_v1*)0x0)->GetClass();
      xAODcLcLEventAuxInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEventAuxInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","2CFD72A2-D3AA-4C18-9B40-E5ACBA20D785");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLEventInfo_v1gR_Dictionary();
   static void DataVectorlExAODcLcLEventInfo_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLEventInfo_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLEventInfo_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLEventInfo_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLEventInfo_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLEventInfo_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::EventInfo_v1>*)
   {
      ::DataVector<xAOD::EventInfo_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::EventInfo_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::EventInfo_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::EventInfo_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLEventInfo_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::EventInfo_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLEventInfo_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLEventInfo_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLEventInfo_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLEventInfo_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLEventInfo_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::EventInfo_v1>","xAOD::EventInfoContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::EventInfo_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::EventInfo_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::EventInfo_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLEventInfo_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::EventInfo_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLEventInfo_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLEventInfo_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","8F061263-D744-4D72-9377-1573FE21CDCE");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEventInfoAuxContainer_v1_Dictionary();
   static void xAODcLcLEventInfoAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEventInfoAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLEventInfoAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLEventInfoAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLEventInfoAuxContainer_v1(void *p);
   static void destruct_xAODcLcLEventInfoAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EventInfoAuxContainer_v1*)
   {
      ::xAOD::EventInfoAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EventInfoAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EventInfoAuxContainer_v1", "xAODEventInfo/versions/EventInfoAuxContainer_v1.h", 28,
                  typeid(::xAOD::EventInfoAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEventInfoAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EventInfoAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLEventInfoAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLEventInfoAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLEventInfoAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEventInfoAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLEventInfoAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EventInfoAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EventInfoAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EventInfoAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEventInfoAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EventInfoAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLEventInfoAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEventInfoAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","742479C0-2699-4949-A9D0-01DBC421BE5B");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklExAODcLcLEventInfo_v1gR_Dictionary();
   static void DataLinklExAODcLcLEventInfo_v1gR_TClassManip(TClass*);
   static void *new_DataLinklExAODcLcLEventInfo_v1gR(void *p = 0);
   static void *newArray_DataLinklExAODcLcLEventInfo_v1gR(Long_t size, void *p);
   static void delete_DataLinklExAODcLcLEventInfo_v1gR(void *p);
   static void deleteArray_DataLinklExAODcLcLEventInfo_v1gR(void *p);
   static void destruct_DataLinklExAODcLcLEventInfo_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<xAOD::EventInfo_v1>*)
   {
      ::DataLink<xAOD::EventInfo_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<xAOD::EventInfo_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<xAOD::EventInfo_v1>", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<xAOD::EventInfo_v1>), DefineBehavior(ptr, ptr),
                  &DataLinklExAODcLcLEventInfo_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<xAOD::EventInfo_v1>) );
      instance.SetNew(&new_DataLinklExAODcLcLEventInfo_v1gR);
      instance.SetNewArray(&newArray_DataLinklExAODcLcLEventInfo_v1gR);
      instance.SetDelete(&delete_DataLinklExAODcLcLEventInfo_v1gR);
      instance.SetDeleteArray(&deleteArray_DataLinklExAODcLcLEventInfo_v1gR);
      instance.SetDestructor(&destruct_DataLinklExAODcLcLEventInfo_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<xAOD::EventInfo_v1>*)
   {
      return GenerateInitInstanceLocal((::DataLink<xAOD::EventInfo_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<xAOD::EventInfo_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklExAODcLcLEventInfo_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<xAOD::EventInfo_v1>*)0x0)->GetClass();
      DataLinklExAODcLcLEventInfo_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklExAODcLcLEventInfo_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::EventInfo_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::EventInfo_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::EventInfo_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::EventInfo_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::EventInfo_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::EventInfo_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::EventInfo_v1> >","ElementLink<xAOD::EventInfoContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::EventInfo_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::EventInfo_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::EventInfo_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::EventInfo_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEstringcOstringgR_Dictionary();
   static void pairlEstringcOstringgR_TClassManip(TClass*);
   static void *new_pairlEstringcOstringgR(void *p = 0);
   static void *newArray_pairlEstringcOstringgR(Long_t size, void *p);
   static void delete_pairlEstringcOstringgR(void *p);
   static void deleteArray_pairlEstringcOstringgR(void *p);
   static void destruct_pairlEstringcOstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<string,string>*)
   {
      pair<string,string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<string,string>));
      static ::ROOT::TGenericClassInfo 
         instance("pair<string,string>", "string", 96,
                  typeid(pair<string,string>), DefineBehavior(ptr, ptr),
                  &pairlEstringcOstringgR_Dictionary, isa_proxy, 0,
                  sizeof(pair<string,string>) );
      instance.SetNew(&new_pairlEstringcOstringgR);
      instance.SetNewArray(&newArray_pairlEstringcOstringgR);
      instance.SetDelete(&delete_pairlEstringcOstringgR);
      instance.SetDeleteArray(&deleteArray_pairlEstringcOstringgR);
      instance.SetDestructor(&destruct_pairlEstringcOstringgR);

      ROOT::AddClassAlternate("pair<string,string>","pair<std::string,std::string>");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<string,string>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEstringcOstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<string,string>*)0x0)->GetClass();
      pairlEstringcOstringgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEstringcOstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEventInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventInfo_v1 : new ::xAOD::EventInfo_v1;
   }
   static void *newArray_xAODcLcLEventInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventInfo_v1[nElements] : new ::xAOD::EventInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEventInfo_v1(void *p) {
      delete ((::xAOD::EventInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLEventInfo_v1(void *p) {
      delete [] ((::xAOD::EventInfo_v1*)p);
   }
   static void destruct_xAODcLcLEventInfo_v1(void *p) {
      typedef ::xAOD::EventInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EventInfo_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEventAuxInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventAuxInfo_v1 : new ::xAOD::EventAuxInfo_v1;
   }
   static void *newArray_xAODcLcLEventAuxInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventAuxInfo_v1[nElements] : new ::xAOD::EventAuxInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEventAuxInfo_v1(void *p) {
      delete ((::xAOD::EventAuxInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLEventAuxInfo_v1(void *p) {
      delete [] ((::xAOD::EventAuxInfo_v1*)p);
   }
   static void destruct_xAODcLcLEventAuxInfo_v1(void *p) {
      typedef ::xAOD::EventAuxInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EventAuxInfo_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLEventInfo_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::EventInfo_v1> : new ::DataVector<xAOD::EventInfo_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLEventInfo_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::EventInfo_v1>[nElements] : new ::DataVector<xAOD::EventInfo_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLEventInfo_v1gR(void *p) {
      delete ((::DataVector<xAOD::EventInfo_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLEventInfo_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::EventInfo_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLEventInfo_v1gR(void *p) {
      typedef ::DataVector<xAOD::EventInfo_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::EventInfo_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEventInfoAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventInfoAuxContainer_v1 : new ::xAOD::EventInfoAuxContainer_v1;
   }
   static void *newArray_xAODcLcLEventInfoAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventInfoAuxContainer_v1[nElements] : new ::xAOD::EventInfoAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEventInfoAuxContainer_v1(void *p) {
      delete ((::xAOD::EventInfoAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLEventInfoAuxContainer_v1(void *p) {
      delete [] ((::xAOD::EventInfoAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLEventInfoAuxContainer_v1(void *p) {
      typedef ::xAOD::EventInfoAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EventInfoAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklExAODcLcLEventInfo_v1gR(void *p) {
      return  p ? new(p) ::DataLink<xAOD::EventInfo_v1> : new ::DataLink<xAOD::EventInfo_v1>;
   }
   static void *newArray_DataLinklExAODcLcLEventInfo_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<xAOD::EventInfo_v1>[nElements] : new ::DataLink<xAOD::EventInfo_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklExAODcLcLEventInfo_v1gR(void *p) {
      delete ((::DataLink<xAOD::EventInfo_v1>*)p);
   }
   static void deleteArray_DataLinklExAODcLcLEventInfo_v1gR(void *p) {
      delete [] ((::DataLink<xAOD::EventInfo_v1>*)p);
   }
   static void destruct_DataLinklExAODcLcLEventInfo_v1gR(void *p) {
      typedef ::DataLink<xAOD::EventInfo_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<xAOD::EventInfo_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::EventInfo_v1> > : new ::ElementLink<DataVector<xAOD::EventInfo_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::EventInfo_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::EventInfo_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::EventInfo_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::EventInfo_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::EventInfo_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::EventInfo_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEstringcOstringgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,string> : new pair<string,string>;
   }
   static void *newArray_pairlEstringcOstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,string>[nElements] : new pair<string,string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEstringcOstringgR(void *p) {
      delete ((pair<string,string>*)p);
   }
   static void deleteArray_pairlEstringcOstringgR(void *p) {
      delete [] ((pair<string,string>*)p);
   }
   static void destruct_pairlEstringcOstringgR(void *p) {
      typedef pair<string,string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<string,string>

namespace ROOT {
   static TClass *vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<pair<string,string> > >*)
   {
      vector<vector<pair<string,string> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<pair<string,string> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<pair<string,string> > >", -2, "vector", 210,
                  typeid(vector<vector<pair<string,string> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<pair<string,string> > >) );
      instance.SetNew(&new_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<pair<string,string> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<pair<string,string> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<pair<string,string> > >*)0x0)->GetClass();
      vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<pair<string,string> > > : new vector<vector<pair<string,string> > >;
   }
   static void *newArray_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<pair<string,string> > >[nElements] : new vector<vector<pair<string,string> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(void *p) {
      delete ((vector<vector<pair<string,string> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<pair<string,string> > >*)p);
   }
   static void destruct_vectorlEvectorlEpairlEstringcOstringgRsPgRsPgR(void *p) {
      typedef vector<vector<pair<string,string> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<pair<string,string> > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >

namespace ROOT {
   static TClass *vectorlEsetlEunsignedsPintgRsPgR_Dictionary();
   static void vectorlEsetlEunsignedsPintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEsetlEunsignedsPintgRsPgR(void *p = 0);
   static void *newArray_vectorlEsetlEunsignedsPintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEsetlEunsignedsPintgRsPgR(void *p);
   static void deleteArray_vectorlEsetlEunsignedsPintgRsPgR(void *p);
   static void destruct_vectorlEsetlEunsignedsPintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<set<unsigned int> >*)
   {
      vector<set<unsigned int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<set<unsigned int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<set<unsigned int> >", -2, "vector", 210,
                  typeid(vector<set<unsigned int> >), DefineBehavior(ptr, ptr),
                  &vectorlEsetlEunsignedsPintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<set<unsigned int> >) );
      instance.SetNew(&new_vectorlEsetlEunsignedsPintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEsetlEunsignedsPintgRsPgR);
      instance.SetDelete(&delete_vectorlEsetlEunsignedsPintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEsetlEunsignedsPintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEsetlEunsignedsPintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<set<unsigned int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<set<unsigned int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEsetlEunsignedsPintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<set<unsigned int> >*)0x0)->GetClass();
      vectorlEsetlEunsignedsPintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEsetlEunsignedsPintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEsetlEunsignedsPintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<set<unsigned int> > : new vector<set<unsigned int> >;
   }
   static void *newArray_vectorlEsetlEunsignedsPintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<set<unsigned int> >[nElements] : new vector<set<unsigned int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEsetlEunsignedsPintgRsPgR(void *p) {
      delete ((vector<set<unsigned int> >*)p);
   }
   static void deleteArray_vectorlEsetlEunsignedsPintgRsPgR(void *p) {
      delete [] ((vector<set<unsigned int> >*)p);
   }
   static void destruct_vectorlEsetlEunsignedsPintgRsPgR(void *p) {
      typedef vector<set<unsigned int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<set<unsigned int> >

namespace ROOT {
   static TClass *vectorlEpairlEstringcOstringgRsPgR_Dictionary();
   static void vectorlEpairlEstringcOstringgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEpairlEstringcOstringgRsPgR(void *p = 0);
   static void *newArray_vectorlEpairlEstringcOstringgRsPgR(Long_t size, void *p);
   static void delete_vectorlEpairlEstringcOstringgRsPgR(void *p);
   static void deleteArray_vectorlEpairlEstringcOstringgRsPgR(void *p);
   static void destruct_vectorlEpairlEstringcOstringgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<pair<string,string> >*)
   {
      vector<pair<string,string> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<pair<string,string> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<pair<string,string> >", -2, "vector", 210,
                  typeid(vector<pair<string,string> >), DefineBehavior(ptr, ptr),
                  &vectorlEpairlEstringcOstringgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<pair<string,string> >) );
      instance.SetNew(&new_vectorlEpairlEstringcOstringgRsPgR);
      instance.SetNewArray(&newArray_vectorlEpairlEstringcOstringgRsPgR);
      instance.SetDelete(&delete_vectorlEpairlEstringcOstringgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEpairlEstringcOstringgRsPgR);
      instance.SetDestructor(&destruct_vectorlEpairlEstringcOstringgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<pair<string,string> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<pair<string,string> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEpairlEstringcOstringgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<pair<string,string> >*)0x0)->GetClass();
      vectorlEpairlEstringcOstringgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEpairlEstringcOstringgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEpairlEstringcOstringgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<pair<string,string> > : new vector<pair<string,string> >;
   }
   static void *newArray_vectorlEpairlEstringcOstringgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<pair<string,string> >[nElements] : new vector<pair<string,string> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEpairlEstringcOstringgRsPgR(void *p) {
      delete ((vector<pair<string,string> >*)p);
   }
   static void deleteArray_vectorlEpairlEstringcOstringgRsPgR(void *p) {
      delete [] ((vector<pair<string,string> >*)p);
   }
   static void destruct_vectorlEpairlEstringcOstringgRsPgR(void *p) {
      typedef vector<pair<string,string> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<pair<string,string> >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > : new vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLEventInfo_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR_Dictionary();
   static void vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(void *p);
   static void destruct_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<xAOD::EventInfo_v1> >*)
   {
      vector<DataLink<xAOD::EventInfo_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<xAOD::EventInfo_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<xAOD::EventInfo_v1> >", -2, "vector", 210,
                  typeid(vector<DataLink<xAOD::EventInfo_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<xAOD::EventInfo_v1> >) );
      instance.SetNew(&new_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<xAOD::EventInfo_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<xAOD::EventInfo_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<xAOD::EventInfo_v1> >*)0x0)->GetClass();
      vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::EventInfo_v1> > : new vector<DataLink<xAOD::EventInfo_v1> >;
   }
   static void *newArray_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::EventInfo_v1> >[nElements] : new vector<DataLink<xAOD::EventInfo_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(void *p) {
      delete ((vector<DataLink<xAOD::EventInfo_v1> >*)p);
   }
   static void deleteArray_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(void *p) {
      delete [] ((vector<DataLink<xAOD::EventInfo_v1> >*)p);
   }
   static void destruct_vectorlEDataLinklExAODcLcLEventInfo_v1gRsPgR(void *p) {
      typedef vector<DataLink<xAOD::EventInfo_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<xAOD::EventInfo_v1> >

namespace ROOT {
   static TClass *setlEunsignedsPintgR_Dictionary();
   static void setlEunsignedsPintgR_TClassManip(TClass*);
   static void *new_setlEunsignedsPintgR(void *p = 0);
   static void *newArray_setlEunsignedsPintgR(Long_t size, void *p);
   static void delete_setlEunsignedsPintgR(void *p);
   static void deleteArray_setlEunsignedsPintgR(void *p);
   static void destruct_setlEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const set<unsigned int>*)
   {
      set<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(set<unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("set<unsigned int>", -2, "set", 90,
                  typeid(set<unsigned int>), DefineBehavior(ptr, ptr),
                  &setlEunsignedsPintgR_Dictionary, isa_proxy, 4,
                  sizeof(set<unsigned int>) );
      instance.SetNew(&new_setlEunsignedsPintgR);
      instance.SetNewArray(&newArray_setlEunsignedsPintgR);
      instance.SetDelete(&delete_setlEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_setlEunsignedsPintgR);
      instance.SetDestructor(&destruct_setlEunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Insert< set<unsigned int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const set<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *setlEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const set<unsigned int>*)0x0)->GetClass();
      setlEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void setlEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_setlEunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) set<unsigned int> : new set<unsigned int>;
   }
   static void *newArray_setlEunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) set<unsigned int>[nElements] : new set<unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_setlEunsignedsPintgR(void *p) {
      delete ((set<unsigned int>*)p);
   }
   static void deleteArray_setlEunsignedsPintgR(void *p) {
      delete [] ((set<unsigned int>*)p);
   }
   static void destruct_setlEunsignedsPintgR(void *p) {
      typedef set<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class set<unsigned int>

namespace {
  void TriggerDictionaryInitialization_xAODEventInfo_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventInfo/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventInfo/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@AE8BED6D-1D41-4CAF-994B-42613FC91A0A)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEventInfo/EventInfo.h")))  EventInfo_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@2CFD72A2-D3AA-4C18-9B40-E5ACBA20D785)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEventInfo/versions/EventAuxInfo_v1.h")))  EventAuxInfo_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@742479C0-2699-4949-A9D0-01DBC421BE5B)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEventInfo/versions/EventInfoAuxContainer_v1.h")))  EventInfoAuxContainer_v1;}
template <typename STORABLE> class __attribute__((annotate("$clingAutoload$AthLinks/DataLink.h")))  DataLink;

namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace std{template <class _CharT> struct __attribute__((annotate("$clingAutoload$string")))  char_traits;
}
namespace std{template <typename _Tp> struct __attribute__((annotate("$clingAutoload$string")))  less;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODEventInfo"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODEventInfoDict.h 630576 2014-11-24 12:36:44Z krasznaa $
#ifndef XAODEVENTINFO_XAODEVENTINFODICT_H
#define XAODEVENTINFO_XAODEVENTINFODICT_H

// System include(s):
#include <vector>
#include <set>

// EDM include(s):
#include "AthLinks/ElementLink.h"
#include "AthLinks/DataLink.h"

// Local include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventInfoContainer.h"
#include "xAODEventInfo/versions/EventInfo_v1.h"
#include "xAODEventInfo/versions/EventAuxInfo_v1.h"
#include "xAODEventInfo/versions/EventInfoContainer_v1.h"
#include "xAODEventInfo/versions/EventInfoAuxContainer_v1.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODEVENTINFO {

      xAOD::EventInfoContainer_v1 c1;

      DataLink< xAOD::EventInfo_v1 > dl1;
      std::vector< DataLink< xAOD::EventInfo_v1 > > dl2;

      ElementLink< xAOD::EventInfoContainer_v1 > el1;
      std::vector< ElementLink< xAOD::EventInfoContainer_v1 > > el2;
      std::vector< std::vector< ElementLink< xAOD::EventInfoContainer_v1 > > > el3;

      std::set< uint32_t > set1;
      std::vector< std::set< uint32_t > > set2;

   };
} // private namespace

#endif // XAODEVENTINFO_XAODEVENTINFODICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<xAOD::EventInfo_v1>", payloadCode, "@",
"DataVector<xAOD::EventInfo_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::EventInfo_v1> >", payloadCode, "@",
"ElementLink<xAOD::EventInfoContainer_v1>", payloadCode, "@",
"pair<std::string,std::string>", payloadCode, "@",
"pair<string,string>", payloadCode, "@",
"set<uint32_t>", payloadCode, "@",
"set<unsigned int>", payloadCode, "@",
"vector<DataLink<xAOD::EventInfo_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::EventInfo_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::EventInfoContainer_v1> >", payloadCode, "@",
"vector<pair<string,string> >", payloadCode, "@",
"vector<set<unsigned int> >", payloadCode, "@",
"vector<std::pair<std::string,std::string> >", payloadCode, "@",
"vector<std::set<uint32_t> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::EventInfoContainer_v1> > >", payloadCode, "@",
"vector<std::vector<std::pair<std::string,std::string> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::EventInfo_v1> > > >", payloadCode, "@",
"vector<vector<pair<string,string> > >", payloadCode, "@",
"xAOD::EventAuxInfo_v1", payloadCode, "@",
"xAOD::EventInfoAuxContainer_v1", payloadCode, "@",
"xAOD::EventInfoContainer_v1", payloadCode, "@",
"xAOD::EventInfo_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODEventInfo_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODEventInfo_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODEventInfo_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODEventInfo_Reflex() {
  TriggerDictionaryInitialization_xAODEventInfo_Reflex_Impl();
}

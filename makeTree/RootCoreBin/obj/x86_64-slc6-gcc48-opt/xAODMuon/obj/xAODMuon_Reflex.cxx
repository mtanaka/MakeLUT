// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODMuondIobjdIxAODMuon_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODMuon/xAODMuon/xAODMuonDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLMuonAuxContainer_v2_Dictionary();
   static void xAODcLcLMuonAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLMuonAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLMuonAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLMuonAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLMuonAuxContainer_v2(void *p);
   static void destruct_xAODcLcLMuonAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MuonAuxContainer_v2*)
   {
      ::xAOD::MuonAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MuonAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MuonAuxContainer_v2", "xAODMuon/versions/MuonAuxContainer_v2.h", 27,
                  typeid(::xAOD::MuonAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLMuonAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MuonAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLMuonAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLMuonAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLMuonAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMuonAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLMuonAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MuonAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::MuonAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MuonAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMuonAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MuonAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLMuonAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMuonAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","AB53C81C-B10C-11E4-AC59-6C3BE51AB9F1");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMuonAuxContainer_v3_Dictionary();
   static void xAODcLcLMuonAuxContainer_v3_TClassManip(TClass*);
   static void *new_xAODcLcLMuonAuxContainer_v3(void *p = 0);
   static void *newArray_xAODcLcLMuonAuxContainer_v3(Long_t size, void *p);
   static void delete_xAODcLcLMuonAuxContainer_v3(void *p);
   static void deleteArray_xAODcLcLMuonAuxContainer_v3(void *p);
   static void destruct_xAODcLcLMuonAuxContainer_v3(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MuonAuxContainer_v3*)
   {
      ::xAOD::MuonAuxContainer_v3 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MuonAuxContainer_v3));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MuonAuxContainer_v3", "xAODMuon/versions/MuonAuxContainer_v3.h", 27,
                  typeid(::xAOD::MuonAuxContainer_v3), DefineBehavior(ptr, ptr),
                  &xAODcLcLMuonAuxContainer_v3_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MuonAuxContainer_v3) );
      instance.SetNew(&new_xAODcLcLMuonAuxContainer_v3);
      instance.SetNewArray(&newArray_xAODcLcLMuonAuxContainer_v3);
      instance.SetDelete(&delete_xAODcLcLMuonAuxContainer_v3);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMuonAuxContainer_v3);
      instance.SetDestructor(&destruct_xAODcLcLMuonAuxContainer_v3);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MuonAuxContainer_v3*)
   {
      return GenerateInitInstanceLocal((::xAOD::MuonAuxContainer_v3*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MuonAuxContainer_v3*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMuonAuxContainer_v3_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MuonAuxContainer_v3*)0x0)->GetClass();
      xAODcLcLMuonAuxContainer_v3_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMuonAuxContainer_v3_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","ECC65005-302B-4662-ACBA-D2CE5A0218B8");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMuon_v1_Dictionary();
   static void xAODcLcLMuon_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMuon_v1(void *p = 0);
   static void *newArray_xAODcLcLMuon_v1(Long_t size, void *p);
   static void delete_xAODcLcLMuon_v1(void *p);
   static void deleteArray_xAODcLcLMuon_v1(void *p);
   static void destruct_xAODcLcLMuon_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLMuon_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::Muon_v1");
      static Long_t offset_m_p4Cached1 = cls->GetDataMemberOffset("m_p4Cached1");
      bool& m_p4Cached1 = *(bool*)(target+offset_m_p4Cached1);
      xAOD::Muon_v1* newObj = (xAOD::Muon_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
      m_p4Cached1 = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::Muon_v1*)
   {
      ::xAOD::Muon_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::Muon_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::Muon_v1", "xAODMuon/versions/Muon_v1.h", 31,
                  typeid(::xAOD::Muon_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMuon_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::Muon_v1) );
      instance.SetNew(&new_xAODcLcLMuon_v1);
      instance.SetNewArray(&newArray_xAODcLcLMuon_v1);
      instance.SetDelete(&delete_xAODcLcLMuon_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMuon_v1);
      instance.SetDestructor(&destruct_xAODcLcLMuon_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::Muon_v1";
      rule->fTarget      = "m_p4Cached1";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLMuon_v1_0);
      rule->fCode        = "\n      m_p4Cached1 = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::Muon_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::Muon_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::Muon_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMuon_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::Muon_v1*)0x0)->GetClass();
      xAODcLcLMuon_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMuon_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLMuon_v1gR_Dictionary();
   static void DataVectorlExAODcLcLMuon_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLMuon_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLMuon_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLMuon_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLMuon_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLMuon_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::Muon_v1>*)
   {
      ::DataVector<xAOD::Muon_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::Muon_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::Muon_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::Muon_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLMuon_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::Muon_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLMuon_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLMuon_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLMuon_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLMuon_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLMuon_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::Muon_v1>","xAOD::MuonContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::Muon_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::Muon_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::Muon_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLMuon_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::Muon_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLMuon_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLMuon_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F84AE51A-F309-4844-B286-8E94C655B724");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMuonAuxContainer_v1_Dictionary();
   static void xAODcLcLMuonAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMuonAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLMuonAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLMuonAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLMuonAuxContainer_v1(void *p);
   static void destruct_xAODcLcLMuonAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MuonAuxContainer_v1*)
   {
      ::xAOD::MuonAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MuonAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MuonAuxContainer_v1", "xAODMuon/versions/MuonAuxContainer_v1.h", 27,
                  typeid(::xAOD::MuonAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMuonAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MuonAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLMuonAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLMuonAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLMuonAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMuonAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLMuonAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MuonAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MuonAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MuonAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMuonAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MuonAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLMuonAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMuonAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","EC9B677A-B3BA-4C75-87D3-373FC478291E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::Muon_v1> >*)
   {
      ::DataLink<DataVector<xAOD::Muon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::Muon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::Muon_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::Muon_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::Muon_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::Muon_v1> >","DataLink<xAOD::MuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::Muon_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::Muon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Muon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Muon_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::Muon_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::Muon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::Muon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::Muon_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::Muon_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::Muon_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::Muon_v1> >","ElementLink<xAOD::MuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::Muon_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::Muon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Muon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Muon_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMuonSegment_v1_Dictionary();
   static void xAODcLcLMuonSegment_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMuonSegment_v1(void *p = 0);
   static void *newArray_xAODcLcLMuonSegment_v1(Long_t size, void *p);
   static void delete_xAODcLcLMuonSegment_v1(void *p);
   static void deleteArray_xAODcLcLMuonSegment_v1(void *p);
   static void destruct_xAODcLcLMuonSegment_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MuonSegment_v1*)
   {
      ::xAOD::MuonSegment_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MuonSegment_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MuonSegment_v1", "xAODMuon/versions/MuonSegment_v1.h", 29,
                  typeid(::xAOD::MuonSegment_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMuonSegment_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MuonSegment_v1) );
      instance.SetNew(&new_xAODcLcLMuonSegment_v1);
      instance.SetNewArray(&newArray_xAODcLcLMuonSegment_v1);
      instance.SetDelete(&delete_xAODcLcLMuonSegment_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMuonSegment_v1);
      instance.SetDestructor(&destruct_xAODcLcLMuonSegment_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MuonSegment_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MuonSegment_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MuonSegment_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMuonSegment_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MuonSegment_v1*)0x0)->GetClass();
      xAODcLcLMuonSegment_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMuonSegment_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMuonSegmentAuxContainer_v1_Dictionary();
   static void xAODcLcLMuonSegmentAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMuonSegmentAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLMuonSegmentAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLMuonSegmentAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLMuonSegmentAuxContainer_v1(void *p);
   static void destruct_xAODcLcLMuonSegmentAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MuonSegmentAuxContainer_v1*)
   {
      ::xAOD::MuonSegmentAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MuonSegmentAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MuonSegmentAuxContainer_v1", "xAODMuon/versions/MuonSegmentAuxContainer_v1.h", 27,
                  typeid(::xAOD::MuonSegmentAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMuonSegmentAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MuonSegmentAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLMuonSegmentAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLMuonSegmentAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLMuonSegmentAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMuonSegmentAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLMuonSegmentAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MuonSegmentAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MuonSegmentAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MuonSegmentAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMuonSegmentAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MuonSegmentAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLMuonSegmentAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMuonSegmentAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","51739E92-98A5-11E3-B7F4-6C3BE51AB9F1");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLMuonSegment_v1gR_Dictionary();
   static void DataVectorlExAODcLcLMuonSegment_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLMuonSegment_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLMuonSegment_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLMuonSegment_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLMuonSegment_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLMuonSegment_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::MuonSegment_v1>*)
   {
      ::DataVector<xAOD::MuonSegment_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::MuonSegment_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::MuonSegment_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::MuonSegment_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLMuonSegment_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::MuonSegment_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLMuonSegment_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLMuonSegment_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLMuonSegment_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLMuonSegment_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLMuonSegment_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::MuonSegment_v1>","xAOD::MuonSegmentContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::MuonSegment_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::MuonSegment_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::MuonSegment_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLMuonSegment_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::MuonSegment_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLMuonSegment_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLMuonSegment_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","9516C67E-98A5-11E3-BDFD-6C3BE51AB9F1");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::MuonSegment_v1> >*)
   {
      ::DataLink<DataVector<xAOD::MuonSegment_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::MuonSegment_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::MuonSegment_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::MuonSegment_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::MuonSegment_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::MuonSegment_v1> >","DataLink<xAOD::MuonSegmentContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::MuonSegment_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::MuonSegment_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::MuonSegment_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::MuonSegment_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::MuonSegment_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::MuonSegment_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::MuonSegment_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::MuonSegment_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::MuonSegment_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::MuonSegment_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::MuonSegment_v1> >","ElementLink<xAOD::MuonSegmentContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::MuonSegment_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::MuonSegment_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::MuonSegment_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::MuonSegment_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLSlowMuon_v1_Dictionary();
   static void xAODcLcLSlowMuon_v1_TClassManip(TClass*);
   static void *new_xAODcLcLSlowMuon_v1(void *p = 0);
   static void *newArray_xAODcLcLSlowMuon_v1(Long_t size, void *p);
   static void delete_xAODcLcLSlowMuon_v1(void *p);
   static void deleteArray_xAODcLcLSlowMuon_v1(void *p);
   static void destruct_xAODcLcLSlowMuon_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::SlowMuon_v1*)
   {
      ::xAOD::SlowMuon_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::SlowMuon_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::SlowMuon_v1", "xAODMuon/versions/SlowMuon_v1.h", 22,
                  typeid(::xAOD::SlowMuon_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLSlowMuon_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::SlowMuon_v1) );
      instance.SetNew(&new_xAODcLcLSlowMuon_v1);
      instance.SetNewArray(&newArray_xAODcLcLSlowMuon_v1);
      instance.SetDelete(&delete_xAODcLcLSlowMuon_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLSlowMuon_v1);
      instance.SetDestructor(&destruct_xAODcLcLSlowMuon_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::SlowMuon_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::SlowMuon_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::SlowMuon_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLSlowMuon_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::SlowMuon_v1*)0x0)->GetClass();
      xAODcLcLSlowMuon_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLSlowMuon_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLSlowMuonAuxContainer_v1_Dictionary();
   static void xAODcLcLSlowMuonAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLSlowMuonAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLSlowMuonAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLSlowMuonAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLSlowMuonAuxContainer_v1(void *p);
   static void destruct_xAODcLcLSlowMuonAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::SlowMuonAuxContainer_v1*)
   {
      ::xAOD::SlowMuonAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::SlowMuonAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::SlowMuonAuxContainer_v1", "xAODMuon/versions/SlowMuonAuxContainer_v1.h", 25,
                  typeid(::xAOD::SlowMuonAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLSlowMuonAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::SlowMuonAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLSlowMuonAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLSlowMuonAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLSlowMuonAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLSlowMuonAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLSlowMuonAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::SlowMuonAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::SlowMuonAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::SlowMuonAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLSlowMuonAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::SlowMuonAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLSlowMuonAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLSlowMuonAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B0B4F66B-C261-4403-AB96-D71249A9CDEC");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLSlowMuon_v1gR_Dictionary();
   static void DataVectorlExAODcLcLSlowMuon_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLSlowMuon_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLSlowMuon_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLSlowMuon_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLSlowMuon_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLSlowMuon_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::SlowMuon_v1>*)
   {
      ::DataVector<xAOD::SlowMuon_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::SlowMuon_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::SlowMuon_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::SlowMuon_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLSlowMuon_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::SlowMuon_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLSlowMuon_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLSlowMuon_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLSlowMuon_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLSlowMuon_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLSlowMuon_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::SlowMuon_v1>","xAOD::SlowMuonContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::SlowMuon_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::SlowMuon_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::SlowMuon_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLSlowMuon_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::SlowMuon_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLSlowMuon_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLSlowMuon_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","78E011F9-AD91-40A8-95E9-E288A5A583FE");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::SlowMuon_v1> >*)
   {
      ::DataLink<DataVector<xAOD::SlowMuon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::SlowMuon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::SlowMuon_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::SlowMuon_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::SlowMuon_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::SlowMuon_v1> >","DataLink<xAOD::SlowMuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::SlowMuon_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::SlowMuon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::SlowMuon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::SlowMuon_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::SlowMuon_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::SlowMuon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::SlowMuon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::SlowMuon_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::SlowMuon_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::SlowMuon_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::SlowMuon_v1> >","ElementLink<xAOD::SlowMuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::SlowMuon_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::SlowMuon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::SlowMuon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::SlowMuon_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMuonAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonAuxContainer_v2 : new ::xAOD::MuonAuxContainer_v2;
   }
   static void *newArray_xAODcLcLMuonAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonAuxContainer_v2[nElements] : new ::xAOD::MuonAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMuonAuxContainer_v2(void *p) {
      delete ((::xAOD::MuonAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLMuonAuxContainer_v2(void *p) {
      delete [] ((::xAOD::MuonAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLMuonAuxContainer_v2(void *p) {
      typedef ::xAOD::MuonAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MuonAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMuonAuxContainer_v3(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonAuxContainer_v3 : new ::xAOD::MuonAuxContainer_v3;
   }
   static void *newArray_xAODcLcLMuonAuxContainer_v3(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonAuxContainer_v3[nElements] : new ::xAOD::MuonAuxContainer_v3[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMuonAuxContainer_v3(void *p) {
      delete ((::xAOD::MuonAuxContainer_v3*)p);
   }
   static void deleteArray_xAODcLcLMuonAuxContainer_v3(void *p) {
      delete [] ((::xAOD::MuonAuxContainer_v3*)p);
   }
   static void destruct_xAODcLcLMuonAuxContainer_v3(void *p) {
      typedef ::xAOD::MuonAuxContainer_v3 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MuonAuxContainer_v3

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMuon_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Muon_v1 : new ::xAOD::Muon_v1;
   }
   static void *newArray_xAODcLcLMuon_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Muon_v1[nElements] : new ::xAOD::Muon_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMuon_v1(void *p) {
      delete ((::xAOD::Muon_v1*)p);
   }
   static void deleteArray_xAODcLcLMuon_v1(void *p) {
      delete [] ((::xAOD::Muon_v1*)p);
   }
   static void destruct_xAODcLcLMuon_v1(void *p) {
      typedef ::xAOD::Muon_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::Muon_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLMuon_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::Muon_v1> : new ::DataVector<xAOD::Muon_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLMuon_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::Muon_v1>[nElements] : new ::DataVector<xAOD::Muon_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLMuon_v1gR(void *p) {
      delete ((::DataVector<xAOD::Muon_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLMuon_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::Muon_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLMuon_v1gR(void *p) {
      typedef ::DataVector<xAOD::Muon_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::Muon_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMuonAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonAuxContainer_v1 : new ::xAOD::MuonAuxContainer_v1;
   }
   static void *newArray_xAODcLcLMuonAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonAuxContainer_v1[nElements] : new ::xAOD::MuonAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMuonAuxContainer_v1(void *p) {
      delete ((::xAOD::MuonAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLMuonAuxContainer_v1(void *p) {
      delete [] ((::xAOD::MuonAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLMuonAuxContainer_v1(void *p) {
      typedef ::xAOD::MuonAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MuonAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::Muon_v1> > : new ::DataLink<DataVector<xAOD::Muon_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::Muon_v1> >[nElements] : new ::DataLink<DataVector<xAOD::Muon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::Muon_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::Muon_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::Muon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::Muon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::Muon_v1> > : new ::ElementLink<DataVector<xAOD::Muon_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::Muon_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::Muon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::Muon_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::Muon_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::Muon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::Muon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMuonSegment_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonSegment_v1 : new ::xAOD::MuonSegment_v1;
   }
   static void *newArray_xAODcLcLMuonSegment_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonSegment_v1[nElements] : new ::xAOD::MuonSegment_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMuonSegment_v1(void *p) {
      delete ((::xAOD::MuonSegment_v1*)p);
   }
   static void deleteArray_xAODcLcLMuonSegment_v1(void *p) {
      delete [] ((::xAOD::MuonSegment_v1*)p);
   }
   static void destruct_xAODcLcLMuonSegment_v1(void *p) {
      typedef ::xAOD::MuonSegment_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MuonSegment_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMuonSegmentAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonSegmentAuxContainer_v1 : new ::xAOD::MuonSegmentAuxContainer_v1;
   }
   static void *newArray_xAODcLcLMuonSegmentAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonSegmentAuxContainer_v1[nElements] : new ::xAOD::MuonSegmentAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMuonSegmentAuxContainer_v1(void *p) {
      delete ((::xAOD::MuonSegmentAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLMuonSegmentAuxContainer_v1(void *p) {
      delete [] ((::xAOD::MuonSegmentAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLMuonSegmentAuxContainer_v1(void *p) {
      typedef ::xAOD::MuonSegmentAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MuonSegmentAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLMuonSegment_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::MuonSegment_v1> : new ::DataVector<xAOD::MuonSegment_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLMuonSegment_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::MuonSegment_v1>[nElements] : new ::DataVector<xAOD::MuonSegment_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLMuonSegment_v1gR(void *p) {
      delete ((::DataVector<xAOD::MuonSegment_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLMuonSegment_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::MuonSegment_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLMuonSegment_v1gR(void *p) {
      typedef ::DataVector<xAOD::MuonSegment_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::MuonSegment_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::MuonSegment_v1> > : new ::DataLink<DataVector<xAOD::MuonSegment_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::MuonSegment_v1> >[nElements] : new ::DataLink<DataVector<xAOD::MuonSegment_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::MuonSegment_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::MuonSegment_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::MuonSegment_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::MuonSegment_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::MuonSegment_v1> > : new ::ElementLink<DataVector<xAOD::MuonSegment_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::MuonSegment_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::MuonSegment_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::MuonSegment_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::MuonSegment_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::MuonSegment_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::MuonSegment_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLSlowMuon_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::SlowMuon_v1 : new ::xAOD::SlowMuon_v1;
   }
   static void *newArray_xAODcLcLSlowMuon_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::SlowMuon_v1[nElements] : new ::xAOD::SlowMuon_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLSlowMuon_v1(void *p) {
      delete ((::xAOD::SlowMuon_v1*)p);
   }
   static void deleteArray_xAODcLcLSlowMuon_v1(void *p) {
      delete [] ((::xAOD::SlowMuon_v1*)p);
   }
   static void destruct_xAODcLcLSlowMuon_v1(void *p) {
      typedef ::xAOD::SlowMuon_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::SlowMuon_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLSlowMuonAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::SlowMuonAuxContainer_v1 : new ::xAOD::SlowMuonAuxContainer_v1;
   }
   static void *newArray_xAODcLcLSlowMuonAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::SlowMuonAuxContainer_v1[nElements] : new ::xAOD::SlowMuonAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLSlowMuonAuxContainer_v1(void *p) {
      delete ((::xAOD::SlowMuonAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLSlowMuonAuxContainer_v1(void *p) {
      delete [] ((::xAOD::SlowMuonAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLSlowMuonAuxContainer_v1(void *p) {
      typedef ::xAOD::SlowMuonAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::SlowMuonAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLSlowMuon_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::SlowMuon_v1> : new ::DataVector<xAOD::SlowMuon_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLSlowMuon_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::SlowMuon_v1>[nElements] : new ::DataVector<xAOD::SlowMuon_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLSlowMuon_v1gR(void *p) {
      delete ((::DataVector<xAOD::SlowMuon_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLSlowMuon_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::SlowMuon_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLSlowMuon_v1gR(void *p) {
      typedef ::DataVector<xAOD::SlowMuon_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::SlowMuon_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::SlowMuon_v1> > : new ::DataLink<DataVector<xAOD::SlowMuon_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::SlowMuon_v1> >[nElements] : new ::DataLink<DataVector<xAOD::SlowMuon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::SlowMuon_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::SlowMuon_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::SlowMuon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::SlowMuon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::SlowMuon_v1> > : new ::ElementLink<DataVector<xAOD::SlowMuon_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::SlowMuon_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::SlowMuon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::SlowMuon_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::SlowMuon_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::SlowMuon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::SlowMuon_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > : new vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::Muon_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::Muon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::Muon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::Muon_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::Muon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::Muon_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::Muon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Muon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Muon_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Muon_v1> > > : new vector<ElementLink<DataVector<xAOD::Muon_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Muon_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::Muon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::Muon_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::Muon_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::Muon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::Muon_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > : new vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::SlowMuon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::SlowMuon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::SlowMuon_v1> > > : new vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLSlowMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::SlowMuon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::Muon_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::Muon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::Muon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::Muon_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::Muon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::Muon_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::Muon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Muon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Muon_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Muon_v1> > > : new vector<DataLink<DataVector<xAOD::Muon_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Muon_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::Muon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::Muon_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::Muon_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::Muon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::Muon_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::MuonSegment_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::MuonSegment_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::MuonSegment_v1> > > : new vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLMuonSegment_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::MuonSegment_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODMuon_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODMuon/Root",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODMuon/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@AB53C81C-B10C-11E4-AC59-6C3BE51AB9F1)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMuon/versions/MuonAuxContainer_v2.h")))  MuonAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@ECC65005-302B-4662-ACBA-D2CE5A0218B8)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMuon/versions/MuonAuxContainer_v3.h")))  MuonAuxContainer_v3;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODMuon/versions/MuonContainer_v1.h")))  Muon_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODMuon/versions/MuonContainer_v1.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@EC9B677A-B3BA-4C75-87D3-373FC478291E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMuon/versions/MuonAuxContainer_v1.h")))  MuonAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODMuon/versions/MuonContainer_v1.h")))  MuonSegment_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@51739E92-98A5-11E3-B7F4-6C3BE51AB9F1)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMuon/MuonSegmentAuxContainer.h")))  MuonSegmentAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODMuon/SlowMuonContainer.h")))  SlowMuon_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B0B4F66B-C261-4403-AB96-D71249A9CDEC)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMuon/SlowMuonAuxContainer.h")))  SlowMuonAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODMuon"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODMuonDict.h 710608 2015-11-25 15:19:32Z emoyse $
#ifndef XAODMUON_XAODMUONDICT_H
#define XAODMUON_XAODMUONDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
# ifndef EIGEN_DONT_VECTORIZE
#   define EIGEN_DONT_VECTORIZE
# endif
#endif // __GCCXML__
 
// Local include(s):

#include "xAODMuon/versions/MuonContainer_v1.h"
#include "xAODMuon/versions/MuonAuxContainer_v1.h"
#include "xAODMuon/versions/MuonAuxContainer_v2.h"
#include "xAODMuon/versions/MuonAuxContainer_v3.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODMuon/MuonSegmentAuxContainer.h"
#include "xAODMuon/SlowMuonContainer.h"
#include "xAODMuon/SlowMuonAuxContainer.h"

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

#ifndef XAOD_ANALYSIS
#include "TrkSegment/SegmentCollection.h"
#endif // not XAOD_ANALYSIS

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODMUON {
      // Classes in this package
     xAOD::MuonContainer                                                       c1;
      xAOD::MuonSegmentContainer                                               c2;
      xAOD::SlowMuonContainer                                                  c4;
      // Links for Muon
      DataLink< xAOD::MuonContainer >                                          l1;
      ElementLink< xAOD::MuonContainer >                                       l2;
      std::vector< DataLink< xAOD::MuonContainer > >                           l3;
      std::vector< ElementLink< xAOD::MuonContainer > >                        l4;
      std::vector< std::vector< ElementLink< xAOD::MuonContainer > > >         l5;
      // Segments
      DataLink< xAOD::MuonSegmentContainer >                                      l6;
      ElementLink< xAOD::MuonSegmentContainer >                                   l7;
      std::vector< DataLink< xAOD::MuonSegmentContainer > >                       l8;
      std::vector< ElementLink< xAOD::MuonSegmentContainer > >                    l9;
      std::vector< std::vector< ElementLink< xAOD::MuonSegmentContainer > > >    l10;
      // Slow Muons
      DataLink< xAOD::SlowMuonContainer >                                        l11;
      ElementLink< xAOD::SlowMuonContainer >                                     l12;
      std::vector< DataLink< xAOD::SlowMuonContainer > >                         l13;
      std::vector< ElementLink< xAOD::SlowMuonContainer > >                      l14;
      std::vector< std::vector< ElementLink< xAOD::SlowMuonContainer > > >       l15;
      // Instantiations of links used by this package
      ElementLink< xAOD::CaloClusterContainer >                                   i1;
      ElementLink< xAOD::TrackParticleContainer >                                 i2;
#ifndef XAOD_ANALYSIS
      // These lines are still needed in order for Reflex to see the
      // member variable of xAOD::MuonSegmentAuxContainer_v1 correctly.
      Trk::SegmentCollection                                                      c3;
      ElementLink< Trk::SegmentCollection >                                       i3;
      std::vector<ElementLink< Trk::SegmentCollection > >                         i4;
#endif // not XAOD_ANALYSIS
   };
}

#endif // XAODMUON_XAODMUONDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::MuonSegment_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::Muon_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::SlowMuon_v1> >", payloadCode, "@",
"DataLink<xAOD::MuonContainer_v1>", payloadCode, "@",
"DataLink<xAOD::MuonSegmentContainer_v1>", payloadCode, "@",
"DataLink<xAOD::SlowMuonContainer_v1>", payloadCode, "@",
"DataVector<xAOD::MuonSegment_v1>", payloadCode, "@",
"DataVector<xAOD::Muon_v1>", payloadCode, "@",
"DataVector<xAOD::SlowMuon_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::MuonSegment_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::Muon_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::SlowMuon_v1> >", payloadCode, "@",
"ElementLink<xAOD::MuonContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::MuonSegmentContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::SlowMuonContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::MuonSegment_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::Muon_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::SlowMuon_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::MuonContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::MuonSegmentContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::SlowMuonContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::Muon_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::MuonContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::MuonSegmentContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::SlowMuonContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::MuonContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::MuonSegmentContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::SlowMuonContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::MuonSegment_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::Muon_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::SlowMuon_v1> > > >", payloadCode, "@",
"xAOD::MuonAuxContainer_v1", payloadCode, "@",
"xAOD::MuonAuxContainer_v2", payloadCode, "@",
"xAOD::MuonAuxContainer_v3", payloadCode, "@",
"xAOD::MuonContainer_v1", payloadCode, "@",
"xAOD::MuonSegmentAuxContainer_v1", payloadCode, "@",
"xAOD::MuonSegmentContainer_v1", payloadCode, "@",
"xAOD::MuonSegment_v1", payloadCode, "@",
"xAOD::Muon_v1", payloadCode, "@",
"xAOD::SlowMuonAuxContainer_v1", payloadCode, "@",
"xAOD::SlowMuonContainer_v1", payloadCode, "@",
"xAOD::SlowMuon_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODMuon_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODMuon_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODMuon_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODMuon_Reflex() {
  TriggerDictionaryInitialization_xAODMuon_Reflex_Impl();
}

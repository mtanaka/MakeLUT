// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIFourMomUtilsdIobjdIFourMomUtils_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/FourMomUtils/FourMomUtils/FourMomUtilsDict.h"

// Header files passed via #pragma extra_include

namespace {
  void TriggerDictionaryInitialization_FourMomUtils_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/FourMomUtils/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/FourMomUtils",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/FourMomUtils/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "FourMomUtils"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
///////////////////////// -*- C++ -*- /////////////////////////////
// FourMomUtilsDict.h
// Header for dict. 'bootstrapping'
// Author  : Sebastien Binet
///////////////////////////////////////////////////////////////////
#ifndef FOURMOMUTILS_FOURMOMUTILSDICT_H
#define FOURMOMUTILS_FOURMOMUTILSDICT_H

#include "FourMomUtils/xAODP4Helpers.h"
#include "FourMomUtils/xAODHelpers.h"

// AthAnalysisBase/ManaCore doesn't currently include these
#ifndef XAOD_ANALYSIS

#include "FourMomUtils/P4Helpers.h"
#include "FourMomUtils/P4Sorters.h"
#include "FourMomUtils/Thrust.h"
#include "FourMomUtils/FoxWolfram.h"
#include "FourMomUtils/JetBroadening.h"
#include "FourMomUtils/JetMasses.h"

#endif

#endif // FOURMOMUTILS_FOURMOMUTILSDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xAOD::Helpers::decoBool", payloadCode, "@",
"xAOD::Helpers::decoDouble", payloadCode, "@",
"xAOD::Helpers::decoFloat", payloadCode, "@",
"xAOD::Helpers::decoInt", payloadCode, "@",
"xAOD::Helpers::decoUInt", payloadCode, "@",
"xAOD::Helpers::getBool", payloadCode, "@",
"xAOD::Helpers::getDouble", payloadCode, "@",
"xAOD::Helpers::getFloat", payloadCode, "@",
"xAOD::Helpers::getInt", payloadCode, "@",
"xAOD::Helpers::getUInt", payloadCode, "@",
"xAOD::Helpers::setBool", payloadCode, "@",
"xAOD::Helpers::setDouble", payloadCode, "@",
"xAOD::Helpers::setFloat", payloadCode, "@",
"xAOD::Helpers::setInt", payloadCode, "@",
"xAOD::Helpers::setUInt", payloadCode, "@",
"xAOD::P4Helpers::deltaAbsP", payloadCode, "@",
"xAOD::P4Helpers::deltaEta", payloadCode, "@",
"xAOD::P4Helpers::deltaPhi", payloadCode, "@",
"xAOD::P4Helpers::deltaR", payloadCode, "@",
"xAOD::P4Helpers::deltaR2", payloadCode, "@",
"xAOD::P4Helpers::deltaRapidity", payloadCode, "@",
"xAOD::P4Helpers::isInDeltaR", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("FourMomUtils_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_FourMomUtils_Reflex_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_FourMomUtils_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_FourMomUtils_Reflex() {
  TriggerDictionaryInitialization_FourMomUtils_Reflex_Impl();
}

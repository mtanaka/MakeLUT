#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIJetResolutiondIobjdIJetResolutionCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "JetResolution/JERProvider.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_JERProvider(void *p = 0);
   static void *newArray_JERProvider(Long_t size, void *p);
   static void delete_JERProvider(void *p);
   static void deleteArray_JERProvider(void *p);
   static void destruct_JERProvider(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JERProvider*)
   {
      ::JERProvider *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JERProvider >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JERProvider", ::JERProvider::Class_Version(), "JetResolution/JERProvider.h", 50,
                  typeid(::JERProvider), DefineBehavior(ptr, ptr),
                  &::JERProvider::Dictionary, isa_proxy, 4,
                  sizeof(::JERProvider) );
      instance.SetNew(&new_JERProvider);
      instance.SetNewArray(&newArray_JERProvider);
      instance.SetDelete(&delete_JERProvider);
      instance.SetDeleteArray(&deleteArray_JERProvider);
      instance.SetDestructor(&destruct_JERProvider);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JERProvider*)
   {
      return GenerateInitInstanceLocal((::JERProvider*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JERProvider*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr JERProvider::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JERProvider::Class_Name()
{
   return "JERProvider";
}

//______________________________________________________________________________
const char *JERProvider::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JERProvider*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JERProvider::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JERProvider*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JERProvider::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JERProvider*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JERProvider::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JERProvider*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void JERProvider::Streamer(TBuffer &R__b)
{
   // Stream an object of class JERProvider.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JERProvider::Class(),this);
   } else {
      R__b.WriteClassBuffer(JERProvider::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JERProvider(void *p) {
      return  p ? new(p) ::JERProvider : new ::JERProvider;
   }
   static void *newArray_JERProvider(Long_t nElements, void *p) {
      return p ? new(p) ::JERProvider[nElements] : new ::JERProvider[nElements];
   }
   // Wrapper around operator delete
   static void delete_JERProvider(void *p) {
      delete ((::JERProvider*)p);
   }
   static void deleteArray_JERProvider(void *p) {
      delete [] ((::JERProvider*)p);
   }
   static void destruct_JERProvider(void *p) {
      typedef ::JERProvider current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JERProvider

namespace ROOT {
   static TClass *maplEintcOfloatgR_Dictionary();
   static void maplEintcOfloatgR_TClassManip(TClass*);
   static void *new_maplEintcOfloatgR(void *p = 0);
   static void *newArray_maplEintcOfloatgR(Long_t size, void *p);
   static void delete_maplEintcOfloatgR(void *p);
   static void deleteArray_maplEintcOfloatgR(void *p);
   static void destruct_maplEintcOfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,float>*)
   {
      map<int,float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,float>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,float>", -2, "map", 96,
                  typeid(map<int,float>), DefineBehavior(ptr, ptr),
                  &maplEintcOfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,float>) );
      instance.SetNew(&new_maplEintcOfloatgR);
      instance.SetNewArray(&newArray_maplEintcOfloatgR);
      instance.SetDelete(&delete_maplEintcOfloatgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOfloatgR);
      instance.SetDestructor(&destruct_maplEintcOfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,float> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<int,float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,float>*)0x0)->GetClass();
      maplEintcOfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOfloatgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,float> : new map<int,float>;
   }
   static void *newArray_maplEintcOfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,float>[nElements] : new map<int,float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOfloatgR(void *p) {
      delete ((map<int,float>*)p);
   }
   static void deleteArray_maplEintcOfloatgR(void *p) {
      delete [] ((map<int,float>*)p);
   }
   static void destruct_maplEintcOfloatgR(void *p) {
      typedef map<int,float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,float>

namespace ROOT {
   static TClass *maplEintcOTGraphErrorsmUgR_Dictionary();
   static void maplEintcOTGraphErrorsmUgR_TClassManip(TClass*);
   static void *new_maplEintcOTGraphErrorsmUgR(void *p = 0);
   static void *newArray_maplEintcOTGraphErrorsmUgR(Long_t size, void *p);
   static void delete_maplEintcOTGraphErrorsmUgR(void *p);
   static void deleteArray_maplEintcOTGraphErrorsmUgR(void *p);
   static void destruct_maplEintcOTGraphErrorsmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,TGraphErrors*>*)
   {
      map<int,TGraphErrors*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,TGraphErrors*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,TGraphErrors*>", -2, "map", 96,
                  typeid(map<int,TGraphErrors*>), DefineBehavior(ptr, ptr),
                  &maplEintcOTGraphErrorsmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,TGraphErrors*>) );
      instance.SetNew(&new_maplEintcOTGraphErrorsmUgR);
      instance.SetNewArray(&newArray_maplEintcOTGraphErrorsmUgR);
      instance.SetDelete(&delete_maplEintcOTGraphErrorsmUgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOTGraphErrorsmUgR);
      instance.SetDestructor(&destruct_maplEintcOTGraphErrorsmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,TGraphErrors*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<int,TGraphErrors*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOTGraphErrorsmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,TGraphErrors*>*)0x0)->GetClass();
      maplEintcOTGraphErrorsmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOTGraphErrorsmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOTGraphErrorsmUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,TGraphErrors*> : new map<int,TGraphErrors*>;
   }
   static void *newArray_maplEintcOTGraphErrorsmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,TGraphErrors*>[nElements] : new map<int,TGraphErrors*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOTGraphErrorsmUgR(void *p) {
      delete ((map<int,TGraphErrors*>*)p);
   }
   static void deleteArray_maplEintcOTGraphErrorsmUgR(void *p) {
      delete [] ((map<int,TGraphErrors*>*)p);
   }
   static void destruct_maplEintcOTGraphErrorsmUgR(void *p) {
      typedef map<int,TGraphErrors*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,TGraphErrors*>

namespace ROOT {
   static TClass *maplEintcOTGraphmUgR_Dictionary();
   static void maplEintcOTGraphmUgR_TClassManip(TClass*);
   static void *new_maplEintcOTGraphmUgR(void *p = 0);
   static void *newArray_maplEintcOTGraphmUgR(Long_t size, void *p);
   static void delete_maplEintcOTGraphmUgR(void *p);
   static void deleteArray_maplEintcOTGraphmUgR(void *p);
   static void destruct_maplEintcOTGraphmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,TGraph*>*)
   {
      map<int,TGraph*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,TGraph*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,TGraph*>", -2, "map", 96,
                  typeid(map<int,TGraph*>), DefineBehavior(ptr, ptr),
                  &maplEintcOTGraphmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,TGraph*>) );
      instance.SetNew(&new_maplEintcOTGraphmUgR);
      instance.SetNewArray(&newArray_maplEintcOTGraphmUgR);
      instance.SetDelete(&delete_maplEintcOTGraphmUgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOTGraphmUgR);
      instance.SetDestructor(&destruct_maplEintcOTGraphmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,TGraph*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<int,TGraph*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOTGraphmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,TGraph*>*)0x0)->GetClass();
      maplEintcOTGraphmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOTGraphmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOTGraphmUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,TGraph*> : new map<int,TGraph*>;
   }
   static void *newArray_maplEintcOTGraphmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,TGraph*>[nElements] : new map<int,TGraph*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOTGraphmUgR(void *p) {
      delete ((map<int,TGraph*>*)p);
   }
   static void deleteArray_maplEintcOTGraphmUgR(void *p) {
      delete [] ((map<int,TGraph*>*)p);
   }
   static void destruct_maplEintcOTGraphmUgR(void *p) {
      typedef map<int,TGraph*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,TGraph*>

namespace ROOT {
   static TClass *maplEintcOTF1mUgR_Dictionary();
   static void maplEintcOTF1mUgR_TClassManip(TClass*);
   static void *new_maplEintcOTF1mUgR(void *p = 0);
   static void *newArray_maplEintcOTF1mUgR(Long_t size, void *p);
   static void delete_maplEintcOTF1mUgR(void *p);
   static void deleteArray_maplEintcOTF1mUgR(void *p);
   static void destruct_maplEintcOTF1mUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,TF1*>*)
   {
      map<int,TF1*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,TF1*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,TF1*>", -2, "map", 96,
                  typeid(map<int,TF1*>), DefineBehavior(ptr, ptr),
                  &maplEintcOTF1mUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,TF1*>) );
      instance.SetNew(&new_maplEintcOTF1mUgR);
      instance.SetNewArray(&newArray_maplEintcOTF1mUgR);
      instance.SetDelete(&delete_maplEintcOTF1mUgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOTF1mUgR);
      instance.SetDestructor(&destruct_maplEintcOTF1mUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,TF1*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<int,TF1*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOTF1mUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,TF1*>*)0x0)->GetClass();
      maplEintcOTF1mUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOTF1mUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOTF1mUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,TF1*> : new map<int,TF1*>;
   }
   static void *newArray_maplEintcOTF1mUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,TF1*>[nElements] : new map<int,TF1*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOTF1mUgR(void *p) {
      delete ((map<int,TF1*>*)p);
   }
   static void deleteArray_maplEintcOTF1mUgR(void *p) {
      delete [] ((map<int,TF1*>*)p);
   }
   static void destruct_maplEintcOTF1mUgR(void *p) {
      typedef map<int,TF1*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,TF1*>

namespace {
  void TriggerDictionaryInitialization_JetResolutionCINT_Impl() {
    static const char* headers[] = {
"JetResolution/JERProvider.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetResolution/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetResolution",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetResolution/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetResolution/Root/LinkDef.h")))  JERProvider;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "JetResolution"
#endif
#ifndef JETRES_STANDALONE
  #define JETRES_STANDALONE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "JetResolution/JERProvider.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"JERProvider", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("JetResolutionCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_JetResolutionCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_JetResolutionCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_JetResolutionCINT() {
  TriggerDictionaryInitialization_JetResolutionCINT_Impl();
}

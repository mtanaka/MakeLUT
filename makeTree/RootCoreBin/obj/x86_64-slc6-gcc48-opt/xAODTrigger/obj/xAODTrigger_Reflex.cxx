// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTriggerdIobjdIxAODTrigger_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigger/xAODTrigger/xAODTriggerDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLByteStreamAuxContainer_v1_Dictionary();
   static void xAODcLcLByteStreamAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLByteStreamAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLByteStreamAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLByteStreamAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLByteStreamAuxContainer_v1(void *p);
   static void destruct_xAODcLcLByteStreamAuxContainer_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLByteStreamAuxContainer_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::ByteStreamAuxContainer_v1* newObj = (xAOD::ByteStreamAuxContainer_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
        newObj->reset();
     
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::ByteStreamAuxContainer_v1*)
   {
      ::xAOD::ByteStreamAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::ByteStreamAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::ByteStreamAuxContainer_v1", "xAODTrigger/versions/ByteStreamAuxContainer_v1.h", 39,
                  typeid(::xAOD::ByteStreamAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLByteStreamAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::ByteStreamAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLByteStreamAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLByteStreamAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLByteStreamAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLByteStreamAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLByteStreamAuxContainer_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::ByteStreamAuxContainer_v1";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLByteStreamAuxContainer_v1_0);
      rule->fCode        = "\n        newObj->reset();\n     ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::ByteStreamAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::ByteStreamAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::ByteStreamAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLByteStreamAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::ByteStreamAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLByteStreamAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLByteStreamAuxContainer_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMuonRoI_v1_Dictionary();
   static void xAODcLcLMuonRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMuonRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLMuonRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLMuonRoI_v1(void *p);
   static void deleteArray_xAODcLcLMuonRoI_v1(void *p);
   static void destruct_xAODcLcLMuonRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MuonRoI_v1*)
   {
      ::xAOD::MuonRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MuonRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MuonRoI_v1", "xAODTrigger/versions/MuonRoI_v1.h", 28,
                  typeid(::xAOD::MuonRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMuonRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MuonRoI_v1) );
      instance.SetNew(&new_xAODcLcLMuonRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLMuonRoI_v1);
      instance.SetDelete(&delete_xAODcLcLMuonRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMuonRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLMuonRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MuonRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MuonRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MuonRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMuonRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MuonRoI_v1*)0x0)->GetClass();
      xAODcLcLMuonRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMuonRoI_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLMuonRoI_v1gR_Dictionary();
   static void DataVectorlExAODcLcLMuonRoI_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLMuonRoI_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLMuonRoI_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLMuonRoI_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLMuonRoI_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLMuonRoI_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::MuonRoI_v1>*)
   {
      ::DataVector<xAOD::MuonRoI_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::MuonRoI_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::MuonRoI_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::MuonRoI_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLMuonRoI_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::MuonRoI_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLMuonRoI_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLMuonRoI_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLMuonRoI_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLMuonRoI_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLMuonRoI_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::MuonRoI_v1>","xAOD::MuonRoIContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::MuonRoI_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::MuonRoI_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::MuonRoI_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLMuonRoI_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::MuonRoI_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLMuonRoI_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLMuonRoI_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","807F1B34-CFD5-4410-AB98-83231B551D6F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMuonRoIAuxContainer_v1_Dictionary();
   static void xAODcLcLMuonRoIAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMuonRoIAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLMuonRoIAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLMuonRoIAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLMuonRoIAuxContainer_v1(void *p);
   static void destruct_xAODcLcLMuonRoIAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MuonRoIAuxContainer_v1*)
   {
      ::xAOD::MuonRoIAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MuonRoIAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MuonRoIAuxContainer_v1", "xAODTrigger/versions/MuonRoIAuxContainer_v1.h", 29,
                  typeid(::xAOD::MuonRoIAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMuonRoIAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MuonRoIAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLMuonRoIAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLMuonRoIAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLMuonRoIAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMuonRoIAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLMuonRoIAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MuonRoIAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MuonRoIAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MuonRoIAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMuonRoIAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MuonRoIAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLMuonRoIAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMuonRoIAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","242CC7E8-D74C-4208-A3F6-2982628F1F8E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetRoI_v1_Dictionary();
   static void xAODcLcLJetRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJetRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLJetRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLJetRoI_v1(void *p);
   static void deleteArray_xAODcLcLJetRoI_v1(void *p);
   static void destruct_xAODcLcLJetRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetRoI_v1*)
   {
      ::xAOD::JetRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetRoI_v1", "xAODTrigger/versions/JetRoI_v1.h", 30,
                  typeid(::xAOD::JetRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetRoI_v1) );
      instance.SetNew(&new_xAODcLcLJetRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLJetRoI_v1);
      instance.SetDelete(&delete_xAODcLcLJetRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLJetRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetRoI_v1*)0x0)->GetClass();
      xAODcLcLJetRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetRoI_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJetRoI_v1gR_Dictionary();
   static void DataVectorlExAODcLcLJetRoI_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJetRoI_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJetRoI_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJetRoI_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJetRoI_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLJetRoI_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JetRoI_v1>*)
   {
      ::DataVector<xAOD::JetRoI_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JetRoI_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JetRoI_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JetRoI_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJetRoI_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JetRoI_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJetRoI_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJetRoI_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJetRoI_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJetRoI_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJetRoI_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JetRoI_v1>","xAOD::JetRoIContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JetRoI_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JetRoI_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JetRoI_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJetRoI_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JetRoI_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLJetRoI_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJetRoI_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","ED39F230-5E9A-11E3-9563-02163E00A725");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetRoIAuxContainer_v1_Dictionary();
   static void xAODcLcLJetRoIAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJetRoIAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLJetRoIAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLJetRoIAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLJetRoIAuxContainer_v1(void *p);
   static void destruct_xAODcLcLJetRoIAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetRoIAuxContainer_v1*)
   {
      ::xAOD::JetRoIAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetRoIAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetRoIAuxContainer_v1", "xAODTrigger/versions/JetRoIAuxContainer_v1.h", 29,
                  typeid(::xAOD::JetRoIAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetRoIAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetRoIAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLJetRoIAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLJetRoIAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLJetRoIAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetRoIAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLJetRoIAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetRoIAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetRoIAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetRoIAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetRoIAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetRoIAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLJetRoIAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetRoIAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","64C2E2A4-5E9A-11E3-BEF2-02163E00A725");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetRoI_v2_Dictionary();
   static void xAODcLcLJetRoI_v2_TClassManip(TClass*);
   static void *new_xAODcLcLJetRoI_v2(void *p = 0);
   static void *newArray_xAODcLcLJetRoI_v2(Long_t size, void *p);
   static void delete_xAODcLcLJetRoI_v2(void *p);
   static void deleteArray_xAODcLcLJetRoI_v2(void *p);
   static void destruct_xAODcLcLJetRoI_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetRoI_v2*)
   {
      ::xAOD::JetRoI_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetRoI_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetRoI_v2", "xAODTrigger/versions/JetRoI_v2.h", 30,
                  typeid(::xAOD::JetRoI_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetRoI_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetRoI_v2) );
      instance.SetNew(&new_xAODcLcLJetRoI_v2);
      instance.SetNewArray(&newArray_xAODcLcLJetRoI_v2);
      instance.SetDelete(&delete_xAODcLcLJetRoI_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetRoI_v2);
      instance.SetDestructor(&destruct_xAODcLcLJetRoI_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetRoI_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetRoI_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetRoI_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetRoI_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetRoI_v2*)0x0)->GetClass();
      xAODcLcLJetRoI_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetRoI_v2_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJetRoI_v2gR_Dictionary();
   static void DataVectorlExAODcLcLJetRoI_v2gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJetRoI_v2gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJetRoI_v2gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJetRoI_v2gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJetRoI_v2gR(void *p);
   static void destruct_DataVectorlExAODcLcLJetRoI_v2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JetRoI_v2>*)
   {
      ::DataVector<xAOD::JetRoI_v2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JetRoI_v2>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JetRoI_v2>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JetRoI_v2>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJetRoI_v2gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JetRoI_v2>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJetRoI_v2gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJetRoI_v2gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJetRoI_v2gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJetRoI_v2gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJetRoI_v2gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JetRoI_v2>","xAOD::JetRoIContainer_v2");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JetRoI_v2>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JetRoI_v2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JetRoI_v2>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJetRoI_v2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JetRoI_v2>*)0x0)->GetClass();
      DataVectorlExAODcLcLJetRoI_v2gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJetRoI_v2gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","8F9A76C9-2083-4774-A0EE-FB585930B664");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetRoIAuxContainer_v2_Dictionary();
   static void xAODcLcLJetRoIAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLJetRoIAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLJetRoIAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLJetRoIAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLJetRoIAuxContainer_v2(void *p);
   static void destruct_xAODcLcLJetRoIAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetRoIAuxContainer_v2*)
   {
      ::xAOD::JetRoIAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetRoIAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetRoIAuxContainer_v2", "xAODTrigger/versions/JetRoIAuxContainer_v2.h", 29,
                  typeid(::xAOD::JetRoIAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetRoIAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetRoIAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLJetRoIAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLJetRoIAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLJetRoIAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetRoIAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLJetRoIAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetRoIAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetRoIAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetRoIAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetRoIAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetRoIAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLJetRoIAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetRoIAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","8448ECAF-10E0-4E8B-9E67-3A00FEA36317");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEmTauRoI_v1_Dictionary();
   static void xAODcLcLEmTauRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEmTauRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLEmTauRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLEmTauRoI_v1(void *p);
   static void deleteArray_xAODcLcLEmTauRoI_v1(void *p);
   static void destruct_xAODcLcLEmTauRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EmTauRoI_v1*)
   {
      ::xAOD::EmTauRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EmTauRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EmTauRoI_v1", "xAODTrigger/versions/EmTauRoI_v1.h", 30,
                  typeid(::xAOD::EmTauRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEmTauRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EmTauRoI_v1) );
      instance.SetNew(&new_xAODcLcLEmTauRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLEmTauRoI_v1);
      instance.SetDelete(&delete_xAODcLcLEmTauRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEmTauRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLEmTauRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EmTauRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EmTauRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EmTauRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEmTauRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EmTauRoI_v1*)0x0)->GetClass();
      xAODcLcLEmTauRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEmTauRoI_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLEmTauRoI_v1gR_Dictionary();
   static void DataVectorlExAODcLcLEmTauRoI_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLEmTauRoI_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLEmTauRoI_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLEmTauRoI_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLEmTauRoI_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLEmTauRoI_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::EmTauRoI_v1>*)
   {
      ::DataVector<xAOD::EmTauRoI_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::EmTauRoI_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::EmTauRoI_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::EmTauRoI_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLEmTauRoI_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::EmTauRoI_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLEmTauRoI_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLEmTauRoI_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLEmTauRoI_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLEmTauRoI_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLEmTauRoI_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::EmTauRoI_v1>","xAOD::EmTauRoIContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::EmTauRoI_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::EmTauRoI_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::EmTauRoI_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLEmTauRoI_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::EmTauRoI_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLEmTauRoI_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLEmTauRoI_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","101CD1EE-5EA2-11E3-895D-02163E00A743");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEmTauRoIAuxContainer_v1_Dictionary();
   static void xAODcLcLEmTauRoIAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEmTauRoIAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLEmTauRoIAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLEmTauRoIAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLEmTauRoIAuxContainer_v1(void *p);
   static void destruct_xAODcLcLEmTauRoIAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EmTauRoIAuxContainer_v1*)
   {
      ::xAOD::EmTauRoIAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EmTauRoIAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EmTauRoIAuxContainer_v1", "xAODTrigger/versions/EmTauRoIAuxContainer_v1.h", 29,
                  typeid(::xAOD::EmTauRoIAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEmTauRoIAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EmTauRoIAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLEmTauRoIAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLEmTauRoIAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLEmTauRoIAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEmTauRoIAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLEmTauRoIAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EmTauRoIAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EmTauRoIAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EmTauRoIAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEmTauRoIAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EmTauRoIAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLEmTauRoIAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEmTauRoIAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","1825AE7E-5EA2-11E3-A41F-02163E00A743");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEmTauRoI_v2_Dictionary();
   static void xAODcLcLEmTauRoI_v2_TClassManip(TClass*);
   static void *new_xAODcLcLEmTauRoI_v2(void *p = 0);
   static void *newArray_xAODcLcLEmTauRoI_v2(Long_t size, void *p);
   static void delete_xAODcLcLEmTauRoI_v2(void *p);
   static void deleteArray_xAODcLcLEmTauRoI_v2(void *p);
   static void destruct_xAODcLcLEmTauRoI_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EmTauRoI_v2*)
   {
      ::xAOD::EmTauRoI_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EmTauRoI_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EmTauRoI_v2", "xAODTrigger/versions/EmTauRoI_v2.h", 30,
                  typeid(::xAOD::EmTauRoI_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLEmTauRoI_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EmTauRoI_v2) );
      instance.SetNew(&new_xAODcLcLEmTauRoI_v2);
      instance.SetNewArray(&newArray_xAODcLcLEmTauRoI_v2);
      instance.SetDelete(&delete_xAODcLcLEmTauRoI_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEmTauRoI_v2);
      instance.SetDestructor(&destruct_xAODcLcLEmTauRoI_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EmTauRoI_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::EmTauRoI_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EmTauRoI_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEmTauRoI_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EmTauRoI_v2*)0x0)->GetClass();
      xAODcLcLEmTauRoI_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEmTauRoI_v2_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLEmTauRoI_v2gR_Dictionary();
   static void DataVectorlExAODcLcLEmTauRoI_v2gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLEmTauRoI_v2gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLEmTauRoI_v2gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLEmTauRoI_v2gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLEmTauRoI_v2gR(void *p);
   static void destruct_DataVectorlExAODcLcLEmTauRoI_v2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::EmTauRoI_v2>*)
   {
      ::DataVector<xAOD::EmTauRoI_v2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::EmTauRoI_v2>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::EmTauRoI_v2>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::EmTauRoI_v2>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLEmTauRoI_v2gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::EmTauRoI_v2>) );
      instance.SetNew(&new_DataVectorlExAODcLcLEmTauRoI_v2gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLEmTauRoI_v2gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLEmTauRoI_v2gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLEmTauRoI_v2gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLEmTauRoI_v2gR);

      ROOT::AddClassAlternate("DataVector<xAOD::EmTauRoI_v2>","xAOD::EmTauRoIContainer_v2");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::EmTauRoI_v2>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::EmTauRoI_v2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::EmTauRoI_v2>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLEmTauRoI_v2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::EmTauRoI_v2>*)0x0)->GetClass();
      DataVectorlExAODcLcLEmTauRoI_v2gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLEmTauRoI_v2gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","6AB862C4-31E6-4F66-AAE8-56BA01E350F2");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEmTauRoIAuxContainer_v2_Dictionary();
   static void xAODcLcLEmTauRoIAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLEmTauRoIAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLEmTauRoIAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLEmTauRoIAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLEmTauRoIAuxContainer_v2(void *p);
   static void destruct_xAODcLcLEmTauRoIAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EmTauRoIAuxContainer_v2*)
   {
      ::xAOD::EmTauRoIAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EmTauRoIAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EmTauRoIAuxContainer_v2", "xAODTrigger/versions/EmTauRoIAuxContainer_v2.h", 29,
                  typeid(::xAOD::EmTauRoIAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLEmTauRoIAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EmTauRoIAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLEmTauRoIAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLEmTauRoIAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLEmTauRoIAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEmTauRoIAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLEmTauRoIAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EmTauRoIAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::EmTauRoIAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EmTauRoIAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEmTauRoIAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EmTauRoIAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLEmTauRoIAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEmTauRoIAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","74EA3AFB-B7BE-4E4B-AD5D-4297CA6EDBCD");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetEtRoI_v1_Dictionary();
   static void xAODcLcLJetEtRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJetEtRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLJetEtRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLJetEtRoI_v1(void *p);
   static void deleteArray_xAODcLcLJetEtRoI_v1(void *p);
   static void destruct_xAODcLcLJetEtRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetEtRoI_v1*)
   {
      ::xAOD::JetEtRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetEtRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetEtRoI_v1", "xAODTrigger/versions/JetEtRoI_v1.h", 30,
                  typeid(::xAOD::JetEtRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetEtRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetEtRoI_v1) );
      instance.SetNew(&new_xAODcLcLJetEtRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLJetEtRoI_v1);
      instance.SetDelete(&delete_xAODcLcLJetEtRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetEtRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLJetEtRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetEtRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetEtRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetEtRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetEtRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetEtRoI_v1*)0x0)->GetClass();
      xAODcLcLJetEtRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetEtRoI_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","6018AD28-5EAF-11E3-9B5D-02163E00A743");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetEtRoIAuxInfo_v1_Dictionary();
   static void xAODcLcLJetEtRoIAuxInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJetEtRoIAuxInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLJetEtRoIAuxInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLJetEtRoIAuxInfo_v1(void *p);
   static void deleteArray_xAODcLcLJetEtRoIAuxInfo_v1(void *p);
   static void destruct_xAODcLcLJetEtRoIAuxInfo_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetEtRoIAuxInfo_v1*)
   {
      ::xAOD::JetEtRoIAuxInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetEtRoIAuxInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetEtRoIAuxInfo_v1", "xAODTrigger/versions/JetEtRoIAuxInfo_v1.h", 29,
                  typeid(::xAOD::JetEtRoIAuxInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetEtRoIAuxInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetEtRoIAuxInfo_v1) );
      instance.SetNew(&new_xAODcLcLJetEtRoIAuxInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLJetEtRoIAuxInfo_v1);
      instance.SetDelete(&delete_xAODcLcLJetEtRoIAuxInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetEtRoIAuxInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLJetEtRoIAuxInfo_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetEtRoIAuxInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetEtRoIAuxInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetEtRoIAuxInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetEtRoIAuxInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetEtRoIAuxInfo_v1*)0x0)->GetClass();
      xAODcLcLJetEtRoIAuxInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetEtRoIAuxInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","6A7C6F7A-5EAF-11E3-BFBC-02163E00A743");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEnergySumRoI_v1_Dictionary();
   static void xAODcLcLEnergySumRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEnergySumRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLEnergySumRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLEnergySumRoI_v1(void *p);
   static void deleteArray_xAODcLcLEnergySumRoI_v1(void *p);
   static void destruct_xAODcLcLEnergySumRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EnergySumRoI_v1*)
   {
      ::xAOD::EnergySumRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EnergySumRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EnergySumRoI_v1", "xAODTrigger/versions/EnergySumRoI_v1.h", 30,
                  typeid(::xAOD::EnergySumRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEnergySumRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EnergySumRoI_v1) );
      instance.SetNew(&new_xAODcLcLEnergySumRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLEnergySumRoI_v1);
      instance.SetDelete(&delete_xAODcLcLEnergySumRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEnergySumRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLEnergySumRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EnergySumRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EnergySumRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EnergySumRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEnergySumRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EnergySumRoI_v1*)0x0)->GetClass();
      xAODcLcLEnergySumRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEnergySumRoI_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","727CB19A-5EB8-11E3-81B0-02163E00A743");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEnergySumRoIAuxInfo_v1_Dictionary();
   static void xAODcLcLEnergySumRoIAuxInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEnergySumRoIAuxInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLEnergySumRoIAuxInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLEnergySumRoIAuxInfo_v1(void *p);
   static void deleteArray_xAODcLcLEnergySumRoIAuxInfo_v1(void *p);
   static void destruct_xAODcLcLEnergySumRoIAuxInfo_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EnergySumRoIAuxInfo_v1*)
   {
      ::xAOD::EnergySumRoIAuxInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EnergySumRoIAuxInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EnergySumRoIAuxInfo_v1", "xAODTrigger/versions/EnergySumRoIAuxInfo_v1.h", 29,
                  typeid(::xAOD::EnergySumRoIAuxInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEnergySumRoIAuxInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EnergySumRoIAuxInfo_v1) );
      instance.SetNew(&new_xAODcLcLEnergySumRoIAuxInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLEnergySumRoIAuxInfo_v1);
      instance.SetDelete(&delete_xAODcLcLEnergySumRoIAuxInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEnergySumRoIAuxInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLEnergySumRoIAuxInfo_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EnergySumRoIAuxInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EnergySumRoIAuxInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EnergySumRoIAuxInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEnergySumRoIAuxInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EnergySumRoIAuxInfo_v1*)0x0)->GetClass();
      xAODcLcLEnergySumRoIAuxInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEnergySumRoIAuxInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","7BD71A00-5EB8-11E3-95AF-02163E00A743");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTriggerMenu_v1_Dictionary();
   static void xAODcLcLTriggerMenu_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTriggerMenu_v1(void *p = 0);
   static void *newArray_xAODcLcLTriggerMenu_v1(Long_t size, void *p);
   static void delete_xAODcLcLTriggerMenu_v1(void *p);
   static void deleteArray_xAODcLcLTriggerMenu_v1(void *p);
   static void destruct_xAODcLcLTriggerMenu_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TriggerMenu_v1*)
   {
      ::xAOD::TriggerMenu_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TriggerMenu_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TriggerMenu_v1", "xAODTrigger/versions/TriggerMenu_v1.h", 29,
                  typeid(::xAOD::TriggerMenu_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTriggerMenu_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TriggerMenu_v1) );
      instance.SetNew(&new_xAODcLcLTriggerMenu_v1);
      instance.SetNewArray(&newArray_xAODcLcLTriggerMenu_v1);
      instance.SetDelete(&delete_xAODcLcLTriggerMenu_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTriggerMenu_v1);
      instance.SetDestructor(&destruct_xAODcLcLTriggerMenu_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TriggerMenu_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TriggerMenu_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TriggerMenu_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTriggerMenu_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TriggerMenu_v1*)0x0)->GetClass();
      xAODcLcLTriggerMenu_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTriggerMenu_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTriggerMenu_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTriggerMenu_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTriggerMenu_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTriggerMenu_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTriggerMenu_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTriggerMenu_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTriggerMenu_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TriggerMenu_v1>*)
   {
      ::DataVector<xAOD::TriggerMenu_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TriggerMenu_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TriggerMenu_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TriggerMenu_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTriggerMenu_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TriggerMenu_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTriggerMenu_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTriggerMenu_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTriggerMenu_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTriggerMenu_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTriggerMenu_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TriggerMenu_v1>","xAOD::TriggerMenuContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TriggerMenu_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TriggerMenu_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TriggerMenu_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTriggerMenu_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TriggerMenu_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTriggerMenu_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTriggerMenu_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","AA55120B-11CF-44A3-B1E4-A5AB062207B7");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTriggerMenuAuxContainer_v1_Dictionary();
   static void xAODcLcLTriggerMenuAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTriggerMenuAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTriggerMenuAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTriggerMenuAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTriggerMenuAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTriggerMenuAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TriggerMenuAuxContainer_v1*)
   {
      ::xAOD::TriggerMenuAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TriggerMenuAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TriggerMenuAuxContainer_v1", "xAODTrigger/versions/TriggerMenuAuxContainer_v1.h", 29,
                  typeid(::xAOD::TriggerMenuAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTriggerMenuAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TriggerMenuAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTriggerMenuAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTriggerMenuAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTriggerMenuAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTriggerMenuAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTriggerMenuAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TriggerMenuAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TriggerMenuAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TriggerMenuAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTriggerMenuAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TriggerMenuAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTriggerMenuAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTriggerMenuAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B8614CC5-8696-4170-8CCC-496DA7671246");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigConfKeys_v1_Dictionary();
   static void xAODcLcLTrigConfKeys_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigConfKeys_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigConfKeys_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigConfKeys_v1(void *p);
   static void deleteArray_xAODcLcLTrigConfKeys_v1(void *p);
   static void destruct_xAODcLcLTrigConfKeys_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigConfKeys_v1*)
   {
      ::xAOD::TrigConfKeys_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigConfKeys_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigConfKeys_v1", "xAODTrigger/versions/TrigConfKeys_v1.h", 27,
                  typeid(::xAOD::TrigConfKeys_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigConfKeys_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigConfKeys_v1) );
      instance.SetNew(&new_xAODcLcLTrigConfKeys_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigConfKeys_v1);
      instance.SetDelete(&delete_xAODcLcLTrigConfKeys_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigConfKeys_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigConfKeys_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigConfKeys_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigConfKeys_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigConfKeys_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigConfKeys_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigConfKeys_v1*)0x0)->GetClass();
      xAODcLcLTrigConfKeys_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigConfKeys_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","A88FD5D2-8BCB-4ADC-978C-0914D86B96B7");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigDecision_v1_Dictionary();
   static void xAODcLcLTrigDecision_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigDecision_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigDecision_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigDecision_v1(void *p);
   static void deleteArray_xAODcLcLTrigDecision_v1(void *p);
   static void destruct_xAODcLcLTrigDecision_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigDecision_v1*)
   {
      ::xAOD::TrigDecision_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigDecision_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigDecision_v1", "xAODTrigger/versions/TrigDecision_v1.h", 35,
                  typeid(::xAOD::TrigDecision_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigDecision_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigDecision_v1) );
      instance.SetNew(&new_xAODcLcLTrigDecision_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigDecision_v1);
      instance.SetDelete(&delete_xAODcLcLTrigDecision_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigDecision_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigDecision_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigDecision_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigDecision_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigDecision_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigDecision_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigDecision_v1*)0x0)->GetClass();
      xAODcLcLTrigDecision_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigDecision_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","092BCB2D-9630-4689-BE86-7B93466420DA");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigDecisionAuxInfo_v1_Dictionary();
   static void xAODcLcLTrigDecisionAuxInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigDecisionAuxInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigDecisionAuxInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigDecisionAuxInfo_v1(void *p);
   static void deleteArray_xAODcLcLTrigDecisionAuxInfo_v1(void *p);
   static void destruct_xAODcLcLTrigDecisionAuxInfo_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigDecisionAuxInfo_v1*)
   {
      ::xAOD::TrigDecisionAuxInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigDecisionAuxInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigDecisionAuxInfo_v1", "xAODTrigger/versions/TrigDecisionAuxInfo_v1.h", 27,
                  typeid(::xAOD::TrigDecisionAuxInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigDecisionAuxInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigDecisionAuxInfo_v1) );
      instance.SetNew(&new_xAODcLcLTrigDecisionAuxInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigDecisionAuxInfo_v1);
      instance.SetDelete(&delete_xAODcLcLTrigDecisionAuxInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigDecisionAuxInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigDecisionAuxInfo_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigDecisionAuxInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigDecisionAuxInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigDecisionAuxInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigDecisionAuxInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigDecisionAuxInfo_v1*)0x0)->GetClass();
      xAODcLcLTrigDecisionAuxInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigDecisionAuxInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F9618829-3B9C-41CB-8A87-D26B5D31CA79");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigNavigation_v1_Dictionary();
   static void xAODcLcLTrigNavigation_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigNavigation_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigNavigation_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigNavigation_v1(void *p);
   static void deleteArray_xAODcLcLTrigNavigation_v1(void *p);
   static void destruct_xAODcLcLTrigNavigation_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigNavigation_v1*)
   {
      ::xAOD::TrigNavigation_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigNavigation_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigNavigation_v1", "xAODTrigger/versions/TrigNavigation_v1.h", 29,
                  typeid(::xAOD::TrigNavigation_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigNavigation_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigNavigation_v1) );
      instance.SetNew(&new_xAODcLcLTrigNavigation_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigNavigation_v1);
      instance.SetDelete(&delete_xAODcLcLTrigNavigation_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigNavigation_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigNavigation_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigNavigation_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigNavigation_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigNavigation_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigNavigation_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigNavigation_v1*)0x0)->GetClass();
      xAODcLcLTrigNavigation_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigNavigation_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C9131CE0-C4D5-47A2-B088-D49ECE2C3C69");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigNavigationAuxInfo_v1_Dictionary();
   static void xAODcLcLTrigNavigationAuxInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigNavigationAuxInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigNavigationAuxInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigNavigationAuxInfo_v1(void *p);
   static void deleteArray_xAODcLcLTrigNavigationAuxInfo_v1(void *p);
   static void destruct_xAODcLcLTrigNavigationAuxInfo_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigNavigationAuxInfo_v1*)
   {
      ::xAOD::TrigNavigationAuxInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigNavigationAuxInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigNavigationAuxInfo_v1", "xAODTrigger/versions/TrigNavigationAuxInfo_v1.h", 27,
                  typeid(::xAOD::TrigNavigationAuxInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigNavigationAuxInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigNavigationAuxInfo_v1) );
      instance.SetNew(&new_xAODcLcLTrigNavigationAuxInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigNavigationAuxInfo_v1);
      instance.SetDelete(&delete_xAODcLcLTrigNavigationAuxInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigNavigationAuxInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigNavigationAuxInfo_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigNavigationAuxInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigNavigationAuxInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigNavigationAuxInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigNavigationAuxInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigNavigationAuxInfo_v1*)0x0)->GetClass();
      xAODcLcLTrigNavigationAuxInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigNavigationAuxInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","924049A6-25B4-4406-A70A-CCAC2E4233E1");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigComposite_v1_Dictionary();
   static void xAODcLcLTrigComposite_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigComposite_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigComposite_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigComposite_v1(void *p);
   static void deleteArray_xAODcLcLTrigComposite_v1(void *p);
   static void destruct_xAODcLcLTrigComposite_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigComposite_v1*)
   {
      ::xAOD::TrigComposite_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigComposite_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigComposite_v1", "xAODTrigger/versions/TrigComposite_v1.h", 31,
                  typeid(::xAOD::TrigComposite_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigComposite_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigComposite_v1) );
      instance.SetNew(&new_xAODcLcLTrigComposite_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigComposite_v1);
      instance.SetDelete(&delete_xAODcLcLTrigComposite_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigComposite_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigComposite_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigComposite_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigComposite_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigComposite_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigComposite_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigComposite_v1*)0x0)->GetClass();
      xAODcLcLTrigComposite_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigComposite_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigComposite_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigComposite_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigComposite_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigComposite_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigComposite_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigComposite_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigComposite_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigComposite_v1>*)
   {
      ::DataVector<xAOD::TrigComposite_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigComposite_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigComposite_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigComposite_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigComposite_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigComposite_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigComposite_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigComposite_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigComposite_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigComposite_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigComposite_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigComposite_v1>","xAOD::TrigCompositeContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigComposite_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigComposite_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigComposite_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigComposite_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigComposite_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigComposite_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigComposite_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","3CB1DCCD-2B78-4E15-AC09-D75B228A29AA");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigCompositeAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigCompositeAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigCompositeAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigCompositeAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigCompositeAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigCompositeAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigCompositeAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigCompositeAuxContainer_v1*)
   {
      ::xAOD::TrigCompositeAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigCompositeAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigCompositeAuxContainer_v1", "xAODTrigger/versions/TrigCompositeAuxContainer_v1.h", 30,
                  typeid(::xAOD::TrigCompositeAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigCompositeAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigCompositeAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigCompositeAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigCompositeAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigCompositeAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigCompositeAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigCompositeAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigCompositeAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigCompositeAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigCompositeAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigCompositeAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigCompositeAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigCompositeAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigCompositeAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","171EB8B8-A777-47D9-94A9-33B2482E2AAF");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLBunchConf_v1_Dictionary();
   static void xAODcLcLBunchConf_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBunchConf_v1(void *p = 0);
   static void *newArray_xAODcLcLBunchConf_v1(Long_t size, void *p);
   static void delete_xAODcLcLBunchConf_v1(void *p);
   static void deleteArray_xAODcLcLBunchConf_v1(void *p);
   static void destruct_xAODcLcLBunchConf_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BunchConf_v1*)
   {
      ::xAOD::BunchConf_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BunchConf_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BunchConf_v1", "xAODTrigger/versions/BunchConf_v1.h", 28,
                  typeid(::xAOD::BunchConf_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBunchConf_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BunchConf_v1) );
      instance.SetNew(&new_xAODcLcLBunchConf_v1);
      instance.SetNewArray(&newArray_xAODcLcLBunchConf_v1);
      instance.SetDelete(&delete_xAODcLcLBunchConf_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBunchConf_v1);
      instance.SetDestructor(&destruct_xAODcLcLBunchConf_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BunchConf_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BunchConf_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BunchConf_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBunchConf_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BunchConf_v1*)0x0)->GetClass();
      xAODcLcLBunchConf_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBunchConf_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLBunchConf_v1gR_Dictionary();
   static void DataVectorlExAODcLcLBunchConf_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLBunchConf_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLBunchConf_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLBunchConf_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLBunchConf_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLBunchConf_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::BunchConf_v1>*)
   {
      ::DataVector<xAOD::BunchConf_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::BunchConf_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::BunchConf_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::BunchConf_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLBunchConf_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::BunchConf_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLBunchConf_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLBunchConf_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLBunchConf_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLBunchConf_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLBunchConf_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::BunchConf_v1>","xAOD::BunchConfContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::BunchConf_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::BunchConf_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::BunchConf_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLBunchConf_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::BunchConf_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLBunchConf_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLBunchConf_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","07B1BB98-24EE-4EA3-B0D5-ECB2B0A3CB65");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLBunchConfAuxContainer_v1_Dictionary();
   static void xAODcLcLBunchConfAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBunchConfAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLBunchConfAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLBunchConfAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLBunchConfAuxContainer_v1(void *p);
   static void destruct_xAODcLcLBunchConfAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BunchConfAuxContainer_v1*)
   {
      ::xAOD::BunchConfAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BunchConfAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BunchConfAuxContainer_v1", "xAODTrigger/versions/BunchConfAuxContainer_v1.h", 28,
                  typeid(::xAOD::BunchConfAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBunchConfAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BunchConfAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLBunchConfAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLBunchConfAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLBunchConfAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBunchConfAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLBunchConfAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BunchConfAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BunchConfAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BunchConfAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBunchConfAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BunchConfAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLBunchConfAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBunchConfAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","29CFA84E-5C7C-4AAD-8FB5-F5EB9AF93423");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLBunchConfKey_v1_Dictionary();
   static void xAODcLcLBunchConfKey_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBunchConfKey_v1(void *p = 0);
   static void *newArray_xAODcLcLBunchConfKey_v1(Long_t size, void *p);
   static void delete_xAODcLcLBunchConfKey_v1(void *p);
   static void deleteArray_xAODcLcLBunchConfKey_v1(void *p);
   static void destruct_xAODcLcLBunchConfKey_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BunchConfKey_v1*)
   {
      ::xAOD::BunchConfKey_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BunchConfKey_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BunchConfKey_v1", "xAODTrigger/versions/BunchConfKey_v1.h", 24,
                  typeid(::xAOD::BunchConfKey_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBunchConfKey_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BunchConfKey_v1) );
      instance.SetNew(&new_xAODcLcLBunchConfKey_v1);
      instance.SetNewArray(&newArray_xAODcLcLBunchConfKey_v1);
      instance.SetDelete(&delete_xAODcLcLBunchConfKey_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBunchConfKey_v1);
      instance.SetDestructor(&destruct_xAODcLcLBunchConfKey_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BunchConfKey_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BunchConfKey_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BunchConfKey_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBunchConfKey_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BunchConfKey_v1*)0x0)->GetClass();
      xAODcLcLBunchConfKey_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBunchConfKey_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B7FA1A14-95A4-4852-813E-CB2271906060");
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLByteStreamAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ByteStreamAuxContainer_v1 : new ::xAOD::ByteStreamAuxContainer_v1;
   }
   static void *newArray_xAODcLcLByteStreamAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ByteStreamAuxContainer_v1[nElements] : new ::xAOD::ByteStreamAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLByteStreamAuxContainer_v1(void *p) {
      delete ((::xAOD::ByteStreamAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLByteStreamAuxContainer_v1(void *p) {
      delete [] ((::xAOD::ByteStreamAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLByteStreamAuxContainer_v1(void *p) {
      typedef ::xAOD::ByteStreamAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::ByteStreamAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMuonRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonRoI_v1 : new ::xAOD::MuonRoI_v1;
   }
   static void *newArray_xAODcLcLMuonRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonRoI_v1[nElements] : new ::xAOD::MuonRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMuonRoI_v1(void *p) {
      delete ((::xAOD::MuonRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLMuonRoI_v1(void *p) {
      delete [] ((::xAOD::MuonRoI_v1*)p);
   }
   static void destruct_xAODcLcLMuonRoI_v1(void *p) {
      typedef ::xAOD::MuonRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MuonRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLMuonRoI_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::MuonRoI_v1> : new ::DataVector<xAOD::MuonRoI_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLMuonRoI_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::MuonRoI_v1>[nElements] : new ::DataVector<xAOD::MuonRoI_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLMuonRoI_v1gR(void *p) {
      delete ((::DataVector<xAOD::MuonRoI_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLMuonRoI_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::MuonRoI_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLMuonRoI_v1gR(void *p) {
      typedef ::DataVector<xAOD::MuonRoI_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::MuonRoI_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMuonRoIAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonRoIAuxContainer_v1 : new ::xAOD::MuonRoIAuxContainer_v1;
   }
   static void *newArray_xAODcLcLMuonRoIAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MuonRoIAuxContainer_v1[nElements] : new ::xAOD::MuonRoIAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMuonRoIAuxContainer_v1(void *p) {
      delete ((::xAOD::MuonRoIAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLMuonRoIAuxContainer_v1(void *p) {
      delete [] ((::xAOD::MuonRoIAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLMuonRoIAuxContainer_v1(void *p) {
      typedef ::xAOD::MuonRoIAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MuonRoIAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetRoI_v1 : new ::xAOD::JetRoI_v1;
   }
   static void *newArray_xAODcLcLJetRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetRoI_v1[nElements] : new ::xAOD::JetRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetRoI_v1(void *p) {
      delete ((::xAOD::JetRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLJetRoI_v1(void *p) {
      delete [] ((::xAOD::JetRoI_v1*)p);
   }
   static void destruct_xAODcLcLJetRoI_v1(void *p) {
      typedef ::xAOD::JetRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJetRoI_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JetRoI_v1> : new ::DataVector<xAOD::JetRoI_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLJetRoI_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JetRoI_v1>[nElements] : new ::DataVector<xAOD::JetRoI_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJetRoI_v1gR(void *p) {
      delete ((::DataVector<xAOD::JetRoI_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJetRoI_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::JetRoI_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJetRoI_v1gR(void *p) {
      typedef ::DataVector<xAOD::JetRoI_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JetRoI_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetRoIAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetRoIAuxContainer_v1 : new ::xAOD::JetRoIAuxContainer_v1;
   }
   static void *newArray_xAODcLcLJetRoIAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetRoIAuxContainer_v1[nElements] : new ::xAOD::JetRoIAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetRoIAuxContainer_v1(void *p) {
      delete ((::xAOD::JetRoIAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLJetRoIAuxContainer_v1(void *p) {
      delete [] ((::xAOD::JetRoIAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLJetRoIAuxContainer_v1(void *p) {
      typedef ::xAOD::JetRoIAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetRoIAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetRoI_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetRoI_v2 : new ::xAOD::JetRoI_v2;
   }
   static void *newArray_xAODcLcLJetRoI_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetRoI_v2[nElements] : new ::xAOD::JetRoI_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetRoI_v2(void *p) {
      delete ((::xAOD::JetRoI_v2*)p);
   }
   static void deleteArray_xAODcLcLJetRoI_v2(void *p) {
      delete [] ((::xAOD::JetRoI_v2*)p);
   }
   static void destruct_xAODcLcLJetRoI_v2(void *p) {
      typedef ::xAOD::JetRoI_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetRoI_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJetRoI_v2gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JetRoI_v2> : new ::DataVector<xAOD::JetRoI_v2>;
   }
   static void *newArray_DataVectorlExAODcLcLJetRoI_v2gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JetRoI_v2>[nElements] : new ::DataVector<xAOD::JetRoI_v2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJetRoI_v2gR(void *p) {
      delete ((::DataVector<xAOD::JetRoI_v2>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJetRoI_v2gR(void *p) {
      delete [] ((::DataVector<xAOD::JetRoI_v2>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJetRoI_v2gR(void *p) {
      typedef ::DataVector<xAOD::JetRoI_v2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JetRoI_v2>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetRoIAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetRoIAuxContainer_v2 : new ::xAOD::JetRoIAuxContainer_v2;
   }
   static void *newArray_xAODcLcLJetRoIAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetRoIAuxContainer_v2[nElements] : new ::xAOD::JetRoIAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetRoIAuxContainer_v2(void *p) {
      delete ((::xAOD::JetRoIAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLJetRoIAuxContainer_v2(void *p) {
      delete [] ((::xAOD::JetRoIAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLJetRoIAuxContainer_v2(void *p) {
      typedef ::xAOD::JetRoIAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetRoIAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEmTauRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EmTauRoI_v1 : new ::xAOD::EmTauRoI_v1;
   }
   static void *newArray_xAODcLcLEmTauRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EmTauRoI_v1[nElements] : new ::xAOD::EmTauRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEmTauRoI_v1(void *p) {
      delete ((::xAOD::EmTauRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLEmTauRoI_v1(void *p) {
      delete [] ((::xAOD::EmTauRoI_v1*)p);
   }
   static void destruct_xAODcLcLEmTauRoI_v1(void *p) {
      typedef ::xAOD::EmTauRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EmTauRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLEmTauRoI_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::EmTauRoI_v1> : new ::DataVector<xAOD::EmTauRoI_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLEmTauRoI_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::EmTauRoI_v1>[nElements] : new ::DataVector<xAOD::EmTauRoI_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLEmTauRoI_v1gR(void *p) {
      delete ((::DataVector<xAOD::EmTauRoI_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLEmTauRoI_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::EmTauRoI_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLEmTauRoI_v1gR(void *p) {
      typedef ::DataVector<xAOD::EmTauRoI_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::EmTauRoI_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEmTauRoIAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EmTauRoIAuxContainer_v1 : new ::xAOD::EmTauRoIAuxContainer_v1;
   }
   static void *newArray_xAODcLcLEmTauRoIAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EmTauRoIAuxContainer_v1[nElements] : new ::xAOD::EmTauRoIAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEmTauRoIAuxContainer_v1(void *p) {
      delete ((::xAOD::EmTauRoIAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLEmTauRoIAuxContainer_v1(void *p) {
      delete [] ((::xAOD::EmTauRoIAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLEmTauRoIAuxContainer_v1(void *p) {
      typedef ::xAOD::EmTauRoIAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EmTauRoIAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEmTauRoI_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EmTauRoI_v2 : new ::xAOD::EmTauRoI_v2;
   }
   static void *newArray_xAODcLcLEmTauRoI_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EmTauRoI_v2[nElements] : new ::xAOD::EmTauRoI_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEmTauRoI_v2(void *p) {
      delete ((::xAOD::EmTauRoI_v2*)p);
   }
   static void deleteArray_xAODcLcLEmTauRoI_v2(void *p) {
      delete [] ((::xAOD::EmTauRoI_v2*)p);
   }
   static void destruct_xAODcLcLEmTauRoI_v2(void *p) {
      typedef ::xAOD::EmTauRoI_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EmTauRoI_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLEmTauRoI_v2gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::EmTauRoI_v2> : new ::DataVector<xAOD::EmTauRoI_v2>;
   }
   static void *newArray_DataVectorlExAODcLcLEmTauRoI_v2gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::EmTauRoI_v2>[nElements] : new ::DataVector<xAOD::EmTauRoI_v2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLEmTauRoI_v2gR(void *p) {
      delete ((::DataVector<xAOD::EmTauRoI_v2>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLEmTauRoI_v2gR(void *p) {
      delete [] ((::DataVector<xAOD::EmTauRoI_v2>*)p);
   }
   static void destruct_DataVectorlExAODcLcLEmTauRoI_v2gR(void *p) {
      typedef ::DataVector<xAOD::EmTauRoI_v2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::EmTauRoI_v2>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEmTauRoIAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EmTauRoIAuxContainer_v2 : new ::xAOD::EmTauRoIAuxContainer_v2;
   }
   static void *newArray_xAODcLcLEmTauRoIAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EmTauRoIAuxContainer_v2[nElements] : new ::xAOD::EmTauRoIAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEmTauRoIAuxContainer_v2(void *p) {
      delete ((::xAOD::EmTauRoIAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLEmTauRoIAuxContainer_v2(void *p) {
      delete [] ((::xAOD::EmTauRoIAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLEmTauRoIAuxContainer_v2(void *p) {
      typedef ::xAOD::EmTauRoIAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EmTauRoIAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetEtRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetEtRoI_v1 : new ::xAOD::JetEtRoI_v1;
   }
   static void *newArray_xAODcLcLJetEtRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetEtRoI_v1[nElements] : new ::xAOD::JetEtRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetEtRoI_v1(void *p) {
      delete ((::xAOD::JetEtRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLJetEtRoI_v1(void *p) {
      delete [] ((::xAOD::JetEtRoI_v1*)p);
   }
   static void destruct_xAODcLcLJetEtRoI_v1(void *p) {
      typedef ::xAOD::JetEtRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetEtRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetEtRoIAuxInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetEtRoIAuxInfo_v1 : new ::xAOD::JetEtRoIAuxInfo_v1;
   }
   static void *newArray_xAODcLcLJetEtRoIAuxInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetEtRoIAuxInfo_v1[nElements] : new ::xAOD::JetEtRoIAuxInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetEtRoIAuxInfo_v1(void *p) {
      delete ((::xAOD::JetEtRoIAuxInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLJetEtRoIAuxInfo_v1(void *p) {
      delete [] ((::xAOD::JetEtRoIAuxInfo_v1*)p);
   }
   static void destruct_xAODcLcLJetEtRoIAuxInfo_v1(void *p) {
      typedef ::xAOD::JetEtRoIAuxInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetEtRoIAuxInfo_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEnergySumRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EnergySumRoI_v1 : new ::xAOD::EnergySumRoI_v1;
   }
   static void *newArray_xAODcLcLEnergySumRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EnergySumRoI_v1[nElements] : new ::xAOD::EnergySumRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEnergySumRoI_v1(void *p) {
      delete ((::xAOD::EnergySumRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLEnergySumRoI_v1(void *p) {
      delete [] ((::xAOD::EnergySumRoI_v1*)p);
   }
   static void destruct_xAODcLcLEnergySumRoI_v1(void *p) {
      typedef ::xAOD::EnergySumRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EnergySumRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEnergySumRoIAuxInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EnergySumRoIAuxInfo_v1 : new ::xAOD::EnergySumRoIAuxInfo_v1;
   }
   static void *newArray_xAODcLcLEnergySumRoIAuxInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EnergySumRoIAuxInfo_v1[nElements] : new ::xAOD::EnergySumRoIAuxInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEnergySumRoIAuxInfo_v1(void *p) {
      delete ((::xAOD::EnergySumRoIAuxInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLEnergySumRoIAuxInfo_v1(void *p) {
      delete [] ((::xAOD::EnergySumRoIAuxInfo_v1*)p);
   }
   static void destruct_xAODcLcLEnergySumRoIAuxInfo_v1(void *p) {
      typedef ::xAOD::EnergySumRoIAuxInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EnergySumRoIAuxInfo_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTriggerMenu_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerMenu_v1 : new ::xAOD::TriggerMenu_v1;
   }
   static void *newArray_xAODcLcLTriggerMenu_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerMenu_v1[nElements] : new ::xAOD::TriggerMenu_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTriggerMenu_v1(void *p) {
      delete ((::xAOD::TriggerMenu_v1*)p);
   }
   static void deleteArray_xAODcLcLTriggerMenu_v1(void *p) {
      delete [] ((::xAOD::TriggerMenu_v1*)p);
   }
   static void destruct_xAODcLcLTriggerMenu_v1(void *p) {
      typedef ::xAOD::TriggerMenu_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TriggerMenu_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTriggerMenu_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TriggerMenu_v1> : new ::DataVector<xAOD::TriggerMenu_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTriggerMenu_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TriggerMenu_v1>[nElements] : new ::DataVector<xAOD::TriggerMenu_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTriggerMenu_v1gR(void *p) {
      delete ((::DataVector<xAOD::TriggerMenu_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTriggerMenu_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TriggerMenu_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTriggerMenu_v1gR(void *p) {
      typedef ::DataVector<xAOD::TriggerMenu_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TriggerMenu_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTriggerMenuAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerMenuAuxContainer_v1 : new ::xAOD::TriggerMenuAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTriggerMenuAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerMenuAuxContainer_v1[nElements] : new ::xAOD::TriggerMenuAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTriggerMenuAuxContainer_v1(void *p) {
      delete ((::xAOD::TriggerMenuAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTriggerMenuAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TriggerMenuAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTriggerMenuAuxContainer_v1(void *p) {
      typedef ::xAOD::TriggerMenuAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TriggerMenuAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigConfKeys_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigConfKeys_v1 : new ::xAOD::TrigConfKeys_v1;
   }
   static void *newArray_xAODcLcLTrigConfKeys_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigConfKeys_v1[nElements] : new ::xAOD::TrigConfKeys_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigConfKeys_v1(void *p) {
      delete ((::xAOD::TrigConfKeys_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigConfKeys_v1(void *p) {
      delete [] ((::xAOD::TrigConfKeys_v1*)p);
   }
   static void destruct_xAODcLcLTrigConfKeys_v1(void *p) {
      typedef ::xAOD::TrigConfKeys_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigConfKeys_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigDecision_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigDecision_v1 : new ::xAOD::TrigDecision_v1;
   }
   static void *newArray_xAODcLcLTrigDecision_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigDecision_v1[nElements] : new ::xAOD::TrigDecision_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigDecision_v1(void *p) {
      delete ((::xAOD::TrigDecision_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigDecision_v1(void *p) {
      delete [] ((::xAOD::TrigDecision_v1*)p);
   }
   static void destruct_xAODcLcLTrigDecision_v1(void *p) {
      typedef ::xAOD::TrigDecision_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigDecision_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigDecisionAuxInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigDecisionAuxInfo_v1 : new ::xAOD::TrigDecisionAuxInfo_v1;
   }
   static void *newArray_xAODcLcLTrigDecisionAuxInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigDecisionAuxInfo_v1[nElements] : new ::xAOD::TrigDecisionAuxInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigDecisionAuxInfo_v1(void *p) {
      delete ((::xAOD::TrigDecisionAuxInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigDecisionAuxInfo_v1(void *p) {
      delete [] ((::xAOD::TrigDecisionAuxInfo_v1*)p);
   }
   static void destruct_xAODcLcLTrigDecisionAuxInfo_v1(void *p) {
      typedef ::xAOD::TrigDecisionAuxInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigDecisionAuxInfo_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigNavigation_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigNavigation_v1 : new ::xAOD::TrigNavigation_v1;
   }
   static void *newArray_xAODcLcLTrigNavigation_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigNavigation_v1[nElements] : new ::xAOD::TrigNavigation_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigNavigation_v1(void *p) {
      delete ((::xAOD::TrigNavigation_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigNavigation_v1(void *p) {
      delete [] ((::xAOD::TrigNavigation_v1*)p);
   }
   static void destruct_xAODcLcLTrigNavigation_v1(void *p) {
      typedef ::xAOD::TrigNavigation_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigNavigation_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigNavigationAuxInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigNavigationAuxInfo_v1 : new ::xAOD::TrigNavigationAuxInfo_v1;
   }
   static void *newArray_xAODcLcLTrigNavigationAuxInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigNavigationAuxInfo_v1[nElements] : new ::xAOD::TrigNavigationAuxInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigNavigationAuxInfo_v1(void *p) {
      delete ((::xAOD::TrigNavigationAuxInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigNavigationAuxInfo_v1(void *p) {
      delete [] ((::xAOD::TrigNavigationAuxInfo_v1*)p);
   }
   static void destruct_xAODcLcLTrigNavigationAuxInfo_v1(void *p) {
      typedef ::xAOD::TrigNavigationAuxInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigNavigationAuxInfo_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigComposite_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigComposite_v1 : new ::xAOD::TrigComposite_v1;
   }
   static void *newArray_xAODcLcLTrigComposite_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigComposite_v1[nElements] : new ::xAOD::TrigComposite_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigComposite_v1(void *p) {
      delete ((::xAOD::TrigComposite_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigComposite_v1(void *p) {
      delete [] ((::xAOD::TrigComposite_v1*)p);
   }
   static void destruct_xAODcLcLTrigComposite_v1(void *p) {
      typedef ::xAOD::TrigComposite_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigComposite_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigComposite_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigComposite_v1> : new ::DataVector<xAOD::TrigComposite_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigComposite_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigComposite_v1>[nElements] : new ::DataVector<xAOD::TrigComposite_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigComposite_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigComposite_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigComposite_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigComposite_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigComposite_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigComposite_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigComposite_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigCompositeAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigCompositeAuxContainer_v1 : new ::xAOD::TrigCompositeAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigCompositeAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigCompositeAuxContainer_v1[nElements] : new ::xAOD::TrigCompositeAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigCompositeAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigCompositeAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigCompositeAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigCompositeAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigCompositeAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigCompositeAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigCompositeAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBunchConf_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BunchConf_v1 : new ::xAOD::BunchConf_v1;
   }
   static void *newArray_xAODcLcLBunchConf_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BunchConf_v1[nElements] : new ::xAOD::BunchConf_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBunchConf_v1(void *p) {
      delete ((::xAOD::BunchConf_v1*)p);
   }
   static void deleteArray_xAODcLcLBunchConf_v1(void *p) {
      delete [] ((::xAOD::BunchConf_v1*)p);
   }
   static void destruct_xAODcLcLBunchConf_v1(void *p) {
      typedef ::xAOD::BunchConf_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BunchConf_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLBunchConf_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::BunchConf_v1> : new ::DataVector<xAOD::BunchConf_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLBunchConf_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::BunchConf_v1>[nElements] : new ::DataVector<xAOD::BunchConf_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLBunchConf_v1gR(void *p) {
      delete ((::DataVector<xAOD::BunchConf_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLBunchConf_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::BunchConf_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLBunchConf_v1gR(void *p) {
      typedef ::DataVector<xAOD::BunchConf_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::BunchConf_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBunchConfAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BunchConfAuxContainer_v1 : new ::xAOD::BunchConfAuxContainer_v1;
   }
   static void *newArray_xAODcLcLBunchConfAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BunchConfAuxContainer_v1[nElements] : new ::xAOD::BunchConfAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBunchConfAuxContainer_v1(void *p) {
      delete ((::xAOD::BunchConfAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLBunchConfAuxContainer_v1(void *p) {
      delete [] ((::xAOD::BunchConfAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLBunchConfAuxContainer_v1(void *p) {
      typedef ::xAOD::BunchConfAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BunchConfAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBunchConfKey_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BunchConfKey_v1 : new ::xAOD::BunchConfKey_v1;
   }
   static void *newArray_xAODcLcLBunchConfKey_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BunchConfKey_v1[nElements] : new ::xAOD::BunchConfKey_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBunchConfKey_v1(void *p) {
      delete ((::xAOD::BunchConfKey_v1*)p);
   }
   static void deleteArray_xAODcLcLBunchConfKey_v1(void *p) {
      delete [] ((::xAOD::BunchConfKey_v1*)p);
   }
   static void destruct_xAODcLcLBunchConfKey_v1(void *p) {
      typedef ::xAOD::BunchConfKey_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BunchConfKey_v1

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<vector<string> > > >*)
   {
      vector<vector<vector<vector<string> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<vector<string> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<vector<string> > > >", -2, "vector", 210,
                  typeid(vector<vector<vector<vector<string> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<vector<string> > > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<vector<string> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<vector<string> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<vector<string> > > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<vector<string> > > > : new vector<vector<vector<vector<string> > > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<vector<string> > > >[nElements] : new vector<vector<vector<vector<string> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<vector<string> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<vector<string> > > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEvectorlEstringgRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<vector<vector<string> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<vector<string> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<unsigned short> > >*)
   {
      vector<vector<vector<unsigned short> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<unsigned short> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<unsigned short> > >", -2, "vector", 210,
                  typeid(vector<vector<vector<unsigned short> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<unsigned short> > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<unsigned short> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<unsigned short> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<unsigned short> > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<unsigned short> > > : new vector<vector<vector<unsigned short> > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<unsigned short> > >[nElements] : new vector<vector<vector<unsigned short> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<unsigned short> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<unsigned short> > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEunsignedsPshortgRsPgRsPgR(void *p) {
      typedef vector<vector<vector<unsigned short> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<unsigned short> > >

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<unsigned int> > >*)
   {
      vector<vector<vector<unsigned int> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<unsigned int> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<unsigned int> > >", -2, "vector", 210,
                  typeid(vector<vector<vector<unsigned int> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<unsigned int> > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<unsigned int> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<unsigned int> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<unsigned int> > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<unsigned int> > > : new vector<vector<vector<unsigned int> > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<unsigned int> > >[nElements] : new vector<vector<vector<unsigned int> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<unsigned int> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<unsigned int> > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEunsignedsPintgRsPgRsPgR(void *p) {
      typedef vector<vector<vector<unsigned int> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<unsigned int> > >

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEstringgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEstringgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEstringgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEstringgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEstringgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEstringgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEstringgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<string> > >*)
   {
      vector<vector<vector<string> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<string> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<string> > >", -2, "vector", 210,
                  typeid(vector<vector<vector<string> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEstringgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<string> > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEstringgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEstringgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEstringgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEstringgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEstringgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<string> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<string> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEstringgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<string> > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEstringgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEstringgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEstringgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<string> > > : new vector<vector<vector<string> > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEstringgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<string> > >[nElements] : new vector<vector<vector<string> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEstringgRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<string> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEstringgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<string> > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEstringgRsPgRsPgR(void *p) {
      typedef vector<vector<vector<string> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<string> > >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPshortgRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPshortgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPshortgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPshortgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPshortgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPshortgRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPshortgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned short> >*)
   {
      vector<vector<unsigned short> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned short> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned short> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned short> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPshortgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned short> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPshortgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned short> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned short> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPshortgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned short> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPshortgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPshortgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPshortgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned short> > : new vector<vector<unsigned short> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPshortgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned short> >[nElements] : new vector<vector<unsigned short> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPshortgRsPgR(void *p) {
      delete ((vector<vector<unsigned short> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPshortgRsPgR(void *p) {
      delete [] ((vector<vector<unsigned short> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPshortgRsPgR(void *p) {
      typedef vector<vector<unsigned short> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned short> >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPintgRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned int> >*)
   {
      vector<vector<unsigned int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned int> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned int> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned int> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned int> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned int> > : new vector<vector<unsigned int> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned int> >[nElements] : new vector<vector<unsigned int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      delete ((vector<vector<unsigned int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      delete [] ((vector<vector<unsigned int> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      typedef vector<vector<unsigned int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned int> >

namespace ROOT {
   static TClass *vectorlEvectorlEstringgRsPgR_Dictionary();
   static void vectorlEvectorlEstringgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEstringgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEstringgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEstringgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEstringgRsPgR(void *p);
   static void destruct_vectorlEvectorlEstringgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<string> >*)
   {
      vector<vector<string> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<string> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<string> >", -2, "vector", 210,
                  typeid(vector<vector<string> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEstringgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<string> >) );
      instance.SetNew(&new_vectorlEvectorlEstringgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEstringgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEstringgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEstringgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEstringgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<string> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<string> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEstringgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<string> >*)0x0)->GetClass();
      vectorlEvectorlEstringgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEstringgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEstringgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<string> > : new vector<vector<string> >;
   }
   static void *newArray_vectorlEvectorlEstringgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<string> >[nElements] : new vector<vector<string> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEstringgRsPgR(void *p) {
      delete ((vector<vector<string> >*)p);
   }
   static void deleteArray_vectorlEvectorlEstringgRsPgR(void *p) {
      delete [] ((vector<vector<string> >*)p);
   }
   static void destruct_vectorlEvectorlEstringgRsPgR(void *p) {
      typedef vector<vector<string> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<string> >

namespace ROOT {
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary();
   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<int> >*)
   {
      vector<vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<int> >", -2, "vector", 210,
                  typeid(vector<vector<int> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<int> >) );
      instance.SetNew(&new_vectorlEvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<int> >*)0x0)->GetClass();
      vectorlEvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<int> > : new vector<vector<int> >;
   }
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<int> >[nElements] : new vector<vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEintgRsPgR(void *p) {
      delete ((vector<vector<int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p) {
      delete [] ((vector<vector<int> >*)p);
   }
   static void destruct_vectorlEvectorlEintgRsPgR(void *p) {
      typedef vector<vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<int> >

namespace ROOT {
   static TClass *vectorlEvectorlEfloatgRsPgR_Dictionary();
   static void vectorlEvectorlEfloatgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEfloatgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEfloatgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEfloatgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEfloatgRsPgR(void *p);
   static void destruct_vectorlEvectorlEfloatgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<float> >*)
   {
      vector<vector<float> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<float> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<float> >", -2, "vector", 210,
                  typeid(vector<vector<float> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEfloatgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<float> >) );
      instance.SetNew(&new_vectorlEvectorlEfloatgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEfloatgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEfloatgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEfloatgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEfloatgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<float> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<float> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEfloatgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<float> >*)0x0)->GetClass();
      vectorlEvectorlEfloatgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEfloatgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEfloatgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<float> > : new vector<vector<float> >;
   }
   static void *newArray_vectorlEvectorlEfloatgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<float> >[nElements] : new vector<vector<float> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEfloatgRsPgR(void *p) {
      delete ((vector<vector<float> >*)p);
   }
   static void deleteArray_vectorlEvectorlEfloatgRsPgR(void *p) {
      delete [] ((vector<vector<float> >*)p);
   }
   static void destruct_vectorlEvectorlEfloatgRsPgR(void *p) {
      typedef vector<vector<float> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<float> >

namespace ROOT {
   static TClass *vectorlEunsignedsPshortgR_Dictionary();
   static void vectorlEunsignedsPshortgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPshortgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPshortgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPshortgR(void *p);
   static void deleteArray_vectorlEunsignedsPshortgR(void *p);
   static void destruct_vectorlEunsignedsPshortgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned short>*)
   {
      vector<unsigned short> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned short>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned short>", -2, "vector", 210,
                  typeid(vector<unsigned short>), DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPshortgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned short>) );
      instance.SetNew(&new_vectorlEunsignedsPshortgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPshortgR);
      instance.SetDelete(&delete_vectorlEunsignedsPshortgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPshortgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPshortgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned short> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned short>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPshortgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned short>*)0x0)->GetClass();
      vectorlEunsignedsPshortgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPshortgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPshortgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned short> : new vector<unsigned short>;
   }
   static void *newArray_vectorlEunsignedsPshortgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned short>[nElements] : new vector<unsigned short>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPshortgR(void *p) {
      delete ((vector<unsigned short>*)p);
   }
   static void deleteArray_vectorlEunsignedsPshortgR(void *p) {
      delete [] ((vector<unsigned short>*)p);
   }
   static void destruct_vectorlEunsignedsPshortgR(void *p) {
      typedef vector<unsigned short> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned short>

namespace ROOT {
   static TClass *vectorlEunsignedsPintgR_Dictionary();
   static void vectorlEunsignedsPintgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPintgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPintgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPintgR(void *p);
   static void deleteArray_vectorlEunsignedsPintgR(void *p);
   static void destruct_vectorlEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned int>*)
   {
      vector<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned int>", -2, "vector", 210,
                  typeid(vector<unsigned int>), DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned int>) );
      instance.SetNew(&new_vectorlEunsignedsPintgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPintgR);
      instance.SetDelete(&delete_vectorlEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPintgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned int>*)0x0)->GetClass();
      vectorlEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned int> : new vector<unsigned int>;
   }
   static void *newArray_vectorlEunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned int>[nElements] : new vector<unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPintgR(void *p) {
      delete ((vector<unsigned int>*)p);
   }
   static void deleteArray_vectorlEunsignedsPintgR(void *p) {
      delete [] ((vector<unsigned int>*)p);
   }
   static void destruct_vectorlEunsignedsPintgR(void *p) {
      typedef vector<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned int>

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 210,
                  typeid(vector<string>), DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 210,
                  typeid(vector<int>), DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 210,
                  typeid(vector<float>), DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *maplEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary();
   static void maplEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<vector<int> > >*)
   {
      map<string,vector<vector<int> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<vector<int> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<vector<int> > >", -2, "map", 96,
                  typeid(map<string,vector<vector<int> > >), DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,vector<vector<int> > >) );
      instance.SetNew(&new_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<vector<int> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<string,vector<vector<int> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<vector<int> > >*)0x0)->GetClass();
      maplEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<vector<int> > > : new map<string,vector<vector<int> > >;
   }
   static void *newArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<vector<int> > >[nElements] : new map<string,vector<vector<int> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      delete ((map<string,vector<vector<int> > >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      delete [] ((map<string,vector<vector<int> > >*)p);
   }
   static void destruct_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      typedef map<string,vector<vector<int> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<vector<int> > >

namespace ROOT {
   static TClass *maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR_Dictionary();
   static void maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<vector<float> > >*)
   {
      map<string,vector<vector<float> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<vector<float> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<vector<float> > >", -2, "map", 96,
                  typeid(map<string,vector<vector<float> > >), DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,vector<vector<float> > >) );
      instance.SetNew(&new_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<vector<float> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<string,vector<vector<float> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<vector<float> > >*)0x0)->GetClass();
      maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<vector<float> > > : new map<string,vector<vector<float> > >;
   }
   static void *newArray_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<vector<float> > >[nElements] : new map<string,vector<vector<float> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      delete ((map<string,vector<vector<float> > >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      delete [] ((map<string,vector<vector<float> > >*)p);
   }
   static void destruct_maplEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      typedef map<string,vector<vector<float> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<vector<float> > >

namespace ROOT {
   static TClass *maplEstringcOvectorlEintgRsPgR_Dictionary();
   static void maplEstringcOvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEintgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEintgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEintgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<int> >*)
   {
      map<string,vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<int> >", -2, "map", 96,
                  typeid(map<string,vector<int> >), DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,vector<int> >) );
      instance.SetNew(&new_maplEstringcOvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEintgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<string,vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<int> >*)0x0)->GetClass();
      maplEstringcOvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<int> > : new map<string,vector<int> >;
   }
   static void *newArray_maplEstringcOvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<int> >[nElements] : new map<string,vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEintgRsPgR(void *p) {
      delete ((map<string,vector<int> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEintgRsPgR(void *p) {
      delete [] ((map<string,vector<int> >*)p);
   }
   static void destruct_maplEstringcOvectorlEintgRsPgR(void *p) {
      typedef map<string,vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<int> >

namespace ROOT {
   static TClass *maplEstringcOvectorlEfloatgRsPgR_Dictionary();
   static void maplEstringcOvectorlEfloatgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEfloatgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEfloatgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEfloatgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEfloatgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEfloatgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<float> >*)
   {
      map<string,vector<float> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<float> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<float> >", -2, "map", 96,
                  typeid(map<string,vector<float> >), DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEfloatgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,vector<float> >) );
      instance.SetNew(&new_maplEstringcOvectorlEfloatgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEfloatgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEfloatgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEfloatgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEfloatgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<float> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<string,vector<float> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEfloatgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<float> >*)0x0)->GetClass();
      maplEstringcOvectorlEfloatgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEfloatgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEfloatgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<float> > : new map<string,vector<float> >;
   }
   static void *newArray_maplEstringcOvectorlEfloatgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,vector<float> >[nElements] : new map<string,vector<float> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEfloatgRsPgR(void *p) {
      delete ((map<string,vector<float> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEfloatgRsPgR(void *p) {
      delete [] ((map<string,vector<float> >*)p);
   }
   static void destruct_maplEstringcOvectorlEfloatgRsPgR(void *p) {
      typedef map<string,vector<float> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<float> >

namespace {
  void TriggerDictionaryInitialization_xAODTrigger_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigger/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigger/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/ByteStreamAuxContainer_v1.h")))  ByteStreamAuxContainer_v1;}
namespace std{template <class _CharT> struct __attribute__((annotate("$clingAutoload$string")))  char_traits;
}
namespace std{template <typename > class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/MuonRoIContainer_v1.h")))  MuonRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@242CC7E8-D74C-4208-A3F6-2982628F1F8E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/MuonRoIAuxContainer_v1.h")))  MuonRoIAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/JetRoIContainer_v1.h")))  JetRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@64C2E2A4-5E9A-11E3-BEF2-02163E00A725)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/JetRoIAuxContainer_v1.h")))  JetRoIAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/JetRoIContainer_v2.h")))  JetRoI_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@8448ECAF-10E0-4E8B-9E67-3A00FEA36317)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/JetRoIAuxContainer_v2.h")))  JetRoIAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/EmTauRoIContainer_v1.h")))  EmTauRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@1825AE7E-5EA2-11E3-A41F-02163E00A743)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/EmTauRoIAuxContainer_v1.h")))  EmTauRoIAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/EmTauRoIContainer_v2.h")))  EmTauRoI_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@74EA3AFB-B7BE-4E4B-AD5D-4297CA6EDBCD)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/EmTauRoIAuxContainer_v2.h")))  EmTauRoIAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@6018AD28-5EAF-11E3-9B5D-02163E00A743)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/JetEtRoI_v1.h")))  JetEtRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@6A7C6F7A-5EAF-11E3-BFBC-02163E00A743)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/JetEtRoIAuxInfo_v1.h")))  JetEtRoIAuxInfo_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@727CB19A-5EB8-11E3-81B0-02163E00A743)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/EnergySumRoI_v1.h")))  EnergySumRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@7BD71A00-5EB8-11E3-95AF-02163E00A743)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/EnergySumRoIAuxInfo_v1.h")))  EnergySumRoIAuxInfo_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TriggerMenu_v1.h")))  TriggerMenu_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B8614CC5-8696-4170-8CCC-496DA7671246)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TriggerMenuAuxContainer_v1.h")))  TriggerMenuAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@A88FD5D2-8BCB-4ADC-978C-0914D86B96B7)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TrigConfKeys_v1.h")))  TrigConfKeys_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@092BCB2D-9630-4689-BE86-7B93466420DA)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TrigDecision_v1.h")))  TrigDecision_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@F9618829-3B9C-41CB-8A87-D26B5D31CA79)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TrigDecisionAuxInfo_v1.h")))  TrigDecisionAuxInfo_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@C9131CE0-C4D5-47A2-B088-D49ECE2C3C69)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TrigNavigation_v1.h")))  TrigNavigation_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@924049A6-25B4-4406-A70A-CCAC2E4233E1)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TrigNavigationAuxInfo_v1.h")))  TrigNavigationAuxInfo_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TrigComposite_v1.h")))  TrigComposite_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@171EB8B8-A777-47D9-94A9-33B2482E2AAF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/TrigCompositeAuxContainer_v1.h")))  TrigCompositeAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigger/versions/BunchConfContainer_v1.h")))  BunchConf_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@29CFA84E-5C7C-4AAD-8FB5-F5EB9AF93423)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/BunchConfAuxContainer_v1.h")))  BunchConfAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B7FA1A14-95A4-4852-813E-CB2271906060)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigger/versions/BunchConfKey_v1.h")))  BunchConfKey_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigger"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTriggerDict.h 635056 2014-12-10 15:35:39Z watsona $
#ifndef XAODTRIGGER_XAODTRIGGERDICT_H
#define XAODTRIGGER_XAODTRIGGERDICT_H

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
#include "xAODTrigger/versions/ByteStreamAuxContainer_v1.h"

#include "xAODTrigger/versions/TriggerMenu_v1.h"
#include "xAODTrigger/versions/TriggerMenuContainer_v1.h"
#include "xAODTrigger/versions/TriggerMenuAuxContainer_v1.h"

#include "xAODTrigger/versions/MuonRoIContainer_v1.h"
#include "xAODTrigger/versions/MuonRoIAuxContainer_v1.h"
#include "xAODTrigger/versions/MuonRoI_v1.h"

#include "xAODTrigger/versions/JetRoIContainer_v1.h"
#include "xAODTrigger/versions/JetRoIAuxContainer_v1.h"
#include "xAODTrigger/versions/JetRoI_v1.h"

#include "xAODTrigger/versions/JetRoIContainer_v2.h"
#include "xAODTrigger/versions/JetRoIAuxContainer_v2.h"
#include "xAODTrigger/versions/JetRoI_v2.h"

#include "xAODTrigger/versions/EmTauRoIContainer_v1.h"
#include "xAODTrigger/versions/EmTauRoIAuxContainer_v1.h"
#include "xAODTrigger/versions/EmTauRoI_v1.h"

#include "xAODTrigger/versions/EmTauRoIContainer_v2.h"
#include "xAODTrigger/versions/EmTauRoIAuxContainer_v2.h"
#include "xAODTrigger/versions/EmTauRoI_v2.h"

#include "xAODTrigger/versions/JetEtRoIAuxInfo_v1.h"
#include "xAODTrigger/versions/JetEtRoI_v1.h"

#include "xAODTrigger/versions/EnergySumRoIAuxInfo_v1.h"
#include "xAODTrigger/versions/EnergySumRoI_v1.h"

#include "xAODTrigger/versions/TrigDecision_v1.h"
#include "xAODTrigger/versions/TrigDecisionAuxInfo_v1.h"

#include "xAODTrigger/versions/TrigNavigation_v1.h"
#include "xAODTrigger/versions/TrigNavigationAuxInfo_v1.h"

#include "xAODTrigger/versions/TrigConfKeys_v1.h"

#include "xAODTrigger/versions/TrigComposite_v1.h"
#include "xAODTrigger/versions/TrigCompositeContainer_v1.h"
#include "xAODTrigger/versions/TrigCompositeAuxContainer_v1.h"

#include "xAODTrigger/versions/BunchConfKey_v1.h"
#include "xAODTrigger/versions/BunchConfContainer_v1.h"
#include "xAODTrigger/versions/BunchConfAuxContainer_v1.h"

namespace{
  struct GCCXML_DUMMY_INSTANTIATION_XAODTRIGGER {

    xAOD::MuonRoIContainer_v1 muon_c1;
    DataLink< xAOD::MuonRoIContainer_v1 > muon_l1;
    ElementLink< xAOD::MuonRoIContainer_v1 > muon_l2;
    ElementLinkVector< xAOD::MuonRoIContainer_v1 > muon_l3;
    std::vector< DataLink<xAOD::MuonRoIContainer_v1 > > muon_l4;
    std::vector< ElementLink<xAOD::MuonRoIContainer_v1 > > muon_l5;
    std::vector< ElementLinkVector< xAOD::MuonRoIContainer_v1 > > muon_l6;

    xAOD::JetRoIContainer_v1 jet_c1;
    DataLink< xAOD::JetRoIContainer_v1 > jet_l1;
    ElementLink< xAOD::JetRoIContainer_v1 > jet_l2;
    ElementLinkVector< xAOD::JetRoIContainer_v1 > jet_l3;
    std::vector< DataLink<xAOD::JetRoIContainer_v1 > > jet_l4;
    std::vector< ElementLink<xAOD::JetRoIContainer_v1 > > jet_l5;
    std::vector< ElementLinkVector< xAOD::JetRoIContainer_v1 > > jet_l6;

    xAOD::JetRoIContainer_v2 jet_v2_c1;
    DataLink< xAOD::JetRoIContainer_v2 > jet_v2_l1;
    ElementLink< xAOD::JetRoIContainer_v2 > jet_v2_l2;
    ElementLinkVector< xAOD::JetRoIContainer_v2 > jet_v2_l3;
    std::vector< DataLink<xAOD::JetRoIContainer_v2 > > jet_v2_l4;
    std::vector< ElementLink<xAOD::JetRoIContainer_v2 > > jet_v2_l5;
    std::vector< ElementLinkVector< xAOD::JetRoIContainer_v2 > > jet_v2_l6;

    xAOD::EmTauRoIContainer_v1 emtau_c1;
    DataLink< xAOD::EmTauRoIContainer_v1 > emtau_l1;
    ElementLink< xAOD::EmTauRoIContainer_v1 > emtau_l2;
    ElementLinkVector< xAOD::EmTauRoIContainer_v1 > emtau_l3;
    std::vector< DataLink<xAOD::EmTauRoIContainer_v1 > > emtau_l4;
    std::vector< ElementLink<xAOD::EmTauRoIContainer_v1 > > emtau_l5;
    std::vector< ElementLinkVector< xAOD::EmTauRoIContainer_v1 > > emtau_l6;

    xAOD::EmTauRoIContainer_v2 emtau_v2_c1;
    DataLink< xAOD::EmTauRoIContainer_v2 > emtau_v2_l1;
    ElementLink< xAOD::EmTauRoIContainer_v2 > emtau_v2_l2;
    ElementLinkVector< xAOD::EmTauRoIContainer_v2 > emtau_v2_l3;
    std::vector< DataLink<xAOD::EmTauRoIContainer_v2 > > emtau_v2_l4;
    std::vector< ElementLink<xAOD::EmTauRoIContainer_v2 > > emtau_v2_l5;
    std::vector< ElementLinkVector< xAOD::EmTauRoIContainer_v2 > > emtau_v2_l6;

    DataLink< xAOD::JetEtRoI_v1 > jetEt_l1;
    std::vector< DataLink< xAOD::JetEtRoI_v1 > > jetEt_l2;

    DataLink< xAOD::EnergySumRoI_v1 > esum_l1;
    std::vector< DataLink< xAOD::EnergySumRoI_v1 > > esum_l2;

    xAOD::TriggerMenuContainer_v1 c2;

    xAOD::TrigCompositeContainer_v1 comp_c1;

    xAOD::BunchConfContainer_v1 c5;
  };
}

#endif // XAODTRIGGER_XAODTRIGGERDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataVector<xAOD::BunchConf_v1>", payloadCode, "@",
"DataVector<xAOD::EmTauRoI_v1>", payloadCode, "@",
"DataVector<xAOD::EmTauRoI_v2>", payloadCode, "@",
"DataVector<xAOD::JetRoI_v1>", payloadCode, "@",
"DataVector<xAOD::JetRoI_v2>", payloadCode, "@",
"DataVector<xAOD::MuonRoI_v1>", payloadCode, "@",
"DataVector<xAOD::TrigComposite_v1>", payloadCode, "@",
"DataVector<xAOD::TriggerMenu_v1>", payloadCode, "@",
"map<std::string,std::vector<float> >", payloadCode, "@",
"map<std::string,std::vector<int> >", payloadCode, "@",
"map<std::string,std::vector<std::vector<float> > >", payloadCode, "@",
"map<std::string,std::vector<std::vector<int> > >", payloadCode, "@",
"map<string,vector<float> >", payloadCode, "@",
"map<string,vector<int> >", payloadCode, "@",
"map<string,vector<vector<float> > >", payloadCode, "@",
"map<string,vector<vector<int> > >", payloadCode, "@",
"vector<std::vector<std::string> >", payloadCode, "@",
"vector<std::vector<std::vector<std::string> > >", payloadCode, "@",
"vector<std::vector<std::vector<std::vector<std::string> > > >", payloadCode, "@",
"vector<std::vector<std::vector<uint16_t> > >", payloadCode, "@",
"vector<std::vector<std::vector<uint32_t> > >", payloadCode, "@",
"vector<std::vector<uint16_t> >", payloadCode, "@",
"vector<vector<string> >", payloadCode, "@",
"vector<vector<unsigned short> >", payloadCode, "@",
"vector<vector<vector<string> > >", payloadCode, "@",
"vector<vector<vector<unsigned int> > >", payloadCode, "@",
"vector<vector<vector<unsigned short> > >", payloadCode, "@",
"vector<vector<vector<vector<string> > > >", payloadCode, "@",
"xAOD::BunchConfAuxContainer_v1", payloadCode, "@",
"xAOD::BunchConfContainer_v1", payloadCode, "@",
"xAOD::BunchConfKey_v1", payloadCode, "@",
"xAOD::BunchConf_v1", payloadCode, "@",
"xAOD::ByteStreamAuxContainer_v1", payloadCode, "@",
"xAOD::EmTauRoIAuxContainer_v1", payloadCode, "@",
"xAOD::EmTauRoIAuxContainer_v2", payloadCode, "@",
"xAOD::EmTauRoIContainer_v1", payloadCode, "@",
"xAOD::EmTauRoIContainer_v2", payloadCode, "@",
"xAOD::EmTauRoI_v1", payloadCode, "@",
"xAOD::EmTauRoI_v2", payloadCode, "@",
"xAOD::EnergySumRoIAuxInfo_v1", payloadCode, "@",
"xAOD::EnergySumRoI_v1", payloadCode, "@",
"xAOD::JetEtRoIAuxInfo_v1", payloadCode, "@",
"xAOD::JetEtRoI_v1", payloadCode, "@",
"xAOD::JetRoIAuxContainer_v1", payloadCode, "@",
"xAOD::JetRoIAuxContainer_v2", payloadCode, "@",
"xAOD::JetRoIContainer_v1", payloadCode, "@",
"xAOD::JetRoIContainer_v2", payloadCode, "@",
"xAOD::JetRoI_v1", payloadCode, "@",
"xAOD::JetRoI_v2", payloadCode, "@",
"xAOD::MuonRoIAuxContainer_v1", payloadCode, "@",
"xAOD::MuonRoIContainer_v1", payloadCode, "@",
"xAOD::MuonRoI_v1", payloadCode, "@",
"xAOD::TrigCompositeAuxContainer_v1", payloadCode, "@",
"xAOD::TrigCompositeContainer_v1", payloadCode, "@",
"xAOD::TrigComposite_v1", payloadCode, "@",
"xAOD::TrigConfKeys_v1", payloadCode, "@",
"xAOD::TrigDecisionAuxInfo_v1", payloadCode, "@",
"xAOD::TrigDecision_v1", payloadCode, "@",
"xAOD::TrigNavigationAuxInfo_v1", payloadCode, "@",
"xAOD::TrigNavigation_v1", payloadCode, "@",
"xAOD::TriggerMenuAuxContainer_v1", payloadCode, "@",
"xAOD::TriggerMenuContainer_v1", payloadCode, "@",
"xAOD::TriggerMenu_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigger_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigger_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigger_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigger_Reflex() {
  TriggerDictionaryInitialization_xAODTrigger_Reflex_Impl();
}

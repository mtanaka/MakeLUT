#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIMuonMomentumCorrectionsdIobjdIMuonMomentumCorrectionsCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "MuonMomentumCorrections/SmearingClass.h"
#include "MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h"

// Header files passed via #pragma extra_include

namespace MuonSmear {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *MuonSmear_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("MuonSmear", 0 /*version*/, "MuonMomentumCorrections/SmearingClass.h", 29,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &MuonSmear_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *MuonSmear_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace Analysis {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *Analysis_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("Analysis", 0 /*version*/, "MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h", 34,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &Analysis_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *Analysis_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static void *new_MuonSmearcLcLSmearingClass(void *p = 0);
   static void *newArray_MuonSmearcLcLSmearingClass(Long_t size, void *p);
   static void delete_MuonSmearcLcLSmearingClass(void *p);
   static void deleteArray_MuonSmearcLcLSmearingClass(void *p);
   static void destruct_MuonSmearcLcLSmearingClass(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MuonSmear::SmearingClass*)
   {
      ::MuonSmear::SmearingClass *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MuonSmear::SmearingClass >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MuonSmear::SmearingClass", ::MuonSmear::SmearingClass::Class_Version(), "MuonMomentumCorrections/SmearingClass.h", 56,
                  typeid(::MuonSmear::SmearingClass), DefineBehavior(ptr, ptr),
                  &::MuonSmear::SmearingClass::Dictionary, isa_proxy, 4,
                  sizeof(::MuonSmear::SmearingClass) );
      instance.SetNew(&new_MuonSmearcLcLSmearingClass);
      instance.SetNewArray(&newArray_MuonSmearcLcLSmearingClass);
      instance.SetDelete(&delete_MuonSmearcLcLSmearingClass);
      instance.SetDeleteArray(&deleteArray_MuonSmearcLcLSmearingClass);
      instance.SetDestructor(&destruct_MuonSmearcLcLSmearingClass);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MuonSmear::SmearingClass*)
   {
      return GenerateInitInstanceLocal((::MuonSmear::SmearingClass*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MuonSmear::SmearingClass*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *AnalysiscLcLMuonResolutionAndMomentumScaleFactors_Dictionary();
   static void AnalysiscLcLMuonResolutionAndMomentumScaleFactors_TClassManip(TClass*);
   static void delete_AnalysiscLcLMuonResolutionAndMomentumScaleFactors(void *p);
   static void deleteArray_AnalysiscLcLMuonResolutionAndMomentumScaleFactors(void *p);
   static void destruct_AnalysiscLcLMuonResolutionAndMomentumScaleFactors(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Analysis::MuonResolutionAndMomentumScaleFactors*)
   {
      ::Analysis::MuonResolutionAndMomentumScaleFactors *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Analysis::MuonResolutionAndMomentumScaleFactors));
      static ::ROOT::TGenericClassInfo 
         instance("Analysis::MuonResolutionAndMomentumScaleFactors", "MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h", 36,
                  typeid(::Analysis::MuonResolutionAndMomentumScaleFactors), DefineBehavior(ptr, ptr),
                  &AnalysiscLcLMuonResolutionAndMomentumScaleFactors_Dictionary, isa_proxy, 4,
                  sizeof(::Analysis::MuonResolutionAndMomentumScaleFactors) );
      instance.SetDelete(&delete_AnalysiscLcLMuonResolutionAndMomentumScaleFactors);
      instance.SetDeleteArray(&deleteArray_AnalysiscLcLMuonResolutionAndMomentumScaleFactors);
      instance.SetDestructor(&destruct_AnalysiscLcLMuonResolutionAndMomentumScaleFactors);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Analysis::MuonResolutionAndMomentumScaleFactors*)
   {
      return GenerateInitInstanceLocal((::Analysis::MuonResolutionAndMomentumScaleFactors*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Analysis::MuonResolutionAndMomentumScaleFactors*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AnalysiscLcLMuonResolutionAndMomentumScaleFactors_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Analysis::MuonResolutionAndMomentumScaleFactors*)0x0)->GetClass();
      AnalysiscLcLMuonResolutionAndMomentumScaleFactors_TClassManip(theClass);
   return theClass;
   }

   static void AnalysiscLcLMuonResolutionAndMomentumScaleFactors_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace MuonSmear {
//______________________________________________________________________________
atomic_TClass_ptr SmearingClass::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *SmearingClass::Class_Name()
{
   return "MuonSmear::SmearingClass";
}

//______________________________________________________________________________
const char *SmearingClass::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonSmear::SmearingClass*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int SmearingClass::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonSmear::SmearingClass*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *SmearingClass::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonSmear::SmearingClass*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *SmearingClass::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonSmear::SmearingClass*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace MuonSmear
namespace MuonSmear {
//______________________________________________________________________________
void SmearingClass::Streamer(TBuffer &R__b)
{
   // Stream an object of class MuonSmear::SmearingClass.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MuonSmear::SmearingClass::Class(),this);
   } else {
      R__b.WriteClassBuffer(MuonSmear::SmearingClass::Class(),this);
   }
}

} // namespace MuonSmear
namespace ROOT {
   // Wrappers around operator new
   static void *new_MuonSmearcLcLSmearingClass(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::MuonSmear::SmearingClass : new ::MuonSmear::SmearingClass;
   }
   static void *newArray_MuonSmearcLcLSmearingClass(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::MuonSmear::SmearingClass[nElements] : new ::MuonSmear::SmearingClass[nElements];
   }
   // Wrapper around operator delete
   static void delete_MuonSmearcLcLSmearingClass(void *p) {
      delete ((::MuonSmear::SmearingClass*)p);
   }
   static void deleteArray_MuonSmearcLcLSmearingClass(void *p) {
      delete [] ((::MuonSmear::SmearingClass*)p);
   }
   static void destruct_MuonSmearcLcLSmearingClass(void *p) {
      typedef ::MuonSmear::SmearingClass current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MuonSmear::SmearingClass

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AnalysiscLcLMuonResolutionAndMomentumScaleFactors(void *p) {
      delete ((::Analysis::MuonResolutionAndMomentumScaleFactors*)p);
   }
   static void deleteArray_AnalysiscLcLMuonResolutionAndMomentumScaleFactors(void *p) {
      delete [] ((::Analysis::MuonResolutionAndMomentumScaleFactors*)p);
   }
   static void destruct_AnalysiscLcLMuonResolutionAndMomentumScaleFactors(void *p) {
      typedef ::Analysis::MuonResolutionAndMomentumScaleFactors current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Analysis::MuonResolutionAndMomentumScaleFactors

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 210,
                  typeid(vector<double>), DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace {
  void TriggerDictionaryInitialization_MuonMomentumCorrectionsCINT_Impl() {
    static const char* headers[] = {
"MuonMomentumCorrections/SmearingClass.h",
"MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonMomentumCorrections/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonMomentumCorrections/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace MuonSmear{class __attribute__((annotate("$clingAutoload$MuonMomentumCorrections/SmearingClass.h")))  SmearingClass;}
namespace Analysis{class __attribute__((annotate("$clingAutoload$MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h")))  MuonResolutionAndMomentumScaleFactors;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "MuonMomentumCorrections"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "MuonMomentumCorrections/SmearingClass.h"
#include "MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Analysis::MuonResolutionAndMomentumScaleFactors", payloadCode, "@",
"MuonSmear::SmearingClass", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("MuonMomentumCorrectionsCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_MuonMomentumCorrectionsCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_MuonMomentumCorrectionsCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_MuonMomentumCorrectionsCINT() {
  TriggerDictionaryInitialization_MuonMomentumCorrectionsCINT_Impl();
}

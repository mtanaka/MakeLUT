// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTruthdIobjdIxAODTruth_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTruth/xAODTruth/xAODTruthDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTruthParticle_v1_Dictionary();
   static void xAODcLcLTruthParticle_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTruthParticle_v1(void *p = 0);
   static void *newArray_xAODcLcLTruthParticle_v1(Long_t size, void *p);
   static void delete_xAODcLcLTruthParticle_v1(void *p);
   static void deleteArray_xAODcLcLTruthParticle_v1(void *p);
   static void destruct_xAODcLcLTruthParticle_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTruthParticle_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TruthParticle_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::TruthParticle_v1* newObj = (xAOD::TruthParticle_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthParticle_v1*)
   {
      ::xAOD::TruthParticle_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthParticle_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthParticle_v1", "xAODTruth/versions/TruthParticle_v1.h", 31,
                  typeid(::xAOD::TruthParticle_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthParticle_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthParticle_v1) );
      instance.SetNew(&new_xAODcLcLTruthParticle_v1);
      instance.SetNewArray(&newArray_xAODcLcLTruthParticle_v1);
      instance.SetDelete(&delete_xAODcLcLTruthParticle_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthParticle_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthParticle_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TruthParticle_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTruthParticle_v1_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthParticle_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthParticle_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthParticle_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthParticle_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthParticle_v1*)0x0)->GetClass();
      xAODcLcLTruthParticle_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthParticle_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthParticle_v1cLcLPolarization_Dictionary();
   static void xAODcLcLTruthParticle_v1cLcLPolarization_TClassManip(TClass*);
   static void *new_xAODcLcLTruthParticle_v1cLcLPolarization(void *p = 0);
   static void *newArray_xAODcLcLTruthParticle_v1cLcLPolarization(Long_t size, void *p);
   static void delete_xAODcLcLTruthParticle_v1cLcLPolarization(void *p);
   static void deleteArray_xAODcLcLTruthParticle_v1cLcLPolarization(void *p);
   static void destruct_xAODcLcLTruthParticle_v1cLcLPolarization(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthParticle_v1::Polarization*)
   {
      ::xAOD::TruthParticle_v1::Polarization *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthParticle_v1::Polarization));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthParticle_v1::Polarization", "xAODTruth/versions/TruthParticle_v1.h", 336,
                  typeid(::xAOD::TruthParticle_v1::Polarization), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthParticle_v1cLcLPolarization_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthParticle_v1::Polarization) );
      instance.SetNew(&new_xAODcLcLTruthParticle_v1cLcLPolarization);
      instance.SetNewArray(&newArray_xAODcLcLTruthParticle_v1cLcLPolarization);
      instance.SetDelete(&delete_xAODcLcLTruthParticle_v1cLcLPolarization);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthParticle_v1cLcLPolarization);
      instance.SetDestructor(&destruct_xAODcLcLTruthParticle_v1cLcLPolarization);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthParticle_v1::Polarization*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthParticle_v1::Polarization*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthParticle_v1::Polarization*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthParticle_v1cLcLPolarization_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthParticle_v1::Polarization*)0x0)->GetClass();
      xAODcLcLTruthParticle_v1cLcLPolarization_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthParticle_v1cLcLPolarization_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthVertex_v1_Dictionary();
   static void xAODcLcLTruthVertex_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTruthVertex_v1(void *p = 0);
   static void *newArray_xAODcLcLTruthVertex_v1(Long_t size, void *p);
   static void delete_xAODcLcLTruthVertex_v1(void *p);
   static void deleteArray_xAODcLcLTruthVertex_v1(void *p);
   static void destruct_xAODcLcLTruthVertex_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTruthVertex_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TruthVertex_v1");
      static Long_t offset_m_v4Cached = cls->GetDataMemberOffset("m_v4Cached");
      bool& m_v4Cached = *(bool*)(target+offset_m_v4Cached);
      xAOD::TruthVertex_v1* newObj = (xAOD::TruthVertex_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_v4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthVertex_v1*)
   {
      ::xAOD::TruthVertex_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthVertex_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthVertex_v1", "xAODTruth/versions/TruthVertex_v1.h", 33,
                  typeid(::xAOD::TruthVertex_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthVertex_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthVertex_v1) );
      instance.SetNew(&new_xAODcLcLTruthVertex_v1);
      instance.SetNewArray(&newArray_xAODcLcLTruthVertex_v1);
      instance.SetDelete(&delete_xAODcLcLTruthVertex_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthVertex_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthVertex_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TruthVertex_v1";
      rule->fTarget      = "m_v4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTruthVertex_v1_0);
      rule->fCode        = "\n       m_v4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthVertex_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthVertex_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthVertex_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthVertex_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthVertex_v1*)0x0)->GetClass();
      xAODcLcLTruthVertex_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthVertex_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthEventBase_v1_Dictionary();
   static void xAODcLcLTruthEventBase_v1_TClassManip(TClass*);
   static void delete_xAODcLcLTruthEventBase_v1(void *p);
   static void deleteArray_xAODcLcLTruthEventBase_v1(void *p);
   static void destruct_xAODcLcLTruthEventBase_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthEventBase_v1*)
   {
      ::xAOD::TruthEventBase_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthEventBase_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthEventBase_v1", "xAODTruth/versions/TruthEventBase_v1.h", 31,
                  typeid(::xAOD::TruthEventBase_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthEventBase_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthEventBase_v1) );
      instance.SetDelete(&delete_xAODcLcLTruthEventBase_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthEventBase_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthEventBase_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthEventBase_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthEventBase_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthEventBase_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthEventBase_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthEventBase_v1*)0x0)->GetClass();
      xAODcLcLTruthEventBase_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthEventBase_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTruthEventBase_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTruthEventBase_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTruthEventBase_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTruthEventBase_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTruthEventBase_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTruthEventBase_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTruthEventBase_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TruthEventBase_v1>*)
   {
      ::DataVector<xAOD::TruthEventBase_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TruthEventBase_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TruthEventBase_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TruthEventBase_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTruthEventBase_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TruthEventBase_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTruthEventBase_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTruthEventBase_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTruthEventBase_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTruthEventBase_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTruthEventBase_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TruthEventBase_v1>","xAOD::TruthEventBaseContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TruthEventBase_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TruthEventBase_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthEventBase_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTruthEventBase_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthEventBase_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTruthEventBase_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTruthEventBase_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthEvent_v1_Dictionary();
   static void xAODcLcLTruthEvent_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTruthEvent_v1(void *p = 0);
   static void *newArray_xAODcLcLTruthEvent_v1(Long_t size, void *p);
   static void delete_xAODcLcLTruthEvent_v1(void *p);
   static void deleteArray_xAODcLcLTruthEvent_v1(void *p);
   static void destruct_xAODcLcLTruthEvent_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthEvent_v1*)
   {
      ::xAOD::TruthEvent_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthEvent_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthEvent_v1", "xAODTruth/versions/TruthEvent_v1.h", 30,
                  typeid(::xAOD::TruthEvent_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthEvent_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthEvent_v1) );
      instance.SetNew(&new_xAODcLcLTruthEvent_v1);
      instance.SetNewArray(&newArray_xAODcLcLTruthEvent_v1);
      instance.SetDelete(&delete_xAODcLcLTruthEvent_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthEvent_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthEvent_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthEvent_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthEvent_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthEvent_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthEvent_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthEvent_v1*)0x0)->GetClass();
      xAODcLcLTruthEvent_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthEvent_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthPileupEvent_v1_Dictionary();
   static void xAODcLcLTruthPileupEvent_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTruthPileupEvent_v1(void *p = 0);
   static void *newArray_xAODcLcLTruthPileupEvent_v1(Long_t size, void *p);
   static void delete_xAODcLcLTruthPileupEvent_v1(void *p);
   static void deleteArray_xAODcLcLTruthPileupEvent_v1(void *p);
   static void destruct_xAODcLcLTruthPileupEvent_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthPileupEvent_v1*)
   {
      ::xAOD::TruthPileupEvent_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthPileupEvent_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthPileupEvent_v1", "xAODTruth/versions/TruthPileupEvent_v1.h", 19,
                  typeid(::xAOD::TruthPileupEvent_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthPileupEvent_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthPileupEvent_v1) );
      instance.SetNew(&new_xAODcLcLTruthPileupEvent_v1);
      instance.SetNewArray(&newArray_xAODcLcLTruthPileupEvent_v1);
      instance.SetDelete(&delete_xAODcLcLTruthPileupEvent_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthPileupEvent_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthPileupEvent_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthPileupEvent_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthPileupEvent_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthPileupEvent_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthPileupEvent_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthPileupEvent_v1*)0x0)->GetClass();
      xAODcLcLTruthPileupEvent_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthPileupEvent_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthEvent_v1cLcLPdfInfo_Dictionary();
   static void xAODcLcLTruthEvent_v1cLcLPdfInfo_TClassManip(TClass*);
   static void *new_xAODcLcLTruthEvent_v1cLcLPdfInfo(void *p = 0);
   static void *newArray_xAODcLcLTruthEvent_v1cLcLPdfInfo(Long_t size, void *p);
   static void delete_xAODcLcLTruthEvent_v1cLcLPdfInfo(void *p);
   static void deleteArray_xAODcLcLTruthEvent_v1cLcLPdfInfo(void *p);
   static void destruct_xAODcLcLTruthEvent_v1cLcLPdfInfo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthEvent_v1::PdfInfo*)
   {
      ::xAOD::TruthEvent_v1::PdfInfo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthEvent_v1::PdfInfo));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthEvent_v1::PdfInfo", "xAODTruth/versions/TruthEvent_v1.h", 95,
                  typeid(::xAOD::TruthEvent_v1::PdfInfo), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthEvent_v1cLcLPdfInfo_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthEvent_v1::PdfInfo) );
      instance.SetNew(&new_xAODcLcLTruthEvent_v1cLcLPdfInfo);
      instance.SetNewArray(&newArray_xAODcLcLTruthEvent_v1cLcLPdfInfo);
      instance.SetDelete(&delete_xAODcLcLTruthEvent_v1cLcLPdfInfo);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthEvent_v1cLcLPdfInfo);
      instance.SetDestructor(&destruct_xAODcLcLTruthEvent_v1cLcLPdfInfo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthEvent_v1::PdfInfo*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthEvent_v1::PdfInfo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthEvent_v1::PdfInfo*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthEvent_v1cLcLPdfInfo_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthEvent_v1::PdfInfo*)0x0)->GetClass();
      xAODcLcLTruthEvent_v1cLcLPdfInfo_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthEvent_v1cLcLPdfInfo_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTruthParticle_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTruthParticle_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTruthParticle_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTruthParticle_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTruthParticle_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTruthParticle_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTruthParticle_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TruthParticle_v1>*)
   {
      ::DataVector<xAOD::TruthParticle_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TruthParticle_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TruthParticle_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TruthParticle_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTruthParticle_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TruthParticle_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTruthParticle_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTruthParticle_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTruthParticle_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTruthParticle_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTruthParticle_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TruthParticle_v1>","xAOD::TruthParticleContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TruthParticle_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TruthParticle_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthParticle_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTruthParticle_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthParticle_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTruthParticle_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTruthParticle_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","58F98A16-E465-4CA5-A099-73033206D8E3");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTruthVertex_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTruthVertex_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTruthVertex_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTruthVertex_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTruthVertex_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTruthVertex_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTruthVertex_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TruthVertex_v1>*)
   {
      ::DataVector<xAOD::TruthVertex_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TruthVertex_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TruthVertex_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TruthVertex_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTruthVertex_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TruthVertex_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTruthVertex_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTruthVertex_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTruthVertex_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTruthVertex_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTruthVertex_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TruthVertex_v1>","xAOD::TruthVertexContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TruthVertex_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TruthVertex_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthVertex_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTruthVertex_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthVertex_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTruthVertex_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTruthVertex_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","5FBAE0AB-09F7-4B6C-B066-0A003FC38ECF");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTruthEvent_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTruthEvent_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTruthEvent_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTruthEvent_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTruthEvent_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTruthEvent_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTruthEvent_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TruthEvent_v1>*)
   {
      ::DataVector<xAOD::TruthEvent_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TruthEvent_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TruthEvent_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TruthEvent_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTruthEvent_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TruthEvent_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTruthEvent_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTruthEvent_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTruthEvent_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTruthEvent_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTruthEvent_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TruthEvent_v1>","xAOD::TruthEventContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TruthEvent_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TruthEvent_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthEvent_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTruthEvent_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthEvent_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTruthEvent_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTruthEvent_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","6290F297-F529-40EE-9FE5-1C577678306D");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTruthPileupEvent_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTruthPileupEvent_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTruthPileupEvent_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTruthPileupEvent_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTruthPileupEvent_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTruthPileupEvent_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTruthPileupEvent_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TruthPileupEvent_v1>*)
   {
      ::DataVector<xAOD::TruthPileupEvent_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TruthPileupEvent_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TruthPileupEvent_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TruthPileupEvent_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTruthPileupEvent_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TruthPileupEvent_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTruthPileupEvent_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTruthPileupEvent_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTruthPileupEvent_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTruthPileupEvent_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTruthPileupEvent_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TruthPileupEvent_v1>","xAOD::TruthPileupEventContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TruthPileupEvent_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TruthPileupEvent_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthPileupEvent_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTruthPileupEvent_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TruthPileupEvent_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTruthPileupEvent_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTruthPileupEvent_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","05ECB16C-A36F-4853-8BB7-C9E7A84B4677");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthParticleAuxContainer_v1_Dictionary();
   static void xAODcLcLTruthParticleAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTruthParticleAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTruthParticleAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTruthParticleAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTruthParticleAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTruthParticleAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthParticleAuxContainer_v1*)
   {
      ::xAOD::TruthParticleAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthParticleAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthParticleAuxContainer_v1", "xAODTruth/versions/TruthParticleAuxContainer_v1.h", 22,
                  typeid(::xAOD::TruthParticleAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthParticleAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthParticleAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTruthParticleAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTruthParticleAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTruthParticleAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthParticleAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthParticleAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthParticleAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthParticleAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthParticleAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthParticleAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthParticleAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTruthParticleAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthParticleAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","BA8FA08F-8DD6-420D-97D5-8B54EABECD65");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthVertexAuxContainer_v1_Dictionary();
   static void xAODcLcLTruthVertexAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTruthVertexAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTruthVertexAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTruthVertexAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTruthVertexAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTruthVertexAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthVertexAuxContainer_v1*)
   {
      ::xAOD::TruthVertexAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthVertexAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthVertexAuxContainer_v1", "xAODTruth/versions/TruthVertexAuxContainer_v1.h", 26,
                  typeid(::xAOD::TruthVertexAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthVertexAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthVertexAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTruthVertexAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTruthVertexAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTruthVertexAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthVertexAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthVertexAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthVertexAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthVertexAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthVertexAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthVertexAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthVertexAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTruthVertexAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthVertexAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B6BD3B02-C411-4EB9-903F-5B099D3B1A3E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthEventAuxContainer_v1_Dictionary();
   static void xAODcLcLTruthEventAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTruthEventAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTruthEventAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTruthEventAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTruthEventAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTruthEventAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthEventAuxContainer_v1*)
   {
      ::xAOD::TruthEventAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthEventAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthEventAuxContainer_v1", "xAODTruth/versions/TruthEventAuxContainer_v1.h", 26,
                  typeid(::xAOD::TruthEventAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthEventAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthEventAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTruthEventAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTruthEventAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTruthEventAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthEventAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthEventAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthEventAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthEventAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthEventAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthEventAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthEventAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTruthEventAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthEventAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","1B945EFD-4F7D-4BDD-9FB1-6FB975315961");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTruthPileupEventAuxContainer_v1_Dictionary();
   static void xAODcLcLTruthPileupEventAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTruthPileupEventAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTruthPileupEventAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTruthPileupEventAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTruthPileupEventAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTruthPileupEventAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TruthPileupEventAuxContainer_v1*)
   {
      ::xAOD::TruthPileupEventAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TruthPileupEventAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TruthPileupEventAuxContainer_v1", "xAODTruth/versions/TruthPileupEventAuxContainer_v1.h", 21,
                  typeid(::xAOD::TruthPileupEventAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTruthPileupEventAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TruthPileupEventAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTruthPileupEventAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTruthPileupEventAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTruthPileupEventAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTruthPileupEventAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTruthPileupEventAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TruthPileupEventAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TruthPileupEventAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TruthPileupEventAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTruthPileupEventAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TruthPileupEventAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTruthPileupEventAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTruthPileupEventAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","9E9DD653-247C-4D5E-B14C-538EADEA6CD2");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TruthParticle_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TruthParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TruthParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TruthParticle_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TruthParticle_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TruthParticle_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TruthParticle_v1> >","DataLink<xAOD::TruthParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TruthParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TruthParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TruthParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TruthParticle_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TruthParticle_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TruthParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TruthParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TruthParticle_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TruthParticle_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TruthParticle_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TruthParticle_v1> >","ElementLink<xAOD::TruthParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TruthParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TruthParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TruthParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TruthParticle_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TruthVertex_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TruthVertex_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TruthVertex_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TruthVertex_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TruthVertex_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TruthVertex_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TruthVertex_v1> >","DataLink<xAOD::TruthVertexContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TruthVertex_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TruthVertex_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TruthVertex_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TruthVertex_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TruthVertex_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TruthVertex_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TruthVertex_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TruthVertex_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TruthVertex_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TruthVertex_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TruthVertex_v1> >","ElementLink<xAOD::TruthVertexContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TruthVertex_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TruthVertex_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TruthVertex_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TruthVertex_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthParticle_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthParticle_v1 : new ::xAOD::TruthParticle_v1;
   }
   static void *newArray_xAODcLcLTruthParticle_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthParticle_v1[nElements] : new ::xAOD::TruthParticle_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthParticle_v1(void *p) {
      delete ((::xAOD::TruthParticle_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthParticle_v1(void *p) {
      delete [] ((::xAOD::TruthParticle_v1*)p);
   }
   static void destruct_xAODcLcLTruthParticle_v1(void *p) {
      typedef ::xAOD::TruthParticle_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthParticle_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthParticle_v1cLcLPolarization(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthParticle_v1::Polarization : new ::xAOD::TruthParticle_v1::Polarization;
   }
   static void *newArray_xAODcLcLTruthParticle_v1cLcLPolarization(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthParticle_v1::Polarization[nElements] : new ::xAOD::TruthParticle_v1::Polarization[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthParticle_v1cLcLPolarization(void *p) {
      delete ((::xAOD::TruthParticle_v1::Polarization*)p);
   }
   static void deleteArray_xAODcLcLTruthParticle_v1cLcLPolarization(void *p) {
      delete [] ((::xAOD::TruthParticle_v1::Polarization*)p);
   }
   static void destruct_xAODcLcLTruthParticle_v1cLcLPolarization(void *p) {
      typedef ::xAOD::TruthParticle_v1::Polarization current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthParticle_v1::Polarization

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthVertex_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthVertex_v1 : new ::xAOD::TruthVertex_v1;
   }
   static void *newArray_xAODcLcLTruthVertex_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthVertex_v1[nElements] : new ::xAOD::TruthVertex_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthVertex_v1(void *p) {
      delete ((::xAOD::TruthVertex_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthVertex_v1(void *p) {
      delete [] ((::xAOD::TruthVertex_v1*)p);
   }
   static void destruct_xAODcLcLTruthVertex_v1(void *p) {
      typedef ::xAOD::TruthVertex_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthVertex_v1

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthEventBase_v1(void *p) {
      delete ((::xAOD::TruthEventBase_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthEventBase_v1(void *p) {
      delete [] ((::xAOD::TruthEventBase_v1*)p);
   }
   static void destruct_xAODcLcLTruthEventBase_v1(void *p) {
      typedef ::xAOD::TruthEventBase_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthEventBase_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTruthEventBase_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TruthEventBase_v1> : new ::DataVector<xAOD::TruthEventBase_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTruthEventBase_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TruthEventBase_v1>[nElements] : new ::DataVector<xAOD::TruthEventBase_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTruthEventBase_v1gR(void *p) {
      delete ((::DataVector<xAOD::TruthEventBase_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTruthEventBase_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TruthEventBase_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTruthEventBase_v1gR(void *p) {
      typedef ::DataVector<xAOD::TruthEventBase_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TruthEventBase_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthEvent_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthEvent_v1 : new ::xAOD::TruthEvent_v1;
   }
   static void *newArray_xAODcLcLTruthEvent_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthEvent_v1[nElements] : new ::xAOD::TruthEvent_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthEvent_v1(void *p) {
      delete ((::xAOD::TruthEvent_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthEvent_v1(void *p) {
      delete [] ((::xAOD::TruthEvent_v1*)p);
   }
   static void destruct_xAODcLcLTruthEvent_v1(void *p) {
      typedef ::xAOD::TruthEvent_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthEvent_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthPileupEvent_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthPileupEvent_v1 : new ::xAOD::TruthPileupEvent_v1;
   }
   static void *newArray_xAODcLcLTruthPileupEvent_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthPileupEvent_v1[nElements] : new ::xAOD::TruthPileupEvent_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthPileupEvent_v1(void *p) {
      delete ((::xAOD::TruthPileupEvent_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthPileupEvent_v1(void *p) {
      delete [] ((::xAOD::TruthPileupEvent_v1*)p);
   }
   static void destruct_xAODcLcLTruthPileupEvent_v1(void *p) {
      typedef ::xAOD::TruthPileupEvent_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthPileupEvent_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthEvent_v1cLcLPdfInfo(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthEvent_v1::PdfInfo : new ::xAOD::TruthEvent_v1::PdfInfo;
   }
   static void *newArray_xAODcLcLTruthEvent_v1cLcLPdfInfo(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthEvent_v1::PdfInfo[nElements] : new ::xAOD::TruthEvent_v1::PdfInfo[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthEvent_v1cLcLPdfInfo(void *p) {
      delete ((::xAOD::TruthEvent_v1::PdfInfo*)p);
   }
   static void deleteArray_xAODcLcLTruthEvent_v1cLcLPdfInfo(void *p) {
      delete [] ((::xAOD::TruthEvent_v1::PdfInfo*)p);
   }
   static void destruct_xAODcLcLTruthEvent_v1cLcLPdfInfo(void *p) {
      typedef ::xAOD::TruthEvent_v1::PdfInfo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthEvent_v1::PdfInfo

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTruthParticle_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TruthParticle_v1> : new ::DataVector<xAOD::TruthParticle_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTruthParticle_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TruthParticle_v1>[nElements] : new ::DataVector<xAOD::TruthParticle_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTruthParticle_v1gR(void *p) {
      delete ((::DataVector<xAOD::TruthParticle_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTruthParticle_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TruthParticle_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTruthParticle_v1gR(void *p) {
      typedef ::DataVector<xAOD::TruthParticle_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TruthParticle_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTruthVertex_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TruthVertex_v1> : new ::DataVector<xAOD::TruthVertex_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTruthVertex_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TruthVertex_v1>[nElements] : new ::DataVector<xAOD::TruthVertex_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTruthVertex_v1gR(void *p) {
      delete ((::DataVector<xAOD::TruthVertex_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTruthVertex_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TruthVertex_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTruthVertex_v1gR(void *p) {
      typedef ::DataVector<xAOD::TruthVertex_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TruthVertex_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTruthEvent_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TruthEvent_v1> : new ::DataVector<xAOD::TruthEvent_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTruthEvent_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TruthEvent_v1>[nElements] : new ::DataVector<xAOD::TruthEvent_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTruthEvent_v1gR(void *p) {
      delete ((::DataVector<xAOD::TruthEvent_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTruthEvent_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TruthEvent_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTruthEvent_v1gR(void *p) {
      typedef ::DataVector<xAOD::TruthEvent_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TruthEvent_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTruthPileupEvent_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TruthPileupEvent_v1> : new ::DataVector<xAOD::TruthPileupEvent_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTruthPileupEvent_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TruthPileupEvent_v1>[nElements] : new ::DataVector<xAOD::TruthPileupEvent_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTruthPileupEvent_v1gR(void *p) {
      delete ((::DataVector<xAOD::TruthPileupEvent_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTruthPileupEvent_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TruthPileupEvent_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTruthPileupEvent_v1gR(void *p) {
      typedef ::DataVector<xAOD::TruthPileupEvent_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TruthPileupEvent_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthParticleAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthParticleAuxContainer_v1 : new ::xAOD::TruthParticleAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTruthParticleAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthParticleAuxContainer_v1[nElements] : new ::xAOD::TruthParticleAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthParticleAuxContainer_v1(void *p) {
      delete ((::xAOD::TruthParticleAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthParticleAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TruthParticleAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTruthParticleAuxContainer_v1(void *p) {
      typedef ::xAOD::TruthParticleAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthParticleAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthVertexAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthVertexAuxContainer_v1 : new ::xAOD::TruthVertexAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTruthVertexAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthVertexAuxContainer_v1[nElements] : new ::xAOD::TruthVertexAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthVertexAuxContainer_v1(void *p) {
      delete ((::xAOD::TruthVertexAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthVertexAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TruthVertexAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTruthVertexAuxContainer_v1(void *p) {
      typedef ::xAOD::TruthVertexAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthVertexAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthEventAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthEventAuxContainer_v1 : new ::xAOD::TruthEventAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTruthEventAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthEventAuxContainer_v1[nElements] : new ::xAOD::TruthEventAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthEventAuxContainer_v1(void *p) {
      delete ((::xAOD::TruthEventAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthEventAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TruthEventAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTruthEventAuxContainer_v1(void *p) {
      typedef ::xAOD::TruthEventAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthEventAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTruthPileupEventAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthPileupEventAuxContainer_v1 : new ::xAOD::TruthPileupEventAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTruthPileupEventAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TruthPileupEventAuxContainer_v1[nElements] : new ::xAOD::TruthPileupEventAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTruthPileupEventAuxContainer_v1(void *p) {
      delete ((::xAOD::TruthPileupEventAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTruthPileupEventAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TruthPileupEventAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTruthPileupEventAuxContainer_v1(void *p) {
      typedef ::xAOD::TruthPileupEventAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TruthPileupEventAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TruthParticle_v1> > : new ::DataLink<DataVector<xAOD::TruthParticle_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TruthParticle_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TruthParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TruthParticle_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TruthParticle_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TruthParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TruthParticle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TruthParticle_v1> > : new ::ElementLink<DataVector<xAOD::TruthParticle_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TruthParticle_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TruthParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TruthParticle_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TruthParticle_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TruthParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TruthParticle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TruthVertex_v1> > : new ::DataLink<DataVector<xAOD::TruthVertex_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TruthVertex_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TruthVertex_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TruthVertex_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TruthVertex_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TruthVertex_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TruthVertex_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TruthVertex_v1> > : new ::ElementLink<DataVector<xAOD::TruthVertex_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TruthVertex_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TruthVertex_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TruthVertex_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TruthVertex_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TruthVertex_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TruthVertex_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > : new vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > : new vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TruthVertex_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TruthVertex_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TruthVertex_v1> > > : new vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTruthVertex_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TruthVertex_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TruthParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TruthParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TruthParticle_v1> > > : new vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTruthParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TruthParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTruth_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTruth/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTruth/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthParticleContainer_v1.h")))  TruthParticle_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthParticleAuxContainer_v1.h")))  TruthVertex_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthEventBaseContainer_v1.h")))  TruthEventBase_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthEventContainer_v1.h")))  TruthEvent_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthPileupEventContainer_v1.h")))  TruthPileupEvent_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthParticleContainer_v1.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@BA8FA08F-8DD6-420D-97D5-8B54EABECD65)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthParticleAuxContainer_v1.h")))  TruthParticleAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B6BD3B02-C411-4EB9-903F-5B099D3B1A3E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthVertexAuxContainer_v1.h")))  TruthVertexAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@1B945EFD-4F7D-4BDD-9FB1-6FB975315961)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthEventAuxContainer_v1.h")))  TruthEventAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@9E9DD653-247C-4D5E-B14C-538EADEA6CD2)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTruth/versions/TruthPileupEventAuxContainer_v1.h")))  TruthPileupEventAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTruth"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTruthDict.h 668406 2015-05-19 15:32:15Z krasznaa $
#ifndef XAODTRUTH_XAODTRUTHDICT_H
#define XAODTRUTH_XAODTRUTHDICT_H

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODTruth/versions/TruthParticleContainer_v1.h"
#include "xAODTruth/versions/TruthParticleAuxContainer_v1.h"
#include "xAODTruth/versions/TruthVertexContainer_v1.h"
#include "xAODTruth/versions/TruthVertexAuxContainer_v1.h"
#include "xAODTruth/versions/TruthEventBaseContainer_v1.h"
#include "xAODTruth/versions/TruthEventContainer_v1.h"
#include "xAODTruth/versions/TruthEventAuxContainer_v1.h"
#include "xAODTruth/versions/TruthPileupEventContainer_v1.h"
#include "xAODTruth/versions/TruthPileupEventAuxContainer_v1.h"
#include "xAODTruth/xAODTruthHelpers.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODTRUTH {
      // The DataVector types:
      xAOD::TruthParticleContainer_v1    c1;
      xAOD::TruthVertexContainer_v1      c2;
      xAOD::TruthEventBaseContainer_v1   c3;
      xAOD::TruthEventContainer_v1       c4;
      xAOD::TruthPileupEventContainer_v1 c5;
      // The smart pointer types:
      DataLink< xAOD::TruthParticleContainer_v1 > dl1;
      std::vector< DataLink< xAOD::TruthParticleContainer_v1 > > dl2;
      DataLink< xAOD::TruthVertexContainer_v1 > dl3;
      std::vector< DataLink< xAOD::TruthVertexContainer_v1 > > dl4;
      ElementLink< xAOD::TruthParticleContainer_v1 > el1;
      std::vector< ElementLink< xAOD::TruthParticleContainer_v1 > > el2;
      std::vector< std::vector< ElementLink< xAOD::TruthParticleContainer_v1 > > > el3;
      ElementLink< xAOD::TruthVertexContainer_v1 > el4;
      std::vector< ElementLink< xAOD::TruthVertexContainer_v1 > > el5;
      std::vector< std::vector< ElementLink< xAOD::TruthVertexContainer_v1 > > > el6;
   };
}

#endif // XAODTRUTH_XAODTRUTHDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::TruthParticle_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::TruthVertex_v1> >", payloadCode, "@",
"DataLink<xAOD::TruthParticleContainer_v1>", payloadCode, "@",
"DataLink<xAOD::TruthVertexContainer_v1>", payloadCode, "@",
"DataVector<xAOD::TruthEventBase_v1>", payloadCode, "@",
"DataVector<xAOD::TruthEvent_v1>", payloadCode, "@",
"DataVector<xAOD::TruthParticle_v1>", payloadCode, "@",
"DataVector<xAOD::TruthPileupEvent_v1>", payloadCode, "@",
"DataVector<xAOD::TruthVertex_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::TruthParticle_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::TruthVertex_v1> >", payloadCode, "@",
"ElementLink<xAOD::TruthParticleContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::TruthVertexContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TruthParticle_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TruthVertex_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::TruthParticleContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::TruthVertexContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::TruthParticleContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::TruthVertexContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TruthParticleContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TruthVertexContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TruthVertex_v1> > > >", payloadCode, "@",
"xAOD::TruthEventAuxContainer_v1", payloadCode, "@",
"xAOD::TruthEventBaseContainer_v1", payloadCode, "@",
"xAOD::TruthEventBase_v1", payloadCode, "@",
"xAOD::TruthEventContainer_v1", payloadCode, "@",
"xAOD::TruthEvent_v1", payloadCode, "@",
"xAOD::TruthEvent_v1::PdfInfo", payloadCode, "@",
"xAOD::TruthHelpers::getParticleTruthOrigin", payloadCode, "@",
"xAOD::TruthHelpers::getParticleTruthType", payloadCode, "@",
"xAOD::TruthHelpers::getTruthParticle", payloadCode, "@",
"xAOD::TruthParticleAuxContainer_v1", payloadCode, "@",
"xAOD::TruthParticleContainer_v1", payloadCode, "@",
"xAOD::TruthParticle_v1", payloadCode, "@",
"xAOD::TruthParticle_v1::Polarization", payloadCode, "@",
"xAOD::TruthPileupEventAuxContainer_v1", payloadCode, "@",
"xAOD::TruthPileupEventContainer_v1", payloadCode, "@",
"xAOD::TruthPileupEvent_v1", payloadCode, "@",
"xAOD::TruthVertexAuxContainer_v1", payloadCode, "@",
"xAOD::TruthVertexContainer_v1", payloadCode, "@",
"xAOD::TruthVertex_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTruth_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTruth_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTruth_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTruth_Reflex() {
  TriggerDictionaryInitialization_xAODTruth_Reflex_Impl();
}

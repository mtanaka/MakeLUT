// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrigL1CalodIobjdIxAODTrigL1Calo_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigL1Calo/xAODTrigL1Calo/xAODTrigL1CaloDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLJEMHits_v1_Dictionary();
   static void xAODcLcLJEMHits_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJEMHits_v1(void *p = 0);
   static void *newArray_xAODcLcLJEMHits_v1(Long_t size, void *p);
   static void delete_xAODcLcLJEMHits_v1(void *p);
   static void deleteArray_xAODcLcLJEMHits_v1(void *p);
   static void destruct_xAODcLcLJEMHits_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMHits_v1*)
   {
      ::xAOD::JEMHits_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMHits_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMHits_v1", "xAODTrigL1Calo/versions/JEMHits_v1.h", 18,
                  typeid(::xAOD::JEMHits_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMHits_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMHits_v1) );
      instance.SetNew(&new_xAODcLcLJEMHits_v1);
      instance.SetNewArray(&newArray_xAODcLcLJEMHits_v1);
      instance.SetDelete(&delete_xAODcLcLJEMHits_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMHits_v1);
      instance.SetDestructor(&destruct_xAODcLcLJEMHits_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMHits_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMHits_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMHits_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMHits_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMHits_v1*)0x0)->GetClass();
      xAODcLcLJEMHits_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMHits_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJEMHits_v1gR_Dictionary();
   static void DataVectorlExAODcLcLJEMHits_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJEMHits_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJEMHits_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJEMHits_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJEMHits_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLJEMHits_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JEMHits_v1>*)
   {
      ::DataVector<xAOD::JEMHits_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JEMHits_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JEMHits_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JEMHits_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJEMHits_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JEMHits_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJEMHits_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJEMHits_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJEMHits_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJEMHits_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJEMHits_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JEMHits_v1>","xAOD::JEMHitsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JEMHits_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JEMHits_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMHits_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJEMHits_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMHits_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLJEMHits_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJEMHits_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","070F54C6-320D-4F22-BD42-C87C6AF99C38");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMHitsAuxContainer_v1_Dictionary();
   static void xAODcLcLJEMHitsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJEMHitsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLJEMHitsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLJEMHitsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLJEMHitsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLJEMHitsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMHitsAuxContainer_v1*)
   {
      ::xAOD::JEMHitsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMHitsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMHitsAuxContainer_v1", "xAODTrigL1Calo/versions/JEMHitsAuxContainer_v1.h", 21,
                  typeid(::xAOD::JEMHitsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMHitsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMHitsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLJEMHitsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLJEMHitsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLJEMHitsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMHitsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLJEMHitsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMHitsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMHitsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMHitsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMHitsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMHitsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLJEMHitsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMHitsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","CE2C5182-53DD-46E0-9DFA-61C214A8FCE5");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMEtSums_v1_Dictionary();
   static void xAODcLcLJEMEtSums_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJEMEtSums_v1(void *p = 0);
   static void *newArray_xAODcLcLJEMEtSums_v1(Long_t size, void *p);
   static void delete_xAODcLcLJEMEtSums_v1(void *p);
   static void deleteArray_xAODcLcLJEMEtSums_v1(void *p);
   static void destruct_xAODcLcLJEMEtSums_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMEtSums_v1*)
   {
      ::xAOD::JEMEtSums_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMEtSums_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMEtSums_v1", "xAODTrigL1Calo/versions/JEMEtSums_v1.h", 18,
                  typeid(::xAOD::JEMEtSums_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMEtSums_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMEtSums_v1) );
      instance.SetNew(&new_xAODcLcLJEMEtSums_v1);
      instance.SetNewArray(&newArray_xAODcLcLJEMEtSums_v1);
      instance.SetDelete(&delete_xAODcLcLJEMEtSums_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMEtSums_v1);
      instance.SetDestructor(&destruct_xAODcLcLJEMEtSums_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMEtSums_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMEtSums_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMEtSums_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMEtSums_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMEtSums_v1*)0x0)->GetClass();
      xAODcLcLJEMEtSums_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMEtSums_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJEMEtSums_v1gR_Dictionary();
   static void DataVectorlExAODcLcLJEMEtSums_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJEMEtSums_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJEMEtSums_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJEMEtSums_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJEMEtSums_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLJEMEtSums_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JEMEtSums_v1>*)
   {
      ::DataVector<xAOD::JEMEtSums_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JEMEtSums_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JEMEtSums_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JEMEtSums_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJEMEtSums_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JEMEtSums_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJEMEtSums_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJEMEtSums_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJEMEtSums_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJEMEtSums_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJEMEtSums_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JEMEtSums_v1>","xAOD::JEMEtSumsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JEMEtSums_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JEMEtSums_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMEtSums_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJEMEtSums_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMEtSums_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLJEMEtSums_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJEMEtSums_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","DDB6D476-D1E2-490E-A81B-8D4FC65F1D81");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMEtSumsAuxContainer_v1_Dictionary();
   static void xAODcLcLJEMEtSumsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJEMEtSumsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLJEMEtSumsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLJEMEtSumsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLJEMEtSumsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLJEMEtSumsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMEtSumsAuxContainer_v1*)
   {
      ::xAOD::JEMEtSumsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMEtSumsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMEtSumsAuxContainer_v1", "xAODTrigL1Calo/versions/JEMEtSumsAuxContainer_v1.h", 21,
                  typeid(::xAOD::JEMEtSumsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMEtSumsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMEtSumsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLJEMEtSumsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLJEMEtSumsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLJEMEtSumsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMEtSumsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLJEMEtSumsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMEtSumsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMEtSumsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMEtSumsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMEtSumsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMEtSumsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLJEMEtSumsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMEtSumsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","4D3C658D-597A-414E-AAA3-BE61A1D664C3");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMRoI_v1_Dictionary();
   static void xAODcLcLJEMRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJEMRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLJEMRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLJEMRoI_v1(void *p);
   static void deleteArray_xAODcLcLJEMRoI_v1(void *p);
   static void destruct_xAODcLcLJEMRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMRoI_v1*)
   {
      ::xAOD::JEMRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMRoI_v1", "xAODTrigL1Calo/versions/JEMRoI_v1.h", 22,
                  typeid(::xAOD::JEMRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMRoI_v1) );
      instance.SetNew(&new_xAODcLcLJEMRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLJEMRoI_v1);
      instance.SetDelete(&delete_xAODcLcLJEMRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLJEMRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMRoI_v1*)0x0)->GetClass();
      xAODcLcLJEMRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMRoI_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJEMRoI_v1gR_Dictionary();
   static void DataVectorlExAODcLcLJEMRoI_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJEMRoI_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJEMRoI_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJEMRoI_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJEMRoI_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLJEMRoI_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JEMRoI_v1>*)
   {
      ::DataVector<xAOD::JEMRoI_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JEMRoI_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JEMRoI_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JEMRoI_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJEMRoI_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JEMRoI_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJEMRoI_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJEMRoI_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJEMRoI_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJEMRoI_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJEMRoI_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JEMRoI_v1>","xAOD::JEMRoIContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JEMRoI_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JEMRoI_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMRoI_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJEMRoI_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMRoI_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLJEMRoI_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJEMRoI_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","52384828-A57D-477D-A8C8-597C78D02686");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMRoIAuxContainer_v1_Dictionary();
   static void xAODcLcLJEMRoIAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJEMRoIAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLJEMRoIAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLJEMRoIAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLJEMRoIAuxContainer_v1(void *p);
   static void destruct_xAODcLcLJEMRoIAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMRoIAuxContainer_v1*)
   {
      ::xAOD::JEMRoIAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMRoIAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMRoIAuxContainer_v1", "xAODTrigL1Calo/versions/JEMRoIAuxContainer_v1.h", 24,
                  typeid(::xAOD::JEMRoIAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMRoIAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMRoIAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLJEMRoIAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLJEMRoIAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLJEMRoIAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMRoIAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLJEMRoIAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMRoIAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMRoIAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMRoIAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMRoIAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMRoIAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLJEMRoIAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMRoIAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","959C24E5-9746-4C70-83FA-A0140C710039");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMHits_v1_Dictionary();
   static void xAODcLcLCPMHits_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCPMHits_v1(void *p = 0);
   static void *newArray_xAODcLcLCPMHits_v1(Long_t size, void *p);
   static void delete_xAODcLcLCPMHits_v1(void *p);
   static void deleteArray_xAODcLcLCPMHits_v1(void *p);
   static void destruct_xAODcLcLCPMHits_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMHits_v1*)
   {
      ::xAOD::CPMHits_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMHits_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMHits_v1", "xAODTrigL1Calo/versions/CPMHits_v1.h", 18,
                  typeid(::xAOD::CPMHits_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMHits_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMHits_v1) );
      instance.SetNew(&new_xAODcLcLCPMHits_v1);
      instance.SetNewArray(&newArray_xAODcLcLCPMHits_v1);
      instance.SetDelete(&delete_xAODcLcLCPMHits_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMHits_v1);
      instance.SetDestructor(&destruct_xAODcLcLCPMHits_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMHits_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMHits_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMHits_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMHits_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMHits_v1*)0x0)->GetClass();
      xAODcLcLCPMHits_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMHits_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCPMHits_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCPMHits_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCPMHits_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCPMHits_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCPMHits_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCPMHits_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCPMHits_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CPMHits_v1>*)
   {
      ::DataVector<xAOD::CPMHits_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CPMHits_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CPMHits_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CPMHits_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCPMHits_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CPMHits_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCPMHits_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCPMHits_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCPMHits_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCPMHits_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCPMHits_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CPMHits_v1>","xAOD::CPMHitsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CPMHits_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CPMHits_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMHits_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCPMHits_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMHits_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCPMHits_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCPMHits_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","3D15DF3F-E014-42E3-9614-37E4CD535F9C");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMHitsAuxContainer_v1_Dictionary();
   static void xAODcLcLCPMHitsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCPMHitsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCPMHitsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCPMHitsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCPMHitsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCPMHitsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMHitsAuxContainer_v1*)
   {
      ::xAOD::CPMHitsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMHitsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMHitsAuxContainer_v1", "xAODTrigL1Calo/versions/CPMHitsAuxContainer_v1.h", 21,
                  typeid(::xAOD::CPMHitsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMHitsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMHitsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCPMHitsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCPMHitsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCPMHitsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMHitsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCPMHitsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMHitsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMHitsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMHitsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMHitsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMHitsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCPMHitsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMHitsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","28FCEB20-A9DC-4FED-9FDD-00F46948E92E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMTower_v1_Dictionary();
   static void xAODcLcLCPMTower_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCPMTower_v1(void *p = 0);
   static void *newArray_xAODcLcLCPMTower_v1(Long_t size, void *p);
   static void delete_xAODcLcLCPMTower_v1(void *p);
   static void deleteArray_xAODcLcLCPMTower_v1(void *p);
   static void destruct_xAODcLcLCPMTower_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMTower_v1*)
   {
      ::xAOD::CPMTower_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMTower_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMTower_v1", "xAODTrigL1Calo/versions/CPMTower_v1.h", 18,
                  typeid(::xAOD::CPMTower_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMTower_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMTower_v1) );
      instance.SetNew(&new_xAODcLcLCPMTower_v1);
      instance.SetNewArray(&newArray_xAODcLcLCPMTower_v1);
      instance.SetDelete(&delete_xAODcLcLCPMTower_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMTower_v1);
      instance.SetDestructor(&destruct_xAODcLcLCPMTower_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMTower_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMTower_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMTower_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMTower_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMTower_v1*)0x0)->GetClass();
      xAODcLcLCPMTower_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMTower_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCPMTower_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCPMTower_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCPMTower_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCPMTower_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCPMTower_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCPMTower_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCPMTower_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CPMTower_v1>*)
   {
      ::DataVector<xAOD::CPMTower_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CPMTower_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CPMTower_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CPMTower_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCPMTower_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CPMTower_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCPMTower_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCPMTower_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCPMTower_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCPMTower_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCPMTower_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CPMTower_v1>","xAOD::CPMTowerContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CPMTower_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CPMTower_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMTower_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCPMTower_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMTower_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCPMTower_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCPMTower_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","D220C61E-ECD1-427C-86D0-2C88E00ABBD2");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMTowerAuxContainer_v1_Dictionary();
   static void xAODcLcLCPMTowerAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCPMTowerAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCPMTowerAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCPMTowerAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCPMTowerAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCPMTowerAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMTowerAuxContainer_v1*)
   {
      ::xAOD::CPMTowerAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMTowerAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMTowerAuxContainer_v1", "xAODTrigL1Calo/versions/CPMTowerAuxContainer_v1.h", 21,
                  typeid(::xAOD::CPMTowerAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMTowerAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMTowerAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCPMTowerAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCPMTowerAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCPMTowerAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMTowerAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCPMTowerAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMTowerAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMTowerAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMTowerAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMTowerAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMTowerAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCPMTowerAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMTowerAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","529B6BD7-7B7D-438C-94D7-BAA49A022C47");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMRoI_v1_Dictionary();
   static void xAODcLcLCPMRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCPMRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLCPMRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLCPMRoI_v1(void *p);
   static void deleteArray_xAODcLcLCPMRoI_v1(void *p);
   static void destruct_xAODcLcLCPMRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMRoI_v1*)
   {
      ::xAOD::CPMRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMRoI_v1", "xAODTrigL1Calo/versions/CPMRoI_v1.h", 22,
                  typeid(::xAOD::CPMRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMRoI_v1) );
      instance.SetNew(&new_xAODcLcLCPMRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLCPMRoI_v1);
      instance.SetDelete(&delete_xAODcLcLCPMRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLCPMRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMRoI_v1*)0x0)->GetClass();
      xAODcLcLCPMRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMRoI_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCPMRoI_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCPMRoI_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCPMRoI_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCPMRoI_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCPMRoI_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCPMRoI_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCPMRoI_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CPMRoI_v1>*)
   {
      ::DataVector<xAOD::CPMRoI_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CPMRoI_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CPMRoI_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CPMRoI_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCPMRoI_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CPMRoI_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCPMRoI_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCPMRoI_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCPMRoI_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCPMRoI_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCPMRoI_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CPMRoI_v1>","xAOD::CPMRoIContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CPMRoI_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CPMRoI_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMRoI_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCPMRoI_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMRoI_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCPMRoI_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCPMRoI_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","4B48CC07-6B22-46EC-9BD5-51379664B9BC");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMRoIAuxContainer_v1_Dictionary();
   static void xAODcLcLCPMRoIAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCPMRoIAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCPMRoIAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCPMRoIAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCPMRoIAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCPMRoIAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMRoIAuxContainer_v1*)
   {
      ::xAOD::CPMRoIAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMRoIAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMRoIAuxContainer_v1", "xAODTrigL1Calo/versions/CPMRoIAuxContainer_v1.h", 24,
                  typeid(::xAOD::CPMRoIAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMRoIAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMRoIAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCPMRoIAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCPMRoIAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCPMRoIAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMRoIAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCPMRoIAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMRoIAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMRoIAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMRoIAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMRoIAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMRoIAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCPMRoIAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMRoIAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","8FA92C8D-214F-4B8F-84CA-CB7267CAD0F2");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMMCPHits_v1_Dictionary();
   static void xAODcLcLCMMCPHits_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMMCPHits_v1(void *p = 0);
   static void *newArray_xAODcLcLCMMCPHits_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMMCPHits_v1(void *p);
   static void deleteArray_xAODcLcLCMMCPHits_v1(void *p);
   static void destruct_xAODcLcLCMMCPHits_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMMCPHits_v1*)
   {
      ::xAOD::CMMCPHits_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMMCPHits_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMMCPHits_v1", "xAODTrigL1Calo/versions/CMMCPHits_v1.h", 19,
                  typeid(::xAOD::CMMCPHits_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMMCPHits_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMMCPHits_v1) );
      instance.SetNew(&new_xAODcLcLCMMCPHits_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMMCPHits_v1);
      instance.SetDelete(&delete_xAODcLcLCMMCPHits_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMMCPHits_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMMCPHits_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMMCPHits_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMMCPHits_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMMCPHits_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMMCPHits_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMMCPHits_v1*)0x0)->GetClass();
      xAODcLcLCMMCPHits_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMMCPHits_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCMMCPHits_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCMMCPHits_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCMMCPHits_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCMMCPHits_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCMMCPHits_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCMMCPHits_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCMMCPHits_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CMMCPHits_v1>*)
   {
      ::DataVector<xAOD::CMMCPHits_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CMMCPHits_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CMMCPHits_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CMMCPHits_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCMMCPHits_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CMMCPHits_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCMMCPHits_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCMMCPHits_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCMMCPHits_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCMMCPHits_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCMMCPHits_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CMMCPHits_v1>","xAOD::CMMCPHitsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CMMCPHits_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CMMCPHits_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CMMCPHits_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCMMCPHits_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CMMCPHits_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCMMCPHits_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCMMCPHits_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","8826CC48-CD08-4FD7-9AC4-358A4888271F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMMCPHitsAuxContainer_v1_Dictionary();
   static void xAODcLcLCMMCPHitsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMMCPHitsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCMMCPHitsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMMCPHitsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCMMCPHitsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCMMCPHitsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMMCPHitsAuxContainer_v1*)
   {
      ::xAOD::CMMCPHitsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMMCPHitsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMMCPHitsAuxContainer_v1", "xAODTrigL1Calo/versions/CMMCPHitsAuxContainer_v1.h", 21,
                  typeid(::xAOD::CMMCPHitsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMMCPHitsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMMCPHitsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCMMCPHitsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMMCPHitsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCMMCPHitsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMMCPHitsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMMCPHitsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMMCPHitsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMMCPHitsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMMCPHitsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMMCPHitsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMMCPHitsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCMMCPHitsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMMCPHitsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","1951D4B4-D471-4256-8F0D-D666043E2889");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMMEtSums_v1_Dictionary();
   static void xAODcLcLCMMEtSums_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMMEtSums_v1(void *p = 0);
   static void *newArray_xAODcLcLCMMEtSums_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMMEtSums_v1(void *p);
   static void deleteArray_xAODcLcLCMMEtSums_v1(void *p);
   static void destruct_xAODcLcLCMMEtSums_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMMEtSums_v1*)
   {
      ::xAOD::CMMEtSums_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMMEtSums_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMMEtSums_v1", "xAODTrigL1Calo/versions/CMMEtSums_v1.h", 18,
                  typeid(::xAOD::CMMEtSums_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMMEtSums_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMMEtSums_v1) );
      instance.SetNew(&new_xAODcLcLCMMEtSums_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMMEtSums_v1);
      instance.SetDelete(&delete_xAODcLcLCMMEtSums_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMMEtSums_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMMEtSums_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMMEtSums_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMMEtSums_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMMEtSums_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMMEtSums_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMMEtSums_v1*)0x0)->GetClass();
      xAODcLcLCMMEtSums_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMMEtSums_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCMMEtSums_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCMMEtSums_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCMMEtSums_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCMMEtSums_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCMMEtSums_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCMMEtSums_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCMMEtSums_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CMMEtSums_v1>*)
   {
      ::DataVector<xAOD::CMMEtSums_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CMMEtSums_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CMMEtSums_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CMMEtSums_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCMMEtSums_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CMMEtSums_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCMMEtSums_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCMMEtSums_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCMMEtSums_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCMMEtSums_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCMMEtSums_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CMMEtSums_v1>","xAOD::CMMEtSumsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CMMEtSums_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CMMEtSums_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CMMEtSums_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCMMEtSums_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CMMEtSums_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCMMEtSums_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCMMEtSums_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","186A613D-0C91-4880-9B09-B41C28B17B53");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMMEtSumsAuxContainer_v1_Dictionary();
   static void xAODcLcLCMMEtSumsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMMEtSumsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCMMEtSumsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMMEtSumsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCMMEtSumsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCMMEtSumsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMMEtSumsAuxContainer_v1*)
   {
      ::xAOD::CMMEtSumsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMMEtSumsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMMEtSumsAuxContainer_v1", "xAODTrigL1Calo/versions/CMMEtSumsAuxContainer_v1.h", 21,
                  typeid(::xAOD::CMMEtSumsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMMEtSumsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMMEtSumsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCMMEtSumsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMMEtSumsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCMMEtSumsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMMEtSumsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMMEtSumsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMMEtSumsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMMEtSumsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMMEtSumsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMMEtSumsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMMEtSumsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCMMEtSumsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMMEtSumsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","A072EDD3-880D-46D0-A120-48C116FDB14E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMMJetHits_v1_Dictionary();
   static void xAODcLcLCMMJetHits_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMMJetHits_v1(void *p = 0);
   static void *newArray_xAODcLcLCMMJetHits_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMMJetHits_v1(void *p);
   static void deleteArray_xAODcLcLCMMJetHits_v1(void *p);
   static void destruct_xAODcLcLCMMJetHits_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMMJetHits_v1*)
   {
      ::xAOD::CMMJetHits_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMMJetHits_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMMJetHits_v1", "xAODTrigL1Calo/versions/CMMJetHits_v1.h", 18,
                  typeid(::xAOD::CMMJetHits_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMMJetHits_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMMJetHits_v1) );
      instance.SetNew(&new_xAODcLcLCMMJetHits_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMMJetHits_v1);
      instance.SetDelete(&delete_xAODcLcLCMMJetHits_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMMJetHits_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMMJetHits_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMMJetHits_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMMJetHits_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMMJetHits_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMMJetHits_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMMJetHits_v1*)0x0)->GetClass();
      xAODcLcLCMMJetHits_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMMJetHits_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCMMJetHits_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCMMJetHits_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCMMJetHits_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCMMJetHits_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCMMJetHits_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCMMJetHits_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCMMJetHits_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CMMJetHits_v1>*)
   {
      ::DataVector<xAOD::CMMJetHits_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CMMJetHits_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CMMJetHits_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CMMJetHits_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCMMJetHits_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CMMJetHits_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCMMJetHits_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCMMJetHits_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCMMJetHits_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCMMJetHits_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCMMJetHits_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CMMJetHits_v1>","xAOD::CMMJetHitsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CMMJetHits_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CMMJetHits_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CMMJetHits_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCMMJetHits_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CMMJetHits_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCMMJetHits_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCMMJetHits_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","43B6130D-4C3F-4E22-9FE2-680E6DFA149F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMMJetHitsAuxContainer_v1_Dictionary();
   static void xAODcLcLCMMJetHitsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMMJetHitsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCMMJetHitsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMMJetHitsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCMMJetHitsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCMMJetHitsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMMJetHitsAuxContainer_v1*)
   {
      ::xAOD::CMMJetHitsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMMJetHitsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMMJetHitsAuxContainer_v1", "xAODTrigL1Calo/versions/CMMJetHitsAuxContainer_v1.h", 21,
                  typeid(::xAOD::CMMJetHitsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMMJetHitsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMMJetHitsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCMMJetHitsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMMJetHitsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCMMJetHitsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMMJetHitsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMMJetHitsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMMJetHitsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMMJetHitsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMMJetHitsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMMJetHitsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMMJetHitsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCMMJetHitsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMMJetHitsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B3C8D000-0E7F-4A0D-8F05-6DA36FFC9DD7");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMMRoI_v1_Dictionary();
   static void xAODcLcLCMMRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMMRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLCMMRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMMRoI_v1(void *p);
   static void deleteArray_xAODcLcLCMMRoI_v1(void *p);
   static void destruct_xAODcLcLCMMRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMMRoI_v1*)
   {
      ::xAOD::CMMRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMMRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMMRoI_v1", "xAODTrigL1Calo/versions/CMMRoI_v1.h", 22,
                  typeid(::xAOD::CMMRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMMRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMMRoI_v1) );
      instance.SetNew(&new_xAODcLcLCMMRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMMRoI_v1);
      instance.SetDelete(&delete_xAODcLcLCMMRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMMRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMMRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMMRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMMRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMMRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMMRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMMRoI_v1*)0x0)->GetClass();
      xAODcLcLCMMRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMMRoI_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","2BA7B6B2-69C6-40E8-8D2A-0398371EDC81");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMMRoIAuxInfo_v1_Dictionary();
   static void xAODcLcLCMMRoIAuxInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMMRoIAuxInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLCMMRoIAuxInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMMRoIAuxInfo_v1(void *p);
   static void deleteArray_xAODcLcLCMMRoIAuxInfo_v1(void *p);
   static void destruct_xAODcLcLCMMRoIAuxInfo_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMMRoIAuxInfo_v1*)
   {
      ::xAOD::CMMRoIAuxInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMMRoIAuxInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMMRoIAuxInfo_v1", "xAODTrigL1Calo/versions/CMMRoIAuxInfo_v1.h", 24,
                  typeid(::xAOD::CMMRoIAuxInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMMRoIAuxInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMMRoIAuxInfo_v1) );
      instance.SetNew(&new_xAODcLcLCMMRoIAuxInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMMRoIAuxInfo_v1);
      instance.SetDelete(&delete_xAODcLcLCMMRoIAuxInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMMRoIAuxInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMMRoIAuxInfo_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMMRoIAuxInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMMRoIAuxInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMMRoIAuxInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMMRoIAuxInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMMRoIAuxInfo_v1*)0x0)->GetClass();
      xAODcLcLCMMRoIAuxInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMMRoIAuxInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B0CAB5B5-6B6B-437D-B353-1397C1F01B7E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetElement_v1_Dictionary();
   static void xAODcLcLJetElement_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJetElement_v1(void *p = 0);
   static void *newArray_xAODcLcLJetElement_v1(Long_t size, void *p);
   static void delete_xAODcLcLJetElement_v1(void *p);
   static void deleteArray_xAODcLcLJetElement_v1(void *p);
   static void destruct_xAODcLcLJetElement_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetElement_v1*)
   {
      ::xAOD::JetElement_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetElement_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetElement_v1", "xAODTrigL1Calo/versions/JetElement_v1.h", 18,
                  typeid(::xAOD::JetElement_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetElement_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetElement_v1) );
      instance.SetNew(&new_xAODcLcLJetElement_v1);
      instance.SetNewArray(&newArray_xAODcLcLJetElement_v1);
      instance.SetDelete(&delete_xAODcLcLJetElement_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetElement_v1);
      instance.SetDestructor(&destruct_xAODcLcLJetElement_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetElement_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetElement_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetElement_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetElement_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetElement_v1*)0x0)->GetClass();
      xAODcLcLJetElement_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetElement_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJetElement_v1gR_Dictionary();
   static void DataVectorlExAODcLcLJetElement_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJetElement_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJetElement_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJetElement_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJetElement_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLJetElement_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JetElement_v1>*)
   {
      ::DataVector<xAOD::JetElement_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JetElement_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JetElement_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JetElement_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJetElement_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JetElement_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJetElement_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJetElement_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJetElement_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJetElement_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJetElement_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JetElement_v1>","xAOD::JetElementContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JetElement_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JetElement_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JetElement_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJetElement_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JetElement_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLJetElement_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJetElement_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B1103844-7187-4305-B20A-748591A54145");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetElementAuxContainer_v1_Dictionary();
   static void xAODcLcLJetElementAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJetElementAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLJetElementAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLJetElementAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLJetElementAuxContainer_v1(void *p);
   static void destruct_xAODcLcLJetElementAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetElementAuxContainer_v1*)
   {
      ::xAOD::JetElementAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetElementAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetElementAuxContainer_v1", "xAODTrigL1Calo/versions/JetElementAuxContainer_v1.h", 21,
                  typeid(::xAOD::JetElementAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetElementAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetElementAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLJetElementAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLJetElementAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLJetElementAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetElementAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLJetElementAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetElementAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetElementAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetElementAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetElementAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetElementAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLJetElementAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetElementAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","7A0A2307-5729-4306-B520-4F75BBCC8E76");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLRODHeader_v1_Dictionary();
   static void xAODcLcLRODHeader_v1_TClassManip(TClass*);
   static void *new_xAODcLcLRODHeader_v1(void *p = 0);
   static void *newArray_xAODcLcLRODHeader_v1(Long_t size, void *p);
   static void delete_xAODcLcLRODHeader_v1(void *p);
   static void deleteArray_xAODcLcLRODHeader_v1(void *p);
   static void destruct_xAODcLcLRODHeader_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::RODHeader_v1*)
   {
      ::xAOD::RODHeader_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::RODHeader_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::RODHeader_v1", "xAODTrigL1Calo/versions/RODHeader_v1.h", 18,
                  typeid(::xAOD::RODHeader_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLRODHeader_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::RODHeader_v1) );
      instance.SetNew(&new_xAODcLcLRODHeader_v1);
      instance.SetNewArray(&newArray_xAODcLcLRODHeader_v1);
      instance.SetDelete(&delete_xAODcLcLRODHeader_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLRODHeader_v1);
      instance.SetDestructor(&destruct_xAODcLcLRODHeader_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::RODHeader_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::RODHeader_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::RODHeader_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLRODHeader_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::RODHeader_v1*)0x0)->GetClass();
      xAODcLcLRODHeader_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLRODHeader_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLRODHeader_v1gR_Dictionary();
   static void DataVectorlExAODcLcLRODHeader_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLRODHeader_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLRODHeader_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLRODHeader_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLRODHeader_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLRODHeader_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::RODHeader_v1>*)
   {
      ::DataVector<xAOD::RODHeader_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::RODHeader_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::RODHeader_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::RODHeader_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLRODHeader_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::RODHeader_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLRODHeader_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLRODHeader_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLRODHeader_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLRODHeader_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLRODHeader_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::RODHeader_v1>","xAOD::RODHeaderContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::RODHeader_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::RODHeader_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::RODHeader_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLRODHeader_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::RODHeader_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLRODHeader_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLRODHeader_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","AA0924D2-23E2-47B3-8C7D-9D3B104B8990");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLRODHeaderAuxContainer_v1_Dictionary();
   static void xAODcLcLRODHeaderAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLRODHeaderAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLRODHeaderAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLRODHeaderAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLRODHeaderAuxContainer_v1(void *p);
   static void destruct_xAODcLcLRODHeaderAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::RODHeaderAuxContainer_v1*)
   {
      ::xAOD::RODHeaderAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::RODHeaderAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::RODHeaderAuxContainer_v1", "xAODTrigL1Calo/versions/RODHeaderAuxContainer_v1.h", 24,
                  typeid(::xAOD::RODHeaderAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLRODHeaderAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::RODHeaderAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLRODHeaderAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLRODHeaderAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLRODHeaderAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLRODHeaderAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLRODHeaderAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::RODHeaderAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::RODHeaderAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::RODHeaderAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLRODHeaderAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::RODHeaderAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLRODHeaderAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLRODHeaderAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","6524E46D-CC9C-43D2-BAE0-55310A6B316C");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTriggerTower_v1_Dictionary();
   static void xAODcLcLTriggerTower_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTriggerTower_v1(void *p = 0);
   static void *newArray_xAODcLcLTriggerTower_v1(Long_t size, void *p);
   static void delete_xAODcLcLTriggerTower_v1(void *p);
   static void deleteArray_xAODcLcLTriggerTower_v1(void *p);
   static void destruct_xAODcLcLTriggerTower_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TriggerTower_v1*)
   {
      ::xAOD::TriggerTower_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TriggerTower_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TriggerTower_v1", "xAODTrigL1Calo/versions/TriggerTower_v1.h", 39,
                  typeid(::xAOD::TriggerTower_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTriggerTower_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TriggerTower_v1) );
      instance.SetNew(&new_xAODcLcLTriggerTower_v1);
      instance.SetNewArray(&newArray_xAODcLcLTriggerTower_v1);
      instance.SetDelete(&delete_xAODcLcLTriggerTower_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTriggerTower_v1);
      instance.SetDestructor(&destruct_xAODcLcLTriggerTower_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TriggerTower_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TriggerTower_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TriggerTower_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTriggerTower_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TriggerTower_v1*)0x0)->GetClass();
      xAODcLcLTriggerTower_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTriggerTower_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTriggerTower_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTriggerTower_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTriggerTower_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTriggerTower_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTriggerTower_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTriggerTower_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTriggerTower_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TriggerTower_v1>*)
   {
      ::DataVector<xAOD::TriggerTower_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TriggerTower_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TriggerTower_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TriggerTower_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTriggerTower_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TriggerTower_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTriggerTower_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTriggerTower_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTriggerTower_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTriggerTower_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTriggerTower_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TriggerTower_v1>","xAOD::TriggerTowerContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TriggerTower_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TriggerTower_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TriggerTower_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTriggerTower_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TriggerTower_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTriggerTower_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTriggerTower_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","DE2A7891-B5FE-4811-8178-EF106743312C");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTriggerTowerAuxContainer_v1_Dictionary();
   static void xAODcLcLTriggerTowerAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTriggerTowerAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTriggerTowerAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTriggerTowerAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTriggerTowerAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTriggerTowerAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TriggerTowerAuxContainer_v1*)
   {
      ::xAOD::TriggerTowerAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TriggerTowerAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TriggerTowerAuxContainer_v1", "xAODTrigL1Calo/versions/TriggerTowerAuxContainer_v1.h", 21,
                  typeid(::xAOD::TriggerTowerAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTriggerTowerAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TriggerTowerAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTriggerTowerAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTriggerTowerAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTriggerTowerAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTriggerTowerAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTriggerTowerAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TriggerTowerAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TriggerTowerAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TriggerTowerAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTriggerTowerAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TriggerTowerAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTriggerTowerAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTriggerTowerAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","3C5D20CF-6F77-4E67-B182-674EDFD2FE38");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTriggerTower_v2_Dictionary();
   static void xAODcLcLTriggerTower_v2_TClassManip(TClass*);
   static void *new_xAODcLcLTriggerTower_v2(void *p = 0);
   static void *newArray_xAODcLcLTriggerTower_v2(Long_t size, void *p);
   static void delete_xAODcLcLTriggerTower_v2(void *p);
   static void deleteArray_xAODcLcLTriggerTower_v2(void *p);
   static void destruct_xAODcLcLTriggerTower_v2(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTriggerTower_v2_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TriggerTower_v2");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::TriggerTower_v2* newObj = (xAOD::TriggerTower_v2*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
     
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TriggerTower_v2*)
   {
      ::xAOD::TriggerTower_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TriggerTower_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TriggerTower_v2", "xAODTrigL1Calo/versions/TriggerTower_v2.h", 46,
                  typeid(::xAOD::TriggerTower_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLTriggerTower_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TriggerTower_v2) );
      instance.SetNew(&new_xAODcLcLTriggerTower_v2);
      instance.SetNewArray(&newArray_xAODcLcLTriggerTower_v2);
      instance.SetDelete(&delete_xAODcLcLTriggerTower_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTriggerTower_v2);
      instance.SetDestructor(&destruct_xAODcLcLTriggerTower_v2);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TriggerTower_v2";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTriggerTower_v2_0);
      rule->fCode        = "\n       m_p4Cached = false;\n     ";
      rule->fVersion     = "[2-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TriggerTower_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::TriggerTower_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TriggerTower_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTriggerTower_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TriggerTower_v2*)0x0)->GetClass();
      xAODcLcLTriggerTower_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTriggerTower_v2_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTriggerTower_v2gR_Dictionary();
   static void DataVectorlExAODcLcLTriggerTower_v2gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTriggerTower_v2gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTriggerTower_v2gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTriggerTower_v2gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTriggerTower_v2gR(void *p);
   static void destruct_DataVectorlExAODcLcLTriggerTower_v2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TriggerTower_v2>*)
   {
      ::DataVector<xAOD::TriggerTower_v2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TriggerTower_v2>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TriggerTower_v2>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TriggerTower_v2>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTriggerTower_v2gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TriggerTower_v2>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTriggerTower_v2gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTriggerTower_v2gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTriggerTower_v2gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTriggerTower_v2gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTriggerTower_v2gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TriggerTower_v2>","xAOD::TriggerTowerContainer_v2");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TriggerTower_v2>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TriggerTower_v2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TriggerTower_v2>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTriggerTower_v2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TriggerTower_v2>*)0x0)->GetClass();
      DataVectorlExAODcLcLTriggerTower_v2gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTriggerTower_v2gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","730DE7B8-C24A-4567-A66D-0386DC50E9AC");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTriggerTowerAuxContainer_v2_Dictionary();
   static void xAODcLcLTriggerTowerAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLTriggerTowerAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLTriggerTowerAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLTriggerTowerAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLTriggerTowerAuxContainer_v2(void *p);
   static void destruct_xAODcLcLTriggerTowerAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TriggerTowerAuxContainer_v2*)
   {
      ::xAOD::TriggerTowerAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TriggerTowerAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TriggerTowerAuxContainer_v2", "xAODTrigL1Calo/versions/TriggerTowerAuxContainer_v2.h", 23,
                  typeid(::xAOD::TriggerTowerAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLTriggerTowerAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TriggerTowerAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLTriggerTowerAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLTriggerTowerAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLTriggerTowerAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTriggerTowerAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLTriggerTowerAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TriggerTowerAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::TriggerTowerAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TriggerTowerAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTriggerTowerAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TriggerTowerAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLTriggerTowerAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTriggerTowerAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","07FF691A-A920-43B2-A4E7-637D335E6929");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXCPHits_v1_Dictionary();
   static void xAODcLcLCMXCPHits_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXCPHits_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXCPHits_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXCPHits_v1(void *p);
   static void deleteArray_xAODcLcLCMXCPHits_v1(void *p);
   static void destruct_xAODcLcLCMXCPHits_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXCPHits_v1*)
   {
      ::xAOD::CMXCPHits_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXCPHits_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXCPHits_v1", "xAODTrigL1Calo/versions/CMXCPHits_v1.h", 22,
                  typeid(::xAOD::CMXCPHits_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXCPHits_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXCPHits_v1) );
      instance.SetNew(&new_xAODcLcLCMXCPHits_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXCPHits_v1);
      instance.SetDelete(&delete_xAODcLcLCMXCPHits_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXCPHits_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXCPHits_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXCPHits_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXCPHits_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXCPHits_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXCPHits_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXCPHits_v1*)0x0)->GetClass();
      xAODcLcLCMXCPHits_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXCPHits_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCMXCPHits_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCMXCPHits_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCMXCPHits_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCMXCPHits_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCMXCPHits_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCMXCPHits_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCMXCPHits_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CMXCPHits_v1>*)
   {
      ::DataVector<xAOD::CMXCPHits_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CMXCPHits_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CMXCPHits_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CMXCPHits_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCMXCPHits_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CMXCPHits_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCMXCPHits_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCMXCPHits_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCMXCPHits_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCMXCPHits_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCMXCPHits_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CMXCPHits_v1>","xAOD::CMXCPHitsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CMXCPHits_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CMXCPHits_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXCPHits_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCMXCPHits_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXCPHits_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCMXCPHits_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCMXCPHits_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","96ED5A94-473C-4E96-A348-78077310635F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXCPHitsAuxContainer_v1_Dictionary();
   static void xAODcLcLCMXCPHitsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXCPHitsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXCPHitsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXCPHitsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCMXCPHitsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCMXCPHitsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXCPHitsAuxContainer_v1*)
   {
      ::xAOD::CMXCPHitsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXCPHitsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXCPHitsAuxContainer_v1", "xAODTrigL1Calo/versions/CMXCPHitsAuxContainer_v1.h", 23,
                  typeid(::xAOD::CMXCPHitsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXCPHitsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXCPHitsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCMXCPHitsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXCPHitsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCMXCPHitsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXCPHitsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXCPHitsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXCPHitsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXCPHitsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXCPHitsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXCPHitsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXCPHitsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCMXCPHitsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXCPHitsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F3F8A07D-536B-42EB-9792-E08DB1F76C1F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXCPTob_v1_Dictionary();
   static void xAODcLcLCMXCPTob_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXCPTob_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXCPTob_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXCPTob_v1(void *p);
   static void deleteArray_xAODcLcLCMXCPTob_v1(void *p);
   static void destruct_xAODcLcLCMXCPTob_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXCPTob_v1*)
   {
      ::xAOD::CMXCPTob_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXCPTob_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXCPTob_v1", "xAODTrigL1Calo/versions/CMXCPTob_v1.h", 22,
                  typeid(::xAOD::CMXCPTob_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXCPTob_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXCPTob_v1) );
      instance.SetNew(&new_xAODcLcLCMXCPTob_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXCPTob_v1);
      instance.SetDelete(&delete_xAODcLcLCMXCPTob_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXCPTob_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXCPTob_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXCPTob_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXCPTob_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXCPTob_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXCPTob_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXCPTob_v1*)0x0)->GetClass();
      xAODcLcLCMXCPTob_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXCPTob_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCMXCPTob_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCMXCPTob_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCMXCPTob_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCMXCPTob_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCMXCPTob_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCMXCPTob_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCMXCPTob_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CMXCPTob_v1>*)
   {
      ::DataVector<xAOD::CMXCPTob_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CMXCPTob_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CMXCPTob_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CMXCPTob_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCMXCPTob_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CMXCPTob_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCMXCPTob_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCMXCPTob_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCMXCPTob_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCMXCPTob_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCMXCPTob_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CMXCPTob_v1>","xAOD::CMXCPTobContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CMXCPTob_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CMXCPTob_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXCPTob_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCMXCPTob_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXCPTob_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCMXCPTob_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCMXCPTob_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","5E70EF98-47ED-441F-8CC3-F1906AA2E8CD");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXCPTobAuxContainer_v1_Dictionary();
   static void xAODcLcLCMXCPTobAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXCPTobAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXCPTobAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXCPTobAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCMXCPTobAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCMXCPTobAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXCPTobAuxContainer_v1*)
   {
      ::xAOD::CMXCPTobAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXCPTobAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXCPTobAuxContainer_v1", "xAODTrigL1Calo/versions/CMXCPTobAuxContainer_v1.h", 23,
                  typeid(::xAOD::CMXCPTobAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXCPTobAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXCPTobAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCMXCPTobAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXCPTobAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCMXCPTobAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXCPTobAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXCPTobAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXCPTobAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXCPTobAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXCPTobAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXCPTobAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXCPTobAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCMXCPTobAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXCPTobAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B4A55AC8-3D30-42AF-B954-82DA7D8A07CB");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXJetHits_v1_Dictionary();
   static void xAODcLcLCMXJetHits_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXJetHits_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXJetHits_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXJetHits_v1(void *p);
   static void deleteArray_xAODcLcLCMXJetHits_v1(void *p);
   static void destruct_xAODcLcLCMXJetHits_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXJetHits_v1*)
   {
      ::xAOD::CMXJetHits_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXJetHits_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXJetHits_v1", "xAODTrigL1Calo/versions/CMXJetHits_v1.h", 24,
                  typeid(::xAOD::CMXJetHits_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXJetHits_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXJetHits_v1) );
      instance.SetNew(&new_xAODcLcLCMXJetHits_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXJetHits_v1);
      instance.SetDelete(&delete_xAODcLcLCMXJetHits_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXJetHits_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXJetHits_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXJetHits_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXJetHits_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXJetHits_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXJetHits_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXJetHits_v1*)0x0)->GetClass();
      xAODcLcLCMXJetHits_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXJetHits_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCMXJetHits_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCMXJetHits_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCMXJetHits_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCMXJetHits_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCMXJetHits_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCMXJetHits_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCMXJetHits_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CMXJetHits_v1>*)
   {
      ::DataVector<xAOD::CMXJetHits_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CMXJetHits_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CMXJetHits_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CMXJetHits_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCMXJetHits_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CMXJetHits_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCMXJetHits_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCMXJetHits_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCMXJetHits_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCMXJetHits_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCMXJetHits_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CMXJetHits_v1>","xAOD::CMXJetHitsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CMXJetHits_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CMXJetHits_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXJetHits_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCMXJetHits_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXJetHits_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCMXJetHits_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCMXJetHits_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","492E21EF-A9B0-4262-80EE-4ADECDEA44E7");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXJetHitsAuxContainer_v1_Dictionary();
   static void xAODcLcLCMXJetHitsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXJetHitsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXJetHitsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXJetHitsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCMXJetHitsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCMXJetHitsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXJetHitsAuxContainer_v1*)
   {
      ::xAOD::CMXJetHitsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXJetHitsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXJetHitsAuxContainer_v1", "xAODTrigL1Calo/versions/CMXJetHitsAuxContainer_v1.h", 23,
                  typeid(::xAOD::CMXJetHitsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXJetHitsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXJetHitsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCMXJetHitsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXJetHitsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCMXJetHitsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXJetHitsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXJetHitsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXJetHitsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXJetHitsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXJetHitsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXJetHitsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXJetHitsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCMXJetHitsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXJetHitsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","509DB371-6A95-4DE1-B950-751B99EBF5EC");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXJetTob_v1_Dictionary();
   static void xAODcLcLCMXJetTob_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXJetTob_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXJetTob_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXJetTob_v1(void *p);
   static void deleteArray_xAODcLcLCMXJetTob_v1(void *p);
   static void destruct_xAODcLcLCMXJetTob_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXJetTob_v1*)
   {
      ::xAOD::CMXJetTob_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXJetTob_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXJetTob_v1", "xAODTrigL1Calo/versions/CMXJetTob_v1.h", 22,
                  typeid(::xAOD::CMXJetTob_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXJetTob_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXJetTob_v1) );
      instance.SetNew(&new_xAODcLcLCMXJetTob_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXJetTob_v1);
      instance.SetDelete(&delete_xAODcLcLCMXJetTob_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXJetTob_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXJetTob_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXJetTob_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXJetTob_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXJetTob_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXJetTob_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXJetTob_v1*)0x0)->GetClass();
      xAODcLcLCMXJetTob_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXJetTob_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCMXJetTob_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCMXJetTob_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCMXJetTob_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCMXJetTob_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCMXJetTob_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCMXJetTob_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCMXJetTob_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CMXJetTob_v1>*)
   {
      ::DataVector<xAOD::CMXJetTob_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CMXJetTob_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CMXJetTob_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CMXJetTob_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCMXJetTob_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CMXJetTob_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCMXJetTob_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCMXJetTob_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCMXJetTob_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCMXJetTob_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCMXJetTob_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CMXJetTob_v1>","xAOD::CMXJetTobContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CMXJetTob_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CMXJetTob_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXJetTob_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCMXJetTob_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXJetTob_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCMXJetTob_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCMXJetTob_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","4116184D-A8AF-43B1-9990-9DF5C1010AB6");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXJetTobAuxContainer_v1_Dictionary();
   static void xAODcLcLCMXJetTobAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXJetTobAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXJetTobAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXJetTobAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCMXJetTobAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCMXJetTobAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXJetTobAuxContainer_v1*)
   {
      ::xAOD::CMXJetTobAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXJetTobAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXJetTobAuxContainer_v1", "xAODTrigL1Calo/versions/CMXJetTobAuxContainer_v1.h", 23,
                  typeid(::xAOD::CMXJetTobAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXJetTobAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXJetTobAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCMXJetTobAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXJetTobAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCMXJetTobAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXJetTobAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXJetTobAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXJetTobAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXJetTobAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXJetTobAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXJetTobAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXJetTobAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCMXJetTobAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXJetTobAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","094F5655-229B-49BE-870D-1E4D13866CED");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXEtSums_v1_Dictionary();
   static void xAODcLcLCMXEtSums_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXEtSums_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXEtSums_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXEtSums_v1(void *p);
   static void deleteArray_xAODcLcLCMXEtSums_v1(void *p);
   static void destruct_xAODcLcLCMXEtSums_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXEtSums_v1*)
   {
      ::xAOD::CMXEtSums_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXEtSums_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXEtSums_v1", "xAODTrigL1Calo/versions/CMXEtSums_v1.h", 22,
                  typeid(::xAOD::CMXEtSums_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXEtSums_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXEtSums_v1) );
      instance.SetNew(&new_xAODcLcLCMXEtSums_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXEtSums_v1);
      instance.SetDelete(&delete_xAODcLcLCMXEtSums_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXEtSums_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXEtSums_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXEtSums_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXEtSums_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXEtSums_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXEtSums_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXEtSums_v1*)0x0)->GetClass();
      xAODcLcLCMXEtSums_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXEtSums_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCMXEtSums_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCMXEtSums_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCMXEtSums_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCMXEtSums_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCMXEtSums_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCMXEtSums_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCMXEtSums_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CMXEtSums_v1>*)
   {
      ::DataVector<xAOD::CMXEtSums_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CMXEtSums_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CMXEtSums_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CMXEtSums_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCMXEtSums_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CMXEtSums_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCMXEtSums_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCMXEtSums_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCMXEtSums_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCMXEtSums_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCMXEtSums_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CMXEtSums_v1>","xAOD::CMXEtSumsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CMXEtSums_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CMXEtSums_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXEtSums_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCMXEtSums_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CMXEtSums_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCMXEtSums_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCMXEtSums_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","751F0DD4-FF7F-4950-9928-3198DA4B1AB4");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCMXEtSumsAuxContainer_v1_Dictionary();
   static void xAODcLcLCMXEtSumsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCMXEtSumsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCMXEtSumsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCMXEtSumsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCMXEtSumsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCMXEtSumsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CMXEtSumsAuxContainer_v1*)
   {
      ::xAOD::CMXEtSumsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CMXEtSumsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CMXEtSumsAuxContainer_v1", "xAODTrigL1Calo/versions/CMXEtSumsAuxContainer_v1.h", 23,
                  typeid(::xAOD::CMXEtSumsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCMXEtSumsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CMXEtSumsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCMXEtSumsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCMXEtSumsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCMXEtSumsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCMXEtSumsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCMXEtSumsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CMXEtSumsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CMXEtSumsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CMXEtSumsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCMXEtSumsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CMXEtSumsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCMXEtSumsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCMXEtSumsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B96297C3-769B-4084-9524-9D4D233B4896");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMTower_v2_Dictionary();
   static void xAODcLcLCPMTower_v2_TClassManip(TClass*);
   static void *new_xAODcLcLCPMTower_v2(void *p = 0);
   static void *newArray_xAODcLcLCPMTower_v2(Long_t size, void *p);
   static void delete_xAODcLcLCPMTower_v2(void *p);
   static void deleteArray_xAODcLcLCPMTower_v2(void *p);
   static void destruct_xAODcLcLCPMTower_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMTower_v2*)
   {
      ::xAOD::CPMTower_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMTower_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMTower_v2", "xAODTrigL1Calo/versions/CPMTower_v2.h", 21,
                  typeid(::xAOD::CPMTower_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMTower_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMTower_v2) );
      instance.SetNew(&new_xAODcLcLCPMTower_v2);
      instance.SetNewArray(&newArray_xAODcLcLCPMTower_v2);
      instance.SetDelete(&delete_xAODcLcLCPMTower_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMTower_v2);
      instance.SetDestructor(&destruct_xAODcLcLCPMTower_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMTower_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMTower_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMTower_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMTower_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMTower_v2*)0x0)->GetClass();
      xAODcLcLCPMTower_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMTower_v2_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCPMTower_v2gR_Dictionary();
   static void DataVectorlExAODcLcLCPMTower_v2gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCPMTower_v2gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCPMTower_v2gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCPMTower_v2gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCPMTower_v2gR(void *p);
   static void destruct_DataVectorlExAODcLcLCPMTower_v2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CPMTower_v2>*)
   {
      ::DataVector<xAOD::CPMTower_v2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CPMTower_v2>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CPMTower_v2>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CPMTower_v2>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCPMTower_v2gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CPMTower_v2>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCPMTower_v2gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCPMTower_v2gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCPMTower_v2gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCPMTower_v2gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCPMTower_v2gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CPMTower_v2>","xAOD::CPMTowerContainer_v2");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CPMTower_v2>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CPMTower_v2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMTower_v2>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCPMTower_v2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMTower_v2>*)0x0)->GetClass();
      DataVectorlExAODcLcLCPMTower_v2gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCPMTower_v2gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","87CC5511-E7E5-4068-8F40-F44B0A1E9A3F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMTowerAuxContainer_v2_Dictionary();
   static void xAODcLcLCPMTowerAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLCPMTowerAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLCPMTowerAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLCPMTowerAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLCPMTowerAuxContainer_v2(void *p);
   static void destruct_xAODcLcLCPMTowerAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMTowerAuxContainer_v2*)
   {
      ::xAOD::CPMTowerAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMTowerAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMTowerAuxContainer_v2", "xAODTrigL1Calo/versions/CPMTowerAuxContainer_v2.h", 23,
                  typeid(::xAOD::CPMTowerAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMTowerAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMTowerAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLCPMTowerAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLCPMTowerAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLCPMTowerAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMTowerAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLCPMTowerAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMTowerAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMTowerAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMTowerAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMTowerAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMTowerAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLCPMTowerAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMTowerAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E41859F4-0B8B-412B-A697-2B5A22C5B720");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMTobRoI_v1_Dictionary();
   static void xAODcLcLCPMTobRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCPMTobRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLCPMTobRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLCPMTobRoI_v1(void *p);
   static void deleteArray_xAODcLcLCPMTobRoI_v1(void *p);
   static void destruct_xAODcLcLCPMTobRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMTobRoI_v1*)
   {
      ::xAOD::CPMTobRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMTobRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMTobRoI_v1", "xAODTrigL1Calo/versions/CPMTobRoI_v1.h", 19,
                  typeid(::xAOD::CPMTobRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMTobRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMTobRoI_v1) );
      instance.SetNew(&new_xAODcLcLCPMTobRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLCPMTobRoI_v1);
      instance.SetDelete(&delete_xAODcLcLCPMTobRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMTobRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLCPMTobRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMTobRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMTobRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMTobRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMTobRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMTobRoI_v1*)0x0)->GetClass();
      xAODcLcLCPMTobRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMTobRoI_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCPMTobRoI_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCPMTobRoI_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCPMTobRoI_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCPMTobRoI_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCPMTobRoI_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCPMTobRoI_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCPMTobRoI_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CPMTobRoI_v1>*)
   {
      ::DataVector<xAOD::CPMTobRoI_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CPMTobRoI_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CPMTobRoI_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::CPMTobRoI_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCPMTobRoI_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CPMTobRoI_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCPMTobRoI_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCPMTobRoI_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCPMTobRoI_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCPMTobRoI_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCPMTobRoI_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CPMTobRoI_v1>","xAOD::CPMTobRoIContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CPMTobRoI_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CPMTobRoI_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMTobRoI_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCPMTobRoI_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CPMTobRoI_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCPMTobRoI_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCPMTobRoI_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B84B5967-8B6C-4743-AC18-FF68E13D9EA6");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCPMTobRoIAuxContainer_v1_Dictionary();
   static void xAODcLcLCPMTobRoIAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCPMTobRoIAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCPMTobRoIAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCPMTobRoIAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCPMTobRoIAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCPMTobRoIAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CPMTobRoIAuxContainer_v1*)
   {
      ::xAOD::CPMTobRoIAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CPMTobRoIAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CPMTobRoIAuxContainer_v1", "xAODTrigL1Calo/versions/CPMTobRoIAuxContainer_v1.h", 21,
                  typeid(::xAOD::CPMTobRoIAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCPMTobRoIAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CPMTobRoIAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCPMTobRoIAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCPMTobRoIAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCPMTobRoIAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCPMTobRoIAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCPMTobRoIAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CPMTobRoIAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CPMTobRoIAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CPMTobRoIAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCPMTobRoIAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CPMTobRoIAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCPMTobRoIAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCPMTobRoIAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","38CB660E-139B-4280-9517-B571CA680A37");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMEtSums_v2_Dictionary();
   static void xAODcLcLJEMEtSums_v2_TClassManip(TClass*);
   static void *new_xAODcLcLJEMEtSums_v2(void *p = 0);
   static void *newArray_xAODcLcLJEMEtSums_v2(Long_t size, void *p);
   static void delete_xAODcLcLJEMEtSums_v2(void *p);
   static void deleteArray_xAODcLcLJEMEtSums_v2(void *p);
   static void destruct_xAODcLcLJEMEtSums_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMEtSums_v2*)
   {
      ::xAOD::JEMEtSums_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMEtSums_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMEtSums_v2", "xAODTrigL1Calo/versions/JEMEtSums_v2.h", 21,
                  typeid(::xAOD::JEMEtSums_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMEtSums_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMEtSums_v2) );
      instance.SetNew(&new_xAODcLcLJEMEtSums_v2);
      instance.SetNewArray(&newArray_xAODcLcLJEMEtSums_v2);
      instance.SetDelete(&delete_xAODcLcLJEMEtSums_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMEtSums_v2);
      instance.SetDestructor(&destruct_xAODcLcLJEMEtSums_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMEtSums_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMEtSums_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMEtSums_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMEtSums_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMEtSums_v2*)0x0)->GetClass();
      xAODcLcLJEMEtSums_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMEtSums_v2_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJEMEtSums_v2gR_Dictionary();
   static void DataVectorlExAODcLcLJEMEtSums_v2gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJEMEtSums_v2gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJEMEtSums_v2gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJEMEtSums_v2gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJEMEtSums_v2gR(void *p);
   static void destruct_DataVectorlExAODcLcLJEMEtSums_v2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JEMEtSums_v2>*)
   {
      ::DataVector<xAOD::JEMEtSums_v2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JEMEtSums_v2>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JEMEtSums_v2>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JEMEtSums_v2>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJEMEtSums_v2gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JEMEtSums_v2>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJEMEtSums_v2gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJEMEtSums_v2gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJEMEtSums_v2gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJEMEtSums_v2gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJEMEtSums_v2gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JEMEtSums_v2>","xAOD::JEMEtSumsContainer_v2");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JEMEtSums_v2>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JEMEtSums_v2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMEtSums_v2>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJEMEtSums_v2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMEtSums_v2>*)0x0)->GetClass();
      DataVectorlExAODcLcLJEMEtSums_v2gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJEMEtSums_v2gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","9EE079C0-5737-4710-A81D-278D97F01E50");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMEtSumsAuxContainer_v2_Dictionary();
   static void xAODcLcLJEMEtSumsAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLJEMEtSumsAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLJEMEtSumsAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLJEMEtSumsAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLJEMEtSumsAuxContainer_v2(void *p);
   static void destruct_xAODcLcLJEMEtSumsAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMEtSumsAuxContainer_v2*)
   {
      ::xAOD::JEMEtSumsAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMEtSumsAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMEtSumsAuxContainer_v2", "xAODTrigL1Calo/versions/JEMEtSumsAuxContainer_v2.h", 23,
                  typeid(::xAOD::JEMEtSumsAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMEtSumsAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMEtSumsAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLJEMEtSumsAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLJEMEtSumsAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLJEMEtSumsAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMEtSumsAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLJEMEtSumsAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMEtSumsAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMEtSumsAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMEtSumsAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMEtSumsAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMEtSumsAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLJEMEtSumsAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMEtSumsAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","015D34AC-2FD5-4357-850E-04FD5EF6F945");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMTobRoI_v1_Dictionary();
   static void xAODcLcLJEMTobRoI_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJEMTobRoI_v1(void *p = 0);
   static void *newArray_xAODcLcLJEMTobRoI_v1(Long_t size, void *p);
   static void delete_xAODcLcLJEMTobRoI_v1(void *p);
   static void deleteArray_xAODcLcLJEMTobRoI_v1(void *p);
   static void destruct_xAODcLcLJEMTobRoI_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMTobRoI_v1*)
   {
      ::xAOD::JEMTobRoI_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMTobRoI_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMTobRoI_v1", "xAODTrigL1Calo/versions/JEMTobRoI_v1.h", 19,
                  typeid(::xAOD::JEMTobRoI_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMTobRoI_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMTobRoI_v1) );
      instance.SetNew(&new_xAODcLcLJEMTobRoI_v1);
      instance.SetNewArray(&newArray_xAODcLcLJEMTobRoI_v1);
      instance.SetDelete(&delete_xAODcLcLJEMTobRoI_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMTobRoI_v1);
      instance.SetDestructor(&destruct_xAODcLcLJEMTobRoI_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMTobRoI_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMTobRoI_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMTobRoI_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMTobRoI_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMTobRoI_v1*)0x0)->GetClass();
      xAODcLcLJEMTobRoI_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMTobRoI_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJEMTobRoI_v1gR_Dictionary();
   static void DataVectorlExAODcLcLJEMTobRoI_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJEMTobRoI_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJEMTobRoI_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJEMTobRoI_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJEMTobRoI_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLJEMTobRoI_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JEMTobRoI_v1>*)
   {
      ::DataVector<xAOD::JEMTobRoI_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JEMTobRoI_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JEMTobRoI_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JEMTobRoI_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJEMTobRoI_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JEMTobRoI_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJEMTobRoI_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJEMTobRoI_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJEMTobRoI_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJEMTobRoI_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJEMTobRoI_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JEMTobRoI_v1>","xAOD::JEMTobRoIContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JEMTobRoI_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JEMTobRoI_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMTobRoI_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJEMTobRoI_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JEMTobRoI_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLJEMTobRoI_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJEMTobRoI_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","2C7D0000-4D7B-45D4-8EE5-B18F172A70C3");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJEMTobRoIAuxContainer_v1_Dictionary();
   static void xAODcLcLJEMTobRoIAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJEMTobRoIAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLJEMTobRoIAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLJEMTobRoIAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLJEMTobRoIAuxContainer_v1(void *p);
   static void destruct_xAODcLcLJEMTobRoIAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JEMTobRoIAuxContainer_v1*)
   {
      ::xAOD::JEMTobRoIAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JEMTobRoIAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JEMTobRoIAuxContainer_v1", "xAODTrigL1Calo/versions/JEMTobRoIAuxContainer_v1.h", 21,
                  typeid(::xAOD::JEMTobRoIAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJEMTobRoIAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JEMTobRoIAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLJEMTobRoIAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLJEMTobRoIAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLJEMTobRoIAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJEMTobRoIAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLJEMTobRoIAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JEMTobRoIAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JEMTobRoIAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JEMTobRoIAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJEMTobRoIAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JEMTobRoIAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLJEMTobRoIAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJEMTobRoIAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","2EAA65B1-B1F3-4530-9413-E572D6282996");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetElement_v2_Dictionary();
   static void xAODcLcLJetElement_v2_TClassManip(TClass*);
   static void *new_xAODcLcLJetElement_v2(void *p = 0);
   static void *newArray_xAODcLcLJetElement_v2(Long_t size, void *p);
   static void delete_xAODcLcLJetElement_v2(void *p);
   static void deleteArray_xAODcLcLJetElement_v2(void *p);
   static void destruct_xAODcLcLJetElement_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetElement_v2*)
   {
      ::xAOD::JetElement_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetElement_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetElement_v2", "xAODTrigL1Calo/versions/JetElement_v2.h", 21,
                  typeid(::xAOD::JetElement_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetElement_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetElement_v2) );
      instance.SetNew(&new_xAODcLcLJetElement_v2);
      instance.SetNewArray(&newArray_xAODcLcLJetElement_v2);
      instance.SetDelete(&delete_xAODcLcLJetElement_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetElement_v2);
      instance.SetDestructor(&destruct_xAODcLcLJetElement_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetElement_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetElement_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetElement_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetElement_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetElement_v2*)0x0)->GetClass();
      xAODcLcLJetElement_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetElement_v2_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJetElement_v2gR_Dictionary();
   static void DataVectorlExAODcLcLJetElement_v2gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJetElement_v2gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJetElement_v2gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJetElement_v2gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJetElement_v2gR(void *p);
   static void destruct_DataVectorlExAODcLcLJetElement_v2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::JetElement_v2>*)
   {
      ::DataVector<xAOD::JetElement_v2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::JetElement_v2>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::JetElement_v2>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::JetElement_v2>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJetElement_v2gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::JetElement_v2>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJetElement_v2gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJetElement_v2gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJetElement_v2gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJetElement_v2gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJetElement_v2gR);

      ROOT::AddClassAlternate("DataVector<xAOD::JetElement_v2>","xAOD::JetElementContainer_v2");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::JetElement_v2>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::JetElement_v2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::JetElement_v2>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJetElement_v2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::JetElement_v2>*)0x0)->GetClass();
      DataVectorlExAODcLcLJetElement_v2gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJetElement_v2gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C4E1BE3D-6499-41A6-9643-4EA716049F60");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetElementAuxContainer_v2_Dictionary();
   static void xAODcLcLJetElementAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLJetElementAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLJetElementAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLJetElementAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLJetElementAuxContainer_v2(void *p);
   static void destruct_xAODcLcLJetElementAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetElementAuxContainer_v2*)
   {
      ::xAOD::JetElementAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetElementAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetElementAuxContainer_v2", "xAODTrigL1Calo/versions/JetElementAuxContainer_v2.h", 23,
                  typeid(::xAOD::JetElementAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetElementAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetElementAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLJetElementAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLJetElementAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLJetElementAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetElementAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLJetElementAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetElementAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetElementAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetElementAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetElementAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetElementAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLJetElementAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetElementAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","A52FADFE-E250-43EC-9766-2898288521BF");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL1TopoRawData_v1_Dictionary();
   static void xAODcLcLL1TopoRawData_v1_TClassManip(TClass*);
   static void *new_xAODcLcLL1TopoRawData_v1(void *p = 0);
   static void *newArray_xAODcLcLL1TopoRawData_v1(Long_t size, void *p);
   static void delete_xAODcLcLL1TopoRawData_v1(void *p);
   static void deleteArray_xAODcLcLL1TopoRawData_v1(void *p);
   static void destruct_xAODcLcLL1TopoRawData_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L1TopoRawData_v1*)
   {
      ::xAOD::L1TopoRawData_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L1TopoRawData_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L1TopoRawData_v1", "xAODTrigL1Calo/versions/L1TopoRawData_v1.h", 23,
                  typeid(::xAOD::L1TopoRawData_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLL1TopoRawData_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L1TopoRawData_v1) );
      instance.SetNew(&new_xAODcLcLL1TopoRawData_v1);
      instance.SetNewArray(&newArray_xAODcLcLL1TopoRawData_v1);
      instance.SetDelete(&delete_xAODcLcLL1TopoRawData_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL1TopoRawData_v1);
      instance.SetDestructor(&destruct_xAODcLcLL1TopoRawData_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L1TopoRawData_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::L1TopoRawData_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L1TopoRawData_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL1TopoRawData_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L1TopoRawData_v1*)0x0)->GetClass();
      xAODcLcLL1TopoRawData_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL1TopoRawData_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLL1TopoRawData_v1gR_Dictionary();
   static void DataVectorlExAODcLcLL1TopoRawData_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLL1TopoRawData_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLL1TopoRawData_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLL1TopoRawData_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLL1TopoRawData_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLL1TopoRawData_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::L1TopoRawData_v1>*)
   {
      ::DataVector<xAOD::L1TopoRawData_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::L1TopoRawData_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::L1TopoRawData_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::L1TopoRawData_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLL1TopoRawData_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::L1TopoRawData_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLL1TopoRawData_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLL1TopoRawData_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLL1TopoRawData_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLL1TopoRawData_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLL1TopoRawData_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::L1TopoRawData_v1>","xAOD::L1TopoRawDataContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::L1TopoRawData_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::L1TopoRawData_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::L1TopoRawData_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLL1TopoRawData_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::L1TopoRawData_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLL1TopoRawData_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLL1TopoRawData_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","59FD769F-86CA-4636-8AB0-61EB3E0482EA");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL1TopoRawDataAuxContainer_v1_Dictionary();
   static void xAODcLcLL1TopoRawDataAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLL1TopoRawDataAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLL1TopoRawDataAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLL1TopoRawDataAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLL1TopoRawDataAuxContainer_v1(void *p);
   static void destruct_xAODcLcLL1TopoRawDataAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L1TopoRawDataAuxContainer_v1*)
   {
      ::xAOD::L1TopoRawDataAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L1TopoRawDataAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L1TopoRawDataAuxContainer_v1", "xAODTrigL1Calo/versions/L1TopoRawDataAuxContainer_v1.h", 24,
                  typeid(::xAOD::L1TopoRawDataAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLL1TopoRawDataAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L1TopoRawDataAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLL1TopoRawDataAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLL1TopoRawDataAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLL1TopoRawDataAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL1TopoRawDataAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLL1TopoRawDataAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L1TopoRawDataAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::L1TopoRawDataAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L1TopoRawDataAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL1TopoRawDataAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L1TopoRawDataAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLL1TopoRawDataAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL1TopoRawDataAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E3D3AF1E-843A-4F21-A639-EE97040D3B6B");
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMHits_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMHits_v1 : new ::xAOD::JEMHits_v1;
   }
   static void *newArray_xAODcLcLJEMHits_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMHits_v1[nElements] : new ::xAOD::JEMHits_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMHits_v1(void *p) {
      delete ((::xAOD::JEMHits_v1*)p);
   }
   static void deleteArray_xAODcLcLJEMHits_v1(void *p) {
      delete [] ((::xAOD::JEMHits_v1*)p);
   }
   static void destruct_xAODcLcLJEMHits_v1(void *p) {
      typedef ::xAOD::JEMHits_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMHits_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJEMHits_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JEMHits_v1> : new ::DataVector<xAOD::JEMHits_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLJEMHits_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JEMHits_v1>[nElements] : new ::DataVector<xAOD::JEMHits_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJEMHits_v1gR(void *p) {
      delete ((::DataVector<xAOD::JEMHits_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJEMHits_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::JEMHits_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJEMHits_v1gR(void *p) {
      typedef ::DataVector<xAOD::JEMHits_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JEMHits_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMHitsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMHitsAuxContainer_v1 : new ::xAOD::JEMHitsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLJEMHitsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMHitsAuxContainer_v1[nElements] : new ::xAOD::JEMHitsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMHitsAuxContainer_v1(void *p) {
      delete ((::xAOD::JEMHitsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLJEMHitsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::JEMHitsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLJEMHitsAuxContainer_v1(void *p) {
      typedef ::xAOD::JEMHitsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMHitsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMEtSums_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMEtSums_v1 : new ::xAOD::JEMEtSums_v1;
   }
   static void *newArray_xAODcLcLJEMEtSums_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMEtSums_v1[nElements] : new ::xAOD::JEMEtSums_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMEtSums_v1(void *p) {
      delete ((::xAOD::JEMEtSums_v1*)p);
   }
   static void deleteArray_xAODcLcLJEMEtSums_v1(void *p) {
      delete [] ((::xAOD::JEMEtSums_v1*)p);
   }
   static void destruct_xAODcLcLJEMEtSums_v1(void *p) {
      typedef ::xAOD::JEMEtSums_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMEtSums_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJEMEtSums_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JEMEtSums_v1> : new ::DataVector<xAOD::JEMEtSums_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLJEMEtSums_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JEMEtSums_v1>[nElements] : new ::DataVector<xAOD::JEMEtSums_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJEMEtSums_v1gR(void *p) {
      delete ((::DataVector<xAOD::JEMEtSums_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJEMEtSums_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::JEMEtSums_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJEMEtSums_v1gR(void *p) {
      typedef ::DataVector<xAOD::JEMEtSums_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JEMEtSums_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMEtSumsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMEtSumsAuxContainer_v1 : new ::xAOD::JEMEtSumsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLJEMEtSumsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMEtSumsAuxContainer_v1[nElements] : new ::xAOD::JEMEtSumsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMEtSumsAuxContainer_v1(void *p) {
      delete ((::xAOD::JEMEtSumsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLJEMEtSumsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::JEMEtSumsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLJEMEtSumsAuxContainer_v1(void *p) {
      typedef ::xAOD::JEMEtSumsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMEtSumsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMRoI_v1 : new ::xAOD::JEMRoI_v1;
   }
   static void *newArray_xAODcLcLJEMRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMRoI_v1[nElements] : new ::xAOD::JEMRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMRoI_v1(void *p) {
      delete ((::xAOD::JEMRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLJEMRoI_v1(void *p) {
      delete [] ((::xAOD::JEMRoI_v1*)p);
   }
   static void destruct_xAODcLcLJEMRoI_v1(void *p) {
      typedef ::xAOD::JEMRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJEMRoI_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JEMRoI_v1> : new ::DataVector<xAOD::JEMRoI_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLJEMRoI_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JEMRoI_v1>[nElements] : new ::DataVector<xAOD::JEMRoI_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJEMRoI_v1gR(void *p) {
      delete ((::DataVector<xAOD::JEMRoI_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJEMRoI_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::JEMRoI_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJEMRoI_v1gR(void *p) {
      typedef ::DataVector<xAOD::JEMRoI_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JEMRoI_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMRoIAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMRoIAuxContainer_v1 : new ::xAOD::JEMRoIAuxContainer_v1;
   }
   static void *newArray_xAODcLcLJEMRoIAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMRoIAuxContainer_v1[nElements] : new ::xAOD::JEMRoIAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMRoIAuxContainer_v1(void *p) {
      delete ((::xAOD::JEMRoIAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLJEMRoIAuxContainer_v1(void *p) {
      delete [] ((::xAOD::JEMRoIAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLJEMRoIAuxContainer_v1(void *p) {
      typedef ::xAOD::JEMRoIAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMRoIAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMHits_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMHits_v1 : new ::xAOD::CPMHits_v1;
   }
   static void *newArray_xAODcLcLCPMHits_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMHits_v1[nElements] : new ::xAOD::CPMHits_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMHits_v1(void *p) {
      delete ((::xAOD::CPMHits_v1*)p);
   }
   static void deleteArray_xAODcLcLCPMHits_v1(void *p) {
      delete [] ((::xAOD::CPMHits_v1*)p);
   }
   static void destruct_xAODcLcLCPMHits_v1(void *p) {
      typedef ::xAOD::CPMHits_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMHits_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCPMHits_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CPMHits_v1> : new ::DataVector<xAOD::CPMHits_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCPMHits_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CPMHits_v1>[nElements] : new ::DataVector<xAOD::CPMHits_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCPMHits_v1gR(void *p) {
      delete ((::DataVector<xAOD::CPMHits_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCPMHits_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CPMHits_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCPMHits_v1gR(void *p) {
      typedef ::DataVector<xAOD::CPMHits_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CPMHits_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMHitsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMHitsAuxContainer_v1 : new ::xAOD::CPMHitsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCPMHitsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMHitsAuxContainer_v1[nElements] : new ::xAOD::CPMHitsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMHitsAuxContainer_v1(void *p) {
      delete ((::xAOD::CPMHitsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCPMHitsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CPMHitsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCPMHitsAuxContainer_v1(void *p) {
      typedef ::xAOD::CPMHitsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMHitsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMTower_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTower_v1 : new ::xAOD::CPMTower_v1;
   }
   static void *newArray_xAODcLcLCPMTower_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTower_v1[nElements] : new ::xAOD::CPMTower_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMTower_v1(void *p) {
      delete ((::xAOD::CPMTower_v1*)p);
   }
   static void deleteArray_xAODcLcLCPMTower_v1(void *p) {
      delete [] ((::xAOD::CPMTower_v1*)p);
   }
   static void destruct_xAODcLcLCPMTower_v1(void *p) {
      typedef ::xAOD::CPMTower_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMTower_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCPMTower_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CPMTower_v1> : new ::DataVector<xAOD::CPMTower_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCPMTower_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CPMTower_v1>[nElements] : new ::DataVector<xAOD::CPMTower_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCPMTower_v1gR(void *p) {
      delete ((::DataVector<xAOD::CPMTower_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCPMTower_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CPMTower_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCPMTower_v1gR(void *p) {
      typedef ::DataVector<xAOD::CPMTower_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CPMTower_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMTowerAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTowerAuxContainer_v1 : new ::xAOD::CPMTowerAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCPMTowerAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTowerAuxContainer_v1[nElements] : new ::xAOD::CPMTowerAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMTowerAuxContainer_v1(void *p) {
      delete ((::xAOD::CPMTowerAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCPMTowerAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CPMTowerAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCPMTowerAuxContainer_v1(void *p) {
      typedef ::xAOD::CPMTowerAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMTowerAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMRoI_v1 : new ::xAOD::CPMRoI_v1;
   }
   static void *newArray_xAODcLcLCPMRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMRoI_v1[nElements] : new ::xAOD::CPMRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMRoI_v1(void *p) {
      delete ((::xAOD::CPMRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLCPMRoI_v1(void *p) {
      delete [] ((::xAOD::CPMRoI_v1*)p);
   }
   static void destruct_xAODcLcLCPMRoI_v1(void *p) {
      typedef ::xAOD::CPMRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCPMRoI_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CPMRoI_v1> : new ::DataVector<xAOD::CPMRoI_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCPMRoI_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CPMRoI_v1>[nElements] : new ::DataVector<xAOD::CPMRoI_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCPMRoI_v1gR(void *p) {
      delete ((::DataVector<xAOD::CPMRoI_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCPMRoI_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CPMRoI_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCPMRoI_v1gR(void *p) {
      typedef ::DataVector<xAOD::CPMRoI_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CPMRoI_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMRoIAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMRoIAuxContainer_v1 : new ::xAOD::CPMRoIAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCPMRoIAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMRoIAuxContainer_v1[nElements] : new ::xAOD::CPMRoIAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMRoIAuxContainer_v1(void *p) {
      delete ((::xAOD::CPMRoIAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCPMRoIAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CPMRoIAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCPMRoIAuxContainer_v1(void *p) {
      typedef ::xAOD::CPMRoIAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMRoIAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMMCPHits_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMCPHits_v1 : new ::xAOD::CMMCPHits_v1;
   }
   static void *newArray_xAODcLcLCMMCPHits_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMCPHits_v1[nElements] : new ::xAOD::CMMCPHits_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMMCPHits_v1(void *p) {
      delete ((::xAOD::CMMCPHits_v1*)p);
   }
   static void deleteArray_xAODcLcLCMMCPHits_v1(void *p) {
      delete [] ((::xAOD::CMMCPHits_v1*)p);
   }
   static void destruct_xAODcLcLCMMCPHits_v1(void *p) {
      typedef ::xAOD::CMMCPHits_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMMCPHits_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCMMCPHits_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CMMCPHits_v1> : new ::DataVector<xAOD::CMMCPHits_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCMMCPHits_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CMMCPHits_v1>[nElements] : new ::DataVector<xAOD::CMMCPHits_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCMMCPHits_v1gR(void *p) {
      delete ((::DataVector<xAOD::CMMCPHits_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCMMCPHits_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CMMCPHits_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCMMCPHits_v1gR(void *p) {
      typedef ::DataVector<xAOD::CMMCPHits_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CMMCPHits_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMMCPHitsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMCPHitsAuxContainer_v1 : new ::xAOD::CMMCPHitsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCMMCPHitsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMCPHitsAuxContainer_v1[nElements] : new ::xAOD::CMMCPHitsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMMCPHitsAuxContainer_v1(void *p) {
      delete ((::xAOD::CMMCPHitsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCMMCPHitsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CMMCPHitsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCMMCPHitsAuxContainer_v1(void *p) {
      typedef ::xAOD::CMMCPHitsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMMCPHitsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMMEtSums_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMEtSums_v1 : new ::xAOD::CMMEtSums_v1;
   }
   static void *newArray_xAODcLcLCMMEtSums_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMEtSums_v1[nElements] : new ::xAOD::CMMEtSums_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMMEtSums_v1(void *p) {
      delete ((::xAOD::CMMEtSums_v1*)p);
   }
   static void deleteArray_xAODcLcLCMMEtSums_v1(void *p) {
      delete [] ((::xAOD::CMMEtSums_v1*)p);
   }
   static void destruct_xAODcLcLCMMEtSums_v1(void *p) {
      typedef ::xAOD::CMMEtSums_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMMEtSums_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCMMEtSums_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CMMEtSums_v1> : new ::DataVector<xAOD::CMMEtSums_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCMMEtSums_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CMMEtSums_v1>[nElements] : new ::DataVector<xAOD::CMMEtSums_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCMMEtSums_v1gR(void *p) {
      delete ((::DataVector<xAOD::CMMEtSums_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCMMEtSums_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CMMEtSums_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCMMEtSums_v1gR(void *p) {
      typedef ::DataVector<xAOD::CMMEtSums_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CMMEtSums_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMMEtSumsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMEtSumsAuxContainer_v1 : new ::xAOD::CMMEtSumsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCMMEtSumsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMEtSumsAuxContainer_v1[nElements] : new ::xAOD::CMMEtSumsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMMEtSumsAuxContainer_v1(void *p) {
      delete ((::xAOD::CMMEtSumsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCMMEtSumsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CMMEtSumsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCMMEtSumsAuxContainer_v1(void *p) {
      typedef ::xAOD::CMMEtSumsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMMEtSumsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMMJetHits_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMJetHits_v1 : new ::xAOD::CMMJetHits_v1;
   }
   static void *newArray_xAODcLcLCMMJetHits_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMJetHits_v1[nElements] : new ::xAOD::CMMJetHits_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMMJetHits_v1(void *p) {
      delete ((::xAOD::CMMJetHits_v1*)p);
   }
   static void deleteArray_xAODcLcLCMMJetHits_v1(void *p) {
      delete [] ((::xAOD::CMMJetHits_v1*)p);
   }
   static void destruct_xAODcLcLCMMJetHits_v1(void *p) {
      typedef ::xAOD::CMMJetHits_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMMJetHits_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCMMJetHits_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CMMJetHits_v1> : new ::DataVector<xAOD::CMMJetHits_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCMMJetHits_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CMMJetHits_v1>[nElements] : new ::DataVector<xAOD::CMMJetHits_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCMMJetHits_v1gR(void *p) {
      delete ((::DataVector<xAOD::CMMJetHits_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCMMJetHits_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CMMJetHits_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCMMJetHits_v1gR(void *p) {
      typedef ::DataVector<xAOD::CMMJetHits_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CMMJetHits_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMMJetHitsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMJetHitsAuxContainer_v1 : new ::xAOD::CMMJetHitsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCMMJetHitsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMJetHitsAuxContainer_v1[nElements] : new ::xAOD::CMMJetHitsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMMJetHitsAuxContainer_v1(void *p) {
      delete ((::xAOD::CMMJetHitsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCMMJetHitsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CMMJetHitsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCMMJetHitsAuxContainer_v1(void *p) {
      typedef ::xAOD::CMMJetHitsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMMJetHitsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMMRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMRoI_v1 : new ::xAOD::CMMRoI_v1;
   }
   static void *newArray_xAODcLcLCMMRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMRoI_v1[nElements] : new ::xAOD::CMMRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMMRoI_v1(void *p) {
      delete ((::xAOD::CMMRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLCMMRoI_v1(void *p) {
      delete [] ((::xAOD::CMMRoI_v1*)p);
   }
   static void destruct_xAODcLcLCMMRoI_v1(void *p) {
      typedef ::xAOD::CMMRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMMRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMMRoIAuxInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMRoIAuxInfo_v1 : new ::xAOD::CMMRoIAuxInfo_v1;
   }
   static void *newArray_xAODcLcLCMMRoIAuxInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMMRoIAuxInfo_v1[nElements] : new ::xAOD::CMMRoIAuxInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMMRoIAuxInfo_v1(void *p) {
      delete ((::xAOD::CMMRoIAuxInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLCMMRoIAuxInfo_v1(void *p) {
      delete [] ((::xAOD::CMMRoIAuxInfo_v1*)p);
   }
   static void destruct_xAODcLcLCMMRoIAuxInfo_v1(void *p) {
      typedef ::xAOD::CMMRoIAuxInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMMRoIAuxInfo_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetElement_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetElement_v1 : new ::xAOD::JetElement_v1;
   }
   static void *newArray_xAODcLcLJetElement_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetElement_v1[nElements] : new ::xAOD::JetElement_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetElement_v1(void *p) {
      delete ((::xAOD::JetElement_v1*)p);
   }
   static void deleteArray_xAODcLcLJetElement_v1(void *p) {
      delete [] ((::xAOD::JetElement_v1*)p);
   }
   static void destruct_xAODcLcLJetElement_v1(void *p) {
      typedef ::xAOD::JetElement_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetElement_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJetElement_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JetElement_v1> : new ::DataVector<xAOD::JetElement_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLJetElement_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JetElement_v1>[nElements] : new ::DataVector<xAOD::JetElement_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJetElement_v1gR(void *p) {
      delete ((::DataVector<xAOD::JetElement_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJetElement_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::JetElement_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJetElement_v1gR(void *p) {
      typedef ::DataVector<xAOD::JetElement_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JetElement_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetElementAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetElementAuxContainer_v1 : new ::xAOD::JetElementAuxContainer_v1;
   }
   static void *newArray_xAODcLcLJetElementAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetElementAuxContainer_v1[nElements] : new ::xAOD::JetElementAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetElementAuxContainer_v1(void *p) {
      delete ((::xAOD::JetElementAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLJetElementAuxContainer_v1(void *p) {
      delete [] ((::xAOD::JetElementAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLJetElementAuxContainer_v1(void *p) {
      typedef ::xAOD::JetElementAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetElementAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLRODHeader_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::RODHeader_v1 : new ::xAOD::RODHeader_v1;
   }
   static void *newArray_xAODcLcLRODHeader_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::RODHeader_v1[nElements] : new ::xAOD::RODHeader_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLRODHeader_v1(void *p) {
      delete ((::xAOD::RODHeader_v1*)p);
   }
   static void deleteArray_xAODcLcLRODHeader_v1(void *p) {
      delete [] ((::xAOD::RODHeader_v1*)p);
   }
   static void destruct_xAODcLcLRODHeader_v1(void *p) {
      typedef ::xAOD::RODHeader_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::RODHeader_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLRODHeader_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::RODHeader_v1> : new ::DataVector<xAOD::RODHeader_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLRODHeader_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::RODHeader_v1>[nElements] : new ::DataVector<xAOD::RODHeader_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLRODHeader_v1gR(void *p) {
      delete ((::DataVector<xAOD::RODHeader_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLRODHeader_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::RODHeader_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLRODHeader_v1gR(void *p) {
      typedef ::DataVector<xAOD::RODHeader_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::RODHeader_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLRODHeaderAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::RODHeaderAuxContainer_v1 : new ::xAOD::RODHeaderAuxContainer_v1;
   }
   static void *newArray_xAODcLcLRODHeaderAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::RODHeaderAuxContainer_v1[nElements] : new ::xAOD::RODHeaderAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLRODHeaderAuxContainer_v1(void *p) {
      delete ((::xAOD::RODHeaderAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLRODHeaderAuxContainer_v1(void *p) {
      delete [] ((::xAOD::RODHeaderAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLRODHeaderAuxContainer_v1(void *p) {
      typedef ::xAOD::RODHeaderAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::RODHeaderAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTriggerTower_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerTower_v1 : new ::xAOD::TriggerTower_v1;
   }
   static void *newArray_xAODcLcLTriggerTower_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerTower_v1[nElements] : new ::xAOD::TriggerTower_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTriggerTower_v1(void *p) {
      delete ((::xAOD::TriggerTower_v1*)p);
   }
   static void deleteArray_xAODcLcLTriggerTower_v1(void *p) {
      delete [] ((::xAOD::TriggerTower_v1*)p);
   }
   static void destruct_xAODcLcLTriggerTower_v1(void *p) {
      typedef ::xAOD::TriggerTower_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TriggerTower_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTriggerTower_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TriggerTower_v1> : new ::DataVector<xAOD::TriggerTower_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTriggerTower_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TriggerTower_v1>[nElements] : new ::DataVector<xAOD::TriggerTower_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTriggerTower_v1gR(void *p) {
      delete ((::DataVector<xAOD::TriggerTower_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTriggerTower_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TriggerTower_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTriggerTower_v1gR(void *p) {
      typedef ::DataVector<xAOD::TriggerTower_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TriggerTower_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTriggerTowerAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerTowerAuxContainer_v1 : new ::xAOD::TriggerTowerAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTriggerTowerAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerTowerAuxContainer_v1[nElements] : new ::xAOD::TriggerTowerAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTriggerTowerAuxContainer_v1(void *p) {
      delete ((::xAOD::TriggerTowerAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTriggerTowerAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TriggerTowerAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTriggerTowerAuxContainer_v1(void *p) {
      typedef ::xAOD::TriggerTowerAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TriggerTowerAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTriggerTower_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerTower_v2 : new ::xAOD::TriggerTower_v2;
   }
   static void *newArray_xAODcLcLTriggerTower_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerTower_v2[nElements] : new ::xAOD::TriggerTower_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTriggerTower_v2(void *p) {
      delete ((::xAOD::TriggerTower_v2*)p);
   }
   static void deleteArray_xAODcLcLTriggerTower_v2(void *p) {
      delete [] ((::xAOD::TriggerTower_v2*)p);
   }
   static void destruct_xAODcLcLTriggerTower_v2(void *p) {
      typedef ::xAOD::TriggerTower_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TriggerTower_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTriggerTower_v2gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TriggerTower_v2> : new ::DataVector<xAOD::TriggerTower_v2>;
   }
   static void *newArray_DataVectorlExAODcLcLTriggerTower_v2gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TriggerTower_v2>[nElements] : new ::DataVector<xAOD::TriggerTower_v2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTriggerTower_v2gR(void *p) {
      delete ((::DataVector<xAOD::TriggerTower_v2>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTriggerTower_v2gR(void *p) {
      delete [] ((::DataVector<xAOD::TriggerTower_v2>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTriggerTower_v2gR(void *p) {
      typedef ::DataVector<xAOD::TriggerTower_v2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TriggerTower_v2>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTriggerTowerAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerTowerAuxContainer_v2 : new ::xAOD::TriggerTowerAuxContainer_v2;
   }
   static void *newArray_xAODcLcLTriggerTowerAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TriggerTowerAuxContainer_v2[nElements] : new ::xAOD::TriggerTowerAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTriggerTowerAuxContainer_v2(void *p) {
      delete ((::xAOD::TriggerTowerAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLTriggerTowerAuxContainer_v2(void *p) {
      delete [] ((::xAOD::TriggerTowerAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLTriggerTowerAuxContainer_v2(void *p) {
      typedef ::xAOD::TriggerTowerAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TriggerTowerAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXCPHits_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXCPHits_v1 : new ::xAOD::CMXCPHits_v1;
   }
   static void *newArray_xAODcLcLCMXCPHits_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXCPHits_v1[nElements] : new ::xAOD::CMXCPHits_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXCPHits_v1(void *p) {
      delete ((::xAOD::CMXCPHits_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXCPHits_v1(void *p) {
      delete [] ((::xAOD::CMXCPHits_v1*)p);
   }
   static void destruct_xAODcLcLCMXCPHits_v1(void *p) {
      typedef ::xAOD::CMXCPHits_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXCPHits_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCMXCPHits_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CMXCPHits_v1> : new ::DataVector<xAOD::CMXCPHits_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCMXCPHits_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CMXCPHits_v1>[nElements] : new ::DataVector<xAOD::CMXCPHits_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCMXCPHits_v1gR(void *p) {
      delete ((::DataVector<xAOD::CMXCPHits_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCMXCPHits_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CMXCPHits_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCMXCPHits_v1gR(void *p) {
      typedef ::DataVector<xAOD::CMXCPHits_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CMXCPHits_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXCPHitsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXCPHitsAuxContainer_v1 : new ::xAOD::CMXCPHitsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCMXCPHitsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXCPHitsAuxContainer_v1[nElements] : new ::xAOD::CMXCPHitsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXCPHitsAuxContainer_v1(void *p) {
      delete ((::xAOD::CMXCPHitsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXCPHitsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CMXCPHitsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCMXCPHitsAuxContainer_v1(void *p) {
      typedef ::xAOD::CMXCPHitsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXCPHitsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXCPTob_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXCPTob_v1 : new ::xAOD::CMXCPTob_v1;
   }
   static void *newArray_xAODcLcLCMXCPTob_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXCPTob_v1[nElements] : new ::xAOD::CMXCPTob_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXCPTob_v1(void *p) {
      delete ((::xAOD::CMXCPTob_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXCPTob_v1(void *p) {
      delete [] ((::xAOD::CMXCPTob_v1*)p);
   }
   static void destruct_xAODcLcLCMXCPTob_v1(void *p) {
      typedef ::xAOD::CMXCPTob_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXCPTob_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCMXCPTob_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CMXCPTob_v1> : new ::DataVector<xAOD::CMXCPTob_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCMXCPTob_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CMXCPTob_v1>[nElements] : new ::DataVector<xAOD::CMXCPTob_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCMXCPTob_v1gR(void *p) {
      delete ((::DataVector<xAOD::CMXCPTob_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCMXCPTob_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CMXCPTob_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCMXCPTob_v1gR(void *p) {
      typedef ::DataVector<xAOD::CMXCPTob_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CMXCPTob_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXCPTobAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXCPTobAuxContainer_v1 : new ::xAOD::CMXCPTobAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCMXCPTobAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXCPTobAuxContainer_v1[nElements] : new ::xAOD::CMXCPTobAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXCPTobAuxContainer_v1(void *p) {
      delete ((::xAOD::CMXCPTobAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXCPTobAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CMXCPTobAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCMXCPTobAuxContainer_v1(void *p) {
      typedef ::xAOD::CMXCPTobAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXCPTobAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXJetHits_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXJetHits_v1 : new ::xAOD::CMXJetHits_v1;
   }
   static void *newArray_xAODcLcLCMXJetHits_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXJetHits_v1[nElements] : new ::xAOD::CMXJetHits_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXJetHits_v1(void *p) {
      delete ((::xAOD::CMXJetHits_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXJetHits_v1(void *p) {
      delete [] ((::xAOD::CMXJetHits_v1*)p);
   }
   static void destruct_xAODcLcLCMXJetHits_v1(void *p) {
      typedef ::xAOD::CMXJetHits_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXJetHits_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCMXJetHits_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CMXJetHits_v1> : new ::DataVector<xAOD::CMXJetHits_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCMXJetHits_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CMXJetHits_v1>[nElements] : new ::DataVector<xAOD::CMXJetHits_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCMXJetHits_v1gR(void *p) {
      delete ((::DataVector<xAOD::CMXJetHits_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCMXJetHits_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CMXJetHits_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCMXJetHits_v1gR(void *p) {
      typedef ::DataVector<xAOD::CMXJetHits_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CMXJetHits_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXJetHitsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXJetHitsAuxContainer_v1 : new ::xAOD::CMXJetHitsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCMXJetHitsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXJetHitsAuxContainer_v1[nElements] : new ::xAOD::CMXJetHitsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXJetHitsAuxContainer_v1(void *p) {
      delete ((::xAOD::CMXJetHitsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXJetHitsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CMXJetHitsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCMXJetHitsAuxContainer_v1(void *p) {
      typedef ::xAOD::CMXJetHitsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXJetHitsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXJetTob_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXJetTob_v1 : new ::xAOD::CMXJetTob_v1;
   }
   static void *newArray_xAODcLcLCMXJetTob_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXJetTob_v1[nElements] : new ::xAOD::CMXJetTob_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXJetTob_v1(void *p) {
      delete ((::xAOD::CMXJetTob_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXJetTob_v1(void *p) {
      delete [] ((::xAOD::CMXJetTob_v1*)p);
   }
   static void destruct_xAODcLcLCMXJetTob_v1(void *p) {
      typedef ::xAOD::CMXJetTob_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXJetTob_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCMXJetTob_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CMXJetTob_v1> : new ::DataVector<xAOD::CMXJetTob_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCMXJetTob_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CMXJetTob_v1>[nElements] : new ::DataVector<xAOD::CMXJetTob_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCMXJetTob_v1gR(void *p) {
      delete ((::DataVector<xAOD::CMXJetTob_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCMXJetTob_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CMXJetTob_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCMXJetTob_v1gR(void *p) {
      typedef ::DataVector<xAOD::CMXJetTob_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CMXJetTob_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXJetTobAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXJetTobAuxContainer_v1 : new ::xAOD::CMXJetTobAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCMXJetTobAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXJetTobAuxContainer_v1[nElements] : new ::xAOD::CMXJetTobAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXJetTobAuxContainer_v1(void *p) {
      delete ((::xAOD::CMXJetTobAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXJetTobAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CMXJetTobAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCMXJetTobAuxContainer_v1(void *p) {
      typedef ::xAOD::CMXJetTobAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXJetTobAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXEtSums_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXEtSums_v1 : new ::xAOD::CMXEtSums_v1;
   }
   static void *newArray_xAODcLcLCMXEtSums_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXEtSums_v1[nElements] : new ::xAOD::CMXEtSums_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXEtSums_v1(void *p) {
      delete ((::xAOD::CMXEtSums_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXEtSums_v1(void *p) {
      delete [] ((::xAOD::CMXEtSums_v1*)p);
   }
   static void destruct_xAODcLcLCMXEtSums_v1(void *p) {
      typedef ::xAOD::CMXEtSums_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXEtSums_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCMXEtSums_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CMXEtSums_v1> : new ::DataVector<xAOD::CMXEtSums_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCMXEtSums_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CMXEtSums_v1>[nElements] : new ::DataVector<xAOD::CMXEtSums_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCMXEtSums_v1gR(void *p) {
      delete ((::DataVector<xAOD::CMXEtSums_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCMXEtSums_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CMXEtSums_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCMXEtSums_v1gR(void *p) {
      typedef ::DataVector<xAOD::CMXEtSums_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CMXEtSums_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCMXEtSumsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXEtSumsAuxContainer_v1 : new ::xAOD::CMXEtSumsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCMXEtSumsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CMXEtSumsAuxContainer_v1[nElements] : new ::xAOD::CMXEtSumsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCMXEtSumsAuxContainer_v1(void *p) {
      delete ((::xAOD::CMXEtSumsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCMXEtSumsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CMXEtSumsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCMXEtSumsAuxContainer_v1(void *p) {
      typedef ::xAOD::CMXEtSumsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CMXEtSumsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMTower_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTower_v2 : new ::xAOD::CPMTower_v2;
   }
   static void *newArray_xAODcLcLCPMTower_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTower_v2[nElements] : new ::xAOD::CPMTower_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMTower_v2(void *p) {
      delete ((::xAOD::CPMTower_v2*)p);
   }
   static void deleteArray_xAODcLcLCPMTower_v2(void *p) {
      delete [] ((::xAOD::CPMTower_v2*)p);
   }
   static void destruct_xAODcLcLCPMTower_v2(void *p) {
      typedef ::xAOD::CPMTower_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMTower_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCPMTower_v2gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CPMTower_v2> : new ::DataVector<xAOD::CPMTower_v2>;
   }
   static void *newArray_DataVectorlExAODcLcLCPMTower_v2gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CPMTower_v2>[nElements] : new ::DataVector<xAOD::CPMTower_v2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCPMTower_v2gR(void *p) {
      delete ((::DataVector<xAOD::CPMTower_v2>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCPMTower_v2gR(void *p) {
      delete [] ((::DataVector<xAOD::CPMTower_v2>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCPMTower_v2gR(void *p) {
      typedef ::DataVector<xAOD::CPMTower_v2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CPMTower_v2>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMTowerAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTowerAuxContainer_v2 : new ::xAOD::CPMTowerAuxContainer_v2;
   }
   static void *newArray_xAODcLcLCPMTowerAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTowerAuxContainer_v2[nElements] : new ::xAOD::CPMTowerAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMTowerAuxContainer_v2(void *p) {
      delete ((::xAOD::CPMTowerAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLCPMTowerAuxContainer_v2(void *p) {
      delete [] ((::xAOD::CPMTowerAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLCPMTowerAuxContainer_v2(void *p) {
      typedef ::xAOD::CPMTowerAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMTowerAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMTobRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTobRoI_v1 : new ::xAOD::CPMTobRoI_v1;
   }
   static void *newArray_xAODcLcLCPMTobRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTobRoI_v1[nElements] : new ::xAOD::CPMTobRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMTobRoI_v1(void *p) {
      delete ((::xAOD::CPMTobRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLCPMTobRoI_v1(void *p) {
      delete [] ((::xAOD::CPMTobRoI_v1*)p);
   }
   static void destruct_xAODcLcLCPMTobRoI_v1(void *p) {
      typedef ::xAOD::CPMTobRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMTobRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCPMTobRoI_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CPMTobRoI_v1> : new ::DataVector<xAOD::CPMTobRoI_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCPMTobRoI_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CPMTobRoI_v1>[nElements] : new ::DataVector<xAOD::CPMTobRoI_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCPMTobRoI_v1gR(void *p) {
      delete ((::DataVector<xAOD::CPMTobRoI_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCPMTobRoI_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CPMTobRoI_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCPMTobRoI_v1gR(void *p) {
      typedef ::DataVector<xAOD::CPMTobRoI_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CPMTobRoI_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCPMTobRoIAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTobRoIAuxContainer_v1 : new ::xAOD::CPMTobRoIAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCPMTobRoIAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CPMTobRoIAuxContainer_v1[nElements] : new ::xAOD::CPMTobRoIAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCPMTobRoIAuxContainer_v1(void *p) {
      delete ((::xAOD::CPMTobRoIAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCPMTobRoIAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CPMTobRoIAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCPMTobRoIAuxContainer_v1(void *p) {
      typedef ::xAOD::CPMTobRoIAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CPMTobRoIAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMEtSums_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMEtSums_v2 : new ::xAOD::JEMEtSums_v2;
   }
   static void *newArray_xAODcLcLJEMEtSums_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMEtSums_v2[nElements] : new ::xAOD::JEMEtSums_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMEtSums_v2(void *p) {
      delete ((::xAOD::JEMEtSums_v2*)p);
   }
   static void deleteArray_xAODcLcLJEMEtSums_v2(void *p) {
      delete [] ((::xAOD::JEMEtSums_v2*)p);
   }
   static void destruct_xAODcLcLJEMEtSums_v2(void *p) {
      typedef ::xAOD::JEMEtSums_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMEtSums_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJEMEtSums_v2gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JEMEtSums_v2> : new ::DataVector<xAOD::JEMEtSums_v2>;
   }
   static void *newArray_DataVectorlExAODcLcLJEMEtSums_v2gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JEMEtSums_v2>[nElements] : new ::DataVector<xAOD::JEMEtSums_v2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJEMEtSums_v2gR(void *p) {
      delete ((::DataVector<xAOD::JEMEtSums_v2>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJEMEtSums_v2gR(void *p) {
      delete [] ((::DataVector<xAOD::JEMEtSums_v2>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJEMEtSums_v2gR(void *p) {
      typedef ::DataVector<xAOD::JEMEtSums_v2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JEMEtSums_v2>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMEtSumsAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMEtSumsAuxContainer_v2 : new ::xAOD::JEMEtSumsAuxContainer_v2;
   }
   static void *newArray_xAODcLcLJEMEtSumsAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMEtSumsAuxContainer_v2[nElements] : new ::xAOD::JEMEtSumsAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMEtSumsAuxContainer_v2(void *p) {
      delete ((::xAOD::JEMEtSumsAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLJEMEtSumsAuxContainer_v2(void *p) {
      delete [] ((::xAOD::JEMEtSumsAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLJEMEtSumsAuxContainer_v2(void *p) {
      typedef ::xAOD::JEMEtSumsAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMEtSumsAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMTobRoI_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMTobRoI_v1 : new ::xAOD::JEMTobRoI_v1;
   }
   static void *newArray_xAODcLcLJEMTobRoI_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMTobRoI_v1[nElements] : new ::xAOD::JEMTobRoI_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMTobRoI_v1(void *p) {
      delete ((::xAOD::JEMTobRoI_v1*)p);
   }
   static void deleteArray_xAODcLcLJEMTobRoI_v1(void *p) {
      delete [] ((::xAOD::JEMTobRoI_v1*)p);
   }
   static void destruct_xAODcLcLJEMTobRoI_v1(void *p) {
      typedef ::xAOD::JEMTobRoI_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMTobRoI_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJEMTobRoI_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JEMTobRoI_v1> : new ::DataVector<xAOD::JEMTobRoI_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLJEMTobRoI_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JEMTobRoI_v1>[nElements] : new ::DataVector<xAOD::JEMTobRoI_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJEMTobRoI_v1gR(void *p) {
      delete ((::DataVector<xAOD::JEMTobRoI_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJEMTobRoI_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::JEMTobRoI_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJEMTobRoI_v1gR(void *p) {
      typedef ::DataVector<xAOD::JEMTobRoI_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JEMTobRoI_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJEMTobRoIAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMTobRoIAuxContainer_v1 : new ::xAOD::JEMTobRoIAuxContainer_v1;
   }
   static void *newArray_xAODcLcLJEMTobRoIAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JEMTobRoIAuxContainer_v1[nElements] : new ::xAOD::JEMTobRoIAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJEMTobRoIAuxContainer_v1(void *p) {
      delete ((::xAOD::JEMTobRoIAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLJEMTobRoIAuxContainer_v1(void *p) {
      delete [] ((::xAOD::JEMTobRoIAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLJEMTobRoIAuxContainer_v1(void *p) {
      typedef ::xAOD::JEMTobRoIAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JEMTobRoIAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetElement_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetElement_v2 : new ::xAOD::JetElement_v2;
   }
   static void *newArray_xAODcLcLJetElement_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetElement_v2[nElements] : new ::xAOD::JetElement_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetElement_v2(void *p) {
      delete ((::xAOD::JetElement_v2*)p);
   }
   static void deleteArray_xAODcLcLJetElement_v2(void *p) {
      delete [] ((::xAOD::JetElement_v2*)p);
   }
   static void destruct_xAODcLcLJetElement_v2(void *p) {
      typedef ::xAOD::JetElement_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetElement_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJetElement_v2gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::JetElement_v2> : new ::DataVector<xAOD::JetElement_v2>;
   }
   static void *newArray_DataVectorlExAODcLcLJetElement_v2gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::JetElement_v2>[nElements] : new ::DataVector<xAOD::JetElement_v2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJetElement_v2gR(void *p) {
      delete ((::DataVector<xAOD::JetElement_v2>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJetElement_v2gR(void *p) {
      delete [] ((::DataVector<xAOD::JetElement_v2>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJetElement_v2gR(void *p) {
      typedef ::DataVector<xAOD::JetElement_v2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::JetElement_v2>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetElementAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetElementAuxContainer_v2 : new ::xAOD::JetElementAuxContainer_v2;
   }
   static void *newArray_xAODcLcLJetElementAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetElementAuxContainer_v2[nElements] : new ::xAOD::JetElementAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetElementAuxContainer_v2(void *p) {
      delete ((::xAOD::JetElementAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLJetElementAuxContainer_v2(void *p) {
      delete [] ((::xAOD::JetElementAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLJetElementAuxContainer_v2(void *p) {
      typedef ::xAOD::JetElementAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetElementAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL1TopoRawData_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L1TopoRawData_v1 : new ::xAOD::L1TopoRawData_v1;
   }
   static void *newArray_xAODcLcLL1TopoRawData_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L1TopoRawData_v1[nElements] : new ::xAOD::L1TopoRawData_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL1TopoRawData_v1(void *p) {
      delete ((::xAOD::L1TopoRawData_v1*)p);
   }
   static void deleteArray_xAODcLcLL1TopoRawData_v1(void *p) {
      delete [] ((::xAOD::L1TopoRawData_v1*)p);
   }
   static void destruct_xAODcLcLL1TopoRawData_v1(void *p) {
      typedef ::xAOD::L1TopoRawData_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L1TopoRawData_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLL1TopoRawData_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::L1TopoRawData_v1> : new ::DataVector<xAOD::L1TopoRawData_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLL1TopoRawData_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::L1TopoRawData_v1>[nElements] : new ::DataVector<xAOD::L1TopoRawData_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLL1TopoRawData_v1gR(void *p) {
      delete ((::DataVector<xAOD::L1TopoRawData_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLL1TopoRawData_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::L1TopoRawData_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLL1TopoRawData_v1gR(void *p) {
      typedef ::DataVector<xAOD::L1TopoRawData_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::L1TopoRawData_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL1TopoRawDataAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L1TopoRawDataAuxContainer_v1 : new ::xAOD::L1TopoRawDataAuxContainer_v1;
   }
   static void *newArray_xAODcLcLL1TopoRawDataAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L1TopoRawDataAuxContainer_v1[nElements] : new ::xAOD::L1TopoRawDataAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL1TopoRawDataAuxContainer_v1(void *p) {
      delete ((::xAOD::L1TopoRawDataAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLL1TopoRawDataAuxContainer_v1(void *p) {
      delete [] ((::xAOD::L1TopoRawDataAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLL1TopoRawDataAuxContainer_v1(void *p) {
      typedef ::xAOD::L1TopoRawDataAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L1TopoRawDataAuxContainer_v1

namespace {
  void TriggerDictionaryInitialization_xAODTrigL1Calo_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigL1Calo/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigL1Calo",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigL1Calo/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMHits_v1.h")))  JEMHits_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@CE2C5182-53DD-46E0-9DFA-61C214A8FCE5)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMHitsAuxContainer_v1.h")))  JEMHitsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMEtSums_v1.h")))  JEMEtSums_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@4D3C658D-597A-414E-AAA3-BE61A1D664C3)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMEtSumsAuxContainer_v1.h")))  JEMEtSumsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMRoI_v1.h")))  JEMRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@959C24E5-9746-4C70-83FA-A0140C710039)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMRoIAuxContainer_v1.h")))  JEMRoIAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMHits_v1.h")))  CPMHits_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@28FCEB20-A9DC-4FED-9FDD-00F46948E92E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMHitsAuxContainer_v1.h")))  CPMHitsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMTower_v1.h")))  CPMTower_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@529B6BD7-7B7D-438C-94D7-BAA49A022C47)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMTowerAuxContainer_v1.h")))  CPMTowerAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMRoI_v1.h")))  CPMRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@8FA92C8D-214F-4B8F-84CA-CB7267CAD0F2)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMRoIAuxContainer_v1.h")))  CPMRoIAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMMCPHits_v1.h")))  CMMCPHits_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@1951D4B4-D471-4256-8F0D-D666043E2889)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMMCPHitsAuxContainer_v1.h")))  CMMCPHitsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMMEtSums_v1.h")))  CMMEtSums_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@A072EDD3-880D-46D0-A120-48C116FDB14E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMMEtSumsAuxContainer_v1.h")))  CMMEtSumsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMMJetHits_v1.h")))  CMMJetHits_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B3C8D000-0E7F-4A0D-8F05-6DA36FFC9DD7)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMMJetHitsAuxContainer_v1.h")))  CMMJetHitsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@2BA7B6B2-69C6-40E8-8D2A-0398371EDC81)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMMRoI_v1.h")))  CMMRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B0CAB5B5-6B6B-437D-B353-1397C1F01B7E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMMRoIAuxInfo_v1.h")))  CMMRoIAuxInfo_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JetElement_v1.h")))  JetElement_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@7A0A2307-5729-4306-B520-4F75BBCC8E76)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JetElementAuxContainer_v1.h")))  JetElementAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/RODHeader_v1.h")))  RODHeader_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@6524E46D-CC9C-43D2-BAE0-55310A6B316C)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/RODHeaderAuxContainer_v1.h")))  RODHeaderAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/TriggerTower_v1.h")))  TriggerTower_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@3C5D20CF-6F77-4E67-B182-674EDFD2FE38)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/TriggerTowerAuxContainer_v1.h")))  TriggerTowerAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/TriggerTower_v2.h")))  TriggerTower_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/TriggerTower_v2.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@07FF691A-A920-43B2-A4E7-637D335E6929)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/TriggerTowerAuxContainer_v2.h")))  TriggerTowerAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXCPHits_v1.h")))  CMXCPHits_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@F3F8A07D-536B-42EB-9792-E08DB1F76C1F)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXCPHitsAuxContainer_v1.h")))  CMXCPHitsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXCPTob_v1.h")))  CMXCPTob_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B4A55AC8-3D30-42AF-B954-82DA7D8A07CB)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXCPTobAuxContainer_v1.h")))  CMXCPTobAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXJetHits_v1.h")))  CMXJetHits_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@509DB371-6A95-4DE1-B950-751B99EBF5EC)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXJetHitsAuxContainer_v1.h")))  CMXJetHitsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXJetTob_v1.h")))  CMXJetTob_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@094F5655-229B-49BE-870D-1E4D13866CED)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXJetTobAuxContainer_v1.h")))  CMXJetTobAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXEtSums_v1.h")))  CMXEtSums_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B96297C3-769B-4084-9524-9D4D233B4896)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CMXEtSumsAuxContainer_v1.h")))  CMXEtSumsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMTower_v2.h")))  CPMTower_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@E41859F4-0B8B-412B-A697-2B5A22C5B720)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMTowerAuxContainer_v2.h")))  CPMTowerAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMTobRoI_v1.h")))  CPMTobRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@38CB660E-139B-4280-9517-B571CA680A37)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/CPMTobRoIAuxContainer_v1.h")))  CPMTobRoIAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMEtSums_v2.h")))  JEMEtSums_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@015D34AC-2FD5-4357-850E-04FD5EF6F945)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMEtSumsAuxContainer_v2.h")))  JEMEtSumsAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMTobRoI_v1.h")))  JEMTobRoI_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@2EAA65B1-B1F3-4530-9413-E572D6282996)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JEMTobRoIAuxContainer_v1.h")))  JEMTobRoIAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JetElement_v2.h")))  JetElement_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@A52FADFE-E250-43EC-9766-2898288521BF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/JetElementAuxContainer_v2.h")))  JetElementAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/L1TopoRawData_v1.h")))  L1TopoRawData_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@E3D3AF1E-843A-4F21-A639-EE97040D3B6B)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigL1Calo/versions/L1TopoRawDataAuxContainer_v1.h")))  L1TopoRawDataAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigL1Calo"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTrigL1CaloDict.h 652824 2015-03-09 23:24:32Z morrisj $
#ifndef XAODTRIGL1CALO_XAODTRIGL1CALOCALOEVENTDICT_H
#define XAODTRIGL1CALO_XAODTRIGL1CALOCALOEVENTDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
// Run 1
#include "xAODTrigL1Calo/versions/JEMHits_v1.h"
#include "xAODTrigL1Calo/versions/JEMHitsContainer_v1.h"
#include "xAODTrigL1Calo/versions/JEMHitsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/JEMEtSums_v1.h"
#include "xAODTrigL1Calo/versions/JEMEtSumsContainer_v1.h"
#include "xAODTrigL1Calo/versions/JEMEtSumsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/JEMRoI_v1.h"
#include "xAODTrigL1Calo/versions/JEMRoIContainer_v1.h"
#include "xAODTrigL1Calo/versions/JEMRoIAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CPMHits_v1.h"
#include "xAODTrigL1Calo/versions/CPMHitsContainer_v1.h"
#include "xAODTrigL1Calo/versions/CPMHitsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CPMTower_v1.h"
#include "xAODTrigL1Calo/versions/CPMTowerContainer_v1.h"
#include "xAODTrigL1Calo/versions/CPMTowerAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CPMRoI_v1.h"
#include "xAODTrigL1Calo/versions/CPMRoIContainer_v1.h"
#include "xAODTrigL1Calo/versions/CPMRoIAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CMMCPHits_v1.h"
#include "xAODTrigL1Calo/versions/CMMCPHitsContainer_v1.h"
#include "xAODTrigL1Calo/versions/CMMCPHitsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CMMEtSums_v1.h"
#include "xAODTrigL1Calo/versions/CMMEtSumsContainer_v1.h"
#include "xAODTrigL1Calo/versions/CMMEtSumsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CMMJetHits_v1.h"
#include "xAODTrigL1Calo/versions/CMMJetHitsContainer_v1.h"
#include "xAODTrigL1Calo/versions/CMMJetHitsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CMMRoI_v1.h"
#include "xAODTrigL1Calo/versions/CMMRoIAuxInfo_v1.h"

#include "xAODTrigL1Calo/versions/JetElement_v1.h"
#include "xAODTrigL1Calo/versions/JetElementContainer_v1.h"
#include "xAODTrigL1Calo/versions/JetElementAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/RODHeader_v1.h"
#include "xAODTrigL1Calo/versions/RODHeaderContainer_v1.h"
#include "xAODTrigL1Calo/versions/RODHeaderAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/TriggerTower_v1.h"
#include "xAODTrigL1Calo/versions/TriggerTowerContainer_v1.h"
#include "xAODTrigL1Calo/versions/TriggerTowerAuxContainer_v1.h"

// Run 2
#include "xAODTrigL1Calo/versions/TriggerTower_v2.h"
#include "xAODTrigL1Calo/versions/TriggerTowerContainer_v2.h"
#include "xAODTrigL1Calo/versions/TriggerTowerAuxContainer_v2.h"

#include "xAODTrigL1Calo/versions/CMXCPHits_v1.h"
#include "xAODTrigL1Calo/versions/CMXCPHitsContainer_v1.h"
#include "xAODTrigL1Calo/versions/CMXCPHitsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CMXCPTob_v1.h"
#include "xAODTrigL1Calo/versions/CMXCPTobContainer_v1.h"
#include "xAODTrigL1Calo/versions/CMXCPTobAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CMXJetHits_v1.h"
#include "xAODTrigL1Calo/versions/CMXJetHitsContainer_v1.h"
#include "xAODTrigL1Calo/versions/CMXJetHitsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CMXJetTob_v1.h"
#include "xAODTrigL1Calo/versions/CMXJetTobContainer_v1.h"
#include "xAODTrigL1Calo/versions/CMXJetTobAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CMXEtSums_v1.h"
#include "xAODTrigL1Calo/versions/CMXEtSumsContainer_v1.h"
#include "xAODTrigL1Calo/versions/CMXEtSumsAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/CPMTower_v2.h"
#include "xAODTrigL1Calo/versions/CPMTowerContainer_v2.h"
#include "xAODTrigL1Calo/versions/CPMTowerAuxContainer_v2.h"

#include "xAODTrigL1Calo/versions/CPMTobRoI_v1.h"
#include "xAODTrigL1Calo/versions/CPMTobRoIContainer_v1.h"
#include "xAODTrigL1Calo/versions/CPMTobRoIAuxContainer_v1.h"


#include "xAODTrigL1Calo/versions/JEMEtSums_v2.h"
#include "xAODTrigL1Calo/versions/JEMEtSumsContainer_v2.h"
#include "xAODTrigL1Calo/versions/JEMEtSumsAuxContainer_v2.h"

#include "xAODTrigL1Calo/versions/JEMTobRoI_v1.h"
#include "xAODTrigL1Calo/versions/JEMTobRoIContainer_v1.h"
#include "xAODTrigL1Calo/versions/JEMTobRoIAuxContainer_v1.h"

#include "xAODTrigL1Calo/versions/JetElement_v2.h"
#include "xAODTrigL1Calo/versions/JetElementContainer_v2.h"
#include "xAODTrigL1Calo/versions/JetElementAuxContainer_v2.h"

#include "xAODTrigL1Calo/versions/L1TopoRawData_v1.h"
#include "xAODTrigL1Calo/versions/L1TopoRawDataContainer_v1.h"
#include "xAODTrigL1Calo/versions/L1TopoRawDataAuxContainer_v1.h"


// Versionless AuxContainers
#include "xAODTrigL1Calo/CMMCPHitsAuxContainer.h"                                                                                                                                                 
#include "xAODTrigL1Calo/CMMEtSumsAuxContainer.h"                                                                                                                                                 
#include "xAODTrigL1Calo/CMMJetHitsAuxContainer.h"                                                                                                                                                
#include "xAODTrigL1Calo/CMMRoIAuxInfo.h"
#include "xAODTrigL1Calo/CMXCPHitsAuxContainer.h"
#include "xAODTrigL1Calo/CMXCPTobAuxContainer.h"
#include "xAODTrigL1Calo/CMXEtSumsAuxContainer.h"
#include "xAODTrigL1Calo/CMXJetHitsAuxContainer.h"
#include "xAODTrigL1Calo/CMXJetTobAuxContainer.h"
#include "xAODTrigL1Calo/CPMHitsAuxContainer.h"
#include "xAODTrigL1Calo/CPMRoIAuxContainer.h"
#include "xAODTrigL1Calo/CPMTobRoIAuxContainer.h"
#include "xAODTrigL1Calo/CPMTowerAuxContainer.h"
#include "xAODTrigL1Calo/JEMEtSumsAuxContainer.h"
#include "xAODTrigL1Calo/JEMHitsAuxContainer.h"
#include "xAODTrigL1Calo/JEMRoIAuxContainer.h"
#include "xAODTrigL1Calo/JEMTobRoIAuxContainer.h"
#include "xAODTrigL1Calo/JetElementAuxContainer.h"
#include "xAODTrigL1Calo/RODHeaderAuxContainer.h"
#include "xAODTrigL1Calo/TriggerTowerAuxContainer.h"
#include "xAODTrigL1Calo/L1TopoRawDataAuxContainer.h"


namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODTRIGL1CALO {
      
    // Run 1
    
    // JEMHits  
    xAOD::JEMHitsContainer_v1 JEMHits_v1_c1;
    DataLink< xAOD::JEMHitsContainer_v1 > JEMHits_v1_l1;
    ElementLink< xAOD::JEMHitsContainer_v1 > JEMHits_v1_l2;
    ElementLinkVector< xAOD::JEMHitsContainer_v1 > JEMHits_v1_l3;
    std::vector< DataLink< xAOD::JEMHitsContainer_v1 > > JEMHits_v1_l4;
    std::vector< ElementLink< xAOD::JEMHitsContainer_v1 > > JEMHits_v1_l5;
    std::vector< ElementLinkVector< xAOD::JEMHitsContainer_v1 > > JEMHits_v1_l6;    
    
    // JEMEtSums
    xAOD::JEMEtSumsContainer_v1 JEMEtSums_v1_c1;
    DataLink< xAOD::JEMEtSumsContainer_v1 > JEMEtSums_v1_l1;
    ElementLink< xAOD::JEMEtSumsContainer_v1 > JEMEtSums_v1_l2;
    ElementLinkVector< xAOD::JEMEtSumsContainer_v1 > JEMEtSums_v1_l3;
    std::vector< DataLink< xAOD::JEMEtSumsContainer_v1 > > JEMEtSums_v1_l4;
    std::vector< ElementLink< xAOD::JEMEtSumsContainer_v1 > > JEMEtSums_v1_l5;
    std::vector< ElementLinkVector< xAOD::JEMEtSumsContainer_v1 > > JEMEtSums_v1_l6;     

    // JEMRoI   
    xAOD::JEMRoIContainer_v1 JEMRoI_v1_c1;
    DataLink< xAOD::JEMRoIContainer_v1 > JEMRoI_v1_l1;
    ElementLink< xAOD::JEMRoIContainer_v1 > JEMRoI_v1_l2;
    ElementLinkVector< xAOD::JEMRoIContainer_v1 > JEMRoI_v1_l3;
    std::vector< DataLink< xAOD::JEMRoIContainer_v1 > > JEMRoI_v1_l4;
    std::vector< ElementLink< xAOD::JEMRoIContainer_v1 > > JEMRoI_v1_l5;
    std::vector< ElementLinkVector< xAOD::JEMRoIContainer_v1 > > JEMRoI_v1_l6;      

    // CPMHits  
    xAOD::CPMHitsContainer_v1 CPMHits_v1_c1;
    DataLink< xAOD::CPMHitsContainer_v1 > CPMHits_v1_l1;
    ElementLink< xAOD::CPMHitsContainer_v1 > CPMHits_v1_l2;
    ElementLinkVector< xAOD::CPMHitsContainer_v1 > CPMHits_v1_l3;
    std::vector< DataLink< xAOD::CPMHitsContainer_v1 > > CPMHits_v1_l4;
    std::vector< ElementLink< xAOD::CPMHitsContainer_v1 > > CPMHits_v1_l5;
    std::vector< ElementLinkVector< xAOD::CPMHitsContainer_v1 > > CPMHits_v1_l6;      
    
    // CPMTower  
    xAOD::CPMTowerContainer_v1 CPMTower_v1_c1;
    DataLink< xAOD::CPMTowerContainer_v1 > CPMTower_v1_l1;
    ElementLink< xAOD::CPMTowerContainer_v1 > CPMTower_v1_l2;
    ElementLinkVector< xAOD::CPMTowerContainer_v1 > CPMTower_v1_l3;
    std::vector< DataLink< xAOD::CPMTowerContainer_v1 > > CPMTower_v1_l4;
    std::vector< ElementLink< xAOD::CPMTowerContainer_v1 > > CPMTower_v1_l5;
    std::vector< ElementLinkVector< xAOD::CPMTowerContainer_v1 > > CPMTower_v1_l6;        
    
    // CPMRoI 
    xAOD::CPMRoIContainer_v1 CPMRoI_v1_c1;
    DataLink< xAOD::CPMRoIContainer_v1 > CPMRoI_v1_l1;
    ElementLink< xAOD::CPMRoIContainer_v1 > CPMRoI_v1_l2;
    ElementLinkVector< xAOD::CPMRoIContainer_v1 > CPMRoI_v1_l3;
    std::vector< DataLink< xAOD::CPMRoIContainer_v1 > > CPMRoI_v1_l4;
    std::vector< ElementLink< xAOD::CPMRoIContainer_v1 > > CPMRoI_v1_l5;
    std::vector< ElementLinkVector< xAOD::CPMRoIContainer_v1 > > CPMRoI_v1_l6;      

    // CMMCPHits
    xAOD::CMMCPHitsContainer_v1 CMMCPHits_v1_c1;
    DataLink< xAOD::CMMCPHitsContainer_v1 > CMMCPHits_v1_l1;
    ElementLink< xAOD::CMMCPHitsContainer_v1 > CMMCPHits_v1_l2;
    ElementLinkVector< xAOD::CMMCPHitsContainer_v1 > CMMCPHits_v1_l3;
    std::vector< DataLink< xAOD::CMMCPHitsContainer_v1 > > CMMCPHits_v1_l4;
    std::vector< ElementLink< xAOD::CMMCPHitsContainer_v1 > > CMMCPHits_v1_l5;
    std::vector< ElementLinkVector< xAOD::CMMCPHitsContainer_v1 > > CMMCPHits_v1_l6;     
    
    // CMMEtSums 
    xAOD::CMMEtSumsContainer_v1 CMMEtSums_v1_c1;
    DataLink< xAOD::CMMEtSumsContainer_v1 > CMMEtSums_v1_l1;
    ElementLink< xAOD::CMMEtSumsContainer_v1 > CMMEtSums_v1_l2;
    ElementLinkVector< xAOD::CMMEtSumsContainer_v1 > CMMEtSums_v1_l3;
    std::vector< DataLink< xAOD::CMMEtSumsContainer_v1 > > CMMEtSums_v1_l4;
    std::vector< ElementLink< xAOD::CMMEtSumsContainer_v1 > > CMMEtSums_v1_l5;
    std::vector< ElementLinkVector< xAOD::CMMEtSumsContainer_v1 > > CMMEtSums_v1_l6;    
    
    // CMMJetHits   
    xAOD::CMMJetHitsContainer_v1 CMMJetHits_v1_c1;
    DataLink< xAOD::CMMJetHitsContainer_v1 > CMMJetHits_v1_l1;
    ElementLink< xAOD::CMMJetHitsContainer_v1 > CMMJetHits_v1_l2;
    ElementLinkVector< xAOD::CMMJetHitsContainer_v1 > CMMJetHits_v1_l3;
    std::vector< DataLink< xAOD::CMMJetHitsContainer_v1 > > CMMJetHits_v1_l4;
    std::vector< ElementLink< xAOD::CMMJetHitsContainer_v1 > > CMMJetHits_v1_l5;
    std::vector< ElementLinkVector< xAOD::CMMJetHitsContainer_v1 > > CMMJetHits_v1_l6;        
        
    // JetElement  
    xAOD::JetElementContainer_v1 JetElement_v1_c1;
    DataLink< xAOD::JetElementContainer_v1 > JetElement_v1_l1;
    ElementLink< xAOD::JetElementContainer_v1 > JetElement_v1_l2;
    ElementLinkVector< xAOD::JetElementContainer_v1 > JetElement_v1_l3;
    std::vector< DataLink< xAOD::JetElementContainer_v1 > > JetElement_v1_l4;
    std::vector< ElementLink< xAOD::JetElementContainer_v1 > > JetElement_v1_l5;
    std::vector< ElementLinkVector< xAOD::JetElementContainer_v1 > > JetElement_v1_l6;        
    
    // RODHeader  
    xAOD::RODHeaderContainer_v1 RODHeader_v1_c1;
    DataLink< xAOD::RODHeaderContainer_v1 > RODHeader_v1_l1;
    ElementLink< xAOD::RODHeaderContainer_v1 > RODHeader_v1_l2;
    ElementLinkVector< xAOD::RODHeaderContainer_v1 > RODHeader_v1_l3;
    std::vector< DataLink< xAOD::RODHeaderContainer_v1 > > RODHeader_v1_l4;
    std::vector< ElementLink< xAOD::RODHeaderContainer_v1 > > RODHeader_v1_l5;
    std::vector< ElementLinkVector< xAOD::RODHeaderContainer_v1 > > RODHeader_v1_l6;      
    
    // TriggerTower 
    xAOD::TriggerTowerContainer_v1 TriggerTower_v1_c1;
    DataLink< xAOD::TriggerTowerContainer_v1 > TriggerTower_v1_l1;
    ElementLink< xAOD::TriggerTowerContainer_v1 > TriggerTower_v1_l2;
    ElementLinkVector< xAOD::TriggerTowerContainer_v1 > TriggerTower_v1_l3;
    std::vector< DataLink< xAOD::TriggerTowerContainer_v1 > > TriggerTower_v1_l4;
    std::vector< ElementLink< xAOD::TriggerTowerContainer_v1 > > TriggerTower_v1_l5;
    std::vector< ElementLinkVector< xAOD::TriggerTowerContainer_v1 > > TriggerTower_v1_l6;      
    
    // Run 2
    
    // TriggerTower
    xAOD::TriggerTowerContainer_v2 TriggerTower_v2_c1;
    DataLink< xAOD::TriggerTowerContainer_v2 > TriggerTower_v2_l1;
    ElementLink< xAOD::TriggerTowerContainer_v2 > TriggerTower_v2_l2;
    ElementLinkVector< xAOD::TriggerTowerContainer_v2 > TriggerTower_v2_l3;
    std::vector< DataLink< xAOD::TriggerTowerContainer_v2 > > TriggerTower_v2_l4;
    std::vector< ElementLink< xAOD::TriggerTowerContainer_v2 > > TriggerTower_v2_l5;
    std::vector< ElementLinkVector< xAOD::TriggerTowerContainer_v2 > > TriggerTower_v2_l6;

    // CMXCPHits
    xAOD::CMXCPHitsContainer_v1 CMXCPHits_v1_c1;
    DataLink< xAOD::CMXCPHitsContainer_v1 > CMXCPHits_v1_l1;
    ElementLink< xAOD::CMXCPHitsContainer_v1 > CMXCPHits_v1_l2;
    ElementLinkVector< xAOD::CMXCPHitsContainer_v1 > CMXCPHits_v1_l3;
    std::vector< DataLink< xAOD::CMXCPHitsContainer_v1 > > CMXCPHits_v1_l4;
    std::vector< ElementLink< xAOD::CMXCPHitsContainer_v1 > > CMXCPHits_v1_l5;
    std::vector< ElementLinkVector< xAOD::CMXCPHitsContainer_v1 > > CMXCPHits_v1_l6;    
    
    // CMXCPTob
    xAOD::CMXCPTobContainer_v1 CMXCPTob_v1_c1;
    DataLink< xAOD::CMXCPTobContainer_v1 > CMXCPTob_v1_l1;
    ElementLink< xAOD::CMXCPTobContainer_v1 > CMXCPTob_v1_l2;
    ElementLinkVector< xAOD::CMXCPTobContainer_v1 > CMXCPTob_v1_l3;
    std::vector< DataLink< xAOD::CMXCPTobContainer_v1 > > CMXCPTob_v1_l4;
    std::vector< ElementLink< xAOD::CMXCPTobContainer_v1 > > CMXCPTob_v1_l5;
    std::vector< ElementLinkVector< xAOD::CMXCPTobContainer_v1 > > CMXCPTob_v1_l6;     
         
    // CMXJetHits
    xAOD::CMXJetHitsContainer_v1 CMXJetHits_v1_c1;
    DataLink< xAOD::CMXJetHitsContainer_v1 > CMXJetHits_v1_l1;
    ElementLink< xAOD::CMXJetHitsContainer_v1 > CMXJetHits_v1_l2;
    ElementLinkVector< xAOD::CMXJetHitsContainer_v1 > CMXJetHits_v1_l3;
    std::vector< DataLink< xAOD::CMXJetHitsContainer_v1 > > CMXJetHits_v1_l4;
    std::vector< ElementLink< xAOD::CMXJetHitsContainer_v1 > > CMXJetHits_v1_l5;
    std::vector< ElementLinkVector< xAOD::CMXJetHitsContainer_v1 > > CMXJetHits_v1_l6;      

    // CMXJetTob
    xAOD::CMXJetTobContainer_v1 CMXJetTob_v1_c1;
    DataLink< xAOD::CMXJetTobContainer_v1 > CMXJetTob_v1_l1;
    ElementLink< xAOD::CMXJetTobContainer_v1 > CMXJetTob_v1_l2;
    ElementLinkVector< xAOD::CMXJetTobContainer_v1 > CMXJetTob_v1_l3;
    std::vector< DataLink< xAOD::CMXJetTobContainer_v1 > > CMXJetTob_v1_l4;
    std::vector< ElementLink< xAOD::CMXJetTobContainer_v1 > > CMXJetTob_v1_l5;
    std::vector< ElementLinkVector< xAOD::CMXJetTobContainer_v1 > > CMXJetTob_v1_l6;    

    // CMXEtSums
    xAOD::CMXEtSumsContainer_v1 CMXEtSums_v1_c1;
    DataLink< xAOD::CMXEtSumsContainer_v1 > CMXEtSums_v1_l1;
    ElementLink< xAOD::CMXEtSumsContainer_v1 > CMXEtSums_v1_l2;
    ElementLinkVector< xAOD::CMXEtSumsContainer_v1 > CMXEtSums_v1_l3;
    std::vector< DataLink< xAOD::CMXEtSumsContainer_v1 > > CMXEtSums_v1_l4;
    std::vector< ElementLink< xAOD::CMXEtSumsContainer_v1 > > CMXEtSums_v1_l5;
    std::vector< ElementLinkVector< xAOD::CMXEtSumsContainer_v1 > > CMXEtSums_v1_l6;        
   
    // CPMTower  
    xAOD::CPMTowerContainer_v2 CPMTower_v2_c1;
    DataLink< xAOD::CPMTowerContainer_v2 > CPMTower_v2_l1;
    ElementLink< xAOD::CPMTowerContainer_v2 > CPMTower_v2_l2;
    ElementLinkVector< xAOD::CPMTowerContainer_v2 > CPMTower_v2_l3;
    std::vector< DataLink< xAOD::CPMTowerContainer_v2 > > CPMTower_v2_l4;
    std::vector< ElementLink< xAOD::CPMTowerContainer_v2 > > CPMTower_v2_l5;
    std::vector< ElementLinkVector< xAOD::CPMTowerContainer_v2 > > CPMTower_v2_l6;     
   
    // CPMTobRoI
    xAOD::CPMTobRoIContainer_v1 CPMTobRoI_v1_c1;
    DataLink< xAOD::CPMTobRoIContainer_v1 > CPMTobRoI_v1_l1;
    ElementLink< xAOD::CPMTobRoIContainer_v1 > CPMTobRoI_v1_l2;
    ElementLinkVector< xAOD::CPMTobRoIContainer_v1 > CPMTobRoI_v1_l3;
    std::vector< DataLink< xAOD::CPMTobRoIContainer_v1 > > CPMTobRoI_v1_l4;
    std::vector< ElementLink< xAOD::CPMTobRoIContainer_v1 > > CPMTobRoI_v1_l5;
    std::vector< ElementLinkVector< xAOD::CPMTobRoIContainer_v1 > > CPMTobRoI_v1_l6;     

    // JEMEtSums
    xAOD::JEMEtSumsContainer_v2 JEMEtSums_v2_c1;
    DataLink< xAOD::JEMEtSumsContainer_v2 > JEMEtSums_v2_l1;
    ElementLink< xAOD::JEMEtSumsContainer_v2 > JEMEtSums_v2_l2;
    ElementLinkVector< xAOD::JEMEtSumsContainer_v2 > JEMEtSums_v2_l3;
    std::vector< DataLink< xAOD::JEMEtSumsContainer_v2 > > JEMEtSums_v2_l4;
    std::vector< ElementLink< xAOD::JEMEtSumsContainer_v2 > > JEMEtSums_v2_l5;
    std::vector< ElementLinkVector< xAOD::JEMEtSumsContainer_v2 > > JEMEtSums_v2_l6;     

    // JEMTobRoI
    xAOD::JEMTobRoIContainer_v1 JEMTobRoI_v1_c1;
    DataLink< xAOD::JEMTobRoIContainer_v1 > JEMTobRoI_v1_l1;
    ElementLink< xAOD::JEMTobRoIContainer_v1 > JEMTobRoI_v1_l2;
    ElementLinkVector< xAOD::JEMTobRoIContainer_v1 > JEMTobRoI_v1_l3;
    std::vector< DataLink< xAOD::JEMTobRoIContainer_v1 > > JEMTobRoI_v1_l4;
    std::vector< ElementLink< xAOD::JEMTobRoIContainer_v1 > > JEMTobRoI_v1_l5;
    std::vector< ElementLinkVector< xAOD::JEMTobRoIContainer_v1 > > JEMTobRoI_v1_l6;        
     
    // JetElement
    xAOD::JetElementContainer_v2 JetElement_v2_c1;
    DataLink< xAOD::JetElementContainer_v2 > JetElement_v2_l1;
    ElementLink< xAOD::JetElementContainer_v2 > JetElement_v2_l2;
    ElementLinkVector< xAOD::JetElementContainer_v2 > JetElement_v2_l3;
    std::vector< DataLink< xAOD::JetElementContainer_v2 > > JetElement_v2_l4;
    std::vector< ElementLink< xAOD::JetElementContainer_v2 > > JetElement_v2_l5;
    std::vector< ElementLinkVector< xAOD::JetElementContainer_v2 > > JetElement_v2_l6;
    
    // L1TopoRawData
    xAOD::L1TopoRawDataContainer_v1 L1TopoRawData_v1_c1;
    DataLink< xAOD::L1TopoRawDataContainer_v1 > L1TopoRawData_v1_l1;
    ElementLink< xAOD::L1TopoRawDataContainer_v1 > L1TopoRawData_v1_l2;
    ElementLinkVector< xAOD::L1TopoRawDataContainer_v1 > L1TopoRawData_v1_l3;
    std::vector< DataLink< xAOD::L1TopoRawDataContainer_v1 > > L1TopoRawData_v1_l4;
    std::vector< ElementLink< xAOD::L1TopoRawDataContainer_v1 > > L1TopoRawData_v1_l5;
    std::vector< ElementLinkVector< xAOD::L1TopoRawDataContainer_v1 > > L1TopoRawData_v1_l6;    
    
  };
}

#endif // XAODTRIGL1CALO_XAODTRIGL1CALOCALOEVENTDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataVector<xAOD::CMMCPHits_v1>", payloadCode, "@",
"DataVector<xAOD::CMMEtSums_v1>", payloadCode, "@",
"DataVector<xAOD::CMMJetHits_v1>", payloadCode, "@",
"DataVector<xAOD::CMXCPHits_v1>", payloadCode, "@",
"DataVector<xAOD::CMXCPTob_v1>", payloadCode, "@",
"DataVector<xAOD::CMXEtSums_v1>", payloadCode, "@",
"DataVector<xAOD::CMXJetHits_v1>", payloadCode, "@",
"DataVector<xAOD::CMXJetTob_v1>", payloadCode, "@",
"DataVector<xAOD::CPMHits_v1>", payloadCode, "@",
"DataVector<xAOD::CPMRoI_v1>", payloadCode, "@",
"DataVector<xAOD::CPMTobRoI_v1>", payloadCode, "@",
"DataVector<xAOD::CPMTower_v1>", payloadCode, "@",
"DataVector<xAOD::CPMTower_v2>", payloadCode, "@",
"DataVector<xAOD::JEMEtSums_v1>", payloadCode, "@",
"DataVector<xAOD::JEMEtSums_v2>", payloadCode, "@",
"DataVector<xAOD::JEMHits_v1>", payloadCode, "@",
"DataVector<xAOD::JEMRoI_v1>", payloadCode, "@",
"DataVector<xAOD::JEMTobRoI_v1>", payloadCode, "@",
"DataVector<xAOD::JetElement_v1>", payloadCode, "@",
"DataVector<xAOD::JetElement_v2>", payloadCode, "@",
"DataVector<xAOD::L1TopoRawData_v1>", payloadCode, "@",
"DataVector<xAOD::RODHeader_v1>", payloadCode, "@",
"DataVector<xAOD::TriggerTower_v1>", payloadCode, "@",
"DataVector<xAOD::TriggerTower_v2>", payloadCode, "@",
"xAOD::CMMCPHitsAuxContainer_v1", payloadCode, "@",
"xAOD::CMMCPHitsContainer_v1", payloadCode, "@",
"xAOD::CMMCPHits_v1", payloadCode, "@",
"xAOD::CMMEtSumsAuxContainer_v1", payloadCode, "@",
"xAOD::CMMEtSumsContainer_v1", payloadCode, "@",
"xAOD::CMMEtSums_v1", payloadCode, "@",
"xAOD::CMMJetHitsAuxContainer_v1", payloadCode, "@",
"xAOD::CMMJetHitsContainer_v1", payloadCode, "@",
"xAOD::CMMJetHits_v1", payloadCode, "@",
"xAOD::CMMRoIAuxInfo_v1", payloadCode, "@",
"xAOD::CMMRoI_v1", payloadCode, "@",
"xAOD::CMXCPHitsAuxContainer_v1", payloadCode, "@",
"xAOD::CMXCPHitsContainer_v1", payloadCode, "@",
"xAOD::CMXCPHits_v1", payloadCode, "@",
"xAOD::CMXCPTobAuxContainer_v1", payloadCode, "@",
"xAOD::CMXCPTobContainer_v1", payloadCode, "@",
"xAOD::CMXCPTob_v1", payloadCode, "@",
"xAOD::CMXEtSumsAuxContainer_v1", payloadCode, "@",
"xAOD::CMXEtSumsContainer_v1", payloadCode, "@",
"xAOD::CMXEtSums_v1", payloadCode, "@",
"xAOD::CMXJetHitsAuxContainer_v1", payloadCode, "@",
"xAOD::CMXJetHitsContainer_v1", payloadCode, "@",
"xAOD::CMXJetHits_v1", payloadCode, "@",
"xAOD::CMXJetTobAuxContainer_v1", payloadCode, "@",
"xAOD::CMXJetTobContainer_v1", payloadCode, "@",
"xAOD::CMXJetTob_v1", payloadCode, "@",
"xAOD::CPMHitsAuxContainer_v1", payloadCode, "@",
"xAOD::CPMHitsContainer_v1", payloadCode, "@",
"xAOD::CPMHits_v1", payloadCode, "@",
"xAOD::CPMRoIAuxContainer_v1", payloadCode, "@",
"xAOD::CPMRoIContainer_v1", payloadCode, "@",
"xAOD::CPMRoI_v1", payloadCode, "@",
"xAOD::CPMTobRoIAuxContainer_v1", payloadCode, "@",
"xAOD::CPMTobRoIContainer_v1", payloadCode, "@",
"xAOD::CPMTobRoI_v1", payloadCode, "@",
"xAOD::CPMTowerAuxContainer_v1", payloadCode, "@",
"xAOD::CPMTowerAuxContainer_v2", payloadCode, "@",
"xAOD::CPMTowerContainer_v1", payloadCode, "@",
"xAOD::CPMTowerContainer_v2", payloadCode, "@",
"xAOD::CPMTower_v1", payloadCode, "@",
"xAOD::CPMTower_v2", payloadCode, "@",
"xAOD::JEMEtSumsAuxContainer_v1", payloadCode, "@",
"xAOD::JEMEtSumsAuxContainer_v2", payloadCode, "@",
"xAOD::JEMEtSumsContainer_v1", payloadCode, "@",
"xAOD::JEMEtSumsContainer_v2", payloadCode, "@",
"xAOD::JEMEtSums_v1", payloadCode, "@",
"xAOD::JEMEtSums_v2", payloadCode, "@",
"xAOD::JEMHitsAuxContainer_v1", payloadCode, "@",
"xAOD::JEMHitsContainer_v1", payloadCode, "@",
"xAOD::JEMHits_v1", payloadCode, "@",
"xAOD::JEMRoIAuxContainer_v1", payloadCode, "@",
"xAOD::JEMRoIContainer_v1", payloadCode, "@",
"xAOD::JEMRoI_v1", payloadCode, "@",
"xAOD::JEMTobRoIAuxContainer_v1", payloadCode, "@",
"xAOD::JEMTobRoIContainer_v1", payloadCode, "@",
"xAOD::JEMTobRoI_v1", payloadCode, "@",
"xAOD::JetElementAuxContainer_v1", payloadCode, "@",
"xAOD::JetElementAuxContainer_v2", payloadCode, "@",
"xAOD::JetElementContainer_v1", payloadCode, "@",
"xAOD::JetElementContainer_v2", payloadCode, "@",
"xAOD::JetElement_v1", payloadCode, "@",
"xAOD::JetElement_v2", payloadCode, "@",
"xAOD::L1TopoRawDataAuxContainer_v1", payloadCode, "@",
"xAOD::L1TopoRawDataContainer_v1", payloadCode, "@",
"xAOD::L1TopoRawData_v1", payloadCode, "@",
"xAOD::RODHeaderAuxContainer_v1", payloadCode, "@",
"xAOD::RODHeaderContainer_v1", payloadCode, "@",
"xAOD::RODHeader_v1", payloadCode, "@",
"xAOD::TriggerTowerAuxContainer_v1", payloadCode, "@",
"xAOD::TriggerTowerAuxContainer_v2", payloadCode, "@",
"xAOD::TriggerTowerContainer_v1", payloadCode, "@",
"xAOD::TriggerTowerContainer_v2", payloadCode, "@",
"xAOD::TriggerTower_v1", payloadCode, "@",
"xAOD::TriggerTower_v2", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigL1Calo_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigL1Calo_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigL1Calo_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigL1Calo_Reflex() {
  TriggerDictionaryInitialization_xAODTrigL1Calo_Reflex_Impl();
}

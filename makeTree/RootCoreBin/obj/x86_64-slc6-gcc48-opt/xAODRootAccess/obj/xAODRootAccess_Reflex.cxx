// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODRootAccessdIobjdIxAODRootAccess_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/xAODRootAccess/xAODRootAccessDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTEvent_Dictionary();
   static void xAODcLcLTEvent_TClassManip(TClass*);
   static void *new_xAODcLcLTEvent(void *p = 0);
   static void *newArray_xAODcLcLTEvent(Long_t size, void *p);
   static void delete_xAODcLcLTEvent(void *p);
   static void deleteArray_xAODcLcLTEvent(void *p);
   static void destruct_xAODcLcLTEvent(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TEvent*)
   {
      ::xAOD::TEvent *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TEvent));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TEvent", "xAODRootAccess/TEvent.h", 66,
                  typeid(::xAOD::TEvent), DefineBehavior(ptr, ptr),
                  &xAODcLcLTEvent_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TEvent) );
      instance.SetNew(&new_xAODcLcLTEvent);
      instance.SetNewArray(&newArray_xAODcLcLTEvent);
      instance.SetDelete(&delete_xAODcLcLTEvent);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTEvent);
      instance.SetDestructor(&destruct_xAODcLcLTEvent);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TEvent*)
   {
      return GenerateInitInstanceLocal((::xAOD::TEvent*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TEvent*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTEvent_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEvent*)0x0)->GetClass();
      xAODcLcLTEvent_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTEvent_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTVirtualManager_Dictionary();
   static void xAODcLcLTVirtualManager_TClassManip(TClass*);
   static void delete_xAODcLcLTVirtualManager(void *p);
   static void deleteArray_xAODcLcLTVirtualManager(void *p);
   static void destruct_xAODcLcLTVirtualManager(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TVirtualManager*)
   {
      ::xAOD::TVirtualManager *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TVirtualManager));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TVirtualManager", "xAODRootAccess/tools/TVirtualManager.h", 23,
                  typeid(::xAOD::TVirtualManager), DefineBehavior(ptr, ptr),
                  &xAODcLcLTVirtualManager_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TVirtualManager) );
      instance.SetDelete(&delete_xAODcLcLTVirtualManager);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTVirtualManager);
      instance.SetDestructor(&destruct_xAODcLcLTVirtualManager);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TVirtualManager*)
   {
      return GenerateInitInstanceLocal((::xAOD::TVirtualManager*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TVirtualManager*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTVirtualManager_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TVirtualManager*)0x0)->GetClass();
      xAODcLcLTVirtualManager_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTVirtualManager_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTStore_Dictionary();
   static void xAODcLcLTStore_TClassManip(TClass*);
   static void *new_xAODcLcLTStore(void *p = 0);
   static void *newArray_xAODcLcLTStore(Long_t size, void *p);
   static void delete_xAODcLcLTStore(void *p);
   static void deleteArray_xAODcLcLTStore(void *p);
   static void destruct_xAODcLcLTStore(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TStore*)
   {
      ::xAOD::TStore *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TStore));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TStore", "xAODRootAccess/TStore.h", 45,
                  typeid(::xAOD::TStore), DefineBehavior(ptr, ptr),
                  &xAODcLcLTStore_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TStore) );
      instance.SetNew(&new_xAODcLcLTStore);
      instance.SetNewArray(&newArray_xAODcLcLTStore);
      instance.SetDelete(&delete_xAODcLcLTStore);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTStore);
      instance.SetDestructor(&destruct_xAODcLcLTStore);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TStore*)
   {
      return GenerateInitInstanceLocal((::xAOD::TStore*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TStore*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTStore_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TStore*)0x0)->GetClass();
      xAODcLcLTStore_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTStore_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTPyEvent_Dictionary();
   static void xAODcLcLTPyEvent_TClassManip(TClass*);
   static void *new_xAODcLcLTPyEvent(void *p = 0);
   static void *newArray_xAODcLcLTPyEvent(Long_t size, void *p);
   static void delete_xAODcLcLTPyEvent(void *p);
   static void deleteArray_xAODcLcLTPyEvent(void *p);
   static void destruct_xAODcLcLTPyEvent(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TPyEvent*)
   {
      ::xAOD::TPyEvent *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TPyEvent));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TPyEvent", "xAODRootAccess/TPyEvent.h", 30,
                  typeid(::xAOD::TPyEvent), DefineBehavior(ptr, ptr),
                  &xAODcLcLTPyEvent_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TPyEvent) );
      instance.SetNew(&new_xAODcLcLTPyEvent);
      instance.SetNewArray(&newArray_xAODcLcLTPyEvent);
      instance.SetDelete(&delete_xAODcLcLTPyEvent);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTPyEvent);
      instance.SetDestructor(&destruct_xAODcLcLTPyEvent);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TPyEvent*)
   {
      return GenerateInitInstanceLocal((::xAOD::TPyEvent*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TPyEvent*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTPyEvent_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TPyEvent*)0x0)->GetClass();
      xAODcLcLTPyEvent_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTPyEvent_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTPyStore_Dictionary();
   static void xAODcLcLTPyStore_TClassManip(TClass*);
   static void *new_xAODcLcLTPyStore(void *p = 0);
   static void *newArray_xAODcLcLTPyStore(Long_t size, void *p);
   static void delete_xAODcLcLTPyStore(void *p);
   static void deleteArray_xAODcLcLTPyStore(void *p);
   static void destruct_xAODcLcLTPyStore(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TPyStore*)
   {
      ::xAOD::TPyStore *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TPyStore));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TPyStore", "xAODRootAccess/TPyStore.h", 31,
                  typeid(::xAOD::TPyStore), DefineBehavior(ptr, ptr),
                  &xAODcLcLTPyStore_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TPyStore) );
      instance.SetNew(&new_xAODcLcLTPyStore);
      instance.SetNewArray(&newArray_xAODcLcLTPyStore);
      instance.SetDelete(&delete_xAODcLcLTPyStore);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTPyStore);
      instance.SetDestructor(&destruct_xAODcLcLTPyStore);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TPyStore*)
   {
      return GenerateInitInstanceLocal((::xAOD::TPyStore*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TPyStore*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTPyStore_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TPyStore*)0x0)->GetClass();
      xAODcLcLTPyStore_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTPyStore_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTActiveStore_Dictionary();
   static void xAODcLcLTActiveStore_TClassManip(TClass*);
   static void *new_xAODcLcLTActiveStore(void *p = 0);
   static void *newArray_xAODcLcLTActiveStore(Long_t size, void *p);
   static void delete_xAODcLcLTActiveStore(void *p);
   static void deleteArray_xAODcLcLTActiveStore(void *p);
   static void destruct_xAODcLcLTActiveStore(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TActiveStore*)
   {
      ::xAOD::TActiveStore *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TActiveStore));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TActiveStore", "xAODRootAccess/TActiveStore.h", 25,
                  typeid(::xAOD::TActiveStore), DefineBehavior(ptr, ptr),
                  &xAODcLcLTActiveStore_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TActiveStore) );
      instance.SetNew(&new_xAODcLcLTActiveStore);
      instance.SetNewArray(&newArray_xAODcLcLTActiveStore);
      instance.SetDelete(&delete_xAODcLcLTActiveStore);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTActiveStore);
      instance.SetDestructor(&destruct_xAODcLcLTActiveStore);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TActiveStore*)
   {
      return GenerateInitInstanceLocal((::xAOD::TActiveStore*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TActiveStore*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTActiveStore_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TActiveStore*)0x0)->GetClass();
      xAODcLcLTActiveStore_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTActiveStore_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTEvent(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TEvent : new ::xAOD::TEvent;
   }
   static void *newArray_xAODcLcLTEvent(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TEvent[nElements] : new ::xAOD::TEvent[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTEvent(void *p) {
      delete ((::xAOD::TEvent*)p);
   }
   static void deleteArray_xAODcLcLTEvent(void *p) {
      delete [] ((::xAOD::TEvent*)p);
   }
   static void destruct_xAODcLcLTEvent(void *p) {
      typedef ::xAOD::TEvent current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TEvent

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTVirtualManager(void *p) {
      delete ((::xAOD::TVirtualManager*)p);
   }
   static void deleteArray_xAODcLcLTVirtualManager(void *p) {
      delete [] ((::xAOD::TVirtualManager*)p);
   }
   static void destruct_xAODcLcLTVirtualManager(void *p) {
      typedef ::xAOD::TVirtualManager current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TVirtualManager

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTStore(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TStore : new ::xAOD::TStore;
   }
   static void *newArray_xAODcLcLTStore(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TStore[nElements] : new ::xAOD::TStore[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTStore(void *p) {
      delete ((::xAOD::TStore*)p);
   }
   static void deleteArray_xAODcLcLTStore(void *p) {
      delete [] ((::xAOD::TStore*)p);
   }
   static void destruct_xAODcLcLTStore(void *p) {
      typedef ::xAOD::TStore current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TStore

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTPyEvent(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TPyEvent : new ::xAOD::TPyEvent;
   }
   static void *newArray_xAODcLcLTPyEvent(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TPyEvent[nElements] : new ::xAOD::TPyEvent[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTPyEvent(void *p) {
      delete ((::xAOD::TPyEvent*)p);
   }
   static void deleteArray_xAODcLcLTPyEvent(void *p) {
      delete [] ((::xAOD::TPyEvent*)p);
   }
   static void destruct_xAODcLcLTPyEvent(void *p) {
      typedef ::xAOD::TPyEvent current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TPyEvent

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTPyStore(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TPyStore : new ::xAOD::TPyStore;
   }
   static void *newArray_xAODcLcLTPyStore(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TPyStore[nElements] : new ::xAOD::TPyStore[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTPyStore(void *p) {
      delete ((::xAOD::TPyStore*)p);
   }
   static void deleteArray_xAODcLcLTPyStore(void *p) {
      delete [] ((::xAOD::TPyStore*)p);
   }
   static void destruct_xAODcLcLTPyStore(void *p) {
      typedef ::xAOD::TPyStore current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TPyStore

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTActiveStore(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TActiveStore : new ::xAOD::TActiveStore;
   }
   static void *newArray_xAODcLcLTActiveStore(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TActiveStore[nElements] : new ::xAOD::TActiveStore[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTActiveStore(void *p) {
      delete ((::xAOD::TActiveStore*)p);
   }
   static void deleteArray_xAODcLcLTActiveStore(void *p) {
      delete [] ((::xAOD::TActiveStore*)p);
   }
   static void destruct_xAODcLcLTActiveStore(void *p) {
      typedef ::xAOD::TActiveStore current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TActiveStore

namespace {
  void TriggerDictionaryInitialization_xAODRootAccess_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODRootAccess/TEvent.h")))  TEvent;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODRootAccess/tools/TVirtualManager.h")))  TVirtualManager;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODRootAccess/TStore.h")))  TStore;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODRootAccess/TPyEvent.h")))  TPyEvent;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODRootAccess/TPyStore.h")))  TPyStore;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODRootAccess/TActiveStore.h")))  TActiveStore;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODRootAccess"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODRootAccessDict.h 649835 2015-02-26 08:19:01Z krasznaa $
#ifndef XAODROOTACCESS_XAODROOTACCESSDICT_H
#define XAODROOTACCESS_XAODROOTACCESSDICT_H

// Local includude(s):
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/TActiveStore.h"
#include "xAODRootAccess/TPyEvent.h"
#include "xAODRootAccess/TPyStore.h"
#include "xAODRootAccess/tools/TVirtualManager.h"

#endif // XAODROOTACCESS_XAODROOTACCESSDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xAOD::TActiveStore", payloadCode, "@",
"xAOD::TEvent", payloadCode, "@",
"xAOD::TPyEvent", payloadCode, "@",
"xAOD::TPyStore", payloadCode, "@",
"xAOD::TStore", payloadCode, "@",
"xAOD::TVirtualManager", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODRootAccess_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODRootAccess_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODRootAccess_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODRootAccess_Reflex() {
  TriggerDictionaryInitialization_xAODRootAccess_Reflex_Impl();
}

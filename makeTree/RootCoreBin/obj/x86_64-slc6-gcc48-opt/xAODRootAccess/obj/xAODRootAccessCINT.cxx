#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODRootAccessdIobjdIxAODRootAccessCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "xAODRootAccess/tools/TEventBranch.h"
#include "xAODRootAccess/tools/TEventTree.h"
#include "xAODRootAccess/tools/TMetaBranch.h"
#include "xAODRootAccess/tools/TMetaTree.h"
#include "xAODRootAccess/tools/TTransTrees.h"
#include "xAODRootAccess/tools/TReturnCode.h"
#include "xAODRootAccess/tools/TFileMerger.h"
#include "xAODRootAccess/tools/TFileChecker.h"
#include "xAODRootAccess/MakeTransientTree.h"
#include "xAODRootAccess/Init.h"

// Header files passed via #pragma extra_include

namespace xAOD {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *xAOD_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("xAOD", 0 /*version*/, "xAODRootAccess/tools/TEventBranch.h", 12,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &xAOD_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *xAOD_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static void delete_xAODcLcLTEventBranch(void *p);
   static void deleteArray_xAODcLcLTEventBranch(void *p);
   static void destruct_xAODcLcLTEventBranch(void *p);
   static void streamer_xAODcLcLTEventBranch(TBuffer &buf, void *obj);
   static void reset_xAODcLcLTEventBranch(void *obj, TFileMergeInfo *info);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TEventBranch*)
   {
      ::xAOD::TEventBranch *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::TEventBranch >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TEventBranch", ::xAOD::TEventBranch::Class_Version(), "xAODRootAccess/tools/TEventBranch.h", 33,
                  typeid(::xAOD::TEventBranch), DefineBehavior(ptr, ptr),
                  &::xAOD::TEventBranch::Dictionary, isa_proxy, 16,
                  sizeof(::xAOD::TEventBranch) );
      instance.SetDelete(&delete_xAODcLcLTEventBranch);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTEventBranch);
      instance.SetDestructor(&destruct_xAODcLcLTEventBranch);
      instance.SetStreamerFunc(&streamer_xAODcLcLTEventBranch);
      instance.SetResetAfterMerge(&reset_xAODcLcLTEventBranch);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TEventBranch*)
   {
      return GenerateInitInstanceLocal((::xAOD::TEventBranch*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TEventBranch*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_xAODcLcLTEventTree(void *p);
   static void deleteArray_xAODcLcLTEventTree(void *p);
   static void destruct_xAODcLcLTEventTree(void *p);
   static void directoryAutoAdd_xAODcLcLTEventTree(void *obj, TDirectory *dir);
   static void streamer_xAODcLcLTEventTree(TBuffer &buf, void *obj);
   static Long64_t merge_xAODcLcLTEventTree(void *obj, TCollection *coll,TFileMergeInfo *info);
   static void reset_xAODcLcLTEventTree(void *obj, TFileMergeInfo *info);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TEventTree*)
   {
      ::xAOD::TEventTree *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::TEventTree >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TEventTree", ::xAOD::TEventTree::Class_Version(), "xAODRootAccess/tools/TEventTree.h", 33,
                  typeid(::xAOD::TEventTree), DefineBehavior(ptr, ptr),
                  &::xAOD::TEventTree::Dictionary, isa_proxy, 16,
                  sizeof(::xAOD::TEventTree) );
      instance.SetDelete(&delete_xAODcLcLTEventTree);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTEventTree);
      instance.SetDestructor(&destruct_xAODcLcLTEventTree);
      instance.SetDirectoryAutoAdd(&directoryAutoAdd_xAODcLcLTEventTree);
      instance.SetStreamerFunc(&streamer_xAODcLcLTEventTree);
      instance.SetMerge(&merge_xAODcLcLTEventTree);
      instance.SetResetAfterMerge(&reset_xAODcLcLTEventTree);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TEventTree*)
   {
      return GenerateInitInstanceLocal((::xAOD::TEventTree*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TEventTree*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_xAODcLcLTMetaBranch(void *p);
   static void deleteArray_xAODcLcLTMetaBranch(void *p);
   static void destruct_xAODcLcLTMetaBranch(void *p);
   static void streamer_xAODcLcLTMetaBranch(TBuffer &buf, void *obj);
   static void reset_xAODcLcLTMetaBranch(void *obj, TFileMergeInfo *info);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TMetaBranch*)
   {
      ::xAOD::TMetaBranch *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::TMetaBranch >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TMetaBranch", ::xAOD::TMetaBranch::Class_Version(), "xAODRootAccess/tools/TMetaBranch.h", 28,
                  typeid(::xAOD::TMetaBranch), DefineBehavior(ptr, ptr),
                  &::xAOD::TMetaBranch::Dictionary, isa_proxy, 16,
                  sizeof(::xAOD::TMetaBranch) );
      instance.SetDelete(&delete_xAODcLcLTMetaBranch);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTMetaBranch);
      instance.SetDestructor(&destruct_xAODcLcLTMetaBranch);
      instance.SetStreamerFunc(&streamer_xAODcLcLTMetaBranch);
      instance.SetResetAfterMerge(&reset_xAODcLcLTMetaBranch);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TMetaBranch*)
   {
      return GenerateInitInstanceLocal((::xAOD::TMetaBranch*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TMetaBranch*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_xAODcLcLTMetaTree(void *p);
   static void deleteArray_xAODcLcLTMetaTree(void *p);
   static void destruct_xAODcLcLTMetaTree(void *p);
   static void directoryAutoAdd_xAODcLcLTMetaTree(void *obj, TDirectory *dir);
   static void streamer_xAODcLcLTMetaTree(TBuffer &buf, void *obj);
   static Long64_t merge_xAODcLcLTMetaTree(void *obj, TCollection *coll,TFileMergeInfo *info);
   static void reset_xAODcLcLTMetaTree(void *obj, TFileMergeInfo *info);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TMetaTree*)
   {
      ::xAOD::TMetaTree *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::TMetaTree >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TMetaTree", ::xAOD::TMetaTree::Class_Version(), "xAODRootAccess/tools/TMetaTree.h", 33,
                  typeid(::xAOD::TMetaTree), DefineBehavior(ptr, ptr),
                  &::xAOD::TMetaTree::Dictionary, isa_proxy, 16,
                  sizeof(::xAOD::TMetaTree) );
      instance.SetDelete(&delete_xAODcLcLTMetaTree);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTMetaTree);
      instance.SetDestructor(&destruct_xAODcLcLTMetaTree);
      instance.SetDirectoryAutoAdd(&directoryAutoAdd_xAODcLcLTMetaTree);
      instance.SetStreamerFunc(&streamer_xAODcLcLTMetaTree);
      instance.SetMerge(&merge_xAODcLcLTMetaTree);
      instance.SetResetAfterMerge(&reset_xAODcLcLTMetaTree);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TMetaTree*)
   {
      return GenerateInitInstanceLocal((::xAOD::TMetaTree*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TMetaTree*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTTransTrees_Dictionary();
   static void xAODcLcLTTransTrees_TClassManip(TClass*);
   static void delete_xAODcLcLTTransTrees(void *p);
   static void deleteArray_xAODcLcLTTransTrees(void *p);
   static void destruct_xAODcLcLTTransTrees(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TTransTrees*)
   {
      ::xAOD::TTransTrees *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TTransTrees));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TTransTrees", "xAODRootAccess/tools/TTransTrees.h", 24,
                  typeid(::xAOD::TTransTrees), DefineBehavior(ptr, ptr),
                  &xAODcLcLTTransTrees_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TTransTrees) );
      instance.SetDelete(&delete_xAODcLcLTTransTrees);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTTransTrees);
      instance.SetDestructor(&destruct_xAODcLcLTTransTrees);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TTransTrees*)
   {
      return GenerateInitInstanceLocal((::xAOD::TTransTrees*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TTransTrees*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTTransTrees_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TTransTrees*)0x0)->GetClass();
      xAODcLcLTTransTrees_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTTransTrees_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTReturnCode_Dictionary();
   static void xAODcLcLTReturnCode_TClassManip(TClass*);
   static void delete_xAODcLcLTReturnCode(void *p);
   static void deleteArray_xAODcLcLTReturnCode(void *p);
   static void destruct_xAODcLcLTReturnCode(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TReturnCode*)
   {
      ::xAOD::TReturnCode *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TReturnCode));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TReturnCode", "xAODRootAccess/tools/TReturnCode.h", 24,
                  typeid(::xAOD::TReturnCode), DefineBehavior(ptr, ptr),
                  &xAODcLcLTReturnCode_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TReturnCode) );
      instance.SetDelete(&delete_xAODcLcLTReturnCode);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTReturnCode);
      instance.SetDestructor(&destruct_xAODcLcLTReturnCode);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TReturnCode*)
   {
      return GenerateInitInstanceLocal((::xAOD::TReturnCode*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TReturnCode*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTReturnCode_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TReturnCode*)0x0)->GetClass();
      xAODcLcLTReturnCode_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTReturnCode_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_xAODcLcLTFileMerger(void *p = 0);
   static void *newArray_xAODcLcLTFileMerger(Long_t size, void *p);
   static void delete_xAODcLcLTFileMerger(void *p);
   static void deleteArray_xAODcLcLTFileMerger(void *p);
   static void destruct_xAODcLcLTFileMerger(void *p);
   static void streamer_xAODcLcLTFileMerger(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TFileMerger*)
   {
      ::xAOD::TFileMerger *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::TFileMerger >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TFileMerger", ::xAOD::TFileMerger::Class_Version(), "xAODRootAccess/tools/TFileMerger.h", 41,
                  typeid(::xAOD::TFileMerger), DefineBehavior(ptr, ptr),
                  &::xAOD::TFileMerger::Dictionary, isa_proxy, 16,
                  sizeof(::xAOD::TFileMerger) );
      instance.SetNew(&new_xAODcLcLTFileMerger);
      instance.SetNewArray(&newArray_xAODcLcLTFileMerger);
      instance.SetDelete(&delete_xAODcLcLTFileMerger);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTFileMerger);
      instance.SetDestructor(&destruct_xAODcLcLTFileMerger);
      instance.SetStreamerFunc(&streamer_xAODcLcLTFileMerger);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TFileMerger*)
   {
      return GenerateInitInstanceLocal((::xAOD::TFileMerger*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TFileMerger*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_xAODcLcLTFileChecker(void *p = 0);
   static void *newArray_xAODcLcLTFileChecker(Long_t size, void *p);
   static void delete_xAODcLcLTFileChecker(void *p);
   static void deleteArray_xAODcLcLTFileChecker(void *p);
   static void destruct_xAODcLcLTFileChecker(void *p);
   static void streamer_xAODcLcLTFileChecker(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TFileChecker*)
   {
      ::xAOD::TFileChecker *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAOD::TFileChecker >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TFileChecker", ::xAOD::TFileChecker::Class_Version(), "xAODRootAccess/tools/TFileChecker.h", 37,
                  typeid(::xAOD::TFileChecker), DefineBehavior(ptr, ptr),
                  &::xAOD::TFileChecker::Dictionary, isa_proxy, 16,
                  sizeof(::xAOD::TFileChecker) );
      instance.SetNew(&new_xAODcLcLTFileChecker);
      instance.SetNewArray(&newArray_xAODcLcLTFileChecker);
      instance.SetDelete(&delete_xAODcLcLTFileChecker);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTFileChecker);
      instance.SetDestructor(&destruct_xAODcLcLTFileChecker);
      instance.SetStreamerFunc(&streamer_xAODcLcLTFileChecker);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TFileChecker*)
   {
      return GenerateInitInstanceLocal((::xAOD::TFileChecker*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TFileChecker*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr TEventBranch::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TEventBranch::Class_Name()
{
   return "xAOD::TEventBranch";
}

//______________________________________________________________________________
const char *TEventBranch::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEventBranch*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TEventBranch::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEventBranch*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TEventBranch::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEventBranch*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TEventBranch::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEventBranch*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr TEventTree::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TEventTree::Class_Name()
{
   return "xAOD::TEventTree";
}

//______________________________________________________________________________
const char *TEventTree::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEventTree*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TEventTree::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEventTree*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TEventTree::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEventTree*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TEventTree::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TEventTree*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr TMetaBranch::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TMetaBranch::Class_Name()
{
   return "xAOD::TMetaBranch";
}

//______________________________________________________________________________
const char *TMetaBranch::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TMetaBranch*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TMetaBranch::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TMetaBranch*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TMetaBranch::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TMetaBranch*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TMetaBranch::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TMetaBranch*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr TMetaTree::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TMetaTree::Class_Name()
{
   return "xAOD::TMetaTree";
}

//______________________________________________________________________________
const char *TMetaTree::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TMetaTree*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TMetaTree::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TMetaTree*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TMetaTree::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TMetaTree*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TMetaTree::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TMetaTree*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr TFileMerger::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TFileMerger::Class_Name()
{
   return "xAOD::TFileMerger";
}

//______________________________________________________________________________
const char *TFileMerger::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TFileMerger*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TFileMerger::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TFileMerger*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TFileMerger::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TFileMerger*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TFileMerger::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TFileMerger*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
atomic_TClass_ptr TFileChecker::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TFileChecker::Class_Name()
{
   return "xAOD::TFileChecker";
}

//______________________________________________________________________________
const char *TFileChecker::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TFileChecker*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TFileChecker::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TFileChecker*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TFileChecker::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TFileChecker*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TFileChecker::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAOD::TFileChecker*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAOD
namespace xAOD {
//______________________________________________________________________________
void TEventBranch::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::TEventBranch.

   TBranchObject::Streamer(R__b);
}

} // namespace xAOD
namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTEventBranch(void *p) {
      delete ((::xAOD::TEventBranch*)p);
   }
   static void deleteArray_xAODcLcLTEventBranch(void *p) {
      delete [] ((::xAOD::TEventBranch*)p);
   }
   static void destruct_xAODcLcLTEventBranch(void *p) {
      typedef ::xAOD::TEventBranch current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_xAODcLcLTEventBranch(TBuffer &buf, void *obj) {
      ((::xAOD::TEventBranch*)obj)->::xAOD::TEventBranch::Streamer(buf);
   }
   // Wrapper around the Reset function.
   static void reset_xAODcLcLTEventBranch(void *obj,TFileMergeInfo *info) {
      ((::xAOD::TEventBranch*)obj)->ResetAfterMerge(info);
   }
} // end of namespace ROOT for class ::xAOD::TEventBranch

namespace xAOD {
//______________________________________________________________________________
void TEventTree::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::TEventTree.

   TTree::Streamer(R__b);
}

} // namespace xAOD
namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTEventTree(void *p) {
      delete ((::xAOD::TEventTree*)p);
   }
   static void deleteArray_xAODcLcLTEventTree(void *p) {
      delete [] ((::xAOD::TEventTree*)p);
   }
   static void destruct_xAODcLcLTEventTree(void *p) {
      typedef ::xAOD::TEventTree current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around the directory auto add.
   static void directoryAutoAdd_xAODcLcLTEventTree(void *p, TDirectory *dir) {
      ((::xAOD::TEventTree*)p)->DirectoryAutoAdd(dir);
   }
   // Wrapper around a custom streamer member function.
   static void streamer_xAODcLcLTEventTree(TBuffer &buf, void *obj) {
      ((::xAOD::TEventTree*)obj)->::xAOD::TEventTree::Streamer(buf);
   }
   // Wrapper around the merge function.
   static Long64_t merge_xAODcLcLTEventTree(void *obj,TCollection *coll,TFileMergeInfo *info) {
      return ((::xAOD::TEventTree*)obj)->Merge(coll,info);
   }
   // Wrapper around the Reset function.
   static void reset_xAODcLcLTEventTree(void *obj,TFileMergeInfo *info) {
      ((::xAOD::TEventTree*)obj)->ResetAfterMerge(info);
   }
} // end of namespace ROOT for class ::xAOD::TEventTree

namespace xAOD {
//______________________________________________________________________________
void TMetaBranch::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::TMetaBranch.

   TBranchObject::Streamer(R__b);
}

} // namespace xAOD
namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTMetaBranch(void *p) {
      delete ((::xAOD::TMetaBranch*)p);
   }
   static void deleteArray_xAODcLcLTMetaBranch(void *p) {
      delete [] ((::xAOD::TMetaBranch*)p);
   }
   static void destruct_xAODcLcLTMetaBranch(void *p) {
      typedef ::xAOD::TMetaBranch current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_xAODcLcLTMetaBranch(TBuffer &buf, void *obj) {
      ((::xAOD::TMetaBranch*)obj)->::xAOD::TMetaBranch::Streamer(buf);
   }
   // Wrapper around the Reset function.
   static void reset_xAODcLcLTMetaBranch(void *obj,TFileMergeInfo *info) {
      ((::xAOD::TMetaBranch*)obj)->ResetAfterMerge(info);
   }
} // end of namespace ROOT for class ::xAOD::TMetaBranch

namespace xAOD {
//______________________________________________________________________________
void TMetaTree::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::TMetaTree.

   TTree::Streamer(R__b);
}

} // namespace xAOD
namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTMetaTree(void *p) {
      delete ((::xAOD::TMetaTree*)p);
   }
   static void deleteArray_xAODcLcLTMetaTree(void *p) {
      delete [] ((::xAOD::TMetaTree*)p);
   }
   static void destruct_xAODcLcLTMetaTree(void *p) {
      typedef ::xAOD::TMetaTree current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around the directory auto add.
   static void directoryAutoAdd_xAODcLcLTMetaTree(void *p, TDirectory *dir) {
      ((::xAOD::TMetaTree*)p)->DirectoryAutoAdd(dir);
   }
   // Wrapper around a custom streamer member function.
   static void streamer_xAODcLcLTMetaTree(TBuffer &buf, void *obj) {
      ((::xAOD::TMetaTree*)obj)->::xAOD::TMetaTree::Streamer(buf);
   }
   // Wrapper around the merge function.
   static Long64_t merge_xAODcLcLTMetaTree(void *obj,TCollection *coll,TFileMergeInfo *info) {
      return ((::xAOD::TMetaTree*)obj)->Merge(coll,info);
   }
   // Wrapper around the Reset function.
   static void reset_xAODcLcLTMetaTree(void *obj,TFileMergeInfo *info) {
      ((::xAOD::TMetaTree*)obj)->ResetAfterMerge(info);
   }
} // end of namespace ROOT for class ::xAOD::TMetaTree

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTTransTrees(void *p) {
      delete ((::xAOD::TTransTrees*)p);
   }
   static void deleteArray_xAODcLcLTTransTrees(void *p) {
      delete [] ((::xAOD::TTransTrees*)p);
   }
   static void destruct_xAODcLcLTTransTrees(void *p) {
      typedef ::xAOD::TTransTrees current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TTransTrees

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTReturnCode(void *p) {
      delete ((::xAOD::TReturnCode*)p);
   }
   static void deleteArray_xAODcLcLTReturnCode(void *p) {
      delete [] ((::xAOD::TReturnCode*)p);
   }
   static void destruct_xAODcLcLTReturnCode(void *p) {
      typedef ::xAOD::TReturnCode current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TReturnCode

namespace xAOD {
//______________________________________________________________________________
void TFileMerger::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::TFileMerger.

   TObject::Streamer(R__b);
}

} // namespace xAOD
namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTFileMerger(void *p) {
      return  p ? new(p) ::xAOD::TFileMerger : new ::xAOD::TFileMerger;
   }
   static void *newArray_xAODcLcLTFileMerger(Long_t nElements, void *p) {
      return p ? new(p) ::xAOD::TFileMerger[nElements] : new ::xAOD::TFileMerger[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTFileMerger(void *p) {
      delete ((::xAOD::TFileMerger*)p);
   }
   static void deleteArray_xAODcLcLTFileMerger(void *p) {
      delete [] ((::xAOD::TFileMerger*)p);
   }
   static void destruct_xAODcLcLTFileMerger(void *p) {
      typedef ::xAOD::TFileMerger current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_xAODcLcLTFileMerger(TBuffer &buf, void *obj) {
      ((::xAOD::TFileMerger*)obj)->::xAOD::TFileMerger::Streamer(buf);
   }
} // end of namespace ROOT for class ::xAOD::TFileMerger

namespace xAOD {
//______________________________________________________________________________
void TFileChecker::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAOD::TFileChecker.

   TObject::Streamer(R__b);
}

} // namespace xAOD
namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTFileChecker(void *p) {
      return  p ? new(p) ::xAOD::TFileChecker : new ::xAOD::TFileChecker;
   }
   static void *newArray_xAODcLcLTFileChecker(Long_t nElements, void *p) {
      return p ? new(p) ::xAOD::TFileChecker[nElements] : new ::xAOD::TFileChecker[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTFileChecker(void *p) {
      delete ((::xAOD::TFileChecker*)p);
   }
   static void deleteArray_xAODcLcLTFileChecker(void *p) {
      delete [] ((::xAOD::TFileChecker*)p);
   }
   static void destruct_xAODcLcLTFileChecker(void *p) {
      typedef ::xAOD::TFileChecker current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_xAODcLcLTFileChecker(TBuffer &buf, void *obj) {
      ((::xAOD::TFileChecker*)obj)->::xAOD::TFileChecker::Streamer(buf);
   }
} // end of namespace ROOT for class ::xAOD::TFileChecker

namespace {
  void TriggerDictionaryInitialization_xAODRootAccessCINT_Impl() {
    static const char* headers[] = {
"xAODRootAccess/tools/TEventBranch.h",
"xAODRootAccess/tools/TEventTree.h",
"xAODRootAccess/tools/TMetaBranch.h",
"xAODRootAccess/tools/TMetaTree.h",
"xAODRootAccess/tools/TTransTrees.h",
"xAODRootAccess/tools/TReturnCode.h",
"xAODRootAccess/tools/TFileMerger.h",
"xAODRootAccess/tools/TFileChecker.h",
"xAODRootAccess/MakeTransientTree.h",
"xAODRootAccess/Init.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root/LinkDef.h")))  TEventBranch;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root/LinkDef.h")))  TEventTree;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root/LinkDef.h")))  TMetaBranch;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root/LinkDef.h")))  TMetaTree;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root/LinkDef.h")))  TTransTrees;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root/LinkDef.h")))  TReturnCode;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root/LinkDef.h")))  TFileMerger;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODRootAccess/Root/LinkDef.h")))  TFileChecker;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODRootAccess"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "xAODRootAccess/tools/TEventBranch.h"
#include "xAODRootAccess/tools/TEventTree.h"
#include "xAODRootAccess/tools/TMetaBranch.h"
#include "xAODRootAccess/tools/TMetaTree.h"
#include "xAODRootAccess/tools/TTransTrees.h"
#include "xAODRootAccess/tools/TReturnCode.h"
#include "xAODRootAccess/tools/TFileMerger.h"
#include "xAODRootAccess/tools/TFileChecker.h"
#include "xAODRootAccess/MakeTransientTree.h"
#include "xAODRootAccess/Init.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xAOD::ClearTransientMetaTree", payloadCode, "@",
"xAOD::ClearTransientTree", payloadCode, "@",
"xAOD::ClearTransientTrees", payloadCode, "@",
"xAOD::Init", payloadCode, "@",
"xAOD::TEventBranch", payloadCode, "@",
"xAOD::TEventTree", payloadCode, "@",
"xAOD::TFileChecker", payloadCode, "@",
"xAOD::TFileMerger", payloadCode, "@",
"xAOD::TMetaBranch", payloadCode, "@",
"xAOD::TMetaTree", payloadCode, "@",
"xAOD::TReturnCode", payloadCode, "@",
"xAOD::TTransTrees", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODRootAccessCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODRootAccessCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODRootAccessCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODRootAccessCINT() {
  TriggerDictionaryInitialization_xAODRootAccessCINT_Impl();
}

// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODMetaDataCnvdIobjdIxAODMetaDataCnv_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMetaDataCnv/xAODMetaDataCnv/xAODMetaDataCnvDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODMakercLcLFileMetaDataTool_Dictionary();
   static void xAODMakercLcLFileMetaDataTool_TClassManip(TClass*);
   static void *new_xAODMakercLcLFileMetaDataTool(void *p = 0);
   static void *newArray_xAODMakercLcLFileMetaDataTool(Long_t size, void *p);
   static void delete_xAODMakercLcLFileMetaDataTool(void *p);
   static void deleteArray_xAODMakercLcLFileMetaDataTool(void *p);
   static void destruct_xAODMakercLcLFileMetaDataTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAODMaker::FileMetaDataTool*)
   {
      ::xAODMaker::FileMetaDataTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAODMaker::FileMetaDataTool));
      static ::ROOT::TGenericClassInfo 
         instance("xAODMaker::FileMetaDataTool", "xAODMetaDataCnv/FileMetaDataTool.h", 36,
                  typeid(::xAODMaker::FileMetaDataTool), DefineBehavior(ptr, ptr),
                  &xAODMakercLcLFileMetaDataTool_Dictionary, isa_proxy, 0,
                  sizeof(::xAODMaker::FileMetaDataTool) );
      instance.SetNew(&new_xAODMakercLcLFileMetaDataTool);
      instance.SetNewArray(&newArray_xAODMakercLcLFileMetaDataTool);
      instance.SetDelete(&delete_xAODMakercLcLFileMetaDataTool);
      instance.SetDeleteArray(&deleteArray_xAODMakercLcLFileMetaDataTool);
      instance.SetDestructor(&destruct_xAODMakercLcLFileMetaDataTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAODMaker::FileMetaDataTool*)
   {
      return GenerateInitInstanceLocal((::xAODMaker::FileMetaDataTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAODMaker::FileMetaDataTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODMakercLcLFileMetaDataTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAODMaker::FileMetaDataTool*)0x0)->GetClass();
      xAODMakercLcLFileMetaDataTool_TClassManip(theClass);
   return theClass;
   }

   static void xAODMakercLcLFileMetaDataTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODMakercLcLFileMetaDataTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAODMaker::FileMetaDataTool : new ::xAODMaker::FileMetaDataTool;
   }
   static void *newArray_xAODMakercLcLFileMetaDataTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAODMaker::FileMetaDataTool[nElements] : new ::xAODMaker::FileMetaDataTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODMakercLcLFileMetaDataTool(void *p) {
      delete ((::xAODMaker::FileMetaDataTool*)p);
   }
   static void deleteArray_xAODMakercLcLFileMetaDataTool(void *p) {
      delete [] ((::xAODMaker::FileMetaDataTool*)p);
   }
   static void destruct_xAODMakercLcLFileMetaDataTool(void *p) {
      typedef ::xAODMaker::FileMetaDataTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAODMaker::FileMetaDataTool

namespace {
  void TriggerDictionaryInitialization_xAODMetaDataCnv_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMetaDataCnv/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMetaDataCnv",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMetaDataCnv/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAODMaker{class __attribute__((annotate("$clingAutoload$xAODMetaDataCnv/FileMetaDataTool.h")))  FileMetaDataTool;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODMetaDataCnv"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODMetaDataCnvDict.h 683697 2015-07-17 09:12:14Z krasznaa $
#ifndef XAODMETADATACNV_XAODMETADATACNVDICT_H
#define XAODMETADATACNV_XAODMETADATACNVDICT_H

// Local include(s):
#include "xAODMetaDataCnv/FileMetaDataTool.h"

#endif // XAODMETADATACNV_XAODMETADATACNVDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xAODMaker::FileMetaDataTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODMetaDataCnv_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODMetaDataCnv_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODMetaDataCnv_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODMetaDataCnv_Reflex() {
  TriggerDictionaryInitialization_xAODMetaDataCnv_Reflex_Impl();
}

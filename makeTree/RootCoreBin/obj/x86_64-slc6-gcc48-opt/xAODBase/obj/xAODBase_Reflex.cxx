// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODBasedIobjdIxAODBase_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODBase/xAODBase/xAODBaseDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLIParticle_Dictionary();
   static void xAODcLcLIParticle_TClassManip(TClass*);
   static void delete_xAODcLcLIParticle(void *p);
   static void deleteArray_xAODcLcLIParticle(void *p);
   static void destruct_xAODcLcLIParticle(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::IParticle*)
   {
      ::xAOD::IParticle *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::IParticle));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::IParticle", "xAODBase/IParticle.h", 35,
                  typeid(::xAOD::IParticle), DefineBehavior(ptr, ptr),
                  &xAODcLcLIParticle_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::IParticle) );
      instance.SetDelete(&delete_xAODcLcLIParticle);
      instance.SetDeleteArray(&deleteArray_xAODcLcLIParticle);
      instance.SetDestructor(&destruct_xAODcLcLIParticle);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::IParticle*)
   {
      return GenerateInitInstanceLocal((::xAOD::IParticle*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::IParticle*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLIParticle_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::IParticle*)0x0)->GetClass();
      xAODcLcLIParticle_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLIParticle_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLIParticlegR_Dictionary();
   static void DataVectorlExAODcLcLIParticlegR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLIParticlegR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLIParticlegR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLIParticlegR(void *p);
   static void deleteArray_DataVectorlExAODcLcLIParticlegR(void *p);
   static void destruct_DataVectorlExAODcLcLIParticlegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::IParticle>*)
   {
      ::DataVector<xAOD::IParticle> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::IParticle>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::IParticle>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::IParticle>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLIParticlegR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::IParticle>) );
      instance.SetNew(&new_DataVectorlExAODcLcLIParticlegR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLIParticlegR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLIParticlegR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLIParticlegR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLIParticlegR);

      ROOT::AddClassAlternate("DataVector<xAOD::IParticle>","xAOD::IParticleContainer");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::IParticle>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::IParticle>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::IParticle>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLIParticlegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::IParticle>*)0x0)->GetClass();
      DataVectorlExAODcLcLIParticlegR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLIParticlegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::IParticle> >*)
   {
      ::DataLink<DataVector<xAOD::IParticle> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::IParticle> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::IParticle> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::IParticle> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::IParticle> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::IParticle> >","DataLink<xAOD::IParticleContainer>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::IParticle> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::IParticle> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::IParticle> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::IParticle> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::IParticle> >*)
   {
      ::ElementLink<DataVector<xAOD::IParticle> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::IParticle> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::IParticle> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::IParticle> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::IParticle> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::IParticle> >","ElementLink<xAOD::IParticleContainer>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::IParticle> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::IParticle> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::IParticle> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::IParticle> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary();
   static void ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(TClass*);
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(void *p = 0);
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(Long_t size, void *p);
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(void *p);
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(void *p);
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLinkVector<DataVector<xAOD::IParticle> >*)
   {
      ::ElementLinkVector<DataVector<xAOD::IParticle> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLinkVector<DataVector<xAOD::IParticle> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLinkVector<DataVector<xAOD::IParticle> >", "AthLinks/ElementLinkVector.h", 34,
                  typeid(::ElementLinkVector<DataVector<xAOD::IParticle> >), DefineBehavior(ptr, ptr),
                  &ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLinkVector<DataVector<xAOD::IParticle> >) );
      instance.SetNew(&new_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetNewArray(&newArray_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDelete(&delete_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR);
      instance.SetDestructor(&destruct_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR);

      ROOT::AddClassAlternate("ElementLinkVector<DataVector<xAOD::IParticle> >","ElementLinkVector<xAOD::IParticleContainer>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLinkVector<DataVector<xAOD::IParticle> >*)
   {
      return GenerateInitInstanceLocal((::ElementLinkVector<DataVector<xAOD::IParticle> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::IParticle> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::IParticle> >*)0x0)->GetClass();
      ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLIParticle(void *p) {
      delete ((::xAOD::IParticle*)p);
   }
   static void deleteArray_xAODcLcLIParticle(void *p) {
      delete [] ((::xAOD::IParticle*)p);
   }
   static void destruct_xAODcLcLIParticle(void *p) {
      typedef ::xAOD::IParticle current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::IParticle

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLIParticlegR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::IParticle> : new ::DataVector<xAOD::IParticle>;
   }
   static void *newArray_DataVectorlExAODcLcLIParticlegR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::IParticle>[nElements] : new ::DataVector<xAOD::IParticle>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLIParticlegR(void *p) {
      delete ((::DataVector<xAOD::IParticle>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLIParticlegR(void *p) {
      delete [] ((::DataVector<xAOD::IParticle>*)p);
   }
   static void destruct_DataVectorlExAODcLcLIParticlegR(void *p) {
      typedef ::DataVector<xAOD::IParticle> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::IParticle>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::IParticle> > : new ::DataLink<DataVector<xAOD::IParticle> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::IParticle> >[nElements] : new ::DataLink<DataVector<xAOD::IParticle> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::IParticle> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::IParticle> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::IParticle> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::IParticle> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::IParticle> > : new ::ElementLink<DataVector<xAOD::IParticle> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::IParticle> >[nElements] : new ::ElementLink<DataVector<xAOD::IParticle> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::IParticle> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::IParticle> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::IParticle> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::IParticle> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      return  p ? new(p) ::ElementLinkVector<DataVector<xAOD::IParticle> > : new ::ElementLinkVector<DataVector<xAOD::IParticle> >;
   }
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLinkVector<DataVector<xAOD::IParticle> >[nElements] : new ::ElementLinkVector<DataVector<xAOD::IParticle> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      delete ((::ElementLinkVector<DataVector<xAOD::IParticle> >*)p);
   }
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      delete [] ((::ElementLinkVector<DataVector<xAOD::IParticle> >*)p);
   }
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgR(void *p) {
      typedef ::ElementLinkVector<DataVector<xAOD::IParticle> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLinkVector<DataVector<xAOD::IParticle> >

namespace ROOT {
   static TClass *vectorlExAODcLcLIParticlemUgR_Dictionary();
   static void vectorlExAODcLcLIParticlemUgR_TClassManip(TClass*);
   static void *new_vectorlExAODcLcLIParticlemUgR(void *p = 0);
   static void *newArray_vectorlExAODcLcLIParticlemUgR(Long_t size, void *p);
   static void delete_vectorlExAODcLcLIParticlemUgR(void *p);
   static void deleteArray_vectorlExAODcLcLIParticlemUgR(void *p);
   static void destruct_vectorlExAODcLcLIParticlemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<xAOD::IParticle*>*)
   {
      vector<xAOD::IParticle*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<xAOD::IParticle*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<xAOD::IParticle*>", -2, "vector", 210,
                  typeid(vector<xAOD::IParticle*>), DefineBehavior(ptr, ptr),
                  &vectorlExAODcLcLIParticlemUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<xAOD::IParticle*>) );
      instance.SetNew(&new_vectorlExAODcLcLIParticlemUgR);
      instance.SetNewArray(&newArray_vectorlExAODcLcLIParticlemUgR);
      instance.SetDelete(&delete_vectorlExAODcLcLIParticlemUgR);
      instance.SetDeleteArray(&deleteArray_vectorlExAODcLcLIParticlemUgR);
      instance.SetDestructor(&destruct_vectorlExAODcLcLIParticlemUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<xAOD::IParticle*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<xAOD::IParticle*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlExAODcLcLIParticlemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<xAOD::IParticle*>*)0x0)->GetClass();
      vectorlExAODcLcLIParticlemUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlExAODcLcLIParticlemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlExAODcLcLIParticlemUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<xAOD::IParticle*> : new vector<xAOD::IParticle*>;
   }
   static void *newArray_vectorlExAODcLcLIParticlemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<xAOD::IParticle*>[nElements] : new vector<xAOD::IParticle*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlExAODcLcLIParticlemUgR(void *p) {
      delete ((vector<xAOD::IParticle*>*)p);
   }
   static void deleteArray_vectorlExAODcLcLIParticlemUgR(void *p) {
      delete [] ((vector<xAOD::IParticle*>*)p);
   }
   static void destruct_vectorlExAODcLcLIParticlemUgR(void *p) {
      typedef vector<xAOD::IParticle*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<xAOD::IParticle*>

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > : new vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >

namespace ROOT {
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary();
   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLinkVector<DataVector<xAOD::IParticle> > >*)
   {
      vector<ElementLinkVector<DataVector<xAOD::IParticle> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLinkVector<DataVector<xAOD::IParticle> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLinkVector<DataVector<xAOD::IParticle> > >", -2, "vector", 210,
                  typeid(vector<ElementLinkVector<DataVector<xAOD::IParticle> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLinkVector<DataVector<xAOD::IParticle> > >) );
      instance.SetNew(&new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLinkVector<DataVector<xAOD::IParticle> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::IParticle> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::IParticle> > >*)0x0)->GetClass();
      vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::IParticle> > > : new vector<ElementLinkVector<DataVector<xAOD::IParticle> > >;
   }
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::IParticle> > >[nElements] : new vector<ElementLinkVector<DataVector<xAOD::IParticle> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete ((vector<ElementLinkVector<DataVector<xAOD::IParticle> > >*)p);
   }
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLinkVector<DataVector<xAOD::IParticle> > >*)p);
   }
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      typedef vector<ElementLinkVector<DataVector<xAOD::IParticle> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLinkVector<DataVector<xAOD::IParticle> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::IParticle> > >*)
   {
      vector<ElementLink<DataVector<xAOD::IParticle> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::IParticle> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::IParticle> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::IParticle> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::IParticle> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::IParticle> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::IParticle> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::IParticle> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::IParticle> > > : new vector<ElementLink<DataVector<xAOD::IParticle> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::IParticle> > >[nElements] : new vector<ElementLink<DataVector<xAOD::IParticle> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::IParticle> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::IParticle> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::IParticle> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::IParticle> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::IParticle> > >*)
   {
      vector<DataLink<DataVector<xAOD::IParticle> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::IParticle> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::IParticle> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::IParticle> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::IParticle> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::IParticle> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::IParticle> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::IParticle> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::IParticle> > > : new vector<DataLink<DataVector<xAOD::IParticle> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::IParticle> > >[nElements] : new vector<DataLink<DataVector<xAOD::IParticle> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::IParticle> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::IParticle> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::IParticle> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::IParticle> > >

namespace {
  void TriggerDictionaryInitialization_xAODBase_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODBase/Root",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODBase",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODBase/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODBase/IParticleContainer.h")))  IParticle;}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODBase"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODBaseDict.h 618909 2014-09-29 10:16:52Z krasznaa $
#ifndef XAODBASE_XAODBASEDICT_H
#define XAODBASE_XAODBASEDICT_H

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
#include "xAODBase/ObjectType.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticleHelpers.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODBASE {
      xAOD::IParticleContainer c1;
      DataLink< xAOD::IParticleContainer > l1;
      ElementLink< xAOD::IParticleContainer > l2;
      ElementLinkVector< xAOD::IParticleContainer > l3;
      std::vector< DataLink< xAOD::IParticleContainer > > l4;
      std::vector< ElementLink< xAOD::IParticleContainer > > l5;
      std::vector< ElementLinkVector< xAOD::IParticleContainer > > l6;
      std::vector< std::vector< ElementLink< xAOD::IParticleContainer > > > l7;
   };
}

template
bool& xAOD::IParticle::auxdata< bool >( const std::string& name,
                                        const std::string& clsname = "" );

template
float& xAOD::IParticle::auxdata< float >( const std::string& name,
                                          const std::string& clsname = "" );

template
int& xAOD::IParticle::auxdata< int >( const std::string& name,
                                      const std::string& clsname = "" );

template
unsigned int&
xAOD::IParticle::auxdata< unsigned int >( const std::string& name,
                                          const std::string& clsname = "" );

template
uint8_t& xAOD::IParticle::auxdata< uint8_t >( const std::string& name,
                                              const std::string& clsname = "" );

template
const bool&
xAOD::IParticle::auxdata< bool >( const std::string& name,
                                  const std::string& clsname = "" ) const;

template
const float&
xAOD::IParticle::auxdata< float >( const std::string& name,
                                   const std::string& clsname = "" ) const;

template
const int&
xAOD::IParticle::auxdata< int >( const std::string& name,
                                 const std::string& clsname = "" ) const;

template
const unsigned int&
xAOD::IParticle::auxdata< unsigned int >( const std::string& name,
                                          const std::string& clsname = "" ) const;

template
const uint8_t&
xAOD::IParticle::auxdata< uint8_t >( const std::string& name,
                                     const std::string& clsname = "" ) const;

template
bool
xAOD::IParticle::isAvailable< bool >( const std::string& name,
                                      const std::string& clsname = "" ) const;

template
bool
xAOD::IParticle::isAvailable< float >( const std::string& name,
                                       const std::string& clsname = "" ) const;

template
bool
xAOD::IParticle::isAvailable< int >( const std::string& name,
                                     const std::string& clsname = "" ) const;

template
bool
xAOD::IParticle::isAvailable< unsigned int >( const std::string& name,
                                              const std::string& clsname = "" ) const;

template
bool
xAOD::IParticle::isAvailable< uint8_t >( const std::string& name,
                                         const std::string& clsname = "" ) const;

#endif // XAODBASE_XAODBASEDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::IParticle> >", payloadCode, "@",
"DataLink<xAOD::IParticleContainer>", payloadCode, "@",
"DataVector<xAOD::IParticle>", payloadCode, "@",
"ElementLink<DataVector<xAOD::IParticle> >", payloadCode, "@",
"ElementLink<xAOD::IParticleContainer>", payloadCode, "@",
"ElementLinkVector<DataVector<xAOD::IParticle> >", payloadCode, "@",
"ElementLinkVector<xAOD::IParticleContainer>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::IParticle> > >", payloadCode, "@",
"vector<DataLink<xAOD::IParticleContainer> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::IParticle> > >", payloadCode, "@",
"vector<ElementLink<xAOD::IParticleContainer> >", payloadCode, "@",
"vector<ElementLinkVector<DataVector<xAOD::IParticle> > >", payloadCode, "@",
"vector<ElementLinkVector<xAOD::IParticleContainer> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::IParticleContainer> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >", payloadCode, "@",
"vector<xAOD::IParticle*>", payloadCode, "@",
"xAOD::IParticle", payloadCode, "@",
"xAOD::IParticleContainer", payloadCode, "@",
"xAOD::Type::ObjectType", payloadCode, "@",
"xAOD::getOriginalObject", payloadCode, "@",
"xAOD::setOriginalObjectLink", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODBase_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODBase_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODBase_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODBase_Reflex() {
  TriggerDictionaryInitialization_xAODBase_Reflex_Impl();
}

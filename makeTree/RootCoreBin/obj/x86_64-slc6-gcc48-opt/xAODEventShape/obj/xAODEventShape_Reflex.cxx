// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODEventShapedIobjdIxAODEventShape_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventShape/xAODEventShape/xAODEventShapeDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLEventShape_v1_Dictionary();
   static void xAODcLcLEventShape_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEventShape_v1(void *p = 0);
   static void *newArray_xAODcLcLEventShape_v1(Long_t size, void *p);
   static void delete_xAODcLcLEventShape_v1(void *p);
   static void deleteArray_xAODcLcLEventShape_v1(void *p);
   static void destruct_xAODcLcLEventShape_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLEventShape_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::EventShape_v1* newObj = (xAOD::EventShape_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
         newObj->setStore( ( SG::IAuxStore* ) 0 );
      
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EventShape_v1*)
   {
      ::xAOD::EventShape_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EventShape_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EventShape_v1", "xAODEventShape/versions/EventShape_v1.h", 23,
                  typeid(::xAOD::EventShape_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEventShape_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EventShape_v1) );
      instance.SetNew(&new_xAODcLcLEventShape_v1);
      instance.SetNewArray(&newArray_xAODcLcLEventShape_v1);
      instance.SetDelete(&delete_xAODcLcLEventShape_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEventShape_v1);
      instance.SetDestructor(&destruct_xAODcLcLEventShape_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::EventShape_v1";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLEventShape_v1_0);
      rule->fCode        = "\n         newObj->setStore( ( SG::IAuxStore* ) 0 );\n      ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EventShape_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EventShape_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EventShape_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEventShape_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EventShape_v1*)0x0)->GetClass();
      xAODcLcLEventShape_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEventShape_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E06E8747-7C16-4BBA-B648-68F64BA70B7D");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEventShapeAuxInfo_v1_Dictionary();
   static void xAODcLcLEventShapeAuxInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEventShapeAuxInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLEventShapeAuxInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLEventShapeAuxInfo_v1(void *p);
   static void deleteArray_xAODcLcLEventShapeAuxInfo_v1(void *p);
   static void destruct_xAODcLcLEventShapeAuxInfo_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EventShapeAuxInfo_v1*)
   {
      ::xAOD::EventShapeAuxInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EventShapeAuxInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EventShapeAuxInfo_v1", "xAODEventShape/versions/EventShapeAuxInfo_v1.h", 17,
                  typeid(::xAOD::EventShapeAuxInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEventShapeAuxInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EventShapeAuxInfo_v1) );
      instance.SetNew(&new_xAODcLcLEventShapeAuxInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLEventShapeAuxInfo_v1);
      instance.SetDelete(&delete_xAODcLcLEventShapeAuxInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEventShapeAuxInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLEventShapeAuxInfo_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EventShapeAuxInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EventShapeAuxInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EventShapeAuxInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEventShapeAuxInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EventShapeAuxInfo_v1*)0x0)->GetClass();
      xAODcLcLEventShapeAuxInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEventShapeAuxInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","7C9459AC-B89C-43A1-A57A-FF035EAA5D81");
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEventShape_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventShape_v1 : new ::xAOD::EventShape_v1;
   }
   static void *newArray_xAODcLcLEventShape_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventShape_v1[nElements] : new ::xAOD::EventShape_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEventShape_v1(void *p) {
      delete ((::xAOD::EventShape_v1*)p);
   }
   static void deleteArray_xAODcLcLEventShape_v1(void *p) {
      delete [] ((::xAOD::EventShape_v1*)p);
   }
   static void destruct_xAODcLcLEventShape_v1(void *p) {
      typedef ::xAOD::EventShape_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EventShape_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEventShapeAuxInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventShapeAuxInfo_v1 : new ::xAOD::EventShapeAuxInfo_v1;
   }
   static void *newArray_xAODcLcLEventShapeAuxInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventShapeAuxInfo_v1[nElements] : new ::xAOD::EventShapeAuxInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEventShapeAuxInfo_v1(void *p) {
      delete ((::xAOD::EventShapeAuxInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLEventShapeAuxInfo_v1(void *p) {
      delete [] ((::xAOD::EventShapeAuxInfo_v1*)p);
   }
   static void destruct_xAODcLcLEventShapeAuxInfo_v1(void *p) {
      typedef ::xAOD::EventShapeAuxInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EventShapeAuxInfo_v1

namespace {
  void TriggerDictionaryInitialization_xAODEventShape_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventShape/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventShape/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@E06E8747-7C16-4BBA-B648-68F64BA70B7D)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEventShape/versions/EventShape_v1.h")))  EventShape_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@7C9459AC-B89C-43A1-A57A-FF035EAA5D81)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEventShape/versions/EventShapeAuxInfo_v1.h")))  EventShapeAuxInfo_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODEventShape"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODEventShapeDict.h 629489 2014-11-19 14:37:55Z krasznaa $
#ifndef XAODEVENTSHAPE_XAODEVENTSHAPEDICT_H
#define XAODEVENTSHAPE_XAODEVENTSHAPEDICT_H

// Includes for the dictionary generation:
#include "xAODEventShape/versions/EventShape_v1.h"
#include "xAODEventShape/versions/EventShapeAuxInfo_v1.h"

#endif // XAODEVENTSHAPE_XAODEVENTSHAPEDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xAOD::EventShapeAuxInfo_v1", payloadCode, "@",
"xAOD::EventShape_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODEventShape_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODEventShape_Reflex_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODEventShape_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODEventShape_Reflex() {
  TriggerDictionaryInitialization_xAODEventShape_Reflex_Impl();
}

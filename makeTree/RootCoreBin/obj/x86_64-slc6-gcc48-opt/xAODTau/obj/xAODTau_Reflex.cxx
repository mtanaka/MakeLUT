// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTaudIobjdIxAODTau_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTau/xAODTau/xAODTauDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTauJet_v1_Dictionary();
   static void xAODcLcLTauJet_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTauJet_v1(void *p = 0);
   static void *newArray_xAODcLcLTauJet_v1(Long_t size, void *p);
   static void delete_xAODcLcLTauJet_v1(void *p);
   static void deleteArray_xAODcLcLTauJet_v1(void *p);
   static void destruct_xAODcLcLTauJet_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTauJet_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TauJet_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::TauJet_v1* newObj = (xAOD::TauJet_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TauJet_v1*)
   {
      ::xAOD::TauJet_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TauJet_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TauJet_v1", "xAODTau/versions/TauJet_v1.h", 30,
                  typeid(::xAOD::TauJet_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTauJet_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TauJet_v1) );
      instance.SetNew(&new_xAODcLcLTauJet_v1);
      instance.SetNewArray(&newArray_xAODcLcLTauJet_v1);
      instance.SetDelete(&delete_xAODcLcLTauJet_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTauJet_v1);
      instance.SetDestructor(&destruct_xAODcLcLTauJet_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TauJet_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTauJet_v1_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TauJet_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TauJet_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TauJet_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTauJet_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TauJet_v1*)0x0)->GetClass();
      xAODcLcLTauJet_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTauJet_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTauJet_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTauJet_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTauJet_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTauJet_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTauJet_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTauJet_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTauJet_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TauJet_v1>*)
   {
      ::DataVector<xAOD::TauJet_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TauJet_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TauJet_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TauJet_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTauJet_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TauJet_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTauJet_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTauJet_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTauJet_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTauJet_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTauJet_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TauJet_v1>","xAOD::TauJetContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TauJet_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TauJet_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TauJet_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTauJet_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TauJet_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTauJet_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTauJet_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","93CCE680-47C0-11E3-997C-02163E00A614");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTauJetAuxContainer_v1_Dictionary();
   static void xAODcLcLTauJetAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTauJetAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTauJetAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTauJetAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTauJetAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTauJetAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TauJetAuxContainer_v1*)
   {
      ::xAOD::TauJetAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TauJetAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TauJetAuxContainer_v1", "xAODTau/versions/TauJetAuxContainer_v1.h", 35,
                  typeid(::xAOD::TauJetAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTauJetAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TauJetAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTauJetAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTauJetAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTauJetAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTauJetAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTauJetAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TauJetAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TauJetAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TauJetAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTauJetAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TauJetAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTauJetAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTauJetAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","EA3CE9A0-18D8-49FD-B978-62857D8D8FD0");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TauJet_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TauJet_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TauJet_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TauJet_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TauJet_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TauJet_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TauJet_v1> >","DataLink<xAOD::TauJetContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TauJet_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TauJet_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TauJet_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TauJet_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TauJet_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TauJet_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TauJet_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TauJet_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TauJet_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TauJet_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TauJet_v1> >","ElementLink<xAOD::TauJetContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TauJet_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TauJet_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TauJet_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TauJet_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTauJet_v2_Dictionary();
   static void xAODcLcLTauJet_v2_TClassManip(TClass*);
   static void *new_xAODcLcLTauJet_v2(void *p = 0);
   static void *newArray_xAODcLcLTauJet_v2(Long_t size, void *p);
   static void delete_xAODcLcLTauJet_v2(void *p);
   static void deleteArray_xAODcLcLTauJet_v2(void *p);
   static void destruct_xAODcLcLTauJet_v2(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTauJet_v2_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TauJet_v2");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::TauJet_v2* newObj = (xAOD::TauJet_v2*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TauJet_v2*)
   {
      ::xAOD::TauJet_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TauJet_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TauJet_v2", "xAODTau/versions/TauJet_v2.h", 30,
                  typeid(::xAOD::TauJet_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLTauJet_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TauJet_v2) );
      instance.SetNew(&new_xAODcLcLTauJet_v2);
      instance.SetNewArray(&newArray_xAODcLcLTauJet_v2);
      instance.SetDelete(&delete_xAODcLcLTauJet_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTauJet_v2);
      instance.SetDestructor(&destruct_xAODcLcLTauJet_v2);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TauJet_v2";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTauJet_v2_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TauJet_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::TauJet_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TauJet_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTauJet_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TauJet_v2*)0x0)->GetClass();
      xAODcLcLTauJet_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTauJet_v2_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTauJet_v2gR_Dictionary();
   static void DataVectorlExAODcLcLTauJet_v2gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTauJet_v2gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTauJet_v2gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTauJet_v2gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTauJet_v2gR(void *p);
   static void destruct_DataVectorlExAODcLcLTauJet_v2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TauJet_v2>*)
   {
      ::DataVector<xAOD::TauJet_v2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TauJet_v2>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TauJet_v2>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TauJet_v2>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTauJet_v2gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TauJet_v2>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTauJet_v2gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTauJet_v2gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTauJet_v2gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTauJet_v2gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTauJet_v2gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TauJet_v2>","xAOD::TauJetContainer_v2");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TauJet_v2>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TauJet_v2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TauJet_v2>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTauJet_v2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TauJet_v2>*)0x0)->GetClass();
      DataVectorlExAODcLcLTauJet_v2gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTauJet_v2gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","AACF5DF5-2D1A-4678-9188-756C27314E2F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTauJetAuxContainer_v2_Dictionary();
   static void xAODcLcLTauJetAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLTauJetAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLTauJetAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLTauJetAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLTauJetAuxContainer_v2(void *p);
   static void destruct_xAODcLcLTauJetAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TauJetAuxContainer_v2*)
   {
      ::xAOD::TauJetAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TauJetAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TauJetAuxContainer_v2", "xAODTau/versions/TauJetAuxContainer_v2.h", 35,
                  typeid(::xAOD::TauJetAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLTauJetAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TauJetAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLTauJetAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLTauJetAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLTauJetAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTauJetAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLTauJetAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TauJetAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::TauJetAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TauJetAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTauJetAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TauJetAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLTauJetAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTauJetAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","2853B3D8-136E-444D-AB48-24B1A0E13083");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TauJet_v2> >*)
   {
      ::DataLink<DataVector<xAOD::TauJet_v2> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TauJet_v2> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TauJet_v2> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TauJet_v2> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TauJet_v2> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TauJet_v2> >","DataLink<xAOD::TauJetContainer_v2>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TauJet_v2> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TauJet_v2> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TauJet_v2> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TauJet_v2> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TauJet_v2> >*)
   {
      ::ElementLink<DataVector<xAOD::TauJet_v2> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TauJet_v2> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TauJet_v2> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TauJet_v2> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TauJet_v2> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TauJet_v2> >","ElementLink<xAOD::TauJetContainer_v2>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TauJet_v2> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TauJet_v2> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TauJet_v2> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TauJet_v2> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTauJet_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TauJet_v1 : new ::xAOD::TauJet_v1;
   }
   static void *newArray_xAODcLcLTauJet_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TauJet_v1[nElements] : new ::xAOD::TauJet_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTauJet_v1(void *p) {
      delete ((::xAOD::TauJet_v1*)p);
   }
   static void deleteArray_xAODcLcLTauJet_v1(void *p) {
      delete [] ((::xAOD::TauJet_v1*)p);
   }
   static void destruct_xAODcLcLTauJet_v1(void *p) {
      typedef ::xAOD::TauJet_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TauJet_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTauJet_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TauJet_v1> : new ::DataVector<xAOD::TauJet_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTauJet_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TauJet_v1>[nElements] : new ::DataVector<xAOD::TauJet_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTauJet_v1gR(void *p) {
      delete ((::DataVector<xAOD::TauJet_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTauJet_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TauJet_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTauJet_v1gR(void *p) {
      typedef ::DataVector<xAOD::TauJet_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TauJet_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTauJetAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TauJetAuxContainer_v1 : new ::xAOD::TauJetAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTauJetAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TauJetAuxContainer_v1[nElements] : new ::xAOD::TauJetAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTauJetAuxContainer_v1(void *p) {
      delete ((::xAOD::TauJetAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTauJetAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TauJetAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTauJetAuxContainer_v1(void *p) {
      typedef ::xAOD::TauJetAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TauJetAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TauJet_v1> > : new ::DataLink<DataVector<xAOD::TauJet_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TauJet_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TauJet_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TauJet_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TauJet_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TauJet_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TauJet_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TauJet_v1> > : new ::ElementLink<DataVector<xAOD::TauJet_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TauJet_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TauJet_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TauJet_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TauJet_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TauJet_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TauJet_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTauJet_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TauJet_v2 : new ::xAOD::TauJet_v2;
   }
   static void *newArray_xAODcLcLTauJet_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TauJet_v2[nElements] : new ::xAOD::TauJet_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTauJet_v2(void *p) {
      delete ((::xAOD::TauJet_v2*)p);
   }
   static void deleteArray_xAODcLcLTauJet_v2(void *p) {
      delete [] ((::xAOD::TauJet_v2*)p);
   }
   static void destruct_xAODcLcLTauJet_v2(void *p) {
      typedef ::xAOD::TauJet_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TauJet_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTauJet_v2gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TauJet_v2> : new ::DataVector<xAOD::TauJet_v2>;
   }
   static void *newArray_DataVectorlExAODcLcLTauJet_v2gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TauJet_v2>[nElements] : new ::DataVector<xAOD::TauJet_v2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTauJet_v2gR(void *p) {
      delete ((::DataVector<xAOD::TauJet_v2>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTauJet_v2gR(void *p) {
      delete [] ((::DataVector<xAOD::TauJet_v2>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTauJet_v2gR(void *p) {
      typedef ::DataVector<xAOD::TauJet_v2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TauJet_v2>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTauJetAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TauJetAuxContainer_v2 : new ::xAOD::TauJetAuxContainer_v2;
   }
   static void *newArray_xAODcLcLTauJetAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TauJetAuxContainer_v2[nElements] : new ::xAOD::TauJetAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTauJetAuxContainer_v2(void *p) {
      delete ((::xAOD::TauJetAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLTauJetAuxContainer_v2(void *p) {
      delete [] ((::xAOD::TauJetAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLTauJetAuxContainer_v2(void *p) {
      typedef ::xAOD::TauJetAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TauJetAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TauJet_v2> > : new ::DataLink<DataVector<xAOD::TauJet_v2> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TauJet_v2> >[nElements] : new ::DataLink<DataVector<xAOD::TauJet_v2> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TauJet_v2> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TauJet_v2> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TauJet_v2> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TauJet_v2> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TauJet_v2> > : new ::ElementLink<DataVector<xAOD::TauJet_v2> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TauJet_v2> >[nElements] : new ::ElementLink<DataVector<xAOD::TauJet_v2> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TauJet_v2> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TauJet_v2> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TauJet_v2> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TauJet_v2> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > > : new vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TauJet_v2> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TauJet_v2> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TauJet_v2> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TauJet_v2> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TauJet_v2> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TauJet_v2> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TauJet_v2> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TauJet_v2> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TauJet_v2> > > : new vector<ElementLink<DataVector<xAOD::TauJet_v2> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TauJet_v2> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TauJet_v2> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TauJet_v2> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TauJet_v2> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TauJet_v2> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TauJet_v2> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TauJet_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TauJet_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TauJet_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TauJet_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TauJet_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TauJet_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TauJet_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TauJet_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TauJet_v1> > > : new vector<ElementLink<DataVector<xAOD::TauJet_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TauJet_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TauJet_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TauJet_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TauJet_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TauJet_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TauJet_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TauJet_v2> > >*)
   {
      vector<DataLink<DataVector<xAOD::TauJet_v2> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TauJet_v2> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TauJet_v2> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TauJet_v2> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TauJet_v2> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TauJet_v2> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TauJet_v2> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TauJet_v2> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TauJet_v2> > > : new vector<DataLink<DataVector<xAOD::TauJet_v2> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TauJet_v2> > >[nElements] : new vector<DataLink<DataVector<xAOD::TauJet_v2> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TauJet_v2> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TauJet_v2> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v2gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TauJet_v2> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TauJet_v2> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TauJet_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TauJet_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TauJet_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TauJet_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TauJet_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TauJet_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TauJet_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TauJet_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TauJet_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TauJet_v1> > > : new vector<DataLink<DataVector<xAOD::TauJet_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TauJet_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TauJet_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TauJet_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TauJet_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTauJet_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TauJet_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TauJet_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTau_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTau/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTau",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTau/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTau/versions/TauJetContainer_v1.h")))  TauJet_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTau/versions/TauJetContainer_v1.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@EA3CE9A0-18D8-49FD-B978-62857D8D8FD0)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTau/versions/TauJetAuxContainer_v1.h")))  TauJetAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTau/versions/TauJetContainer_v2.h")))  TauJet_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@2853B3D8-136E-444D-AB48-24B1A0E13083)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTau/versions/TauJetAuxContainer_v2.h")))  TauJetAuxContainer_v2;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTau"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTauDict.h 632166 2014-11-29 15:06:03Z krasznaa $
#ifndef XAODTAU_XAODTAUDICT_H
#define XAODTAU_XAODTAUDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
#include "xAODTau/versions/TauJetContainer_v1.h"
#include "xAODTau/versions/TauJetAuxContainer_v1.h"
#include "xAODTau/versions/TauJetContainer_v2.h"
#include "xAODTau/versions/TauJetAuxContainer_v2.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"

/// Declare a dummy CLID for xAOD::TauJetContainer_v1. This is only necessary
/// to get DataLink<xAOD::TauJetContainer_v1> and
/// ElementLink<xAOD::TauJetContainer_v1> compiling in the dictionary. The CLID
/// is not needed in the "real" code, since users are never exposed to the _v1
/// classes in Athena anymore.
CLASS_DEF( xAOD::TauJetContainer_v1, 12345678, 10 )

namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODTAU {
    xAOD::TauJetContainer_v1 c1;
    DataLink< xAOD::TauJetContainer_v1 > l1;
    ElementLink< xAOD::TauJetContainer_v1 > l2;
    ElementLinkVector< xAOD::TauJetContainer_v1 > l3;
    std::vector< DataLink< xAOD::TauJetContainer_v1 > > l4;
    std::vector< ElementLink< xAOD::TauJetContainer_v1 > > l5;
    std::vector< std::vector< ElementLink< xAOD::TauJetContainer_v1 > > > l6;
    std::vector< ElementLinkVector< xAOD::TauJetContainer_v1 > > l7;

    xAOD::TauJetContainer_v2 c2;
    DataLink< xAOD::TauJetContainer_v2 > l8;
    ElementLink< xAOD::TauJetContainer_v2 > l9;
    ElementLinkVector< xAOD::TauJetContainer_v2 > l10;
    std::vector< DataLink< xAOD::TauJetContainer_v2 > > l11;
    std::vector< ElementLink< xAOD::TauJetContainer_v2 > > l12;
    std::vector< std::vector< ElementLink< xAOD::TauJetContainer_v2 > > > l13;
    std::vector< ElementLinkVector< xAOD::TauJetContainer_v2 > > l14;


    // Instantiate the classes used by xAOD::TauJetAuxContainer, so that
    // Reflex would see them with their "correct type". Note that the
    // dictionary for these types comes from other places. This instantiation
    // is just needed for "Reflex related technical reasons"...
    ElementLink< xAOD::TrackParticleContainer > auxlink1;
    std::vector< ElementLink< xAOD::TrackParticleContainer > > auxlink2;
    ElementLink< xAOD::JetContainer > auxlink3;
    std::vector< ElementLink< xAOD::JetContainer > > auxlink4;
    ElementLink< xAOD::PFOContainer > auxlink5;
    std::vector< ElementLink< xAOD::PFOContainer > > auxlink6;
    ElementLink< xAOD::VertexContainer > auxlink7;
    std::vector< ElementLink< xAOD::VertexContainer > > auxlink8;
   };
}

#endif // XAODTAU_XAODTAUDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::TauJet_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::TauJet_v2> >", payloadCode, "@",
"DataLink<xAOD::TauJetContainer_v1>", payloadCode, "@",
"DataLink<xAOD::TauJetContainer_v2>", payloadCode, "@",
"DataVector<xAOD::TauJet_v1>", payloadCode, "@",
"DataVector<xAOD::TauJet_v2>", payloadCode, "@",
"ElementLink<DataVector<xAOD::TauJet_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::TauJet_v2> >", payloadCode, "@",
"ElementLink<xAOD::TauJetContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::TauJetContainer_v2>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TauJet_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TauJet_v2> > >", payloadCode, "@",
"vector<DataLink<xAOD::TauJetContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::TauJetContainer_v2> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TauJet_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TauJet_v2> > >", payloadCode, "@",
"vector<ElementLink<xAOD::TauJetContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::TauJetContainer_v2> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TauJetContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TauJetContainer_v2> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TauJet_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TauJet_v2> > > >", payloadCode, "@",
"xAOD::TauJetAuxContainer_v1", payloadCode, "@",
"xAOD::TauJetAuxContainer_v2", payloadCode, "@",
"xAOD::TauJetContainer_v1", payloadCode, "@",
"xAOD::TauJetContainer_v2", payloadCode, "@",
"xAOD::TauJetParameters::Author", payloadCode, "@",
"xAOD::TauJetParameters::DecayMode", payloadCode, "@",
"xAOD::TauJetParameters::Detail", payloadCode, "@",
"xAOD::TauJetParameters::IsTauFlag", payloadCode, "@",
"xAOD::TauJetParameters::PanTauDetails", payloadCode, "@",
"xAOD::TauJetParameters::PanTau_Constituents", payloadCode, "@",
"xAOD::TauJetParameters::SeedTypes", payloadCode, "@",
"xAOD::TauJetParameters::TauCalibType", payloadCode, "@",
"xAOD::TauJetParameters::TauHLVType", payloadCode, "@",
"xAOD::TauJetParameters::TauID", payloadCode, "@",
"xAOD::TauJetParameters::TauTrackFlag", payloadCode, "@",
"xAOD::TauJetParameters::VetoFlags", payloadCode, "@",
"xAOD::TauJet_v1", payloadCode, "@",
"xAOD::TauJet_v2", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTau_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTau_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTau_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTau_Reflex() {
  TriggerDictionaryInitialization_xAODTau_Reflex_Impl();
}

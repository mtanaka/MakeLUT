// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrackingdIobjdIxAODTracking_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODTracking/xAODTracking/xAODTrackingDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLVertex_v1_Dictionary();
   static void xAODcLcLVertex_v1_TClassManip(TClass*);
   static void *new_xAODcLcLVertex_v1(void *p = 0);
   static void *newArray_xAODcLcLVertex_v1(Long_t size, void *p);
   static void delete_xAODcLcLVertex_v1(void *p);
   static void deleteArray_xAODcLcLVertex_v1(void *p);
   static void destruct_xAODcLcLVertex_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLVertex_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::Vertex_v1* newObj = (xAOD::Vertex_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       newObj->resetCache();
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::Vertex_v1*)
   {
      ::xAOD::Vertex_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::Vertex_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::Vertex_v1", "xAODTracking/versions/Vertex_v1.h", 40,
                  typeid(::xAOD::Vertex_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLVertex_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::Vertex_v1) );
      instance.SetNew(&new_xAODcLcLVertex_v1);
      instance.SetNewArray(&newArray_xAODcLcLVertex_v1);
      instance.SetDelete(&delete_xAODcLcLVertex_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLVertex_v1);
      instance.SetDestructor(&destruct_xAODcLcLVertex_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::Vertex_v1";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLVertex_v1_0);
      rule->fCode        = "\n       newObj->resetCache();\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::Vertex_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::Vertex_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::Vertex_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLVertex_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::Vertex_v1*)0x0)->GetClass();
      xAODcLcLVertex_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLVertex_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLVertex_v1gR_Dictionary();
   static void DataVectorlExAODcLcLVertex_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLVertex_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLVertex_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLVertex_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLVertex_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLVertex_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::Vertex_v1>*)
   {
      ::DataVector<xAOD::Vertex_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::Vertex_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::Vertex_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::Vertex_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLVertex_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::Vertex_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLVertex_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLVertex_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLVertex_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLVertex_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLVertex_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::Vertex_v1>","xAOD::VertexContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::Vertex_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::Vertex_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::Vertex_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLVertex_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::Vertex_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLVertex_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLVertex_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","00B6DBA1-0E0D-4FDA-BFEF-F08EEFFE4BA0");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLVertexAuxContainer_v1_Dictionary();
   static void xAODcLcLVertexAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLVertexAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLVertexAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLVertexAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLVertexAuxContainer_v1(void *p);
   static void destruct_xAODcLcLVertexAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::VertexAuxContainer_v1*)
   {
      ::xAOD::VertexAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::VertexAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::VertexAuxContainer_v1", "xAODTracking/versions/VertexAuxContainer_v1.h", 27,
                  typeid(::xAOD::VertexAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLVertexAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::VertexAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLVertexAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLVertexAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLVertexAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLVertexAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLVertexAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::VertexAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::VertexAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::VertexAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLVertexAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::VertexAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLVertexAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLVertexAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B1F73A82-9B4E-4508-8EB0-EF7D6E05BA57");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::Vertex_v1> >*)
   {
      ::DataLink<DataVector<xAOD::Vertex_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::Vertex_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::Vertex_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::Vertex_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::Vertex_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::Vertex_v1> >","DataLink<xAOD::VertexContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::Vertex_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::Vertex_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Vertex_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Vertex_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::Vertex_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::Vertex_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::Vertex_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::Vertex_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::Vertex_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::Vertex_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::Vertex_v1> >","ElementLink<xAOD::VertexContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::Vertex_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::Vertex_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Vertex_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Vertex_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrackParticle_v1_Dictionary();
   static void xAODcLcLTrackParticle_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrackParticle_v1(void *p = 0);
   static void *newArray_xAODcLcLTrackParticle_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrackParticle_v1(void *p);
   static void deleteArray_xAODcLcLTrackParticle_v1(void *p);
   static void destruct_xAODcLcLTrackParticle_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTrackParticle_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TrackParticle_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::TrackParticle_v1* newObj = (xAOD::TrackParticle_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
      m_p4Cached = false;
     
   }
   static void read_xAODcLcLTrackParticle_v1_1( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TrackParticle_v1");
      static Long_t offset_m_perigeeCached = cls->GetDataMemberOffset("m_perigeeCached");
      bool& m_perigeeCached = *(bool*)(target+offset_m_perigeeCached);
      xAOD::TrackParticle_v1* newObj = (xAOD::TrackParticle_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
      m_perigeeCached = false;
     
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackParticle_v1*)
   {
      ::xAOD::TrackParticle_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackParticle_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackParticle_v1", "xAODTracking/versions/TrackParticle_v1.h", 36,
                  typeid(::xAOD::TrackParticle_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackParticle_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackParticle_v1) );
      instance.SetNew(&new_xAODcLcLTrackParticle_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrackParticle_v1);
      instance.SetDelete(&delete_xAODcLcLTrackParticle_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackParticle_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrackParticle_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(2);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TrackParticle_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTrackParticle_v1_0);
      rule->fCode        = "\n      m_p4Cached = false;\n     ";
      rule->fVersion     = "[1-]";
      rule = &readrules[1];
      rule->fSourceClass = "xAOD::TrackParticle_v1";
      rule->fTarget      = "m_perigeeCached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTrackParticle_v1_1);
      rule->fCode        = "\n      m_perigeeCached = false;\n     ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackParticle_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackParticle_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackParticle_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackParticle_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackParticle_v1*)0x0)->GetClass();
      xAODcLcLTrackParticle_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackParticle_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrackParticleAuxContainer_v1_Dictionary();
   static void xAODcLcLTrackParticleAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrackParticleAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrackParticleAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrackParticleAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrackParticleAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrackParticleAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackParticleAuxContainer_v1*)
   {
      ::xAOD::TrackParticleAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackParticleAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackParticleAuxContainer_v1", "xAODTracking/versions/TrackParticleAuxContainer_v1.h", 32,
                  typeid(::xAOD::TrackParticleAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackParticleAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackParticleAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrackParticleAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrackParticleAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrackParticleAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackParticleAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrackParticleAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackParticleAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackParticleAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackParticleAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackParticleAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackParticleAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrackParticleAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackParticleAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C3B01EA0-CA87-4C96-967F-E0F9A75BD370");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrackParticle_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrackParticle_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrackParticle_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrackParticle_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrackParticle_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrackParticle_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrackParticle_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrackParticle_v1>*)
   {
      ::DataVector<xAOD::TrackParticle_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrackParticle_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrackParticle_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TrackParticle_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrackParticle_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrackParticle_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrackParticle_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrackParticle_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrackParticle_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrackParticle_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrackParticle_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrackParticle_v1>","xAOD::TrackParticleContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrackParticle_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrackParticle_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrackParticle_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrackParticle_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrackParticle_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrackParticle_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrackParticle_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F7564EE8-3BD2-11E3-A42F-6C3BE51AB9F1");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrackParticleAuxContainer_v2_Dictionary();
   static void xAODcLcLTrackParticleAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLTrackParticleAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLTrackParticleAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLTrackParticleAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLTrackParticleAuxContainer_v2(void *p);
   static void destruct_xAODcLcLTrackParticleAuxContainer_v2(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTrackParticleAuxContainer_v2_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::TrackParticleAuxContainer_v2* newObj = (xAOD::TrackParticleAuxContainer_v2*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
      newObj->toTransient();
     
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackParticleAuxContainer_v2*)
   {
      ::xAOD::TrackParticleAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackParticleAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackParticleAuxContainer_v2", "xAODTracking/versions/TrackParticleAuxContainer_v2.h", 32,
                  typeid(::xAOD::TrackParticleAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackParticleAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackParticleAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLTrackParticleAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLTrackParticleAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLTrackParticleAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackParticleAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLTrackParticleAuxContainer_v2);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TrackParticleAuxContainer_v2";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTrackParticleAuxContainer_v2_0);
      rule->fCode        = "\n      newObj->toTransient();\n     ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackParticleAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackParticleAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackParticleAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackParticleAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackParticleAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLTrackParticleAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackParticleAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","53031BE5-2156-41E8-B70C-41A1C0572FC5");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLNeutralParticle_v1_Dictionary();
   static void xAODcLcLNeutralParticle_v1_TClassManip(TClass*);
   static void *new_xAODcLcLNeutralParticle_v1(void *p = 0);
   static void *newArray_xAODcLcLNeutralParticle_v1(Long_t size, void *p);
   static void delete_xAODcLcLNeutralParticle_v1(void *p);
   static void deleteArray_xAODcLcLNeutralParticle_v1(void *p);
   static void destruct_xAODcLcLNeutralParticle_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLNeutralParticle_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::NeutralParticle_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::NeutralParticle_v1* newObj = (xAOD::NeutralParticle_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
      m_p4Cached = false;
     
   }
   static void read_xAODcLcLNeutralParticle_v1_1( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::NeutralParticle_v1");
      static Long_t offset_m_perigeeCached = cls->GetDataMemberOffset("m_perigeeCached");
      bool& m_perigeeCached = *(bool*)(target+offset_m_perigeeCached);
      xAOD::NeutralParticle_v1* newObj = (xAOD::NeutralParticle_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
      m_perigeeCached = false;
     
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::NeutralParticle_v1*)
   {
      ::xAOD::NeutralParticle_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::NeutralParticle_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::NeutralParticle_v1", "xAODTracking/versions/NeutralParticle_v1.h", 30,
                  typeid(::xAOD::NeutralParticle_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLNeutralParticle_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::NeutralParticle_v1) );
      instance.SetNew(&new_xAODcLcLNeutralParticle_v1);
      instance.SetNewArray(&newArray_xAODcLcLNeutralParticle_v1);
      instance.SetDelete(&delete_xAODcLcLNeutralParticle_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLNeutralParticle_v1);
      instance.SetDestructor(&destruct_xAODcLcLNeutralParticle_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(2);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::NeutralParticle_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLNeutralParticle_v1_0);
      rule->fCode        = "\n      m_p4Cached = false;\n     ";
      rule->fVersion     = "[1-]";
      rule = &readrules[1];
      rule->fSourceClass = "xAOD::NeutralParticle_v1";
      rule->fTarget      = "m_perigeeCached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLNeutralParticle_v1_1);
      rule->fCode        = "\n      m_perigeeCached = false;\n     ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::NeutralParticle_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::NeutralParticle_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::NeutralParticle_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLNeutralParticle_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::NeutralParticle_v1*)0x0)->GetClass();
      xAODcLcLNeutralParticle_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLNeutralParticle_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLNeutralParticleAuxContainer_v1_Dictionary();
   static void xAODcLcLNeutralParticleAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLNeutralParticleAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLNeutralParticleAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLNeutralParticleAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLNeutralParticleAuxContainer_v1(void *p);
   static void destruct_xAODcLcLNeutralParticleAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::NeutralParticleAuxContainer_v1*)
   {
      ::xAOD::NeutralParticleAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::NeutralParticleAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::NeutralParticleAuxContainer_v1", "xAODTracking/versions/NeutralParticleAuxContainer_v1.h", 32,
                  typeid(::xAOD::NeutralParticleAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLNeutralParticleAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::NeutralParticleAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLNeutralParticleAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLNeutralParticleAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLNeutralParticleAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLNeutralParticleAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLNeutralParticleAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::NeutralParticleAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::NeutralParticleAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::NeutralParticleAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLNeutralParticleAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::NeutralParticleAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLNeutralParticleAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLNeutralParticleAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","006862BC-5CEB-11E3-9FB4-02163E00A92F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLNeutralParticle_v1gR_Dictionary();
   static void DataVectorlExAODcLcLNeutralParticle_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLNeutralParticle_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLNeutralParticle_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLNeutralParticle_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLNeutralParticle_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLNeutralParticle_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::NeutralParticle_v1>*)
   {
      ::DataVector<xAOD::NeutralParticle_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::NeutralParticle_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::NeutralParticle_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::NeutralParticle_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLNeutralParticle_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::NeutralParticle_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLNeutralParticle_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLNeutralParticle_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLNeutralParticle_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLNeutralParticle_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLNeutralParticle_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::NeutralParticle_v1>","xAOD::NeutralParticleContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::NeutralParticle_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::NeutralParticle_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::NeutralParticle_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLNeutralParticle_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::NeutralParticle_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLNeutralParticle_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLNeutralParticle_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","12869D6A-5CEB-11E3-B64A-02163E00A92F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TrackParticle_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TrackParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TrackParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TrackParticle_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TrackParticle_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TrackParticle_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TrackParticle_v1> >","DataLink<xAOD::TrackParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TrackParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TrackParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrackParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrackParticle_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrackParticle_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrackParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrackParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrackParticle_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrackParticle_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrackParticle_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrackParticle_v1> >","ElementLink<xAOD::TrackParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrackParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrackParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrackParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrackParticle_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::NeutralParticle_v1> >*)
   {
      ::DataLink<DataVector<xAOD::NeutralParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::NeutralParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::NeutralParticle_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::NeutralParticle_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::NeutralParticle_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::NeutralParticle_v1> >","DataLink<xAOD::NeutralParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::NeutralParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::NeutralParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::NeutralParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::NeutralParticle_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::NeutralParticle_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::NeutralParticle_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::NeutralParticle_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::NeutralParticle_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::NeutralParticle_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::NeutralParticle_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::NeutralParticle_v1> >","ElementLink<xAOD::NeutralParticleContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::NeutralParticle_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::NeutralParticle_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::NeutralParticle_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::NeutralParticle_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrackMeasurementValidation_v1_Dictionary();
   static void xAODcLcLTrackMeasurementValidation_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrackMeasurementValidation_v1(void *p = 0);
   static void *newArray_xAODcLcLTrackMeasurementValidation_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrackMeasurementValidation_v1(void *p);
   static void deleteArray_xAODcLcLTrackMeasurementValidation_v1(void *p);
   static void destruct_xAODcLcLTrackMeasurementValidation_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackMeasurementValidation_v1*)
   {
      ::xAOD::TrackMeasurementValidation_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackMeasurementValidation_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackMeasurementValidation_v1", "xAODTracking/versions/TrackMeasurementValidation_v1.h", 19,
                  typeid(::xAOD::TrackMeasurementValidation_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackMeasurementValidation_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackMeasurementValidation_v1) );
      instance.SetNew(&new_xAODcLcLTrackMeasurementValidation_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrackMeasurementValidation_v1);
      instance.SetDelete(&delete_xAODcLcLTrackMeasurementValidation_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackMeasurementValidation_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrackMeasurementValidation_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackMeasurementValidation_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackMeasurementValidation_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackMeasurementValidation_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackMeasurementValidation_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackMeasurementValidation_v1*)0x0)->GetClass();
      xAODcLcLTrackMeasurementValidation_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackMeasurementValidation_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrackMeasurementValidationAuxContainer_v1_Dictionary();
   static void xAODcLcLTrackMeasurementValidationAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrackMeasurementValidationAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrackMeasurementValidationAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrackMeasurementValidationAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrackMeasurementValidationAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrackMeasurementValidationAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackMeasurementValidationAuxContainer_v1*)
   {
      ::xAOD::TrackMeasurementValidationAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackMeasurementValidationAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackMeasurementValidationAuxContainer_v1", "xAODTracking/versions/TrackMeasurementValidationAuxContainer_v1.h", 23,
                  typeid(::xAOD::TrackMeasurementValidationAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackMeasurementValidationAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackMeasurementValidationAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrackMeasurementValidationAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrackMeasurementValidationAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrackMeasurementValidationAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackMeasurementValidationAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrackMeasurementValidationAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackMeasurementValidationAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackMeasurementValidationAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackMeasurementValidationAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackMeasurementValidationAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackMeasurementValidationAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrackMeasurementValidationAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackMeasurementValidationAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","61B62A1A-4C51-43A2-9623-1B9E910A81E8");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrackMeasurementValidation_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrackMeasurementValidation_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrackMeasurementValidation_v1>*)
   {
      ::DataVector<xAOD::TrackMeasurementValidation_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrackMeasurementValidation_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrackMeasurementValidation_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrackMeasurementValidation_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrackMeasurementValidation_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrackMeasurementValidation_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrackMeasurementValidation_v1>","xAOD::TrackMeasurementValidationContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrackMeasurementValidation_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrackMeasurementValidation_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrackMeasurementValidation_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrackMeasurementValidation_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrackMeasurementValidation_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrackMeasurementValidation_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrackMeasurementValidation_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","868F1FD8-AFE7-4B40-B12E-73716C37A6B0");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >","DataLink<xAOD::TrackMeasurementValidationContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >","ElementLink<xAOD::TrackMeasurementValidationContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrackStateValidation_v1_Dictionary();
   static void xAODcLcLTrackStateValidation_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrackStateValidation_v1(void *p = 0);
   static void *newArray_xAODcLcLTrackStateValidation_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrackStateValidation_v1(void *p);
   static void deleteArray_xAODcLcLTrackStateValidation_v1(void *p);
   static void destruct_xAODcLcLTrackStateValidation_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackStateValidation_v1*)
   {
      ::xAOD::TrackStateValidation_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackStateValidation_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackStateValidation_v1", "xAODTracking/versions/TrackStateValidation_v1.h", 24,
                  typeid(::xAOD::TrackStateValidation_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackStateValidation_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackStateValidation_v1) );
      instance.SetNew(&new_xAODcLcLTrackStateValidation_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrackStateValidation_v1);
      instance.SetDelete(&delete_xAODcLcLTrackStateValidation_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackStateValidation_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrackStateValidation_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackStateValidation_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackStateValidation_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackStateValidation_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackStateValidation_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackStateValidation_v1*)0x0)->GetClass();
      xAODcLcLTrackStateValidation_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackStateValidation_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrackStateValidationAuxContainer_v1_Dictionary();
   static void xAODcLcLTrackStateValidationAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrackStateValidationAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrackStateValidationAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrackStateValidationAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrackStateValidationAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrackStateValidationAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrackStateValidationAuxContainer_v1*)
   {
      ::xAOD::TrackStateValidationAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrackStateValidationAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrackStateValidationAuxContainer_v1", "xAODTracking/versions/TrackStateValidationAuxContainer_v1.h", 27,
                  typeid(::xAOD::TrackStateValidationAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrackStateValidationAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrackStateValidationAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrackStateValidationAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrackStateValidationAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrackStateValidationAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrackStateValidationAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrackStateValidationAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrackStateValidationAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrackStateValidationAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrackStateValidationAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrackStateValidationAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrackStateValidationAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrackStateValidationAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrackStateValidationAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","797432BD-DBEE-4C08-BF8E-E0556434A3F3");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrackStateValidation_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrackStateValidation_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrackStateValidation_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrackStateValidation_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrackStateValidation_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrackStateValidation_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrackStateValidation_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrackStateValidation_v1>*)
   {
      ::DataVector<xAOD::TrackStateValidation_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrackStateValidation_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrackStateValidation_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrackStateValidation_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrackStateValidation_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrackStateValidation_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrackStateValidation_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrackStateValidation_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrackStateValidation_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrackStateValidation_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrackStateValidation_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrackStateValidation_v1>","xAOD::TrackStateValidationContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrackStateValidation_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrackStateValidation_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrackStateValidation_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrackStateValidation_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrackStateValidation_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrackStateValidation_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrackStateValidation_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","149664B4-C7A7-4373-B5BA-D8D2DB3E65B5");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TrackStateValidation_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TrackStateValidation_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TrackStateValidation_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TrackStateValidation_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TrackStateValidation_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TrackStateValidation_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TrackStateValidation_v1> >","DataLink<xAOD::TrackStateValidationContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TrackStateValidation_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TrackStateValidation_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrackStateValidation_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrackStateValidation_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrackStateValidation_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrackStateValidation_v1> >","ElementLink<xAOD::TrackStateValidationContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR_Dictionary();
   static void EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR_TClassManip(TClass*);
   static void *new_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(void *p = 0);
   static void *newArray_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(Long_t size, void *p);
   static void delete_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(void *p);
   static void deleteArray_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(void *p);
   static void destruct_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Eigen::Matrix<double,6,1,0,6,1>*)
   {
      ::Eigen::Matrix<double,6,1,0,6,1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Eigen::Matrix<double,6,1,0,6,1>));
      static ::ROOT::TGenericClassInfo 
         instance("Eigen::Matrix<double,6,1,0,6,1>", "Eigen/src/Core/Matrix.h", 127,
                  typeid(::Eigen::Matrix<double,6,1,0,6,1>), DefineBehavior(ptr, ptr),
                  &EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR_Dictionary, isa_proxy, 0,
                  sizeof(::Eigen::Matrix<double,6,1,0,6,1>) );
      instance.SetNew(&new_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR);
      instance.SetNewArray(&newArray_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR);
      instance.SetDelete(&delete_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR);
      instance.SetDeleteArray(&deleteArray_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR);
      instance.SetDestructor(&destruct_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR);

      ROOT::AddClassAlternate("Eigen::Matrix<double,6,1,0,6,1>","xAOD::CurvilinearParameters_t");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Eigen::Matrix<double,6,1,0,6,1>*)
   {
      return GenerateInitInstanceLocal((::Eigen::Matrix<double,6,1,0,6,1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Eigen::Matrix<double,6,1,0,6,1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Eigen::Matrix<double,6,1,0,6,1>*)0x0)->GetClass();
      EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR_TClassManip(theClass);
   return theClass;
   }

   static void EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLVertex_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Vertex_v1 : new ::xAOD::Vertex_v1;
   }
   static void *newArray_xAODcLcLVertex_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Vertex_v1[nElements] : new ::xAOD::Vertex_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLVertex_v1(void *p) {
      delete ((::xAOD::Vertex_v1*)p);
   }
   static void deleteArray_xAODcLcLVertex_v1(void *p) {
      delete [] ((::xAOD::Vertex_v1*)p);
   }
   static void destruct_xAODcLcLVertex_v1(void *p) {
      typedef ::xAOD::Vertex_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::Vertex_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLVertex_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::Vertex_v1> : new ::DataVector<xAOD::Vertex_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLVertex_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::Vertex_v1>[nElements] : new ::DataVector<xAOD::Vertex_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLVertex_v1gR(void *p) {
      delete ((::DataVector<xAOD::Vertex_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLVertex_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::Vertex_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLVertex_v1gR(void *p) {
      typedef ::DataVector<xAOD::Vertex_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::Vertex_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLVertexAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::VertexAuxContainer_v1 : new ::xAOD::VertexAuxContainer_v1;
   }
   static void *newArray_xAODcLcLVertexAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::VertexAuxContainer_v1[nElements] : new ::xAOD::VertexAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLVertexAuxContainer_v1(void *p) {
      delete ((::xAOD::VertexAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLVertexAuxContainer_v1(void *p) {
      delete [] ((::xAOD::VertexAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLVertexAuxContainer_v1(void *p) {
      typedef ::xAOD::VertexAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::VertexAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::Vertex_v1> > : new ::DataLink<DataVector<xAOD::Vertex_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::Vertex_v1> >[nElements] : new ::DataLink<DataVector<xAOD::Vertex_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::Vertex_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::Vertex_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::Vertex_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::Vertex_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::Vertex_v1> > : new ::ElementLink<DataVector<xAOD::Vertex_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::Vertex_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::Vertex_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::Vertex_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::Vertex_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::Vertex_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::Vertex_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackParticle_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticle_v1 : new ::xAOD::TrackParticle_v1;
   }
   static void *newArray_xAODcLcLTrackParticle_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticle_v1[nElements] : new ::xAOD::TrackParticle_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackParticle_v1(void *p) {
      delete ((::xAOD::TrackParticle_v1*)p);
   }
   static void deleteArray_xAODcLcLTrackParticle_v1(void *p) {
      delete [] ((::xAOD::TrackParticle_v1*)p);
   }
   static void destruct_xAODcLcLTrackParticle_v1(void *p) {
      typedef ::xAOD::TrackParticle_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackParticle_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackParticleAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticleAuxContainer_v1 : new ::xAOD::TrackParticleAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrackParticleAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticleAuxContainer_v1[nElements] : new ::xAOD::TrackParticleAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackParticleAuxContainer_v1(void *p) {
      delete ((::xAOD::TrackParticleAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrackParticleAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrackParticleAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrackParticleAuxContainer_v1(void *p) {
      typedef ::xAOD::TrackParticleAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackParticleAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrackParticle_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrackParticle_v1> : new ::DataVector<xAOD::TrackParticle_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrackParticle_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrackParticle_v1>[nElements] : new ::DataVector<xAOD::TrackParticle_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrackParticle_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrackParticle_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrackParticle_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrackParticle_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrackParticle_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrackParticle_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrackParticle_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackParticleAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticleAuxContainer_v2 : new ::xAOD::TrackParticleAuxContainer_v2;
   }
   static void *newArray_xAODcLcLTrackParticleAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackParticleAuxContainer_v2[nElements] : new ::xAOD::TrackParticleAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackParticleAuxContainer_v2(void *p) {
      delete ((::xAOD::TrackParticleAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLTrackParticleAuxContainer_v2(void *p) {
      delete [] ((::xAOD::TrackParticleAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLTrackParticleAuxContainer_v2(void *p) {
      typedef ::xAOD::TrackParticleAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackParticleAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLNeutralParticle_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::NeutralParticle_v1 : new ::xAOD::NeutralParticle_v1;
   }
   static void *newArray_xAODcLcLNeutralParticle_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::NeutralParticle_v1[nElements] : new ::xAOD::NeutralParticle_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLNeutralParticle_v1(void *p) {
      delete ((::xAOD::NeutralParticle_v1*)p);
   }
   static void deleteArray_xAODcLcLNeutralParticle_v1(void *p) {
      delete [] ((::xAOD::NeutralParticle_v1*)p);
   }
   static void destruct_xAODcLcLNeutralParticle_v1(void *p) {
      typedef ::xAOD::NeutralParticle_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::NeutralParticle_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLNeutralParticleAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::NeutralParticleAuxContainer_v1 : new ::xAOD::NeutralParticleAuxContainer_v1;
   }
   static void *newArray_xAODcLcLNeutralParticleAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::NeutralParticleAuxContainer_v1[nElements] : new ::xAOD::NeutralParticleAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLNeutralParticleAuxContainer_v1(void *p) {
      delete ((::xAOD::NeutralParticleAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLNeutralParticleAuxContainer_v1(void *p) {
      delete [] ((::xAOD::NeutralParticleAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLNeutralParticleAuxContainer_v1(void *p) {
      typedef ::xAOD::NeutralParticleAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::NeutralParticleAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLNeutralParticle_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::NeutralParticle_v1> : new ::DataVector<xAOD::NeutralParticle_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLNeutralParticle_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::NeutralParticle_v1>[nElements] : new ::DataVector<xAOD::NeutralParticle_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLNeutralParticle_v1gR(void *p) {
      delete ((::DataVector<xAOD::NeutralParticle_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLNeutralParticle_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::NeutralParticle_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLNeutralParticle_v1gR(void *p) {
      typedef ::DataVector<xAOD::NeutralParticle_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::NeutralParticle_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TrackParticle_v1> > : new ::DataLink<DataVector<xAOD::TrackParticle_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TrackParticle_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TrackParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TrackParticle_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TrackParticle_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TrackParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TrackParticle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrackParticle_v1> > : new ::ElementLink<DataVector<xAOD::TrackParticle_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrackParticle_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrackParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrackParticle_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrackParticle_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrackParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrackParticle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::NeutralParticle_v1> > : new ::DataLink<DataVector<xAOD::NeutralParticle_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::NeutralParticle_v1> >[nElements] : new ::DataLink<DataVector<xAOD::NeutralParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::NeutralParticle_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::NeutralParticle_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::NeutralParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::NeutralParticle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::NeutralParticle_v1> > : new ::ElementLink<DataVector<xAOD::NeutralParticle_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::NeutralParticle_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::NeutralParticle_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::NeutralParticle_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::NeutralParticle_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::NeutralParticle_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::NeutralParticle_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackMeasurementValidation_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackMeasurementValidation_v1 : new ::xAOD::TrackMeasurementValidation_v1;
   }
   static void *newArray_xAODcLcLTrackMeasurementValidation_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackMeasurementValidation_v1[nElements] : new ::xAOD::TrackMeasurementValidation_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackMeasurementValidation_v1(void *p) {
      delete ((::xAOD::TrackMeasurementValidation_v1*)p);
   }
   static void deleteArray_xAODcLcLTrackMeasurementValidation_v1(void *p) {
      delete [] ((::xAOD::TrackMeasurementValidation_v1*)p);
   }
   static void destruct_xAODcLcLTrackMeasurementValidation_v1(void *p) {
      typedef ::xAOD::TrackMeasurementValidation_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackMeasurementValidation_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackMeasurementValidationAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackMeasurementValidationAuxContainer_v1 : new ::xAOD::TrackMeasurementValidationAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrackMeasurementValidationAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackMeasurementValidationAuxContainer_v1[nElements] : new ::xAOD::TrackMeasurementValidationAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackMeasurementValidationAuxContainer_v1(void *p) {
      delete ((::xAOD::TrackMeasurementValidationAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrackMeasurementValidationAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrackMeasurementValidationAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrackMeasurementValidationAuxContainer_v1(void *p) {
      typedef ::xAOD::TrackMeasurementValidationAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackMeasurementValidationAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrackMeasurementValidation_v1> : new ::DataVector<xAOD::TrackMeasurementValidation_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrackMeasurementValidation_v1>[nElements] : new ::DataVector<xAOD::TrackMeasurementValidation_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrackMeasurementValidation_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrackMeasurementValidation_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrackMeasurementValidation_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrackMeasurementValidation_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrackMeasurementValidation_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > : new ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > : new ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackStateValidation_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackStateValidation_v1 : new ::xAOD::TrackStateValidation_v1;
   }
   static void *newArray_xAODcLcLTrackStateValidation_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackStateValidation_v1[nElements] : new ::xAOD::TrackStateValidation_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackStateValidation_v1(void *p) {
      delete ((::xAOD::TrackStateValidation_v1*)p);
   }
   static void deleteArray_xAODcLcLTrackStateValidation_v1(void *p) {
      delete [] ((::xAOD::TrackStateValidation_v1*)p);
   }
   static void destruct_xAODcLcLTrackStateValidation_v1(void *p) {
      typedef ::xAOD::TrackStateValidation_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackStateValidation_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrackStateValidationAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackStateValidationAuxContainer_v1 : new ::xAOD::TrackStateValidationAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrackStateValidationAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrackStateValidationAuxContainer_v1[nElements] : new ::xAOD::TrackStateValidationAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrackStateValidationAuxContainer_v1(void *p) {
      delete ((::xAOD::TrackStateValidationAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrackStateValidationAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrackStateValidationAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrackStateValidationAuxContainer_v1(void *p) {
      typedef ::xAOD::TrackStateValidationAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrackStateValidationAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrackStateValidation_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrackStateValidation_v1> : new ::DataVector<xAOD::TrackStateValidation_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrackStateValidation_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrackStateValidation_v1>[nElements] : new ::DataVector<xAOD::TrackStateValidation_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrackStateValidation_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrackStateValidation_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrackStateValidation_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrackStateValidation_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrackStateValidation_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrackStateValidation_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrackStateValidation_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TrackStateValidation_v1> > : new ::DataLink<DataVector<xAOD::TrackStateValidation_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TrackStateValidation_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TrackStateValidation_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TrackStateValidation_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TrackStateValidation_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TrackStateValidation_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TrackStateValidation_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> > : new ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrackStateValidation_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(void *p) {
      return  p ? new(p) ::Eigen::Matrix<double,6,1,0,6,1> : new ::Eigen::Matrix<double,6,1,0,6,1>;
   }
   static void *newArray_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(Long_t nElements, void *p) {
      return p ? new(p) ::Eigen::Matrix<double,6,1,0,6,1>[nElements] : new ::Eigen::Matrix<double,6,1,0,6,1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(void *p) {
      delete ((::Eigen::Matrix<double,6,1,0,6,1>*)p);
   }
   static void deleteArray_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(void *p) {
      delete [] ((::Eigen::Matrix<double,6,1,0,6,1>*)p);
   }
   static void destruct_EigencLcLMatrixlEdoublecO6cO1cO0cO6cO1gR(void *p) {
      typedef ::Eigen::Matrix<double,6,1,0,6,1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Eigen::Matrix<double,6,1,0,6,1>

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEintgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEintgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEintgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEintgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEintgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEintgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEintgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<int> > >*)
   {
      vector<vector<vector<int> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<int> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<int> > >", -2, "vector", 210,
                  typeid(vector<vector<vector<int> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEintgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<int> > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEintgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEintgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<int> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<int> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEintgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<int> > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEintgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEintgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEintgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<int> > > : new vector<vector<vector<int> > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEintgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<int> > >[nElements] : new vector<vector<vector<int> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEintgRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<int> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEintgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<int> > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEintgRsPgRsPgR(void *p) {
      typedef vector<vector<vector<int> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<int> > >

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEfloatgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<float> > >*)
   {
      vector<vector<vector<float> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<float> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<float> > >", -2, "vector", 210,
                  typeid(vector<vector<vector<float> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEfloatgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<float> > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEfloatgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<float> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<float> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEfloatgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<float> > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<float> > > : new vector<vector<vector<float> > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<float> > >[nElements] : new vector<vector<vector<float> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<float> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<float> > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      typedef vector<vector<vector<float> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<float> > >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPlonggRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPlonggRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned long> >*)
   {
      vector<vector<unsigned long> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned long> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned long> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned long> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned long> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned long> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned long> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned long> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned long> > : new vector<vector<unsigned long> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPlonggRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned long> >[nElements] : new vector<vector<unsigned long> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      delete ((vector<vector<unsigned long> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      delete [] ((vector<vector<unsigned long> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      typedef vector<vector<unsigned long> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned long> >

namespace ROOT {
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary();
   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<int> >*)
   {
      vector<vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<int> >", -2, "vector", 210,
                  typeid(vector<vector<int> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<int> >) );
      instance.SetNew(&new_vectorlEvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<int> >*)0x0)->GetClass();
      vectorlEvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<int> > : new vector<vector<int> >;
   }
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<int> >[nElements] : new vector<vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEintgRsPgR(void *p) {
      delete ((vector<vector<int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p) {
      delete [] ((vector<vector<int> >*)p);
   }
   static void destruct_vectorlEvectorlEintgRsPgR(void *p) {
      typedef vector<vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<int> >

namespace ROOT {
   static TClass *vectorlEvectorlEfloatgRsPgR_Dictionary();
   static void vectorlEvectorlEfloatgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEfloatgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEfloatgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEfloatgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEfloatgRsPgR(void *p);
   static void destruct_vectorlEvectorlEfloatgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<float> >*)
   {
      vector<vector<float> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<float> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<float> >", -2, "vector", 210,
                  typeid(vector<vector<float> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEfloatgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<float> >) );
      instance.SetNew(&new_vectorlEvectorlEfloatgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEfloatgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEfloatgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEfloatgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEfloatgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<float> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<float> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEfloatgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<float> >*)0x0)->GetClass();
      vectorlEvectorlEfloatgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEfloatgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEfloatgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<float> > : new vector<vector<float> >;
   }
   static void *newArray_vectorlEvectorlEfloatgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<float> >[nElements] : new vector<vector<float> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEfloatgRsPgR(void *p) {
      delete ((vector<vector<float> >*)p);
   }
   static void deleteArray_vectorlEvectorlEfloatgRsPgR(void *p) {
      delete [] ((vector<vector<float> >*)p);
   }
   static void destruct_vectorlEvectorlEfloatgRsPgR(void *p) {
      typedef vector<vector<float> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<float> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >

namespace ROOT {
   static TClass *vectorlEunsignedsPlonggR_Dictionary();
   static void vectorlEunsignedsPlonggR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPlonggR(void *p = 0);
   static void *newArray_vectorlEunsignedsPlonggR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPlonggR(void *p);
   static void deleteArray_vectorlEunsignedsPlonggR(void *p);
   static void destruct_vectorlEunsignedsPlonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned long>*)
   {
      vector<unsigned long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned long>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned long>", -2, "vector", 210,
                  typeid(vector<unsigned long>), DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPlonggR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned long>) );
      instance.SetNew(&new_vectorlEunsignedsPlonggR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPlonggR);
      instance.SetDelete(&delete_vectorlEunsignedsPlonggR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPlonggR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPlonggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned long> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned long>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPlonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned long>*)0x0)->GetClass();
      vectorlEunsignedsPlonggR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPlonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPlonggR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned long> : new vector<unsigned long>;
   }
   static void *newArray_vectorlEunsignedsPlonggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned long>[nElements] : new vector<unsigned long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPlonggR(void *p) {
      delete ((vector<unsigned long>*)p);
   }
   static void deleteArray_vectorlEunsignedsPlonggR(void *p) {
      delete [] ((vector<unsigned long>*)p);
   }
   static void destruct_vectorlEunsignedsPlonggR(void *p) {
      typedef vector<unsigned long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned long>

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 210,
                  typeid(vector<int>), DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 210,
                  typeid(vector<float>), DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::Vertex_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::Vertex_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::Vertex_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::Vertex_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::Vertex_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::Vertex_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Vertex_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Vertex_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Vertex_v1> > > : new vector<ElementLink<DataVector<xAOD::Vertex_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Vertex_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::Vertex_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::Vertex_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::Vertex_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::Vertex_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::Vertex_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > : new vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > : new vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > : new vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > : new vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::Vertex_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::Vertex_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::Vertex_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::Vertex_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::Vertex_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::Vertex_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::Vertex_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Vertex_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Vertex_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Vertex_v1> > > : new vector<DataLink<DataVector<xAOD::Vertex_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Vertex_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::Vertex_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::Vertex_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::Vertex_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLVertex_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::Vertex_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::Vertex_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > > : new vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackStateValidation_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TrackParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TrackParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrackParticle_v1> > > : new vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TrackParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > : new vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrackMeasurementValidation_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > > : new vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLNeutralParticle_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTracking_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODTracking/Root",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODTracking/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTracking/versions/TrackParticleAuxContainer_v1.h")))  Vertex_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@B1F73A82-9B4E-4508-8EB0-EF7D6E05BA57)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTracking/versions/VertexAuxContainer_v1.h")))  VertexAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTracking/TrackParticleContainer.h")))  TrackParticle_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@C3B01EA0-CA87-4C96-967F-E0F9A75BD370)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTracking/versions/TrackParticleAuxContainer_v1.h")))  TrackParticleAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTracking/TrackParticleContainer.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@53031BE5-2156-41E8-B70C-41A1C0572FC5)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTracking/versions/TrackParticleAuxContainer_v2.h")))  TrackParticleAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTracking/versions/TrackParticleAuxContainer_v1.h")))  NeutralParticle_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@006862BC-5CEB-11E3-9FB4-02163E00A92F)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTracking/versions/NeutralParticleAuxContainer_v1.h")))  NeutralParticleAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTracking/TrackMeasurementValidationContainer.h")))  TrackMeasurementValidation_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@61B62A1A-4C51-43A2-9623-1B9E910A81E8)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTracking/versions/TrackMeasurementValidationAuxContainer_v1.h")))  TrackMeasurementValidationAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTracking/TrackStateValidationContainer.h")))  TrackStateValidation_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@797432BD-DBEE-4C08-BF8E-E0556434A3F3)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTracking/versions/TrackStateValidationAuxContainer_v1.h")))  TrackStateValidationAuxContainer_v1;}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTracking"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTrackingDict.h 573808 2013-12-04 14:09:11Z salzburg $
#ifndef XAODTRACKING_XAODTRACKINGDICT_H
#define XAODTRACKING_XAODTRACKINGDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// System include(s):
#include <bitset>
 
// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"
 
// Local include(s):
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/versions/TrackParticleContainer_v1.h"
#include "xAODTracking/versions/TrackParticleAuxContainer_v1.h"
#include "xAODTracking/versions/TrackParticleAuxContainer_v2.h"
#include "xAODTracking/NeutralParticleContainer.h"
#include "xAODTracking/versions/NeutralParticleContainer_v1.h"
#include "xAODTracking/versions/NeutralParticleAuxContainer_v1.h"
#include "xAODTracking/versions/Vertex_v1.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/versions/VertexContainer_v1.h"
#include "xAODTracking/versions/VertexContainer_v1.h"
#include "xAODTracking/versions/VertexAuxContainer_v1.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/versions/TrackMeasurementValidationContainer_v1.h"
#include "xAODTracking/versions/TrackMeasurementValidationAuxContainer_v1.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/versions/TrackStateValidationContainer_v1.h"
#include "xAODTracking/versions/TrackStateValidationAuxContainer_v1.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODTRACKING {
     xAOD::TrackParticleContainer_v1                                              c1;
     DataLink< xAOD::TrackParticleContainer_v1 >                                  l1;
     ElementLink< xAOD::TrackParticleContainer >                               l2;
     ElementLinkVector< xAOD::TrackParticleContainer >                         l3;
     std::vector< DataLink< xAOD::TrackParticleContainer_v1 > >                   l4;
     std::vector< ElementLink< xAOD::TrackParticleContainer_v1 > >                l5;
     std::vector< ElementLinkVector< xAOD::TrackParticleContainer_v1 > >          l6;
     std::vector< std::vector< ElementLink< xAOD::TrackParticleContainer_v1 > > > l7;

     xAOD::NeutralParticleContainer_v1                                              nc1;
     DataLink< xAOD::NeutralParticleContainer_v1 >                                  nl1;
     ElementLink< xAOD::NeutralParticleContainer >                               nl2;
     ElementLinkVector< xAOD::NeutralParticleContainer >                         nl3;
     std::vector< DataLink< xAOD::NeutralParticleContainer_v1 > >                   nl4;
     std::vector< ElementLink< xAOD::NeutralParticleContainer_v1 > >                nl5;
     std::vector< ElementLinkVector< xAOD::NeutralParticleContainer_v1 > >          nl6;
     std::vector< std::vector< ElementLink< xAOD::NeutralParticleContainer_v1 > > > nl7;
     
#if ( ! defined(XAOD_STANDALONE) ) && ( ! defined(XAOD_MANACORE) )
     std::bitset< 11 >                                                            dummy1;
     TrackCollection                                                              c2;
     ElementLink<TrackCollection>                                                 l8;
#endif // not XAOD_STANDALONE and not XAOD_MANACORE

      // Container(s):
      xAOD::VertexContainer_v1                                                    c3;
      // Data link(s):
      DataLink< xAOD::VertexContainer_v1 >                                        dl1;
      std::vector< DataLink< xAOD::VertexContainer_v1 > >                         dl2;
      // Element link(s):
      ElementLink< xAOD::VertexContainer >                                     el1;
      std::vector< ElementLink< xAOD::VertexContainer_v1 > >                      el2;
      std::vector< std::vector< ElementLink< xAOD::VertexContainer_v1 > > >       el3;

      // Container(s):
      xAOD::TrackMeasurementValidationContainer_v1                                                    c4;
      // Data link(s):
      DataLink< xAOD::TrackMeasurementValidationContainer_v1 >                                        pdl1;
      std::vector< DataLink< xAOD::TrackMeasurementValidationContainer_v1 > >                         pdl2;
      // Element link(s):
      ElementLink< xAOD::TrackMeasurementValidationContainer >                                     pel1;
      std::vector< ElementLink< xAOD::TrackMeasurementValidationContainer_v1 > >                      pel2;
      std::vector< std::vector< ElementLink< xAOD::TrackMeasurementValidationContainer_v1 > > >       pel3;

      // Container(s):
      xAOD::TrackStateValidationContainer_v1                                                    c5;
      // Data link(s):
      DataLink< xAOD::TrackStateValidationContainer_v1 >                                        mdl1;
      std::vector< DataLink< xAOD::TrackStateValidationContainer_v1 > >                         mdl2;
      // Element link(s):
      ElementLink< xAOD::TrackStateValidationContainer >                                     mel1;
      std::vector< ElementLink< xAOD::TrackStateValidationContainer_v1 > >                      mel2;
      std::vector< std::vector< ElementLink< xAOD::TrackStateValidationContainer_v1 > > >       mel3;

      std::vector<std::vector<std::vector<int> > > vecDummyIntX;
      std::vector<std::vector<std::vector<float> > > vecDummyFltX;
      std::vector<std::vector<unsigned long> > vecDummyULX;


  };
}

#endif // XAODTRACKPARTICLE_XAODTRACKPARTICLEDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::NeutralParticle_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::TrackParticle_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::TrackStateValidation_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::Vertex_v1> >", payloadCode, "@",
"DataLink<xAOD::NeutralParticleContainer_v1>", payloadCode, "@",
"DataLink<xAOD::TrackMeasurementValidationContainer_v1>", payloadCode, "@",
"DataLink<xAOD::TrackParticleContainer_v1>", payloadCode, "@",
"DataLink<xAOD::TrackStateValidationContainer_v1>", payloadCode, "@",
"DataLink<xAOD::VertexContainer_v1>", payloadCode, "@",
"DataVector<xAOD::NeutralParticle_v1>", payloadCode, "@",
"DataVector<xAOD::TrackMeasurementValidation_v1>", payloadCode, "@",
"DataVector<xAOD::TrackParticle_v1>", payloadCode, "@",
"DataVector<xAOD::TrackStateValidation_v1>", payloadCode, "@",
"DataVector<xAOD::Vertex_v1>", payloadCode, "@",
"Eigen::Matrix<double,6,1,0,6,1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::NeutralParticle_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrackParticle_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrackStateValidation_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::Vertex_v1> >", payloadCode, "@",
"ElementLink<xAOD::NeutralParticleContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::TrackMeasurementValidationContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::TrackParticleContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::TrackStateValidationContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::VertexContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::NeutralParticle_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TrackParticle_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TrackStateValidation_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::Vertex_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::NeutralParticleContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::TrackMeasurementValidationContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::TrackParticleContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::TrackStateValidationContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::VertexContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::Vertex_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::NeutralParticleContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::TrackMeasurementValidationContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::TrackParticleContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::TrackStateValidationContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::VertexContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::NeutralParticleContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrackMeasurementValidationContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrackParticleContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrackStateValidationContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::VertexContainer_v1> > >", payloadCode, "@",
"vector<std::vector<std::vector<float> > >", payloadCode, "@",
"vector<std::vector<std::vector<int> > >", payloadCode, "@",
"vector<std::vector<unsigned long> >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::NeutralParticle_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrackMeasurementValidation_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrackParticle_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::Vertex_v1> > > >", payloadCode, "@",
"vector<vector<unsigned long> >", payloadCode, "@",
"vector<vector<vector<float> > >", payloadCode, "@",
"vector<vector<vector<int> > >", payloadCode, "@",
"xAOD::CurvilinearParameters_t", payloadCode, "@",
"xAOD::MuonSummaryType", payloadCode, "@",
"xAOD::NeutralParticleAuxContainer_v1", payloadCode, "@",
"xAOD::NeutralParticleContainer_v1", payloadCode, "@",
"xAOD::NeutralParticle_v1", payloadCode, "@",
"xAOD::ParameterPosition", payloadCode, "@",
"xAOD::ParticleHypothesis", payloadCode, "@",
"xAOD::SummaryType", payloadCode, "@",
"xAOD::TrackFitter", payloadCode, "@",
"xAOD::TrackMeasurementValidationAuxContainer_v1", payloadCode, "@",
"xAOD::TrackMeasurementValidationContainer_v1", payloadCode, "@",
"xAOD::TrackMeasurementValidation_v1", payloadCode, "@",
"xAOD::TrackParticleAuxContainer_v1", payloadCode, "@",
"xAOD::TrackParticleAuxContainer_v2", payloadCode, "@",
"xAOD::TrackParticleContainer_v1", payloadCode, "@",
"xAOD::TrackParticle_v1", payloadCode, "@",
"xAOD::TrackPatternRecoInfo", payloadCode, "@",
"xAOD::TrackProperties", payloadCode, "@",
"xAOD::TrackStateValidationAuxContainer_v1", payloadCode, "@",
"xAOD::TrackStateValidationContainer_v1", payloadCode, "@",
"xAOD::TrackStateValidation_v1", payloadCode, "@",
"xAOD::VertexAuxContainer_v1", payloadCode, "@",
"xAOD::VertexContainer_v1", payloadCode, "@",
"xAOD::Vertex_v1", payloadCode, "@",
"xAOD::VxType::VertexType", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTracking_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTracking_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTracking_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTracking_Reflex() {
  TriggerDictionaryInitialization_xAODTracking_Reflex_Impl();
}

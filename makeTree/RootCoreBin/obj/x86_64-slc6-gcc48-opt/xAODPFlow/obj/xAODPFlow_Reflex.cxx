// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODPFlowdIobjdIxAODPFlow_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODPFlow/xAODPFlow/xAODPFlowDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLPFO_v1_Dictionary();
   static void xAODcLcLPFO_v1_TClassManip(TClass*);
   static void *new_xAODcLcLPFO_v1(void *p = 0);
   static void *newArray_xAODcLcLPFO_v1(Long_t size, void *p);
   static void delete_xAODcLcLPFO_v1(void *p);
   static void deleteArray_xAODcLcLPFO_v1(void *p);
   static void destruct_xAODcLcLPFO_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLPFO_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::PFO_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::PFO_v1* newObj = (xAOD::PFO_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }
   static void read_xAODcLcLPFO_v1_1( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::PFO_v1");
      static Long_t offset_m_p4EMCached = cls->GetDataMemberOffset("m_p4EMCached");
      bool& m_p4EMCached = *(bool*)(target+offset_m_p4EMCached);
      xAOD::PFO_v1* newObj = (xAOD::PFO_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4EMCached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::PFO_v1*)
   {
      ::xAOD::PFO_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::PFO_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::PFO_v1", "xAODPFlow/versions/PFO_v1.h", 30,
                  typeid(::xAOD::PFO_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLPFO_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::PFO_v1) );
      instance.SetNew(&new_xAODcLcLPFO_v1);
      instance.SetNewArray(&newArray_xAODcLcLPFO_v1);
      instance.SetDelete(&delete_xAODcLcLPFO_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLPFO_v1);
      instance.SetDestructor(&destruct_xAODcLcLPFO_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(2);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::PFO_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLPFO_v1_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      rule = &readrules[1];
      rule->fSourceClass = "xAOD::PFO_v1";
      rule->fTarget      = "m_p4EMCached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLPFO_v1_1);
      rule->fCode        = "\n       m_p4EMCached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::PFO_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::PFO_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::PFO_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLPFO_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::PFO_v1*)0x0)->GetClass();
      xAODcLcLPFO_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLPFO_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLPFO_v1gR_Dictionary();
   static void DataVectorlExAODcLcLPFO_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLPFO_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLPFO_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLPFO_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLPFO_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLPFO_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::PFO_v1>*)
   {
      ::DataVector<xAOD::PFO_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::PFO_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::PFO_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::PFO_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLPFO_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::PFO_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLPFO_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLPFO_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLPFO_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLPFO_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLPFO_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::PFO_v1>","xAOD::PFOContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::PFO_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::PFO_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::PFO_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLPFO_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::PFO_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLPFO_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLPFO_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","476378BF-054D-499E-9CC9-000F501B30F2");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLPFOAuxContainer_v1_Dictionary();
   static void xAODcLcLPFOAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLPFOAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLPFOAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLPFOAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLPFOAuxContainer_v1(void *p);
   static void destruct_xAODcLcLPFOAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::PFOAuxContainer_v1*)
   {
      ::xAOD::PFOAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::PFOAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::PFOAuxContainer_v1", "xAODPFlow/versions/PFOAuxContainer_v1.h", 31,
                  typeid(::xAOD::PFOAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLPFOAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::PFOAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLPFOAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLPFOAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLPFOAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLPFOAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLPFOAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::PFOAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::PFOAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::PFOAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLPFOAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::PFOAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLPFOAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLPFOAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F691F845-4A3D-466A-9187-FED7837D9372");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::PFO_v1> >*)
   {
      ::DataLink<DataVector<xAOD::PFO_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::PFO_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::PFO_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::PFO_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::PFO_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::PFO_v1> >","DataLink<xAOD::PFOContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::PFO_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::PFO_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::PFO_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::PFO_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::PFO_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::PFO_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::PFO_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::PFO_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::PFO_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::PFO_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::PFO_v1> >","ElementLink<xAOD::PFOContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::PFO_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::PFO_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::PFO_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::PFO_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLPFO_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PFO_v1 : new ::xAOD::PFO_v1;
   }
   static void *newArray_xAODcLcLPFO_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PFO_v1[nElements] : new ::xAOD::PFO_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLPFO_v1(void *p) {
      delete ((::xAOD::PFO_v1*)p);
   }
   static void deleteArray_xAODcLcLPFO_v1(void *p) {
      delete [] ((::xAOD::PFO_v1*)p);
   }
   static void destruct_xAODcLcLPFO_v1(void *p) {
      typedef ::xAOD::PFO_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::PFO_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLPFO_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::PFO_v1> : new ::DataVector<xAOD::PFO_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLPFO_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::PFO_v1>[nElements] : new ::DataVector<xAOD::PFO_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLPFO_v1gR(void *p) {
      delete ((::DataVector<xAOD::PFO_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLPFO_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::PFO_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLPFO_v1gR(void *p) {
      typedef ::DataVector<xAOD::PFO_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::PFO_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLPFOAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PFOAuxContainer_v1 : new ::xAOD::PFOAuxContainer_v1;
   }
   static void *newArray_xAODcLcLPFOAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PFOAuxContainer_v1[nElements] : new ::xAOD::PFOAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLPFOAuxContainer_v1(void *p) {
      delete ((::xAOD::PFOAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLPFOAuxContainer_v1(void *p) {
      delete [] ((::xAOD::PFOAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLPFOAuxContainer_v1(void *p) {
      typedef ::xAOD::PFOAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::PFOAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::PFO_v1> > : new ::DataLink<DataVector<xAOD::PFO_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::PFO_v1> >[nElements] : new ::DataLink<DataVector<xAOD::PFO_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::PFO_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::PFO_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::PFO_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::PFO_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::PFO_v1> > : new ::ElementLink<DataVector<xAOD::PFO_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::PFO_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::PFO_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::PFO_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::PFO_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::PFO_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::PFO_v1> >

namespace ROOT {
   static TClass *vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR_Dictionary();
   static void vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR_TClassManip(TClass*);
   static void *new_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(void *p = 0);
   static void *newArray_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(Long_t size, void *p);
   static void delete_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(void *p);
   static void deleteArray_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(void *p);
   static void destruct_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<xAOD::PFODetails::PFOLeptonType>*)
   {
      vector<xAOD::PFODetails::PFOLeptonType> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<xAOD::PFODetails::PFOLeptonType>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<xAOD::PFODetails::PFOLeptonType>", -2, "vector", 210,
                  typeid(vector<xAOD::PFODetails::PFOLeptonType>), DefineBehavior(ptr, ptr),
                  &vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR_Dictionary, isa_proxy, 4,
                  sizeof(vector<xAOD::PFODetails::PFOLeptonType>) );
      instance.SetNew(&new_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR);
      instance.SetNewArray(&newArray_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR);
      instance.SetDelete(&delete_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR);
      instance.SetDeleteArray(&deleteArray_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR);
      instance.SetDestructor(&destruct_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<xAOD::PFODetails::PFOLeptonType> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<xAOD::PFODetails::PFOLeptonType>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<xAOD::PFODetails::PFOLeptonType>*)0x0)->GetClass();
      vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<xAOD::PFODetails::PFOLeptonType> : new vector<xAOD::PFODetails::PFOLeptonType>;
   }
   static void *newArray_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<xAOD::PFODetails::PFOLeptonType>[nElements] : new vector<xAOD::PFODetails::PFOLeptonType>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(void *p) {
      delete ((vector<xAOD::PFODetails::PFOLeptonType>*)p);
   }
   static void deleteArray_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(void *p) {
      delete [] ((vector<xAOD::PFODetails::PFOLeptonType>*)p);
   }
   static void destruct_vectorlExAODcLcLPFODetailscLcLPFOLeptonTypegR(void *p) {
      typedef vector<xAOD::PFODetails::PFOLeptonType> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<xAOD::PFODetails::PFOLeptonType>

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > : new vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::IParticle> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::PFO_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::PFO_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::PFO_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::PFO_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::PFO_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::PFO_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::PFO_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::PFO_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::PFO_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::PFO_v1> > > : new vector<ElementLink<DataVector<xAOD::PFO_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::PFO_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::PFO_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::PFO_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::PFO_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::PFO_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::PFO_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::IParticle> > >*)
   {
      vector<ElementLink<DataVector<xAOD::IParticle> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::IParticle> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::IParticle> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::IParticle> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::IParticle> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::IParticle> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::IParticle> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::IParticle> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::IParticle> > > : new vector<ElementLink<DataVector<xAOD::IParticle> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::IParticle> > >[nElements] : new vector<ElementLink<DataVector<xAOD::IParticle> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::IParticle> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::IParticle> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLIParticlegRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::IParticle> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::IParticle> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::PFO_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::PFO_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::PFO_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::PFO_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::PFO_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::PFO_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::PFO_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::PFO_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::PFO_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::PFO_v1> > > : new vector<DataLink<DataVector<xAOD::PFO_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::PFO_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::PFO_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::PFO_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::PFO_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLPFO_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::PFO_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::PFO_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODPFlow_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODPFlow/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODPFlow/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODPFlow/PFOContainer.h")))  PFO_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODPFlow/PFOContainer.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@F691F845-4A3D-466A-9187-FED7837D9372)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODPFlow/PFOAuxContainer.h")))  PFOAuxContainer_v1;}
namespace xAOD{namespace PFODetails{enum  __attribute__((annotate("$clingAutoload$xAODPFlow/PFOContainer.h"))) PFOLeptonType : unsigned int;}}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODPFlow"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
/* Class to set what goes in the dictionary  - authors M. Hodgkinson amd M. Janus */

#ifndef XAODPFLOW_XAODPFODICT_H
#define XAODPFLOW_XAODPFODICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFOAuxContainer.h"
#include "xAODPFlow/versions/PFOContainer_v1.h"
#include "xAODPFlow/versions/PFOAuxContainer_v1.h"
#include "xAODPFlow/PFODefs.h"

namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODPFLOW {
    xAOD::PFOContainer_v1 c1;
    DataLink< xAOD::PFOContainer_v1 > l1;
    ElementLink< xAOD::PFOContainer_v1 > l2;
    std::vector< DataLink< xAOD::PFOContainer_v1 > > l4;
    std::vector< ElementLink< xAOD::PFOContainer_v1 > > l5;
    std::vector< std::vector< ElementLink< xAOD::PFOContainer_v1 > > > l6;

    xAOD::IParticleContainer c2;
    ElementLink<xAOD::IParticleContainer> l8;
    std::vector< ElementLink<xAOD::IParticleContainer> > l9;
    std::vector<std::vector< ElementLink<xAOD::IParticleContainer> > > l10;

    std::vector<xAOD::PFODetails::PFOLeptonType> l11;

   };
}

#endif // XAODPFLOW_XAODPFODICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::PFO_v1> >", payloadCode, "@",
"DataLink<xAOD::PFOContainer_v1>", payloadCode, "@",
"DataVector<xAOD::PFO_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::PFO_v1> >", payloadCode, "@",
"ElementLink<xAOD::PFOContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::PFO_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::PFOContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::IParticle> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::PFO_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::IParticleContainer> >", payloadCode, "@",
"vector<ElementLink<xAOD::PFOContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::IParticleContainer> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::PFOContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::IParticle> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::PFO_v1> > > >", payloadCode, "@",
"vector<xAOD::PFODetails::PFOLeptonType>", payloadCode, "@",
"xAOD::PFOAuxContainer_v1", payloadCode, "@",
"xAOD::PFOContainer_v1", payloadCode, "@",
"xAOD::PFODetails::PFOAttributes", payloadCode, "@",
"xAOD::PFODetails::PFOLeptonType", payloadCode, "@",
"xAOD::PFODetails::PFOParticleType", payloadCode, "@",
"xAOD::PFO_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODPFlow_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODPFlow_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODPFlow_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODPFlow_Reflex() {
  TriggerDictionaryInitialization_xAODPFlow_Reflex_Impl();
}

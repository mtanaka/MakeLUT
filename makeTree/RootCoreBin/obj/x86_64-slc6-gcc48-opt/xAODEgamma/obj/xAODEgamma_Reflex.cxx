// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODEgammadIobjdIxAODEgamma_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEgamma/xAODEgamma/xAODEgammaDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLEgamma_v1_Dictionary();
   static void xAODcLcLEgamma_v1_TClassManip(TClass*);
   static void delete_xAODcLcLEgamma_v1(void *p);
   static void deleteArray_xAODcLcLEgamma_v1(void *p);
   static void destruct_xAODcLcLEgamma_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLEgamma_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::Egamma_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::Egamma_v1* newObj = (xAOD::Egamma_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::Egamma_v1*)
   {
      ::xAOD::Egamma_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::Egamma_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::Egamma_v1", "xAODEgamma/versions/Egamma_v1.h", 48,
                  typeid(::xAOD::Egamma_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEgamma_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::Egamma_v1) );
      instance.SetDelete(&delete_xAODcLcLEgamma_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEgamma_v1);
      instance.SetDestructor(&destruct_xAODcLcLEgamma_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::Egamma_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLEgamma_v1_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::Egamma_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::Egamma_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::Egamma_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEgamma_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::Egamma_v1*)0x0)->GetClass();
      xAODcLcLEgamma_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEgamma_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLEgamma_v1gR_Dictionary();
   static void DataVectorlExAODcLcLEgamma_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLEgamma_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLEgamma_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLEgamma_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLEgamma_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLEgamma_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::Egamma_v1>*)
   {
      ::DataVector<xAOD::Egamma_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::Egamma_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::Egamma_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::Egamma_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLEgamma_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::Egamma_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLEgamma_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLEgamma_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLEgamma_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLEgamma_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLEgamma_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::Egamma_v1>","xAOD::EgammaContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::Egamma_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::Egamma_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::Egamma_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLEgamma_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::Egamma_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLEgamma_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLEgamma_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","CE53CD7A-0B8E-44F5-9AB1-997E87713BC5");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEgammaAuxContainer_v1_Dictionary();
   static void xAODcLcLEgammaAuxContainer_v1_TClassManip(TClass*);
   static void delete_xAODcLcLEgammaAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLEgammaAuxContainer_v1(void *p);
   static void destruct_xAODcLcLEgammaAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EgammaAuxContainer_v1*)
   {
      ::xAOD::EgammaAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EgammaAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EgammaAuxContainer_v1", "xAODEgamma/versions/EgammaAuxContainer_v1.h", 35,
                  typeid(::xAOD::EgammaAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEgammaAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EgammaAuxContainer_v1) );
      instance.SetDelete(&delete_xAODcLcLEgammaAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEgammaAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLEgammaAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EgammaAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EgammaAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EgammaAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEgammaAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EgammaAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLEgammaAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEgammaAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","01A7F3AA-D5CA-4FCF-A314-20F822FF6CA0");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::Egamma_v1> >*)
   {
      ::DataLink<DataVector<xAOD::Egamma_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::Egamma_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::Egamma_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::Egamma_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::Egamma_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::Egamma_v1> >","DataLink<xAOD::EgammaContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::Egamma_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::Egamma_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Egamma_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Egamma_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::Egamma_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::Egamma_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::Egamma_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::Egamma_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::Egamma_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::Egamma_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::Egamma_v1> >","ElementLink<xAOD::EgammaContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::Egamma_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::Egamma_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Egamma_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Egamma_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary();
   static void ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLinkVector<DataVector<xAOD::Egamma_v1> >*)
   {
      ::ElementLinkVector<DataVector<xAOD::Egamma_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLinkVector<DataVector<xAOD::Egamma_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLinkVector<DataVector<xAOD::Egamma_v1> >", "AthLinks/ElementLinkVector.h", 34,
                  typeid(::ElementLinkVector<DataVector<xAOD::Egamma_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLinkVector<DataVector<xAOD::Egamma_v1> >) );
      instance.SetNew(&new_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLinkVector<DataVector<xAOD::Egamma_v1> >","ElementLinkVector<xAOD::EgammaContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLinkVector<DataVector<xAOD::Egamma_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLinkVector<DataVector<xAOD::Egamma_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Egamma_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Egamma_v1> >*)0x0)->GetClass();
      ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLElectron_v1_Dictionary();
   static void xAODcLcLElectron_v1_TClassManip(TClass*);
   static void *new_xAODcLcLElectron_v1(void *p = 0);
   static void *newArray_xAODcLcLElectron_v1(Long_t size, void *p);
   static void delete_xAODcLcLElectron_v1(void *p);
   static void deleteArray_xAODcLcLElectron_v1(void *p);
   static void destruct_xAODcLcLElectron_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::Electron_v1*)
   {
      ::xAOD::Electron_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::Electron_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::Electron_v1", "xAODEgamma/versions/Electron_v1.h", 33,
                  typeid(::xAOD::Electron_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLElectron_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::Electron_v1) );
      instance.SetNew(&new_xAODcLcLElectron_v1);
      instance.SetNewArray(&newArray_xAODcLcLElectron_v1);
      instance.SetDelete(&delete_xAODcLcLElectron_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLElectron_v1);
      instance.SetDestructor(&destruct_xAODcLcLElectron_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::Electron_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::Electron_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::Electron_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLElectron_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::Electron_v1*)0x0)->GetClass();
      xAODcLcLElectron_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLElectron_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLElectron_v1gR_Dictionary();
   static void DataVectorlExAODcLcLElectron_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLElectron_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLElectron_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLElectron_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLElectron_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLElectron_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::Electron_v1>*)
   {
      ::DataVector<xAOD::Electron_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::Electron_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::Electron_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::Electron_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLElectron_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::Electron_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLElectron_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLElectron_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLElectron_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLElectron_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLElectron_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::Electron_v1>","xAOD::ElectronContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::Electron_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::Electron_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::Electron_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLElectron_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::Electron_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLElectron_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLElectron_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","9CA52CF4-E219-45B8-9971-6DAA89125952");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLElectronAuxContainer_v1_Dictionary();
   static void xAODcLcLElectronAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLElectronAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLElectronAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLElectronAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLElectronAuxContainer_v1(void *p);
   static void destruct_xAODcLcLElectronAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::ElectronAuxContainer_v1*)
   {
      ::xAOD::ElectronAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::ElectronAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::ElectronAuxContainer_v1", "xAODEgamma/versions/ElectronAuxContainer_v1.h", 29,
                  typeid(::xAOD::ElectronAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLElectronAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::ElectronAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLElectronAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLElectronAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLElectronAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLElectronAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLElectronAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::ElectronAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::ElectronAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::ElectronAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLElectronAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::ElectronAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLElectronAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLElectronAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","85A46300-3F57-454C-8B7E-94B653AA70CF");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLElectronAuxContainer_v2_Dictionary();
   static void xAODcLcLElectronAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLElectronAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLElectronAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLElectronAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLElectronAuxContainer_v2(void *p);
   static void destruct_xAODcLcLElectronAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::ElectronAuxContainer_v2*)
   {
      ::xAOD::ElectronAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::ElectronAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::ElectronAuxContainer_v2", "xAODEgamma/versions/ElectronAuxContainer_v2.h", 31,
                  typeid(::xAOD::ElectronAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLElectronAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::ElectronAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLElectronAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLElectronAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLElectronAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLElectronAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLElectronAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::ElectronAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::ElectronAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::ElectronAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLElectronAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::ElectronAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLElectronAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLElectronAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","7160FC1C-937D-474C-909B-2C0FCE1DD755");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLElectronAuxContainer_v3_Dictionary();
   static void xAODcLcLElectronAuxContainer_v3_TClassManip(TClass*);
   static void *new_xAODcLcLElectronAuxContainer_v3(void *p = 0);
   static void *newArray_xAODcLcLElectronAuxContainer_v3(Long_t size, void *p);
   static void delete_xAODcLcLElectronAuxContainer_v3(void *p);
   static void deleteArray_xAODcLcLElectronAuxContainer_v3(void *p);
   static void destruct_xAODcLcLElectronAuxContainer_v3(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::ElectronAuxContainer_v3*)
   {
      ::xAOD::ElectronAuxContainer_v3 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::ElectronAuxContainer_v3));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::ElectronAuxContainer_v3", "xAODEgamma/versions/ElectronAuxContainer_v3.h", 31,
                  typeid(::xAOD::ElectronAuxContainer_v3), DefineBehavior(ptr, ptr),
                  &xAODcLcLElectronAuxContainer_v3_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::ElectronAuxContainer_v3) );
      instance.SetNew(&new_xAODcLcLElectronAuxContainer_v3);
      instance.SetNewArray(&newArray_xAODcLcLElectronAuxContainer_v3);
      instance.SetDelete(&delete_xAODcLcLElectronAuxContainer_v3);
      instance.SetDeleteArray(&deleteArray_xAODcLcLElectronAuxContainer_v3);
      instance.SetDestructor(&destruct_xAODcLcLElectronAuxContainer_v3);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::ElectronAuxContainer_v3*)
   {
      return GenerateInitInstanceLocal((::xAOD::ElectronAuxContainer_v3*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::ElectronAuxContainer_v3*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLElectronAuxContainer_v3_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::ElectronAuxContainer_v3*)0x0)->GetClass();
      xAODcLcLElectronAuxContainer_v3_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLElectronAuxContainer_v3_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","5435686E-4B00-11E4-AEAA-02163E00A5BB");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::Electron_v1> >*)
   {
      ::DataLink<DataVector<xAOD::Electron_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::Electron_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::Electron_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::Electron_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::Electron_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::Electron_v1> >","DataLink<xAOD::ElectronContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::Electron_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::Electron_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Electron_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Electron_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::Electron_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::Electron_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::Electron_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::Electron_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::Electron_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::Electron_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::Electron_v1> >","ElementLink<xAOD::ElectronContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::Electron_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::Electron_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Electron_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Electron_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary();
   static void ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLinkVector<DataVector<xAOD::Electron_v1> >*)
   {
      ::ElementLinkVector<DataVector<xAOD::Electron_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLinkVector<DataVector<xAOD::Electron_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLinkVector<DataVector<xAOD::Electron_v1> >", "AthLinks/ElementLinkVector.h", 34,
                  typeid(::ElementLinkVector<DataVector<xAOD::Electron_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLinkVector<DataVector<xAOD::Electron_v1> >) );
      instance.SetNew(&new_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLinkVector<DataVector<xAOD::Electron_v1> >","ElementLinkVector<xAOD::ElectronContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLinkVector<DataVector<xAOD::Electron_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLinkVector<DataVector<xAOD::Electron_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Electron_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Electron_v1> >*)0x0)->GetClass();
      ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLPhoton_v1_Dictionary();
   static void xAODcLcLPhoton_v1_TClassManip(TClass*);
   static void *new_xAODcLcLPhoton_v1(void *p = 0);
   static void *newArray_xAODcLcLPhoton_v1(Long_t size, void *p);
   static void delete_xAODcLcLPhoton_v1(void *p);
   static void deleteArray_xAODcLcLPhoton_v1(void *p);
   static void destruct_xAODcLcLPhoton_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::Photon_v1*)
   {
      ::xAOD::Photon_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::Photon_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::Photon_v1", "xAODEgamma/versions/Photon_v1.h", 32,
                  typeid(::xAOD::Photon_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLPhoton_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::Photon_v1) );
      instance.SetNew(&new_xAODcLcLPhoton_v1);
      instance.SetNewArray(&newArray_xAODcLcLPhoton_v1);
      instance.SetDelete(&delete_xAODcLcLPhoton_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLPhoton_v1);
      instance.SetDestructor(&destruct_xAODcLcLPhoton_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::Photon_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::Photon_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::Photon_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLPhoton_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::Photon_v1*)0x0)->GetClass();
      xAODcLcLPhoton_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLPhoton_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLPhoton_v1gR_Dictionary();
   static void DataVectorlExAODcLcLPhoton_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLPhoton_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLPhoton_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLPhoton_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLPhoton_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLPhoton_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::Photon_v1>*)
   {
      ::DataVector<xAOD::Photon_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::Photon_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::Photon_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::Photon_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLPhoton_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::Photon_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLPhoton_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLPhoton_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLPhoton_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLPhoton_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLPhoton_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::Photon_v1>","xAOD::PhotonContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::Photon_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::Photon_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::Photon_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLPhoton_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::Photon_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLPhoton_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLPhoton_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","5F045AAE-DBD8-47E4-90AC-9162530A9565");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLPhotonAuxContainer_v1_Dictionary();
   static void xAODcLcLPhotonAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLPhotonAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLPhotonAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLPhotonAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLPhotonAuxContainer_v1(void *p);
   static void destruct_xAODcLcLPhotonAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::PhotonAuxContainer_v1*)
   {
      ::xAOD::PhotonAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::PhotonAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::PhotonAuxContainer_v1", "xAODEgamma/versions/PhotonAuxContainer_v1.h", 32,
                  typeid(::xAOD::PhotonAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLPhotonAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::PhotonAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLPhotonAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLPhotonAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLPhotonAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLPhotonAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLPhotonAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::PhotonAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::PhotonAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::PhotonAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLPhotonAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::PhotonAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLPhotonAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLPhotonAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","67A1818E-4591-4100-B8BD-9A3C9E0D4EBB");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLPhotonAuxContainer_v2_Dictionary();
   static void xAODcLcLPhotonAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLPhotonAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLPhotonAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLPhotonAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLPhotonAuxContainer_v2(void *p);
   static void destruct_xAODcLcLPhotonAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::PhotonAuxContainer_v2*)
   {
      ::xAOD::PhotonAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::PhotonAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::PhotonAuxContainer_v2", "xAODEgamma/versions/PhotonAuxContainer_v2.h", 31,
                  typeid(::xAOD::PhotonAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLPhotonAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::PhotonAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLPhotonAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLPhotonAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLPhotonAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLPhotonAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLPhotonAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::PhotonAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::PhotonAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::PhotonAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLPhotonAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::PhotonAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLPhotonAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLPhotonAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","CAE4C9A6-B3D0-429B-9A4F-1F174D892CA5");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLPhotonAuxContainer_v3_Dictionary();
   static void xAODcLcLPhotonAuxContainer_v3_TClassManip(TClass*);
   static void *new_xAODcLcLPhotonAuxContainer_v3(void *p = 0);
   static void *newArray_xAODcLcLPhotonAuxContainer_v3(Long_t size, void *p);
   static void delete_xAODcLcLPhotonAuxContainer_v3(void *p);
   static void deleteArray_xAODcLcLPhotonAuxContainer_v3(void *p);
   static void destruct_xAODcLcLPhotonAuxContainer_v3(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::PhotonAuxContainer_v3*)
   {
      ::xAOD::PhotonAuxContainer_v3 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::PhotonAuxContainer_v3));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::PhotonAuxContainer_v3", "xAODEgamma/versions/PhotonAuxContainer_v3.h", 31,
                  typeid(::xAOD::PhotonAuxContainer_v3), DefineBehavior(ptr, ptr),
                  &xAODcLcLPhotonAuxContainer_v3_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::PhotonAuxContainer_v3) );
      instance.SetNew(&new_xAODcLcLPhotonAuxContainer_v3);
      instance.SetNewArray(&newArray_xAODcLcLPhotonAuxContainer_v3);
      instance.SetDelete(&delete_xAODcLcLPhotonAuxContainer_v3);
      instance.SetDeleteArray(&deleteArray_xAODcLcLPhotonAuxContainer_v3);
      instance.SetDestructor(&destruct_xAODcLcLPhotonAuxContainer_v3);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::PhotonAuxContainer_v3*)
   {
      return GenerateInitInstanceLocal((::xAOD::PhotonAuxContainer_v3*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::PhotonAuxContainer_v3*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLPhotonAuxContainer_v3_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::PhotonAuxContainer_v3*)0x0)->GetClass();
      xAODcLcLPhotonAuxContainer_v3_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLPhotonAuxContainer_v3_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","5C389DEC-4B00-11E4-B9F0-02163E00A5BB");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::Photon_v1> >*)
   {
      ::DataLink<DataVector<xAOD::Photon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::Photon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::Photon_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::Photon_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::Photon_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::Photon_v1> >","DataLink<xAOD::PhotonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::Photon_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::Photon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Photon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Photon_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::Photon_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::Photon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::Photon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::Photon_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::Photon_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::Photon_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::Photon_v1> >","ElementLink<xAOD::PhotonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::Photon_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::Photon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Photon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Photon_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary();
   static void ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLinkVector<DataVector<xAOD::Photon_v1> >*)
   {
      ::ElementLinkVector<DataVector<xAOD::Photon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLinkVector<DataVector<xAOD::Photon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLinkVector<DataVector<xAOD::Photon_v1> >", "AthLinks/ElementLinkVector.h", 34,
                  typeid(::ElementLinkVector<DataVector<xAOD::Photon_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLinkVector<DataVector<xAOD::Photon_v1> >) );
      instance.SetNew(&new_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLinkVector<DataVector<xAOD::Photon_v1> >","ElementLinkVector<xAOD::PhotonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLinkVector<DataVector<xAOD::Photon_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLinkVector<DataVector<xAOD::Photon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Photon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Photon_v1> >*)0x0)->GetClass();
      ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook_Dictionary();
   static void xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook_TClassManip(TClass*);
   static void *new_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(void *p = 0);
   static void *newArray_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(Long_t size, void *p);
   static void delete_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(void *p);
   static void deleteArray_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(void *p);
   static void destruct_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook*)
   {
      ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook", "xAODEgamma/EgammaDefs.h", 129,
                  typeid(::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook), DefineBehavior(ptr, ptr),
                  &xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook) );
      instance.SetNew(&new_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook);
      instance.SetNewArray(&newArray_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook);
      instance.SetDelete(&delete_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook);
      instance.SetDestructor(&destruct_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook*)
   {
      return GenerateInitInstanceLocal((::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook*)0x0)->GetClass();
      xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLEgamma_v1(void *p) {
      delete ((::xAOD::Egamma_v1*)p);
   }
   static void deleteArray_xAODcLcLEgamma_v1(void *p) {
      delete [] ((::xAOD::Egamma_v1*)p);
   }
   static void destruct_xAODcLcLEgamma_v1(void *p) {
      typedef ::xAOD::Egamma_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::Egamma_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLEgamma_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::Egamma_v1> : new ::DataVector<xAOD::Egamma_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLEgamma_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::Egamma_v1>[nElements] : new ::DataVector<xAOD::Egamma_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLEgamma_v1gR(void *p) {
      delete ((::DataVector<xAOD::Egamma_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLEgamma_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::Egamma_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLEgamma_v1gR(void *p) {
      typedef ::DataVector<xAOD::Egamma_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::Egamma_v1>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLEgammaAuxContainer_v1(void *p) {
      delete ((::xAOD::EgammaAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLEgammaAuxContainer_v1(void *p) {
      delete [] ((::xAOD::EgammaAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLEgammaAuxContainer_v1(void *p) {
      typedef ::xAOD::EgammaAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EgammaAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::Egamma_v1> > : new ::DataLink<DataVector<xAOD::Egamma_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::Egamma_v1> >[nElements] : new ::DataLink<DataVector<xAOD::Egamma_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::Egamma_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::Egamma_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::Egamma_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::Egamma_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::Egamma_v1> > : new ::ElementLink<DataVector<xAOD::Egamma_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::Egamma_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::Egamma_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::Egamma_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::Egamma_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::Egamma_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::Egamma_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLinkVector<DataVector<xAOD::Egamma_v1> > : new ::ElementLinkVector<DataVector<xAOD::Egamma_v1> >;
   }
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLinkVector<DataVector<xAOD::Egamma_v1> >[nElements] : new ::ElementLinkVector<DataVector<xAOD::Egamma_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      delete ((::ElementLinkVector<DataVector<xAOD::Egamma_v1> >*)p);
   }
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      delete [] ((::ElementLinkVector<DataVector<xAOD::Egamma_v1> >*)p);
   }
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgR(void *p) {
      typedef ::ElementLinkVector<DataVector<xAOD::Egamma_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLinkVector<DataVector<xAOD::Egamma_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLElectron_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Electron_v1 : new ::xAOD::Electron_v1;
   }
   static void *newArray_xAODcLcLElectron_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Electron_v1[nElements] : new ::xAOD::Electron_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLElectron_v1(void *p) {
      delete ((::xAOD::Electron_v1*)p);
   }
   static void deleteArray_xAODcLcLElectron_v1(void *p) {
      delete [] ((::xAOD::Electron_v1*)p);
   }
   static void destruct_xAODcLcLElectron_v1(void *p) {
      typedef ::xAOD::Electron_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::Electron_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLElectron_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::Electron_v1> : new ::DataVector<xAOD::Electron_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLElectron_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::Electron_v1>[nElements] : new ::DataVector<xAOD::Electron_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLElectron_v1gR(void *p) {
      delete ((::DataVector<xAOD::Electron_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLElectron_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::Electron_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLElectron_v1gR(void *p) {
      typedef ::DataVector<xAOD::Electron_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::Electron_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLElectronAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ElectronAuxContainer_v1 : new ::xAOD::ElectronAuxContainer_v1;
   }
   static void *newArray_xAODcLcLElectronAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ElectronAuxContainer_v1[nElements] : new ::xAOD::ElectronAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLElectronAuxContainer_v1(void *p) {
      delete ((::xAOD::ElectronAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLElectronAuxContainer_v1(void *p) {
      delete [] ((::xAOD::ElectronAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLElectronAuxContainer_v1(void *p) {
      typedef ::xAOD::ElectronAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::ElectronAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLElectronAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ElectronAuxContainer_v2 : new ::xAOD::ElectronAuxContainer_v2;
   }
   static void *newArray_xAODcLcLElectronAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ElectronAuxContainer_v2[nElements] : new ::xAOD::ElectronAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLElectronAuxContainer_v2(void *p) {
      delete ((::xAOD::ElectronAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLElectronAuxContainer_v2(void *p) {
      delete [] ((::xAOD::ElectronAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLElectronAuxContainer_v2(void *p) {
      typedef ::xAOD::ElectronAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::ElectronAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLElectronAuxContainer_v3(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ElectronAuxContainer_v3 : new ::xAOD::ElectronAuxContainer_v3;
   }
   static void *newArray_xAODcLcLElectronAuxContainer_v3(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ElectronAuxContainer_v3[nElements] : new ::xAOD::ElectronAuxContainer_v3[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLElectronAuxContainer_v3(void *p) {
      delete ((::xAOD::ElectronAuxContainer_v3*)p);
   }
   static void deleteArray_xAODcLcLElectronAuxContainer_v3(void *p) {
      delete [] ((::xAOD::ElectronAuxContainer_v3*)p);
   }
   static void destruct_xAODcLcLElectronAuxContainer_v3(void *p) {
      typedef ::xAOD::ElectronAuxContainer_v3 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::ElectronAuxContainer_v3

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::Electron_v1> > : new ::DataLink<DataVector<xAOD::Electron_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::Electron_v1> >[nElements] : new ::DataLink<DataVector<xAOD::Electron_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::Electron_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::Electron_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::Electron_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::Electron_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::Electron_v1> > : new ::ElementLink<DataVector<xAOD::Electron_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::Electron_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::Electron_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::Electron_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::Electron_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::Electron_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::Electron_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLinkVector<DataVector<xAOD::Electron_v1> > : new ::ElementLinkVector<DataVector<xAOD::Electron_v1> >;
   }
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLinkVector<DataVector<xAOD::Electron_v1> >[nElements] : new ::ElementLinkVector<DataVector<xAOD::Electron_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      delete ((::ElementLinkVector<DataVector<xAOD::Electron_v1> >*)p);
   }
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      delete [] ((::ElementLinkVector<DataVector<xAOD::Electron_v1> >*)p);
   }
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgR(void *p) {
      typedef ::ElementLinkVector<DataVector<xAOD::Electron_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLinkVector<DataVector<xAOD::Electron_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLPhoton_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Photon_v1 : new ::xAOD::Photon_v1;
   }
   static void *newArray_xAODcLcLPhoton_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Photon_v1[nElements] : new ::xAOD::Photon_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLPhoton_v1(void *p) {
      delete ((::xAOD::Photon_v1*)p);
   }
   static void deleteArray_xAODcLcLPhoton_v1(void *p) {
      delete [] ((::xAOD::Photon_v1*)p);
   }
   static void destruct_xAODcLcLPhoton_v1(void *p) {
      typedef ::xAOD::Photon_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::Photon_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLPhoton_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::Photon_v1> : new ::DataVector<xAOD::Photon_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLPhoton_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::Photon_v1>[nElements] : new ::DataVector<xAOD::Photon_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLPhoton_v1gR(void *p) {
      delete ((::DataVector<xAOD::Photon_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLPhoton_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::Photon_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLPhoton_v1gR(void *p) {
      typedef ::DataVector<xAOD::Photon_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::Photon_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLPhotonAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PhotonAuxContainer_v1 : new ::xAOD::PhotonAuxContainer_v1;
   }
   static void *newArray_xAODcLcLPhotonAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PhotonAuxContainer_v1[nElements] : new ::xAOD::PhotonAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLPhotonAuxContainer_v1(void *p) {
      delete ((::xAOD::PhotonAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLPhotonAuxContainer_v1(void *p) {
      delete [] ((::xAOD::PhotonAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLPhotonAuxContainer_v1(void *p) {
      typedef ::xAOD::PhotonAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::PhotonAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLPhotonAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PhotonAuxContainer_v2 : new ::xAOD::PhotonAuxContainer_v2;
   }
   static void *newArray_xAODcLcLPhotonAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PhotonAuxContainer_v2[nElements] : new ::xAOD::PhotonAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLPhotonAuxContainer_v2(void *p) {
      delete ((::xAOD::PhotonAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLPhotonAuxContainer_v2(void *p) {
      delete [] ((::xAOD::PhotonAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLPhotonAuxContainer_v2(void *p) {
      typedef ::xAOD::PhotonAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::PhotonAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLPhotonAuxContainer_v3(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PhotonAuxContainer_v3 : new ::xAOD::PhotonAuxContainer_v3;
   }
   static void *newArray_xAODcLcLPhotonAuxContainer_v3(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::PhotonAuxContainer_v3[nElements] : new ::xAOD::PhotonAuxContainer_v3[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLPhotonAuxContainer_v3(void *p) {
      delete ((::xAOD::PhotonAuxContainer_v3*)p);
   }
   static void deleteArray_xAODcLcLPhotonAuxContainer_v3(void *p) {
      delete [] ((::xAOD::PhotonAuxContainer_v3*)p);
   }
   static void destruct_xAODcLcLPhotonAuxContainer_v3(void *p) {
      typedef ::xAOD::PhotonAuxContainer_v3 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::PhotonAuxContainer_v3

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::Photon_v1> > : new ::DataLink<DataVector<xAOD::Photon_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::Photon_v1> >[nElements] : new ::DataLink<DataVector<xAOD::Photon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::Photon_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::Photon_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::Photon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::Photon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::Photon_v1> > : new ::ElementLink<DataVector<xAOD::Photon_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::Photon_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::Photon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::Photon_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::Photon_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::Photon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::Photon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLinkVector<DataVector<xAOD::Photon_v1> > : new ::ElementLinkVector<DataVector<xAOD::Photon_v1> >;
   }
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLinkVector<DataVector<xAOD::Photon_v1> >[nElements] : new ::ElementLinkVector<DataVector<xAOD::Photon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      delete ((::ElementLinkVector<DataVector<xAOD::Photon_v1> >*)p);
   }
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      delete [] ((::ElementLinkVector<DataVector<xAOD::Photon_v1> >*)p);
   }
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgR(void *p) {
      typedef ::ElementLinkVector<DataVector<xAOD::Photon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLinkVector<DataVector<xAOD::Photon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook : new ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook;
   }
   static void *newArray_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook[nElements] : new ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(void *p) {
      delete ((::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook*)p);
   }
   static void deleteArray_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(void *p) {
      delete [] ((::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook*)p);
   }
   static void destruct_xAODcLcLEgammaParameterscLcLROOT6_NamespaceAutoloadHook(void *p) {
      typedef ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook

namespace ROOT {
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >*)
   {
      vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >*)0x0)->GetClass();
      vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > > : new vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >;
   }
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >[nElements] : new vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >*)
   {
      vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >*)0x0)->GetClass();
      vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > > : new vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >;
   }
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >[nElements] : new vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >*)
   {
      vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >*)0x0)->GetClass();
      vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > > : new vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >;
   }
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >[nElements] : new vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::Photon_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::Photon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::Photon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::Photon_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::Photon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::Photon_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::Photon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Photon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Photon_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Photon_v1> > > : new vector<ElementLink<DataVector<xAOD::Photon_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Photon_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::Photon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::Photon_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::Photon_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::Photon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::Photon_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::Electron_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::Electron_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::Electron_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::Electron_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::Electron_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::Electron_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::Electron_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Electron_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Electron_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Electron_v1> > > : new vector<ElementLink<DataVector<xAOD::Electron_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Electron_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::Electron_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::Electron_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::Electron_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::Electron_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::Electron_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::Egamma_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::Egamma_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::Egamma_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::Egamma_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::Egamma_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::Egamma_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::Egamma_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Egamma_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Egamma_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Egamma_v1> > > : new vector<ElementLink<DataVector<xAOD::Egamma_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Egamma_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::Egamma_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::Egamma_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::Egamma_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::Egamma_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::Egamma_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::Photon_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::Photon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::Photon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::Photon_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::Photon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::Photon_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::Photon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Photon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Photon_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Photon_v1> > > : new vector<DataLink<DataVector<xAOD::Photon_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Photon_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::Photon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::Photon_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::Photon_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLPhoton_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::Photon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::Photon_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::Electron_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::Electron_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::Electron_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::Electron_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::Electron_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::Electron_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::Electron_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Electron_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Electron_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Electron_v1> > > : new vector<DataLink<DataVector<xAOD::Electron_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Electron_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::Electron_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::Electron_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::Electron_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLElectron_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::Electron_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::Electron_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::Egamma_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::Egamma_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::Egamma_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::Egamma_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::Egamma_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::Egamma_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::Egamma_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Egamma_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Egamma_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Egamma_v1> > > : new vector<DataLink<DataVector<xAOD::Egamma_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Egamma_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::Egamma_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::Egamma_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::Egamma_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLEgamma_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::Egamma_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::Egamma_v1> > >

namespace ROOT {
   static TClass *setlEconstsPxAODcLcLTrackParticle_v1mUgR_Dictionary();
   static void setlEconstsPxAODcLcLTrackParticle_v1mUgR_TClassManip(TClass*);
   static void *new_setlEconstsPxAODcLcLTrackParticle_v1mUgR(void *p = 0);
   static void *newArray_setlEconstsPxAODcLcLTrackParticle_v1mUgR(Long_t size, void *p);
   static void delete_setlEconstsPxAODcLcLTrackParticle_v1mUgR(void *p);
   static void deleteArray_setlEconstsPxAODcLcLTrackParticle_v1mUgR(void *p);
   static void destruct_setlEconstsPxAODcLcLTrackParticle_v1mUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const set<const xAOD::TrackParticle_v1*>*)
   {
      set<const xAOD::TrackParticle_v1*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(set<const xAOD::TrackParticle_v1*>));
      static ::ROOT::TGenericClassInfo 
         instance("set<const xAOD::TrackParticle_v1*>", -2, "set", 90,
                  typeid(set<const xAOD::TrackParticle_v1*>), DefineBehavior(ptr, ptr),
                  &setlEconstsPxAODcLcLTrackParticle_v1mUgR_Dictionary, isa_proxy, 4,
                  sizeof(set<const xAOD::TrackParticle_v1*>) );
      instance.SetNew(&new_setlEconstsPxAODcLcLTrackParticle_v1mUgR);
      instance.SetNewArray(&newArray_setlEconstsPxAODcLcLTrackParticle_v1mUgR);
      instance.SetDelete(&delete_setlEconstsPxAODcLcLTrackParticle_v1mUgR);
      instance.SetDeleteArray(&deleteArray_setlEconstsPxAODcLcLTrackParticle_v1mUgR);
      instance.SetDestructor(&destruct_setlEconstsPxAODcLcLTrackParticle_v1mUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Insert< set<const xAOD::TrackParticle_v1*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const set<const xAOD::TrackParticle_v1*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *setlEconstsPxAODcLcLTrackParticle_v1mUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const set<const xAOD::TrackParticle_v1*>*)0x0)->GetClass();
      setlEconstsPxAODcLcLTrackParticle_v1mUgR_TClassManip(theClass);
   return theClass;
   }

   static void setlEconstsPxAODcLcLTrackParticle_v1mUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_setlEconstsPxAODcLcLTrackParticle_v1mUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) set<const xAOD::TrackParticle_v1*> : new set<const xAOD::TrackParticle_v1*>;
   }
   static void *newArray_setlEconstsPxAODcLcLTrackParticle_v1mUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) set<const xAOD::TrackParticle_v1*>[nElements] : new set<const xAOD::TrackParticle_v1*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_setlEconstsPxAODcLcLTrackParticle_v1mUgR(void *p) {
      delete ((set<const xAOD::TrackParticle_v1*>*)p);
   }
   static void deleteArray_setlEconstsPxAODcLcLTrackParticle_v1mUgR(void *p) {
      delete [] ((set<const xAOD::TrackParticle_v1*>*)p);
   }
   static void destruct_setlEconstsPxAODcLcLTrackParticle_v1mUgR(void *p) {
      typedef set<const xAOD::TrackParticle_v1*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class set<const xAOD::TrackParticle_v1*>

namespace {
  void TriggerDictionaryInitialization_xAODEgamma_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEgamma/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEgamma/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODEgamma/EgammaContainer.h")))  Egamma_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODEgamma/EgammaContainer.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@01A7F3AA-D5CA-4FCF-A314-20F822FF6CA0)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEgamma/versions/EgammaAuxContainer_v1.h")))  EgammaAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODEgamma/ElectronContainer.h")))  Electron_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@85A46300-3F57-454C-8B7E-94B653AA70CF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEgamma/versions/ElectronAuxContainer_v1.h")))  ElectronAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@7160FC1C-937D-474C-909B-2C0FCE1DD755)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEgamma/versions/ElectronAuxContainer_v2.h")))  ElectronAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@5435686E-4B00-11E4-AEAA-02163E00A5BB)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEgamma/versions/ElectronAuxContainer_v3.h")))  ElectronAuxContainer_v3;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODEgamma/PhotonContainer.h")))  Photon_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@67A1818E-4591-4100-B8BD-9A3C9E0D4EBB)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEgamma/versions/PhotonAuxContainer_v1.h")))  PhotonAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@CAE4C9A6-B3D0-429B-9A4F-1F174D892CA5)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEgamma/versions/PhotonAuxContainer_v2.h")))  PhotonAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@5C389DEC-4B00-11E4-B9F0-02163E00A5BB)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEgamma/versions/PhotonAuxContainer_v3.h")))  PhotonAuxContainer_v3;}
namespace xAOD{namespace EgammaParameters{struct __attribute__((annotate("$clingAutoload$xAODEgamma/EgammaContainer.h")))  ROOT6_NamespaceAutoloadHook;}}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODEgamma/ElectronContainer.h")))  TrackParticle_v1;}
namespace std{template <typename _Tp> struct __attribute__((annotate("$clingAutoload$string")))  less;
}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODEgamma"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODEgammaDict.h 645853 2015-02-10 20:40:06Z blenzi $
#ifndef XAODEGAMMA_XAODEGAMMADICT_H
#define XAODEGAMMA_XAODEGAMMADICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
#include "xAODEgamma/EgammaContainer.h"
#include "xAODEgamma/versions/EgammaContainer_v1.h"
#include "xAODEgamma/versions/EgammaAuxContainer_v1.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/versions/ElectronContainer_v1.h"
#include "xAODEgamma/versions/ElectronAuxContainer_v1.h"
#include "xAODEgamma/versions/ElectronAuxContainer_v2.h"
#include "xAODEgamma/versions/ElectronAuxContainer_v3.h"

#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/versions/PhotonContainer_v1.h"
#include "xAODEgamma/versions/PhotonAuxContainer_v1.h"
#include "xAODEgamma/versions/PhotonAuxContainer_v2.h"
#include "xAODEgamma/versions/PhotonAuxContainer_v3.h"

#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include "xAODEgamma/PhotonxAODHelpers.h"
#include "xAODEgamma/EgammaTruthxAODHelpers.h"
#include "xAODEgamma/EgammaEnums.h"
#include "xAODEgamma/EgammaDefs.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODEGAMMA {
      xAOD::EgammaContainer_v1 eg_c1;
      DataLink< xAOD::EgammaContainer_v1 > eg_l1;
      ElementLink< xAOD::EgammaContainer_v1 > eg_l2;
      ElementLinkVector< xAOD::EgammaContainer_v1 > eg_l3;
      std::vector< DataLink< xAOD::EgammaContainer_v1 > > eg_l4;
      std::vector< ElementLink< xAOD::EgammaContainer_v1 > > eg_l5;
      std::vector< ElementLinkVector< xAOD::EgammaContainer_v1 > > eg_l6;

      xAOD::ElectronContainer_v1 el_c1;
      DataLink< xAOD::ElectronContainer_v1 > el_l1;
      ElementLink< xAOD::ElectronContainer_v1 > el_l2;
      ElementLinkVector< xAOD::ElectronContainer_v1 > el_l3;
      std::vector< DataLink< xAOD::ElectronContainer_v1 > > el_l4;
      std::vector< ElementLink< xAOD::ElectronContainer_v1 > > el_l5;
      std::vector< ElementLinkVector< xAOD::ElectronContainer_v1 > > el_l6;

      xAOD::PhotonContainer_v1 ph_c1;
      DataLink< xAOD::PhotonContainer_v1 > ph_l1;
      ElementLink< xAOD::PhotonContainer_v1 > ph_l2;
      ElementLinkVector< xAOD::PhotonContainer_v1 > ph_l3;
      std::vector< DataLink< xAOD::PhotonContainer_v1 > > ph_l4;
      std::vector< ElementLink< xAOD::PhotonContainer_v1 > > ph_l5;
      std::vector< ElementLinkVector< xAOD::PhotonContainer_v1 > > ph_l6;

     // Instantiate the classes used by xAOD::Electron, xAODPhoton so that
     // Reflex would see them with their "correct type". Note that the
     // dictionary for these types comes from other places. This instantiation
     // is just needed for "Reflex related technical reasons"...
     ElementLink< xAOD::TrackParticleContainer > auxlink1;
     std::vector< ElementLink< xAOD::TrackParticleContainer > > auxlink2;
     ElementLink< xAOD::CaloClusterContainer > auxlink3;
     std::vector< ElementLink< xAOD::CaloClusterContainer > > auxlink4;
     ElementLink< xAOD::VertexContainer > auxlink5;
     std::vector< ElementLink< xAOD::VertexContainer > > auxlink6;
     
     std::set<const xAOD::TrackParticle*> setTP;

   };
}

#endif // XAODEGAMMA_XAODEGAMMADICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::Egamma_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::Electron_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::Photon_v1> >", payloadCode, "@",
"DataLink<xAOD::EgammaContainer_v1>", payloadCode, "@",
"DataLink<xAOD::ElectronContainer_v1>", payloadCode, "@",
"DataLink<xAOD::PhotonContainer_v1>", payloadCode, "@",
"DataVector<xAOD::Egamma_v1>", payloadCode, "@",
"DataVector<xAOD::Electron_v1>", payloadCode, "@",
"DataVector<xAOD::Photon_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::Egamma_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::Electron_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::Photon_v1> >", payloadCode, "@",
"ElementLink<xAOD::EgammaContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::ElectronContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::PhotonContainer_v1>", payloadCode, "@",
"ElementLinkVector<DataVector<xAOD::Egamma_v1> >", payloadCode, "@",
"ElementLinkVector<DataVector<xAOD::Electron_v1> >", payloadCode, "@",
"ElementLinkVector<DataVector<xAOD::Photon_v1> >", payloadCode, "@",
"ElementLinkVector<xAOD::EgammaContainer_v1>", payloadCode, "@",
"ElementLinkVector<xAOD::ElectronContainer_v1>", payloadCode, "@",
"ElementLinkVector<xAOD::PhotonContainer_v1>", payloadCode, "@",
"set<const xAOD::TrackParticle*>", payloadCode, "@",
"set<const xAOD::TrackParticle_v1*>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::Egamma_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::Electron_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::Photon_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::EgammaContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::ElectronContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::PhotonContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::Egamma_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::Electron_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::Photon_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::EgammaContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::ElectronContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::PhotonContainer_v1> >", payloadCode, "@",
"vector<ElementLinkVector<DataVector<xAOD::Egamma_v1> > >", payloadCode, "@",
"vector<ElementLinkVector<DataVector<xAOD::Electron_v1> > >", payloadCode, "@",
"vector<ElementLinkVector<DataVector<xAOD::Photon_v1> > >", payloadCode, "@",
"vector<ElementLinkVector<xAOD::EgammaContainer_v1> >", payloadCode, "@",
"vector<ElementLinkVector<xAOD::ElectronContainer_v1> >", payloadCode, "@",
"vector<ElementLinkVector<xAOD::PhotonContainer_v1> >", payloadCode, "@",
"xAOD::EgammaAuxContainer_v1", payloadCode, "@",
"xAOD::EgammaContainer_v1", payloadCode, "@",
"xAOD::EgammaHelpers::conversionRadius", payloadCode, "@",
"xAOD::EgammaHelpers::conversionType", payloadCode, "@",
"xAOD::EgammaHelpers::getBkgElectronMother", payloadCode, "@",
"xAOD::EgammaHelpers::getLastMeasurementQoverP", payloadCode, "@",
"xAOD::EgammaHelpers::getOriginalTrackParticle", payloadCode, "@",
"xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF", payloadCode, "@",
"xAOD::EgammaHelpers::getRecoElectron", payloadCode, "@",
"xAOD::EgammaHelpers::getRecoPhoton", payloadCode, "@",
"xAOD::EgammaHelpers::getTrackParticles", payloadCode, "@",
"xAOD::EgammaHelpers::isBarrel", payloadCode, "@",
"xAOD::EgammaHelpers::isConvertedPhoton", payloadCode, "@",
"xAOD::EgammaHelpers::isElectron", payloadCode, "@",
"xAOD::EgammaHelpers::isFwdElectron", payloadCode, "@",
"xAOD::EgammaHelpers::isPhoton", payloadCode, "@",
"xAOD::EgammaHelpers::isTrueConvertedPhoton", payloadCode, "@",
"xAOD::EgammaHelpers::momentumAtVertex", payloadCode, "@",
"xAOD::EgammaHelpers::numberOfSiHits", payloadCode, "@",
"xAOD::EgammaHelpers::numberOfSiTracks", payloadCode, "@",
"xAOD::EgammaHelpers::summaryValueFloat", payloadCode, "@",
"xAOD::EgammaHelpers::summaryValueInt", payloadCode, "@",
"xAOD::EgammaParameters::ALLOQ", payloadCode, "@",
"xAOD::EgammaParameters::AuthorALL", payloadCode, "@",
"xAOD::EgammaParameters::AuthorAmbiguous", payloadCode, "@",
"xAOD::EgammaParameters::AuthorCaloTopo35", payloadCode, "@",
"xAOD::EgammaParameters::AuthorElectron", payloadCode, "@",
"xAOD::EgammaParameters::AuthorFwdElectron", payloadCode, "@",
"xAOD::EgammaParameters::AuthorPhoton", payloadCode, "@",
"xAOD::EgammaParameters::AuthorSofte", payloadCode, "@",
"xAOD::EgammaParameters::AuthorTrigElectron", payloadCode, "@",
"xAOD::EgammaParameters::AuthorTrigPhoton", payloadCode, "@",
"xAOD::EgammaParameters::AuthorUnknown", payloadCode, "@",
"xAOD::EgammaParameters::BADCLUSELECTRON", payloadCode, "@",
"xAOD::EgammaParameters::BADCLUSPHOTON", payloadCode, "@",
"xAOD::EgammaParameters::BitDefOQ", payloadCode, "@",
"xAOD::EgammaParameters::ConversionType", payloadCode, "@",
"xAOD::EgammaParameters::EgammaType", payloadCode, "@",
"xAOD::EgammaParameters::ROOT6_NamespaceAutoloadHook", payloadCode, "@",
"xAOD::EgammaParameters::SelectionMenu", payloadCode, "@",
"xAOD::EgammaParameters::SelectionisEM", payloadCode, "@",
"xAOD::EgammaParameters::ShowerShapeType", payloadCode, "@",
"xAOD::EgammaParameters::TrackCaloMatchType", payloadCode, "@",
"xAOD::EgammaParameters::VertexCaloMatchType", payloadCode, "@",
"xAOD::Egamma_v1", payloadCode, "@",
"xAOD::ElectronAuxContainer_v1", payloadCode, "@",
"xAOD::ElectronAuxContainer_v2", payloadCode, "@",
"xAOD::ElectronAuxContainer_v3", payloadCode, "@",
"xAOD::ElectronContainer_v1", payloadCode, "@",
"xAOD::Electron_v1", payloadCode, "@",
"xAOD::PhotonAuxContainer_v1", payloadCode, "@",
"xAOD::PhotonAuxContainer_v2", payloadCode, "@",
"xAOD::PhotonAuxContainer_v3", payloadCode, "@",
"xAOD::PhotonContainer_v1", payloadCode, "@",
"xAOD::Photon_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODEgamma_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODEgamma_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODEgamma_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODEgamma_Reflex() {
  TriggerDictionaryInitialization_xAODEgamma_Reflex_Impl();
}

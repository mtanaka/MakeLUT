#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdItrigTutTestdIobjdItrigTutTestCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "trigTutTest/MyxAODAnalysisData.h"
#include "trigTutTest/Util.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_MyxAODAnalysisData(void *p = 0);
   static void *newArray_MyxAODAnalysisData(Long_t size, void *p);
   static void delete_MyxAODAnalysisData(void *p);
   static void deleteArray_MyxAODAnalysisData(void *p);
   static void destruct_MyxAODAnalysisData(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MyxAODAnalysisData*)
   {
      ::MyxAODAnalysisData *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MyxAODAnalysisData >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MyxAODAnalysisData", ::MyxAODAnalysisData::Class_Version(), "trigTutTest/MyxAODAnalysisData.h", 20,
                  typeid(::MyxAODAnalysisData), DefineBehavior(ptr, ptr),
                  &::MyxAODAnalysisData::Dictionary, isa_proxy, 4,
                  sizeof(::MyxAODAnalysisData) );
      instance.SetNew(&new_MyxAODAnalysisData);
      instance.SetNewArray(&newArray_MyxAODAnalysisData);
      instance.SetDelete(&delete_MyxAODAnalysisData);
      instance.SetDeleteArray(&deleteArray_MyxAODAnalysisData);
      instance.SetDestructor(&destruct_MyxAODAnalysisData);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MyxAODAnalysisData*)
   {
      return GenerateInitInstanceLocal((::MyxAODAnalysisData*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MyxAODAnalysisData*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *Util_Dictionary();
   static void Util_TClassManip(TClass*);
   static void *new_Util(void *p = 0);
   static void *newArray_Util(Long_t size, void *p);
   static void delete_Util(void *p);
   static void deleteArray_Util(void *p);
   static void destruct_Util(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Util*)
   {
      ::Util *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Util));
      static ::ROOT::TGenericClassInfo 
         instance("Util", "trigTutTest/Util.h", 9,
                  typeid(::Util), DefineBehavior(ptr, ptr),
                  &Util_Dictionary, isa_proxy, 4,
                  sizeof(::Util) );
      instance.SetNew(&new_Util);
      instance.SetNewArray(&newArray_Util);
      instance.SetDelete(&delete_Util);
      instance.SetDeleteArray(&deleteArray_Util);
      instance.SetDestructor(&destruct_Util);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Util*)
   {
      return GenerateInitInstanceLocal((::Util*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Util*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Util_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Util*)0x0)->GetClass();
      Util_TClassManip(theClass);
   return theClass;
   }

   static void Util_TClassManip(TClass* ){
   }

} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr MyxAODAnalysisData::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MyxAODAnalysisData::Class_Name()
{
   return "MyxAODAnalysisData";
}

//______________________________________________________________________________
const char *MyxAODAnalysisData::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MyxAODAnalysisData*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MyxAODAnalysisData::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MyxAODAnalysisData*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MyxAODAnalysisData::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MyxAODAnalysisData*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MyxAODAnalysisData::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MyxAODAnalysisData*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void MyxAODAnalysisData::Streamer(TBuffer &R__b)
{
   // Stream an object of class MyxAODAnalysisData.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MyxAODAnalysisData::Class(),this);
   } else {
      R__b.WriteClassBuffer(MyxAODAnalysisData::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MyxAODAnalysisData(void *p) {
      return  p ? new(p) ::MyxAODAnalysisData : new ::MyxAODAnalysisData;
   }
   static void *newArray_MyxAODAnalysisData(Long_t nElements, void *p) {
      return p ? new(p) ::MyxAODAnalysisData[nElements] : new ::MyxAODAnalysisData[nElements];
   }
   // Wrapper around operator delete
   static void delete_MyxAODAnalysisData(void *p) {
      delete ((::MyxAODAnalysisData*)p);
   }
   static void deleteArray_MyxAODAnalysisData(void *p) {
      delete [] ((::MyxAODAnalysisData*)p);
   }
   static void destruct_MyxAODAnalysisData(void *p) {
      typedef ::MyxAODAnalysisData current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MyxAODAnalysisData

namespace ROOT {
   // Wrappers around operator new
   static void *new_Util(void *p) {
      return  p ? new(p) ::Util : new ::Util;
   }
   static void *newArray_Util(Long_t nElements, void *p) {
      return p ? new(p) ::Util[nElements] : new ::Util[nElements];
   }
   // Wrapper around operator delete
   static void delete_Util(void *p) {
      delete ((::Util*)p);
   }
   static void deleteArray_Util(void *p) {
      delete [] ((::Util*)p);
   }
   static void destruct_Util(void *p) {
      typedef ::Util current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Util

namespace ROOT {
   static TClass *maplEstringcOintgR_Dictionary();
   static void maplEstringcOintgR_TClassManip(TClass*);
   static void *new_maplEstringcOintgR(void *p = 0);
   static void *newArray_maplEstringcOintgR(Long_t size, void *p);
   static void delete_maplEstringcOintgR(void *p);
   static void deleteArray_maplEstringcOintgR(void *p);
   static void destruct_maplEstringcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,int>*)
   {
      map<string,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,int>", -2, "map", 96,
                  typeid(map<string,int>), DefineBehavior(ptr, ptr),
                  &maplEstringcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,int>) );
      instance.SetNew(&new_maplEstringcOintgR);
      instance.SetNewArray(&newArray_maplEstringcOintgR);
      instance.SetDelete(&delete_maplEstringcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOintgR);
      instance.SetDestructor(&destruct_maplEstringcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<string,int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,int>*)0x0)->GetClass();
      maplEstringcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,int> : new map<string,int>;
   }
   static void *newArray_maplEstringcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<string,int>[nElements] : new map<string,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOintgR(void *p) {
      delete ((map<string,int>*)p);
   }
   static void deleteArray_maplEstringcOintgR(void *p) {
      delete [] ((map<string,int>*)p);
   }
   static void destruct_maplEstringcOintgR(void *p) {
      typedef map<string,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,int>

namespace {
  void TriggerDictionaryInitialization_trigTutTestCINT_Impl() {
    static const char* headers[] = {
"trigTutTest/MyxAODAnalysisData.h",
"trigTutTest/Util.h",
0
    };
    static const char* includePaths[] = {
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/trigTutTest/Root",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/trigTutTest",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/trigTutTest/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/trigTutTest/Root/LinkDef.h")))  MyxAODAnalysisData;
class __attribute__((annotate("$clingAutoload$/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/trigTutTest/Root/LinkDef.h")))  Util;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "trigTutTest"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "trigTutTest/MyxAODAnalysisData.h"
#include "trigTutTest/Util.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"MyxAODAnalysisData", payloadCode, "@",
"Util", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("trigTutTestCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_trigTutTestCINT_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_trigTutTestCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_trigTutTestCINT() {
  TriggerDictionaryInitialization_trigTutTestCINT_Impl();
}

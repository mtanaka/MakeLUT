// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIMuonSelectorToolsdIobjdIMuonSelectorTools_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonSelectorTools/MuonSelectorTools/MuonSelectorToolsDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *CPcLcLMuonSelectionTool_Dictionary();
   static void CPcLcLMuonSelectionTool_TClassManip(TClass*);
   static void *new_CPcLcLMuonSelectionTool(void *p = 0);
   static void *newArray_CPcLcLMuonSelectionTool(Long_t size, void *p);
   static void delete_CPcLcLMuonSelectionTool(void *p);
   static void deleteArray_CPcLcLMuonSelectionTool(void *p);
   static void destruct_CPcLcLMuonSelectionTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::MuonSelectionTool*)
   {
      ::CP::MuonSelectionTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CP::MuonSelectionTool));
      static ::ROOT::TGenericClassInfo 
         instance("CP::MuonSelectionTool", "MuonSelectorTools/MuonSelectionTool.h", 25,
                  typeid(::CP::MuonSelectionTool), DefineBehavior(ptr, ptr),
                  &CPcLcLMuonSelectionTool_Dictionary, isa_proxy, 0,
                  sizeof(::CP::MuonSelectionTool) );
      instance.SetNew(&new_CPcLcLMuonSelectionTool);
      instance.SetNewArray(&newArray_CPcLcLMuonSelectionTool);
      instance.SetDelete(&delete_CPcLcLMuonSelectionTool);
      instance.SetDeleteArray(&deleteArray_CPcLcLMuonSelectionTool);
      instance.SetDestructor(&destruct_CPcLcLMuonSelectionTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::MuonSelectionTool*)
   {
      return GenerateInitInstanceLocal((::CP::MuonSelectionTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CP::MuonSelectionTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CPcLcLMuonSelectionTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CP::MuonSelectionTool*)0x0)->GetClass();
      CPcLcLMuonSelectionTool_TClassManip(theClass);
   return theClass;
   }

   static void CPcLcLMuonSelectionTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CPcLcLMuonLegacySelectionTool_Dictionary();
   static void CPcLcLMuonLegacySelectionTool_TClassManip(TClass*);
   static void *new_CPcLcLMuonLegacySelectionTool(void *p = 0);
   static void *newArray_CPcLcLMuonLegacySelectionTool(Long_t size, void *p);
   static void delete_CPcLcLMuonLegacySelectionTool(void *p);
   static void deleteArray_CPcLcLMuonLegacySelectionTool(void *p);
   static void destruct_CPcLcLMuonLegacySelectionTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::MuonLegacySelectionTool*)
   {
      ::CP::MuonLegacySelectionTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CP::MuonLegacySelectionTool));
      static ::ROOT::TGenericClassInfo 
         instance("CP::MuonLegacySelectionTool", "MuonSelectorTools/MuonLegacySelectionTool.h", 18,
                  typeid(::CP::MuonLegacySelectionTool), DefineBehavior(ptr, ptr),
                  &CPcLcLMuonLegacySelectionTool_Dictionary, isa_proxy, 0,
                  sizeof(::CP::MuonLegacySelectionTool) );
      instance.SetNew(&new_CPcLcLMuonLegacySelectionTool);
      instance.SetNewArray(&newArray_CPcLcLMuonLegacySelectionTool);
      instance.SetDelete(&delete_CPcLcLMuonLegacySelectionTool);
      instance.SetDeleteArray(&deleteArray_CPcLcLMuonLegacySelectionTool);
      instance.SetDestructor(&destruct_CPcLcLMuonLegacySelectionTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::MuonLegacySelectionTool*)
   {
      return GenerateInitInstanceLocal((::CP::MuonLegacySelectionTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CP::MuonLegacySelectionTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CPcLcLMuonLegacySelectionTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CP::MuonLegacySelectionTool*)0x0)->GetClass();
      CPcLcLMuonLegacySelectionTool_TClassManip(theClass);
   return theClass;
   }

   static void CPcLcLMuonLegacySelectionTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_CPcLcLMuonSelectionTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::CP::MuonSelectionTool : new ::CP::MuonSelectionTool;
   }
   static void *newArray_CPcLcLMuonSelectionTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::CP::MuonSelectionTool[nElements] : new ::CP::MuonSelectionTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_CPcLcLMuonSelectionTool(void *p) {
      delete ((::CP::MuonSelectionTool*)p);
   }
   static void deleteArray_CPcLcLMuonSelectionTool(void *p) {
      delete [] ((::CP::MuonSelectionTool*)p);
   }
   static void destruct_CPcLcLMuonSelectionTool(void *p) {
      typedef ::CP::MuonSelectionTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CP::MuonSelectionTool

namespace ROOT {
   // Wrappers around operator new
   static void *new_CPcLcLMuonLegacySelectionTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::CP::MuonLegacySelectionTool : new ::CP::MuonLegacySelectionTool;
   }
   static void *newArray_CPcLcLMuonLegacySelectionTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::CP::MuonLegacySelectionTool[nElements] : new ::CP::MuonLegacySelectionTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_CPcLcLMuonLegacySelectionTool(void *p) {
      delete ((::CP::MuonLegacySelectionTool*)p);
   }
   static void deleteArray_CPcLcLMuonLegacySelectionTool(void *p) {
      delete [] ((::CP::MuonLegacySelectionTool*)p);
   }
   static void destruct_CPcLcLMuonLegacySelectionTool(void *p) {
      typedef ::CP::MuonLegacySelectionTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CP::MuonLegacySelectionTool

namespace {
  void TriggerDictionaryInitialization_MuonSelectorTools_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonSelectorTools/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonSelectorTools/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace CP{class __attribute__((annotate("$clingAutoload$MuonSelectorTools/MuonSelectionTool.h")))  MuonSelectionTool;}
namespace CP{class __attribute__((annotate("$clingAutoload$MuonSelectorTools/MuonLegacySelectionTool.h")))  MuonLegacySelectionTool;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "MuonSelectorTools"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef MUONSELECTORTOOLS_MUONSELECTORTOOLSDICT_H
#define MUONSELECTORTOOLS_MUONSELECTORTOOLSDICT_H

#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// Includes for the dictionary generation:
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonSelectorTools/MuonLegacySelectionTool.h"

#endif // MUONSELECTORTOOLS_MUONSELECTORTOOLSDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"CP::MuonLegacySelectionTool", payloadCode, "@",
"CP::MuonSelectionTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("MuonSelectorTools_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_MuonSelectorTools_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_MuonSelectorTools_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_MuonSelectorTools_Reflex() {
  TriggerDictionaryInitialization_MuonSelectorTools_Reflex_Impl();
}

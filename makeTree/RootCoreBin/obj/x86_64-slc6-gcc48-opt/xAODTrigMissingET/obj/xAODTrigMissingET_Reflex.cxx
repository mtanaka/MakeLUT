// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrigMissingETdIobjdIxAODTrigMissingET_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigMissingET/xAODTrigMissingET/xAODTrigMissingETDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTrigMissingET_v1_Dictionary();
   static void xAODcLcLTrigMissingET_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigMissingET_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigMissingET_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigMissingET_v1(void *p);
   static void deleteArray_xAODcLcLTrigMissingET_v1(void *p);
   static void destruct_xAODcLcLTrigMissingET_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigMissingET_v1*)
   {
      ::xAOD::TrigMissingET_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigMissingET_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigMissingET_v1", "xAODTrigMissingET/versions/TrigMissingET_v1.h", 27,
                  typeid(::xAOD::TrigMissingET_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigMissingET_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigMissingET_v1) );
      instance.SetNew(&new_xAODcLcLTrigMissingET_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigMissingET_v1);
      instance.SetDelete(&delete_xAODcLcLTrigMissingET_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigMissingET_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigMissingET_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigMissingET_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigMissingET_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigMissingET_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigMissingET_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigMissingET_v1*)0x0)->GetClass();
      xAODcLcLTrigMissingET_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigMissingET_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigMissingET_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigMissingET_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigMissingET_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigMissingET_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigMissingET_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigMissingET_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigMissingET_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigMissingET_v1>*)
   {
      ::DataVector<xAOD::TrigMissingET_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigMissingET_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigMissingET_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigMissingET_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigMissingET_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigMissingET_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigMissingET_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigMissingET_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigMissingET_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigMissingET_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigMissingET_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigMissingET_v1>","xAOD::TrigMissingETContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigMissingET_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigMissingET_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigMissingET_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigMissingET_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigMissingET_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigMissingET_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigMissingET_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","3EA0678D-5DCA-4BA8-BB29-775CB9A246AC");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigMissingETAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigMissingETAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigMissingETAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigMissingETAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigMissingETAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigMissingETAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigMissingETAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigMissingETAuxContainer_v1*)
   {
      ::xAOD::TrigMissingETAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigMissingETAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigMissingETAuxContainer_v1", "xAODTrigMissingET/versions/TrigMissingETAuxContainer_v1.h", 27,
                  typeid(::xAOD::TrigMissingETAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigMissingETAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigMissingETAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigMissingETAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigMissingETAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigMissingETAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigMissingETAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigMissingETAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigMissingETAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigMissingETAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigMissingETAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigMissingETAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigMissingETAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigMissingETAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigMissingETAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E8C1613E-5E47-4B7F-8ED7-B30A8FE21DB4");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TrigMissingET_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TrigMissingET_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TrigMissingET_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TrigMissingET_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TrigMissingET_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TrigMissingET_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TrigMissingET_v1> >","DataLink<xAOD::TrigMissingETContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TrigMissingET_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TrigMissingET_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrigMissingET_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrigMissingET_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrigMissingET_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrigMissingET_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrigMissingET_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrigMissingET_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrigMissingET_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrigMissingET_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrigMissingET_v1> >","ElementLink<xAOD::TrigMissingETContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrigMissingET_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrigMissingET_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigMissingET_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigMissingET_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigMissingET_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigMissingET_v1 : new ::xAOD::TrigMissingET_v1;
   }
   static void *newArray_xAODcLcLTrigMissingET_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigMissingET_v1[nElements] : new ::xAOD::TrigMissingET_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigMissingET_v1(void *p) {
      delete ((::xAOD::TrigMissingET_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigMissingET_v1(void *p) {
      delete [] ((::xAOD::TrigMissingET_v1*)p);
   }
   static void destruct_xAODcLcLTrigMissingET_v1(void *p) {
      typedef ::xAOD::TrigMissingET_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigMissingET_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigMissingET_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigMissingET_v1> : new ::DataVector<xAOD::TrigMissingET_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigMissingET_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigMissingET_v1>[nElements] : new ::DataVector<xAOD::TrigMissingET_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigMissingET_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigMissingET_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigMissingET_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigMissingET_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigMissingET_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigMissingET_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigMissingET_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigMissingETAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigMissingETAuxContainer_v1 : new ::xAOD::TrigMissingETAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigMissingETAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigMissingETAuxContainer_v1[nElements] : new ::xAOD::TrigMissingETAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigMissingETAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigMissingETAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigMissingETAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigMissingETAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigMissingETAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigMissingETAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigMissingETAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TrigMissingET_v1> > : new ::DataLink<DataVector<xAOD::TrigMissingET_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TrigMissingET_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TrigMissingET_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TrigMissingET_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TrigMissingET_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TrigMissingET_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TrigMissingET_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrigMissingET_v1> > : new ::ElementLink<DataVector<xAOD::TrigMissingET_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrigMissingET_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrigMissingET_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrigMissingET_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrigMissingET_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrigMissingET_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrigMissingET_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > : new vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > > : new vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigMissingET_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTrigMissingET_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigMissingET/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigMissingET",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigMissingET/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMissingET/TrigMissingET.h")))  TrigMissingET_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@E8C1613E-5E47-4B7F-8ED7-B30A8FE21DB4)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMissingET/versions/TrigMissingETAuxContainer_v1.h")))  TrigMissingETAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigMissingET"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTrigMissingETDict.h 630930 2014-11-25 14:57:49Z gwatts $
#ifndef xAODTrigMissingET_xAODTrigMissingET_DICT_H
#define xAODTrigMissingET_xAODTrigMissingET_DICT_H

// Include all headers here that need to have dictionaries
// generated for them.

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODTrigMissingET/TrigMissingET.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigMissingET/versions/TrigMissingET_v1.h"
#include "xAODTrigMissingET/versions/TrigMissingETContainer_v1.h"
#include "xAODTrigMissingET/versions/TrigMissingETAuxContainer_v1.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODTRIGMISSINGET {
      xAOD::TrigMissingETContainer_v1 c1;
      DataLink< xAOD::TrigMissingETContainer_v1 > dl1;
      std::vector< DataLink< xAOD::TrigMissingETContainer_v1 > > dl2;
      ElementLink< xAOD::TrigMissingETContainer_v1 > el1;
      std::vector< ElementLink< xAOD::TrigMissingETContainer_v1 > > el2;
      std::vector< std::vector< ElementLink< xAOD::TrigMissingETContainer_v1 > > > el3;
   };
} // private namespace

#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::TrigMissingET_v1> >", payloadCode, "@",
"DataLink<xAOD::TrigMissingETContainer_v1>", payloadCode, "@",
"DataVector<xAOD::TrigMissingET_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrigMissingET_v1> >", payloadCode, "@",
"ElementLink<xAOD::TrigMissingETContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TrigMissingET_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::TrigMissingETContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::TrigMissingETContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrigMissingETContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrigMissingET_v1> > > >", payloadCode, "@",
"xAOD::TrigMissingETAuxContainer_v1", payloadCode, "@",
"xAOD::TrigMissingETContainer_v1", payloadCode, "@",
"xAOD::TrigMissingET_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigMissingET_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigMissingET_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigMissingET_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigMissingET_Reflex() {
  TriggerDictionaryInitialization_xAODTrigMissingET_Reflex_Impl();
}

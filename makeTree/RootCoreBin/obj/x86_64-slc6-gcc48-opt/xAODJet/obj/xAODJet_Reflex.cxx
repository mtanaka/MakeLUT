// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODJetdIobjdIxAODJet_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODJet/xAODJet/xAODJetDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLJet_v1_Dictionary();
   static void xAODcLcLJet_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJet_v1(void *p = 0);
   static void *newArray_xAODcLcLJet_v1(Long_t size, void *p);
   static void delete_xAODcLcLJet_v1(void *p);
   static void deleteArray_xAODcLcLJet_v1(void *p);
   static void destruct_xAODcLcLJet_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLJet_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::Jet_v1");
      static Long_t offset_m_pxpypzeCached = cls->GetDataMemberOffset("m_pxpypzeCached");
      bool& m_pxpypzeCached = *(bool*)(target+offset_m_pxpypzeCached);
      xAOD::Jet_v1* newObj = (xAOD::Jet_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_pxpypzeCached = false;
       newObj->reset();
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::Jet_v1*)
   {
      ::xAOD::Jet_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::Jet_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::Jet_v1", "xAODJet/versions/Jet_v1.h", 52,
                  typeid(::xAOD::Jet_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJet_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::Jet_v1) );
      instance.SetNew(&new_xAODcLcLJet_v1);
      instance.SetNewArray(&newArray_xAODcLcLJet_v1);
      instance.SetDelete(&delete_xAODcLcLJet_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJet_v1);
      instance.SetDestructor(&destruct_xAODcLcLJet_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::Jet_v1";
      rule->fTarget      = "m_pxpypzeCached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLJet_v1_0);
      rule->fCode        = "\n       m_pxpypzeCached = false;\n       newObj->reset();\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::Jet_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::Jet_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::Jet_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJet_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::Jet_v1*)0x0)->GetClass();
      xAODcLcLJet_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJet_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLJet_v1gR_Dictionary();
   static void DataVectorlExAODcLcLJet_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLJet_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLJet_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLJet_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLJet_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLJet_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::Jet_v1>*)
   {
      ::DataVector<xAOD::Jet_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::Jet_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::Jet_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::Jet_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLJet_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::Jet_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLJet_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLJet_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLJet_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLJet_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLJet_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::Jet_v1>","xAOD::JetContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::Jet_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::Jet_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::Jet_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLJet_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::Jet_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLJet_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLJet_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","646342DF-D1E1-422A-975D-EA6DBF13CB6D");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetAuxContainer_v1_Dictionary();
   static void xAODcLcLJetAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJetAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLJetAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLJetAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLJetAuxContainer_v1(void *p);
   static void destruct_xAODcLcLJetAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetAuxContainer_v1*)
   {
      ::xAOD::JetAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetAuxContainer_v1", "xAODJet/versions/JetAuxContainer_v1.h", 32,
                  typeid(::xAOD::JetAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLJetAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLJetAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLJetAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLJetAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLJetAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","17E48A73-2219-44A0-8913-D59DB61F4B15");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLJetTrigAuxContainer_v1_Dictionary();
   static void xAODcLcLJetTrigAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLJetTrigAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLJetTrigAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLJetTrigAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLJetTrigAuxContainer_v1(void *p);
   static void destruct_xAODcLcLJetTrigAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::JetTrigAuxContainer_v1*)
   {
      ::xAOD::JetTrigAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::JetTrigAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::JetTrigAuxContainer_v1", "xAODJet/versions/JetTrigAuxContainer_v1.h", 32,
                  typeid(::xAOD::JetTrigAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLJetTrigAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::JetTrigAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLJetTrigAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLJetTrigAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLJetTrigAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLJetTrigAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLJetTrigAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::JetTrigAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::JetTrigAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::JetTrigAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLJetTrigAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::JetTrigAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLJetTrigAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLJetTrigAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","89AE2C6B-A862-499C-8BDA-11D24FAC83F1");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::Jet_v1> >*)
   {
      ::DataLink<DataVector<xAOD::Jet_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::Jet_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::Jet_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::Jet_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::Jet_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::Jet_v1> >","DataLink<xAOD::JetContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::Jet_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::Jet_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Jet_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::Jet_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::Jet_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::Jet_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::Jet_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::Jet_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::Jet_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::Jet_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::Jet_v1> >","ElementLink<xAOD::JetContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::Jet_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::Jet_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Jet_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::Jet_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary();
   static void ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLinkVector<DataVector<xAOD::Jet_v1> >*)
   {
      ::ElementLinkVector<DataVector<xAOD::Jet_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLinkVector<DataVector<xAOD::Jet_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLinkVector<DataVector<xAOD::Jet_v1> >", "AthLinks/ElementLinkVector.h", 34,
                  typeid(::ElementLinkVector<DataVector<xAOD::Jet_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLinkVector<DataVector<xAOD::Jet_v1> >) );
      instance.SetNew(&new_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLinkVector<DataVector<xAOD::Jet_v1> >","ElementLinkVector<xAOD::JetContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLinkVector<DataVector<xAOD::Jet_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLinkVector<DataVector<xAOD::Jet_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Jet_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLinkVector<DataVector<xAOD::Jet_v1> >*)0x0)->GetClass();
      ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR_Dictionary();
   static void pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR_TClassManip(TClass*);
   static void *new_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(void *p = 0);
   static void *newArray_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(Long_t size, void *p);
   static void delete_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(void *p);
   static void deleteArray_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(void *p);
   static void destruct_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<string,vector<const xAOD::IParticle*> >*)
   {
      pair<string,vector<const xAOD::IParticle*> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<string,vector<const xAOD::IParticle*> >));
      static ::ROOT::TGenericClassInfo 
         instance("pair<string,vector<const xAOD::IParticle*> >", "string", 96,
                  typeid(pair<string,vector<const xAOD::IParticle*> >), DefineBehavior(ptr, ptr),
                  &pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(pair<string,vector<const xAOD::IParticle*> >) );
      instance.SetNew(&new_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR);
      instance.SetNewArray(&newArray_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR);
      instance.SetDelete(&delete_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR);
      instance.SetDeleteArray(&deleteArray_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR);
      instance.SetDestructor(&destruct_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR);

      ROOT::AddClassAlternate("pair<string,vector<const xAOD::IParticle*> >","pair<std::string,std::vector<const xAOD::IParticle*> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<string,vector<const xAOD::IParticle*> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<string,vector<const xAOD::IParticle*> >*)0x0)->GetClass();
      pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEstringcOvectorlEfloatgRsPgR_Dictionary();
   static void pairlEstringcOvectorlEfloatgRsPgR_TClassManip(TClass*);
   static void *new_pairlEstringcOvectorlEfloatgRsPgR(void *p = 0);
   static void *newArray_pairlEstringcOvectorlEfloatgRsPgR(Long_t size, void *p);
   static void delete_pairlEstringcOvectorlEfloatgRsPgR(void *p);
   static void deleteArray_pairlEstringcOvectorlEfloatgRsPgR(void *p);
   static void destruct_pairlEstringcOvectorlEfloatgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<string,vector<float> >*)
   {
      pair<string,vector<float> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<string,vector<float> >));
      static ::ROOT::TGenericClassInfo 
         instance("pair<string,vector<float> >", "string", 96,
                  typeid(pair<string,vector<float> >), DefineBehavior(ptr, ptr),
                  &pairlEstringcOvectorlEfloatgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(pair<string,vector<float> >) );
      instance.SetNew(&new_pairlEstringcOvectorlEfloatgRsPgR);
      instance.SetNewArray(&newArray_pairlEstringcOvectorlEfloatgRsPgR);
      instance.SetDelete(&delete_pairlEstringcOvectorlEfloatgRsPgR);
      instance.SetDeleteArray(&deleteArray_pairlEstringcOvectorlEfloatgRsPgR);
      instance.SetDestructor(&destruct_pairlEstringcOvectorlEfloatgRsPgR);

      ROOT::AddClassAlternate("pair<string,vector<float> >","pair<std::string,std::vector<float> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<string,vector<float> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEstringcOvectorlEfloatgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<string,vector<float> >*)0x0)->GetClass();
      pairlEstringcOvectorlEfloatgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEstringcOvectorlEfloatgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary();
   static void pairlEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(TClass*);
   static void *new_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p = 0);
   static void *newArray_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(Long_t size, void *p);
   static void delete_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);
   static void deleteArray_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);
   static void destruct_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<string,vector<vector<int> > >*)
   {
      pair<string,vector<vector<int> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<string,vector<vector<int> > >));
      static ::ROOT::TGenericClassInfo 
         instance("pair<string,vector<vector<int> > >", "string", 96,
                  typeid(pair<string,vector<vector<int> > >), DefineBehavior(ptr, ptr),
                  &pairlEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(pair<string,vector<vector<int> > >) );
      instance.SetNew(&new_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetNewArray(&newArray_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDelete(&delete_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDestructor(&destruct_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR);

      ROOT::AddClassAlternate("pair<string,vector<vector<int> > >","pair<std::string,std::vector<std::vector<int> > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<string,vector<vector<int> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<string,vector<vector<int> > >*)0x0)->GetClass();
      pairlEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR_Dictionary();
   static void pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(TClass*);
   static void *new_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p = 0);
   static void *newArray_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(Long_t size, void *p);
   static void delete_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p);
   static void deleteArray_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p);
   static void destruct_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<string,vector<vector<float> > >*)
   {
      pair<string,vector<vector<float> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<string,vector<vector<float> > >));
      static ::ROOT::TGenericClassInfo 
         instance("pair<string,vector<vector<float> > >", "string", 96,
                  typeid(pair<string,vector<vector<float> > >), DefineBehavior(ptr, ptr),
                  &pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(pair<string,vector<vector<float> > >) );
      instance.SetNew(&new_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetNewArray(&newArray_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDelete(&delete_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR);
      instance.SetDestructor(&destruct_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR);

      ROOT::AddClassAlternate("pair<string,vector<vector<float> > >","pair<std::string,std::vector<std::vector<float> > >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<string,vector<vector<float> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<string,vector<vector<float> > >*)0x0)->GetClass();
      pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJet_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Jet_v1 : new ::xAOD::Jet_v1;
   }
   static void *newArray_xAODcLcLJet_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::Jet_v1[nElements] : new ::xAOD::Jet_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJet_v1(void *p) {
      delete ((::xAOD::Jet_v1*)p);
   }
   static void deleteArray_xAODcLcLJet_v1(void *p) {
      delete [] ((::xAOD::Jet_v1*)p);
   }
   static void destruct_xAODcLcLJet_v1(void *p) {
      typedef ::xAOD::Jet_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::Jet_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLJet_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::Jet_v1> : new ::DataVector<xAOD::Jet_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLJet_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::Jet_v1>[nElements] : new ::DataVector<xAOD::Jet_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLJet_v1gR(void *p) {
      delete ((::DataVector<xAOD::Jet_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLJet_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::Jet_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLJet_v1gR(void *p) {
      typedef ::DataVector<xAOD::Jet_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::Jet_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetAuxContainer_v1 : new ::xAOD::JetAuxContainer_v1;
   }
   static void *newArray_xAODcLcLJetAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetAuxContainer_v1[nElements] : new ::xAOD::JetAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetAuxContainer_v1(void *p) {
      delete ((::xAOD::JetAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLJetAuxContainer_v1(void *p) {
      delete [] ((::xAOD::JetAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLJetAuxContainer_v1(void *p) {
      typedef ::xAOD::JetAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLJetTrigAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetTrigAuxContainer_v1 : new ::xAOD::JetTrigAuxContainer_v1;
   }
   static void *newArray_xAODcLcLJetTrigAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::JetTrigAuxContainer_v1[nElements] : new ::xAOD::JetTrigAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLJetTrigAuxContainer_v1(void *p) {
      delete ((::xAOD::JetTrigAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLJetTrigAuxContainer_v1(void *p) {
      delete [] ((::xAOD::JetTrigAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLJetTrigAuxContainer_v1(void *p) {
      typedef ::xAOD::JetTrigAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::JetTrigAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::Jet_v1> > : new ::DataLink<DataVector<xAOD::Jet_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::Jet_v1> >[nElements] : new ::DataLink<DataVector<xAOD::Jet_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::Jet_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::Jet_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::Jet_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::Jet_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::Jet_v1> > : new ::ElementLink<DataVector<xAOD::Jet_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::Jet_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::Jet_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::Jet_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::Jet_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::Jet_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::Jet_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLinkVector<DataVector<xAOD::Jet_v1> > : new ::ElementLinkVector<DataVector<xAOD::Jet_v1> >;
   }
   static void *newArray_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLinkVector<DataVector<xAOD::Jet_v1> >[nElements] : new ::ElementLinkVector<DataVector<xAOD::Jet_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      delete ((::ElementLinkVector<DataVector<xAOD::Jet_v1> >*)p);
   }
   static void deleteArray_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      delete [] ((::ElementLinkVector<DataVector<xAOD::Jet_v1> >*)p);
   }
   static void destruct_ElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgR(void *p) {
      typedef ::ElementLinkVector<DataVector<xAOD::Jet_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLinkVector<DataVector<xAOD::Jet_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,vector<const xAOD::IParticle*> > : new pair<string,vector<const xAOD::IParticle*> >;
   }
   static void *newArray_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,vector<const xAOD::IParticle*> >[nElements] : new pair<string,vector<const xAOD::IParticle*> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(void *p) {
      delete ((pair<string,vector<const xAOD::IParticle*> >*)p);
   }
   static void deleteArray_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(void *p) {
      delete [] ((pair<string,vector<const xAOD::IParticle*> >*)p);
   }
   static void destruct_pairlEstringcOvectorlEconstsPxAODcLcLIParticlemUgRsPgR(void *p) {
      typedef pair<string,vector<const xAOD::IParticle*> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<string,vector<const xAOD::IParticle*> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEstringcOvectorlEfloatgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,vector<float> > : new pair<string,vector<float> >;
   }
   static void *newArray_pairlEstringcOvectorlEfloatgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,vector<float> >[nElements] : new pair<string,vector<float> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEstringcOvectorlEfloatgRsPgR(void *p) {
      delete ((pair<string,vector<float> >*)p);
   }
   static void deleteArray_pairlEstringcOvectorlEfloatgRsPgR(void *p) {
      delete [] ((pair<string,vector<float> >*)p);
   }
   static void destruct_pairlEstringcOvectorlEfloatgRsPgR(void *p) {
      typedef pair<string,vector<float> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<string,vector<float> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,vector<vector<int> > > : new pair<string,vector<vector<int> > >;
   }
   static void *newArray_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,vector<vector<int> > >[nElements] : new pair<string,vector<vector<int> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      delete ((pair<string,vector<vector<int> > >*)p);
   }
   static void deleteArray_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      delete [] ((pair<string,vector<vector<int> > >*)p);
   }
   static void destruct_pairlEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      typedef pair<string,vector<vector<int> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<string,vector<vector<int> > >

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,vector<vector<float> > > : new pair<string,vector<vector<float> > >;
   }
   static void *newArray_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<string,vector<vector<float> > >[nElements] : new pair<string,vector<vector<float> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      delete ((pair<string,vector<vector<float> > >*)p);
   }
   static void deleteArray_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      delete [] ((pair<string,vector<vector<float> > >*)p);
   }
   static void destruct_pairlEstringcOvectorlEvectorlEfloatgRsPgRsPgR(void *p) {
      typedef pair<string,vector<vector<float> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<string,vector<vector<float> > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >*)
   {
      vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >*)0x0)->GetClass();
      vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > > : new vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >;
   }
   static void *newArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >[nElements] : new vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinkVectorlEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::Jet_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::Jet_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::Jet_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::Jet_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::Jet_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::Jet_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::Jet_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Jet_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::Jet_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Jet_v1> > > : new vector<ElementLink<DataVector<xAOD::Jet_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::Jet_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::Jet_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::Jet_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::Jet_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::Jet_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::Jet_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::Jet_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::Jet_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::Jet_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::Jet_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::Jet_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::Jet_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::Jet_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Jet_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::Jet_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Jet_v1> > > : new vector<DataLink<DataVector<xAOD::Jet_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::Jet_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::Jet_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::Jet_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::Jet_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLJet_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::Jet_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::Jet_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODJet_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODJet/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODJet",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODJet/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODJet/JetContainer.h")))  Jet_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODJet/JetContainer.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@17E48A73-2219-44A0-8913-D59DB61F4B15)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODJet/versions/JetAuxContainer_v1.h")))  JetAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@89AE2C6B-A862-499C-8BDA-11D24FAC83F1)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODJet/versions/JetTrigAuxContainer_v1.h")))  JetTrigAuxContainer_v1;}
namespace std{template <class _CharT> struct __attribute__((annotate("$clingAutoload$string")))  char_traits;
}
namespace std{template <typename > class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODJet"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODJetDict.h 631914 2014-11-28 16:08:50Z krasznaa $
#ifndef XAODJET_XAODJETDICT_H
#define XAODJET_XAODJETDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
#include "xAODJet/JetContainer.h"
#include "xAODJet/versions/JetContainer_v1.h"
#include "xAODJet/versions/JetAuxContainer_v1.h"
#include "xAODJet/versions/JetTrigAuxContainer_v1.h"
#include "xAODJet/JetTypes.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODJET {
      // Instantiate the container for gccxml:
      xAOD::JetContainer_v1 c1;

      // Smart pointers to Jet_v1:
      DataLink< xAOD::JetContainer_v1 > e1;
      std::vector< DataLink< xAOD::JetContainer_v1 > > e2;
      ElementLink< xAOD::JetContainer_v1 > e3;
      std::vector< ElementLink< xAOD::JetContainer_v1 > > e4;
      std::vector< std::vector< ElementLink< xAOD::JetContainer_v1 > > > e5;
      ElementLinkVector< xAOD::JetContainer_v1 > e6;
      std::vector< ElementLinkVector< xAOD::JetContainer_v1 > > e7;

      // Instantiations to create the EL member dictionaries correctly:
      xAOD::IParticleContainer pc1;
      ElementLink<xAOD::IParticleContainer> e10;

     std::pair<std::string,std::vector<const xAOD::IParticle*> > bug1;
     std::pair<std::string,std::vector<float> > bug2;
     std::pair<std::string,std::vector<std::vector<int> > > bug3;
     std::pair<std::string,std::vector<std::vector<float> > > bug4;

     // momentums     
     //xAOD::JetFourMom_t jet4mom;
     std::vector<xAOD::JetFourMom_t> vjet4mom;
   };
}

#endif // XAODJET_XAODJETDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::Jet_v1> >", payloadCode, "@",
"DataLink<xAOD::JetContainer_v1>", payloadCode, "@",
"DataVector<xAOD::Jet_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::Jet_v1> >", payloadCode, "@",
"ElementLink<xAOD::JetContainer_v1>", payloadCode, "@",
"ElementLinkVector<DataVector<xAOD::Jet_v1> >", payloadCode, "@",
"ElementLinkVector<xAOD::JetContainer_v1>", payloadCode, "@",
"pair<std::string,std::vector<const xAOD::IParticle*> >", payloadCode, "@",
"pair<std::string,std::vector<float> >", payloadCode, "@",
"pair<std::string,std::vector<std::vector<float> > >", payloadCode, "@",
"pair<std::string,std::vector<std::vector<int> > >", payloadCode, "@",
"pair<string,vector<const xAOD::IParticle*> >", payloadCode, "@",
"pair<string,vector<float> >", payloadCode, "@",
"pair<string,vector<vector<float> > >", payloadCode, "@",
"pair<string,vector<vector<int> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::Jet_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::JetContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::Jet_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::JetContainer_v1> >", payloadCode, "@",
"vector<ElementLinkVector<DataVector<xAOD::Jet_v1> > >", payloadCode, "@",
"vector<ElementLinkVector<xAOD::JetContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::JetContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::Jet_v1> > > >", payloadCode, "@",
"xAOD::JetAuxContainer_v1", payloadCode, "@",
"xAOD::JetContainer_v1", payloadCode, "@",
"xAOD::JetTrigAuxContainer_v1", payloadCode, "@",
"xAOD::Jet_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODJet_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODJet_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODJet_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODJet_Reflex() {
  TriggerDictionaryInitialization_xAODJet_Reflex_Impl();
}

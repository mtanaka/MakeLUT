// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdImakeTreedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODCaloEventdIobjdIxAODCaloEvent_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCaloEvent/xAODCaloEvent/xAODCaloEventDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLCaloCluster_v1_Dictionary();
   static void xAODcLcLCaloCluster_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCaloCluster_v1(void *p = 0);
   static void *newArray_xAODcLcLCaloCluster_v1(Long_t size, void *p);
   static void delete_xAODcLcLCaloCluster_v1(void *p);
   static void deleteArray_xAODcLcLCaloCluster_v1(void *p);
   static void destruct_xAODcLcLCaloCluster_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLCaloCluster_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::CaloCluster_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bitset<xAOD::CaloCluster_v1::NSTATES>& m_p4Cached = *(bitset<xAOD::CaloCluster_v1::NSTATES>*)(target+offset_m_p4Cached);
      xAOD::CaloCluster_v1* newObj = (xAOD::CaloCluster_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached.reset();
    
   }
   static void read_xAODcLcLCaloCluster_v1_1( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::CaloCluster_v1");
      static Long_t offset_m_ptCached = cls->GetDataMemberOffset("m_ptCached");
      bitset<xAOD::CaloCluster_v1::NSTATES>& m_ptCached = *(bitset<xAOD::CaloCluster_v1::NSTATES>*)(target+offset_m_ptCached);
      xAOD::CaloCluster_v1* newObj = (xAOD::CaloCluster_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_ptCached.reset();
    
   }
   static void read_xAODcLcLCaloCluster_v1_2( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::CaloCluster_v1");
      static Long_t offset_m_signalState = cls->GetDataMemberOffset("m_signalState");
      xAOD::CaloCluster_v1::State& m_signalState = *(xAOD::CaloCluster_v1::State*)(target+offset_m_signalState);
      xAOD::CaloCluster_v1* newObj = (xAOD::CaloCluster_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_signalState=xAOD::CaloCluster_v1::CALIBRATED;
    
   }
   static void read_xAODcLcLCaloCluster_v1_3( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::CaloCluster_v1");
      static Long_t offset_m_cellLinks = cls->GetDataMemberOffset("m_cellLinks");
      CaloClusterCellLink*& m_cellLinks = *(CaloClusterCellLink**)(target+offset_m_cellLinks);
      xAOD::CaloCluster_v1* newObj = (xAOD::CaloCluster_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_cellLinks=0;
    
   }
   static void read_xAODcLcLCaloCluster_v1_4( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::CaloCluster_v1");
      static Long_t offset_m_ownCellLinks = cls->GetDataMemberOffset("m_ownCellLinks");
      bool& m_ownCellLinks = *(bool*)(target+offset_m_ownCellLinks);
      xAOD::CaloCluster_v1* newObj = (xAOD::CaloCluster_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_ownCellLinks=false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloCluster_v1*)
   {
      ::xAOD::CaloCluster_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloCluster_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloCluster_v1", "xAODCaloEvent/versions/CaloCluster_v1.h", 43,
                  typeid(::xAOD::CaloCluster_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloCluster_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloCluster_v1) );
      instance.SetNew(&new_xAODcLcLCaloCluster_v1);
      instance.SetNewArray(&newArray_xAODcLcLCaloCluster_v1);
      instance.SetDelete(&delete_xAODcLcLCaloCluster_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloCluster_v1);
      instance.SetDestructor(&destruct_xAODcLcLCaloCluster_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(5);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::CaloCluster_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLCaloCluster_v1_0);
      rule->fCode        = "\n       m_p4Cached.reset();\n    ";
      rule->fVersion     = "[1-]";
      rule = &readrules[1];
      rule->fSourceClass = "xAOD::CaloCluster_v1";
      rule->fTarget      = "m_ptCached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLCaloCluster_v1_1);
      rule->fCode        = "\n       m_ptCached.reset();\n    ";
      rule->fVersion     = "[1-]";
      rule = &readrules[2];
      rule->fSourceClass = "xAOD::CaloCluster_v1";
      rule->fTarget      = "m_signalState";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLCaloCluster_v1_2);
      rule->fCode        = "\n       m_signalState=xAOD::CaloCluster_v1::CALIBRATED;\n    ";
      rule->fVersion     = "[1-]";
      rule = &readrules[3];
      rule->fSourceClass = "xAOD::CaloCluster_v1";
      rule->fTarget      = "m_cellLinks";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLCaloCluster_v1_3);
      rule->fCode        = "\n       m_cellLinks=0;\n    ";
      rule->fVersion     = "[1-]";
      rule = &readrules[4];
      rule->fSourceClass = "xAOD::CaloCluster_v1";
      rule->fTarget      = "m_ownCellLinks";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLCaloCluster_v1_4);
      rule->fCode        = "\n       m_ownCellLinks=false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloCluster_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloCluster_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloCluster_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloCluster_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloCluster_v1*)0x0)->GetClass();
      xAODcLcLCaloCluster_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloCluster_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCaloCluster_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCaloCluster_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCaloCluster_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCaloCluster_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCaloCluster_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCaloCluster_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCaloCluster_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CaloCluster_v1>*)
   {
      ::DataVector<xAOD::CaloCluster_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CaloCluster_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CaloCluster_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::CaloCluster_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCaloCluster_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CaloCluster_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCaloCluster_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCaloCluster_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCaloCluster_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCaloCluster_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCaloCluster_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CaloCluster_v1>","xAOD::CaloClusterContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CaloCluster_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CaloCluster_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CaloCluster_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCaloCluster_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CaloCluster_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCaloCluster_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCaloCluster_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","24997BCA-3F6A-4530-8826-822EE9FC3C08");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCaloClusterAuxContainer_v1_Dictionary();
   static void xAODcLcLCaloClusterAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCaloClusterAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCaloClusterAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCaloClusterAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCaloClusterAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCaloClusterAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloClusterAuxContainer_v1*)
   {
      ::xAOD::CaloClusterAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloClusterAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloClusterAuxContainer_v1", "xAODCaloEvent/versions/CaloClusterAuxContainer_v1.h", 25,
                  typeid(::xAOD::CaloClusterAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloClusterAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloClusterAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCaloClusterAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCaloClusterAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCaloClusterAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloClusterAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCaloClusterAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloClusterAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloClusterAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloClusterAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloClusterAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloClusterAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCaloClusterAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloClusterAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","CE498B3B-A32D-43A3-B9B3-C13D136BACFC");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCaloClusterAuxContainer_v2_Dictionary();
   static void xAODcLcLCaloClusterAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLCaloClusterAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLCaloClusterAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLCaloClusterAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLCaloClusterAuxContainer_v2(void *p);
   static void destruct_xAODcLcLCaloClusterAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloClusterAuxContainer_v2*)
   {
      ::xAOD::CaloClusterAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloClusterAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloClusterAuxContainer_v2", "xAODCaloEvent/versions/CaloClusterAuxContainer_v2.h", 25,
                  typeid(::xAOD::CaloClusterAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloClusterAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloClusterAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLCaloClusterAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLCaloClusterAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLCaloClusterAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloClusterAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLCaloClusterAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloClusterAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloClusterAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloClusterAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloClusterAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloClusterAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLCaloClusterAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloClusterAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","451393B0-69CD-11E4-A739-02163E00A64D");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::CaloCluster_v1> >*)
   {
      ::DataLink<DataVector<xAOD::CaloCluster_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::CaloCluster_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::CaloCluster_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::CaloCluster_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::CaloCluster_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::CaloCluster_v1> >","DataLink<xAOD::CaloClusterContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::CaloCluster_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::CaloCluster_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::CaloCluster_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::CaloCluster_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::CaloCluster_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::CaloCluster_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::CaloCluster_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::CaloCluster_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::CaloCluster_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::CaloCluster_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::CaloCluster_v1> >","ElementLink<xAOD::CaloClusterContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::CaloCluster_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::CaloCluster_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::CaloCluster_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::CaloCluster_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCaloClusterBadChannelData_v1_Dictionary();
   static void xAODcLcLCaloClusterBadChannelData_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCaloClusterBadChannelData_v1(void *p = 0);
   static void *newArray_xAODcLcLCaloClusterBadChannelData_v1(Long_t size, void *p);
   static void delete_xAODcLcLCaloClusterBadChannelData_v1(void *p);
   static void deleteArray_xAODcLcLCaloClusterBadChannelData_v1(void *p);
   static void destruct_xAODcLcLCaloClusterBadChannelData_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloClusterBadChannelData_v1*)
   {
      ::xAOD::CaloClusterBadChannelData_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloClusterBadChannelData_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloClusterBadChannelData_v1", "xAODCaloEvent/versions/CaloClusterBadChannelData_v1.h", 11,
                  typeid(::xAOD::CaloClusterBadChannelData_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloClusterBadChannelData_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloClusterBadChannelData_v1) );
      instance.SetNew(&new_xAODcLcLCaloClusterBadChannelData_v1);
      instance.SetNewArray(&newArray_xAODcLcLCaloClusterBadChannelData_v1);
      instance.SetDelete(&delete_xAODcLcLCaloClusterBadChannelData_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloClusterBadChannelData_v1);
      instance.SetDestructor(&destruct_xAODcLcLCaloClusterBadChannelData_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloClusterBadChannelData_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloClusterBadChannelData_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloClusterBadChannelData_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloClusterBadChannelData_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloClusterBadChannelData_v1*)0x0)->GetClass();
      xAODcLcLCaloClusterBadChannelData_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloClusterBadChannelData_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCaloTower_v1_Dictionary();
   static void xAODcLcLCaloTower_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCaloTower_v1(void *p = 0);
   static void *newArray_xAODcLcLCaloTower_v1(Long_t size, void *p);
   static void delete_xAODcLcLCaloTower_v1(void *p);
   static void deleteArray_xAODcLcLCaloTower_v1(void *p);
   static void destruct_xAODcLcLCaloTower_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLCaloTower_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::CaloTower_v1");
      static Long_t offset_m_isComplete = cls->GetDataMemberOffset("m_isComplete");
      bool& m_isComplete = *(bool*)(target+offset_m_isComplete);
      xAOD::CaloTower_v1* newObj = (xAOD::CaloTower_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_isComplete=false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloTower_v1*)
   {
      ::xAOD::CaloTower_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloTower_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloTower_v1", "xAODCaloEvent/versions/CaloTower_v1.h", 9,
                  typeid(::xAOD::CaloTower_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloTower_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloTower_v1) );
      instance.SetNew(&new_xAODcLcLCaloTower_v1);
      instance.SetNewArray(&newArray_xAODcLcLCaloTower_v1);
      instance.SetDelete(&delete_xAODcLcLCaloTower_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloTower_v1);
      instance.SetDestructor(&destruct_xAODcLcLCaloTower_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::CaloTower_v1";
      rule->fTarget      = "m_isComplete";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLCaloTower_v1_0);
      rule->fCode        = "\n       m_isComplete=false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloTower_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloTower_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloTower_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloTower_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloTower_v1*)0x0)->GetClass();
      xAODcLcLCaloTower_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloTower_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCaloTowerContainer_v1_Dictionary();
   static void xAODcLcLCaloTowerContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCaloTowerContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCaloTowerContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCaloTowerContainer_v1(void *p);
   static void deleteArray_xAODcLcLCaloTowerContainer_v1(void *p);
   static void destruct_xAODcLcLCaloTowerContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloTowerContainer_v1*)
   {
      ::xAOD::CaloTowerContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloTowerContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloTowerContainer_v1", "xAODCaloEvent/versions/CaloTowerContainer_v1.h", 15,
                  typeid(::xAOD::CaloTowerContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloTowerContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloTowerContainer_v1) );
      instance.SetNew(&new_xAODcLcLCaloTowerContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCaloTowerContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCaloTowerContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloTowerContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCaloTowerContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloTowerContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloTowerContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloTowerContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloTowerContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloTowerContainer_v1*)0x0)->GetClass();
      xAODcLcLCaloTowerContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloTowerContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","EEA02A0F-98D3-464D-BAF1-1C944A700B8A");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCaloTowerAuxContainer_v1_Dictionary();
   static void xAODcLcLCaloTowerAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLCaloTowerAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLCaloTowerAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLCaloTowerAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLCaloTowerAuxContainer_v1(void *p);
   static void destruct_xAODcLcLCaloTowerAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloTowerAuxContainer_v1*)
   {
      ::xAOD::CaloTowerAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloTowerAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloTowerAuxContainer_v1", "xAODCaloEvent/versions/CaloTowerAuxContainer_v1.h", 11,
                  typeid(::xAOD::CaloTowerAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloTowerAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloTowerAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLCaloTowerAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLCaloTowerAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLCaloTowerAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloTowerAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLCaloTowerAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloTowerAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloTowerAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloTowerAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloTowerAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloTowerAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLCaloTowerAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloTowerAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","CE74E4D1-D2F4-4CED-8191-EC26D8836575");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLCaloTower_v1gR_Dictionary();
   static void DataVectorlExAODcLcLCaloTower_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLCaloTower_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLCaloTower_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLCaloTower_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLCaloTower_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLCaloTower_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::CaloTower_v1>*)
   {
      ::DataVector<xAOD::CaloTower_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::CaloTower_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::CaloTower_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::CaloTower_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLCaloTower_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::CaloTower_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLCaloTower_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLCaloTower_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLCaloTower_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLCaloTower_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLCaloTower_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::CaloTower_v1>","xAOD::CaloTowerContainerBase_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::CaloTower_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::CaloTower_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::CaloTower_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLCaloTower_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::CaloTower_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLCaloTower_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLCaloTower_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklExAODcLcLCaloTowerContainer_v1gR_Dictionary();
   static void DataLinklExAODcLcLCaloTowerContainer_v1gR_TClassManip(TClass*);
   static void *new_DataLinklExAODcLcLCaloTowerContainer_v1gR(void *p = 0);
   static void *newArray_DataLinklExAODcLcLCaloTowerContainer_v1gR(Long_t size, void *p);
   static void delete_DataLinklExAODcLcLCaloTowerContainer_v1gR(void *p);
   static void deleteArray_DataLinklExAODcLcLCaloTowerContainer_v1gR(void *p);
   static void destruct_DataLinklExAODcLcLCaloTowerContainer_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<xAOD::CaloTowerContainer_v1>*)
   {
      ::DataLink<xAOD::CaloTowerContainer_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<xAOD::CaloTowerContainer_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<xAOD::CaloTowerContainer_v1>", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<xAOD::CaloTowerContainer_v1>), DefineBehavior(ptr, ptr),
                  &DataLinklExAODcLcLCaloTowerContainer_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<xAOD::CaloTowerContainer_v1>) );
      instance.SetNew(&new_DataLinklExAODcLcLCaloTowerContainer_v1gR);
      instance.SetNewArray(&newArray_DataLinklExAODcLcLCaloTowerContainer_v1gR);
      instance.SetDelete(&delete_DataLinklExAODcLcLCaloTowerContainer_v1gR);
      instance.SetDeleteArray(&deleteArray_DataLinklExAODcLcLCaloTowerContainer_v1gR);
      instance.SetDestructor(&destruct_DataLinklExAODcLcLCaloTowerContainer_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<xAOD::CaloTowerContainer_v1>*)
   {
      return GenerateInitInstanceLocal((::DataLink<xAOD::CaloTowerContainer_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<xAOD::CaloTowerContainer_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklExAODcLcLCaloTowerContainer_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<xAOD::CaloTowerContainer_v1>*)0x0)->GetClass();
      DataLinklExAODcLcLCaloTowerContainer_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklExAODcLcLCaloTowerContainer_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklExAODcLcLCaloTowerContainer_v1gR_Dictionary();
   static void ElementLinklExAODcLcLCaloTowerContainer_v1gR_TClassManip(TClass*);
   static void *new_ElementLinklExAODcLcLCaloTowerContainer_v1gR(void *p = 0);
   static void *newArray_ElementLinklExAODcLcLCaloTowerContainer_v1gR(Long_t size, void *p);
   static void delete_ElementLinklExAODcLcLCaloTowerContainer_v1gR(void *p);
   static void deleteArray_ElementLinklExAODcLcLCaloTowerContainer_v1gR(void *p);
   static void destruct_ElementLinklExAODcLcLCaloTowerContainer_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<xAOD::CaloTowerContainer_v1>*)
   {
      ::ElementLink<xAOD::CaloTowerContainer_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<xAOD::CaloTowerContainer_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<xAOD::CaloTowerContainer_v1>", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<xAOD::CaloTowerContainer_v1>), DefineBehavior(ptr, ptr),
                  &ElementLinklExAODcLcLCaloTowerContainer_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<xAOD::CaloTowerContainer_v1>) );
      instance.SetNew(&new_ElementLinklExAODcLcLCaloTowerContainer_v1gR);
      instance.SetNewArray(&newArray_ElementLinklExAODcLcLCaloTowerContainer_v1gR);
      instance.SetDelete(&delete_ElementLinklExAODcLcLCaloTowerContainer_v1gR);
      instance.SetDeleteArray(&deleteArray_ElementLinklExAODcLcLCaloTowerContainer_v1gR);
      instance.SetDestructor(&destruct_ElementLinklExAODcLcLCaloTowerContainer_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<xAOD::CaloTowerContainer_v1>*)
   {
      return GenerateInitInstanceLocal((::ElementLink<xAOD::CaloTowerContainer_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<xAOD::CaloTowerContainer_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklExAODcLcLCaloTowerContainer_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<xAOD::CaloTowerContainer_v1>*)0x0)->GetClass();
      ElementLinklExAODcLcLCaloTowerContainer_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklExAODcLcLCaloTowerContainer_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCaloVertexedClusterBase_Dictionary();
   static void xAODcLcLCaloVertexedClusterBase_TClassManip(TClass*);
   static void delete_xAODcLcLCaloVertexedClusterBase(void *p);
   static void deleteArray_xAODcLcLCaloVertexedClusterBase(void *p);
   static void destruct_xAODcLcLCaloVertexedClusterBase(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloVertexedClusterBase*)
   {
      ::xAOD::CaloVertexedClusterBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloVertexedClusterBase));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloVertexedClusterBase", "xAODCaloEvent/CaloVertexedClusterBase.h", 35,
                  typeid(::xAOD::CaloVertexedClusterBase), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloVertexedClusterBase_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloVertexedClusterBase) );
      instance.SetDelete(&delete_xAODcLcLCaloVertexedClusterBase);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloVertexedClusterBase);
      instance.SetDestructor(&destruct_xAODcLcLCaloVertexedClusterBase);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloVertexedClusterBase*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloVertexedClusterBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloVertexedClusterBase*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloVertexedClusterBase_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloVertexedClusterBase*)0x0)->GetClass();
      xAODcLcLCaloVertexedClusterBase_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloVertexedClusterBase_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLCaloVertexedTopoCluster_Dictionary();
   static void xAODcLcLCaloVertexedTopoCluster_TClassManip(TClass*);
   static void delete_xAODcLcLCaloVertexedTopoCluster(void *p);
   static void deleteArray_xAODcLcLCaloVertexedTopoCluster(void *p);
   static void destruct_xAODcLcLCaloVertexedTopoCluster(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::CaloVertexedTopoCluster*)
   {
      ::xAOD::CaloVertexedTopoCluster *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::CaloVertexedTopoCluster));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::CaloVertexedTopoCluster", "xAODCaloEvent/CaloVertexedTopoCluster.h", 31,
                  typeid(::xAOD::CaloVertexedTopoCluster), DefineBehavior(ptr, ptr),
                  &xAODcLcLCaloVertexedTopoCluster_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::CaloVertexedTopoCluster) );
      instance.SetDelete(&delete_xAODcLcLCaloVertexedTopoCluster);
      instance.SetDeleteArray(&deleteArray_xAODcLcLCaloVertexedTopoCluster);
      instance.SetDestructor(&destruct_xAODcLcLCaloVertexedTopoCluster);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::CaloVertexedTopoCluster*)
   {
      return GenerateInitInstanceLocal((::xAOD::CaloVertexedTopoCluster*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::CaloVertexedTopoCluster*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLCaloVertexedTopoCluster_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::CaloVertexedTopoCluster*)0x0)->GetClass();
      xAODcLcLCaloVertexedTopoCluster_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLCaloVertexedTopoCluster_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCaloCluster_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloCluster_v1 : new ::xAOD::CaloCluster_v1;
   }
   static void *newArray_xAODcLcLCaloCluster_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloCluster_v1[nElements] : new ::xAOD::CaloCluster_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloCluster_v1(void *p) {
      delete ((::xAOD::CaloCluster_v1*)p);
   }
   static void deleteArray_xAODcLcLCaloCluster_v1(void *p) {
      delete [] ((::xAOD::CaloCluster_v1*)p);
   }
   static void destruct_xAODcLcLCaloCluster_v1(void *p) {
      typedef ::xAOD::CaloCluster_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloCluster_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCaloCluster_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CaloCluster_v1> : new ::DataVector<xAOD::CaloCluster_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCaloCluster_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CaloCluster_v1>[nElements] : new ::DataVector<xAOD::CaloCluster_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCaloCluster_v1gR(void *p) {
      delete ((::DataVector<xAOD::CaloCluster_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCaloCluster_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CaloCluster_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCaloCluster_v1gR(void *p) {
      typedef ::DataVector<xAOD::CaloCluster_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CaloCluster_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCaloClusterAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloClusterAuxContainer_v1 : new ::xAOD::CaloClusterAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCaloClusterAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloClusterAuxContainer_v1[nElements] : new ::xAOD::CaloClusterAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloClusterAuxContainer_v1(void *p) {
      delete ((::xAOD::CaloClusterAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCaloClusterAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CaloClusterAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCaloClusterAuxContainer_v1(void *p) {
      typedef ::xAOD::CaloClusterAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloClusterAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCaloClusterAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloClusterAuxContainer_v2 : new ::xAOD::CaloClusterAuxContainer_v2;
   }
   static void *newArray_xAODcLcLCaloClusterAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloClusterAuxContainer_v2[nElements] : new ::xAOD::CaloClusterAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloClusterAuxContainer_v2(void *p) {
      delete ((::xAOD::CaloClusterAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLCaloClusterAuxContainer_v2(void *p) {
      delete [] ((::xAOD::CaloClusterAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLCaloClusterAuxContainer_v2(void *p) {
      typedef ::xAOD::CaloClusterAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloClusterAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::CaloCluster_v1> > : new ::DataLink<DataVector<xAOD::CaloCluster_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::CaloCluster_v1> >[nElements] : new ::DataLink<DataVector<xAOD::CaloCluster_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::CaloCluster_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::CaloCluster_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::CaloCluster_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::CaloCluster_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::CaloCluster_v1> > : new ::ElementLink<DataVector<xAOD::CaloCluster_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::CaloCluster_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::CaloCluster_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::CaloCluster_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::CaloCluster_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::CaloCluster_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::CaloCluster_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCaloClusterBadChannelData_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloClusterBadChannelData_v1 : new ::xAOD::CaloClusterBadChannelData_v1;
   }
   static void *newArray_xAODcLcLCaloClusterBadChannelData_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloClusterBadChannelData_v1[nElements] : new ::xAOD::CaloClusterBadChannelData_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloClusterBadChannelData_v1(void *p) {
      delete ((::xAOD::CaloClusterBadChannelData_v1*)p);
   }
   static void deleteArray_xAODcLcLCaloClusterBadChannelData_v1(void *p) {
      delete [] ((::xAOD::CaloClusterBadChannelData_v1*)p);
   }
   static void destruct_xAODcLcLCaloClusterBadChannelData_v1(void *p) {
      typedef ::xAOD::CaloClusterBadChannelData_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloClusterBadChannelData_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCaloTower_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloTower_v1 : new ::xAOD::CaloTower_v1;
   }
   static void *newArray_xAODcLcLCaloTower_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloTower_v1[nElements] : new ::xAOD::CaloTower_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloTower_v1(void *p) {
      delete ((::xAOD::CaloTower_v1*)p);
   }
   static void deleteArray_xAODcLcLCaloTower_v1(void *p) {
      delete [] ((::xAOD::CaloTower_v1*)p);
   }
   static void destruct_xAODcLcLCaloTower_v1(void *p) {
      typedef ::xAOD::CaloTower_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloTower_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCaloTowerContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloTowerContainer_v1 : new ::xAOD::CaloTowerContainer_v1;
   }
   static void *newArray_xAODcLcLCaloTowerContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloTowerContainer_v1[nElements] : new ::xAOD::CaloTowerContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloTowerContainer_v1(void *p) {
      delete ((::xAOD::CaloTowerContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCaloTowerContainer_v1(void *p) {
      delete [] ((::xAOD::CaloTowerContainer_v1*)p);
   }
   static void destruct_xAODcLcLCaloTowerContainer_v1(void *p) {
      typedef ::xAOD::CaloTowerContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloTowerContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLCaloTowerAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloTowerAuxContainer_v1 : new ::xAOD::CaloTowerAuxContainer_v1;
   }
   static void *newArray_xAODcLcLCaloTowerAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::CaloTowerAuxContainer_v1[nElements] : new ::xAOD::CaloTowerAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloTowerAuxContainer_v1(void *p) {
      delete ((::xAOD::CaloTowerAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLCaloTowerAuxContainer_v1(void *p) {
      delete [] ((::xAOD::CaloTowerAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLCaloTowerAuxContainer_v1(void *p) {
      typedef ::xAOD::CaloTowerAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloTowerAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLCaloTower_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::CaloTower_v1> : new ::DataVector<xAOD::CaloTower_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLCaloTower_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::CaloTower_v1>[nElements] : new ::DataVector<xAOD::CaloTower_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLCaloTower_v1gR(void *p) {
      delete ((::DataVector<xAOD::CaloTower_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLCaloTower_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::CaloTower_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLCaloTower_v1gR(void *p) {
      typedef ::DataVector<xAOD::CaloTower_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::CaloTower_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklExAODcLcLCaloTowerContainer_v1gR(void *p) {
      return  p ? new(p) ::DataLink<xAOD::CaloTowerContainer_v1> : new ::DataLink<xAOD::CaloTowerContainer_v1>;
   }
   static void *newArray_DataLinklExAODcLcLCaloTowerContainer_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<xAOD::CaloTowerContainer_v1>[nElements] : new ::DataLink<xAOD::CaloTowerContainer_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklExAODcLcLCaloTowerContainer_v1gR(void *p) {
      delete ((::DataLink<xAOD::CaloTowerContainer_v1>*)p);
   }
   static void deleteArray_DataLinklExAODcLcLCaloTowerContainer_v1gR(void *p) {
      delete [] ((::DataLink<xAOD::CaloTowerContainer_v1>*)p);
   }
   static void destruct_DataLinklExAODcLcLCaloTowerContainer_v1gR(void *p) {
      typedef ::DataLink<xAOD::CaloTowerContainer_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<xAOD::CaloTowerContainer_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklExAODcLcLCaloTowerContainer_v1gR(void *p) {
      return  p ? new(p) ::ElementLink<xAOD::CaloTowerContainer_v1> : new ::ElementLink<xAOD::CaloTowerContainer_v1>;
   }
   static void *newArray_ElementLinklExAODcLcLCaloTowerContainer_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<xAOD::CaloTowerContainer_v1>[nElements] : new ::ElementLink<xAOD::CaloTowerContainer_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklExAODcLcLCaloTowerContainer_v1gR(void *p) {
      delete ((::ElementLink<xAOD::CaloTowerContainer_v1>*)p);
   }
   static void deleteArray_ElementLinklExAODcLcLCaloTowerContainer_v1gR(void *p) {
      delete [] ((::ElementLink<xAOD::CaloTowerContainer_v1>*)p);
   }
   static void destruct_ElementLinklExAODcLcLCaloTowerContainer_v1gR(void *p) {
      typedef ::ElementLink<xAOD::CaloTowerContainer_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<xAOD::CaloTowerContainer_v1>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloVertexedClusterBase(void *p) {
      delete ((::xAOD::CaloVertexedClusterBase*)p);
   }
   static void deleteArray_xAODcLcLCaloVertexedClusterBase(void *p) {
      delete [] ((::xAOD::CaloVertexedClusterBase*)p);
   }
   static void destruct_xAODcLcLCaloVertexedClusterBase(void *p) {
      typedef ::xAOD::CaloVertexedClusterBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloVertexedClusterBase

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLCaloVertexedTopoCluster(void *p) {
      delete ((::xAOD::CaloVertexedTopoCluster*)p);
   }
   static void deleteArray_xAODcLcLCaloVertexedTopoCluster(void *p) {
      delete [] ((::xAOD::CaloVertexedTopoCluster*)p);
   }
   static void destruct_xAODcLcLCaloVertexedTopoCluster(void *p) {
      typedef ::xAOD::CaloVertexedTopoCluster current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::CaloVertexedTopoCluster

namespace ROOT {
   static TClass *vectorlExAODcLcLCaloClusterBadChannelData_v1gR_Dictionary();
   static void vectorlExAODcLcLCaloClusterBadChannelData_v1gR_TClassManip(TClass*);
   static void *new_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(void *p = 0);
   static void *newArray_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(Long_t size, void *p);
   static void delete_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(void *p);
   static void deleteArray_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(void *p);
   static void destruct_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<xAOD::CaloClusterBadChannelData_v1>*)
   {
      vector<xAOD::CaloClusterBadChannelData_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<xAOD::CaloClusterBadChannelData_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<xAOD::CaloClusterBadChannelData_v1>", -2, "vector", 210,
                  typeid(vector<xAOD::CaloClusterBadChannelData_v1>), DefineBehavior(ptr, ptr),
                  &vectorlExAODcLcLCaloClusterBadChannelData_v1gR_Dictionary, isa_proxy, 4,
                  sizeof(vector<xAOD::CaloClusterBadChannelData_v1>) );
      instance.SetNew(&new_vectorlExAODcLcLCaloClusterBadChannelData_v1gR);
      instance.SetNewArray(&newArray_vectorlExAODcLcLCaloClusterBadChannelData_v1gR);
      instance.SetDelete(&delete_vectorlExAODcLcLCaloClusterBadChannelData_v1gR);
      instance.SetDeleteArray(&deleteArray_vectorlExAODcLcLCaloClusterBadChannelData_v1gR);
      instance.SetDestructor(&destruct_vectorlExAODcLcLCaloClusterBadChannelData_v1gR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<xAOD::CaloClusterBadChannelData_v1> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<xAOD::CaloClusterBadChannelData_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlExAODcLcLCaloClusterBadChannelData_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<xAOD::CaloClusterBadChannelData_v1>*)0x0)->GetClass();
      vectorlExAODcLcLCaloClusterBadChannelData_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlExAODcLcLCaloClusterBadChannelData_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<xAOD::CaloClusterBadChannelData_v1> : new vector<xAOD::CaloClusterBadChannelData_v1>;
   }
   static void *newArray_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<xAOD::CaloClusterBadChannelData_v1>[nElements] : new vector<xAOD::CaloClusterBadChannelData_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(void *p) {
      delete ((vector<xAOD::CaloClusterBadChannelData_v1>*)p);
   }
   static void deleteArray_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(void *p) {
      delete [] ((vector<xAOD::CaloClusterBadChannelData_v1>*)p);
   }
   static void destruct_vectorlExAODcLcLCaloClusterBadChannelData_v1gR(void *p) {
      typedef vector<xAOD::CaloClusterBadChannelData_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<xAOD::CaloClusterBadChannelData_v1>

namespace ROOT {
   static TClass *vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR_Dictionary();
   static void vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(void *p);
   static void deleteArray_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(void *p);
   static void destruct_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<xAOD::CaloClusterBadChannelData_v1> >*)
   {
      vector<vector<xAOD::CaloClusterBadChannelData_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<xAOD::CaloClusterBadChannelData_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<xAOD::CaloClusterBadChannelData_v1> >", -2, "vector", 210,
                  typeid(vector<vector<xAOD::CaloClusterBadChannelData_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<xAOD::CaloClusterBadChannelData_v1> >) );
      instance.SetNew(&new_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<xAOD::CaloClusterBadChannelData_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<xAOD::CaloClusterBadChannelData_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<xAOD::CaloClusterBadChannelData_v1> >*)0x0)->GetClass();
      vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<xAOD::CaloClusterBadChannelData_v1> > : new vector<vector<xAOD::CaloClusterBadChannelData_v1> >;
   }
   static void *newArray_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<xAOD::CaloClusterBadChannelData_v1> >[nElements] : new vector<vector<xAOD::CaloClusterBadChannelData_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(void *p) {
      delete ((vector<vector<xAOD::CaloClusterBadChannelData_v1> >*)p);
   }
   static void deleteArray_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(void *p) {
      delete [] ((vector<vector<xAOD::CaloClusterBadChannelData_v1> >*)p);
   }
   static void destruct_vectorlEvectorlExAODcLcLCaloClusterBadChannelData_v1gRsPgR(void *p) {
      typedef vector<vector<xAOD::CaloClusterBadChannelData_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<xAOD::CaloClusterBadChannelData_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >*)
   {
      vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > > : new vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >[nElements] : new vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR_Dictionary();
   static void vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p);
   static void destruct_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<xAOD::CaloTowerContainer_v1> >*)
   {
      vector<ElementLink<xAOD::CaloTowerContainer_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<xAOD::CaloTowerContainer_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<xAOD::CaloTowerContainer_v1> >", -2, "vector", 210,
                  typeid(vector<ElementLink<xAOD::CaloTowerContainer_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<xAOD::CaloTowerContainer_v1> >) );
      instance.SetNew(&new_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<xAOD::CaloTowerContainer_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<xAOD::CaloTowerContainer_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<xAOD::CaloTowerContainer_v1> >*)0x0)->GetClass();
      vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::CaloTowerContainer_v1> > : new vector<ElementLink<xAOD::CaloTowerContainer_v1> >;
   }
   static void *newArray_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::CaloTowerContainer_v1> >[nElements] : new vector<ElementLink<xAOD::CaloTowerContainer_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p) {
      delete ((vector<ElementLink<xAOD::CaloTowerContainer_v1> >*)p);
   }
   static void deleteArray_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p) {
      delete [] ((vector<ElementLink<xAOD::CaloTowerContainer_v1> >*)p);
   }
   static void destruct_vectorlEElementLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p) {
      typedef vector<ElementLink<xAOD::CaloTowerContainer_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<xAOD::CaloTowerContainer_v1> >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > : new vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR_Dictionary();
   static void vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p);
   static void destruct_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<xAOD::CaloTowerContainer_v1> >*)
   {
      vector<DataLink<xAOD::CaloTowerContainer_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<xAOD::CaloTowerContainer_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<xAOD::CaloTowerContainer_v1> >", -2, "vector", 210,
                  typeid(vector<DataLink<xAOD::CaloTowerContainer_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<xAOD::CaloTowerContainer_v1> >) );
      instance.SetNew(&new_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<xAOD::CaloTowerContainer_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<xAOD::CaloTowerContainer_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<xAOD::CaloTowerContainer_v1> >*)0x0)->GetClass();
      vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::CaloTowerContainer_v1> > : new vector<DataLink<xAOD::CaloTowerContainer_v1> >;
   }
   static void *newArray_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::CaloTowerContainer_v1> >[nElements] : new vector<DataLink<xAOD::CaloTowerContainer_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p) {
      delete ((vector<DataLink<xAOD::CaloTowerContainer_v1> >*)p);
   }
   static void deleteArray_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p) {
      delete [] ((vector<DataLink<xAOD::CaloTowerContainer_v1> >*)p);
   }
   static void destruct_vectorlEDataLinklExAODcLcLCaloTowerContainer_v1gRsPgR(void *p) {
      typedef vector<DataLink<xAOD::CaloTowerContainer_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<xAOD::CaloTowerContainer_v1> >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::CaloCluster_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::CaloCluster_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::CaloCluster_v1> > > : new vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLCaloCluster_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::CaloCluster_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >

namespace ROOT {
   static TClass *bitsetlE3gR_Dictionary();
   static void bitsetlE3gR_TClassManip(TClass*);
   static void *new_bitsetlE3gR(void *p = 0);
   static void *newArray_bitsetlE3gR(Long_t size, void *p);
   static void delete_bitsetlE3gR(void *p);
   static void deleteArray_bitsetlE3gR(void *p);
   static void destruct_bitsetlE3gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const bitset<3>*)
   {
      bitset<3> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(bitset<3>));
      static ::ROOT::TGenericClassInfo 
         instance("bitset<3>", 2, "bitset", 748,
                  typeid(bitset<3>), DefineBehavior(ptr, ptr),
                  &bitsetlE3gR_Dictionary, isa_proxy, 4,
                  sizeof(bitset<3>) );
      instance.SetNew(&new_bitsetlE3gR);
      instance.SetNewArray(&newArray_bitsetlE3gR);
      instance.SetDelete(&delete_bitsetlE3gR);
      instance.SetDeleteArray(&deleteArray_bitsetlE3gR);
      instance.SetDestructor(&destruct_bitsetlE3gR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback<TStdBitsetHelper< bitset<3> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const bitset<3>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *bitsetlE3gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const bitset<3>*)0x0)->GetClass();
      bitsetlE3gR_TClassManip(theClass);
   return theClass;
   }

   static void bitsetlE3gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_bitsetlE3gR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) bitset<3> : new bitset<3>;
   }
   static void *newArray_bitsetlE3gR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) bitset<3>[nElements] : new bitset<3>[nElements];
   }
   // Wrapper around operator delete
   static void delete_bitsetlE3gR(void *p) {
      delete ((bitset<3>*)p);
   }
   static void deleteArray_bitsetlE3gR(void *p) {
      delete [] ((bitset<3>*)p);
   }
   static void destruct_bitsetlE3gR(void *p) {
      typedef bitset<3> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class bitset<3>

namespace {
  void TriggerDictionaryInitialization_xAODCaloEvent_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCaloEvent/Root",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/makeTree/xAODCaloEvent/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCaloEvent/CaloCluster.h")))  CaloCluster_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCaloEvent/CaloCluster.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@CE498B3B-A32D-43A3-B9B3-C13D136BACFC)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCaloEvent/versions/CaloClusterAuxContainer_v1.h")))  CaloClusterAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@451393B0-69CD-11E4-A739-02163E00A64D)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCaloEvent/versions/CaloClusterAuxContainer_v2.h")))  CaloClusterAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCaloEvent/CaloCluster.h")))  CaloClusterBadChannelData_v1;}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCaloEvent/CaloTower.h")))  CaloTower_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@EEA02A0F-98D3-464D-BAF1-1C944A700B8A)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCaloEvent/CaloTowerContainer.h")))  CaloTowerContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@CE74E4D1-D2F4-4CED-8191-EC26D8836575)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCaloEvent/CaloTowerAuxContainer.h")))  CaloTowerAuxContainer_v1;}
namespace std{template <size_t _Nb> class __attribute__((annotate("$clingAutoload$bitset")))  bitset;
}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCaloEvent/CaloVertexedClusterBase.h")))  CaloVertexedClusterBase;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCaloEvent/CaloVertexedTopoCluster.h")))  CaloVertexedTopoCluster;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODCaloEvent"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODCaloEventDict.h 700669 2015-10-15 08:31:13Z wlampl $
#ifndef XAODCALOEVENT_XAODCALOEVENTDICT_H
#define XAODCALOEVENT_XAODCALOEVENTDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__


// System include(s):
#include <vector>
#include <bitset>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/versions/CaloCluster_v1.h"
#include "xAODCaloEvent/versions/CaloClusterContainer_v1.h"
#include "xAODCaloEvent/versions/CaloClusterAuxContainer_v1.h"
#include "xAODCaloEvent/versions/CaloClusterAuxContainer_v2.h"
#include "xAODCaloEvent/CaloVertexedClusterBase.h"
#include "xAODCaloEvent/CaloVertexedTopoCluster.h"

//#include "xAODCaloEvent/CaloTowerDescriptor.h"
//#include "xAODCaloEvent/CaloTowerGrid.h"
#include "xAODCaloEvent/CaloTower.h"
#include "xAODCaloEvent/CaloTowerContainer.h"
#include "xAODCaloEvent/CaloTowerAuxContainer.h"
#include "xAODCaloEvent/versions/CaloTower_v1.h"
#include "xAODCaloEvent/versions/CaloTowerContainer_v1.h"
#include "xAODCaloEvent/versions/CaloTowerAuxContainer_v1.h"


namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODCALOEVENT {
      // Container(s):
      xAOD::CaloClusterContainer_v1 c1;
      // Data link(s):
      DataLink< xAOD::CaloClusterContainer_v1 > dl1;
     std::vector< DataLink< xAOD::CaloClusterContainer_v1 > > dl2;
      // Element link(s):
      ElementLink< xAOD::CaloClusterContainer_v1 > el1;
      std::vector< ElementLink< xAOD::CaloClusterContainer_v1 > > el2;
      std::vector< std::vector< ElementLink< xAOD::CaloClusterContainer_v1 > > > el3;
      // Additional type(s) needed:
      std::bitset< 3 > type1; // Using 3 instead of CaloCluster::NSTATES...

     //BadChannel lists 
     xAOD::CaloClusterBadChannelData_v1 bcd;
     std::vector<xAOD::CaloClusterBadChannelData_v1> vbcd;
     std::vector<std::vector<xAOD::CaloClusterBadChannelData_v1> > vvbcd;

     // CaloTower containers
     xAOD::CaloTowerContainer_v1 t1;
     xAOD::CaloTowerContainerBase_v1 dvt;
     // Data link(s)
     DataLink< xAOD::CaloTowerContainer_v1 > tdl1;
     std::vector< DataLink< xAOD::CaloTowerContainer_v1 > > tdl2;
     // Element link(s)
     ElementLink< xAOD::CaloTowerContainer_v1 > tel1;
     std::vector< ElementLink< xAOD::CaloTowerContainer_v1 > > tel2;
     std::vector< std::vector< ElementLink< xAOD::CaloTowerContainer_v1 > > > tel3;

   };
}

#endif // XAODCALOEVENT_XAODCALOEVENTDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::CaloCluster_v1> >", payloadCode, "@",
"DataLink<xAOD::CaloClusterContainer_v1>", payloadCode, "@",
"DataLink<xAOD::CaloTowerContainer_v1>", payloadCode, "@",
"DataVector<xAOD::CaloCluster_v1>", payloadCode, "@",
"DataVector<xAOD::CaloTower_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::CaloCluster_v1> >", payloadCode, "@",
"ElementLink<xAOD::CaloClusterContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::CaloTowerContainer_v1>", payloadCode, "@",
"bitset<3>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::CaloCluster_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::CaloClusterContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::CaloTowerContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::CaloClusterContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::CaloTowerContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::CaloClusterContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::CaloTowerContainer_v1> > >", payloadCode, "@",
"vector<std::vector<xAOD::CaloClusterBadChannelData_v1> >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::CaloCluster_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<xAOD::CaloTowerContainer_v1> > >", payloadCode, "@",
"vector<vector<xAOD::CaloClusterBadChannelData_v1> >", payloadCode, "@",
"vector<xAOD::CaloClusterBadChannelData_v1>", payloadCode, "@",
"xAOD::CaloClusterAuxContainer_v1", payloadCode, "@",
"xAOD::CaloClusterAuxContainer_v2", payloadCode, "@",
"xAOD::CaloClusterBadChannelData_v1", payloadCode, "@",
"xAOD::CaloClusterContainer_v1", payloadCode, "@",
"xAOD::CaloCluster_v1", payloadCode, "@",
"xAOD::CaloTowerAuxContainer_v1", payloadCode, "@",
"xAOD::CaloTowerContainerBase_v1", payloadCode, "@",
"xAOD::CaloTowerContainer_v1", payloadCode, "@",
"xAOD::CaloTower_v1", payloadCode, "@",
"xAOD::CaloVertexedClusterBase", payloadCode, "@",
"xAOD::CaloVertexedTopoCluster", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODCaloEvent_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODCaloEvent_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODCaloEvent_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODCaloEvent_Reflex() {
  TriggerDictionaryInitialization_xAODCaloEvent_Reflex_Impl();
}

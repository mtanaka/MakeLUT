# echo "setup xAODTrigMuon xAODTrigMuon-00-02-08 in /home/mtanaka/WorkSpace/anal-new/Event/xAOD"

if test "${CMTROOT}" = ""; then
  CMTROOT=/afs/cern.ch/sw/contrib/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtxAODTrigMuontempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtxAODTrigMuontempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=xAODTrigMuon -version=xAODTrigMuon-00-02-08 -path=/home/mtanaka/WorkSpace/anal-new/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODTrigMuontempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=xAODTrigMuon -version=xAODTrigMuon-00-02-08 -path=/home/mtanaka/WorkSpace/anal-new/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODTrigMuontempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtxAODTrigMuontempfile}
  unset cmtxAODTrigMuontempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtxAODTrigMuontempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtxAODTrigMuontempfile}
unset cmtxAODTrigMuontempfile
return $cmtsetupstatus


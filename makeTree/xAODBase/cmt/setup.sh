# echo "setup xAODBase xAODBase-00-00-22 in /home/mtanaka/Event/xAOD"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtxAODBasetempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtxAODBasetempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=xAODBase -version=xAODBase-00-00-22 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODBasetempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=xAODBase -version=xAODBase-00-00-22 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODBasetempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtxAODBasetempfile}
  unset cmtxAODBasetempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtxAODBasetempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtxAODBasetempfile}
unset cmtxAODBasetempfile
return $cmtsetupstatus


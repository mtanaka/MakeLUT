/**
   @mainpage xAODBase package

   @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>

   $Revision$
   $Date$

   @section xAODBaseOverview Overview

   This is the most base-package of the xAOD EDM. It defines general
   interfaces that are used in all parts of the xAOD code.

   @section xAODBaseClasses Main Types

   The main enumerations, definitions and classes of the package are
   the following:
      - xAOD::Type::ObjectType: Enumeration describing all major xAOD
        object types.
      - xAOD::IParticle: Interface for all particle-like EDM classes
      - xAOD::IParticleContainer: Base class for all the particle-like
        containers in the xAOD EDM.

   @htmlinclude used_packages.html

   @include requirements
*/

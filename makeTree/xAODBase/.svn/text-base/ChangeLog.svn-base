2014-09-29 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added some helper functions following TJ's teplate. They are to
	  be used for setting ElementLinks from deep/shallow copied object
	  to their parents.
	* Created dictionaries for these functions as well, to make it
	  possible to use them from PyROOT later on.
	* Tagging as xAODBase-00-00-22

2014-09-26 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added two new IParticle types: Particle and CompositeParticle
	* Updated the print operator to know about these types.
	* Renamed the unit test for the ObjectType operator in order
	  for RootCore to recognise it as a unit test.
	* Declared the unit test for CMT as well, and created a reference
	  file for it.
	* Tagging as xAODBase-00-00-21

2014-07-09 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Adding two new IParticle types: L2IsoMuon, L2CombinedMuon
	* Taught the print operator about the new values.
	* Tagging as xAODBase-00-00-20

2014-07-01  scott snyder  <snyder@bnl.gov>

	* Tagging xAODBase-00-00-19.
	* xAODBase/IParticle.h: Use SG_BASE to declare that IParticle
	derives from AuxElement.

2014-06-20 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added a new object type, TruthPileupEvent, based on the code
	  provided by Andy Buckley.
	* Updated his e-mail address in the IParticle.h header.
	* Added the new type in the print operator in ObjectType.cxx.
	* Finally decided to remove hscript.yml. Hwaf will not come
	  back, it's fairly obvious by now.
	* Tagging as xAODBase-00-00-18

2014-05-23 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Updated IParticle::isAvailable to use the function declared
	  in AuxElement with the same name.
	* Added IParticle::isAvailableWritable, implemented in the same
	  manner.
	* Tagging as xAODBase-00-00-17

2014-05-23 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Now generating dictionaries for the most commonly used
	  template functions in xAOD::IParticle. Should allow for
	  using these functions from PyROOT hopefully.
	* Tagging as xAODBase-00-00-16

2014-04-15 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added an output operator to be able to easily print
	  xAOD::Type::ObjectType enumeration values in (debug)
	  messages.
	* Added a simple test for this new functionality. (Only
	  compiled by RootCore for now.)
	* Removed the LinkDef.h file, as it's no longer needed.
	* Updated the CMT requirements file to compile the new
	  source file correctly, but didn't actually test this update.
	  (Only tested the new code with RootCore.)
	* Tagging as xAODBase-00-00-15

2014-04-08 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* With the very latest version of the ROOT master branch, the
	  IParticle dictionaries are now generated correctly.
	* Could remove some of the exclusion rules after the recent
	  bugfixes in rootcling, but not all of them.
	* Tagging as xAODBase-00-00-14

2014-04-05 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Changed the name of GCCXML_DUMMY_INSTANTIATION to
	  a more unique name in the dictionary header. Was necessary,
	  as rootcling is complaining about seeing the same struct name
	  in multiple libraries. (Which really shouldn't clash actually,
	  but let's do this until ROOT 6 stops complaining about this.)
	* Still not tagging, as the DataVector selection rule is still
	  not applied correctly.

2014-04-05 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Tried making rootcling take the DataVector selection rules
	  into account, but for some reason I didn't manage. To be
	  followed up with the ROOT developers...
	* Not tagging for the moment.

2014-02-07 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added a new function, xAOD::IParticle::isAvailable,
	  for making it easy to check if a user decoration is
	  available on the object.
	* Tagging as xAODBase-00-00-13

2014-01-24 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Made the package depend on libPhysics.so for the RootCore
	  build.
	* Tagging as xAODBase-00-00-12

2014-01-22 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Updated the package to compile with RootCore.
	* Had to introduce an empty CINT dictionary for the standalone
	  build, otherwise the Reflex dictionary's compilation would
	  fail.
	* Not tagging yet.

2014-01-13 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Adding the xAOD::Type::TruthEvent type.
	* Tagging as xAODBase-00-00-11

2013-12-04 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Adding the xAOD::Type::NeutralParticle type.
	* Had to shift the enumeration numbers a bit, but at this stage this
	  should still be okay.
	* Tagging as xAODBase-00-00-10

2013-11-19 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Adding a dictionary for
	  vector<vector<ElementLink<IParticleContainer> > > in order to
	  save vectors of element links following the A-team recommendation.
	* Tagging as xAODBase-00-00-09

2013-11-15 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Fixing a typo in cmt/requirements. Strange that it didn't cause
	  any compilation problems...
	* Enabling keyword expansion on hscript.yml, and formatting it a
	  bit.
	* Tagging as xAODBase-00-00-08

2013-11-14  Sebastien Binet  <binet@farnsworth>

	* tagging xAODBase-00-00-07
	* hwafize
	* A hscript
	* D wscript

2013-11-06 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Adding a bunch of smart pointer dictionaries to the package,
	  so clients would be free to use whatever kind of smart pointers
	  they want to an IParticle.
	* Finally declared that the package only privately uses the
	  AthContainers and AthLinks packages in standalone mode.
	* Tagging as xAODBase-00-00-06

2013-10-28 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Made xAOD::IParticle inherit from SG::AuxElement finally.
	* Made the xAOD::AuxElement::Accessor class protected inside
	  xAOD::IParticle to make it less obvious to use for novice
	  analysers. Analysers should instead use the xAOD interface
	  classes as much as possible.
	* Introduced a helper typedef for xAOD::IParticleContainer. This
	  is really just meant to make the code a bit nicer.
	* Updated the documentation very slightly.
	* Tagging as xAODBase-00-00-05

2013-10-25 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added a new enumeration type to the package: xAOD::Type::ObjectType
	* It will be used to provide easy identification to all types of
	  xAOD objects, by each of them providing a type() function
	  returning one of these values.
	* Tagging as xAODBase-00-00-04

2013-10-03 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Updated the package's requirements file to compile the
	  dictionaries in Athena as well.
	* Tagging as xAODBase-00-00-03

2013-10-02 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* The package now generates a dictionary for xAOD::IParticle
	  and DataVector<xAOD::IParticle> in standalone compilation.
	* Will tag the change once the requirements file is updated
	  as well.

2013-09-19 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Made sure that all packages that use this one, are linked
	  against libPhysics. In order to have access to TLorentzVector.
	* Tagging as xAODBase-00-00-02

2013-09-17 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added a requirements file to the package in order to "compile"
	  it in Athena as well.
	* Also added a Doxygen main description file.
	* Tagging as xAODBase-00-00-01

2013-09-17 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Created the xAODBase package, with just the xAOD::IParticle
	  interface class for now.
	* Started writing the code in the hwaf environment, so no
	  CMT requirements file yet. (Will come once I'll have the
	  package checked out in a CMT workarea as well.)
	* Not tagging for now.

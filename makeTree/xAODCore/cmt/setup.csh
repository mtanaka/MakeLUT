# echo "setup xAODCore xAODCore-00-00-84-04 in /home/mtanaka/Event/xAOD"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtxAODCoretempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtxAODCoretempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=xAODCore -version=xAODCore-00-00-84-04 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODCoretempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=xAODCore -version=xAODCore-00-00-84-04 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODCoretempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtxAODCoretempfile}
  unset cmtxAODCoretempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtxAODCoretempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtxAODCoretempfile}
unset cmtxAODCoretempfile
exit $cmtsetupstatus


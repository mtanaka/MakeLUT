# echo "setup xAODCaloEvent xAODCaloEvent-00-01-16 in /home/mtanaka/Event/xAOD"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtxAODCaloEventtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtxAODCaloEventtempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=xAODCaloEvent -version=xAODCaloEvent-00-01-16 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODCaloEventtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=xAODCaloEvent -version=xAODCaloEvent-00-01-16 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODCaloEventtempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtxAODCaloEventtempfile}
  unset cmtxAODCaloEventtempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtxAODCaloEventtempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtxAODCaloEventtempfile}
unset cmtxAODCaloEventtempfile
return $cmtsetupstatus


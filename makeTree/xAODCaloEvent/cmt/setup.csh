# echo "setup xAODCaloEvent xAODCaloEvent-00-01-16 in /home/mtanaka/Event/xAOD"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtxAODCaloEventtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtxAODCaloEventtempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=xAODCaloEvent -version=xAODCaloEvent-00-01-16 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODCaloEventtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=xAODCaloEvent -version=xAODCaloEvent-00-01-16 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODCaloEventtempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtxAODCaloEventtempfile}
  unset cmtxAODCaloEventtempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtxAODCaloEventtempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtxAODCaloEventtempfile}
unset cmtxAODCaloEventtempfile
exit $cmtsetupstatus


/**
   @mainpage xAODCaloEvent package

   @author Scott Snyder <Scott.Snyder@cern.ch>
   @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>

   $Revision$
   $Date$

   @section xAODCaloEventOverview Overview

   This package holds the ROOT-readable implementation of the reconstructed
   calorimeter information of the detector.

   @section xAODCaloEventClasses Main Classes

   The main class(es) of the package are the following:
      - xAOD::CaloCluster: Typedef to the latest <code>CaloCluster_vX</code>
        class version.
      - xAOD::CaloCluster_v1: The current latest calorimeter cluster
        implementation.

   @htmlinclude used_packages.html

   @include requirements
*/

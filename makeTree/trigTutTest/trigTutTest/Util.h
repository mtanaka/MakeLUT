#ifndef _INC_UTIL
#define _INC_UTIL

#include "TH1.h"
#include <iostream>
#include <map>
using namespace std;

class Util {
  public:
    Util();
    ~Util();
    int initialize();
    bool isPassedSA(int thr, float eta, float phi, float pt);
    bool isPassedComb(int thr, float eta, float pt);
    bool isPassedEF(int thr, float eta, float pt);
    float calcDr(float eta1, float eta2, float phi1, float phi2);
    pair<int,int> GetBinNumber(float eta, float phi);
    //bool isSmall(float phi);
    bool isSmall(float eez);
    bool isSmallInner(float brinnr);
    int GetPhiBinNumber(float phi, int sector);
    float calcAlpha(float r1, float z1, float r2, float z2);
    float calcIntercept(float r1, float z1, float r2, float z2);
    float calcDrThrL1(float pt);
    float calcDrThrL1LUT(float pt);
    float computeRadius3Points(float InnerZ, float InnerR,float EEZ, float EER,float MiddleZ, float MiddleR);
    bool isBadPhi(int phibinall);
    int GetPhiBinAllNumber(float phi);
    bool useSmall(int phibin);
    float whichSAPT(float alphapt,float betapt,float tgcpt,float innerspz,float middlespz,float outerspz, float tgcmid1z, float beta);
    float calcDistance(float x1,float y1,float x2,float y2,float x3,float y3);
    double dRL1byPt( double mupt );

};


#endif

#ifndef MyAnalysis_MyxAODAnalysisData_H
#define MyAnalysis_MyxAODAnalysisData_H

#include <EventLoop/Algorithm.h>
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "trigTutTest/Util.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "TH1.h"
#include <iostream>
#include <map>
using namespace Trig;
using namespace TrigConf;
using namespace std;

class MyxAODAnalysisData : public EL::Algorithm
{
  
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;
  xAOD::TEvent *m_event;  //!
  int m_eventCounter; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  std::string outputName;
  //tree output
  TTree *tree; //!

  float offline_pt; //!
  float offline_eta; //!
  float offline_phi; //!
  int offline_charge; //!
  float ext_eta; //!
  float ext_phi; //!
  float L1_eta; //!
  float L1_phi; //!
  float L2_pt; //!
  float L2_eta; //!
  float L2_phi; //!
  int L2_charge; //!
  float L2_ec_alpha; //!
  float L2_ec_beta; //!
  float L2_ec_radius; //!
  float L2_br_radius; //!
  float L2_etaMap; //!
  float L2_phiMap; //!
  int L2_saddress; //!
  float tgcInn_eta; //!
  float tgcInn_phi; //!
  float tgcMid1_r; //!
  float tgcMid1_z; //!
  float tgcMid1_eta; //!
  float tgcMid1_phi; //!
  float tgcMid2_r; //!
  float tgcMid2_z; //!
  vector < float > sp_r; //!
  vector < float > sp_z; //!
  vector < float > segment_r; //!
  vector < float > segment_z; //!
  vector < float > segment_eta; //!
  vector < float > segment_phi; //!
  vector < int > segment_chamber; //!

  bool runLocal; //!
  bool doDebug; //!
  bool useGRL; //!

  //tool
  Trig::TrigDecisionTool *trigDecTool; //! 
  TrigConf::xAODConfigTool *configTool; //!

  GoodRunsListSelectionTool *m_grl; //!

  map<string,int> triggerCounts;

  // this is a standard constructor
  MyxAODAnalysisData ();

  Util m_util; //!

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysisData, 1);
  
};

#endif

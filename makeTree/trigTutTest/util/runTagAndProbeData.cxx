//
// Look at all the triggers that have fired in a app.
//

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"

#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/OutputStream.h"
//#include "EventLoopAlgs/NTupleSvc.h"
#include "EventLoop/Algorithm.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODMuon/MuonContainer.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#include "trigTutTest/MyxAODAnalysisData.h"

#include "TSystem.h"

#include <iostream>

using namespace std;
using namespace Trig;
using namespace TrigConf;
using namespace xAOD;

// Config
const char* APP_NAME = "runTagAndProbeData";
const char* OUTPUT_FILE = "output/aho.root";

int main( int argc, char* argv[] ) {

  // Initialize (as done for all xAOD standalone programs!)
  RETURN_CHECK (APP_NAME, xAOD::Init(APP_NAME));

  SH::SampleHandler sh;
  
  //data16
  const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/user/m/mtanaka/WorkSpace/public");
  SH::ScanDir().sampleDepth(0).samplePattern("DAOD_MUON0.08384499._000001.pool.root.1").scan(sh, inputFilePath);

  //data15
  //const char* inputFilePath = gSystem->ExpandPathName ("/data/maxi174/zp/mtanaka/data15_13TeV_DAOD_MUON0/data15_13TeV.00282784.physics_Main.merge.DAOD_MUON0.r7562_p2521_p2510_tid07706227_00");
  //SH::ScanDir().sampleDepth(0).samplePattern("DAOD_MUON0.07706227._000001.pool.root.1").scan(sh, inputFilePath);

  sh.setMetaString ( "nc_tree", "CollectionTree" );

  sh.print ();

  EL::Job job;
  job.sampleHandler ( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, 1000);

  MyxAODAnalysisData* alg = new MyxAODAnalysisData();
  job.algsAdd ( alg );
  
  EL::DirectDriver driver;
  driver.submit (job, "submitDirData");

  return 0;
}

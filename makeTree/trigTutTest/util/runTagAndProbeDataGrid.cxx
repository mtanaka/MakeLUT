//
// Look at all the triggers that have fired in a app.
//

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"

#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "EventLoop/OutputStream.h"
//#include "EventLoopAlgs/NTupleSvc.h"
#include "EventLoop/Algorithm.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODMuon/MuonContainer.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#include "trigTutTest/MyxAODAnalysisData.h"

#include "TSystem.h"

#include <iostream>
#include <fstream>

using namespace std;
using namespace Trig;
using namespace TrigConf;
using namespace xAOD;

// Config
const char* APP_NAME = "runTagAndProbeDataGrid";
const char* OUTPUT_FILE = "output/aho.root";

int main( int argc, char* argv[] ) {

  cout << argv[1] << endl;

  ifstream finlist(argv[1]);
  string file_rec;
  cout << "input sample" << endl;
  while(finlist>>file_rec) {
    cout << file_rec.c_str() << endl;

    RETURN_CHECK (APP_NAME, xAOD::Init(APP_NAME));

    SH::SampleHandler sh;

    SH::scanDQ2(sh,file_rec);

    sh.setMetaString ( "nc_tree", "CollectionTree" );

    sh.print ();

    EL::Job job;
    job.sampleHandler ( sh );
    job.options()->setDouble (EL::Job::optMaxEvents, -1);

    MyxAODAnalysisData* alg = new MyxAODAnalysisData();
    job.algsAdd ( alg );

    stringstream name;
    //name << "user.mtanaka." << file_rec.c_str() << "_MakeLUT2016_v2";
    name << "user.mtanaka.mc_Zmumu_MakeLUT2016_v3";
    cout << name.str() << endl;

    stringstream dirName;
    dirName << "submitDirDataGrid/" << name.str();
    cout << dirName.str() << endl;
    EL::PrunDriver driver;
    driver.options()->setString("nc_outputSampleName",name.str().c_str());
    driver.submitOnly (job, dirName.str().c_str());
    name.str("");
    dirName.str("");
  }
  
  return 0;
}

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <AsgTools/ToolHandle.h>
#include "trigTutTest/MyxAODAnalysisData.h"
#include <iostream>
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTrigMuon/L2CombinedMuonContainer.h"
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigMuon/TrigMuonDefs.h"
#include "xAODTrigger/MuonRoIContainer.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigDecisionTool/ChainGroupFunctions.h"
#include "TrigConfInterfaces/ITrigConfigTool.h"
#include "trigTutTest/Util.h"
#include "TTree.h"
#include "TH1.h"
#include "TLorentzVector.h"
#include "TSystem.h"
using namespace std;
using namespace Trig;
using namespace TrigConf;
using namespace xAOD;

const double PI = 3.14159265258979;

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysisData)


MyxAODAnalysisData :: MyxAODAnalysisData ()
{
}

EL::StatusCode MyxAODAnalysisData :: setupJob (EL::Job& job)
{

  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();
  xAOD::Init(); // call before opening first file


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisData :: histInitialize ()
{
  cout << "histInitialize" << endl;
  
  tree = new TTree ("validationT", "validationT");
  tree->Branch( "offline_pt",     &offline_pt );
  tree->Branch( "offline_eta",     &offline_eta );
  tree->Branch( "offline_phi",     &offline_phi );
  tree->Branch( "offline_charge",     &offline_charge );
  tree->Branch( "ext_eta",     &ext_eta );
  tree->Branch( "ext_phi",     &ext_phi );
  tree->Branch( "L1_eta",     &L1_eta );
  tree->Branch( "L1_phi",     &L1_phi );
  tree->Branch( "L2_pt",     &L2_pt );
  tree->Branch( "L2_eta",     &L2_eta );
  tree->Branch( "L2_phi",     &L2_phi );
  tree->Branch( "L2_charge",     &L2_charge );
  tree->Branch( "L2_ec_alpha",     &L2_ec_alpha );
  tree->Branch( "L2_ec_beta",     &L2_ec_beta );
  tree->Branch( "L2_ec_radius",     &L2_ec_radius );
  tree->Branch( "L2_br_radius",     &L2_br_radius );
  tree->Branch( "L2_etaMap",     &L2_etaMap );
  tree->Branch( "L2_phiMap",     &L2_phiMap );
  tree->Branch( "L2_saddress",     &L2_saddress );
  tree->Branch( "tgcInn_eta",     &tgcInn_eta );
  tree->Branch( "tgcInn_phi",     &tgcInn_phi );
  tree->Branch( "tgcMid1_r",     &tgcMid1_r );
  tree->Branch( "tgcMid1_z",     &tgcMid1_z );
  tree->Branch( "tgcMid1_eta",     &tgcMid1_eta );
  tree->Branch( "tgcMid1_phi",     &tgcMid1_phi );
  tree->Branch( "tgcMid2_r",     &tgcMid2_r );
  tree->Branch( "tgcMid2_z",     &tgcMid2_z );
  tree->Branch( "sp_r",     &sp_r );
  tree->Branch( "sp_z",     &sp_z );
  tree->Branch( "segment_r",     &segment_r );
  tree->Branch( "segment_z",     &segment_z );
  tree->Branch( "segment_eta",     &segment_eta );
  tree->Branch( "segment_phi",     &segment_phi );
  tree->Branch( "segment_chamber",     &segment_chamber );

  tree->Print();

  wk()->addOutput(tree);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisData :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisData :: changeInput (bool firstFile)
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisData :: initialize ()
{

  runLocal=true;
  doDebug=false;
  useGRL=true;

  m_event = wk()->xaodEvent(); // you should have already added this as described before

  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", m_event->getEntries() ); // print long long int

  // count number of events
  m_eventCounter = 0;

  //xAODConfigTool configTool("xAODConfigTool");

  configTool = new xAODConfigTool("configTool");
  ToolHandle<TrigConf::ITrigConfigTool> configHandle(configTool);
  configHandle->initialize();

  trigDecTool = new TrigDecisionTool("TrigDecisionTool");
  trigDecTool->setProperty("ConfigTool",configHandle);
  trigDecTool->setProperty("TrigDecisionKey","xTrigDecision");
  trigDecTool->initialize();

  m_util.initialize();

  // GRL
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  const char*GRLFilePath;
  if(runLocal){//run local
    GRLFilePath = 
      "/afs/cern.ch/user/a/atlasdqm/grlgen/All_Good/data16_13TeV.periodAllYear_DetStatus-v81-pro20-09_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml";
  }
  else {//run grid
    GRLFilePath = 
      "__panda_rootCoreWorkDir/trigTutTest/share/data16_13TeV.periodAllYear_DetStatus-v81-pro20-09_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml";
  }
  const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(fullGRLFilePath);
  m_grl->setProperty( "GoodRunsListVec", vecStringGRL);
  m_grl->setProperty("PassThrough", false); // if true (default) will ignore result of GRL and will just pass all events
  bool scgrl = m_grl->initialize();
  if (!scgrl){
    cout << "ERROR: Following GRL is not found" << endl;
    cout << GRLFilePath << endl;
    return EL::StatusCode::FAILURE;
  }


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisData :: execute ()
{

  // print every 100 events, so we know where we are:
  if( (m_eventCounter % 10000) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  auto chainGroup = trigDecTool->getChainGroup(".*");
  for(auto &trig : chainGroup->getListOfTriggers()) {
    auto cg = trigDecTool->getChainGroup(trig);
    if (cg->isPassed()) {
      triggerCounts[trig]++;
    } else {
      triggerCounts[trig] += 0;
    }
  }

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
    Error("execute()", "Failed to retrieve event info collection. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  bool isMC = false;
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) isMC = true; // can do something with this later

  // if data check if event passes GRL
  if(useGRL){
    if(!isMC)// it's data!
      if(!m_grl->passRunLB(*eventInfo)) return EL::StatusCode::SUCCESS; // go to next event
  }

  const MuonContainer* offmuons=0;
  const MuonRoIContainer* L1muons=0;
  const L2StandAloneMuonContainer* SAmuons=0;
  const L2CombinedMuonContainer* COMBmuons=0;
  const MuonContainer* EFmuons=0;
  m_event->retrieve(offmuons, "Muons");
  m_event->retrieve(L1muons, "LVL1MuonRoIs");
  m_event->retrieve(SAmuons, "HLT_xAOD__L2StandAloneMuonContainer_MuonL2SAInfo");
  m_event->retrieve(COMBmuons, "HLT_xAOD__L2CombinedMuonContainer_MuonL2CBInfo");
  m_event->retrieve(EFmuons, "HLT_xAOD__MuonContainer_MuonEFInfo");
  MuonContainer::const_iterator offmuon_itr = offmuons->begin();
  MuonContainer::const_iterator offmuon_begin = offmuons->begin();
  MuonContainer::const_iterator offmuon_end = offmuons->end();

  float offmuon_pt, offmuon_eta, offmuon_phi, offmuon_charge;
  vector<float> offline_segment_r,offline_segment_z,offline_segment_eta,offline_segment_phi;
  vector<int> offline_segment_chamber;
  const float muon_mass = 0.1056;//GeV

  for ( ;offmuon_itr<offmuon_end; ++offmuon_itr){
    if ((*offmuon_itr)->author()!=1 ) continue;//Require MuidCo
    if ((*offmuon_itr)->muonType()!=0 ) continue;//Require Combined
    if ((*offmuon_itr)->quality()>1.5 ) continue;//Require tight or medium quality
    offmuon_pt = (*offmuon_itr)->pt()/1000.;//GeV
    offmuon_eta = (*offmuon_itr)->eta();
    if (fabs(offmuon_eta)>2.5) continue; //eta cut
    offmuon_phi = (*offmuon_itr)->phi();
    offmuon_charge = (*offmuon_itr)->charge();
    offline_segment_r.clear();offline_segment_z.clear();offline_segment_chamber.clear();
    offline_segment_eta.clear();offline_segment_phi.clear();
    int nMuonSegments = (*offmuon_itr)->nMuonSegments();
    for (int seg=0; seg<nMuonSegments; seg++){
      auto elementLink = (*offmuon_itr)->muonSegmentLink(seg);
      float x = (*elementLink)->x();
      float y = (*elementLink)->y();
      float z = (*elementLink)->z();
      float r = sqrt(x*x+y*y);
      float phi = atan(y/x);
      if (x<0 && y>0) phi = phi + PI;
      if (x<0 && y<0) phi = phi - PI;
      float l = sqrt(z*z+r*r);
      float tan = sqrt((l-z)/(l+z));
      float eta = -log(tan);
      int chamber = (*elementLink)->chamberIndex();
      offline_segment_z.push_back(z);
      offline_segment_r.push_back(r);
      offline_segment_eta.push_back(eta);
      offline_segment_phi.push_back(phi);
      offline_segment_chamber.push_back(chamber);
    }

    if (offmuon_pt<4) continue;

    //////L1
    MuonRoIContainer::const_iterator roi_itr = L1muons->begin();
    MuonRoIContainer::const_iterator roi_end = L1muons->end();
    bool passL1 = false;
    float minDrL1=1000.;
    int min_roi_thrnum=-1;
    int matchRoiNum=-1;
    float m_L1_eta,m_L1_phi;
    for (;roi_itr!=roi_end;++roi_itr){
      float roi_eta = (*roi_itr)->eta();
      float  roi_phi = (*roi_itr)->phi();
      float dr = m_util.calcDr(roi_eta,offmuon_eta,roi_phi,offmuon_phi);
      if (dr<minDrL1){
        minDrL1 = dr;
        matchRoiNum = (*roi_itr)->getRoI();
        min_roi_thrnum=(*roi_itr)->getThrNumber();
        m_L1_eta=roi_eta;
        m_L1_phi=roi_phi;
      }
    }
    if (minDrL1<m_util.dRL1byPt(offmuon_pt) && min_roi_thrnum>=1) passL1=true;
    if (!passL1) continue;
    if(doDebug) cout << "L1 pass" << endl;
    if(doDebug) cout << "match RoI number=" << matchRoiNum << endl;

    ////////SA
    auto fc = trigDecTool->features("HLT_mu4",TrigDefs::alsoDeactivateTEs);
    auto fcSA = fc.containerFeature<xAOD::L2StandAloneMuonContainer>("",TrigDefs::alsoDeactivateTEs);
    bool matchSA=false;
    float m_L2_pt,m_L2_eta,m_L2_phi,m_L2_ec_alpha,m_L2_ec_beta,m_L2_ec_radius,m_L2_br_radius,m_L2_etaMap,m_L2_phiMap;
    float m_tgcInn_eta,m_tgcInn_phi,m_tgcMid1_r,m_tgcMid1_z,m_tgcMid1_eta,m_tgcMid1_phi,m_tgcMid2_r,m_tgcMid2_z;
    int m_L2_charge,m_L2_saddress;
    vector<float> m_sp_r,m_sp_z;
    m_sp_r.clear();m_sp_z.clear();
    for (auto &fcsa : fcSA){
      const HLT::TriggerElement* tesa = ( fcsa.te() );
      const L2StandAloneMuonContainer* contsa = fcsa.cptr();
      for (auto samuon : *contsa) {
        int sa_roinum = samuon->roiNumber();
        float sa_pt = fabs(samuon->pt());
        float sa_eta = samuon->eta();
        float sa_phi = samuon->phi();
        int sa_charge = (samuon->pt()>0)? 1 : -1;
        if (sa_roinum==matchRoiNum) {
          matchSA=true;
          m_L2_pt = fabs(samuon->pt());
          m_L2_eta = samuon->eta();
          m_L2_phi = samuon->phi();
          m_L2_charge = (samuon->pt()>0)? 1 : -1;
          m_L2_saddress = samuon->sAddress();
          m_L2_ec_alpha = samuon->endcapAlpha();
          m_L2_ec_beta = samuon->endcapBeta();
          m_L2_ec_radius = samuon->endcapRadius();
          m_L2_br_radius = samuon->barrelRadius();
          m_L2_etaMap = samuon->etaMap();
          m_L2_phiMap = samuon->phiMap();
          m_tgcInn_eta = samuon->tgcInnEta();
          m_tgcInn_phi = samuon->tgcInnPhi();
          m_tgcMid1_r = samuon->tgcMid1R();
          m_tgcMid1_z = samuon->tgcMid1Z();
          m_tgcMid1_eta = samuon->tgcMid1Eta();
          m_tgcMid1_phi = samuon->tgcMid1Phi();
          m_tgcMid2_r = samuon->tgcMid2R();
          m_tgcMid2_z = samuon->tgcMid2Z();
          for(int ch=0;ch<7;ch++){
            m_sp_r.push_back(samuon->superPointR(ch));
            m_sp_z.push_back(samuon->superPointZ(ch));
          }
        }
      }
    }

    if(!matchSA) continue;
    if(doDebug) cout << "Saved to output ntuple!" << endl;

    offline_pt = offmuon_pt;
    offline_eta = offmuon_eta;
    offline_phi = offmuon_phi;
    offline_charge = offmuon_charge;
    L1_eta = m_L1_eta;
    L1_phi = m_L1_phi;
    L2_pt = m_L2_pt;
    L2_eta = m_L2_eta;
    L2_phi = m_L2_phi;
    L2_charge = m_L2_charge;
    L2_saddress = m_L2_saddress;
    L2_ec_alpha = m_L2_ec_alpha;
    L2_ec_beta = m_L2_ec_beta;
    L2_ec_radius = m_L2_ec_radius;
    L2_br_radius = m_L2_br_radius;
    L2_etaMap = m_L2_etaMap;
    L2_phiMap = m_L2_phiMap;
    tgcInn_eta = m_tgcInn_eta;
    tgcInn_phi = m_tgcInn_phi;
    tgcMid1_r = m_tgcMid1_r;
    tgcMid1_z = m_tgcMid1_z;
    tgcMid1_eta = m_tgcMid1_eta;
    tgcMid1_phi = m_tgcMid1_phi;
    tgcMid2_r = m_tgcMid2_r;
    tgcMid2_z = m_tgcMid2_z;
    sp_r.clear();sp_z.clear();
    for(int ich=0;ich<7;ich++){
      sp_r.push_back(m_sp_r[ich]);
      sp_z.push_back(m_sp_z[ich]);
    }
    segment_r.clear();segment_z.clear();
    segment_eta.clear();segment_phi.clear();
    segment_chamber.clear();
    for (int seg=0;seg<nMuonSegments;seg++){
      segment_r.push_back(offline_segment_r[seg]);
      segment_z.push_back(offline_segment_z[seg]);
      segment_eta.push_back(offline_segment_eta[seg]);
      segment_phi.push_back(offline_segment_phi[seg]);
      segment_chamber.push_back(offline_segment_chamber[seg]);
    }
    tree->Fill();

  }//loop of offline muon1

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisData :: postExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisData :: finalize ()
{
  cout << "finalize" << endl;

  //cout << "Final trigger tally" << endl;
  //for (auto &i : triggerCounts) cout << "  " << i.first << ": " << i.second << " times" << endl;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisData :: histFinalize ()
{
  cout << "histFInalize" << endl;
  return EL::StatusCode::SUCCESS;
}


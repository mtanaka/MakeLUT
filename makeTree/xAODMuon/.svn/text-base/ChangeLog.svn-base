2015-12-08 Will Leight
	* More self-explanatory variable names
	* Tagging as xAODMuon-00-17-04

2015-12-06 Will Leight
	* Adding new muon variables for close hits, out-of-bounds hits, and non-deweighted layers
	* Tagging as xAODMuon-00-17-03

2015-11-25 Edward.Moyse@cern.ch
	* Schema migrated xAODMuon, moving varcone* to Aux (i.e. no longer dynamic)
	* Tagging as xAODMuon-00-17-02

2015-09-17 Edward.Moyse@cern.ch
	* Fixed bug where the incorrect number of bits were zeroed in setQuality()
	* Tagging as xAODMuon-00-16-03

2015-09-15 Edward.Moyse@cern.ch
	* Fixed m_p4Cached1 not being reset when setP4 called
	* Tagging as xAODMuon-00-16-02

2015-09-03 Edward.Moyse@cern.ch
	* Fixed Accessors
	* Tagging as xAODMuon-00-16-01

2015-08-20 Edward.Moyse@cern.ch
	* Added nearbyhit counts for Inner/Middle/Outer
	* Documentation cleanup.
	* Tagging as xAODMuon-00-16-00

2015-07-29 Edward.Moyse@cern.ch
	* Possible fix for ATLASRECTS-2205.

2015-06-10 Edward.Moyse@cern.ch
	* Ensure that the quality is zeroed first in setQuality()
	* Tagging as xAODMuon-00-15-06

2015-06-05 Will Buttinger <will@cern.ch>
        * Root/xAODMuonCLIDs.cxx : added to get the CLASS_DEF stuff properly set up at compile time (add clid to clid.db etc)
        * Tagging as xAODMuon-00-15-05

2015-05-27 Edward.Moyse@cern.ch
	* General overhaul of return methods to avoid routinely throwing exceptions:
        - for methods which returns by pointer and where the accessor might not exist, use isAvailable(...) to check if the accessor is usable  
	- however for methods which return an elementlink, it is the responsibility of the client to make sure the link is valid.
	* Tagging xAODMuon-00-15-04

2015-05-19 Edward.Moyse@cern.ch
	* First attempt at fixing ATR-10738 (don't allow exceptions to be thrown in access methods).
	* Tagging xAODMuon-00-15-03

2015-03-22  scott snyder  <snyder@bnl.gov>

	* Tagging xAODMuon-00-15-02.
	* Root/Muon_v1.cxx: Fix int->string conversion.

2015-03-18 Edward.Moyse@cern.ch
	* fix allAuthors() documentation

2015-03-05 Edward.Moyse@cern.ch
	* (re)fixing a nasty bug which returns the wrong link to a trackparticle. 
	* ATR-10387 (and ATLASRECTS-1784)
	* Tagging xAODMuon-00-15-01

2015-02-17 Edward.Moyse@cern.ch
	* Muon_v2 - move ParamDefs to aux store, and clean up charge() etc
	* Tagging xAODMuon-00-15-00	

2014-12-21 Jcohen Meyer
	* addressing coverity defects 13169-70
	* Tagging xAODMuon-00-14-02

2014-12-15 Edward.Moyse@cern.ch
	* Added missing int paramdef methods (https://its.cern.ch/jira/browse/ATR-9706 and ATLASRECTS-1434))
	* Updated test, and made a few changes to remove unnecessary Muon_v1.
	* Tagging xAODMuon-00-14-01

2014-12-05  Massimiliano Bellomo  <massimiliano.bellomo@cern.ch>
	* Muon_v1.h added ParamsDef for measured energy loss
        * requirements fixing compiler warning
	* Tagging xAODMuon-00-13-02

2014-12-03 Edward.Moyse@cern.ch
	* Add new methods for isolation corrections
	* Cleaned up some defunct isolation types from the Aux store
	* Tagging xAODMuon-00-13-01

2014-11-27 Heberth Torres <htorres@cern.ch>
	* Added setter for muonLink in the SlowMuon
	* Tagging xAODMuon-00-12-02

2014-11-18 Edward.Moyse@cern.ch
	* Fixed typo
	* Tagging xAODMuon-00-12-01

2014-11-17 Edward.Moyse@cern.ch
        * Moved the Muon CLIDs to versionless (this will break Muon configurations and so should only be accepted as part of a bundle)
        * Tagging xAODMuon-00-12-00

2014-11-14 Heberth Torres <htorres@cern.ch>
	* Moved the SlowMuon CLID's to the versionless configuration
	* Tagging xAODMuon-00-11-04
	
2014-11-14 Heberth Torres <htorres@cern.ch>
	* Added CLID for xAOD::SlowMuon_v1
	* Added SlowMuon to selection.xml
	* Tagging xAODMuon-00-11-03

2014-11-14 Heberth Torres <htorres@cern.ch>
	* Added dictionaries for SlowMuon
	* Tagging xAODMuon-00-11-02

2014-11-13 Heberth Torres <htorres@cern.ch>
	* Modified the SlowMuon object
	* Tagging xAODMuon-00-11-01

2014-11-12 Heberth Torres <htorres@cern.ch>
	* Added preliminary version of the SlowMuon object
	* Tagging xAODMuon-00-11-00
	
2014-10-31 Edward.Moyse@cern.ch
	* Make charge a static
	* Tagging xAODMuon-00-10-00

2014-10-23 will@cern.ch
        * cmt/requirements: Put back in the use_ifndef pattern to stop the compile warnings in athanalysisbase
        * left untagged so that it can be collected in next tag (no rush to get this fixed basically)

2014-10-15 Edward.Moyse@cern.ch
	* Added some new methods at client request, so information can be accessed from Muon
	* Added new 'VeryLoose' quality enum
	* Tagging xAODMuon-00-09-02

2014-10-10 Edward.Moyse@cern.ch
	* Add some CaloTag quantities, and fixed a few problems with setParameters(...)
	* Tagging xAODMuon-00-09-00

2014-09-16 Edward.Moyse@cern.ch
	* Add protection for missing accessors
	* Tagging xAODMuon-00-08-02

2014-08-15 Heberth Torres <htorres@cern.ch>
	* Enable support for Athena Analysis Releases (MANA)
	* Tagging xAODMuon-00-08-01

2014-07-15 Heberth Torres <htorres@cern.ch>
	* Added muon parameter meanDeltaADCCountsMDT
	* Tagging xAODMuon-00-07-02

2014-06-26 Edward.Moyse@cern.ch
  * Adding unit test (doesn't do much yet)
  * Tagging xAODMuon-00-07-01

2014-06-23 Edward.Moyse@cern.ch
  * Adding charge() method
  * Tagging xAODMuon-00-06-00

2014-06-20 Edward.Moyse@cern.ch
  * Attempt to fix problem with exception being thrown when using MuonSelectionTool.

2014-06-17 Edward.Moyse@cern.ch
  * Update to authors (clean out cruft, and fit within 16bit)
  * Tagging xAODMuon-00-05-01

2014-05-27 Edward.Moyse@cern.ch
  * New setP4 method.
  * Pack passesIDCuts & passesHighPt into 'quality' uint8_t, plus setters and getters.
  * Tagging xAODMuon-00-05-00

2014-05-11  scott snyder  <snyder@bnl.gov>

	* Tagging xAODMuon-00-04-03.
	* Root/Muon_v1.cxx (p4): Bug fix obo Tulay.

2014-05-09 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* xAOD::Muon_v1::energyLossType finally uses uint8_t as the type
	  for the persistent variable.
	* Apparently variable is never filler during reconstruction,
	  otherwise it would've been found already as an issue. :-/
	* Tagging as xAODMuon-00-04-02

2014-05-07 Edward.Moyse@cern.ch
  * Typo with the segmentlinks type
  * Tagged xAODMuon-00-04-01

2014-04-29 Edward.Moyse@cern.ch
	* Muon: Make segment links static.
	  * Renamed methods to follow new convention (elementlinks -> xxxLink, bare pointers -> xxx)
	* Tagged xAODMuon-00-04-00

2014-04-25 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Fixed the updated ElementLink handling functions. We can't use
	  the AuxStore macros for these getter functions anymore, since the
	  function names are not the same as the underlying decoration's
	  name.
	* Added a Muon_v1::cluster() function for returning a bare pointer
	  for the associated calo cluster.
	* Made all the bare pointer returning functions return null pointers
	  in case the ElementLink is invalid.
	* Made all the bare pointer returning functions use the ElementLink
	  returning functions internally. To avoid the terrible code
	  duplication that was in the package before.
	* Made the RootCore compilation depend on MuonIdHelpers.
	* Tagging as xAODMuon-00-03-01

2014-04-24 Edward.Moyse@cern.ch
	* Muon_v1: don't hide MuonSegment links section any more in standalone

2014-04-23 Edward.Moyse@cern.ch
	* MuonSegment: Change interface to use enums where possible (and added dependency on MuonIdHelpers)

2014-04-23 Edward.Moyse@cern.ch
	* Change interface to use new conventions for links

2014-04-15 Edward.Moyse@cern.ch
	* Remove templated accessor methods
        * Tagging as xAODMuon-00-03-00

2014-04-08 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Could remove some of the exclusion rules after the recent
	  bugfixes in rootcling, but not all of them.
	* The package no longer needs to generate a dictionary for
	  DataVector<xAOD::IParticle> in order to generate the muon
	  dictionaries correctly.
	* Tagging as xAODMuon-00-02-16

2014-04-06 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Extended the exclusion list in selection.xml a little more.
	  By now the package only generates an unnecessary dictionary
	  for DataVector<xAOD::IParticle>, but that's unavoidable with
	  v5-99-06. (If we suppress that class, the selection rule for
	  DataVector is not taken into account.)
	* Tagging as xAODMuon-00-02-15

2014-04-05 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Changed the name of GCCXML_DUMMY_INSTANTIATION to
	  a more unique name in the dictionary header. Was necessary,
	  as rootcling is complaining about seeing the same struct name
	  in multiple libraries. (Which really shouldn't clash actually,
	  but let's do this until ROOT 6 stops complaining about this.)
	* Tagging as xAODMuon-00-02-14

2014-04-05 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Making the dictionary generation work correctly with ROOT 6.
	* Temporarily excluding a number of classes in selection.xml
	  until rootcling learns not to want to generate a dictionary for
	  every class that it encounters. :-P
	* Got rid of a warning in MuonAccessors_v1.icc about an unused
	  function parameter.
	* Tagging as xAODMuon-00-02-13

2014-03-28 Heberth Torres <htorres@cern.ch>
	* Added vectors for the muon isolations in MuonAuxContainer
	* Tagging as xAODMuon-00-02-12

2014-03-27 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Declaring the dependency on xAODPrimitives for RootCore
	  as well.
	* Tagging as xAODMuon-00-02-11

2014-03-25 Heberth Torres <htorres@cern.ch>
	* Updated to use common IsolationType and getIsolationAccessor
	* Tagging as xAODMuon-00-02-10

2014-03-21 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added dictionaries for vector<vector<EL<...> > > versions
	  of ElementLinks pointing at muons and muon segments.
	* Tweaked the ignore flags for cmt/.
	* Tagging as xAODMuon-00-02-09

2014-03-14 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Changed the implementation of Muon_v1::quality and
	  Muon_v1::setQuality to explicitly convert between
	  Muon_v1::Quality and uint8_t.
	* Changed the name of Muon_v1::m_p4Cached to Muon_v1::m_p4Cached1.
	  For some wicked reason ROOT gets confused about the read
	  rule defined in selection.xml in standalone mode when using the
	  previous variable name. But if I use an alternative name, then
	  all seems to be at peace. This will have to be followed up
	  when we get the time...
	* Tagging as xAODMuon-00-02-08

2014-03-14 Edward.Moyse@cern.ch
  * Reverted Attila's changes related to Athena-only dictionaries (the dictionary is already shifted to TrkSegment, or should be)
  * Removed EL<SegmentCollection> from the MuonSegment Aux Store
  * Tagging as xAODMuon-00-02-07	

2014-03-14 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Fixing the RootCore compilation once more. Had to hide
	  Trk::SegmentCollection from the standalone compilation consistently.
	* For now the ElementLink<Trk::SegmentCollection> dictionary is moved
	  into an Athena-only dictionary within this package. But it should
	  really just be moved into the TrkSegment package...
	* Enabled keyword expansion on the source files.
	* Tagging as xAODMuon-00-02-06

2014-03-10 Edward.Moyse@cern.ch
  * Adding missing dictionaries for MuonSegment
  * Tagging as xAODMuon-00-02-05

2014-03-06 Heberth Torres <htorres@cern.ch>
  * Changing nMDTHits, nCSCHits, nRPCHits, nTGCHits by nPrecisionHits, nPhiLayers, nTrigEtaLayers
  * Tagging as xAODMuon-00-02-03

2014-02-19 Niels van Eldik
	* MuonSegment: adding identifier
	* Tagging as xAODMuon-00-02-02

2014-02-18 Niels van Eldik
	* MuonSegment: adding element link to ESD segment
	* Muon: add element link to xAOD segment
	* Tagging as xAODMuon-00-02-01

2014-02-18 Edward.Moyse@cern.ch
  * Adding MuonSegment to dict and selection.xml
  * Tagging as xAODMuon-00-02-00 (Adding segment shouldn't increment just the bugfix number)

2014-02-14 Heberth Torres <htorres@cern.ch>
  * making a proxy for MuonSegmentContainer
  * changing numberDoF from int to float (to be check later)
  * Tagging as xAODMuon-00-01-17

2014-02-14 Niels van Eldik
  * adding MuonSegment
  * Tagging as xAODMuon-00-01-16

2014-02-13 Niels van Eldik
  * extend MuonHitSummary Info
  * Tagging as xAODMuon-00-01-15

2014-02-12 Niels van Eldik
  * add MuonHitSummary Info
  * Tagging as xAODMuon-00-01-14

2014-02-07 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
  * Made the package compile with RootCore.
  * Just had to move the source files, and hard-code the
    EIGEN_DONT_VERTORIZE option for the dictionary generation.
  * Tagging as xAODMuon-00-01-13

2014-01-27 Edward.Moyse@cern.ch
  * Don't store Muon_v1 in names...
  * Tagging as xAODMuon-00-01-12

2014-01-30 Edward.Moyse@cern.ch
  * Added matching chi2/DOF to Paramdefs and accessor helper
  * Tagging as xAODMuon-00-01-11

2014-01-27 Niels van Eldik
  * fix bug in accessor of InDetTP
  * Tagging as xAODMuon-00-01-10

2014-01-14 Edward.Moyse@cern.ch
  * Updated for move to xAODTracking
  * Tagging as xAODMuon-00-01-09

2013-12-19 Edward.Moyse@cern.ch
  * Code cleanup (+ minor things for Eigen)
  * Tagging as xAODMuon-00-01-07

2013-12-10 Edward.Moyse@cern.ch
  * Using the new xAODCore macros and removed the private ones here
  * Tagging as xAODMuon-00-01-06

2013-12-09 Edward.Moyse@cern.ch
  * Use new macros to simplify implementation
  * Remove mass from aux store (hardcoding for the moment)
  * Adding MuonAccessors. 
  * Commenting out MuonSegment method (compilation fails because it doesn't find Muon::MuonSegment, and I can't work out why)
  * Fix for setEL methods
  * Tagging as xAODMuon-00-01-05

2013-12-06 Edward.Moyse@cern.ch
  * Fixes for dictionary (which now works)
  * Implemented the summaryValue method
  * Tagging as xAODMuon-00-01-04

2013-12-04 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
  * Made the code work in standalone mode.
  * Replaced the wscript file with the "new type" hscript.yml one.
  * Hid the implementation of the Muon_v1::muonSegments() function
    in standalone mode.
  * Tagging as xAODMuon-00-01-03

2013-12-04  Edward.Moyse@cern.ch
  * Misc fixes
  * Tagging xAODMuon-00-01-02

2013-12-02      Edward.Moyse@cern.ch
        * Started work on proper implementation. Changes to interface following feedback.
        * Tagging xAODMuon-00-01-01

2013-11-08      Edward.Moyse@cern.ch
        * First import
        * Tagging xAODMuon-00-00-00

# echo "cleanup xAODMuon xAODMuon-00-17-04 in /home/mtanaka/Event/xAOD"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtxAODMuontempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtxAODMuontempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=xAODMuon -version=xAODMuon-00-17-04 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory $* >${cmtxAODMuontempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=xAODMuon -version=xAODMuon-00-17-04 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory $* >${cmtxAODMuontempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtxAODMuontempfile}
  unset cmtxAODMuontempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtxAODMuontempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtxAODMuontempfile}
unset cmtxAODMuontempfile
exit $cmtcleanupstatus


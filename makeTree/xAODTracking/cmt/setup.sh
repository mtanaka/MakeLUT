# echo "setup xAODTracking xAODTracking-00-13-21 in /home/mtanaka/Event/xAOD"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtxAODTrackingtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtxAODTrackingtempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=xAODTracking -version=xAODTracking-00-13-21 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODTrackingtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=xAODTracking -version=xAODTracking-00-13-21 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODTrackingtempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtxAODTrackingtempfile}
  unset cmtxAODTrackingtempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtxAODTrackingtempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtxAODTrackingtempfile}
unset cmtxAODTrackingtempfile
return $cmtsetupstatus


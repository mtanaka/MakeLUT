2015-12-08 Will Leight
	* Added new muon variables to enums in TrackingPrimitives.h, also a new accessor for one variable in TrackSummaryAccessors_v1.cxx
	* New variables: # of non-deweighted precision layers, # of precision close hits, # of precision out-of-bounds hits, origin (Endcap/Barrel or Large/Small) of stations that actually contribute to the track fit
	* Tagged as xAODTracking-00-13-21

2015-10-16 Christian Ohm
	* Tweaked the name 'LargeD0' => 'SiSpacePointsSeedMaker_LargeD0'
	* Tagged as xAODTracking-00-13-20

2015-10-07 Christian Ohm
	* Added 'LargeD0' value to the TrackPatternRecoInfo enum
	* Tagged as xAODTracking-00-13-19

2015-10-02 Goetz Gaycken
	* Resolved 
	 - ATLASRECTS-2385: removing of unused electron probabilites
	 - ATLASRECTS-2043: Addition of TRT total number of high threshold
	   hits
	 - ATLASRECTS-2045: Addition of number of hits used for TRT dE/dx
	   computation.

2015-08-18 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* As it turns out, the previous implementation was quite buggy.
	  It broke the reading of the phi variable, and even the new
	  variable was not read all that correctly from new AOD files.
	* Updated the implementation according to Scott's suggestion
	  on https://its.cern.ch/jira/browse/ATLASRECTS-2315.
	* Tagging as xAODTracking-00-13-17

2015-08-04 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added explicit schema evolution code to the ROOT dictionary to
	  read the newly added
	  xAOD::TrackParticleAuxContainer_v2::numberOfDBMHits correctly
	  from files that don't have this variable.
	* The exact implementation may need to be changed, the number of
	  hits is now set to 0 in case the file didn't have this info.
	* Tagging as xAODTracking-00-13-16

2015-06-24 Goetz Gaycken
  * Added missing inline keywords for TrackParticle helper methods.

2015-06-16 Nick Styles
  * Add number of DBM hits to track summary
  * Tagged asxAODTracking-00-13-14 

2015-05-28 Goetz Gaycken
  * added helper to compute the d0 significance which takes the width of the
    beamspot into account.

2015-05-19 will buttinger <will@cern.ch>
		* Added Root/xAODTrackingCLIDs.cxx and line to requirements file to ensure all CLIDs are added to the clid.db at compile time
	* Tagged as xAODTracking-00-13-12

2015-04-14 Edward.Moyse@cern.ch
  * Coverity fixes for : 18324, 12240
  * Tagging xAODTracking-00-13-11

2015-03-21  scott snyder  <snyder@bnl.gov>

	* Tagging xAODTracking-00-13-10.
	* test/xAODTracking_TrackParticlexAODHelpers_test.cxx (testPtErr):
	Fix self-assignment (fix clang warning).

2015-03-06 Edward.Moyse@cern.ch
  * Coverity fixes for: 17422, 17421
  * Tagged as xAODTracking-00-13-09

2015-02-04 Edward.Moyse@cern.ch
  * Removed debug statements
  * ATLASRECTS-1640
  * Tagged as xAODTracking-00-13-08

2015-02-10 Nick Styles
  * Fix for perigees to use tilted surface if tilts available
  * Tagged as xAODTracking-00-13-07

2015-02-04 Edward.Moyse@cern.ch
  * Coverity fixes
  * Tagged as xAODTracking-00-13-06

2015-01-30 Nick Styles
  * Add TRT shared hits to summary
  * Tagged as xAODTracking-00-13-05 

2015-01-21 Nick Styles
  * Add protection for getting number of track parameters to avoid crashes running on ESD
  * Tagged as xAODTracking-00-13-04

2014-12-04 Peter van Gemmeren  <gemmeren@anl.gov>
	* Clean selection.xml to avoid unnecessary loading of Dict in ROOT6
	* Tagging as xAODTracking-00-13-03

2014-12-04 Nick Styles
  * Correct accessor name inconsistency in Summary info
  * Tagged as xAODTracking-00-13-02

2014-12-04 Anthony Morley
  * Correct accessor name inconsistency in TrackStateValidation
  * Tagged as xAODTracking-00-13-01

2014-12-04 Anthony Morley
  * Class name changes
   xAOD::PrepRawData-> xAOD::TrackMeasurementValidation
   xAOD::MeasurementStateOnSurface-> xAOD::TrackStateValidation
  * Tagged as xAODTracking-00-13-00

2014-11-27 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Prevented ROOT 6 from generating a dictionary for
	  xAOD::Type::ObjectType in this package.
	* Fixed the logic errors in TrackParticlexAODHelpers.h that
	  Marcin identified.
	* Modified the assignment operator of xAOD::TrackParticle_v1
	  to work on an object that's already part of a container.
	  (To optimise deep copying a bit.)
	* Tagging as xAODTracking-00-12-07

2014-11-26  Marcin Nowak  <Marcin.Nowak@cern.ch>
	* merged in branch tag xAODTracking-00-12-03-02
	(excluded NeutralParticle::m_perigeeParameters from ROOT6 dictionary)
	* Tagged as xAODTracking-00-12-06
	
	* merged in branch tag xAODTracking-00-12-03-01
	* Tagged as xAODTracking-00-12-05

2014-11-25 Nick Styles
	* Add nHits dEdX
	* Tagged as xAODTracking-00-12-04

2014-11-26  Marcin Nowak  <Marcin.Nowak@cern.ch>
	* xAODTracking\versions\TrackParticle_v1.h:
	exclude TrackParticle::m_perigeeParameters from ROOT6 dictionary 
	* Tagged as xAODTracking-00-12-03-01

2014-11-14 Nick Styles
	* Add hit pattern
	* Tagged as xAODTracking-00-12-03

2014-11-13 Nick Styles
	* Add pixel dEdX info
	* Tagged as xAODTracking-00-12-02

2014-11-12 Nick Styles
        * Fix typo in naming
        * Tagged as xAODTracking-00-12-01

2014-11-12 Nick Styles
        * Removing BLayer info items from Aux info (interfaces remain, and will return InnermostPixelLayer info items)
        * Tagged as xAODTracking-00-12-00

2014-11-11 Nick Styles
        * Adding tilts for beam line
        * Tagged as xAODTracking-00-11-06

2014-10-30 Nick Styles
        * Yet more fixes to migrate CLID numbers to versionless typedefs
	* Fix to root access of new summary info
        * Tagged as xAODTracking-00-11-05

2014-10-29 Edward.Moyse@cern.ch
        * More fixes to migrate CLID numbers to versionless typedefs
        * Tagged as xAODTracking-00-11-04

2014-10-27 Nick Styles
	* Adding new accessors
	* Tagged as xAODTracking-00-11-03
	
2014-10-24 Edward.Moyse@cern.ch
	* Migrate CLID numbers to versionless typedefs
	* Tagged as xAODTracking-00-11-02

2014-10-23 Edward.Moyse@cern.ch
	* Added missing files
        * Tagged as xAODTracking-00-11-01

2014-10-22 Edward.Moyse@cern.ch
	* First attempts to do v2 - add radiusOfFirstHit and identifierOfFirstHit, and make other parameters dynamic
	* Tagged as xAODTracking-00-11-00

2014-09-16 Edward.Moyse@cern.ch
	* Changes to make it easier to use in Analysis releases (requested by Anthony)
	* Tagged as xAODTracking-00-10-03

2014-09-10 Nick Styles
	* Added extra info to summary (to replace b-layer hits)
	* Tagged as xAODTracking-00-10-02

2014-09-04 David Quarrie <David.Quarrie@cern.ch>
	* cmt/requirements
		Specify the required ROOT components for cmake (transparent to CMT)
	* Tagged as xAODTracking-00-10-01
	
2014-09-05 Edward.Moyse@cern.ch
	* Changes from Anthony
	* Tagging as xAODTracking-00-10-00

2014-09-02 Edward.Moyse@cern.ch
	* Fixed problem with ContainerProxies for new MeasurementStateOnSurface classes.
	* Tagging as xAODTracking-00-09-01

2014-09-01 Heberth Torres <htorres@cern.ch>
	* Added MeasurementStateOnSurface classes.
	* Tagging as xAODTracking-00-09-00

2014-08-26 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Fixed the package to compile in standalone mode.
	* Removed ParticleCaloExtension.h's dependence on two outdated
	  packages (DataModel, CLIDSvc), and replaced the includes with
	  headers from xAODCore. (Needs a recent xAODCore to work.)
	* At the same time, removed the dependence on SGTools all
	  together, and only use the CLASS_DEF.h and BaseInfo.h headers
	  from xAODCore now.
	* Updated the requirements file accordingly, but didn't actually
	  test the Athena compilation. :-(
	* Tagging as xAODTracking-00-08-02

2014-08-25 Niels van Eldik
	* add covariance matrices
	* Tagging as xAODTracking-00-08-01

2014-08-25 Niels van Eldik
	* Rename TrackParticleCaloExtension -> ParticleCaloExtension
	* add collection to avoid leaks
	* Tagging as xAODTracking-00-08-00
	
2014-08-19 Nick Styles
	* Dictionaries for enums and CurvilinearParameters_t
	* Tagging as xAODTracking-00-07-08
	
2014-08-18 Niels van Eldik
	* add protection against missing perigee covariancea
	* Tagging as xAODTracking-00-07-07

2014-08-14 Heberth Torres <htorres@cern.ch>
	* Updated requirements file for MANA
	* Tagging as xAODTracking-00-07-06

2014-08-10 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Adding support for neutrals into VertexAuxContainer_v1
	* Tagging as xAODTracking-00-07-05

2014-08-05 Olivier Arnaez <olivier.arnaez@cern.ch>
	* Adding support for neutrals into Vertex_v1
	* Tagging as xAODTracking-00-07-04

2014-08-01 Heberth Torres <htorres@cern.ch>
	* Enable support for Athena Analysis Releases (MANA)
	* Tagging as xAODTracking-00-07-03

2014-07-22  Niels van Eldik
	* add TrackParticleCaloExtension as normal object, to be transformed into xAOD object
	* Tagging as xAODTracking-00-07-02

2014-07-21  scott snyder  <snyder@bnl.gov>

	* Tagging xAODTracking-00-07-01.
	* xAODTracking/versions/Vertex_v1.h: Use SG_BASE to declare that
	Vertex_v1 derives from AuxElement.

2014-07-15 Edward.Moyse@cern.ch
    * Added copy ctors and assignment operators and updated unit test
    * Tagging as xAODTracking-00-07-00

2014-07-04  Niels van Eldik
	* finish xAOD::PrepRawData dics
	* Tagging as xAODTracking-00-06-03

2014-07-01  scott snyder  <snyder@bnl.gov>

	* Tagging xAODTracking-00-06-02.
	* xAODTracking/selection.xml, xAODTracking/xAODTrackingDict.h: Add
	dictionary entries for PrepRawData_v1, PrepRawDataContainer_v1.

2014-06-26 Heberth Torres <htorres@cern.ch>
	* Changed names of the PrepRawData variables
	* Tagging as xAODTracking-00-06-01

2014-06-26 Heberth Torres <htorres@cern.ch>
	* Added PrepRawData classes.
	* Tagging as xAODTracking-00-06-00

2014-05-27 Edward.Moyse@cern.ch
	* Make TrackSummary accessors accessible from other package (needed by new xAODMuon)
	* Tagging as xAODTracking-00-05-00

2014-05-21 Nick Styles <nicholas.styles -at- cern.ch>
	* Added new enums to TrackingPrimitives
	* Tagging as xAODTracking-00-04-04

2014-05-14 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Updated the names of the variables in the auxiliary store
	  classes to follow the new naming convention. (This was a big
	  oversight not to do this long before.)
	* This actually simplified the code quite a bit.
	* The class interfaces didn't change because of this.
	* Tagging as xAODTracking-00-04-03

2014-05-09 Heberth Torres <htorres@cern.ch>
	* Added protection for qOverP==0, in TrackParticle_v1::p4()

2014-05-05 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Corrected the code for the TrackParticle link functions to
	  behave correctly with the current auxiliary classes.
	* Updated the ElementLink handling code in xAOD::Vertex to
	  follow the new naming convention.
	* The variables of xAOD::Vertex are now declared transient
	  in selection.xml instead of trying to hide them in the code.
	  (Which was not working with ROOT 6.) This was a hack made
	  for some reason that I can't quite remember. But both ROOT 5
	  and ROOT 6 compile the proper code correctly, so let's do
	  things by the book now.
	* Interactive usage seems to be in order, correctly reading "old"
	  files.
	* Tagging as xAODTracking-00-04-01

2014-04-23 Edward.Moyse@cern.ch
	* Change interface to use new conventions for links

2014-04-15 Edward.Moyse@cern.ch
	* Change interface to remove templated summary accessor functions
	* Tagging as xAODTracking-00-04-00

2014-04-08 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Could remove some of the exclusion rules after the recent
	  bugfixes in rootcling, but not all of them.
	* The package no longer needs to generate a dictionary for
	  DataVector<xAOD::IParticle> in order to generate the tracking
	  dictionaries correctly.
	* Tagging as xAODTracking-00-03-10

2014-04-06 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Extended the exclusion list in selection.xml a little more.
	  By now the package only generates an unnecessary dictionary
	  for DataVector<xAOD::IParticle>, but that's unavoidable with
	  v5-99-06. (If we suppress that class, the selection rule for
	  DataVector is not taken into account.)
	* Tagging as xAODTracking-00-03-09

2014-04-05 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Changed the name of GCCXML_DUMMY_INSTANTIATION to
	  a more unique name in the dictionary header. Was necessary,
	  as rootcling is complaining about seeing the same struct name
	  in multiple libraries. (Which really shouldn't clash actually,
	  but let's do this until ROOT 6 stops complaining about this.)
	* Tagging as xAODTracking-00-03-08

2014-04-05 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Making the dictionary generation work correctly with ROOT 6.
	* Temporarily excluding a number of classes in selection.xml
	  until rootcling learns not to want to generate a dictionary for
	  every class that it encounters. :-P
	* Got rid of some compilation warnings from
	  TrackSummaryAccessors_v1.icc.
	* Tagging as xAODTracking-00-03-07

2014-03-27 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Protected xAOD::TrackParticle_v1::summaryValue against slimmed
	  decorations.
	* Made the xAOD::Vertex_v1::trackParticle and
	  xAOD::Vertex_v1::trackWeight functions behave nicely when they
	  receive invalid indices.
	* Tagging as xAODTracking-00-03-06

2014-03-25 Edward.Moyse@cern.ch
  * Added a bit more doxygen documentation (especially on the SummaryType, indicating whether they're uint8_t or float)

2014-03-20 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Making xAOD::TrackParticle_v1::track() return an invalid
	  ElementLink in case nothing was set yet, instead of throwing an
	  exception. As this can happen during muon reconstruction...
	* Tagging as xAODTracking-00-03-05

2014-03-19 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Found a few issues with the way TrackParticle was handling its
	  variables.
	  - phi0 is no longer called phi0, the function was adjusted.
	  - The type of parameterPosition was adjusted in the aux container.
	  - The parameterPosition variable is now resized in the
	    setTrackParameters(...) call. (May not be the correct behaviour.)
	* Added a Doxygen mainpage.
	* Added a unit test for the TrackParticle class. Will need to be
	  extended later on to test all aspects of the class. Of course
	  similar tests will then need to be added for the other two
	  classes as well.
	* Demoted the Trk::Track link in TrackParticle to be a dynamic
	  decoration on the object. This is to simplify the ROOT I/O
	  of the auxiliary class. Because of this the change must be used
	  with the latest version of xAODTrackingAthenaPool. (Which makes
	  sure that the dictionary for this dynamic branch is loaded.)
	* Tagging as xAODTracking-00-03-04

2014-03-18 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Putting back the interface into xAOD::Vertex to hold on to
	  VxTrackAtVertex objects. But this time the memory management
	  of this is handled very differently than before. The objects are
	  kept as dynamic auxiliary data, in a simple std::vector. (Not
	  using pointers at any stage.)
	* Practically no runtime testing done at this point. (Output lists
	  will have to be very carefully constructed for RecExCommon.)
	* Tagging as xAODTracking-00-03-03

2014-03-18 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Making the Eigen code in xAOD::Vertex visible in the standalone
	  build.
	* This is a predecessor to putting VxTrackAtVertex back into
	  xAOD::Vertex, which will happen momentarily.

2014-03-17 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Found the reason for vertex->nTrackParticles() and
	  vertex->trackParticles().size() returning different answers.
	  The specialised accessors were apparently not using the
	  correct Accessor object. In fact, I don't know what
	  object they were using, as the variables were not declared
	  correctly as static in Vertex_v1.cxx.
	* With this simple modification the code seems to behave
	  correctly now.
	* Tagging as xAODTracking-00-03-02

2014-03-10 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Finally making use of Scott's DATAVECTOR_BASE_FWD macro.
	  Unfortunately now I need to put the "missing part" of this
	  macro into the Container_v1 headers explicitly. Not too
	  nice, but let's do it like this for now...
	* This should finally allow us to include only "xAODTracking/Vertex.h"
	  on its own.
	* Tagging as xAODTracking-00-03-01

2014-02-10 Edward.Moyse@cern.ch
	* Adding new function to search for parameters at a specified position indexOfParameterAtPosition(...)
	* Tagging as xAODTracking-00-03-00

2014-03-05 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Adding copy constructor to Vertex_v1
	* Tagging as xAODTracking-00-02-01

2014-03-03 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Removing reco-only members of Vertex
	* Tagging as xAODTracking-00-02-00

2014-02-13 Niels van Eldik
	* extending MuonHitSummary Info
	* Tagging as xAODTracking-00-01-05

2014-02-12 Niels van Eldik
	* add MuonHitSummary Info
	* Tagging as xAODTracking-00-01-04

2014-02-12 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Added an explicit for the uint8_t type in
	  TrackSummaryAccessors_v1.cxx.
	* Since the RootCore nightly is not built in C++11 mode yet,
	  and the only two lines relying on C++11 could be made
	  C++03 compatible very easily, removed the two instances
	  of "auto" from the code. For now.
	* Tagging as xAODTracking-00-01-03

2014-02-07 Edward.Moyse@cern.ch
	* Add vertex to list of static variables for TrackParticle
	* Tagging as xAODTracking-00-01-02

2014-02-07 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Modified the standalone compilation a bit. Now RootCore doesn't
	  compile all the source files with -DEIGEN_DONT_VECTORIZE
	  anymore, this define is only used while generating the Reflex
	  dictionary.
	* This implementation is a bit hard-coded, but for now it should
	  be okay...
	* Tagging as xAODTracking-00-01-01

2014-02-04 Edward.Moyse@cern.ch
	* Actually use ElementLinks for Vertices (which necessitated some fiddling with the forward includes)
	  * Interface changed
	* (NeutralParticle still not fixed)
	* Tagging as xAODTracking-00-01-00

2014-01-30 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Forgot to update the requirements file with the previous
	  update... Done now.
	* Tagging as xAODTracking-00-00-06

2014-01-30 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Making the package compile in standalone mode using
	  RootCore.
	* In order to compile the Reflex dictionaries the Eigen
	  vectorization had to be turned off (for now).
	* Fixed some minor mistakes that were in the code in
	  standalone mode.
	* Tagging as xAODTracking-00-00-05

2014-01-29 Edward.Moyse@cern.ch
  * Fix for bug with trackFitter showing up as dynamic variable
  * Tagging as xAODTracking-00-00-04

2014-01-23 Niels van Eldik
  * add forward declare headers
  * move to using Amg::expand and Amg::compress
  * Tagging as xAODTracking-00-00-02

2014-01-21  Ruslan.Mashinistov@cern.ch
  * Migration MatrixX -> AmgSumMatrix(3)
  * Tagging as xAODTracking-00-00-01

2014-01-14  Edward.Moyse@cern.ch
  * First import (merger of xAODVertex and xAODTrackParticle)
  * Tagging as xAODTracking-00-00-00

// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODCoredIobjdIxAODCore_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/xAODCore/xAODCore/xAODCoreDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLAuxContainerBase_Dictionary();
   static void xAODcLcLAuxContainerBase_TClassManip(TClass*);
   static void *new_xAODcLcLAuxContainerBase(void *p = 0);
   static void *newArray_xAODcLcLAuxContainerBase(Long_t size, void *p);
   static void delete_xAODcLcLAuxContainerBase(void *p);
   static void deleteArray_xAODcLcLAuxContainerBase(void *p);
   static void destruct_xAODcLcLAuxContainerBase(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLAuxContainerBase_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::AuxContainerBase* newObj = (xAOD::AuxContainerBase*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
         newObj->clearDecorations();
         newObj->lock();
      
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::AuxContainerBase*)
   {
      ::xAOD::AuxContainerBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::AuxContainerBase));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::AuxContainerBase", "xAODCore/AuxContainerBase.h", 43,
                  typeid(::xAOD::AuxContainerBase), DefineBehavior(ptr, ptr),
                  &xAODcLcLAuxContainerBase_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::AuxContainerBase) );
      instance.SetNew(&new_xAODcLcLAuxContainerBase);
      instance.SetNewArray(&newArray_xAODcLcLAuxContainerBase);
      instance.SetDelete(&delete_xAODcLcLAuxContainerBase);
      instance.SetDeleteArray(&deleteArray_xAODcLcLAuxContainerBase);
      instance.SetDestructor(&destruct_xAODcLcLAuxContainerBase);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::AuxContainerBase";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLAuxContainerBase_0);
      rule->fCode        = "\n         newObj->clearDecorations();\n         newObj->lock();\n      ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::AuxContainerBase*)
   {
      return GenerateInitInstanceLocal((::xAOD::AuxContainerBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::AuxContainerBase*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLAuxContainerBase_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::AuxContainerBase*)0x0)->GetClass();
      xAODcLcLAuxContainerBase_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLAuxContainerBase_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C87C71B3-B03F-42FC-AF99-DF497F148397");
      TDataMember* theMember_m_name = theClass->GetDataMember("m_name");
      theMember_m_name->CreateAttributeMap();
      TDictAttributeMap* memberAttrMap_m_name( theMember_m_name->GetAttributeMap() );
      memberAttrMap_m_name->AddProperty("transiet","true");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLAuxInfoBase_Dictionary();
   static void xAODcLcLAuxInfoBase_TClassManip(TClass*);
   static void *new_xAODcLcLAuxInfoBase(void *p = 0);
   static void *newArray_xAODcLcLAuxInfoBase(Long_t size, void *p);
   static void delete_xAODcLcLAuxInfoBase(void *p);
   static void deleteArray_xAODcLcLAuxInfoBase(void *p);
   static void destruct_xAODcLcLAuxInfoBase(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLAuxInfoBase_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::AuxInfoBase* newObj = (xAOD::AuxInfoBase*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
         newObj->clearDecorations();
         newObj->lock();
      
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::AuxInfoBase*)
   {
      ::xAOD::AuxInfoBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::AuxInfoBase));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::AuxInfoBase", "xAODCore/AuxInfoBase.h", 40,
                  typeid(::xAOD::AuxInfoBase), DefineBehavior(ptr, ptr),
                  &xAODcLcLAuxInfoBase_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::AuxInfoBase) );
      instance.SetNew(&new_xAODcLcLAuxInfoBase);
      instance.SetNewArray(&newArray_xAODcLcLAuxInfoBase);
      instance.SetDelete(&delete_xAODcLcLAuxInfoBase);
      instance.SetDeleteArray(&deleteArray_xAODcLcLAuxInfoBase);
      instance.SetDestructor(&destruct_xAODcLcLAuxInfoBase);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::AuxInfoBase";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLAuxInfoBase_0);
      rule->fCode        = "\n         newObj->clearDecorations();\n         newObj->lock();\n      ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::AuxInfoBase*)
   {
      return GenerateInitInstanceLocal((::xAOD::AuxInfoBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::AuxInfoBase*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLAuxInfoBase_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::AuxInfoBase*)0x0)->GetClass();
      xAODcLcLAuxInfoBase_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLAuxInfoBase_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","47FE21D1-5196-4961-A8A1-1DC1B4104DD8");
      TDataMember* theMember_m_name = theClass->GetDataMember("m_name");
      theMember_m_name->CreateAttributeMap();
      TDictAttributeMap* memberAttrMap_m_name( theMember_m_name->GetAttributeMap() );
      memberAttrMap_m_name->AddProperty("transiet","true");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLShallowAuxContainer_Dictionary();
   static void xAODcLcLShallowAuxContainer_TClassManip(TClass*);
   static void *new_xAODcLcLShallowAuxContainer(void *p = 0);
   static void *newArray_xAODcLcLShallowAuxContainer(Long_t size, void *p);
   static void delete_xAODcLcLShallowAuxContainer(void *p);
   static void deleteArray_xAODcLcLShallowAuxContainer(void *p);
   static void destruct_xAODcLcLShallowAuxContainer(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLShallowAuxContainer_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::ShallowAuxContainer* newObj = (xAOD::ShallowAuxContainer*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
         newObj->clearDecorations();
         newObj->lock();
      
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::ShallowAuxContainer*)
   {
      ::xAOD::ShallowAuxContainer *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::ShallowAuxContainer));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::ShallowAuxContainer", "xAODCore/ShallowAuxContainer.h", 42,
                  typeid(::xAOD::ShallowAuxContainer), DefineBehavior(ptr, ptr),
                  &xAODcLcLShallowAuxContainer_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::ShallowAuxContainer) );
      instance.SetNew(&new_xAODcLcLShallowAuxContainer);
      instance.SetNewArray(&newArray_xAODcLcLShallowAuxContainer);
      instance.SetDelete(&delete_xAODcLcLShallowAuxContainer);
      instance.SetDeleteArray(&deleteArray_xAODcLcLShallowAuxContainer);
      instance.SetDestructor(&destruct_xAODcLcLShallowAuxContainer);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::ShallowAuxContainer";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLShallowAuxContainer_0);
      rule->fCode        = "\n         newObj->clearDecorations();\n         newObj->lock();\n      ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::ShallowAuxContainer*)
   {
      return GenerateInitInstanceLocal((::xAOD::ShallowAuxContainer*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::ShallowAuxContainer*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLShallowAuxContainer_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::ShallowAuxContainer*)0x0)->GetClass();
      xAODcLcLShallowAuxContainer_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLShallowAuxContainer_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C63C39D7-9501-49DC-B1B0-6AD98B1AEEFA");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLShallowAuxInfo_Dictionary();
   static void xAODcLcLShallowAuxInfo_TClassManip(TClass*);
   static void *new_xAODcLcLShallowAuxInfo(void *p = 0);
   static void *newArray_xAODcLcLShallowAuxInfo(Long_t size, void *p);
   static void delete_xAODcLcLShallowAuxInfo(void *p);
   static void deleteArray_xAODcLcLShallowAuxInfo(void *p);
   static void destruct_xAODcLcLShallowAuxInfo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::ShallowAuxInfo*)
   {
      ::xAOD::ShallowAuxInfo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::ShallowAuxInfo));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::ShallowAuxInfo", "xAODCore/ShallowAuxInfo.h", 24,
                  typeid(::xAOD::ShallowAuxInfo), DefineBehavior(ptr, ptr),
                  &xAODcLcLShallowAuxInfo_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::ShallowAuxInfo) );
      instance.SetNew(&new_xAODcLcLShallowAuxInfo);
      instance.SetNewArray(&newArray_xAODcLcLShallowAuxInfo);
      instance.SetDelete(&delete_xAODcLcLShallowAuxInfo);
      instance.SetDeleteArray(&deleteArray_xAODcLcLShallowAuxInfo);
      instance.SetDestructor(&destruct_xAODcLcLShallowAuxInfo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::ShallowAuxInfo*)
   {
      return GenerateInitInstanceLocal((::xAOD::ShallowAuxInfo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::ShallowAuxInfo*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLShallowAuxInfo_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::ShallowAuxInfo*)0x0)->GetClass();
      xAODcLcLShallowAuxInfo_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLShallowAuxInfo_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","BE505E75-8760-4F39-9331-689CB5443DB1");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTDVCollectionProxy_Dictionary();
   static void xAODcLcLTDVCollectionProxy_TClassManip(TClass*);
   static void delete_xAODcLcLTDVCollectionProxy(void *p);
   static void deleteArray_xAODcLcLTDVCollectionProxy(void *p);
   static void destruct_xAODcLcLTDVCollectionProxy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TDVCollectionProxy*)
   {
      ::xAOD::TDVCollectionProxy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TDVCollectionProxy));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TDVCollectionProxy", "xAODCore/tools/TDVCollectionProxy.h", 37,
                  typeid(::xAOD::TDVCollectionProxy), DefineBehavior(ptr, ptr),
                  &xAODcLcLTDVCollectionProxy_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TDVCollectionProxy) );
      instance.SetDelete(&delete_xAODcLcLTDVCollectionProxy);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTDVCollectionProxy);
      instance.SetDestructor(&destruct_xAODcLcLTDVCollectionProxy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TDVCollectionProxy*)
   {
      return GenerateInitInstanceLocal((::xAOD::TDVCollectionProxy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TDVCollectionProxy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTDVCollectionProxy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TDVCollectionProxy*)0x0)->GetClass();
      xAODcLcLTDVCollectionProxy_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTDVCollectionProxy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTDVCollectionProxyDummy_Dictionary();
   static void xAODcLcLTDVCollectionProxyDummy_TClassManip(TClass*);
   static void *new_xAODcLcLTDVCollectionProxyDummy(void *p = 0);
   static void *newArray_xAODcLcLTDVCollectionProxyDummy(Long_t size, void *p);
   static void delete_xAODcLcLTDVCollectionProxyDummy(void *p);
   static void deleteArray_xAODcLcLTDVCollectionProxyDummy(void *p);
   static void destruct_xAODcLcLTDVCollectionProxyDummy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TDVCollectionProxyDummy*)
   {
      ::xAOD::TDVCollectionProxyDummy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TDVCollectionProxyDummy));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TDVCollectionProxyDummy", "xAODCore/tools/TDVCollectionProxy.h", 16,
                  typeid(::xAOD::TDVCollectionProxyDummy), DefineBehavior(ptr, ptr),
                  &xAODcLcLTDVCollectionProxyDummy_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TDVCollectionProxyDummy) );
      instance.SetNew(&new_xAODcLcLTDVCollectionProxyDummy);
      instance.SetNewArray(&newArray_xAODcLcLTDVCollectionProxyDummy);
      instance.SetDelete(&delete_xAODcLcLTDVCollectionProxyDummy);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTDVCollectionProxyDummy);
      instance.SetDestructor(&destruct_xAODcLcLTDVCollectionProxyDummy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TDVCollectionProxyDummy*)
   {
      return GenerateInitInstanceLocal((::xAOD::TDVCollectionProxyDummy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TDVCollectionProxyDummy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTDVCollectionProxyDummy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TDVCollectionProxyDummy*)0x0)->GetClass();
      xAODcLcLTDVCollectionProxyDummy_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTDVCollectionProxyDummy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTDVCollectionProxyDummygR_Dictionary();
   static void DataVectorlExAODcLcLTDVCollectionProxyDummygR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTDVCollectionProxyDummygR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTDVCollectionProxyDummygR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTDVCollectionProxyDummygR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTDVCollectionProxyDummygR(void *p);
   static void destruct_DataVectorlExAODcLcLTDVCollectionProxyDummygR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TDVCollectionProxyDummy>*)
   {
      ::DataVector<xAOD::TDVCollectionProxyDummy> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TDVCollectionProxyDummy>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TDVCollectionProxyDummy>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TDVCollectionProxyDummy>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTDVCollectionProxyDummygR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TDVCollectionProxyDummy>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTDVCollectionProxyDummygR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTDVCollectionProxyDummygR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTDVCollectionProxyDummygR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTDVCollectionProxyDummygR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTDVCollectionProxyDummygR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TDVCollectionProxyDummy>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TDVCollectionProxyDummy>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TDVCollectionProxyDummy>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTDVCollectionProxyDummygR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TDVCollectionProxyDummy>*)0x0)->GetClass();
      DataVectorlExAODcLcLTDVCollectionProxyDummygR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTDVCollectionProxyDummygR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLAuxContainerBase(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::AuxContainerBase : new ::xAOD::AuxContainerBase;
   }
   static void *newArray_xAODcLcLAuxContainerBase(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::AuxContainerBase[nElements] : new ::xAOD::AuxContainerBase[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLAuxContainerBase(void *p) {
      delete ((::xAOD::AuxContainerBase*)p);
   }
   static void deleteArray_xAODcLcLAuxContainerBase(void *p) {
      delete [] ((::xAOD::AuxContainerBase*)p);
   }
   static void destruct_xAODcLcLAuxContainerBase(void *p) {
      typedef ::xAOD::AuxContainerBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::AuxContainerBase

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLAuxInfoBase(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::AuxInfoBase : new ::xAOD::AuxInfoBase;
   }
   static void *newArray_xAODcLcLAuxInfoBase(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::AuxInfoBase[nElements] : new ::xAOD::AuxInfoBase[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLAuxInfoBase(void *p) {
      delete ((::xAOD::AuxInfoBase*)p);
   }
   static void deleteArray_xAODcLcLAuxInfoBase(void *p) {
      delete [] ((::xAOD::AuxInfoBase*)p);
   }
   static void destruct_xAODcLcLAuxInfoBase(void *p) {
      typedef ::xAOD::AuxInfoBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::AuxInfoBase

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLShallowAuxContainer(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ShallowAuxContainer : new ::xAOD::ShallowAuxContainer;
   }
   static void *newArray_xAODcLcLShallowAuxContainer(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ShallowAuxContainer[nElements] : new ::xAOD::ShallowAuxContainer[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLShallowAuxContainer(void *p) {
      delete ((::xAOD::ShallowAuxContainer*)p);
   }
   static void deleteArray_xAODcLcLShallowAuxContainer(void *p) {
      delete [] ((::xAOD::ShallowAuxContainer*)p);
   }
   static void destruct_xAODcLcLShallowAuxContainer(void *p) {
      typedef ::xAOD::ShallowAuxContainer current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::ShallowAuxContainer

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLShallowAuxInfo(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ShallowAuxInfo : new ::xAOD::ShallowAuxInfo;
   }
   static void *newArray_xAODcLcLShallowAuxInfo(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::ShallowAuxInfo[nElements] : new ::xAOD::ShallowAuxInfo[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLShallowAuxInfo(void *p) {
      delete ((::xAOD::ShallowAuxInfo*)p);
   }
   static void deleteArray_xAODcLcLShallowAuxInfo(void *p) {
      delete [] ((::xAOD::ShallowAuxInfo*)p);
   }
   static void destruct_xAODcLcLShallowAuxInfo(void *p) {
      typedef ::xAOD::ShallowAuxInfo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::ShallowAuxInfo

namespace ROOT {
   // Wrapper around operator delete
   static void delete_xAODcLcLTDVCollectionProxy(void *p) {
      delete ((::xAOD::TDVCollectionProxy*)p);
   }
   static void deleteArray_xAODcLcLTDVCollectionProxy(void *p) {
      delete [] ((::xAOD::TDVCollectionProxy*)p);
   }
   static void destruct_xAODcLcLTDVCollectionProxy(void *p) {
      typedef ::xAOD::TDVCollectionProxy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TDVCollectionProxy

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTDVCollectionProxyDummy(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TDVCollectionProxyDummy : new ::xAOD::TDVCollectionProxyDummy;
   }
   static void *newArray_xAODcLcLTDVCollectionProxyDummy(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TDVCollectionProxyDummy[nElements] : new ::xAOD::TDVCollectionProxyDummy[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTDVCollectionProxyDummy(void *p) {
      delete ((::xAOD::TDVCollectionProxyDummy*)p);
   }
   static void deleteArray_xAODcLcLTDVCollectionProxyDummy(void *p) {
      delete [] ((::xAOD::TDVCollectionProxyDummy*)p);
   }
   static void destruct_xAODcLcLTDVCollectionProxyDummy(void *p) {
      typedef ::xAOD::TDVCollectionProxyDummy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TDVCollectionProxyDummy

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTDVCollectionProxyDummygR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TDVCollectionProxyDummy> : new ::DataVector<xAOD::TDVCollectionProxyDummy>;
   }
   static void *newArray_DataVectorlExAODcLcLTDVCollectionProxyDummygR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TDVCollectionProxyDummy>[nElements] : new ::DataVector<xAOD::TDVCollectionProxyDummy>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTDVCollectionProxyDummygR(void *p) {
      delete ((::DataVector<xAOD::TDVCollectionProxyDummy>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTDVCollectionProxyDummygR(void *p) {
      delete [] ((::DataVector<xAOD::TDVCollectionProxyDummy>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTDVCollectionProxyDummygR(void *p) {
      typedef ::DataVector<xAOD::TDVCollectionProxyDummy> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TDVCollectionProxyDummy>

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPintgRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned int> >*)
   {
      vector<vector<unsigned int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned int> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned int> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPintgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned int> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned int> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned int> > : new vector<vector<unsigned int> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned int> >[nElements] : new vector<vector<unsigned int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      delete ((vector<vector<unsigned int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      delete [] ((vector<vector<unsigned int> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      typedef vector<vector<unsigned int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned int> >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPchargRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPchargRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPchargRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPchargRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPchargRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPchargRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned char> >*)
   {
      vector<vector<unsigned char> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned char> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned char> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned char> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPchargRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned char> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned char> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned char> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPchargRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned char> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned char> > : new vector<vector<unsigned char> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPchargRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned char> >[nElements] : new vector<vector<unsigned char> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      delete ((vector<vector<unsigned char> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      delete [] ((vector<vector<unsigned char> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      typedef vector<vector<unsigned char> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned char> >

namespace ROOT {
   static TClass *vectorlEvectorlEsignedsPchargRsPgR_Dictionary();
   static void vectorlEvectorlEsignedsPchargRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEsignedsPchargRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEsignedsPchargRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEsignedsPchargRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEsignedsPchargRsPgR(void *p);
   static void destruct_vectorlEvectorlEsignedsPchargRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<signed char> >*)
   {
      vector<vector<signed char> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<signed char> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<signed char> >", -2, "vector", 210,
                  typeid(vector<vector<signed char> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEsignedsPchargRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<signed char> >) );
      instance.SetNew(&new_vectorlEvectorlEsignedsPchargRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEsignedsPchargRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEsignedsPchargRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEsignedsPchargRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEsignedsPchargRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<signed char> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<signed char> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEsignedsPchargRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<signed char> >*)0x0)->GetClass();
      vectorlEvectorlEsignedsPchargRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEsignedsPchargRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEsignedsPchargRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<signed char> > : new vector<vector<signed char> >;
   }
   static void *newArray_vectorlEvectorlEsignedsPchargRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<signed char> >[nElements] : new vector<vector<signed char> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEsignedsPchargRsPgR(void *p) {
      delete ((vector<vector<signed char> >*)p);
   }
   static void deleteArray_vectorlEvectorlEsignedsPchargRsPgR(void *p) {
      delete [] ((vector<vector<signed char> >*)p);
   }
   static void destruct_vectorlEvectorlEsignedsPchargRsPgR(void *p) {
      typedef vector<vector<signed char> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<signed char> >

namespace ROOT {
   static TClass *vectorlEvectorlEchargRsPgR_Dictionary();
   static void vectorlEvectorlEchargRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEchargRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEchargRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEchargRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEchargRsPgR(void *p);
   static void destruct_vectorlEvectorlEchargRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<char> >*)
   {
      vector<vector<char> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<char> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<char> >", -2, "vector", 210,
                  typeid(vector<vector<char> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEchargRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<char> >) );
      instance.SetNew(&new_vectorlEvectorlEchargRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEchargRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEchargRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEchargRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEchargRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<char> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<char> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEchargRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<char> >*)0x0)->GetClass();
      vectorlEvectorlEchargRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEchargRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEchargRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<char> > : new vector<vector<char> >;
   }
   static void *newArray_vectorlEvectorlEchargRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<char> >[nElements] : new vector<vector<char> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEchargRsPgR(void *p) {
      delete ((vector<vector<char> >*)p);
   }
   static void deleteArray_vectorlEvectorlEchargRsPgR(void *p) {
      delete [] ((vector<vector<char> >*)p);
   }
   static void destruct_vectorlEvectorlEchargRsPgR(void *p) {
      typedef vector<vector<char> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<char> >

namespace ROOT {
   static TClass *vectorlEunsignedsPintgR_Dictionary();
   static void vectorlEunsignedsPintgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPintgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPintgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPintgR(void *p);
   static void deleteArray_vectorlEunsignedsPintgR(void *p);
   static void destruct_vectorlEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned int>*)
   {
      vector<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned int>", -2, "vector", 210,
                  typeid(vector<unsigned int>), DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned int>) );
      instance.SetNew(&new_vectorlEunsignedsPintgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPintgR);
      instance.SetDelete(&delete_vectorlEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPintgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned int>*)0x0)->GetClass();
      vectorlEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned int> : new vector<unsigned int>;
   }
   static void *newArray_vectorlEunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned int>[nElements] : new vector<unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPintgR(void *p) {
      delete ((vector<unsigned int>*)p);
   }
   static void deleteArray_vectorlEunsignedsPintgR(void *p) {
      delete [] ((vector<unsigned int>*)p);
   }
   static void destruct_vectorlEunsignedsPintgR(void *p) {
      typedef vector<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned int>

namespace ROOT {
   static TClass *vectorlEunsignedsPchargR_Dictionary();
   static void vectorlEunsignedsPchargR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPchargR(void *p = 0);
   static void *newArray_vectorlEunsignedsPchargR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPchargR(void *p);
   static void deleteArray_vectorlEunsignedsPchargR(void *p);
   static void destruct_vectorlEunsignedsPchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned char>*)
   {
      vector<unsigned char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned char>", -2, "vector", 210,
                  typeid(vector<unsigned char>), DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPchargR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned char>) );
      instance.SetNew(&new_vectorlEunsignedsPchargR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPchargR);
      instance.SetDelete(&delete_vectorlEunsignedsPchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPchargR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned char> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned char>*)0x0)->GetClass();
      vectorlEunsignedsPchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPchargR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned char> : new vector<unsigned char>;
   }
   static void *newArray_vectorlEunsignedsPchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned char>[nElements] : new vector<unsigned char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPchargR(void *p) {
      delete ((vector<unsigned char>*)p);
   }
   static void deleteArray_vectorlEunsignedsPchargR(void *p) {
      delete [] ((vector<unsigned char>*)p);
   }
   static void destruct_vectorlEunsignedsPchargR(void *p) {
      typedef vector<unsigned char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned char>

namespace ROOT {
   static TClass *vectorlEsignedsPchargR_Dictionary();
   static void vectorlEsignedsPchargR_TClassManip(TClass*);
   static void *new_vectorlEsignedsPchargR(void *p = 0);
   static void *newArray_vectorlEsignedsPchargR(Long_t size, void *p);
   static void delete_vectorlEsignedsPchargR(void *p);
   static void deleteArray_vectorlEsignedsPchargR(void *p);
   static void destruct_vectorlEsignedsPchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<signed char>*)
   {
      vector<signed char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<signed char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<signed char>", -2, "vector", 210,
                  typeid(vector<signed char>), DefineBehavior(ptr, ptr),
                  &vectorlEsignedsPchargR_Dictionary, isa_proxy, 4,
                  sizeof(vector<signed char>) );
      instance.SetNew(&new_vectorlEsignedsPchargR);
      instance.SetNewArray(&newArray_vectorlEsignedsPchargR);
      instance.SetDelete(&delete_vectorlEsignedsPchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEsignedsPchargR);
      instance.SetDestructor(&destruct_vectorlEsignedsPchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<signed char> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<signed char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEsignedsPchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<signed char>*)0x0)->GetClass();
      vectorlEsignedsPchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEsignedsPchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEsignedsPchargR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<signed char> : new vector<signed char>;
   }
   static void *newArray_vectorlEsignedsPchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<signed char>[nElements] : new vector<signed char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEsignedsPchargR(void *p) {
      delete ((vector<signed char>*)p);
   }
   static void deleteArray_vectorlEsignedsPchargR(void *p) {
      delete [] ((vector<signed char>*)p);
   }
   static void destruct_vectorlEsignedsPchargR(void *p) {
      typedef vector<signed char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<signed char>

namespace ROOT {
   static TClass *vectorlEchargR_Dictionary();
   static void vectorlEchargR_TClassManip(TClass*);
   static void *new_vectorlEchargR(void *p = 0);
   static void *newArray_vectorlEchargR(Long_t size, void *p);
   static void delete_vectorlEchargR(void *p);
   static void deleteArray_vectorlEchargR(void *p);
   static void destruct_vectorlEchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<char>*)
   {
      vector<char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<char>", -2, "vector", 210,
                  typeid(vector<char>), DefineBehavior(ptr, ptr),
                  &vectorlEchargR_Dictionary, isa_proxy, 4,
                  sizeof(vector<char>) );
      instance.SetNew(&new_vectorlEchargR);
      instance.SetNewArray(&newArray_vectorlEchargR);
      instance.SetDelete(&delete_vectorlEchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEchargR);
      instance.SetDestructor(&destruct_vectorlEchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<char> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<char>*)0x0)->GetClass();
      vectorlEchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEchargR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<char> : new vector<char>;
   }
   static void *newArray_vectorlEchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<char>[nElements] : new vector<char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEchargR(void *p) {
      delete ((vector<char>*)p);
   }
   static void deleteArray_vectorlEchargR(void *p) {
      delete [] ((vector<char>*)p);
   }
   static void destruct_vectorlEchargR(void *p) {
      typedef vector<char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<char>

namespace ROOT {
   static TClass *vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR_Dictionary();
   static void vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(void *p);
   static void deleteArray_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(void *p);
   static void destruct_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >*)
   {
      vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >", -2, "vector", 210,
                  typeid(vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >), DefineBehavior(ptr, ptr),
                  &vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >) );
      instance.SetNew(&new_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >*)0x0)->GetClass();
      vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > > : new vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >;
   }
   static void *newArray_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >[nElements] : new vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(void *p) {
      delete ((vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >*)p);
   }
   static void deleteArray_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(void *p) {
      delete [] ((vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >*)p);
   }
   static void destruct_vectorlEROOTcLcLMathcLcLLorentzVectorlEROOTcLcLMathcLcLPtEtaPhiM4DlEdoublegRsPgRsPgR(void *p) {
      typedef vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >

namespace {
  void TriggerDictionaryInitialization_xAODCore_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/xAODCore/Root",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/xAODCore",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/xAODCore/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@C87C71B3-B03F-42FC-AF99-DF497F148397)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCore/xAODCoreRflxDict.h")))  AuxContainerBase;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@47FE21D1-5196-4961-A8A1-1DC1B4104DD8)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCore/xAODCoreRflxDict.h")))  AuxInfoBase;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@C63C39D7-9501-49DC-B1B0-6AD98B1AEEFA)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCore/xAODCoreRflxDict.h")))  ShallowAuxContainer;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@BE505E75-8760-4F39-9331-689CB5443DB1)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODCore/xAODCoreRflxDict.h")))  ShallowAuxInfo;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCore/xAODCoreRflxDict.h")))  TDVCollectionProxy;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODCore/xAODCoreRflxDict.h")))  TDVCollectionProxyDummy;}
namespace ROOT{namespace Math{template <typename T> class __attribute__((annotate("$clingAutoload$xAODCore/xAODCoreRflxDict.h")))  PtEtaPhiM4D;
}}
namespace ROOT{namespace Math{template <class CoordSystem> class __attribute__((annotate("$clingAutoload$xAODCore/xAODCoreRflxDict.h")))  LorentzVector;
}}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODCore"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODCoreDict.h 579782 2014-01-23 15:40:31Z krasznaa $
#ifndef XAODCORE_XAODCOREDICT_H
#define XAODCORE_XAODCOREDICT_H

// This header is here for technical reasons. It is needed for a
// successful RootCore compilation. (But it would mess up if used
// in a CMT compilation.)

// Local include(s):
#include "xAODCore/xAODCoreRflxDict.h"

#endif // XAODCORE_XAODCOREDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataVector<xAOD::TDVCollectionProxyDummy>", payloadCode, "@",
"vector<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > >", payloadCode, "@",
"vector<char>", payloadCode, "@",
"vector<signed char>", payloadCode, "@",
"vector<std::vector<char> >", payloadCode, "@",
"vector<std::vector<signed char> >", payloadCode, "@",
"vector<std::vector<uint32_t> >", payloadCode, "@",
"vector<std::vector<unsigned char> >", payloadCode, "@",
"vector<uint32_t>", payloadCode, "@",
"vector<unsigned char>", payloadCode, "@",
"vector<unsigned int>", payloadCode, "@",
"vector<vector<char> >", payloadCode, "@",
"vector<vector<signed char> >", payloadCode, "@",
"vector<vector<unsigned char> >", payloadCode, "@",
"vector<vector<unsigned int> >", payloadCode, "@",
"xAOD::AuxContainerBase", payloadCode, "@",
"xAOD::AuxInfoBase", payloadCode, "@",
"xAOD::ShallowAuxContainer", payloadCode, "@",
"xAOD::ShallowAuxInfo", payloadCode, "@",
"xAOD::TDVCollectionProxy", payloadCode, "@",
"xAOD::TDVCollectionProxyDummy", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODCore_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODCore_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODCore_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODCore_Reflex() {
  TriggerDictionaryInitialization_xAODCore_Reflex_Impl();
}

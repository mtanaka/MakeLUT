// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrigCalodIobjdIxAODTrigCalo_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigCalo/xAODTrigCalo/xAODTrigCaloDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTrigCaloCluster_v1_Dictionary();
   static void xAODcLcLTrigCaloCluster_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigCaloCluster_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigCaloCluster_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigCaloCluster_v1(void *p);
   static void deleteArray_xAODcLcLTrigCaloCluster_v1(void *p);
   static void destruct_xAODcLcLTrigCaloCluster_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigCaloCluster_v1*)
   {
      ::xAOD::TrigCaloCluster_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigCaloCluster_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigCaloCluster_v1", "xAODTrigCalo/versions/TrigCaloCluster_v1.h", 23,
                  typeid(::xAOD::TrigCaloCluster_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigCaloCluster_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigCaloCluster_v1) );
      instance.SetNew(&new_xAODcLcLTrigCaloCluster_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigCaloCluster_v1);
      instance.SetDelete(&delete_xAODcLcLTrigCaloCluster_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigCaloCluster_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigCaloCluster_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigCaloCluster_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigCaloCluster_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigCaloCluster_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigCaloCluster_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigCaloCluster_v1*)0x0)->GetClass();
      xAODcLcLTrigCaloCluster_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigCaloCluster_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigCaloCluster_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigCaloCluster_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigCaloCluster_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigCaloCluster_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigCaloCluster_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigCaloCluster_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigCaloCluster_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigCaloCluster_v1>*)
   {
      ::DataVector<xAOD::TrigCaloCluster_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigCaloCluster_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigCaloCluster_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigCaloCluster_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigCaloCluster_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigCaloCluster_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigCaloCluster_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigCaloCluster_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigCaloCluster_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigCaloCluster_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigCaloCluster_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigCaloCluster_v1>","xAOD::TrigCaloClusterContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigCaloCluster_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigCaloCluster_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigCaloCluster_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigCaloCluster_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigCaloCluster_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigCaloCluster_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigCaloCluster_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","0C7F7869-C67A-4E4A-8793-358F8B32DFEA");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigCaloClusterAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigCaloClusterAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigCaloClusterAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigCaloClusterAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigCaloClusterAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigCaloClusterAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigCaloClusterAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigCaloClusterAuxContainer_v1*)
   {
      ::xAOD::TrigCaloClusterAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigCaloClusterAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigCaloClusterAuxContainer_v1", "xAODTrigCalo/versions/TrigCaloClusterAuxContainer_v1.h", 26,
                  typeid(::xAOD::TrigCaloClusterAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigCaloClusterAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigCaloClusterAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigCaloClusterAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigCaloClusterAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigCaloClusterAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigCaloClusterAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigCaloClusterAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigCaloClusterAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigCaloClusterAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigCaloClusterAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigCaloClusterAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigCaloClusterAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigCaloClusterAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigCaloClusterAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","4A2F55AF-D465-42A8-A4CF-2DB84D9628E5");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >","ElementLink<xAOD::TrigCaloClusterContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigEMCluster_v1_Dictionary();
   static void xAODcLcLTrigEMCluster_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigEMCluster_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigEMCluster_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigEMCluster_v1(void *p);
   static void deleteArray_xAODcLcLTrigEMCluster_v1(void *p);
   static void destruct_xAODcLcLTrigEMCluster_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigEMCluster_v1*)
   {
      ::xAOD::TrigEMCluster_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigEMCluster_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigEMCluster_v1", "xAODTrigCalo/versions/TrigEMCluster_v1.h", 23,
                  typeid(::xAOD::TrigEMCluster_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigEMCluster_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigEMCluster_v1) );
      instance.SetNew(&new_xAODcLcLTrigEMCluster_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigEMCluster_v1);
      instance.SetDelete(&delete_xAODcLcLTrigEMCluster_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigEMCluster_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigEMCluster_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigEMCluster_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigEMCluster_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigEMCluster_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigEMCluster_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigEMCluster_v1*)0x0)->GetClass();
      xAODcLcLTrigEMCluster_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigEMCluster_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigEMCluster_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigEMCluster_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigEMCluster_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigEMCluster_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigEMCluster_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigEMCluster_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigEMCluster_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigEMCluster_v1>*)
   {
      ::DataVector<xAOD::TrigEMCluster_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigEMCluster_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigEMCluster_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigEMCluster_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigEMCluster_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigEMCluster_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigEMCluster_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigEMCluster_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigEMCluster_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigEMCluster_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigEMCluster_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigEMCluster_v1>","xAOD::TrigEMClusterContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigEMCluster_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigEMCluster_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigEMCluster_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigEMCluster_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigEMCluster_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigEMCluster_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigEMCluster_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","B37C59EC-1A1E-467C-BFCE-52EA08694C60");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigEMClusterAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigEMClusterAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigEMClusterAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigEMClusterAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigEMClusterAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigEMClusterAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigEMClusterAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigEMClusterAuxContainer_v1*)
   {
      ::xAOD::TrigEMClusterAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigEMClusterAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigEMClusterAuxContainer_v1", "xAODTrigCalo/versions/TrigEMClusterAuxContainer_v1.h", 26,
                  typeid(::xAOD::TrigEMClusterAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigEMClusterAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigEMClusterAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigEMClusterAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigEMClusterAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigEMClusterAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigEMClusterAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigEMClusterAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigEMClusterAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigEMClusterAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigEMClusterAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigEMClusterAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigEMClusterAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigEMClusterAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigEMClusterAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","CE08F151-A2BA-44ED-B854-7D8B9C8679DE");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrigEMCluster_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrigEMCluster_v1> >","ElementLink<xAOD::TrigEMClusterContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigCaloCluster_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigCaloCluster_v1 : new ::xAOD::TrigCaloCluster_v1;
   }
   static void *newArray_xAODcLcLTrigCaloCluster_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigCaloCluster_v1[nElements] : new ::xAOD::TrigCaloCluster_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigCaloCluster_v1(void *p) {
      delete ((::xAOD::TrigCaloCluster_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigCaloCluster_v1(void *p) {
      delete [] ((::xAOD::TrigCaloCluster_v1*)p);
   }
   static void destruct_xAODcLcLTrigCaloCluster_v1(void *p) {
      typedef ::xAOD::TrigCaloCluster_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigCaloCluster_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigCaloCluster_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigCaloCluster_v1> : new ::DataVector<xAOD::TrigCaloCluster_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigCaloCluster_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigCaloCluster_v1>[nElements] : new ::DataVector<xAOD::TrigCaloCluster_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigCaloCluster_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigCaloCluster_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigCaloCluster_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigCaloCluster_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigCaloCluster_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigCaloCluster_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigCaloCluster_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigCaloClusterAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigCaloClusterAuxContainer_v1 : new ::xAOD::TrigCaloClusterAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigCaloClusterAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigCaloClusterAuxContainer_v1[nElements] : new ::xAOD::TrigCaloClusterAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigCaloClusterAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigCaloClusterAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigCaloClusterAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigCaloClusterAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigCaloClusterAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigCaloClusterAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigCaloClusterAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > : new ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigEMCluster_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigEMCluster_v1 : new ::xAOD::TrigEMCluster_v1;
   }
   static void *newArray_xAODcLcLTrigEMCluster_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigEMCluster_v1[nElements] : new ::xAOD::TrigEMCluster_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigEMCluster_v1(void *p) {
      delete ((::xAOD::TrigEMCluster_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigEMCluster_v1(void *p) {
      delete [] ((::xAOD::TrigEMCluster_v1*)p);
   }
   static void destruct_xAODcLcLTrigEMCluster_v1(void *p) {
      typedef ::xAOD::TrigEMCluster_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigEMCluster_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigEMCluster_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigEMCluster_v1> : new ::DataVector<xAOD::TrigEMCluster_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigEMCluster_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigEMCluster_v1>[nElements] : new ::DataVector<xAOD::TrigEMCluster_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigEMCluster_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigEMCluster_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigEMCluster_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigEMCluster_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigEMCluster_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigEMCluster_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigEMCluster_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigEMClusterAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigEMClusterAuxContainer_v1 : new ::xAOD::TrigEMClusterAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigEMClusterAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigEMClusterAuxContainer_v1[nElements] : new ::xAOD::TrigEMClusterAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigEMClusterAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigEMClusterAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigEMClusterAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigEMClusterAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigEMClusterAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigEMClusterAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigEMClusterAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> > : new ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrigEMCluster_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > : new vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigEMCluster_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > : new vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigCaloCluster_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTrigCalo_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigCalo/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigCalo",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigCalo/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigCalo/TrigCaloClusterContainer.h")))  TrigCaloCluster_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@4A2F55AF-D465-42A8-A4CF-2DB84D9628E5)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigCalo/versions/TrigCaloClusterAuxContainer_v1.h")))  TrigCaloClusterAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigCalo/TrigEMClusterContainer.h")))  TrigEMCluster_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@CE08F151-A2BA-44ED-B854-7D8B9C8679DE)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigCalo/versions/TrigEMClusterAuxContainer_v1.h")))  TrigEMClusterAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigCalo"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTrigCaloDict.h 631117 2014-11-26 08:48:22Z gwatts $
#ifndef XAODTRIGCALO_XAODTRIGCALODICT_H
#define XAODTRIGCALO_XAODTRIGCALODICT_H

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODTrigCalo/TrigCaloClusterContainer.h"
#include "xAODTrigCalo/TrigEMClusterContainer.h"
#include "xAODTrigCalo/versions/TrigCaloCluster_v1.h"
#include "xAODTrigCalo/versions/TrigCaloClusterContainer_v1.h"
#include "xAODTrigCalo/versions/TrigCaloClusterAuxContainer_v1.h"
#include "xAODTrigCalo/versions/TrigEMCluster_v1.h"
#include "xAODTrigCalo/versions/TrigEMClusterContainer_v1.h"
#include "xAODTrigCalo/versions/TrigEMClusterAuxContainer_v1.h"

namespace{
  struct GCCXML_DUMMY_INSTANTIATION_XAODTRIGCALO {

    xAOD::TrigCaloClusterContainer_v1 c3;
    ElementLink< xAOD::TrigCaloClusterContainer_v1 > a1;
    std::vector< ElementLink< xAOD::TrigCaloClusterContainer_v1 > > a3;
    std::vector< std::vector< ElementLink< xAOD::TrigCaloClusterContainer_v1 > > > a4;

    xAOD::TrigEMClusterContainer_v1 c4;
    ElementLink< xAOD::TrigEMClusterContainer_v1 > b1;
    std::vector< ElementLink< xAOD::TrigEMClusterContainer_v1 > > b3;
    std::vector< std::vector< ElementLink< xAOD::TrigEMClusterContainer_v1 > > > b4;
  };
}

#endif // XAODTRIGCALO_XAODTRIGCALODICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataVector<xAOD::TrigCaloCluster_v1>", payloadCode, "@",
"DataVector<xAOD::TrigEMCluster_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrigCaloCluster_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrigEMCluster_v1> >", payloadCode, "@",
"ElementLink<xAOD::TrigCaloClusterContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::TrigEMClusterContainer_v1>", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::TrigCaloClusterContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::TrigEMClusterContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrigCaloClusterContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrigEMClusterContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrigCaloCluster_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrigEMCluster_v1> > > >", payloadCode, "@",
"xAOD::TrigCaloClusterAuxContainer_v1", payloadCode, "@",
"xAOD::TrigCaloClusterContainer_v1", payloadCode, "@",
"xAOD::TrigCaloCluster_v1", payloadCode, "@",
"xAOD::TrigEMClusterAuxContainer_v1", payloadCode, "@",
"xAOD::TrigEMClusterContainer_v1", payloadCode, "@",
"xAOD::TrigEMCluster_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigCalo_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigCalo_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigCalo_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigCalo_Reflex() {
  TriggerDictionaryInitialization_xAODTrigCalo_Reflex_Impl();
}

// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIElectronPhotonSelectorToolsdIobjdIElectronPhotonSelectorTools_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/ElectronPhotonSelectorTools/ElectronPhotonSelectorTools/ElectronPhotonSelectorToolsDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *egammaPIDcLcLROOT6_NamespaceAutoloadHook_Dictionary();
   static void egammaPIDcLcLROOT6_NamespaceAutoloadHook_TClassManip(TClass*);
   static void *new_egammaPIDcLcLROOT6_NamespaceAutoloadHook(void *p = 0);
   static void *newArray_egammaPIDcLcLROOT6_NamespaceAutoloadHook(Long_t size, void *p);
   static void delete_egammaPIDcLcLROOT6_NamespaceAutoloadHook(void *p);
   static void deleteArray_egammaPIDcLcLROOT6_NamespaceAutoloadHook(void *p);
   static void destruct_egammaPIDcLcLROOT6_NamespaceAutoloadHook(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egammaPID::ROOT6_NamespaceAutoloadHook*)
   {
      ::egammaPID::ROOT6_NamespaceAutoloadHook *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egammaPID::ROOT6_NamespaceAutoloadHook));
      static ::ROOT::TGenericClassInfo 
         instance("egammaPID::ROOT6_NamespaceAutoloadHook", "ElectronPhotonSelectorTools/egammaPIDdefs.h", 752,
                  typeid(::egammaPID::ROOT6_NamespaceAutoloadHook), DefineBehavior(ptr, ptr),
                  &egammaPIDcLcLROOT6_NamespaceAutoloadHook_Dictionary, isa_proxy, 0,
                  sizeof(::egammaPID::ROOT6_NamespaceAutoloadHook) );
      instance.SetNew(&new_egammaPIDcLcLROOT6_NamespaceAutoloadHook);
      instance.SetNewArray(&newArray_egammaPIDcLcLROOT6_NamespaceAutoloadHook);
      instance.SetDelete(&delete_egammaPIDcLcLROOT6_NamespaceAutoloadHook);
      instance.SetDeleteArray(&deleteArray_egammaPIDcLcLROOT6_NamespaceAutoloadHook);
      instance.SetDestructor(&destruct_egammaPIDcLcLROOT6_NamespaceAutoloadHook);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egammaPID::ROOT6_NamespaceAutoloadHook*)
   {
      return GenerateInitInstanceLocal((::egammaPID::ROOT6_NamespaceAutoloadHook*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::egammaPID::ROOT6_NamespaceAutoloadHook*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egammaPIDcLcLROOT6_NamespaceAutoloadHook_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egammaPID::ROOT6_NamespaceAutoloadHook*)0x0)->GetClass();
      egammaPIDcLcLROOT6_NamespaceAutoloadHook_TClassManip(theClass);
   return theClass;
   }

   static void egammaPIDcLcLROOT6_NamespaceAutoloadHook_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *LikeEnumcLcLROOT6_NamespaceAutoloadHook_Dictionary();
   static void LikeEnumcLcLROOT6_NamespaceAutoloadHook_TClassManip(TClass*);
   static void *new_LikeEnumcLcLROOT6_NamespaceAutoloadHook(void *p = 0);
   static void *newArray_LikeEnumcLcLROOT6_NamespaceAutoloadHook(Long_t size, void *p);
   static void delete_LikeEnumcLcLROOT6_NamespaceAutoloadHook(void *p);
   static void deleteArray_LikeEnumcLcLROOT6_NamespaceAutoloadHook(void *p);
   static void destruct_LikeEnumcLcLROOT6_NamespaceAutoloadHook(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LikeEnum::ROOT6_NamespaceAutoloadHook*)
   {
      ::LikeEnum::ROOT6_NamespaceAutoloadHook *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::LikeEnum::ROOT6_NamespaceAutoloadHook));
      static ::ROOT::TGenericClassInfo 
         instance("LikeEnum::ROOT6_NamespaceAutoloadHook", "ElectronPhotonSelectorTools/TElectronLikelihoodTool.h", 158,
                  typeid(::LikeEnum::ROOT6_NamespaceAutoloadHook), DefineBehavior(ptr, ptr),
                  &LikeEnumcLcLROOT6_NamespaceAutoloadHook_Dictionary, isa_proxy, 0,
                  sizeof(::LikeEnum::ROOT6_NamespaceAutoloadHook) );
      instance.SetNew(&new_LikeEnumcLcLROOT6_NamespaceAutoloadHook);
      instance.SetNewArray(&newArray_LikeEnumcLcLROOT6_NamespaceAutoloadHook);
      instance.SetDelete(&delete_LikeEnumcLcLROOT6_NamespaceAutoloadHook);
      instance.SetDeleteArray(&deleteArray_LikeEnumcLcLROOT6_NamespaceAutoloadHook);
      instance.SetDestructor(&destruct_LikeEnumcLcLROOT6_NamespaceAutoloadHook);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LikeEnum::ROOT6_NamespaceAutoloadHook*)
   {
      return GenerateInitInstanceLocal((::LikeEnum::ROOT6_NamespaceAutoloadHook*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::LikeEnum::ROOT6_NamespaceAutoloadHook*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *LikeEnumcLcLROOT6_NamespaceAutoloadHook_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::LikeEnum::ROOT6_NamespaceAutoloadHook*)0x0)->GetClass();
      LikeEnumcLcLROOT6_NamespaceAutoloadHook_TClassManip(theClass);
   return theClass;
   }

   static void LikeEnumcLcLROOT6_NamespaceAutoloadHook_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AsgElectronIsEMSelector_Dictionary();
   static void AsgElectronIsEMSelector_TClassManip(TClass*);
   static void delete_AsgElectronIsEMSelector(void *p);
   static void deleteArray_AsgElectronIsEMSelector(void *p);
   static void destruct_AsgElectronIsEMSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AsgElectronIsEMSelector*)
   {
      ::AsgElectronIsEMSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AsgElectronIsEMSelector));
      static ::ROOT::TGenericClassInfo 
         instance("AsgElectronIsEMSelector", "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h", 29,
                  typeid(::AsgElectronIsEMSelector), DefineBehavior(ptr, ptr),
                  &AsgElectronIsEMSelector_Dictionary, isa_proxy, 0,
                  sizeof(::AsgElectronIsEMSelector) );
      instance.SetDelete(&delete_AsgElectronIsEMSelector);
      instance.SetDeleteArray(&deleteArray_AsgElectronIsEMSelector);
      instance.SetDestructor(&destruct_AsgElectronIsEMSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AsgElectronIsEMSelector*)
   {
      return GenerateInitInstanceLocal((::AsgElectronIsEMSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AsgElectronIsEMSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AsgElectronIsEMSelector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AsgElectronIsEMSelector*)0x0)->GetClass();
      AsgElectronIsEMSelector_TClassManip(theClass);
   return theClass;
   }

   static void AsgElectronIsEMSelector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AsgForwardElectronIsEMSelector_Dictionary();
   static void AsgForwardElectronIsEMSelector_TClassManip(TClass*);
   static void delete_AsgForwardElectronIsEMSelector(void *p);
   static void deleteArray_AsgForwardElectronIsEMSelector(void *p);
   static void destruct_AsgForwardElectronIsEMSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AsgForwardElectronIsEMSelector*)
   {
      ::AsgForwardElectronIsEMSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AsgForwardElectronIsEMSelector));
      static ::ROOT::TGenericClassInfo 
         instance("AsgForwardElectronIsEMSelector", "ElectronPhotonSelectorTools/AsgForwardElectronIsEMSelector.h", 29,
                  typeid(::AsgForwardElectronIsEMSelector), DefineBehavior(ptr, ptr),
                  &AsgForwardElectronIsEMSelector_Dictionary, isa_proxy, 0,
                  sizeof(::AsgForwardElectronIsEMSelector) );
      instance.SetDelete(&delete_AsgForwardElectronIsEMSelector);
      instance.SetDeleteArray(&deleteArray_AsgForwardElectronIsEMSelector);
      instance.SetDestructor(&destruct_AsgForwardElectronIsEMSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AsgForwardElectronIsEMSelector*)
   {
      return GenerateInitInstanceLocal((::AsgForwardElectronIsEMSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AsgForwardElectronIsEMSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AsgForwardElectronIsEMSelector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AsgForwardElectronIsEMSelector*)0x0)->GetClass();
      AsgForwardElectronIsEMSelector_TClassManip(theClass);
   return theClass;
   }

   static void AsgForwardElectronIsEMSelector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AsgPhotonIsEMSelector_Dictionary();
   static void AsgPhotonIsEMSelector_TClassManip(TClass*);
   static void delete_AsgPhotonIsEMSelector(void *p);
   static void deleteArray_AsgPhotonIsEMSelector(void *p);
   static void destruct_AsgPhotonIsEMSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AsgPhotonIsEMSelector*)
   {
      ::AsgPhotonIsEMSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AsgPhotonIsEMSelector));
      static ::ROOT::TGenericClassInfo 
         instance("AsgPhotonIsEMSelector", "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h", 32,
                  typeid(::AsgPhotonIsEMSelector), DefineBehavior(ptr, ptr),
                  &AsgPhotonIsEMSelector_Dictionary, isa_proxy, 0,
                  sizeof(::AsgPhotonIsEMSelector) );
      instance.SetDelete(&delete_AsgPhotonIsEMSelector);
      instance.SetDeleteArray(&deleteArray_AsgPhotonIsEMSelector);
      instance.SetDestructor(&destruct_AsgPhotonIsEMSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AsgPhotonIsEMSelector*)
   {
      return GenerateInitInstanceLocal((::AsgPhotonIsEMSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AsgPhotonIsEMSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AsgPhotonIsEMSelector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AsgPhotonIsEMSelector*)0x0)->GetClass();
      AsgPhotonIsEMSelector_TClassManip(theClass);
   return theClass;
   }

   static void AsgPhotonIsEMSelector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AsgElectronMultiLeptonSelector_Dictionary();
   static void AsgElectronMultiLeptonSelector_TClassManip(TClass*);
   static void delete_AsgElectronMultiLeptonSelector(void *p);
   static void deleteArray_AsgElectronMultiLeptonSelector(void *p);
   static void destruct_AsgElectronMultiLeptonSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AsgElectronMultiLeptonSelector*)
   {
      ::AsgElectronMultiLeptonSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AsgElectronMultiLeptonSelector));
      static ::ROOT::TGenericClassInfo 
         instance("AsgElectronMultiLeptonSelector", "ElectronPhotonSelectorTools/AsgElectronMultiLeptonSelector.h", 26,
                  typeid(::AsgElectronMultiLeptonSelector), DefineBehavior(ptr, ptr),
                  &AsgElectronMultiLeptonSelector_Dictionary, isa_proxy, 0,
                  sizeof(::AsgElectronMultiLeptonSelector) );
      instance.SetDelete(&delete_AsgElectronMultiLeptonSelector);
      instance.SetDeleteArray(&deleteArray_AsgElectronMultiLeptonSelector);
      instance.SetDestructor(&destruct_AsgElectronMultiLeptonSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AsgElectronMultiLeptonSelector*)
   {
      return GenerateInitInstanceLocal((::AsgElectronMultiLeptonSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AsgElectronMultiLeptonSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AsgElectronMultiLeptonSelector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AsgElectronMultiLeptonSelector*)0x0)->GetClass();
      AsgElectronMultiLeptonSelector_TClassManip(theClass);
   return theClass;
   }

   static void AsgElectronMultiLeptonSelector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AsgElectronLikelihoodTool_Dictionary();
   static void AsgElectronLikelihoodTool_TClassManip(TClass*);
   static void delete_AsgElectronLikelihoodTool(void *p);
   static void deleteArray_AsgElectronLikelihoodTool(void *p);
   static void destruct_AsgElectronLikelihoodTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AsgElectronLikelihoodTool*)
   {
      ::AsgElectronLikelihoodTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AsgElectronLikelihoodTool));
      static ::ROOT::TGenericClassInfo 
         instance("AsgElectronLikelihoodTool", "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h", 14,
                  typeid(::AsgElectronLikelihoodTool), DefineBehavior(ptr, ptr),
                  &AsgElectronLikelihoodTool_Dictionary, isa_proxy, 0,
                  sizeof(::AsgElectronLikelihoodTool) );
      instance.SetDelete(&delete_AsgElectronLikelihoodTool);
      instance.SetDeleteArray(&deleteArray_AsgElectronLikelihoodTool);
      instance.SetDestructor(&destruct_AsgElectronLikelihoodTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AsgElectronLikelihoodTool*)
   {
      return GenerateInitInstanceLocal((::AsgElectronLikelihoodTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AsgElectronLikelihoodTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AsgElectronLikelihoodTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AsgElectronLikelihoodTool*)0x0)->GetClass();
      AsgElectronLikelihoodTool_TClassManip(theClass);
   return theClass;
   }

   static void AsgElectronLikelihoodTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *IEGammaAmbiguityTool_Dictionary();
   static void IEGammaAmbiguityTool_TClassManip(TClass*);
   static void delete_IEGammaAmbiguityTool(void *p);
   static void deleteArray_IEGammaAmbiguityTool(void *p);
   static void destruct_IEGammaAmbiguityTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IEGammaAmbiguityTool*)
   {
      ::IEGammaAmbiguityTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IEGammaAmbiguityTool));
      static ::ROOT::TGenericClassInfo 
         instance("IEGammaAmbiguityTool", "ElectronPhotonSelectorTools/IEGammaAmbiguityTool.h", 26,
                  typeid(::IEGammaAmbiguityTool), DefineBehavior(ptr, ptr),
                  &IEGammaAmbiguityTool_Dictionary, isa_proxy, 0,
                  sizeof(::IEGammaAmbiguityTool) );
      instance.SetDelete(&delete_IEGammaAmbiguityTool);
      instance.SetDeleteArray(&deleteArray_IEGammaAmbiguityTool);
      instance.SetDestructor(&destruct_IEGammaAmbiguityTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IEGammaAmbiguityTool*)
   {
      return GenerateInitInstanceLocal((::IEGammaAmbiguityTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::IEGammaAmbiguityTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IEGammaAmbiguityTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IEGammaAmbiguityTool*)0x0)->GetClass();
      IEGammaAmbiguityTool_TClassManip(theClass);
   return theClass;
   }

   static void IEGammaAmbiguityTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *EGammaAmbiguityTool_Dictionary();
   static void EGammaAmbiguityTool_TClassManip(TClass*);
   static void delete_EGammaAmbiguityTool(void *p);
   static void deleteArray_EGammaAmbiguityTool(void *p);
   static void destruct_EGammaAmbiguityTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::EGammaAmbiguityTool*)
   {
      ::EGammaAmbiguityTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::EGammaAmbiguityTool));
      static ::ROOT::TGenericClassInfo 
         instance("EGammaAmbiguityTool", "ElectronPhotonSelectorTools/EGammaAmbiguityTool.h", 19,
                  typeid(::EGammaAmbiguityTool), DefineBehavior(ptr, ptr),
                  &EGammaAmbiguityTool_Dictionary, isa_proxy, 0,
                  sizeof(::EGammaAmbiguityTool) );
      instance.SetDelete(&delete_EGammaAmbiguityTool);
      instance.SetDeleteArray(&deleteArray_EGammaAmbiguityTool);
      instance.SetDestructor(&destruct_EGammaAmbiguityTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::EGammaAmbiguityTool*)
   {
      return GenerateInitInstanceLocal((::EGammaAmbiguityTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::EGammaAmbiguityTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *EGammaAmbiguityTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::EGammaAmbiguityTool*)0x0)->GetClass();
      EGammaAmbiguityTool_TClassManip(theClass);
   return theClass;
   }

   static void EGammaAmbiguityTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_egammaPIDcLcLROOT6_NamespaceAutoloadHook(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::egammaPID::ROOT6_NamespaceAutoloadHook : new ::egammaPID::ROOT6_NamespaceAutoloadHook;
   }
   static void *newArray_egammaPIDcLcLROOT6_NamespaceAutoloadHook(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::egammaPID::ROOT6_NamespaceAutoloadHook[nElements] : new ::egammaPID::ROOT6_NamespaceAutoloadHook[nElements];
   }
   // Wrapper around operator delete
   static void delete_egammaPIDcLcLROOT6_NamespaceAutoloadHook(void *p) {
      delete ((::egammaPID::ROOT6_NamespaceAutoloadHook*)p);
   }
   static void deleteArray_egammaPIDcLcLROOT6_NamespaceAutoloadHook(void *p) {
      delete [] ((::egammaPID::ROOT6_NamespaceAutoloadHook*)p);
   }
   static void destruct_egammaPIDcLcLROOT6_NamespaceAutoloadHook(void *p) {
      typedef ::egammaPID::ROOT6_NamespaceAutoloadHook current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egammaPID::ROOT6_NamespaceAutoloadHook

namespace ROOT {
   // Wrappers around operator new
   static void *new_LikeEnumcLcLROOT6_NamespaceAutoloadHook(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::LikeEnum::ROOT6_NamespaceAutoloadHook : new ::LikeEnum::ROOT6_NamespaceAutoloadHook;
   }
   static void *newArray_LikeEnumcLcLROOT6_NamespaceAutoloadHook(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::LikeEnum::ROOT6_NamespaceAutoloadHook[nElements] : new ::LikeEnum::ROOT6_NamespaceAutoloadHook[nElements];
   }
   // Wrapper around operator delete
   static void delete_LikeEnumcLcLROOT6_NamespaceAutoloadHook(void *p) {
      delete ((::LikeEnum::ROOT6_NamespaceAutoloadHook*)p);
   }
   static void deleteArray_LikeEnumcLcLROOT6_NamespaceAutoloadHook(void *p) {
      delete [] ((::LikeEnum::ROOT6_NamespaceAutoloadHook*)p);
   }
   static void destruct_LikeEnumcLcLROOT6_NamespaceAutoloadHook(void *p) {
      typedef ::LikeEnum::ROOT6_NamespaceAutoloadHook current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LikeEnum::ROOT6_NamespaceAutoloadHook

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AsgElectronIsEMSelector(void *p) {
      delete ((::AsgElectronIsEMSelector*)p);
   }
   static void deleteArray_AsgElectronIsEMSelector(void *p) {
      delete [] ((::AsgElectronIsEMSelector*)p);
   }
   static void destruct_AsgElectronIsEMSelector(void *p) {
      typedef ::AsgElectronIsEMSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AsgElectronIsEMSelector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AsgForwardElectronIsEMSelector(void *p) {
      delete ((::AsgForwardElectronIsEMSelector*)p);
   }
   static void deleteArray_AsgForwardElectronIsEMSelector(void *p) {
      delete [] ((::AsgForwardElectronIsEMSelector*)p);
   }
   static void destruct_AsgForwardElectronIsEMSelector(void *p) {
      typedef ::AsgForwardElectronIsEMSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AsgForwardElectronIsEMSelector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AsgPhotonIsEMSelector(void *p) {
      delete ((::AsgPhotonIsEMSelector*)p);
   }
   static void deleteArray_AsgPhotonIsEMSelector(void *p) {
      delete [] ((::AsgPhotonIsEMSelector*)p);
   }
   static void destruct_AsgPhotonIsEMSelector(void *p) {
      typedef ::AsgPhotonIsEMSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AsgPhotonIsEMSelector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AsgElectronMultiLeptonSelector(void *p) {
      delete ((::AsgElectronMultiLeptonSelector*)p);
   }
   static void deleteArray_AsgElectronMultiLeptonSelector(void *p) {
      delete [] ((::AsgElectronMultiLeptonSelector*)p);
   }
   static void destruct_AsgElectronMultiLeptonSelector(void *p) {
      typedef ::AsgElectronMultiLeptonSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AsgElectronMultiLeptonSelector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AsgElectronLikelihoodTool(void *p) {
      delete ((::AsgElectronLikelihoodTool*)p);
   }
   static void deleteArray_AsgElectronLikelihoodTool(void *p) {
      delete [] ((::AsgElectronLikelihoodTool*)p);
   }
   static void destruct_AsgElectronLikelihoodTool(void *p) {
      typedef ::AsgElectronLikelihoodTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AsgElectronLikelihoodTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IEGammaAmbiguityTool(void *p) {
      delete ((::IEGammaAmbiguityTool*)p);
   }
   static void deleteArray_IEGammaAmbiguityTool(void *p) {
      delete [] ((::IEGammaAmbiguityTool*)p);
   }
   static void destruct_IEGammaAmbiguityTool(void *p) {
      typedef ::IEGammaAmbiguityTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IEGammaAmbiguityTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_EGammaAmbiguityTool(void *p) {
      delete ((::EGammaAmbiguityTool*)p);
   }
   static void deleteArray_EGammaAmbiguityTool(void *p) {
      delete [] ((::EGammaAmbiguityTool*)p);
   }
   static void destruct_EGammaAmbiguityTool(void *p) {
      typedef ::EGammaAmbiguityTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::EGammaAmbiguityTool

namespace {
  void TriggerDictionaryInitialization_ElectronPhotonSelectorTools_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/ElectronPhotonSelectorTools/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/ElectronPhotonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/ElectronPhotonSelectorTools/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace egammaPID{struct __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/egammaPIDdefs.h")))  ROOT6_NamespaceAutoloadHook;}
namespace LikeEnum{struct __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h")))  ROOT6_NamespaceAutoloadHook;}
class __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h")))  AsgElectronIsEMSelector;
class __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/AsgForwardElectronIsEMSelector.h")))  AsgForwardElectronIsEMSelector;
class __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h")))  AsgPhotonIsEMSelector;
class __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/AsgElectronMultiLeptonSelector.h")))  AsgElectronMultiLeptonSelector;
class __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h")))  AsgElectronLikelihoodTool;
class __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/IEGammaAmbiguityTool.h")))  IEGammaAmbiguityTool;
class __attribute__((annotate("$clingAutoload$ElectronPhotonSelectorTools/EGammaAmbiguityTool.h")))  EGammaAmbiguityTool;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "ElectronPhotonSelectorTools"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*-c++-*-

#ifndef __ELECTRONPHOTONSELECTORTOOLSDICT__
#define __ELECTRONPHOTONSELECTORTOOLSDICT__

/**
   @brief For dictionary generation.

   @author Tulay Cuhadar Donszelmann, Jovan Mitrevski
   @date   September 2011

*/

#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronMultiLeptonSelector.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronPhotonIsEMSelectorConfigHelper.h"
#include "ElectronPhotonSelectorTools/TElectronLikelihoodTool.h" //For the LikelihoodEnums
#include "ElectronPhotonSelectorTools/IEGammaAmbiguityTool.h"
#include "ElectronPhotonSelectorTools/EGammaAmbiguityTool.h" // for python

#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"AsgConfigHelper::HelperDouble", payloadCode, "@",
"AsgConfigHelper::HelperFloat", payloadCode, "@",
"AsgConfigHelper::HelperInt", payloadCode, "@",
"AsgElectronIsEMSelector", payloadCode, "@",
"AsgElectronLikelihoodTool", payloadCode, "@",
"AsgElectronMultiLeptonSelector", payloadCode, "@",
"AsgForwardElectronIsEMSelector", payloadCode, "@",
"AsgPhotonIsEMSelector", payloadCode, "@",
"EGammaAmbiguityTool", payloadCode, "@",
"IEGammaAmbiguityTool", payloadCode, "@",
"LikeEnum::Menu", payloadCode, "@",
"LikeEnum::ROOT6_NamespaceAutoloadHook", payloadCode, "@",
"egammaPID::ALLNOTRT_ELECTRON", payloadCode, "@",
"egammaPID::ALL_ELECTRON", payloadCode, "@",
"egammaPID::AMBIGUITYRESOLVE_PHOTON", payloadCode, "@",
"egammaPID::BitDefElectron", payloadCode, "@",
"egammaPID::BitDefForwardElectron", payloadCode, "@",
"egammaPID::BitDefPhoton", payloadCode, "@",
"egammaPID::CALOBACK_ELECTRON", payloadCode, "@",
"egammaPID::CALOMIDDLE_ELECTRON", payloadCode, "@",
"egammaPID::CALOMIDDLE_ELECTRON_HLT", payloadCode, "@",
"egammaPID::CALOMIDDLE_PHOTON", payloadCode, "@",
"egammaPID::CALORIMETRICISOLATION_ELECTRON", payloadCode, "@",
"egammaPID::CALORIMETRICISOLATION_PHOTON", payloadCode, "@",
"egammaPID::CALOSTRIPS_ELECTRON", payloadCode, "@",
"egammaPID::CALOSTRIPS_LOOSE_ELECTRON", payloadCode, "@",
"egammaPID::CALOSTRIPS_PHOTONMEDIUM", payloadCode, "@",
"egammaPID::CALOSTRIPS_PHOTONTIGHT", payloadCode, "@",
"egammaPID::CALOTRACKISOLATION_ELECTRON", payloadCode, "@",
"egammaPID::CALOTRACKISOLATION_PHOTON", payloadCode, "@",
"egammaPID::CALO_ELECTRON", payloadCode, "@",
"egammaPID::CALO_ELECTRON_HLT", payloadCode, "@",
"egammaPID::CALO_PHOTON_RETA_WETA2_ERATIO", payloadCode, "@",
"egammaPID::CONVMATCH_ELECTRON", payloadCode, "@",
"egammaPID::EgPidUndefined", payloadCode, "@",
"egammaPID::ElectronLoose", payloadCode, "@",
"egammaPID::ElectronLoose1", payloadCode, "@",
"egammaPID::ElectronLooseHLT", payloadCode, "@",
"egammaPID::ElectronLooseIso", payloadCode, "@",
"egammaPID::ElectronLoosePP", payloadCode, "@",
"egammaPID::ElectronLoosePPIso", payloadCode, "@",
"egammaPID::ElectronMedium", payloadCode, "@",
"egammaPID::ElectronMedium1", payloadCode, "@",
"egammaPID::ElectronMediumHLT", payloadCode, "@",
"egammaPID::ElectronMediumIso", payloadCode, "@",
"egammaPID::ElectronMediumIso_WithTrackMatch", payloadCode, "@",
"egammaPID::ElectronMediumIso_WithoutTrackMatch", payloadCode, "@",
"egammaPID::ElectronMediumPP", payloadCode, "@",
"egammaPID::ElectronMediumPPIso", payloadCode, "@",
"egammaPID::ElectronMedium_WithTrackMatch", payloadCode, "@",
"egammaPID::ElectronMedium_WithoutTrackMatch", payloadCode, "@",
"egammaPID::ElectronTight", payloadCode, "@",
"egammaPID::ElectronTight1", payloadCode, "@",
"egammaPID::ElectronTightHLT", payloadCode, "@",
"egammaPID::ElectronTightIso", payloadCode, "@",
"egammaPID::ElectronTightIso_WithLooseEtaTrackMatch", payloadCode, "@",
"egammaPID::ElectronTightIso_WithTightEtaTrackMatch", payloadCode, "@",
"egammaPID::ElectronTightIso_WithTrackMatch", payloadCode, "@",
"egammaPID::ElectronTightIso_WithoutTrackMatch", payloadCode, "@",
"egammaPID::ElectronTightPP", payloadCode, "@",
"egammaPID::ElectronTightPPIso", payloadCode, "@",
"egammaPID::ElectronTight_NoConvCut", payloadCode, "@",
"egammaPID::ElectronTight_WithLooseEtaTrackMatch", payloadCode, "@",
"egammaPID::ElectronTight_WithTightEtaTrackMatch", payloadCode, "@",
"egammaPID::ElectronTight_WithTrackMatch", payloadCode, "@",
"egammaPID::ElectronTight_WithTrackMatch_NoConvCut", payloadCode, "@",
"egammaPID::ElectronTight_WithoutTrackMatch", payloadCode, "@",
"egammaPID::ElectronTight_WithoutTrackMatch_NoConvCut", payloadCode, "@",
"egammaPID::ForwardElectronLoose", payloadCode, "@",
"egammaPID::ForwardElectronTight", payloadCode, "@",
"egammaPID::HADLEAKETA_ELECTRON", payloadCode, "@",
"egammaPID::HADLEAKETA_PHOTON", payloadCode, "@",
"egammaPID::HADLEAKETA_PHOTON_EF", payloadCode, "@",
"egammaPID::ID_ForwardElectron", payloadCode, "@",
"egammaPID::ISOLATION_ELECTRON", payloadCode, "@",
"egammaPID::ISOLATION_PHOTON", payloadCode, "@",
"egammaPID::PID", payloadCode, "@",
"egammaPID::PhotonLoose", payloadCode, "@",
"egammaPID::PhotonLooseAR", payloadCode, "@",
"egammaPID::PhotonLooseARIso", payloadCode, "@",
"egammaPID::PhotonLooseEF", payloadCode, "@",
"egammaPID::PhotonLooseIso", payloadCode, "@",
"egammaPID::PhotonMedium", payloadCode, "@",
"egammaPID::PhotonMediumAR", payloadCode, "@",
"egammaPID::PhotonMediumARIso", payloadCode, "@",
"egammaPID::PhotonMediumEF", payloadCode, "@",
"egammaPID::PhotonMediumIso", payloadCode, "@",
"egammaPID::PhotonTight", payloadCode, "@",
"egammaPID::PhotonTightAR", payloadCode, "@",
"egammaPID::PhotonTightARIso", payloadCode, "@",
"egammaPID::PhotonTightIso", payloadCode, "@",
"egammaPID::ROOT6_NamespaceAutoloadHook", payloadCode, "@",
"egammaPID::TRACKINGISOLATION_ELECTRON", payloadCode, "@",
"egammaPID::TRACKINGISOLATION_PHOTON", payloadCode, "@",
"egammaPID::TRACKINGLOOSE_ELECTRON", payloadCode, "@",
"egammaPID::TRACKINGNOBLAYER_ELECTRON", payloadCode, "@",
"egammaPID::TRACKING_ELECTRON", payloadCode, "@",
"egammaPID::TRACKMATCHDETATIGHT_ELECTRON", payloadCode, "@",
"egammaPID::TRACKMATCHDETA_ELECTRON", payloadCode, "@",
"egammaPID::TRACKMATCHNOEOVERP_ELECTRON", payloadCode, "@",
"egammaPID::TRACKMATCHTIGHT_ELECTRON", payloadCode, "@",
"egammaPID::TRACKMATCHTIGHT_ELECTRON_EF", payloadCode, "@",
"egammaPID::TRACKMATCH_ELECTRON", payloadCode, "@",
"egammaPID::TRACKMATCH_ELECTRON_EF", payloadCode, "@",
"egammaPID::TRACKMATCH_ELECTRON_NoEoP_EF", payloadCode, "@",
"egammaPID::TRACKMATCH_PHOTON", payloadCode, "@",
"egammaPID::TRT_ELECTRON", payloadCode, "@",
"egammaPID::TRT_EPROB_ELECTRON", payloadCode, "@",
"egammaPID::TRT_RATIO_ELECTRON", payloadCode, "@",
"egammaPID::egammaIDQuality", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("ElectronPhotonSelectorTools_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_ElectronPhotonSelectorTools_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_ElectronPhotonSelectorTools_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_ElectronPhotonSelectorTools_Reflex() {
  TriggerDictionaryInitialization_ElectronPhotonSelectorTools_Reflex_Impl();
}

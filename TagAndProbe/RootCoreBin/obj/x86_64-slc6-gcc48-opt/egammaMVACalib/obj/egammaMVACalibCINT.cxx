#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIegammaMVACalibdIobjdIegammaMVACalibCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "egammaMVACalib/egammaMVACalib.h"
#include "egammaMVACalib/Node.h"
#include "egammaMVACalib/BDT.h"
#include "egammaMVACalib/egammaMVATool.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *egammaMVACalib_Dictionary();
   static void egammaMVACalib_TClassManip(TClass*);
   static void delete_egammaMVACalib(void *p);
   static void deleteArray_egammaMVACalib(void *p);
   static void destruct_egammaMVACalib(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egammaMVACalib*)
   {
      ::egammaMVACalib *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egammaMVACalib));
      static ::ROOT::TGenericClassInfo 
         instance("egammaMVACalib", "egammaMVACalib/egammaMVACalib.h", 53,
                  typeid(::egammaMVACalib), DefineBehavior(ptr, ptr),
                  &egammaMVACalib_Dictionary, isa_proxy, 4,
                  sizeof(::egammaMVACalib) );
      instance.SetDelete(&delete_egammaMVACalib);
      instance.SetDeleteArray(&deleteArray_egammaMVACalib);
      instance.SetDestructor(&destruct_egammaMVACalib);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egammaMVACalib*)
   {
      return GenerateInitInstanceLocal((::egammaMVACalib*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::egammaMVACalib*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egammaMVACalib_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egammaMVACalib*)0x0)->GetClass();
      egammaMVACalib_TClassManip(theClass);
   return theClass;
   }

   static void egammaMVACalib_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *egammaMVACalibcLcLReaderID_Dictionary();
   static void egammaMVACalibcLcLReaderID_TClassManip(TClass*);
   static void *new_egammaMVACalibcLcLReaderID(void *p = 0);
   static void *newArray_egammaMVACalibcLcLReaderID(Long_t size, void *p);
   static void delete_egammaMVACalibcLcLReaderID(void *p);
   static void deleteArray_egammaMVACalibcLcLReaderID(void *p);
   static void destruct_egammaMVACalibcLcLReaderID(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egammaMVACalib::ReaderID*)
   {
      ::egammaMVACalib::ReaderID *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egammaMVACalib::ReaderID));
      static ::ROOT::TGenericClassInfo 
         instance("egammaMVACalib::ReaderID", "egammaMVACalib/egammaMVACalib.h", 61,
                  typeid(::egammaMVACalib::ReaderID), DefineBehavior(ptr, ptr),
                  &egammaMVACalibcLcLReaderID_Dictionary, isa_proxy, 4,
                  sizeof(::egammaMVACalib::ReaderID) );
      instance.SetNew(&new_egammaMVACalibcLcLReaderID);
      instance.SetNewArray(&newArray_egammaMVACalibcLcLReaderID);
      instance.SetDelete(&delete_egammaMVACalibcLcLReaderID);
      instance.SetDeleteArray(&deleteArray_egammaMVACalibcLcLReaderID);
      instance.SetDestructor(&destruct_egammaMVACalibcLcLReaderID);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egammaMVACalib::ReaderID*)
   {
      return GenerateInitInstanceLocal((::egammaMVACalib::ReaderID*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::egammaMVACalib::ReaderID*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egammaMVACalibcLcLReaderID_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egammaMVACalib::ReaderID*)0x0)->GetClass();
      egammaMVACalibcLcLReaderID_TClassManip(theClass);
   return theClass;
   }

   static void egammaMVACalibcLcLReaderID_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *egammaMVACalibNmspcLcLNode_Dictionary();
   static void egammaMVACalibNmspcLcLNode_TClassManip(TClass*);
   static void delete_egammaMVACalibNmspcLcLNode(void *p);
   static void deleteArray_egammaMVACalibNmspcLcLNode(void *p);
   static void destruct_egammaMVACalibNmspcLcLNode(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egammaMVACalibNmsp::Node*)
   {
      ::egammaMVACalibNmsp::Node *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egammaMVACalibNmsp::Node));
      static ::ROOT::TGenericClassInfo 
         instance("egammaMVACalibNmsp::Node", "egammaMVACalib/Node.h", 20,
                  typeid(::egammaMVACalibNmsp::Node), DefineBehavior(ptr, ptr),
                  &egammaMVACalibNmspcLcLNode_Dictionary, isa_proxy, 4,
                  sizeof(::egammaMVACalibNmsp::Node) );
      instance.SetDelete(&delete_egammaMVACalibNmspcLcLNode);
      instance.SetDeleteArray(&deleteArray_egammaMVACalibNmspcLcLNode);
      instance.SetDestructor(&destruct_egammaMVACalibNmspcLcLNode);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egammaMVACalibNmsp::Node*)
   {
      return GenerateInitInstanceLocal((::egammaMVACalibNmsp::Node*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::egammaMVACalibNmsp::Node*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egammaMVACalibNmspcLcLNode_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egammaMVACalibNmsp::Node*)0x0)->GetClass();
      egammaMVACalibNmspcLcLNode_TClassManip(theClass);
   return theClass;
   }

   static void egammaMVACalibNmspcLcLNode_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *egammaMVACalibNmspcLcLBDT_Dictionary();
   static void egammaMVACalibNmspcLcLBDT_TClassManip(TClass*);
   static void delete_egammaMVACalibNmspcLcLBDT(void *p);
   static void deleteArray_egammaMVACalibNmspcLcLBDT(void *p);
   static void destruct_egammaMVACalibNmspcLcLBDT(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egammaMVACalibNmsp::BDT*)
   {
      ::egammaMVACalibNmsp::BDT *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egammaMVACalibNmsp::BDT));
      static ::ROOT::TGenericClassInfo 
         instance("egammaMVACalibNmsp::BDT", "egammaMVACalib/BDT.h", 25,
                  typeid(::egammaMVACalibNmsp::BDT), DefineBehavior(ptr, ptr),
                  &egammaMVACalibNmspcLcLBDT_Dictionary, isa_proxy, 4,
                  sizeof(::egammaMVACalibNmsp::BDT) );
      instance.SetDelete(&delete_egammaMVACalibNmspcLcLBDT);
      instance.SetDeleteArray(&deleteArray_egammaMVACalibNmspcLcLBDT);
      instance.SetDestructor(&destruct_egammaMVACalibNmspcLcLBDT);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egammaMVACalibNmsp::BDT*)
   {
      return GenerateInitInstanceLocal((::egammaMVACalibNmsp::BDT*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::egammaMVACalibNmsp::BDT*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egammaMVACalibNmspcLcLBDT_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egammaMVACalibNmsp::BDT*)0x0)->GetClass();
      egammaMVACalibNmspcLcLBDT_TClassManip(theClass);
   return theClass;
   }

   static void egammaMVACalibNmspcLcLBDT_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *egammaMVATool_Dictionary();
   static void egammaMVATool_TClassManip(TClass*);
   static void delete_egammaMVATool(void *p);
   static void deleteArray_egammaMVATool(void *p);
   static void destruct_egammaMVATool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egammaMVATool*)
   {
      ::egammaMVATool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egammaMVATool));
      static ::ROOT::TGenericClassInfo 
         instance("egammaMVATool", "egammaMVACalib/egammaMVATool.h", 14,
                  typeid(::egammaMVATool), DefineBehavior(ptr, ptr),
                  &egammaMVATool_Dictionary, isa_proxy, 0,
                  sizeof(::egammaMVATool) );
      instance.SetDelete(&delete_egammaMVATool);
      instance.SetDeleteArray(&deleteArray_egammaMVATool);
      instance.SetDestructor(&destruct_egammaMVATool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egammaMVATool*)
   {
      return GenerateInitInstanceLocal((::egammaMVATool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::egammaMVATool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egammaMVATool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egammaMVATool*)0x0)->GetClass();
      egammaMVATool_TClassManip(theClass);
   return theClass;
   }

   static void egammaMVATool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_egammaMVACalib(void *p) {
      delete ((::egammaMVACalib*)p);
   }
   static void deleteArray_egammaMVACalib(void *p) {
      delete [] ((::egammaMVACalib*)p);
   }
   static void destruct_egammaMVACalib(void *p) {
      typedef ::egammaMVACalib current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egammaMVACalib

namespace ROOT {
   // Wrappers around operator new
   static void *new_egammaMVACalibcLcLReaderID(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::egammaMVACalib::ReaderID : new ::egammaMVACalib::ReaderID;
   }
   static void *newArray_egammaMVACalibcLcLReaderID(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::egammaMVACalib::ReaderID[nElements] : new ::egammaMVACalib::ReaderID[nElements];
   }
   // Wrapper around operator delete
   static void delete_egammaMVACalibcLcLReaderID(void *p) {
      delete ((::egammaMVACalib::ReaderID*)p);
   }
   static void deleteArray_egammaMVACalibcLcLReaderID(void *p) {
      delete [] ((::egammaMVACalib::ReaderID*)p);
   }
   static void destruct_egammaMVACalibcLcLReaderID(void *p) {
      typedef ::egammaMVACalib::ReaderID current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egammaMVACalib::ReaderID

namespace ROOT {
   // Wrapper around operator delete
   static void delete_egammaMVACalibNmspcLcLNode(void *p) {
      delete ((::egammaMVACalibNmsp::Node*)p);
   }
   static void deleteArray_egammaMVACalibNmspcLcLNode(void *p) {
      delete [] ((::egammaMVACalibNmsp::Node*)p);
   }
   static void destruct_egammaMVACalibNmspcLcLNode(void *p) {
      typedef ::egammaMVACalibNmsp::Node current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egammaMVACalibNmsp::Node

namespace ROOT {
   // Wrapper around operator delete
   static void delete_egammaMVACalibNmspcLcLBDT(void *p) {
      delete ((::egammaMVACalibNmsp::BDT*)p);
   }
   static void deleteArray_egammaMVACalibNmspcLcLBDT(void *p) {
      delete [] ((::egammaMVACalibNmsp::BDT*)p);
   }
   static void destruct_egammaMVACalibNmspcLcLBDT(void *p) {
      typedef ::egammaMVACalibNmsp::BDT current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egammaMVACalibNmsp::BDT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_egammaMVATool(void *p) {
      delete ((::egammaMVATool*)p);
   }
   static void deleteArray_egammaMVATool(void *p) {
      delete [] ((::egammaMVATool*)p);
   }
   static void destruct_egammaMVATool(void *p) {
      typedef ::egammaMVATool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egammaMVATool

namespace ROOT {
   static TClass *vectorlEfloatmUgR_Dictionary();
   static void vectorlEfloatmUgR_TClassManip(TClass*);
   static void *new_vectorlEfloatmUgR(void *p = 0);
   static void *newArray_vectorlEfloatmUgR(Long_t size, void *p);
   static void delete_vectorlEfloatmUgR(void *p);
   static void deleteArray_vectorlEfloatmUgR(void *p);
   static void destruct_vectorlEfloatmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float*>*)
   {
      vector<float*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float*>", -2, "vector", 210,
                  typeid(vector<float*>), DefineBehavior(ptr, ptr),
                  &vectorlEfloatmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<float*>) );
      instance.SetNew(&new_vectorlEfloatmUgR);
      instance.SetNewArray(&newArray_vectorlEfloatmUgR);
      instance.SetDelete(&delete_vectorlEfloatmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatmUgR);
      instance.SetDestructor(&destruct_vectorlEfloatmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<float*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float*>*)0x0)->GetClass();
      vectorlEfloatmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatmUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float*> : new vector<float*>;
   }
   static void *newArray_vectorlEfloatmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float*>[nElements] : new vector<float*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatmUgR(void *p) {
      delete ((vector<float*>*)p);
   }
   static void deleteArray_vectorlEfloatmUgR(void *p) {
      delete [] ((vector<float*>*)p);
   }
   static void destruct_vectorlEfloatmUgR(void *p) {
      typedef vector<float*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float*>

namespace ROOT {
   static TClass *vectorlEegammaMVACalibNmspcLcLNodemUgR_Dictionary();
   static void vectorlEegammaMVACalibNmspcLcLNodemUgR_TClassManip(TClass*);
   static void *new_vectorlEegammaMVACalibNmspcLcLNodemUgR(void *p = 0);
   static void *newArray_vectorlEegammaMVACalibNmspcLcLNodemUgR(Long_t size, void *p);
   static void delete_vectorlEegammaMVACalibNmspcLcLNodemUgR(void *p);
   static void deleteArray_vectorlEegammaMVACalibNmspcLcLNodemUgR(void *p);
   static void destruct_vectorlEegammaMVACalibNmspcLcLNodemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<egammaMVACalibNmsp::Node*>*)
   {
      vector<egammaMVACalibNmsp::Node*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<egammaMVACalibNmsp::Node*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<egammaMVACalibNmsp::Node*>", -2, "vector", 210,
                  typeid(vector<egammaMVACalibNmsp::Node*>), DefineBehavior(ptr, ptr),
                  &vectorlEegammaMVACalibNmspcLcLNodemUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<egammaMVACalibNmsp::Node*>) );
      instance.SetNew(&new_vectorlEegammaMVACalibNmspcLcLNodemUgR);
      instance.SetNewArray(&newArray_vectorlEegammaMVACalibNmspcLcLNodemUgR);
      instance.SetDelete(&delete_vectorlEegammaMVACalibNmspcLcLNodemUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEegammaMVACalibNmspcLcLNodemUgR);
      instance.SetDestructor(&destruct_vectorlEegammaMVACalibNmspcLcLNodemUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<egammaMVACalibNmsp::Node*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<egammaMVACalibNmsp::Node*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEegammaMVACalibNmspcLcLNodemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<egammaMVACalibNmsp::Node*>*)0x0)->GetClass();
      vectorlEegammaMVACalibNmspcLcLNodemUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEegammaMVACalibNmspcLcLNodemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEegammaMVACalibNmspcLcLNodemUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<egammaMVACalibNmsp::Node*> : new vector<egammaMVACalibNmsp::Node*>;
   }
   static void *newArray_vectorlEegammaMVACalibNmspcLcLNodemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<egammaMVACalibNmsp::Node*>[nElements] : new vector<egammaMVACalibNmsp::Node*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEegammaMVACalibNmspcLcLNodemUgR(void *p) {
      delete ((vector<egammaMVACalibNmsp::Node*>*)p);
   }
   static void deleteArray_vectorlEegammaMVACalibNmspcLcLNodemUgR(void *p) {
      delete [] ((vector<egammaMVACalibNmsp::Node*>*)p);
   }
   static void destruct_vectorlEegammaMVACalibNmspcLcLNodemUgR(void *p) {
      typedef vector<egammaMVACalibNmsp::Node*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<egammaMVACalibNmsp::Node*>

namespace ROOT {
   static TClass *maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR_Dictionary();
   static void maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR_TClassManip(TClass*);
   static void *new_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(void *p = 0);
   static void *newArray_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(Long_t size, void *p);
   static void delete_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(void *p);
   static void deleteArray_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(void *p);
   static void destruct_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>*)
   {
      map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>", -2, "map", 96,
                  typeid(map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>), DefineBehavior(ptr, ptr),
                  &maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>) );
      instance.SetNew(&new_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR);
      instance.SetNewArray(&newArray_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR);
      instance.SetDelete(&delete_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR);
      instance.SetDeleteArray(&deleteArray_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR);
      instance.SetDestructor(&destruct_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>*)0x0)->GetClass();
      maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*> : new map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>;
   }
   static void *newArray_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>[nElements] : new map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(void *p) {
      delete ((map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>*)p);
   }
   static void deleteArray_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(void *p) {
      delete [] ((map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>*)p);
   }
   static void destruct_maplEpairlEegammaMVACalibcLcLReaderIDcOegammaMVACalibcLcLShiftTypegRcOTF1mUgR(void *p) {
      typedef map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<pair<egammaMVACalib::ReaderID,egammaMVACalib::ShiftType>,TF1*>

namespace ROOT {
   static TClass *maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR_Dictionary();
   static void maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR_TClassManip(TClass*);
   static void *new_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(void *p = 0);
   static void *newArray_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(Long_t size, void *p);
   static void delete_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(void *p);
   static void deleteArray_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(void *p);
   static void destruct_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<egammaMVACalib::ReaderID,map<TString,TString> >*)
   {
      map<egammaMVACalib::ReaderID,map<TString,TString> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<egammaMVACalib::ReaderID,map<TString,TString> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<egammaMVACalib::ReaderID,map<TString,TString> >", -2, "map", 96,
                  typeid(map<egammaMVACalib::ReaderID,map<TString,TString> >), DefineBehavior(ptr, ptr),
                  &maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<egammaMVACalib::ReaderID,map<TString,TString> >) );
      instance.SetNew(&new_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR);
      instance.SetNewArray(&newArray_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR);
      instance.SetDelete(&delete_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR);
      instance.SetDestructor(&destruct_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<egammaMVACalib::ReaderID,map<TString,TString> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<egammaMVACalib::ReaderID,map<TString,TString> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<egammaMVACalib::ReaderID,map<TString,TString> >*)0x0)->GetClass();
      maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<egammaMVACalib::ReaderID,map<TString,TString> > : new map<egammaMVACalib::ReaderID,map<TString,TString> >;
   }
   static void *newArray_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<egammaMVACalib::ReaderID,map<TString,TString> >[nElements] : new map<egammaMVACalib::ReaderID,map<TString,TString> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(void *p) {
      delete ((map<egammaMVACalib::ReaderID,map<TString,TString> >*)p);
   }
   static void deleteArray_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(void *p) {
      delete [] ((map<egammaMVACalib::ReaderID,map<TString,TString> >*)p);
   }
   static void destruct_maplEegammaMVACalibcLcLReaderIDcOmaplETStringcOTStringgRsPgR(void *p) {
      typedef map<egammaMVACalib::ReaderID,map<TString,TString> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<egammaMVACalib::ReaderID,map<TString,TString> >

namespace ROOT {
   static TClass *maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR_Dictionary();
   static void maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR_TClassManip(TClass*);
   static void *new_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(void *p = 0);
   static void *newArray_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(Long_t size, void *p);
   static void delete_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(void *p);
   static void deleteArray_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(void *p);
   static void destruct_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>*)
   {
      map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>", -2, "map", 96,
                  typeid(map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>), DefineBehavior(ptr, ptr),
                  &maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>) );
      instance.SetNew(&new_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR);
      instance.SetNewArray(&newArray_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR);
      instance.SetDelete(&delete_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR);
      instance.SetDeleteArray(&deleteArray_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR);
      instance.SetDestructor(&destruct_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>*)0x0)->GetClass();
      maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*> : new map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>;
   }
   static void *newArray_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>[nElements] : new map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(void *p) {
      delete ((map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>*)p);
   }
   static void deleteArray_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(void *p) {
      delete [] ((map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>*)p);
   }
   static void destruct_maplEegammaMVACalibcLcLReaderIDcOegammaMVACalibNmspcLcLBDTmUgR(void *p) {
      typedef map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<egammaMVACalib::ReaderID,egammaMVACalibNmsp::BDT*>

namespace ROOT {
   static TClass *maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR_Dictionary();
   static void maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR_TClassManip(TClass*);
   static void *new_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(void *p = 0);
   static void *newArray_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(Long_t size, void *p);
   static void delete_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(void *p);
   static void deleteArray_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(void *p);
   static void destruct_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<egammaMVACalib::ReaderID,TMVA::Reader*>*)
   {
      map<egammaMVACalib::ReaderID,TMVA::Reader*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<egammaMVACalib::ReaderID,TMVA::Reader*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<egammaMVACalib::ReaderID,TMVA::Reader*>", -2, "map", 96,
                  typeid(map<egammaMVACalib::ReaderID,TMVA::Reader*>), DefineBehavior(ptr, ptr),
                  &maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<egammaMVACalib::ReaderID,TMVA::Reader*>) );
      instance.SetNew(&new_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR);
      instance.SetNewArray(&newArray_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR);
      instance.SetDelete(&delete_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR);
      instance.SetDeleteArray(&deleteArray_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR);
      instance.SetDestructor(&destruct_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<egammaMVACalib::ReaderID,TMVA::Reader*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<egammaMVACalib::ReaderID,TMVA::Reader*>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<egammaMVACalib::ReaderID,TMVA::Reader*>*)0x0)->GetClass();
      maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<egammaMVACalib::ReaderID,TMVA::Reader*> : new map<egammaMVACalib::ReaderID,TMVA::Reader*>;
   }
   static void *newArray_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<egammaMVACalib::ReaderID,TMVA::Reader*>[nElements] : new map<egammaMVACalib::ReaderID,TMVA::Reader*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(void *p) {
      delete ((map<egammaMVACalib::ReaderID,TMVA::Reader*>*)p);
   }
   static void deleteArray_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(void *p) {
      delete [] ((map<egammaMVACalib::ReaderID,TMVA::Reader*>*)p);
   }
   static void destruct_maplEegammaMVACalibcLcLReaderIDcOTMVAcLcLReadermUgR(void *p) {
      typedef map<egammaMVACalib::ReaderID,TMVA::Reader*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<egammaMVACalib::ReaderID,TMVA::Reader*>

namespace ROOT {
   static TClass *maplETStringcOegammaMVACalibcLcLVarFormulagR_Dictionary();
   static void maplETStringcOegammaMVACalibcLcLVarFormulagR_TClassManip(TClass*);
   static void *new_maplETStringcOegammaMVACalibcLcLVarFormulagR(void *p = 0);
   static void *newArray_maplETStringcOegammaMVACalibcLcLVarFormulagR(Long_t size, void *p);
   static void delete_maplETStringcOegammaMVACalibcLcLVarFormulagR(void *p);
   static void deleteArray_maplETStringcOegammaMVACalibcLcLVarFormulagR(void *p);
   static void destruct_maplETStringcOegammaMVACalibcLcLVarFormulagR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,egammaMVACalib::VarFormula>*)
   {
      map<TString,egammaMVACalib::VarFormula> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,egammaMVACalib::VarFormula>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,egammaMVACalib::VarFormula>", -2, "map", 96,
                  typeid(map<TString,egammaMVACalib::VarFormula>), DefineBehavior(ptr, ptr),
                  &maplETStringcOegammaMVACalibcLcLVarFormulagR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,egammaMVACalib::VarFormula>) );
      instance.SetNew(&new_maplETStringcOegammaMVACalibcLcLVarFormulagR);
      instance.SetNewArray(&newArray_maplETStringcOegammaMVACalibcLcLVarFormulagR);
      instance.SetDelete(&delete_maplETStringcOegammaMVACalibcLcLVarFormulagR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOegammaMVACalibcLcLVarFormulagR);
      instance.SetDestructor(&destruct_maplETStringcOegammaMVACalibcLcLVarFormulagR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,egammaMVACalib::VarFormula> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<TString,egammaMVACalib::VarFormula>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOegammaMVACalibcLcLVarFormulagR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,egammaMVACalib::VarFormula>*)0x0)->GetClass();
      maplETStringcOegammaMVACalibcLcLVarFormulagR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOegammaMVACalibcLcLVarFormulagR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOegammaMVACalibcLcLVarFormulagR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<TString,egammaMVACalib::VarFormula> : new map<TString,egammaMVACalib::VarFormula>;
   }
   static void *newArray_maplETStringcOegammaMVACalibcLcLVarFormulagR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<TString,egammaMVACalib::VarFormula>[nElements] : new map<TString,egammaMVACalib::VarFormula>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOegammaMVACalibcLcLVarFormulagR(void *p) {
      delete ((map<TString,egammaMVACalib::VarFormula>*)p);
   }
   static void deleteArray_maplETStringcOegammaMVACalibcLcLVarFormulagR(void *p) {
      delete [] ((map<TString,egammaMVACalib::VarFormula>*)p);
   }
   static void destruct_maplETStringcOegammaMVACalibcLcLVarFormulagR(void *p) {
      typedef map<TString,egammaMVACalib::VarFormula> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,egammaMVACalib::VarFormula>

namespace ROOT {
   static TClass *maplETStringcOTStringgR_Dictionary();
   static void maplETStringcOTStringgR_TClassManip(TClass*);
   static void *new_maplETStringcOTStringgR(void *p = 0);
   static void *newArray_maplETStringcOTStringgR(Long_t size, void *p);
   static void delete_maplETStringcOTStringgR(void *p);
   static void deleteArray_maplETStringcOTStringgR(void *p);
   static void destruct_maplETStringcOTStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,TString>*)
   {
      map<TString,TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,TString>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,TString>", -2, "map", 96,
                  typeid(map<TString,TString>), DefineBehavior(ptr, ptr),
                  &maplETStringcOTStringgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,TString>) );
      instance.SetNew(&new_maplETStringcOTStringgR);
      instance.SetNewArray(&newArray_maplETStringcOTStringgR);
      instance.SetDelete(&delete_maplETStringcOTStringgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOTStringgR);
      instance.SetDestructor(&destruct_maplETStringcOTStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<TString,TString>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOTStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,TString>*)0x0)->GetClass();
      maplETStringcOTStringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOTStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOTStringgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<TString,TString> : new map<TString,TString>;
   }
   static void *newArray_maplETStringcOTStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<TString,TString>[nElements] : new map<TString,TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOTStringgR(void *p) {
      delete ((map<TString,TString>*)p);
   }
   static void deleteArray_maplETStringcOTStringgR(void *p) {
      delete [] ((map<TString,TString>*)p);
   }
   static void destruct_maplETStringcOTStringgR(void *p) {
      typedef map<TString,TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,TString>

namespace {
  void TriggerDictionaryInitialization_egammaMVACalibCINT_Impl() {
    static const char* headers[] = {
"egammaMVACalib/egammaMVACalib.h",
"egammaMVACalib/Node.h",
"egammaMVACalib/BDT.h",
"egammaMVACalib/egammaMVATool.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/egammaMVACalib/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/egammaMVACalib",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/egammaMVACalib/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$egammaMVACalib/egammaMVACalib.h")))  egammaMVACalib;
namespace egammaMVACalibNmsp{class __attribute__((annotate("$clingAutoload$egammaMVACalib/Node.h")))  Node;}
namespace egammaMVACalibNmsp{class __attribute__((annotate("$clingAutoload$egammaMVACalib/BDT.h")))  BDT;}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
class __attribute__((annotate("$clingAutoload$egammaMVACalib/egammaMVATool.h")))  egammaMVATool;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "egammaMVACalib"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "egammaMVACalib/egammaMVACalib.h"
#include "egammaMVACalib/Node.h"
#include "egammaMVACalib/BDT.h"
#include "egammaMVACalib/egammaMVATool.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"egammaMVACalib", payloadCode, "@",
"egammaMVACalib::ReaderID", payloadCode, "@",
"egammaMVACalibNmsp::BDT", payloadCode, "@",
"egammaMVACalibNmsp::Node", payloadCode, "@",
"egammaMVATool", payloadCode, "@",
"vector<egammaMVACalibNmsp::Node*>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("egammaMVACalibCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_egammaMVACalibCINT_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_egammaMVACalibCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_egammaMVACalibCINT() {
  TriggerDictionaryInitialization_egammaMVACalibCINT_Impl();
}

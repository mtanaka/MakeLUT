#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdITrigBunchCrossingTooldIobjdITrigBunchCrossingToolCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "TrigBunchCrossingTool/BunchCrossingToolBase.h"
#include "TrigBunchCrossingTool/D3PDBunchCrossingTool.h"
#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include "TrigBunchCrossingTool/BunchCrossingToolBase.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *TrigcLcLBunchCrossingToolBase_Dictionary();
   static void TrigcLcLBunchCrossingToolBase_TClassManip(TClass*);
   static void *new_TrigcLcLBunchCrossingToolBase(void *p = 0);
   static void *newArray_TrigcLcLBunchCrossingToolBase(Long_t size, void *p);
   static void delete_TrigcLcLBunchCrossingToolBase(void *p);
   static void deleteArray_TrigcLcLBunchCrossingToolBase(void *p);
   static void destruct_TrigcLcLBunchCrossingToolBase(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Trig::BunchCrossingToolBase*)
   {
      ::Trig::BunchCrossingToolBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Trig::BunchCrossingToolBase));
      static ::ROOT::TGenericClassInfo 
         instance("Trig::BunchCrossingToolBase", "TrigBunchCrossingTool/BunchCrossingToolBase.h", 36,
                  typeid(::Trig::BunchCrossingToolBase), DefineBehavior(ptr, ptr),
                  &TrigcLcLBunchCrossingToolBase_Dictionary, isa_proxy, 4,
                  sizeof(::Trig::BunchCrossingToolBase) );
      instance.SetNew(&new_TrigcLcLBunchCrossingToolBase);
      instance.SetNewArray(&newArray_TrigcLcLBunchCrossingToolBase);
      instance.SetDelete(&delete_TrigcLcLBunchCrossingToolBase);
      instance.SetDeleteArray(&deleteArray_TrigcLcLBunchCrossingToolBase);
      instance.SetDestructor(&destruct_TrigcLcLBunchCrossingToolBase);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Trig::BunchCrossingToolBase*)
   {
      return GenerateInitInstanceLocal((::Trig::BunchCrossingToolBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Trig::BunchCrossingToolBase*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigcLcLBunchCrossingToolBase_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Trig::BunchCrossingToolBase*)0x0)->GetClass();
      TrigcLcLBunchCrossingToolBase_TClassManip(theClass);
   return theClass;
   }

   static void TrigcLcLBunchCrossingToolBase_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrigcLcLD3PDBunchCrossingTool_Dictionary();
   static void TrigcLcLD3PDBunchCrossingTool_TClassManip(TClass*);
   static void *new_TrigcLcLD3PDBunchCrossingTool(void *p = 0);
   static void *newArray_TrigcLcLD3PDBunchCrossingTool(Long_t size, void *p);
   static void delete_TrigcLcLD3PDBunchCrossingTool(void *p);
   static void deleteArray_TrigcLcLD3PDBunchCrossingTool(void *p);
   static void destruct_TrigcLcLD3PDBunchCrossingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Trig::D3PDBunchCrossingTool*)
   {
      ::Trig::D3PDBunchCrossingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Trig::D3PDBunchCrossingTool));
      static ::ROOT::TGenericClassInfo 
         instance("Trig::D3PDBunchCrossingTool", "TrigBunchCrossingTool/D3PDBunchCrossingTool.h", 44,
                  typeid(::Trig::D3PDBunchCrossingTool), DefineBehavior(ptr, ptr),
                  &TrigcLcLD3PDBunchCrossingTool_Dictionary, isa_proxy, 4,
                  sizeof(::Trig::D3PDBunchCrossingTool) );
      instance.SetNew(&new_TrigcLcLD3PDBunchCrossingTool);
      instance.SetNewArray(&newArray_TrigcLcLD3PDBunchCrossingTool);
      instance.SetDelete(&delete_TrigcLcLD3PDBunchCrossingTool);
      instance.SetDeleteArray(&deleteArray_TrigcLcLD3PDBunchCrossingTool);
      instance.SetDestructor(&destruct_TrigcLcLD3PDBunchCrossingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Trig::D3PDBunchCrossingTool*)
   {
      return GenerateInitInstanceLocal((::Trig::D3PDBunchCrossingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Trig::D3PDBunchCrossingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigcLcLD3PDBunchCrossingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Trig::D3PDBunchCrossingTool*)0x0)->GetClass();
      TrigcLcLD3PDBunchCrossingTool_TClassManip(theClass);
   return theClass;
   }

   static void TrigcLcLD3PDBunchCrossingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrigcLcLStaticBunchCrossingTool_Dictionary();
   static void TrigcLcLStaticBunchCrossingTool_TClassManip(TClass*);
   static void *new_TrigcLcLStaticBunchCrossingTool(void *p = 0);
   static void *newArray_TrigcLcLStaticBunchCrossingTool(Long_t size, void *p);
   static void delete_TrigcLcLStaticBunchCrossingTool(void *p);
   static void deleteArray_TrigcLcLStaticBunchCrossingTool(void *p);
   static void destruct_TrigcLcLStaticBunchCrossingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Trig::StaticBunchCrossingTool*)
   {
      ::Trig::StaticBunchCrossingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Trig::StaticBunchCrossingTool));
      static ::ROOT::TGenericClassInfo 
         instance("Trig::StaticBunchCrossingTool", "TrigBunchCrossingTool/StaticBunchCrossingTool.h", 30,
                  typeid(::Trig::StaticBunchCrossingTool), DefineBehavior(ptr, ptr),
                  &TrigcLcLStaticBunchCrossingTool_Dictionary, isa_proxy, 4,
                  sizeof(::Trig::StaticBunchCrossingTool) );
      instance.SetNew(&new_TrigcLcLStaticBunchCrossingTool);
      instance.SetNewArray(&newArray_TrigcLcLStaticBunchCrossingTool);
      instance.SetDelete(&delete_TrigcLcLStaticBunchCrossingTool);
      instance.SetDeleteArray(&deleteArray_TrigcLcLStaticBunchCrossingTool);
      instance.SetDestructor(&destruct_TrigcLcLStaticBunchCrossingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Trig::StaticBunchCrossingTool*)
   {
      return GenerateInitInstanceLocal((::Trig::StaticBunchCrossingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Trig::StaticBunchCrossingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigcLcLStaticBunchCrossingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Trig::StaticBunchCrossingTool*)0x0)->GetClass();
      TrigcLcLStaticBunchCrossingTool_TClassManip(theClass);
   return theClass;
   }

   static void TrigcLcLStaticBunchCrossingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrigcLcLWebBunchCrossingTool_Dictionary();
   static void TrigcLcLWebBunchCrossingTool_TClassManip(TClass*);
   static void *new_TrigcLcLWebBunchCrossingTool(void *p = 0);
   static void *newArray_TrigcLcLWebBunchCrossingTool(Long_t size, void *p);
   static void delete_TrigcLcLWebBunchCrossingTool(void *p);
   static void deleteArray_TrigcLcLWebBunchCrossingTool(void *p);
   static void destruct_TrigcLcLWebBunchCrossingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Trig::WebBunchCrossingTool*)
   {
      ::Trig::WebBunchCrossingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Trig::WebBunchCrossingTool));
      static ::ROOT::TGenericClassInfo 
         instance("Trig::WebBunchCrossingTool", "TrigBunchCrossingTool/WebBunchCrossingTool.h", 34,
                  typeid(::Trig::WebBunchCrossingTool), DefineBehavior(ptr, ptr),
                  &TrigcLcLWebBunchCrossingTool_Dictionary, isa_proxy, 4,
                  sizeof(::Trig::WebBunchCrossingTool) );
      instance.SetNew(&new_TrigcLcLWebBunchCrossingTool);
      instance.SetNewArray(&newArray_TrigcLcLWebBunchCrossingTool);
      instance.SetDelete(&delete_TrigcLcLWebBunchCrossingTool);
      instance.SetDeleteArray(&deleteArray_TrigcLcLWebBunchCrossingTool);
      instance.SetDestructor(&destruct_TrigcLcLWebBunchCrossingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Trig::WebBunchCrossingTool*)
   {
      return GenerateInitInstanceLocal((::Trig::WebBunchCrossingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Trig::WebBunchCrossingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigcLcLWebBunchCrossingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Trig::WebBunchCrossingTool*)0x0)->GetClass();
      TrigcLcLWebBunchCrossingTool_TClassManip(theClass);
   return theClass;
   }

   static void TrigcLcLWebBunchCrossingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrigcLcLBunchCrossingToolBase(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::Trig::BunchCrossingToolBase : new ::Trig::BunchCrossingToolBase;
   }
   static void *newArray_TrigcLcLBunchCrossingToolBase(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::Trig::BunchCrossingToolBase[nElements] : new ::Trig::BunchCrossingToolBase[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrigcLcLBunchCrossingToolBase(void *p) {
      delete ((::Trig::BunchCrossingToolBase*)p);
   }
   static void deleteArray_TrigcLcLBunchCrossingToolBase(void *p) {
      delete [] ((::Trig::BunchCrossingToolBase*)p);
   }
   static void destruct_TrigcLcLBunchCrossingToolBase(void *p) {
      typedef ::Trig::BunchCrossingToolBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Trig::BunchCrossingToolBase

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrigcLcLD3PDBunchCrossingTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::Trig::D3PDBunchCrossingTool : new ::Trig::D3PDBunchCrossingTool;
   }
   static void *newArray_TrigcLcLD3PDBunchCrossingTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::Trig::D3PDBunchCrossingTool[nElements] : new ::Trig::D3PDBunchCrossingTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrigcLcLD3PDBunchCrossingTool(void *p) {
      delete ((::Trig::D3PDBunchCrossingTool*)p);
   }
   static void deleteArray_TrigcLcLD3PDBunchCrossingTool(void *p) {
      delete [] ((::Trig::D3PDBunchCrossingTool*)p);
   }
   static void destruct_TrigcLcLD3PDBunchCrossingTool(void *p) {
      typedef ::Trig::D3PDBunchCrossingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Trig::D3PDBunchCrossingTool

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrigcLcLStaticBunchCrossingTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::Trig::StaticBunchCrossingTool : new ::Trig::StaticBunchCrossingTool;
   }
   static void *newArray_TrigcLcLStaticBunchCrossingTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::Trig::StaticBunchCrossingTool[nElements] : new ::Trig::StaticBunchCrossingTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrigcLcLStaticBunchCrossingTool(void *p) {
      delete ((::Trig::StaticBunchCrossingTool*)p);
   }
   static void deleteArray_TrigcLcLStaticBunchCrossingTool(void *p) {
      delete [] ((::Trig::StaticBunchCrossingTool*)p);
   }
   static void destruct_TrigcLcLStaticBunchCrossingTool(void *p) {
      typedef ::Trig::StaticBunchCrossingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Trig::StaticBunchCrossingTool

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrigcLcLWebBunchCrossingTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::Trig::WebBunchCrossingTool : new ::Trig::WebBunchCrossingTool;
   }
   static void *newArray_TrigcLcLWebBunchCrossingTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::Trig::WebBunchCrossingTool[nElements] : new ::Trig::WebBunchCrossingTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrigcLcLWebBunchCrossingTool(void *p) {
      delete ((::Trig::WebBunchCrossingTool*)p);
   }
   static void deleteArray_TrigcLcLWebBunchCrossingTool(void *p) {
      delete [] ((::Trig::WebBunchCrossingTool*)p);
   }
   static void destruct_TrigcLcLWebBunchCrossingTool(void *p) {
      typedef ::Trig::WebBunchCrossingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Trig::WebBunchCrossingTool

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 210,
                  typeid(vector<int>), DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 210,
                  typeid(vector<float>), DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *setlETrigcLcLBunchTraingR_Dictionary();
   static void setlETrigcLcLBunchTraingR_TClassManip(TClass*);
   static void *new_setlETrigcLcLBunchTraingR(void *p = 0);
   static void *newArray_setlETrigcLcLBunchTraingR(Long_t size, void *p);
   static void delete_setlETrigcLcLBunchTraingR(void *p);
   static void deleteArray_setlETrigcLcLBunchTraingR(void *p);
   static void destruct_setlETrigcLcLBunchTraingR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const set<Trig::BunchTrain>*)
   {
      set<Trig::BunchTrain> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(set<Trig::BunchTrain>));
      static ::ROOT::TGenericClassInfo 
         instance("set<Trig::BunchTrain>", -2, "set", 90,
                  typeid(set<Trig::BunchTrain>), DefineBehavior(ptr, ptr),
                  &setlETrigcLcLBunchTraingR_Dictionary, isa_proxy, 0,
                  sizeof(set<Trig::BunchTrain>) );
      instance.SetNew(&new_setlETrigcLcLBunchTraingR);
      instance.SetNewArray(&newArray_setlETrigcLcLBunchTraingR);
      instance.SetDelete(&delete_setlETrigcLcLBunchTraingR);
      instance.SetDeleteArray(&deleteArray_setlETrigcLcLBunchTraingR);
      instance.SetDestructor(&destruct_setlETrigcLcLBunchTraingR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Insert< set<Trig::BunchTrain> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const set<Trig::BunchTrain>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *setlETrigcLcLBunchTraingR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const set<Trig::BunchTrain>*)0x0)->GetClass();
      setlETrigcLcLBunchTraingR_TClassManip(theClass);
   return theClass;
   }

   static void setlETrigcLcLBunchTraingR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_setlETrigcLcLBunchTraingR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) set<Trig::BunchTrain> : new set<Trig::BunchTrain>;
   }
   static void *newArray_setlETrigcLcLBunchTraingR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) set<Trig::BunchTrain>[nElements] : new set<Trig::BunchTrain>[nElements];
   }
   // Wrapper around operator delete
   static void delete_setlETrigcLcLBunchTraingR(void *p) {
      delete ((set<Trig::BunchTrain>*)p);
   }
   static void deleteArray_setlETrigcLcLBunchTraingR(void *p) {
      delete [] ((set<Trig::BunchTrain>*)p);
   }
   static void destruct_setlETrigcLcLBunchTraingR(void *p) {
      typedef set<Trig::BunchTrain> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class set<Trig::BunchTrain>

namespace ROOT {
   static TClass *setlETrigcLcLBunchCrossinggR_Dictionary();
   static void setlETrigcLcLBunchCrossinggR_TClassManip(TClass*);
   static void *new_setlETrigcLcLBunchCrossinggR(void *p = 0);
   static void *newArray_setlETrigcLcLBunchCrossinggR(Long_t size, void *p);
   static void delete_setlETrigcLcLBunchCrossinggR(void *p);
   static void deleteArray_setlETrigcLcLBunchCrossinggR(void *p);
   static void destruct_setlETrigcLcLBunchCrossinggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const set<Trig::BunchCrossing>*)
   {
      set<Trig::BunchCrossing> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(set<Trig::BunchCrossing>));
      static ::ROOT::TGenericClassInfo 
         instance("set<Trig::BunchCrossing>", -2, "set", 90,
                  typeid(set<Trig::BunchCrossing>), DefineBehavior(ptr, ptr),
                  &setlETrigcLcLBunchCrossinggR_Dictionary, isa_proxy, 0,
                  sizeof(set<Trig::BunchCrossing>) );
      instance.SetNew(&new_setlETrigcLcLBunchCrossinggR);
      instance.SetNewArray(&newArray_setlETrigcLcLBunchCrossinggR);
      instance.SetDelete(&delete_setlETrigcLcLBunchCrossinggR);
      instance.SetDeleteArray(&deleteArray_setlETrigcLcLBunchCrossinggR);
      instance.SetDestructor(&destruct_setlETrigcLcLBunchCrossinggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Insert< set<Trig::BunchCrossing> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const set<Trig::BunchCrossing>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *setlETrigcLcLBunchCrossinggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const set<Trig::BunchCrossing>*)0x0)->GetClass();
      setlETrigcLcLBunchCrossinggR_TClassManip(theClass);
   return theClass;
   }

   static void setlETrigcLcLBunchCrossinggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_setlETrigcLcLBunchCrossinggR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) set<Trig::BunchCrossing> : new set<Trig::BunchCrossing>;
   }
   static void *newArray_setlETrigcLcLBunchCrossinggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) set<Trig::BunchCrossing>[nElements] : new set<Trig::BunchCrossing>[nElements];
   }
   // Wrapper around operator delete
   static void delete_setlETrigcLcLBunchCrossinggR(void *p) {
      delete ((set<Trig::BunchCrossing>*)p);
   }
   static void deleteArray_setlETrigcLcLBunchCrossinggR(void *p) {
      delete [] ((set<Trig::BunchCrossing>*)p);
   }
   static void destruct_setlETrigcLcLBunchCrossinggR(void *p) {
      typedef set<Trig::BunchCrossing> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class set<Trig::BunchCrossing>

namespace ROOT {
   static TClass *maplEunsignedsPintcOTrigcLcLBunchConfiggR_Dictionary();
   static void maplEunsignedsPintcOTrigcLcLBunchConfiggR_TClassManip(TClass*);
   static void *new_maplEunsignedsPintcOTrigcLcLBunchConfiggR(void *p = 0);
   static void *newArray_maplEunsignedsPintcOTrigcLcLBunchConfiggR(Long_t size, void *p);
   static void delete_maplEunsignedsPintcOTrigcLcLBunchConfiggR(void *p);
   static void deleteArray_maplEunsignedsPintcOTrigcLcLBunchConfiggR(void *p);
   static void destruct_maplEunsignedsPintcOTrigcLcLBunchConfiggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<unsigned int,Trig::BunchConfig>*)
   {
      map<unsigned int,Trig::BunchConfig> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<unsigned int,Trig::BunchConfig>));
      static ::ROOT::TGenericClassInfo 
         instance("map<unsigned int,Trig::BunchConfig>", -2, "map", 96,
                  typeid(map<unsigned int,Trig::BunchConfig>), DefineBehavior(ptr, ptr),
                  &maplEunsignedsPintcOTrigcLcLBunchConfiggR_Dictionary, isa_proxy, 0,
                  sizeof(map<unsigned int,Trig::BunchConfig>) );
      instance.SetNew(&new_maplEunsignedsPintcOTrigcLcLBunchConfiggR);
      instance.SetNewArray(&newArray_maplEunsignedsPintcOTrigcLcLBunchConfiggR);
      instance.SetDelete(&delete_maplEunsignedsPintcOTrigcLcLBunchConfiggR);
      instance.SetDeleteArray(&deleteArray_maplEunsignedsPintcOTrigcLcLBunchConfiggR);
      instance.SetDestructor(&destruct_maplEunsignedsPintcOTrigcLcLBunchConfiggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<unsigned int,Trig::BunchConfig> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<unsigned int,Trig::BunchConfig>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEunsignedsPintcOTrigcLcLBunchConfiggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<unsigned int,Trig::BunchConfig>*)0x0)->GetClass();
      maplEunsignedsPintcOTrigcLcLBunchConfiggR_TClassManip(theClass);
   return theClass;
   }

   static void maplEunsignedsPintcOTrigcLcLBunchConfiggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEunsignedsPintcOTrigcLcLBunchConfiggR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<unsigned int,Trig::BunchConfig> : new map<unsigned int,Trig::BunchConfig>;
   }
   static void *newArray_maplEunsignedsPintcOTrigcLcLBunchConfiggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<unsigned int,Trig::BunchConfig>[nElements] : new map<unsigned int,Trig::BunchConfig>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEunsignedsPintcOTrigcLcLBunchConfiggR(void *p) {
      delete ((map<unsigned int,Trig::BunchConfig>*)p);
   }
   static void deleteArray_maplEunsignedsPintcOTrigcLcLBunchConfiggR(void *p) {
      delete [] ((map<unsigned int,Trig::BunchConfig>*)p);
   }
   static void destruct_maplEunsignedsPintcOTrigcLcLBunchConfiggR(void *p) {
      typedef map<unsigned int,Trig::BunchConfig> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<unsigned int,Trig::BunchConfig>

namespace ROOT {
   static TClass *maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR_Dictionary();
   static void maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR_TClassManip(TClass*);
   static void *new_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(void *p = 0);
   static void *newArray_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(Long_t size, void *p);
   static void delete_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(void *p);
   static void deleteArray_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(void *p);
   static void destruct_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>*)
   {
      map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>", -2, "map", 96,
                  typeid(map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>), DefineBehavior(ptr, ptr),
                  &maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>) );
      instance.SetNew(&new_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR);
      instance.SetNewArray(&newArray_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR);
      instance.SetDelete(&delete_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR);
      instance.SetDestructor(&destruct_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>*)0x0)->GetClass();
      maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int> : new map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>;
   }
   static void *newArray_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>[nElements] : new map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(void *p) {
      delete ((map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>*)p);
   }
   static void deleteArray_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(void *p) {
      delete [] ((map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>*)p);
   }
   static void destruct_maplEpairlETrigcLcLWebBunchCrossingToolcLcLIOVcOTrigcLcLWebBunchCrossingToolcLcLIOVgRcOintgR(void *p) {
      typedef map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<pair<Trig::WebBunchCrossingTool::IOV,Trig::WebBunchCrossingTool::IOV>,int>

namespace ROOT {
   static TClass *maplEintcOvectorlEintgRsPgR_Dictionary();
   static void maplEintcOvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_maplEintcOvectorlEintgRsPgR(void *p = 0);
   static void *newArray_maplEintcOvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_maplEintcOvectorlEintgRsPgR(void *p);
   static void deleteArray_maplEintcOvectorlEintgRsPgR(void *p);
   static void destruct_maplEintcOvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,vector<int> >*)
   {
      map<int,vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,vector<int> >", -2, "map", 96,
                  typeid(map<int,vector<int> >), DefineBehavior(ptr, ptr),
                  &maplEintcOvectorlEintgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,vector<int> >) );
      instance.SetNew(&new_maplEintcOvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_maplEintcOvectorlEintgRsPgR);
      instance.SetDelete(&delete_maplEintcOvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_maplEintcOvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,vector<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<int,vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,vector<int> >*)0x0)->GetClass();
      maplEintcOvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,vector<int> > : new map<int,vector<int> >;
   }
   static void *newArray_maplEintcOvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,vector<int> >[nElements] : new map<int,vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOvectorlEintgRsPgR(void *p) {
      delete ((map<int,vector<int> >*)p);
   }
   static void deleteArray_maplEintcOvectorlEintgRsPgR(void *p) {
      delete [] ((map<int,vector<int> >*)p);
   }
   static void destruct_maplEintcOvectorlEintgRsPgR(void *p) {
      typedef map<int,vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,vector<int> >

namespace ROOT {
   static TClass *maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR_Dictionary();
   static void maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(void *p = 0);
   static void *newArray_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(void *p);
   static void deleteArray_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(void *p);
   static void destruct_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,pair<vector<int>,vector<int> > >*)
   {
      map<int,pair<vector<int>,vector<int> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,pair<vector<int>,vector<int> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,pair<vector<int>,vector<int> > >", -2, "map", 96,
                  typeid(map<int,pair<vector<int>,vector<int> > >), DefineBehavior(ptr, ptr),
                  &maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,pair<vector<int>,vector<int> > >) );
      instance.SetNew(&new_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR);
      instance.SetDelete(&delete_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,pair<vector<int>,vector<int> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<int,pair<vector<int>,vector<int> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,pair<vector<int>,vector<int> > >*)0x0)->GetClass();
      maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,pair<vector<int>,vector<int> > > : new map<int,pair<vector<int>,vector<int> > >;
   }
   static void *newArray_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,pair<vector<int>,vector<int> > >[nElements] : new map<int,pair<vector<int>,vector<int> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(void *p) {
      delete ((map<int,pair<vector<int>,vector<int> > >*)p);
   }
   static void deleteArray_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(void *p) {
      delete [] ((map<int,pair<vector<int>,vector<int> > >*)p);
   }
   static void destruct_maplEintcOpairlEvectorlEintgRcOvectorlEintgRsPgRsPgR(void *p) {
      typedef map<int,pair<vector<int>,vector<int> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,pair<vector<int>,vector<int> > >

namespace {
  void TriggerDictionaryInitialization_TrigBunchCrossingToolCINT_Impl() {
    static const char* headers[] = {
"TrigBunchCrossingTool/BunchCrossingToolBase.h",
"TrigBunchCrossingTool/D3PDBunchCrossingTool.h",
"TrigBunchCrossingTool/StaticBunchCrossingTool.h",
"TrigBunchCrossingTool/WebBunchCrossingTool.h",
"TrigBunchCrossingTool/BunchCrossingToolBase.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigBunchCrossingTool/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigBunchCrossingTool",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigBunchCrossingTool/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace Trig{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigBunchCrossingTool/Root/LinkDef.h")))  BunchCrossingToolBase;}
namespace Trig{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigBunchCrossingTool/Root/LinkDef.h")))  D3PDBunchCrossingTool;}
namespace Trig{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigBunchCrossingTool/Root/LinkDef.h")))  StaticBunchCrossingTool;}
namespace Trig{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigBunchCrossingTool/Root/LinkDef.h")))  WebBunchCrossingTool;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "TrigBunchCrossingTool"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "TrigBunchCrossingTool/BunchCrossingToolBase.h"
#include "TrigBunchCrossingTool/D3PDBunchCrossingTool.h"
#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include "TrigBunchCrossingTool/BunchCrossingToolBase.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Trig::BunchCrossingToolBase", payloadCode, "@",
"Trig::D3PDBunchCrossingTool", payloadCode, "@",
"Trig::StaticBunchCrossingTool", payloadCode, "@",
"Trig::WebBunchCrossingTool", payloadCode, "@",
"vector<float>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TrigBunchCrossingToolCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TrigBunchCrossingToolCINT_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TrigBunchCrossingToolCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TrigBunchCrossingToolCINT() {
  TriggerDictionaryInitialization_TrigBunchCrossingToolCINT_Impl();
}

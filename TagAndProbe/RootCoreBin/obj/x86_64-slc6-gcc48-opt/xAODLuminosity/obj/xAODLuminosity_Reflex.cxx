// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODLuminositydIobjdIxAODLuminosity_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODLuminosity/xAODLuminosity/xAODLuminosityDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLLumiBlockRange_v1_Dictionary();
   static void xAODcLcLLumiBlockRange_v1_TClassManip(TClass*);
   static void *new_xAODcLcLLumiBlockRange_v1(void *p = 0);
   static void *newArray_xAODcLcLLumiBlockRange_v1(Long_t size, void *p);
   static void delete_xAODcLcLLumiBlockRange_v1(void *p);
   static void deleteArray_xAODcLcLLumiBlockRange_v1(void *p);
   static void destruct_xAODcLcLLumiBlockRange_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::LumiBlockRange_v1*)
   {
      ::xAOD::LumiBlockRange_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::LumiBlockRange_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::LumiBlockRange_v1", "xAODLuminosity/versions/LumiBlockRange_v1.h", 28,
                  typeid(::xAOD::LumiBlockRange_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLLumiBlockRange_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::LumiBlockRange_v1) );
      instance.SetNew(&new_xAODcLcLLumiBlockRange_v1);
      instance.SetNewArray(&newArray_xAODcLcLLumiBlockRange_v1);
      instance.SetDelete(&delete_xAODcLcLLumiBlockRange_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLLumiBlockRange_v1);
      instance.SetDestructor(&destruct_xAODcLcLLumiBlockRange_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::LumiBlockRange_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::LumiBlockRange_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::LumiBlockRange_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLLumiBlockRange_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::LumiBlockRange_v1*)0x0)->GetClass();
      xAODcLcLLumiBlockRange_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLLumiBlockRange_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLLumiBlockRange_v1gR_Dictionary();
   static void DataVectorlExAODcLcLLumiBlockRange_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLLumiBlockRange_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLLumiBlockRange_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLLumiBlockRange_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLLumiBlockRange_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLLumiBlockRange_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::LumiBlockRange_v1>*)
   {
      ::DataVector<xAOD::LumiBlockRange_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::LumiBlockRange_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::LumiBlockRange_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::LumiBlockRange_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLLumiBlockRange_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::LumiBlockRange_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLLumiBlockRange_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLLumiBlockRange_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLLumiBlockRange_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLLumiBlockRange_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLLumiBlockRange_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::LumiBlockRange_v1>","xAOD::LumiBlockRangeContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::LumiBlockRange_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::LumiBlockRange_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::LumiBlockRange_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLLumiBlockRange_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::LumiBlockRange_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLLumiBlockRange_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLLumiBlockRange_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","9BAB09FC-3F1C-49F5-90BE-8FDB804AC8B0");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLLumiBlockRangeAuxContainer_v1_Dictionary();
   static void xAODcLcLLumiBlockRangeAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLLumiBlockRangeAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLLumiBlockRangeAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLLumiBlockRangeAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLLumiBlockRangeAuxContainer_v1(void *p);
   static void destruct_xAODcLcLLumiBlockRangeAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::LumiBlockRangeAuxContainer_v1*)
   {
      ::xAOD::LumiBlockRangeAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::LumiBlockRangeAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::LumiBlockRangeAuxContainer_v1", "xAODLuminosity/versions/LumiBlockRangeAuxContainer_v1.h", 29,
                  typeid(::xAOD::LumiBlockRangeAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLLumiBlockRangeAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::LumiBlockRangeAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLLumiBlockRangeAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLLumiBlockRangeAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLLumiBlockRangeAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLLumiBlockRangeAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLLumiBlockRangeAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::LumiBlockRangeAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::LumiBlockRangeAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::LumiBlockRangeAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLLumiBlockRangeAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::LumiBlockRangeAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLLumiBlockRangeAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLLumiBlockRangeAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","514696E1-A262-470A-BEB1-FF980CD8827A");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLBCMRawData_v1_Dictionary();
   static void xAODcLcLBCMRawData_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBCMRawData_v1(void *p = 0);
   static void *newArray_xAODcLcLBCMRawData_v1(Long_t size, void *p);
   static void delete_xAODcLcLBCMRawData_v1(void *p);
   static void deleteArray_xAODcLcLBCMRawData_v1(void *p);
   static void destruct_xAODcLcLBCMRawData_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BCMRawData_v1*)
   {
      ::xAOD::BCMRawData_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BCMRawData_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BCMRawData_v1", "xAODLuminosity/versions/BCMRawData_v1.h", 26,
                  typeid(::xAOD::BCMRawData_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBCMRawData_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BCMRawData_v1) );
      instance.SetNew(&new_xAODcLcLBCMRawData_v1);
      instance.SetNewArray(&newArray_xAODcLcLBCMRawData_v1);
      instance.SetDelete(&delete_xAODcLcLBCMRawData_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBCMRawData_v1);
      instance.SetDestructor(&destruct_xAODcLcLBCMRawData_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BCMRawData_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BCMRawData_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BCMRawData_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBCMRawData_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BCMRawData_v1*)0x0)->GetClass();
      xAODcLcLBCMRawData_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBCMRawData_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLBCMRawData_v1gR_Dictionary();
   static void DataVectorlExAODcLcLBCMRawData_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLBCMRawData_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLBCMRawData_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLBCMRawData_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLBCMRawData_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLBCMRawData_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::BCMRawData_v1>*)
   {
      ::DataVector<xAOD::BCMRawData_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::BCMRawData_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::BCMRawData_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::BCMRawData_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLBCMRawData_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::BCMRawData_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLBCMRawData_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLBCMRawData_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLBCMRawData_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLBCMRawData_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLBCMRawData_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::BCMRawData_v1>","xAOD::BCMRawDataContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::BCMRawData_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::BCMRawData_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::BCMRawData_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLBCMRawData_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::BCMRawData_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLBCMRawData_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLBCMRawData_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F0D41C60-4A5F-4678-A468-28E45DB9E408");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLBCMRawDataAuxContainer_v1_Dictionary();
   static void xAODcLcLBCMRawDataAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLBCMRawDataAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLBCMRawDataAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLBCMRawDataAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLBCMRawDataAuxContainer_v1(void *p);
   static void destruct_xAODcLcLBCMRawDataAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::BCMRawDataAuxContainer_v1*)
   {
      ::xAOD::BCMRawDataAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::BCMRawDataAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::BCMRawDataAuxContainer_v1", "xAODLuminosity/versions/BCMRawDataAuxContainer_v1.h", 27,
                  typeid(::xAOD::BCMRawDataAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLBCMRawDataAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::BCMRawDataAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLBCMRawDataAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLBCMRawDataAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLBCMRawDataAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLBCMRawDataAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLBCMRawDataAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::BCMRawDataAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::BCMRawDataAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::BCMRawDataAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLBCMRawDataAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::BCMRawDataAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLBCMRawDataAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLBCMRawDataAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E13E64B0-E832-4FA5-8D23-97DD36BFEE4B");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::BCMRawData_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::BCMRawData_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::BCMRawData_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::BCMRawData_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::BCMRawData_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::BCMRawData_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::BCMRawData_v1> >","ElementLink<xAOD::BCMRawDataContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::BCMRawData_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::BCMRawData_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::BCMRawData_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::BCMRawData_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::BCMRawData_v1> >*)
   {
      ::DataLink<DataVector<xAOD::BCMRawData_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::BCMRawData_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::BCMRawData_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::BCMRawData_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::BCMRawData_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::BCMRawData_v1> >","DataLink<xAOD::BCMRawDataContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::BCMRawData_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::BCMRawData_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::BCMRawData_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::BCMRawData_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLLumiBlockRange_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::LumiBlockRange_v1 : new ::xAOD::LumiBlockRange_v1;
   }
   static void *newArray_xAODcLcLLumiBlockRange_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::LumiBlockRange_v1[nElements] : new ::xAOD::LumiBlockRange_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLLumiBlockRange_v1(void *p) {
      delete ((::xAOD::LumiBlockRange_v1*)p);
   }
   static void deleteArray_xAODcLcLLumiBlockRange_v1(void *p) {
      delete [] ((::xAOD::LumiBlockRange_v1*)p);
   }
   static void destruct_xAODcLcLLumiBlockRange_v1(void *p) {
      typedef ::xAOD::LumiBlockRange_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::LumiBlockRange_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLLumiBlockRange_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::LumiBlockRange_v1> : new ::DataVector<xAOD::LumiBlockRange_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLLumiBlockRange_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::LumiBlockRange_v1>[nElements] : new ::DataVector<xAOD::LumiBlockRange_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLLumiBlockRange_v1gR(void *p) {
      delete ((::DataVector<xAOD::LumiBlockRange_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLLumiBlockRange_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::LumiBlockRange_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLLumiBlockRange_v1gR(void *p) {
      typedef ::DataVector<xAOD::LumiBlockRange_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::LumiBlockRange_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLLumiBlockRangeAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::LumiBlockRangeAuxContainer_v1 : new ::xAOD::LumiBlockRangeAuxContainer_v1;
   }
   static void *newArray_xAODcLcLLumiBlockRangeAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::LumiBlockRangeAuxContainer_v1[nElements] : new ::xAOD::LumiBlockRangeAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLLumiBlockRangeAuxContainer_v1(void *p) {
      delete ((::xAOD::LumiBlockRangeAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLLumiBlockRangeAuxContainer_v1(void *p) {
      delete [] ((::xAOD::LumiBlockRangeAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLLumiBlockRangeAuxContainer_v1(void *p) {
      typedef ::xAOD::LumiBlockRangeAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::LumiBlockRangeAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBCMRawData_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BCMRawData_v1 : new ::xAOD::BCMRawData_v1;
   }
   static void *newArray_xAODcLcLBCMRawData_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BCMRawData_v1[nElements] : new ::xAOD::BCMRawData_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBCMRawData_v1(void *p) {
      delete ((::xAOD::BCMRawData_v1*)p);
   }
   static void deleteArray_xAODcLcLBCMRawData_v1(void *p) {
      delete [] ((::xAOD::BCMRawData_v1*)p);
   }
   static void destruct_xAODcLcLBCMRawData_v1(void *p) {
      typedef ::xAOD::BCMRawData_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BCMRawData_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLBCMRawData_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::BCMRawData_v1> : new ::DataVector<xAOD::BCMRawData_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLBCMRawData_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::BCMRawData_v1>[nElements] : new ::DataVector<xAOD::BCMRawData_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLBCMRawData_v1gR(void *p) {
      delete ((::DataVector<xAOD::BCMRawData_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLBCMRawData_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::BCMRawData_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLBCMRawData_v1gR(void *p) {
      typedef ::DataVector<xAOD::BCMRawData_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::BCMRawData_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLBCMRawDataAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BCMRawDataAuxContainer_v1 : new ::xAOD::BCMRawDataAuxContainer_v1;
   }
   static void *newArray_xAODcLcLBCMRawDataAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::BCMRawDataAuxContainer_v1[nElements] : new ::xAOD::BCMRawDataAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLBCMRawDataAuxContainer_v1(void *p) {
      delete ((::xAOD::BCMRawDataAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLBCMRawDataAuxContainer_v1(void *p) {
      delete [] ((::xAOD::BCMRawDataAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLBCMRawDataAuxContainer_v1(void *p) {
      typedef ::xAOD::BCMRawDataAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::BCMRawDataAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::BCMRawData_v1> > : new ::ElementLink<DataVector<xAOD::BCMRawData_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::BCMRawData_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::BCMRawData_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::BCMRawData_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::BCMRawData_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::BCMRawData_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::BCMRawData_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::BCMRawData_v1> > : new ::DataLink<DataVector<xAOD::BCMRawData_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::BCMRawData_v1> >[nElements] : new ::DataLink<DataVector<xAOD::BCMRawData_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::BCMRawData_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::BCMRawData_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::BCMRawData_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::BCMRawData_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > : new vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::BCMRawData_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::BCMRawData_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::BCMRawData_v1> > > : new vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLBCMRawData_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::BCMRawData_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODLuminosity_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODLuminosity/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODLuminosity",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODLuminosity/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODLuminosity/versions/LumiBlockRange_v1.h")))  LumiBlockRange_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@514696E1-A262-470A-BEB1-FF980CD8827A)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODLuminosity/versions/LumiBlockRangeAuxContainer_v1.h")))  LumiBlockRangeAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODLuminosity/versions/BCMRawData_v1.h")))  BCMRawData_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@E13E64B0-E832-4FA5-8D23-97DD36BFEE4B)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODLuminosity/versions/BCMRawDataAuxContainer_v1.h")))  BCMRawDataAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODLuminosity"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODLuminosityDict.h 652111 2015-03-06 10:13:22Z krasznaa $
#ifndef XAODLUMINOSITY_XAODLUMINOSITYDICT_H
#define XAODLUMINOSITY_XAODLUMINOSITYDICT_H

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/ElementLink.h"
#include "AthLinks/DataLink.h"

// Local include(s):
#include "xAODLuminosity/versions/LumiBlockRange_v1.h"
#include "xAODLuminosity/versions/LumiBlockRangeContainer_v1.h"
#include "xAODLuminosity/versions/LumiBlockRangeAuxContainer_v1.h"
#include "xAODLuminosity/versions/BCMRawData_v1.h"
#include "xAODLuminosity/versions/BCMRawDataContainer_v1.h"
#include "xAODLuminosity/versions/BCMRawDataAuxContainer_v1.h"
#include "xAODLuminosity/BCMRawDataContainer.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODLUMINOSITY {
      /// Container(s):
      xAOD::LumiBlockRangeContainer_v1 c1;
      xAOD::BCMRawDataContainer_v1 c2;
      /// Element link(s):
      ElementLink< xAOD::BCMRawDataContainer_v1 > el1;
      std::vector< ElementLink< xAOD::BCMRawDataContainer_v1 > > el2;
      std::vector< std::vector< ElementLink< xAOD::BCMRawDataContainer_v1 > > > el3;
      /// Data link(s):
      DataLink< xAOD::BCMRawDataContainer_v1 > dl1;
      std::vector< DataLink< xAOD::BCMRawDataContainer_v1 > > dl2;
   };
}

#endif // XAODLUMINOSITY_XAODLUMINOSITYDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::BCMRawData_v1> >", payloadCode, "@",
"DataLink<xAOD::BCMRawDataContainer_v1>", payloadCode, "@",
"DataVector<xAOD::BCMRawData_v1>", payloadCode, "@",
"DataVector<xAOD::LumiBlockRange_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::BCMRawData_v1> >", payloadCode, "@",
"ElementLink<xAOD::BCMRawDataContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::BCMRawData_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::BCMRawDataContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::BCMRawDataContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::BCMRawDataContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::BCMRawData_v1> > > >", payloadCode, "@",
"xAOD::BCMRawDataAuxContainer_v1", payloadCode, "@",
"xAOD::BCMRawDataContainer_v1", payloadCode, "@",
"xAOD::BCMRawData_v1", payloadCode, "@",
"xAOD::LumiBlockRangeAuxContainer_v1", payloadCode, "@",
"xAOD::LumiBlockRangeContainer_v1", payloadCode, "@",
"xAOD::LumiBlockRange_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODLuminosity_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODLuminosity_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODLuminosity_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODLuminosity_Reflex() {
  TriggerDictionaryInitialization_xAODLuminosity_Reflex_Impl();
}

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdITrigAnalysisInterfacesdIobjdITrigAnalysisInterfacesCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "TrigAnalysisInterfaces/IBunchCrossingConfProvider.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *TrigcLcLIBunchCrossingTool_Dictionary();
   static void TrigcLcLIBunchCrossingTool_TClassManip(TClass*);
   static void delete_TrigcLcLIBunchCrossingTool(void *p);
   static void deleteArray_TrigcLcLIBunchCrossingTool(void *p);
   static void destruct_TrigcLcLIBunchCrossingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Trig::IBunchCrossingTool*)
   {
      ::Trig::IBunchCrossingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Trig::IBunchCrossingTool));
      static ::ROOT::TGenericClassInfo 
         instance("Trig::IBunchCrossingTool", "TrigAnalysisInterfaces/IBunchCrossingTool.h", 35,
                  typeid(::Trig::IBunchCrossingTool), DefineBehavior(ptr, ptr),
                  &TrigcLcLIBunchCrossingTool_Dictionary, isa_proxy, 4,
                  sizeof(::Trig::IBunchCrossingTool) );
      instance.SetDelete(&delete_TrigcLcLIBunchCrossingTool);
      instance.SetDeleteArray(&deleteArray_TrigcLcLIBunchCrossingTool);
      instance.SetDestructor(&destruct_TrigcLcLIBunchCrossingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Trig::IBunchCrossingTool*)
   {
      return GenerateInitInstanceLocal((::Trig::IBunchCrossingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Trig::IBunchCrossingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigcLcLIBunchCrossingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Trig::IBunchCrossingTool*)0x0)->GetClass();
      TrigcLcLIBunchCrossingTool_TClassManip(theClass);
   return theClass;
   }

   static void TrigcLcLIBunchCrossingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrigcLcLIBunchCrossingConfProvider_Dictionary();
   static void TrigcLcLIBunchCrossingConfProvider_TClassManip(TClass*);
   static void delete_TrigcLcLIBunchCrossingConfProvider(void *p);
   static void deleteArray_TrigcLcLIBunchCrossingConfProvider(void *p);
   static void destruct_TrigcLcLIBunchCrossingConfProvider(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Trig::IBunchCrossingConfProvider*)
   {
      ::Trig::IBunchCrossingConfProvider *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Trig::IBunchCrossingConfProvider));
      static ::ROOT::TGenericClassInfo 
         instance("Trig::IBunchCrossingConfProvider", "TrigAnalysisInterfaces/IBunchCrossingConfProvider.h", 48,
                  typeid(::Trig::IBunchCrossingConfProvider), DefineBehavior(ptr, ptr),
                  &TrigcLcLIBunchCrossingConfProvider_Dictionary, isa_proxy, 4,
                  sizeof(::Trig::IBunchCrossingConfProvider) );
      instance.SetDelete(&delete_TrigcLcLIBunchCrossingConfProvider);
      instance.SetDeleteArray(&deleteArray_TrigcLcLIBunchCrossingConfProvider);
      instance.SetDestructor(&destruct_TrigcLcLIBunchCrossingConfProvider);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Trig::IBunchCrossingConfProvider*)
   {
      return GenerateInitInstanceLocal((::Trig::IBunchCrossingConfProvider*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Trig::IBunchCrossingConfProvider*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigcLcLIBunchCrossingConfProvider_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Trig::IBunchCrossingConfProvider*)0x0)->GetClass();
      TrigcLcLIBunchCrossingConfProvider_TClassManip(theClass);
   return theClass;
   }

   static void TrigcLcLIBunchCrossingConfProvider_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TrigcLcLIBunchCrossingTool(void *p) {
      delete ((::Trig::IBunchCrossingTool*)p);
   }
   static void deleteArray_TrigcLcLIBunchCrossingTool(void *p) {
      delete [] ((::Trig::IBunchCrossingTool*)p);
   }
   static void destruct_TrigcLcLIBunchCrossingTool(void *p) {
      typedef ::Trig::IBunchCrossingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Trig::IBunchCrossingTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TrigcLcLIBunchCrossingConfProvider(void *p) {
      delete ((::Trig::IBunchCrossingConfProvider*)p);
   }
   static void deleteArray_TrigcLcLIBunchCrossingConfProvider(void *p) {
      delete [] ((::Trig::IBunchCrossingConfProvider*)p);
   }
   static void destruct_TrigcLcLIBunchCrossingConfProvider(void *p) {
      typedef ::Trig::IBunchCrossingConfProvider current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Trig::IBunchCrossingConfProvider

namespace {
  void TriggerDictionaryInitialization_TrigAnalysisInterfacesCINT_Impl() {
    static const char* headers[] = {
"TrigAnalysisInterfaces/IBunchCrossingTool.h",
"TrigAnalysisInterfaces/IBunchCrossingConfProvider.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigAnalysisInterfaces/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigAnalysisInterfaces/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace Trig{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigAnalysisInterfaces/Root/LinkDef.h")))  IBunchCrossingTool;}
namespace Trig{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigAnalysisInterfaces/Root/LinkDef.h")))  IBunchCrossingConfProvider;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "TrigAnalysisInterfaces"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "TrigAnalysisInterfaces/IBunchCrossingConfProvider.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Trig::IBunchCrossingConfProvider", payloadCode, "@",
"Trig::IBunchCrossingTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TrigAnalysisInterfacesCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TrigAnalysisInterfacesCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TrigAnalysisInterfacesCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TrigAnalysisInterfacesCINT() {
  TriggerDictionaryInitialization_TrigAnalysisInterfacesCINT_Impl();
}

// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODMissingETdIobjdIxAODMissingET_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMissingET/xAODMissingET/xAODMissingETDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLMissingET_v1_Dictionary();
   static void xAODcLcLMissingET_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingET_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingET_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingET_v1(void *p);
   static void deleteArray_xAODcLcLMissingET_v1(void *p);
   static void destruct_xAODcLcLMissingET_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingET_v1*)
   {
      ::xAOD::MissingET_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingET_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingET_v1", "xAODMissingET/versions/MissingET_v1.h", 16,
                  typeid(::xAOD::MissingET_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingET_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingET_v1) );
      instance.SetNew(&new_xAODcLcLMissingET_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingET_v1);
      instance.SetDelete(&delete_xAODcLcLMissingET_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingET_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingET_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingET_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingET_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingET_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingET_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingET_v1*)0x0)->GetClass();
      xAODcLcLMissingET_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingET_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLMissingET_v1gR_Dictionary();
   static void DataVectorlExAODcLcLMissingET_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLMissingET_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLMissingET_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLMissingET_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLMissingET_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLMissingET_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::MissingET_v1>*)
   {
      ::DataVector<xAOD::MissingET_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::MissingET_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::MissingET_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::MissingET_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLMissingET_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::MissingET_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLMissingET_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLMissingET_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLMissingET_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLMissingET_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLMissingET_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::MissingET_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::MissingET_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::MissingET_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLMissingET_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::MissingET_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLMissingET_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLMissingET_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETContainer_v1_Dictionary();
   static void xAODcLcLMissingETContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingETContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingETContainer_v1(void *p);
   static void deleteArray_xAODcLcLMissingETContainer_v1(void *p);
   static void destruct_xAODcLcLMissingETContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETContainer_v1*)
   {
      ::xAOD::MissingETContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETContainer_v1", "xAODMissingET/versions/MissingETContainer_v1.h", 15,
                  typeid(::xAOD::MissingETContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETContainer_v1) );
      instance.SetNew(&new_xAODcLcLMissingETContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingETContainer_v1);
      instance.SetDelete(&delete_xAODcLcLMissingETContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingETContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETContainer_v1*)0x0)->GetClass();
      xAODcLcLMissingETContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","F49162FE-6BC0-49BC-A7DA-A792136BD939");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETAuxContainer_v1_Dictionary();
   static void xAODcLcLMissingETAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingETAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingETAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLMissingETAuxContainer_v1(void *p);
   static void destruct_xAODcLcLMissingETAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETAuxContainer_v1*)
   {
      ::xAOD::MissingETAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETAuxContainer_v1", "xAODMissingET/versions/MissingETAuxContainer_v1.h", 14,
                  typeid(::xAOD::MissingETAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLMissingETAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingETAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLMissingETAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingETAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLMissingETAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","2F92EC94-8CD1-49F3-BCA4-3D78599D4D60");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETComponent_v1_Dictionary();
   static void xAODcLcLMissingETComponent_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETComponent_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingETComponent_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingETComponent_v1(void *p);
   static void deleteArray_xAODcLcLMissingETComponent_v1(void *p);
   static void destruct_xAODcLcLMissingETComponent_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETComponent_v1*)
   {
      ::xAOD::MissingETComponent_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETComponent_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETComponent_v1", "xAODMissingET/versions/MissingETComponent_v1.h", 18,
                  typeid(::xAOD::MissingETComponent_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETComponent_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETComponent_v1) );
      instance.SetNew(&new_xAODcLcLMissingETComponent_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingETComponent_v1);
      instance.SetDelete(&delete_xAODcLcLMissingETComponent_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETComponent_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingETComponent_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETComponent_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETComponent_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETComponent_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETComponent_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETComponent_v1*)0x0)->GetClass();
      xAODcLcLMissingETComponent_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETComponent_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETComponent_v1cLcLWeight_Dictionary();
   static void xAODcLcLMissingETComponent_v1cLcLWeight_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETComponent_v1cLcLWeight(void *p = 0);
   static void *newArray_xAODcLcLMissingETComponent_v1cLcLWeight(Long_t size, void *p);
   static void delete_xAODcLcLMissingETComponent_v1cLcLWeight(void *p);
   static void deleteArray_xAODcLcLMissingETComponent_v1cLcLWeight(void *p);
   static void destruct_xAODcLcLMissingETComponent_v1cLcLWeight(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETComponent_v1::Weight*)
   {
      ::xAOD::MissingETComponent_v1::Weight *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETComponent_v1::Weight));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETComponent_v1::Weight", "xAODMissingET/versions/MissingETComponent_v1.h", 22,
                  typeid(::xAOD::MissingETComponent_v1::Weight), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETComponent_v1cLcLWeight_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETComponent_v1::Weight) );
      instance.SetNew(&new_xAODcLcLMissingETComponent_v1cLcLWeight);
      instance.SetNewArray(&newArray_xAODcLcLMissingETComponent_v1cLcLWeight);
      instance.SetDelete(&delete_xAODcLcLMissingETComponent_v1cLcLWeight);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETComponent_v1cLcLWeight);
      instance.SetDestructor(&destruct_xAODcLcLMissingETComponent_v1cLcLWeight);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETComponent_v1::Weight*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETComponent_v1::Weight*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETComponent_v1::Weight*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETComponent_v1cLcLWeight_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETComponent_v1::Weight*)0x0)->GetClass();
      xAODcLcLMissingETComponent_v1cLcLWeight_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETComponent_v1cLcLWeight_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLMissingETComponent_v1gR_Dictionary();
   static void DataVectorlExAODcLcLMissingETComponent_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLMissingETComponent_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLMissingETComponent_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLMissingETComponent_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLMissingETComponent_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLMissingETComponent_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::MissingETComponent_v1>*)
   {
      ::DataVector<xAOD::MissingETComponent_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::MissingETComponent_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::MissingETComponent_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::MissingETComponent_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLMissingETComponent_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::MissingETComponent_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLMissingETComponent_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLMissingETComponent_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLMissingETComponent_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLMissingETComponent_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLMissingETComponent_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::MissingETComponent_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::MissingETComponent_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::MissingETComponent_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLMissingETComponent_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::MissingETComponent_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLMissingETComponent_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLMissingETComponent_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETComponentMap_v1_Dictionary();
   static void xAODcLcLMissingETComponentMap_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETComponentMap_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingETComponentMap_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingETComponentMap_v1(void *p);
   static void deleteArray_xAODcLcLMissingETComponentMap_v1(void *p);
   static void destruct_xAODcLcLMissingETComponentMap_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETComponentMap_v1*)
   {
      ::xAOD::MissingETComponentMap_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETComponentMap_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETComponentMap_v1", "xAODMissingET/versions/MissingETComponentMap_v1.h", 19,
                  typeid(::xAOD::MissingETComponentMap_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETComponentMap_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETComponentMap_v1) );
      instance.SetNew(&new_xAODcLcLMissingETComponentMap_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingETComponentMap_v1);
      instance.SetDelete(&delete_xAODcLcLMissingETComponentMap_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETComponentMap_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingETComponentMap_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETComponentMap_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETComponentMap_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETComponentMap_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETComponentMap_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETComponentMap_v1*)0x0)->GetClass();
      xAODcLcLMissingETComponentMap_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETComponentMap_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","994D9D32-820F-47B1-A54B-37C15CD0FD1E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETAuxComponentMap_v1_Dictionary();
   static void xAODcLcLMissingETAuxComponentMap_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETAuxComponentMap_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingETAuxComponentMap_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingETAuxComponentMap_v1(void *p);
   static void deleteArray_xAODcLcLMissingETAuxComponentMap_v1(void *p);
   static void destruct_xAODcLcLMissingETAuxComponentMap_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETAuxComponentMap_v1*)
   {
      ::xAOD::MissingETAuxComponentMap_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETAuxComponentMap_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETAuxComponentMap_v1", "xAODMissingET/versions/MissingETAuxComponentMap_v1.h", 20,
                  typeid(::xAOD::MissingETAuxComponentMap_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETAuxComponentMap_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETAuxComponentMap_v1) );
      instance.SetNew(&new_xAODcLcLMissingETAuxComponentMap_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingETAuxComponentMap_v1);
      instance.SetDelete(&delete_xAODcLcLMissingETAuxComponentMap_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETAuxComponentMap_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingETAuxComponentMap_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETAuxComponentMap_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETAuxComponentMap_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETAuxComponentMap_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETAuxComponentMap_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETAuxComponentMap_v1*)0x0)->GetClass();
      xAODcLcLMissingETAuxComponentMap_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETAuxComponentMap_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","96287268-4165-4288-8FC1-6227FD4AB95D");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETAssociation_v1_Dictionary();
   static void xAODcLcLMissingETAssociation_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETAssociation_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingETAssociation_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingETAssociation_v1(void *p);
   static void deleteArray_xAODcLcLMissingETAssociation_v1(void *p);
   static void destruct_xAODcLcLMissingETAssociation_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETAssociation_v1*)
   {
      ::xAOD::MissingETAssociation_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETAssociation_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETAssociation_v1", "xAODMissingET/versions/MissingETAssociation_v1.h", 19,
                  typeid(::xAOD::MissingETAssociation_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETAssociation_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETAssociation_v1) );
      instance.SetNew(&new_xAODcLcLMissingETAssociation_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingETAssociation_v1);
      instance.SetDelete(&delete_xAODcLcLMissingETAssociation_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETAssociation_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingETAssociation_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETAssociation_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETAssociation_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETAssociation_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETAssociation_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETAssociation_v1*)0x0)->GetClass();
      xAODcLcLMissingETAssociation_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETAssociation_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETAssociation_v1cLcLConstVec_Dictionary();
   static void xAODcLcLMissingETAssociation_v1cLcLConstVec_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETAssociation_v1cLcLConstVec(void *p = 0);
   static void *newArray_xAODcLcLMissingETAssociation_v1cLcLConstVec(Long_t size, void *p);
   static void delete_xAODcLcLMissingETAssociation_v1cLcLConstVec(void *p);
   static void deleteArray_xAODcLcLMissingETAssociation_v1cLcLConstVec(void *p);
   static void destruct_xAODcLcLMissingETAssociation_v1cLcLConstVec(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETAssociation_v1::ConstVec*)
   {
      ::xAOD::MissingETAssociation_v1::ConstVec *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETAssociation_v1::ConstVec));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETAssociation_v1::ConstVec", "xAODMissingET/versions/MissingETAssociation_v1.h", 26,
                  typeid(::xAOD::MissingETAssociation_v1::ConstVec), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETAssociation_v1cLcLConstVec_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETAssociation_v1::ConstVec) );
      instance.SetNew(&new_xAODcLcLMissingETAssociation_v1cLcLConstVec);
      instance.SetNewArray(&newArray_xAODcLcLMissingETAssociation_v1cLcLConstVec);
      instance.SetDelete(&delete_xAODcLcLMissingETAssociation_v1cLcLConstVec);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETAssociation_v1cLcLConstVec);
      instance.SetDestructor(&destruct_xAODcLcLMissingETAssociation_v1cLcLConstVec);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETAssociation_v1::ConstVec*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETAssociation_v1::ConstVec*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETAssociation_v1::ConstVec*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETAssociation_v1cLcLConstVec_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETAssociation_v1::ConstVec*)0x0)->GetClass();
      xAODcLcLMissingETAssociation_v1cLcLConstVec_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETAssociation_v1cLcLConstVec_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLMissingETAssociation_v1gR_Dictionary();
   static void DataVectorlExAODcLcLMissingETAssociation_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLMissingETAssociation_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLMissingETAssociation_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLMissingETAssociation_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLMissingETAssociation_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLMissingETAssociation_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::MissingETAssociation_v1>*)
   {
      ::DataVector<xAOD::MissingETAssociation_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::MissingETAssociation_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::MissingETAssociation_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::MissingETAssociation_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLMissingETAssociation_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::MissingETAssociation_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLMissingETAssociation_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLMissingETAssociation_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLMissingETAssociation_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLMissingETAssociation_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLMissingETAssociation_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::MissingETAssociation_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::MissingETAssociation_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::MissingETAssociation_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLMissingETAssociation_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::MissingETAssociation_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLMissingETAssociation_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLMissingETAssociation_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETAssociationMap_v1_Dictionary();
   static void xAODcLcLMissingETAssociationMap_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETAssociationMap_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingETAssociationMap_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingETAssociationMap_v1(void *p);
   static void deleteArray_xAODcLcLMissingETAssociationMap_v1(void *p);
   static void destruct_xAODcLcLMissingETAssociationMap_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETAssociationMap_v1*)
   {
      ::xAOD::MissingETAssociationMap_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETAssociationMap_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETAssociationMap_v1", "xAODMissingET/versions/MissingETAssociationMap_v1.h", 32,
                  typeid(::xAOD::MissingETAssociationMap_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETAssociationMap_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETAssociationMap_v1) );
      instance.SetNew(&new_xAODcLcLMissingETAssociationMap_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingETAssociationMap_v1);
      instance.SetDelete(&delete_xAODcLcLMissingETAssociationMap_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETAssociationMap_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingETAssociationMap_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETAssociationMap_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETAssociationMap_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETAssociationMap_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETAssociationMap_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETAssociationMap_v1*)0x0)->GetClass();
      xAODcLcLMissingETAssociationMap_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETAssociationMap_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E2EAA116-F03E-430E-B4EF-216AAF7DEEE8");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLMissingETAuxAssociationMap_v1_Dictionary();
   static void xAODcLcLMissingETAuxAssociationMap_v1_TClassManip(TClass*);
   static void *new_xAODcLcLMissingETAuxAssociationMap_v1(void *p = 0);
   static void *newArray_xAODcLcLMissingETAuxAssociationMap_v1(Long_t size, void *p);
   static void delete_xAODcLcLMissingETAuxAssociationMap_v1(void *p);
   static void deleteArray_xAODcLcLMissingETAuxAssociationMap_v1(void *p);
   static void destruct_xAODcLcLMissingETAuxAssociationMap_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::MissingETAuxAssociationMap_v1*)
   {
      ::xAOD::MissingETAuxAssociationMap_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::MissingETAuxAssociationMap_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::MissingETAuxAssociationMap_v1", "xAODMissingET/versions/MissingETAuxAssociationMap_v1.h", 20,
                  typeid(::xAOD::MissingETAuxAssociationMap_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLMissingETAuxAssociationMap_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::MissingETAuxAssociationMap_v1) );
      instance.SetNew(&new_xAODcLcLMissingETAuxAssociationMap_v1);
      instance.SetNewArray(&newArray_xAODcLcLMissingETAuxAssociationMap_v1);
      instance.SetDelete(&delete_xAODcLcLMissingETAuxAssociationMap_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLMissingETAuxAssociationMap_v1);
      instance.SetDestructor(&destruct_xAODcLcLMissingETAuxAssociationMap_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::MissingETAuxAssociationMap_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::MissingETAuxAssociationMap_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::MissingETAuxAssociationMap_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLMissingETAuxAssociationMap_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::MissingETAuxAssociationMap_v1*)0x0)->GetClass();
      xAODcLcLMissingETAuxAssociationMap_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLMissingETAuxAssociationMap_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","48EEF1CD-F937-445C-A09C-B978D152868E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklExAODcLcLMissingETContainer_v1gR_Dictionary();
   static void DataLinklExAODcLcLMissingETContainer_v1gR_TClassManip(TClass*);
   static void *new_DataLinklExAODcLcLMissingETContainer_v1gR(void *p = 0);
   static void *newArray_DataLinklExAODcLcLMissingETContainer_v1gR(Long_t size, void *p);
   static void delete_DataLinklExAODcLcLMissingETContainer_v1gR(void *p);
   static void deleteArray_DataLinklExAODcLcLMissingETContainer_v1gR(void *p);
   static void destruct_DataLinklExAODcLcLMissingETContainer_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<xAOD::MissingETContainer_v1>*)
   {
      ::DataLink<xAOD::MissingETContainer_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<xAOD::MissingETContainer_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<xAOD::MissingETContainer_v1>", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<xAOD::MissingETContainer_v1>), DefineBehavior(ptr, ptr),
                  &DataLinklExAODcLcLMissingETContainer_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<xAOD::MissingETContainer_v1>) );
      instance.SetNew(&new_DataLinklExAODcLcLMissingETContainer_v1gR);
      instance.SetNewArray(&newArray_DataLinklExAODcLcLMissingETContainer_v1gR);
      instance.SetDelete(&delete_DataLinklExAODcLcLMissingETContainer_v1gR);
      instance.SetDeleteArray(&deleteArray_DataLinklExAODcLcLMissingETContainer_v1gR);
      instance.SetDestructor(&destruct_DataLinklExAODcLcLMissingETContainer_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<xAOD::MissingETContainer_v1>*)
   {
      return GenerateInitInstanceLocal((::DataLink<xAOD::MissingETContainer_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<xAOD::MissingETContainer_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklExAODcLcLMissingETContainer_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<xAOD::MissingETContainer_v1>*)0x0)->GetClass();
      DataLinklExAODcLcLMissingETContainer_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklExAODcLcLMissingETContainer_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklExAODcLcLMissingETComponentMap_v1gR_Dictionary();
   static void DataLinklExAODcLcLMissingETComponentMap_v1gR_TClassManip(TClass*);
   static void *new_DataLinklExAODcLcLMissingETComponentMap_v1gR(void *p = 0);
   static void *newArray_DataLinklExAODcLcLMissingETComponentMap_v1gR(Long_t size, void *p);
   static void delete_DataLinklExAODcLcLMissingETComponentMap_v1gR(void *p);
   static void deleteArray_DataLinklExAODcLcLMissingETComponentMap_v1gR(void *p);
   static void destruct_DataLinklExAODcLcLMissingETComponentMap_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<xAOD::MissingETComponentMap_v1>*)
   {
      ::DataLink<xAOD::MissingETComponentMap_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<xAOD::MissingETComponentMap_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<xAOD::MissingETComponentMap_v1>", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<xAOD::MissingETComponentMap_v1>), DefineBehavior(ptr, ptr),
                  &DataLinklExAODcLcLMissingETComponentMap_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<xAOD::MissingETComponentMap_v1>) );
      instance.SetNew(&new_DataLinklExAODcLcLMissingETComponentMap_v1gR);
      instance.SetNewArray(&newArray_DataLinklExAODcLcLMissingETComponentMap_v1gR);
      instance.SetDelete(&delete_DataLinklExAODcLcLMissingETComponentMap_v1gR);
      instance.SetDeleteArray(&deleteArray_DataLinklExAODcLcLMissingETComponentMap_v1gR);
      instance.SetDestructor(&destruct_DataLinklExAODcLcLMissingETComponentMap_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<xAOD::MissingETComponentMap_v1>*)
   {
      return GenerateInitInstanceLocal((::DataLink<xAOD::MissingETComponentMap_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<xAOD::MissingETComponentMap_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklExAODcLcLMissingETComponentMap_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<xAOD::MissingETComponentMap_v1>*)0x0)->GetClass();
      DataLinklExAODcLcLMissingETComponentMap_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklExAODcLcLMissingETComponentMap_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklExAODcLcLMissingETContainer_v1gR_Dictionary();
   static void ElementLinklExAODcLcLMissingETContainer_v1gR_TClassManip(TClass*);
   static void *new_ElementLinklExAODcLcLMissingETContainer_v1gR(void *p = 0);
   static void *newArray_ElementLinklExAODcLcLMissingETContainer_v1gR(Long_t size, void *p);
   static void delete_ElementLinklExAODcLcLMissingETContainer_v1gR(void *p);
   static void deleteArray_ElementLinklExAODcLcLMissingETContainer_v1gR(void *p);
   static void destruct_ElementLinklExAODcLcLMissingETContainer_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<xAOD::MissingETContainer_v1>*)
   {
      ::ElementLink<xAOD::MissingETContainer_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<xAOD::MissingETContainer_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<xAOD::MissingETContainer_v1>", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<xAOD::MissingETContainer_v1>), DefineBehavior(ptr, ptr),
                  &ElementLinklExAODcLcLMissingETContainer_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<xAOD::MissingETContainer_v1>) );
      instance.SetNew(&new_ElementLinklExAODcLcLMissingETContainer_v1gR);
      instance.SetNewArray(&newArray_ElementLinklExAODcLcLMissingETContainer_v1gR);
      instance.SetDelete(&delete_ElementLinklExAODcLcLMissingETContainer_v1gR);
      instance.SetDeleteArray(&deleteArray_ElementLinklExAODcLcLMissingETContainer_v1gR);
      instance.SetDestructor(&destruct_ElementLinklExAODcLcLMissingETContainer_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<xAOD::MissingETContainer_v1>*)
   {
      return GenerateInitInstanceLocal((::ElementLink<xAOD::MissingETContainer_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<xAOD::MissingETContainer_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklExAODcLcLMissingETContainer_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<xAOD::MissingETContainer_v1>*)0x0)->GetClass();
      ElementLinklExAODcLcLMissingETContainer_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklExAODcLcLMissingETContainer_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklExAODcLcLMissingETComponentMap_v1gR_Dictionary();
   static void ElementLinklExAODcLcLMissingETComponentMap_v1gR_TClassManip(TClass*);
   static void *new_ElementLinklExAODcLcLMissingETComponentMap_v1gR(void *p = 0);
   static void *newArray_ElementLinklExAODcLcLMissingETComponentMap_v1gR(Long_t size, void *p);
   static void delete_ElementLinklExAODcLcLMissingETComponentMap_v1gR(void *p);
   static void deleteArray_ElementLinklExAODcLcLMissingETComponentMap_v1gR(void *p);
   static void destruct_ElementLinklExAODcLcLMissingETComponentMap_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<xAOD::MissingETComponentMap_v1>*)
   {
      ::ElementLink<xAOD::MissingETComponentMap_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<xAOD::MissingETComponentMap_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<xAOD::MissingETComponentMap_v1>", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<xAOD::MissingETComponentMap_v1>), DefineBehavior(ptr, ptr),
                  &ElementLinklExAODcLcLMissingETComponentMap_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<xAOD::MissingETComponentMap_v1>) );
      instance.SetNew(&new_ElementLinklExAODcLcLMissingETComponentMap_v1gR);
      instance.SetNewArray(&newArray_ElementLinklExAODcLcLMissingETComponentMap_v1gR);
      instance.SetDelete(&delete_ElementLinklExAODcLcLMissingETComponentMap_v1gR);
      instance.SetDeleteArray(&deleteArray_ElementLinklExAODcLcLMissingETComponentMap_v1gR);
      instance.SetDestructor(&destruct_ElementLinklExAODcLcLMissingETComponentMap_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<xAOD::MissingETComponentMap_v1>*)
   {
      return GenerateInitInstanceLocal((::ElementLink<xAOD::MissingETComponentMap_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<xAOD::MissingETComponentMap_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklExAODcLcLMissingETComponentMap_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<xAOD::MissingETComponentMap_v1>*)0x0)->GetClass();
      ElementLinklExAODcLcLMissingETComponentMap_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklExAODcLcLMissingETComponentMap_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklExAODcLcLMissingETAssociationMap_v1gR_Dictionary();
   static void ElementLinklExAODcLcLMissingETAssociationMap_v1gR_TClassManip(TClass*);
   static void *new_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(void *p = 0);
   static void *newArray_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(Long_t size, void *p);
   static void delete_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(void *p);
   static void deleteArray_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(void *p);
   static void destruct_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<xAOD::MissingETAssociationMap_v1>*)
   {
      ::ElementLink<xAOD::MissingETAssociationMap_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<xAOD::MissingETAssociationMap_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<xAOD::MissingETAssociationMap_v1>", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<xAOD::MissingETAssociationMap_v1>), DefineBehavior(ptr, ptr),
                  &ElementLinklExAODcLcLMissingETAssociationMap_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<xAOD::MissingETAssociationMap_v1>) );
      instance.SetNew(&new_ElementLinklExAODcLcLMissingETAssociationMap_v1gR);
      instance.SetNewArray(&newArray_ElementLinklExAODcLcLMissingETAssociationMap_v1gR);
      instance.SetDelete(&delete_ElementLinklExAODcLcLMissingETAssociationMap_v1gR);
      instance.SetDeleteArray(&deleteArray_ElementLinklExAODcLcLMissingETAssociationMap_v1gR);
      instance.SetDestructor(&destruct_ElementLinklExAODcLcLMissingETAssociationMap_v1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<xAOD::MissingETAssociationMap_v1>*)
   {
      return GenerateInitInstanceLocal((::ElementLink<xAOD::MissingETAssociationMap_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<xAOD::MissingETAssociationMap_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklExAODcLcLMissingETAssociationMap_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<xAOD::MissingETAssociationMap_v1>*)0x0)->GetClass();
      ElementLinklExAODcLcLMissingETAssociationMap_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklExAODcLcLMissingETAssociationMap_v1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingET_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingET_v1 : new ::xAOD::MissingET_v1;
   }
   static void *newArray_xAODcLcLMissingET_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingET_v1[nElements] : new ::xAOD::MissingET_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingET_v1(void *p) {
      delete ((::xAOD::MissingET_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingET_v1(void *p) {
      delete [] ((::xAOD::MissingET_v1*)p);
   }
   static void destruct_xAODcLcLMissingET_v1(void *p) {
      typedef ::xAOD::MissingET_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingET_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLMissingET_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::MissingET_v1> : new ::DataVector<xAOD::MissingET_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLMissingET_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::MissingET_v1>[nElements] : new ::DataVector<xAOD::MissingET_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLMissingET_v1gR(void *p) {
      delete ((::DataVector<xAOD::MissingET_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLMissingET_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::MissingET_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLMissingET_v1gR(void *p) {
      typedef ::DataVector<xAOD::MissingET_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::MissingET_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETContainer_v1 : new ::xAOD::MissingETContainer_v1;
   }
   static void *newArray_xAODcLcLMissingETContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETContainer_v1[nElements] : new ::xAOD::MissingETContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETContainer_v1(void *p) {
      delete ((::xAOD::MissingETContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingETContainer_v1(void *p) {
      delete [] ((::xAOD::MissingETContainer_v1*)p);
   }
   static void destruct_xAODcLcLMissingETContainer_v1(void *p) {
      typedef ::xAOD::MissingETContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAuxContainer_v1 : new ::xAOD::MissingETAuxContainer_v1;
   }
   static void *newArray_xAODcLcLMissingETAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAuxContainer_v1[nElements] : new ::xAOD::MissingETAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETAuxContainer_v1(void *p) {
      delete ((::xAOD::MissingETAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingETAuxContainer_v1(void *p) {
      delete [] ((::xAOD::MissingETAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLMissingETAuxContainer_v1(void *p) {
      typedef ::xAOD::MissingETAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETComponent_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETComponent_v1 : new ::xAOD::MissingETComponent_v1;
   }
   static void *newArray_xAODcLcLMissingETComponent_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETComponent_v1[nElements] : new ::xAOD::MissingETComponent_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETComponent_v1(void *p) {
      delete ((::xAOD::MissingETComponent_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingETComponent_v1(void *p) {
      delete [] ((::xAOD::MissingETComponent_v1*)p);
   }
   static void destruct_xAODcLcLMissingETComponent_v1(void *p) {
      typedef ::xAOD::MissingETComponent_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETComponent_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETComponent_v1cLcLWeight(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETComponent_v1::Weight : new ::xAOD::MissingETComponent_v1::Weight;
   }
   static void *newArray_xAODcLcLMissingETComponent_v1cLcLWeight(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETComponent_v1::Weight[nElements] : new ::xAOD::MissingETComponent_v1::Weight[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETComponent_v1cLcLWeight(void *p) {
      delete ((::xAOD::MissingETComponent_v1::Weight*)p);
   }
   static void deleteArray_xAODcLcLMissingETComponent_v1cLcLWeight(void *p) {
      delete [] ((::xAOD::MissingETComponent_v1::Weight*)p);
   }
   static void destruct_xAODcLcLMissingETComponent_v1cLcLWeight(void *p) {
      typedef ::xAOD::MissingETComponent_v1::Weight current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETComponent_v1::Weight

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLMissingETComponent_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::MissingETComponent_v1> : new ::DataVector<xAOD::MissingETComponent_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLMissingETComponent_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::MissingETComponent_v1>[nElements] : new ::DataVector<xAOD::MissingETComponent_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLMissingETComponent_v1gR(void *p) {
      delete ((::DataVector<xAOD::MissingETComponent_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLMissingETComponent_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::MissingETComponent_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLMissingETComponent_v1gR(void *p) {
      typedef ::DataVector<xAOD::MissingETComponent_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::MissingETComponent_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETComponentMap_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETComponentMap_v1 : new ::xAOD::MissingETComponentMap_v1;
   }
   static void *newArray_xAODcLcLMissingETComponentMap_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETComponentMap_v1[nElements] : new ::xAOD::MissingETComponentMap_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETComponentMap_v1(void *p) {
      delete ((::xAOD::MissingETComponentMap_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingETComponentMap_v1(void *p) {
      delete [] ((::xAOD::MissingETComponentMap_v1*)p);
   }
   static void destruct_xAODcLcLMissingETComponentMap_v1(void *p) {
      typedef ::xAOD::MissingETComponentMap_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETComponentMap_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETAuxComponentMap_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAuxComponentMap_v1 : new ::xAOD::MissingETAuxComponentMap_v1;
   }
   static void *newArray_xAODcLcLMissingETAuxComponentMap_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAuxComponentMap_v1[nElements] : new ::xAOD::MissingETAuxComponentMap_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETAuxComponentMap_v1(void *p) {
      delete ((::xAOD::MissingETAuxComponentMap_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingETAuxComponentMap_v1(void *p) {
      delete [] ((::xAOD::MissingETAuxComponentMap_v1*)p);
   }
   static void destruct_xAODcLcLMissingETAuxComponentMap_v1(void *p) {
      typedef ::xAOD::MissingETAuxComponentMap_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETAuxComponentMap_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETAssociation_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAssociation_v1 : new ::xAOD::MissingETAssociation_v1;
   }
   static void *newArray_xAODcLcLMissingETAssociation_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAssociation_v1[nElements] : new ::xAOD::MissingETAssociation_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETAssociation_v1(void *p) {
      delete ((::xAOD::MissingETAssociation_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingETAssociation_v1(void *p) {
      delete [] ((::xAOD::MissingETAssociation_v1*)p);
   }
   static void destruct_xAODcLcLMissingETAssociation_v1(void *p) {
      typedef ::xAOD::MissingETAssociation_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETAssociation_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETAssociation_v1cLcLConstVec(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAssociation_v1::ConstVec : new ::xAOD::MissingETAssociation_v1::ConstVec;
   }
   static void *newArray_xAODcLcLMissingETAssociation_v1cLcLConstVec(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAssociation_v1::ConstVec[nElements] : new ::xAOD::MissingETAssociation_v1::ConstVec[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETAssociation_v1cLcLConstVec(void *p) {
      delete ((::xAOD::MissingETAssociation_v1::ConstVec*)p);
   }
   static void deleteArray_xAODcLcLMissingETAssociation_v1cLcLConstVec(void *p) {
      delete [] ((::xAOD::MissingETAssociation_v1::ConstVec*)p);
   }
   static void destruct_xAODcLcLMissingETAssociation_v1cLcLConstVec(void *p) {
      typedef ::xAOD::MissingETAssociation_v1::ConstVec current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETAssociation_v1::ConstVec

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLMissingETAssociation_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::MissingETAssociation_v1> : new ::DataVector<xAOD::MissingETAssociation_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLMissingETAssociation_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::MissingETAssociation_v1>[nElements] : new ::DataVector<xAOD::MissingETAssociation_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLMissingETAssociation_v1gR(void *p) {
      delete ((::DataVector<xAOD::MissingETAssociation_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLMissingETAssociation_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::MissingETAssociation_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLMissingETAssociation_v1gR(void *p) {
      typedef ::DataVector<xAOD::MissingETAssociation_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::MissingETAssociation_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETAssociationMap_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAssociationMap_v1 : new ::xAOD::MissingETAssociationMap_v1;
   }
   static void *newArray_xAODcLcLMissingETAssociationMap_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAssociationMap_v1[nElements] : new ::xAOD::MissingETAssociationMap_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETAssociationMap_v1(void *p) {
      delete ((::xAOD::MissingETAssociationMap_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingETAssociationMap_v1(void *p) {
      delete [] ((::xAOD::MissingETAssociationMap_v1*)p);
   }
   static void destruct_xAODcLcLMissingETAssociationMap_v1(void *p) {
      typedef ::xAOD::MissingETAssociationMap_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETAssociationMap_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLMissingETAuxAssociationMap_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAuxAssociationMap_v1 : new ::xAOD::MissingETAuxAssociationMap_v1;
   }
   static void *newArray_xAODcLcLMissingETAuxAssociationMap_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::MissingETAuxAssociationMap_v1[nElements] : new ::xAOD::MissingETAuxAssociationMap_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLMissingETAuxAssociationMap_v1(void *p) {
      delete ((::xAOD::MissingETAuxAssociationMap_v1*)p);
   }
   static void deleteArray_xAODcLcLMissingETAuxAssociationMap_v1(void *p) {
      delete [] ((::xAOD::MissingETAuxAssociationMap_v1*)p);
   }
   static void destruct_xAODcLcLMissingETAuxAssociationMap_v1(void *p) {
      typedef ::xAOD::MissingETAuxAssociationMap_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::MissingETAuxAssociationMap_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklExAODcLcLMissingETContainer_v1gR(void *p) {
      return  p ? new(p) ::DataLink<xAOD::MissingETContainer_v1> : new ::DataLink<xAOD::MissingETContainer_v1>;
   }
   static void *newArray_DataLinklExAODcLcLMissingETContainer_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<xAOD::MissingETContainer_v1>[nElements] : new ::DataLink<xAOD::MissingETContainer_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklExAODcLcLMissingETContainer_v1gR(void *p) {
      delete ((::DataLink<xAOD::MissingETContainer_v1>*)p);
   }
   static void deleteArray_DataLinklExAODcLcLMissingETContainer_v1gR(void *p) {
      delete [] ((::DataLink<xAOD::MissingETContainer_v1>*)p);
   }
   static void destruct_DataLinklExAODcLcLMissingETContainer_v1gR(void *p) {
      typedef ::DataLink<xAOD::MissingETContainer_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<xAOD::MissingETContainer_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklExAODcLcLMissingETComponentMap_v1gR(void *p) {
      return  p ? new(p) ::DataLink<xAOD::MissingETComponentMap_v1> : new ::DataLink<xAOD::MissingETComponentMap_v1>;
   }
   static void *newArray_DataLinklExAODcLcLMissingETComponentMap_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<xAOD::MissingETComponentMap_v1>[nElements] : new ::DataLink<xAOD::MissingETComponentMap_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklExAODcLcLMissingETComponentMap_v1gR(void *p) {
      delete ((::DataLink<xAOD::MissingETComponentMap_v1>*)p);
   }
   static void deleteArray_DataLinklExAODcLcLMissingETComponentMap_v1gR(void *p) {
      delete [] ((::DataLink<xAOD::MissingETComponentMap_v1>*)p);
   }
   static void destruct_DataLinklExAODcLcLMissingETComponentMap_v1gR(void *p) {
      typedef ::DataLink<xAOD::MissingETComponentMap_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<xAOD::MissingETComponentMap_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklExAODcLcLMissingETContainer_v1gR(void *p) {
      return  p ? new(p) ::ElementLink<xAOD::MissingETContainer_v1> : new ::ElementLink<xAOD::MissingETContainer_v1>;
   }
   static void *newArray_ElementLinklExAODcLcLMissingETContainer_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<xAOD::MissingETContainer_v1>[nElements] : new ::ElementLink<xAOD::MissingETContainer_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklExAODcLcLMissingETContainer_v1gR(void *p) {
      delete ((::ElementLink<xAOD::MissingETContainer_v1>*)p);
   }
   static void deleteArray_ElementLinklExAODcLcLMissingETContainer_v1gR(void *p) {
      delete [] ((::ElementLink<xAOD::MissingETContainer_v1>*)p);
   }
   static void destruct_ElementLinklExAODcLcLMissingETContainer_v1gR(void *p) {
      typedef ::ElementLink<xAOD::MissingETContainer_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<xAOD::MissingETContainer_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklExAODcLcLMissingETComponentMap_v1gR(void *p) {
      return  p ? new(p) ::ElementLink<xAOD::MissingETComponentMap_v1> : new ::ElementLink<xAOD::MissingETComponentMap_v1>;
   }
   static void *newArray_ElementLinklExAODcLcLMissingETComponentMap_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<xAOD::MissingETComponentMap_v1>[nElements] : new ::ElementLink<xAOD::MissingETComponentMap_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklExAODcLcLMissingETComponentMap_v1gR(void *p) {
      delete ((::ElementLink<xAOD::MissingETComponentMap_v1>*)p);
   }
   static void deleteArray_ElementLinklExAODcLcLMissingETComponentMap_v1gR(void *p) {
      delete [] ((::ElementLink<xAOD::MissingETComponentMap_v1>*)p);
   }
   static void destruct_ElementLinklExAODcLcLMissingETComponentMap_v1gR(void *p) {
      typedef ::ElementLink<xAOD::MissingETComponentMap_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<xAOD::MissingETComponentMap_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(void *p) {
      return  p ? new(p) ::ElementLink<xAOD::MissingETAssociationMap_v1> : new ::ElementLink<xAOD::MissingETAssociationMap_v1>;
   }
   static void *newArray_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<xAOD::MissingETAssociationMap_v1>[nElements] : new ::ElementLink<xAOD::MissingETAssociationMap_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(void *p) {
      delete ((::ElementLink<xAOD::MissingETAssociationMap_v1>*)p);
   }
   static void deleteArray_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(void *p) {
      delete [] ((::ElementLink<xAOD::MissingETAssociationMap_v1>*)p);
   }
   static void destruct_ElementLinklExAODcLcLMissingETAssociationMap_v1gR(void *p) {
      typedef ::ElementLink<xAOD::MissingETAssociationMap_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<xAOD::MissingETAssociationMap_v1>

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<unsigned long> > >*)
   {
      vector<vector<vector<unsigned long> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<unsigned long> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<unsigned long> > >", -2, "vector", 210,
                  typeid(vector<vector<vector<unsigned long> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<unsigned long> > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<unsigned long> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<unsigned long> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<unsigned long> > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<unsigned long> > > : new vector<vector<vector<unsigned long> > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<unsigned long> > >[nElements] : new vector<vector<vector<unsigned long> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<unsigned long> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<unsigned long> > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEunsignedsPlonggRsPgRsPgR(void *p) {
      typedef vector<vector<vector<unsigned long> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<unsigned long> > >

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<unsigned char> > >*)
   {
      vector<vector<vector<unsigned char> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<unsigned char> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<unsigned char> > >", -2, "vector", 210,
                  typeid(vector<vector<vector<unsigned char> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<unsigned char> > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<unsigned char> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<unsigned char> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<unsigned char> > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<unsigned char> > > : new vector<vector<vector<unsigned char> > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<unsigned char> > >[nElements] : new vector<vector<vector<unsigned char> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<unsigned char> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<unsigned char> > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEunsignedsPchargRsPgRsPgR(void *p) {
      typedef vector<vector<vector<unsigned char> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<unsigned char> > >

namespace ROOT {
   static TClass *vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<vector<ULong64_t> > >*)
   {
      vector<vector<vector<ULong64_t> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<vector<ULong64_t> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<vector<ULong64_t> > >", -2, "vector", 210,
                  typeid(vector<vector<vector<ULong64_t> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<vector<ULong64_t> > >) );
      instance.SetNew(&new_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<vector<ULong64_t> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<vector<ULong64_t> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<vector<ULong64_t> > >*)0x0)->GetClass();
      vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<ULong64_t> > > : new vector<vector<vector<ULong64_t> > >;
   }
   static void *newArray_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<vector<ULong64_t> > >[nElements] : new vector<vector<vector<ULong64_t> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(void *p) {
      delete ((vector<vector<vector<ULong64_t> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<vector<ULong64_t> > >*)p);
   }
   static void destruct_vectorlEvectorlEvectorlEULong64_tgRsPgRsPgR(void *p) {
      typedef vector<vector<vector<ULong64_t> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<vector<ULong64_t> > >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPlonggRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPlonggRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPlonggRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned long> >*)
   {
      vector<vector<unsigned long> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned long> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned long> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned long> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned long> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPlonggRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned long> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned long> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPlonggRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned long> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPlonggRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned long> > : new vector<vector<unsigned long> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPlonggRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned long> >[nElements] : new vector<vector<unsigned long> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      delete ((vector<vector<unsigned long> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      delete [] ((vector<vector<unsigned long> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPlonggRsPgR(void *p) {
      typedef vector<vector<unsigned long> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned long> >

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPchargRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPchargRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPchargRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPchargRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPchargRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPchargRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned char> >*)
   {
      vector<vector<unsigned char> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned char> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned char> >", -2, "vector", 210,
                  typeid(vector<vector<unsigned char> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPchargRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<unsigned char> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPchargRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned char> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned char> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPchargRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned char> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPchargRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned char> > : new vector<vector<unsigned char> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPchargRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<unsigned char> >[nElements] : new vector<vector<unsigned char> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      delete ((vector<vector<unsigned char> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      delete [] ((vector<vector<unsigned char> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPchargRsPgR(void *p) {
      typedef vector<vector<unsigned char> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned char> >

namespace ROOT {
   static TClass *vectorlEvectorlEULong64_tgRsPgR_Dictionary();
   static void vectorlEvectorlEULong64_tgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEULong64_tgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEULong64_tgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEULong64_tgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEULong64_tgRsPgR(void *p);
   static void destruct_vectorlEvectorlEULong64_tgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ULong64_t> >*)
   {
      vector<vector<ULong64_t> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ULong64_t> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ULong64_t> >", -2, "vector", 210,
                  typeid(vector<vector<ULong64_t> >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEULong64_tgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ULong64_t> >) );
      instance.SetNew(&new_vectorlEvectorlEULong64_tgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEULong64_tgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEULong64_tgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEULong64_tgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEULong64_tgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ULong64_t> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ULong64_t> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEULong64_tgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ULong64_t> >*)0x0)->GetClass();
      vectorlEvectorlEULong64_tgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEULong64_tgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEULong64_tgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ULong64_t> > : new vector<vector<ULong64_t> >;
   }
   static void *newArray_vectorlEvectorlEULong64_tgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ULong64_t> >[nElements] : new vector<vector<ULong64_t> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEULong64_tgRsPgR(void *p) {
      delete ((vector<vector<ULong64_t> >*)p);
   }
   static void deleteArray_vectorlEvectorlEULong64_tgRsPgR(void *p) {
      delete [] ((vector<vector<ULong64_t> >*)p);
   }
   static void destruct_vectorlEvectorlEULong64_tgRsPgR(void *p) {
      typedef vector<vector<ULong64_t> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ULong64_t> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >*)
   {
      vector<vector<ElementLink<xAOD::MissingETContainer_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<xAOD::MissingETContainer_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::MissingETContainer_v1> > > : new vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >[nElements] : new vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<xAOD::MissingETContainer_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >*)
   {
      vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > > : new vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >[nElements] : new vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >*)
   {
      vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > > : new vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >[nElements] : new vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >

namespace ROOT {
   static TClass *vectorlEunsignedsPlonggR_Dictionary();
   static void vectorlEunsignedsPlonggR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPlonggR(void *p = 0);
   static void *newArray_vectorlEunsignedsPlonggR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPlonggR(void *p);
   static void deleteArray_vectorlEunsignedsPlonggR(void *p);
   static void destruct_vectorlEunsignedsPlonggR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned long>*)
   {
      vector<unsigned long> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned long>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned long>", -2, "vector", 210,
                  typeid(vector<unsigned long>), DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPlonggR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned long>) );
      instance.SetNew(&new_vectorlEunsignedsPlonggR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPlonggR);
      instance.SetDelete(&delete_vectorlEunsignedsPlonggR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPlonggR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPlonggR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned long> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned long>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPlonggR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned long>*)0x0)->GetClass();
      vectorlEunsignedsPlonggR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPlonggR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPlonggR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned long> : new vector<unsigned long>;
   }
   static void *newArray_vectorlEunsignedsPlonggR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned long>[nElements] : new vector<unsigned long>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPlonggR(void *p) {
      delete ((vector<unsigned long>*)p);
   }
   static void deleteArray_vectorlEunsignedsPlonggR(void *p) {
      delete [] ((vector<unsigned long>*)p);
   }
   static void destruct_vectorlEunsignedsPlonggR(void *p) {
      typedef vector<unsigned long> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned long>

namespace ROOT {
   static TClass *vectorlEunsignedsPchargR_Dictionary();
   static void vectorlEunsignedsPchargR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPchargR(void *p = 0);
   static void *newArray_vectorlEunsignedsPchargR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPchargR(void *p);
   static void deleteArray_vectorlEunsignedsPchargR(void *p);
   static void destruct_vectorlEunsignedsPchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned char>*)
   {
      vector<unsigned char> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned char>", -2, "vector", 210,
                  typeid(vector<unsigned char>), DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPchargR_Dictionary, isa_proxy, 4,
                  sizeof(vector<unsigned char>) );
      instance.SetNew(&new_vectorlEunsignedsPchargR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPchargR);
      instance.SetDelete(&delete_vectorlEunsignedsPchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPchargR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned char> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<unsigned char>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned char>*)0x0)->GetClass();
      vectorlEunsignedsPchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPchargR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned char> : new vector<unsigned char>;
   }
   static void *newArray_vectorlEunsignedsPchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<unsigned char>[nElements] : new vector<unsigned char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPchargR(void *p) {
      delete ((vector<unsigned char>*)p);
   }
   static void deleteArray_vectorlEunsignedsPchargR(void *p) {
      delete [] ((vector<unsigned char>*)p);
   }
   static void destruct_vectorlEunsignedsPchargR(void *p) {
      typedef vector<unsigned char> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned char>

namespace ROOT {
   static TClass *vectorlEULong64_tgR_Dictionary();
   static void vectorlEULong64_tgR_TClassManip(TClass*);
   static void *new_vectorlEULong64_tgR(void *p = 0);
   static void *newArray_vectorlEULong64_tgR(Long_t size, void *p);
   static void delete_vectorlEULong64_tgR(void *p);
   static void deleteArray_vectorlEULong64_tgR(void *p);
   static void destruct_vectorlEULong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ULong64_t>*)
   {
      vector<ULong64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ULong64_t>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ULong64_t>", -2, "vector", 210,
                  typeid(vector<ULong64_t>), DefineBehavior(ptr, ptr),
                  &vectorlEULong64_tgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ULong64_t>) );
      instance.SetNew(&new_vectorlEULong64_tgR);
      instance.SetNewArray(&newArray_vectorlEULong64_tgR);
      instance.SetDelete(&delete_vectorlEULong64_tgR);
      instance.SetDeleteArray(&deleteArray_vectorlEULong64_tgR);
      instance.SetDestructor(&destruct_vectorlEULong64_tgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ULong64_t> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ULong64_t>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEULong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ULong64_t>*)0x0)->GetClass();
      vectorlEULong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEULong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEULong64_tgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ULong64_t> : new vector<ULong64_t>;
   }
   static void *newArray_vectorlEULong64_tgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ULong64_t>[nElements] : new vector<ULong64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEULong64_tgR(void *p) {
      delete ((vector<ULong64_t>*)p);
   }
   static void deleteArray_vectorlEULong64_tgR(void *p) {
      delete [] ((vector<ULong64_t>*)p);
   }
   static void destruct_vectorlEULong64_tgR(void *p) {
      typedef vector<ULong64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ULong64_t>

namespace ROOT {
   static TClass *vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR_Dictionary();
   static void vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p);
   static void destruct_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<xAOD::MissingETContainer_v1> >*)
   {
      vector<ElementLink<xAOD::MissingETContainer_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<xAOD::MissingETContainer_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<xAOD::MissingETContainer_v1> >", -2, "vector", 210,
                  typeid(vector<ElementLink<xAOD::MissingETContainer_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<xAOD::MissingETContainer_v1> >) );
      instance.SetNew(&new_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<xAOD::MissingETContainer_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<xAOD::MissingETContainer_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<xAOD::MissingETContainer_v1> >*)0x0)->GetClass();
      vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::MissingETContainer_v1> > : new vector<ElementLink<xAOD::MissingETContainer_v1> >;
   }
   static void *newArray_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::MissingETContainer_v1> >[nElements] : new vector<ElementLink<xAOD::MissingETContainer_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p) {
      delete ((vector<ElementLink<xAOD::MissingETContainer_v1> >*)p);
   }
   static void deleteArray_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p) {
      delete [] ((vector<ElementLink<xAOD::MissingETContainer_v1> >*)p);
   }
   static void destruct_vectorlEElementLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p) {
      typedef vector<ElementLink<xAOD::MissingETContainer_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<xAOD::MissingETContainer_v1> >

namespace ROOT {
   static TClass *vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR_Dictionary();
   static void vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p);
   static void destruct_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<xAOD::MissingETComponentMap_v1> >*)
   {
      vector<ElementLink<xAOD::MissingETComponentMap_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<xAOD::MissingETComponentMap_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<xAOD::MissingETComponentMap_v1> >", -2, "vector", 210,
                  typeid(vector<ElementLink<xAOD::MissingETComponentMap_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<xAOD::MissingETComponentMap_v1> >) );
      instance.SetNew(&new_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<xAOD::MissingETComponentMap_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<xAOD::MissingETComponentMap_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<xAOD::MissingETComponentMap_v1> >*)0x0)->GetClass();
      vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::MissingETComponentMap_v1> > : new vector<ElementLink<xAOD::MissingETComponentMap_v1> >;
   }
   static void *newArray_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::MissingETComponentMap_v1> >[nElements] : new vector<ElementLink<xAOD::MissingETComponentMap_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p) {
      delete ((vector<ElementLink<xAOD::MissingETComponentMap_v1> >*)p);
   }
   static void deleteArray_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p) {
      delete [] ((vector<ElementLink<xAOD::MissingETComponentMap_v1> >*)p);
   }
   static void destruct_vectorlEElementLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p) {
      typedef vector<ElementLink<xAOD::MissingETComponentMap_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<xAOD::MissingETComponentMap_v1> >

namespace ROOT {
   static TClass *vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR_Dictionary();
   static void vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(void *p);
   static void destruct_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<xAOD::MissingETAssociationMap_v1> >*)
   {
      vector<ElementLink<xAOD::MissingETAssociationMap_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<xAOD::MissingETAssociationMap_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<xAOD::MissingETAssociationMap_v1> >", -2, "vector", 210,
                  typeid(vector<ElementLink<xAOD::MissingETAssociationMap_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<xAOD::MissingETAssociationMap_v1> >) );
      instance.SetNew(&new_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<xAOD::MissingETAssociationMap_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<xAOD::MissingETAssociationMap_v1> >*)0x0)->GetClass();
      vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::MissingETAssociationMap_v1> > : new vector<ElementLink<xAOD::MissingETAssociationMap_v1> >;
   }
   static void *newArray_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<xAOD::MissingETAssociationMap_v1> >[nElements] : new vector<ElementLink<xAOD::MissingETAssociationMap_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(void *p) {
      delete ((vector<ElementLink<xAOD::MissingETAssociationMap_v1> >*)p);
   }
   static void deleteArray_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(void *p) {
      delete [] ((vector<ElementLink<xAOD::MissingETAssociationMap_v1> >*)p);
   }
   static void destruct_vectorlEElementLinklExAODcLcLMissingETAssociationMap_v1gRsPgR(void *p) {
      typedef vector<ElementLink<xAOD::MissingETAssociationMap_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<xAOD::MissingETAssociationMap_v1> >

namespace ROOT {
   static TClass *vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR_Dictionary();
   static void vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p);
   static void destruct_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<xAOD::MissingETContainer_v1> >*)
   {
      vector<DataLink<xAOD::MissingETContainer_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<xAOD::MissingETContainer_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<xAOD::MissingETContainer_v1> >", -2, "vector", 210,
                  typeid(vector<DataLink<xAOD::MissingETContainer_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<xAOD::MissingETContainer_v1> >) );
      instance.SetNew(&new_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<xAOD::MissingETContainer_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<xAOD::MissingETContainer_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<xAOD::MissingETContainer_v1> >*)0x0)->GetClass();
      vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::MissingETContainer_v1> > : new vector<DataLink<xAOD::MissingETContainer_v1> >;
   }
   static void *newArray_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::MissingETContainer_v1> >[nElements] : new vector<DataLink<xAOD::MissingETContainer_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p) {
      delete ((vector<DataLink<xAOD::MissingETContainer_v1> >*)p);
   }
   static void deleteArray_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p) {
      delete [] ((vector<DataLink<xAOD::MissingETContainer_v1> >*)p);
   }
   static void destruct_vectorlEDataLinklExAODcLcLMissingETContainer_v1gRsPgR(void *p) {
      typedef vector<DataLink<xAOD::MissingETContainer_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<xAOD::MissingETContainer_v1> >

namespace ROOT {
   static TClass *vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR_Dictionary();
   static void vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p);
   static void destruct_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<xAOD::MissingETComponentMap_v1> >*)
   {
      vector<DataLink<xAOD::MissingETComponentMap_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<xAOD::MissingETComponentMap_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<xAOD::MissingETComponentMap_v1> >", -2, "vector", 210,
                  typeid(vector<DataLink<xAOD::MissingETComponentMap_v1> >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<xAOD::MissingETComponentMap_v1> >) );
      instance.SetNew(&new_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<xAOD::MissingETComponentMap_v1> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<xAOD::MissingETComponentMap_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<xAOD::MissingETComponentMap_v1> >*)0x0)->GetClass();
      vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::MissingETComponentMap_v1> > : new vector<DataLink<xAOD::MissingETComponentMap_v1> >;
   }
   static void *newArray_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<xAOD::MissingETComponentMap_v1> >[nElements] : new vector<DataLink<xAOD::MissingETComponentMap_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p) {
      delete ((vector<DataLink<xAOD::MissingETComponentMap_v1> >*)p);
   }
   static void deleteArray_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p) {
      delete [] ((vector<DataLink<xAOD::MissingETComponentMap_v1> >*)p);
   }
   static void destruct_vectorlEDataLinklExAODcLcLMissingETComponentMap_v1gRsPgR(void *p) {
      typedef vector<DataLink<xAOD::MissingETComponentMap_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<xAOD::MissingETComponentMap_v1> >

namespace {
  void TriggerDictionaryInitialization_xAODMissingET_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMissingET/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMissingET/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETContainer.h")))  MissingET_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@F49162FE-6BC0-49BC-A7DA-A792136BD939)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETContainer.h")))  MissingETContainer_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@2F92EC94-8CD1-49F3-BCA4-3D78599D4D60)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETAuxContainer.h")))  MissingETAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETComponentMap.h")))  MissingETComponent_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@994D9D32-820F-47B1-A54B-37C15CD0FD1E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETComponentMap.h")))  MissingETComponentMap_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@96287268-4165-4288-8FC1-6227FD4AB95D)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETAuxComponentMap.h")))  MissingETAuxComponentMap_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETAssociationMap.h")))  MissingETAssociation_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@E2EAA116-F03E-430E-B4EF-216AAF7DEEE8)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETAssociationMap.h")))  MissingETAssociationMap_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@48EEF1CD-F937-445C-A09C-B978D152868E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMissingET/MissingETAuxAssociationMap.h")))  MissingETAuxAssociationMap_v1;}
template <typename STORABLE> class __attribute__((annotate("$clingAutoload$AthLinks/DataLink.h")))  DataLink;

namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
template <typename STORABLE> class __attribute__((annotate("$clingAutoload$AthLinks/ElementLink.h")))  ElementLink;

)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODMissingET"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODMissingETDict.h 662681 2015-04-23 18:04:34Z khoo $
#ifndef XAODMISSINGET_XAODMISSINGETDICT_H
#define XAODMISSINGET_XAODMISSINGETDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETComponentMap.h"
#include "xAODMissingET/MissingETAuxComponentMap.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETAuxAssociationMap.h"
#include "xAODMissingET/versions/MissingETBase.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODMISSINGET {
      // Container(s):
      DataVector< xAOD::MissingET_v1 > c1;
      xAOD::MissingETContainer_v1 c2;
      DataVector< xAOD::MissingETComponent_v1 > c3;
      xAOD::MissingETComponentMap_v1 c4;
      DataVector< xAOD::MissingETAssociation_v1 > c5;
      xAOD::MissingETAssociationMap_v1 c6;
      // Smart pointer(s):
      DataLink< xAOD::MissingETContainer_v1 > dl1;
      std::vector< DataLink< xAOD::MissingETContainer_v1 > > dl2;
      ElementLink< xAOD::MissingETContainer_v1 > el1;
      std::vector< ElementLink< xAOD::MissingETContainer_v1 > > el2;
      std::vector< std::vector< ElementLink< xAOD::MissingETContainer_v1 > > > el3;
      DataLink< xAOD::MissingETComponentMap_v1 > dl3;
      std::vector< DataLink< xAOD::MissingETComponentMap_v1 > > dl4;
      ElementLink< xAOD::MissingETComponentMap_v1 > el4;
      std::vector< ElementLink< xAOD::MissingETComponentMap_v1 > > el5;
      std::vector< std::vector< ElementLink< xAOD::MissingETComponentMap_v1 > > > el6;
      DataLink< xAOD::MissingETAssociationMap_v1 > dl5;
      std::vector< DataLink< xAOD::MissingETAssociationMap_v1 > > dl6;
      ElementLink< xAOD::MissingETAssociationMap_v1 > el7;
      std::vector< ElementLink< xAOD::MissingETAssociationMap_v1 > > el8;
      std::vector< std::vector< ElementLink< xAOD::MissingETAssociationMap_v1 > > > el9;

      // Smart pointers needed for the correct generation of the auxiliary
      // class dictionaries:
      ElementLink< xAOD::IParticleContainer > el10;
      std::vector< ElementLink< xAOD::IParticleContainer > > el11;
      std::vector< std::vector< ElementLink< xAOD::IParticleContainer > > > el12;

      std::vector< MissingETBase::Types::bitmask_t > vec1;
      std::vector< unsigned long long > vec2;
      std::vector< std::vector< size_t > > vec3;
      std::vector< std::vector< unsigned char > > vec4;
      std::vector< std::vector< unsigned long long > > vec5;
      std::vector< std::vector< std::vector< size_t > > > vec6;
      std::vector< std::vector< std::vector< unsigned long > > > vec7;
      std::vector< std::vector< std::vector< unsigned char > > > vec8;
      std::vector< std::vector<std::vector<unsigned long long> > > vec9;
   };
}

#endif // XAODMISSINGET_XAODMISSINGETDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<xAOD::MissingETComponentMap_v1>", payloadCode, "@",
"DataLink<xAOD::MissingETContainer_v1>", payloadCode, "@",
"DataVector<xAOD::MissingETAssociation_v1>", payloadCode, "@",
"DataVector<xAOD::MissingETComponent_v1>", payloadCode, "@",
"DataVector<xAOD::MissingET_v1>", payloadCode, "@",
"ElementLink<xAOD::MissingETAssociationMap_v1>", payloadCode, "@",
"ElementLink<xAOD::MissingETComponentMap_v1>", payloadCode, "@",
"ElementLink<xAOD::MissingETContainer_v1>", payloadCode, "@",
"vector<DataLink<xAOD::MissingETComponentMap_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::MissingETContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::MissingETAssociationMap_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::MissingETComponentMap_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::MissingETContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::MissingETComponentMap_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::MissingETContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ULong64_t> >", payloadCode, "@",
"vector<std::vector<std::vector<ULong64_t> > >", payloadCode, "@",
"vector<std::vector<std::vector<size_t> > >", payloadCode, "@",
"vector<std::vector<std::vector<unsigned char> > >", payloadCode, "@",
"vector<vector<ElementLink<xAOD::MissingETAssociationMap_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<xAOD::MissingETComponentMap_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<xAOD::MissingETContainer_v1> > >", payloadCode, "@",
"vector<vector<ULong64_t> >", payloadCode, "@",
"vector<vector<vector<ULong64_t> > >", payloadCode, "@",
"vector<vector<vector<unsigned char> > >", payloadCode, "@",
"vector<vector<vector<unsigned long> > >", payloadCode, "@",
"xAOD::MissingETAssociationMap_v1", payloadCode, "@",
"xAOD::MissingETAssociation_v1", payloadCode, "@",
"xAOD::MissingETAssociation_v1::ConstVec", payloadCode, "@",
"xAOD::MissingETAuxAssociationMap_v1", payloadCode, "@",
"xAOD::MissingETAuxComponentMap_v1", payloadCode, "@",
"xAOD::MissingETAuxContainer_v1", payloadCode, "@",
"xAOD::MissingETComponentMap_v1", payloadCode, "@",
"xAOD::MissingETComponent_v1", payloadCode, "@",
"xAOD::MissingETComponent_v1::Weight", payloadCode, "@",
"xAOD::MissingETContainer_v1", payloadCode, "@",
"xAOD::MissingET_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODMissingET_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODMissingET_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODMissingET_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODMissingET_Reflex() {
  TriggerDictionaryInitialization_xAODMissingET_Reflex_Impl();
}

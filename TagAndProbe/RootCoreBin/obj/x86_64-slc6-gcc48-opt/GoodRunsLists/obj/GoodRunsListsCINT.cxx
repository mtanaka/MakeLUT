#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIGoodRunsListsdIobjdIGoodRunsListsCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "GoodRunsLists/DQHelperFunctions.h"
#include "GoodRunsLists/RegularFormula.h"
#include "GoodRunsLists/TGoodRunsListReader.h"
#include "GoodRunsLists/TGoodRunsListWriter.h"

// Header files passed via #pragma extra_include

namespace Root {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *Root_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("Root", 0 /*version*/, "GoodRunsLists/TLumiBlockRange.h", 15,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &Root_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *Root_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace DQ {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *DQ_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("DQ", 0 /*version*/, "GoodRunsLists/DQHelperFunctions.h", 4,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &DQ_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *DQ_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static void *new_RootcLcLRegularFormula(void *p = 0);
   static void *newArray_RootcLcLRegularFormula(Long_t size, void *p);
   static void delete_RootcLcLRegularFormula(void *p);
   static void deleteArray_RootcLcLRegularFormula(void *p);
   static void destruct_RootcLcLRegularFormula(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Root::RegularFormula*)
   {
      ::Root::RegularFormula *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Root::RegularFormula >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Root::RegularFormula", ::Root::RegularFormula::Class_Version(), "GoodRunsLists/RegularFormula.h", 18,
                  typeid(::Root::RegularFormula), DefineBehavior(ptr, ptr),
                  &::Root::RegularFormula::Dictionary, isa_proxy, 4,
                  sizeof(::Root::RegularFormula) );
      instance.SetNew(&new_RootcLcLRegularFormula);
      instance.SetNewArray(&newArray_RootcLcLRegularFormula);
      instance.SetDelete(&delete_RootcLcLRegularFormula);
      instance.SetDeleteArray(&deleteArray_RootcLcLRegularFormula);
      instance.SetDestructor(&destruct_RootcLcLRegularFormula);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Root::RegularFormula*)
   {
      return GenerateInitInstanceLocal((::Root::RegularFormula*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Root::RegularFormula*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RootcLcLTGoodRunsListReader(void *p = 0);
   static void *newArray_RootcLcLTGoodRunsListReader(Long_t size, void *p);
   static void delete_RootcLcLTGoodRunsListReader(void *p);
   static void deleteArray_RootcLcLTGoodRunsListReader(void *p);
   static void destruct_RootcLcLTGoodRunsListReader(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Root::TGoodRunsListReader*)
   {
      ::Root::TGoodRunsListReader *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Root::TGoodRunsListReader >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Root::TGoodRunsListReader", ::Root::TGoodRunsListReader::Class_Version(), "GoodRunsLists/TGoodRunsListReader.h", 32,
                  typeid(::Root::TGoodRunsListReader), DefineBehavior(ptr, ptr),
                  &::Root::TGoodRunsListReader::Dictionary, isa_proxy, 4,
                  sizeof(::Root::TGoodRunsListReader) );
      instance.SetNew(&new_RootcLcLTGoodRunsListReader);
      instance.SetNewArray(&newArray_RootcLcLTGoodRunsListReader);
      instance.SetDelete(&delete_RootcLcLTGoodRunsListReader);
      instance.SetDeleteArray(&deleteArray_RootcLcLTGoodRunsListReader);
      instance.SetDestructor(&destruct_RootcLcLTGoodRunsListReader);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Root::TGoodRunsListReader*)
   {
      return GenerateInitInstanceLocal((::Root::TGoodRunsListReader*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Root::TGoodRunsListReader*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RootcLcLTGoodRunsListWriter(void *p = 0);
   static void *newArray_RootcLcLTGoodRunsListWriter(Long_t size, void *p);
   static void delete_RootcLcLTGoodRunsListWriter(void *p);
   static void deleteArray_RootcLcLTGoodRunsListWriter(void *p);
   static void destruct_RootcLcLTGoodRunsListWriter(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Root::TGoodRunsListWriter*)
   {
      ::Root::TGoodRunsListWriter *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Root::TGoodRunsListWriter >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Root::TGoodRunsListWriter", ::Root::TGoodRunsListWriter::Class_Version(), "GoodRunsLists/TGoodRunsListWriter.h", 35,
                  typeid(::Root::TGoodRunsListWriter), DefineBehavior(ptr, ptr),
                  &::Root::TGoodRunsListWriter::Dictionary, isa_proxy, 4,
                  sizeof(::Root::TGoodRunsListWriter) );
      instance.SetNew(&new_RootcLcLTGoodRunsListWriter);
      instance.SetNewArray(&newArray_RootcLcLTGoodRunsListWriter);
      instance.SetDelete(&delete_RootcLcLTGoodRunsListWriter);
      instance.SetDeleteArray(&deleteArray_RootcLcLTGoodRunsListWriter);
      instance.SetDestructor(&destruct_RootcLcLTGoodRunsListWriter);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Root::TGoodRunsListWriter*)
   {
      return GenerateInitInstanceLocal((::Root::TGoodRunsListWriter*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Root::TGoodRunsListWriter*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RootcLcLTLumiBlockRange(void *p = 0);
   static void *newArray_RootcLcLTLumiBlockRange(Long_t size, void *p);
   static void delete_RootcLcLTLumiBlockRange(void *p);
   static void deleteArray_RootcLcLTLumiBlockRange(void *p);
   static void destruct_RootcLcLTLumiBlockRange(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Root::TLumiBlockRange*)
   {
      ::Root::TLumiBlockRange *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Root::TLumiBlockRange >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Root::TLumiBlockRange", ::Root::TLumiBlockRange::Class_Version(), "GoodRunsLists/TLumiBlockRange.h", 17,
                  typeid(::Root::TLumiBlockRange), DefineBehavior(ptr, ptr),
                  &::Root::TLumiBlockRange::Dictionary, isa_proxy, 4,
                  sizeof(::Root::TLumiBlockRange) );
      instance.SetNew(&new_RootcLcLTLumiBlockRange);
      instance.SetNewArray(&newArray_RootcLcLTLumiBlockRange);
      instance.SetDelete(&delete_RootcLcLTLumiBlockRange);
      instance.SetDeleteArray(&deleteArray_RootcLcLTLumiBlockRange);
      instance.SetDestructor(&destruct_RootcLcLTLumiBlockRange);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Root::TLumiBlockRange*)
   {
      return GenerateInitInstanceLocal((::Root::TLumiBlockRange*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Root::TLumiBlockRange*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *__gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR_Dictionary();
   static void __gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR_TClassManip(TClass*);
   static void *new___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(void *p = 0);
   static void *newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(Long_t size, void *p);
   static void delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(void *p);
   static void deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(void *p);
   static void destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >*)
   {
      ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >));
      static ::ROOT::TGenericClassInfo 
         instance("__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >", "string", 708,
                  typeid(::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >), DefineBehavior(ptr, ptr),
                  &__gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >) );
      instance.SetNew(&new___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR);
      instance.SetNewArray(&newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR);
      instance.SetDelete(&delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR);
      instance.SetDeleteArray(&deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR);
      instance.SetDestructor(&destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR);

      ROOT::AddClassAlternate("__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >","vector<Root::TLumiBlockRange>::iterator");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >*)
   {
      return GenerateInitInstanceLocal((::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *__gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >*)0x0)->GetClass();
      __gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void __gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_RootcLcLTGoodRun(void *p = 0);
   static void *newArray_RootcLcLTGoodRun(Long_t size, void *p);
   static void delete_RootcLcLTGoodRun(void *p);
   static void deleteArray_RootcLcLTGoodRun(void *p);
   static void destruct_RootcLcLTGoodRun(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Root::TGoodRun*)
   {
      ::Root::TGoodRun *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Root::TGoodRun >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Root::TGoodRun", ::Root::TGoodRun::Class_Version(), "GoodRunsLists/TGoodRun.h", 18,
                  typeid(::Root::TGoodRun), DefineBehavior(ptr, ptr),
                  &::Root::TGoodRun::Dictionary, isa_proxy, 4,
                  sizeof(::Root::TGoodRun) );
      instance.SetNew(&new_RootcLcLTGoodRun);
      instance.SetNewArray(&newArray_RootcLcLTGoodRun);
      instance.SetDelete(&delete_RootcLcLTGoodRun);
      instance.SetDeleteArray(&deleteArray_RootcLcLTGoodRun);
      instance.SetDestructor(&destruct_RootcLcLTGoodRun);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Root::TGoodRun*)
   {
      return GenerateInitInstanceLocal((::Root::TGoodRun*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Root::TGoodRun*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *__gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR_Dictionary();
   static void __gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR_TClassManip(TClass*);
   static void *new___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(void *p = 0);
   static void *newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(Long_t size, void *p);
   static void delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(void *p);
   static void deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(void *p);
   static void destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >*)
   {
      ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >));
      static ::ROOT::TGenericClassInfo 
         instance("__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >", "string", 708,
                  typeid(::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >), DefineBehavior(ptr, ptr),
                  &__gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >) );
      instance.SetNew(&new___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR);
      instance.SetNewArray(&newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR);
      instance.SetDelete(&delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR);
      instance.SetDeleteArray(&deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR);
      instance.SetDestructor(&destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR);

      ROOT::AddClassAlternate("__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >","vector<Root::TGoodRun>::iterator");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >*)
   {
      return GenerateInitInstanceLocal((::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *__gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >*)0x0)->GetClass();
      __gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void __gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEintcORootcLcLTGoodRungR_Dictionary();
   static void pairlEintcORootcLcLTGoodRungR_TClassManip(TClass*);
   static void *new_pairlEintcORootcLcLTGoodRungR(void *p = 0);
   static void *newArray_pairlEintcORootcLcLTGoodRungR(Long_t size, void *p);
   static void delete_pairlEintcORootcLcLTGoodRungR(void *p);
   static void deleteArray_pairlEintcORootcLcLTGoodRungR(void *p);
   static void destruct_pairlEintcORootcLcLTGoodRungR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<int,Root::TGoodRun>*)
   {
      pair<int,Root::TGoodRun> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<int,Root::TGoodRun>));
      static ::ROOT::TGenericClassInfo 
         instance("pair<int,Root::TGoodRun>", "string", 96,
                  typeid(pair<int,Root::TGoodRun>), DefineBehavior(ptr, ptr),
                  &pairlEintcORootcLcLTGoodRungR_Dictionary, isa_proxy, 4,
                  sizeof(pair<int,Root::TGoodRun>) );
      instance.SetNew(&new_pairlEintcORootcLcLTGoodRungR);
      instance.SetNewArray(&newArray_pairlEintcORootcLcLTGoodRungR);
      instance.SetDelete(&delete_pairlEintcORootcLcLTGoodRungR);
      instance.SetDeleteArray(&deleteArray_pairlEintcORootcLcLTGoodRungR);
      instance.SetDestructor(&destruct_pairlEintcORootcLcLTGoodRungR);
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<int,Root::TGoodRun>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEintcORootcLcLTGoodRungR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<int,Root::TGoodRun>*)0x0)->GetClass();
      pairlEintcORootcLcLTGoodRungR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEintcORootcLcLTGoodRungR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *_Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR_Dictionary();
   static void _Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR_TClassManip(TClass*);
   static void *new__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(void *p = 0);
   static void *newArray__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(Long_t size, void *p);
   static void delete__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(void *p);
   static void deleteArray__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(void *p);
   static void destruct__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >*)
   {
      ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >));
      static ::ROOT::TGenericClassInfo 
         instance("_Rb_tree_iterator<pair<const int,Root::TGoodRun> >", "map", 157,
                  typeid(::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >), DefineBehavior(ptr, ptr),
                  &_Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >) );
      instance.SetNew(&new__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR);
      instance.SetNewArray(&newArray__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR);
      instance.SetDelete(&delete__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR);
      instance.SetDeleteArray(&deleteArray__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR);
      instance.SetDestructor(&destruct__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR);

      ROOT::AddClassAlternate("_Rb_tree_iterator<pair<const int,Root::TGoodRun> >","map<int,Root::TGoodRun>::iterator");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *_Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >*)0x0)->GetClass();
      _Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void _Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlETStringcOTStringgR_Dictionary();
   static void pairlETStringcOTStringgR_TClassManip(TClass*);
   static void *new_pairlETStringcOTStringgR(void *p = 0);
   static void *newArray_pairlETStringcOTStringgR(Long_t size, void *p);
   static void delete_pairlETStringcOTStringgR(void *p);
   static void deleteArray_pairlETStringcOTStringgR(void *p);
   static void destruct_pairlETStringcOTStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<TString,TString>*)
   {
      pair<TString,TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<TString,TString>));
      static ::ROOT::TGenericClassInfo 
         instance("pair<TString,TString>", "string", 96,
                  typeid(pair<TString,TString>), DefineBehavior(ptr, ptr),
                  &pairlETStringcOTStringgR_Dictionary, isa_proxy, 4,
                  sizeof(pair<TString,TString>) );
      instance.SetNew(&new_pairlETStringcOTStringgR);
      instance.SetNewArray(&newArray_pairlETStringcOTStringgR);
      instance.SetDelete(&delete_pairlETStringcOTStringgR);
      instance.SetDeleteArray(&deleteArray_pairlETStringcOTStringgR);
      instance.SetDestructor(&destruct_pairlETStringcOTStringgR);
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<TString,TString>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlETStringcOTStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<TString,TString>*)0x0)->GetClass();
      pairlETStringcOTStringgR_TClassManip(theClass);
   return theClass;
   }

   static void pairlETStringcOTStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_RootcLcLTGoodRunsList(void *p = 0);
   static void *newArray_RootcLcLTGoodRunsList(Long_t size, void *p);
   static void delete_RootcLcLTGoodRunsList(void *p);
   static void deleteArray_RootcLcLTGoodRunsList(void *p);
   static void destruct_RootcLcLTGoodRunsList(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Root::TGoodRunsList*)
   {
      ::Root::TGoodRunsList *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Root::TGoodRunsList >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Root::TGoodRunsList", ::Root::TGoodRunsList::Class_Version(), "GoodRunsLists/TGoodRunsList.h", 21,
                  typeid(::Root::TGoodRunsList), DefineBehavior(ptr, ptr),
                  &::Root::TGoodRunsList::Dictionary, isa_proxy, 4,
                  sizeof(::Root::TGoodRunsList) );
      instance.SetNew(&new_RootcLcLTGoodRunsList);
      instance.SetNewArray(&newArray_RootcLcLTGoodRunsList);
      instance.SetDelete(&delete_RootcLcLTGoodRunsList);
      instance.SetDeleteArray(&deleteArray_RootcLcLTGoodRunsList);
      instance.SetDestructor(&destruct_RootcLcLTGoodRunsList);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Root::TGoodRunsList*)
   {
      return GenerateInitInstanceLocal((::Root::TGoodRunsList*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Root::TGoodRunsList*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *__gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR_Dictionary();
   static void __gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR_TClassManip(TClass*);
   static void *new___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(void *p = 0);
   static void *newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(Long_t size, void *p);
   static void delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(void *p);
   static void deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(void *p);
   static void destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >*)
   {
      ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >));
      static ::ROOT::TGenericClassInfo 
         instance("__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >", "string", 708,
                  typeid(::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >), DefineBehavior(ptr, ptr),
                  &__gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >) );
      instance.SetNew(&new___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR);
      instance.SetNewArray(&newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR);
      instance.SetDelete(&delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR);
      instance.SetDeleteArray(&deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR);
      instance.SetDestructor(&destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR);

      ROOT::AddClassAlternate("__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >","vector<Root::TGoodRunsList>::iterator");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >*)
   {
      return GenerateInitInstanceLocal((::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *__gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >*)0x0)->GetClass();
      __gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void __gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_RootcLcLTGRLCollection(void *p = 0);
   static void *newArray_RootcLcLTGRLCollection(Long_t size, void *p);
   static void delete_RootcLcLTGRLCollection(void *p);
   static void deleteArray_RootcLcLTGRLCollection(void *p);
   static void destruct_RootcLcLTGRLCollection(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Root::TGRLCollection*)
   {
      ::Root::TGRLCollection *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Root::TGRLCollection >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Root::TGRLCollection", ::Root::TGRLCollection::Class_Version(), "GoodRunsLists/TGRLCollection.h", 20,
                  typeid(::Root::TGRLCollection), DefineBehavior(ptr, ptr),
                  &::Root::TGRLCollection::Dictionary, isa_proxy, 4,
                  sizeof(::Root::TGRLCollection) );
      instance.SetNew(&new_RootcLcLTGRLCollection);
      instance.SetNewArray(&newArray_RootcLcLTGRLCollection);
      instance.SetDelete(&delete_RootcLcLTGRLCollection);
      instance.SetDeleteArray(&deleteArray_RootcLcLTGRLCollection);
      instance.SetDestructor(&destruct_RootcLcLTGRLCollection);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Root::TGRLCollection*)
   {
      return GenerateInitInstanceLocal((::Root::TGRLCollection*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Root::TGRLCollection*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace Root {
//______________________________________________________________________________
atomic_TClass_ptr RegularFormula::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RegularFormula::Class_Name()
{
   return "Root::RegularFormula";
}

//______________________________________________________________________________
const char *RegularFormula::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::RegularFormula*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RegularFormula::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::RegularFormula*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RegularFormula::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::RegularFormula*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RegularFormula::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::RegularFormula*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Root
namespace Root {
//______________________________________________________________________________
atomic_TClass_ptr TGoodRunsListReader::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TGoodRunsListReader::Class_Name()
{
   return "Root::TGoodRunsListReader";
}

//______________________________________________________________________________
const char *TGoodRunsListReader::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsListReader*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TGoodRunsListReader::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsListReader*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TGoodRunsListReader::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsListReader*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TGoodRunsListReader::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsListReader*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Root
namespace Root {
//______________________________________________________________________________
atomic_TClass_ptr TGoodRunsListWriter::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TGoodRunsListWriter::Class_Name()
{
   return "Root::TGoodRunsListWriter";
}

//______________________________________________________________________________
const char *TGoodRunsListWriter::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsListWriter*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TGoodRunsListWriter::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsListWriter*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TGoodRunsListWriter::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsListWriter*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TGoodRunsListWriter::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsListWriter*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Root
namespace Root {
//______________________________________________________________________________
atomic_TClass_ptr TLumiBlockRange::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TLumiBlockRange::Class_Name()
{
   return "Root::TLumiBlockRange";
}

//______________________________________________________________________________
const char *TLumiBlockRange::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TLumiBlockRange*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TLumiBlockRange::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TLumiBlockRange*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TLumiBlockRange::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TLumiBlockRange*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TLumiBlockRange::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TLumiBlockRange*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Root
namespace Root {
//______________________________________________________________________________
atomic_TClass_ptr TGoodRun::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TGoodRun::Class_Name()
{
   return "Root::TGoodRun";
}

//______________________________________________________________________________
const char *TGoodRun::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRun*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TGoodRun::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRun*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TGoodRun::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRun*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TGoodRun::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRun*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Root
namespace Root {
//______________________________________________________________________________
atomic_TClass_ptr TGoodRunsList::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TGoodRunsList::Class_Name()
{
   return "Root::TGoodRunsList";
}

//______________________________________________________________________________
const char *TGoodRunsList::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsList*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TGoodRunsList::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsList*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TGoodRunsList::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsList*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TGoodRunsList::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGoodRunsList*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Root
namespace Root {
//______________________________________________________________________________
atomic_TClass_ptr TGRLCollection::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TGRLCollection::Class_Name()
{
   return "Root::TGRLCollection";
}

//______________________________________________________________________________
const char *TGRLCollection::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGRLCollection*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TGRLCollection::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Root::TGRLCollection*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TGRLCollection::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGRLCollection*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TGRLCollection::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Root::TGRLCollection*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Root
namespace Root {
//______________________________________________________________________________
void RegularFormula::Streamer(TBuffer &R__b)
{
   // Stream an object of class Root::RegularFormula.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Root::RegularFormula::Class(),this);
   } else {
      R__b.WriteClassBuffer(Root::RegularFormula::Class(),this);
   }
}

} // namespace Root
namespace ROOT {
   // Wrappers around operator new
   static void *new_RootcLcLRegularFormula(void *p) {
      return  p ? new(p) ::Root::RegularFormula : new ::Root::RegularFormula;
   }
   static void *newArray_RootcLcLRegularFormula(Long_t nElements, void *p) {
      return p ? new(p) ::Root::RegularFormula[nElements] : new ::Root::RegularFormula[nElements];
   }
   // Wrapper around operator delete
   static void delete_RootcLcLRegularFormula(void *p) {
      delete ((::Root::RegularFormula*)p);
   }
   static void deleteArray_RootcLcLRegularFormula(void *p) {
      delete [] ((::Root::RegularFormula*)p);
   }
   static void destruct_RootcLcLRegularFormula(void *p) {
      typedef ::Root::RegularFormula current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Root::RegularFormula

namespace Root {
//______________________________________________________________________________
void TGoodRunsListReader::Streamer(TBuffer &R__b)
{
   // Stream an object of class Root::TGoodRunsListReader.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Root::TGoodRunsListReader::Class(),this);
   } else {
      R__b.WriteClassBuffer(Root::TGoodRunsListReader::Class(),this);
   }
}

} // namespace Root
namespace ROOT {
   // Wrappers around operator new
   static void *new_RootcLcLTGoodRunsListReader(void *p) {
      return  p ? new(p) ::Root::TGoodRunsListReader : new ::Root::TGoodRunsListReader;
   }
   static void *newArray_RootcLcLTGoodRunsListReader(Long_t nElements, void *p) {
      return p ? new(p) ::Root::TGoodRunsListReader[nElements] : new ::Root::TGoodRunsListReader[nElements];
   }
   // Wrapper around operator delete
   static void delete_RootcLcLTGoodRunsListReader(void *p) {
      delete ((::Root::TGoodRunsListReader*)p);
   }
   static void deleteArray_RootcLcLTGoodRunsListReader(void *p) {
      delete [] ((::Root::TGoodRunsListReader*)p);
   }
   static void destruct_RootcLcLTGoodRunsListReader(void *p) {
      typedef ::Root::TGoodRunsListReader current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Root::TGoodRunsListReader

namespace Root {
//______________________________________________________________________________
void TGoodRunsListWriter::Streamer(TBuffer &R__b)
{
   // Stream an object of class Root::TGoodRunsListWriter.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Root::TGoodRunsListWriter::Class(),this);
   } else {
      R__b.WriteClassBuffer(Root::TGoodRunsListWriter::Class(),this);
   }
}

} // namespace Root
namespace ROOT {
   // Wrappers around operator new
   static void *new_RootcLcLTGoodRunsListWriter(void *p) {
      return  p ? new(p) ::Root::TGoodRunsListWriter : new ::Root::TGoodRunsListWriter;
   }
   static void *newArray_RootcLcLTGoodRunsListWriter(Long_t nElements, void *p) {
      return p ? new(p) ::Root::TGoodRunsListWriter[nElements] : new ::Root::TGoodRunsListWriter[nElements];
   }
   // Wrapper around operator delete
   static void delete_RootcLcLTGoodRunsListWriter(void *p) {
      delete ((::Root::TGoodRunsListWriter*)p);
   }
   static void deleteArray_RootcLcLTGoodRunsListWriter(void *p) {
      delete [] ((::Root::TGoodRunsListWriter*)p);
   }
   static void destruct_RootcLcLTGoodRunsListWriter(void *p) {
      typedef ::Root::TGoodRunsListWriter current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Root::TGoodRunsListWriter

namespace Root {
//______________________________________________________________________________
void TLumiBlockRange::Streamer(TBuffer &R__b)
{
   // Stream an object of class Root::TLumiBlockRange.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Root::TLumiBlockRange::Class(),this);
   } else {
      R__b.WriteClassBuffer(Root::TLumiBlockRange::Class(),this);
   }
}

} // namespace Root
namespace ROOT {
   // Wrappers around operator new
   static void *new_RootcLcLTLumiBlockRange(void *p) {
      return  p ? new(p) ::Root::TLumiBlockRange : new ::Root::TLumiBlockRange;
   }
   static void *newArray_RootcLcLTLumiBlockRange(Long_t nElements, void *p) {
      return p ? new(p) ::Root::TLumiBlockRange[nElements] : new ::Root::TLumiBlockRange[nElements];
   }
   // Wrapper around operator delete
   static void delete_RootcLcLTLumiBlockRange(void *p) {
      delete ((::Root::TLumiBlockRange*)p);
   }
   static void deleteArray_RootcLcLTLumiBlockRange(void *p) {
      delete [] ((::Root::TLumiBlockRange*)p);
   }
   static void destruct_RootcLcLTLumiBlockRange(void *p) {
      typedef ::Root::TLumiBlockRange current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Root::TLumiBlockRange

namespace ROOT {
   // Wrappers around operator new
   static void *new___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> > : new ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >;
   }
   static void *newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >[nElements] : new ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >[nElements];
   }
   // Wrapper around operator delete
   static void delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(void *p) {
      delete ((::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >*)p);
   }
   static void deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(void *p) {
      delete [] ((::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >*)p);
   }
   static void destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTLumiBlockRangemUcOvectorlERootcLcLTLumiBlockRangegRsPgR(void *p) {
      typedef ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >

namespace Root {
//______________________________________________________________________________
void TGoodRun::Streamer(TBuffer &R__b)
{
   // Stream an object of class Root::TGoodRun.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Root::TGoodRun::Class(),this);
   } else {
      R__b.WriteClassBuffer(Root::TGoodRun::Class(),this);
   }
}

} // namespace Root
namespace ROOT {
   // Wrappers around operator new
   static void *new_RootcLcLTGoodRun(void *p) {
      return  p ? new(p) ::Root::TGoodRun : new ::Root::TGoodRun;
   }
   static void *newArray_RootcLcLTGoodRun(Long_t nElements, void *p) {
      return p ? new(p) ::Root::TGoodRun[nElements] : new ::Root::TGoodRun[nElements];
   }
   // Wrapper around operator delete
   static void delete_RootcLcLTGoodRun(void *p) {
      delete ((::Root::TGoodRun*)p);
   }
   static void deleteArray_RootcLcLTGoodRun(void *p) {
      delete [] ((::Root::TGoodRun*)p);
   }
   static void destruct_RootcLcLTGoodRun(void *p) {
      typedef ::Root::TGoodRun current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Root::TGoodRun

namespace ROOT {
   // Wrappers around operator new
   static void *new___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> > : new ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >;
   }
   static void *newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >[nElements] : new ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >[nElements];
   }
   // Wrapper around operator delete
   static void delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(void *p) {
      delete ((::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >*)p);
   }
   static void deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(void *p) {
      delete [] ((::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >*)p);
   }
   static void destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunmUcOvectorlERootcLcLTGoodRungRsPgR(void *p) {
      typedef ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEintcORootcLcLTGoodRungR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<int,Root::TGoodRun> : new pair<int,Root::TGoodRun>;
   }
   static void *newArray_pairlEintcORootcLcLTGoodRungR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<int,Root::TGoodRun>[nElements] : new pair<int,Root::TGoodRun>[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEintcORootcLcLTGoodRungR(void *p) {
      delete ((pair<int,Root::TGoodRun>*)p);
   }
   static void deleteArray_pairlEintcORootcLcLTGoodRungR(void *p) {
      delete [] ((pair<int,Root::TGoodRun>*)p);
   }
   static void destruct_pairlEintcORootcLcLTGoodRungR(void *p) {
      typedef pair<int,Root::TGoodRun> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<int,Root::TGoodRun>

namespace ROOT {
   // Wrappers around operator new
   static void *new__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> > : new ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >;
   }
   static void *newArray__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >[nElements] : new ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >[nElements];
   }
   // Wrapper around operator delete
   static void delete__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(void *p) {
      delete ((::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >*)p);
   }
   static void deleteArray__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(void *p) {
      delete [] ((::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >*)p);
   }
   static void destruct__Rb_tree_iteratorlEpairlEconstsPintcORootcLcLTGoodRungRsPgR(void *p) {
      typedef ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::_Rb_tree_iterator<pair<const int,Root::TGoodRun> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlETStringcOTStringgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<TString,TString> : new pair<TString,TString>;
   }
   static void *newArray_pairlETStringcOTStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<TString,TString>[nElements] : new pair<TString,TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlETStringcOTStringgR(void *p) {
      delete ((pair<TString,TString>*)p);
   }
   static void deleteArray_pairlETStringcOTStringgR(void *p) {
      delete [] ((pair<TString,TString>*)p);
   }
   static void destruct_pairlETStringcOTStringgR(void *p) {
      typedef pair<TString,TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<TString,TString>

namespace Root {
//______________________________________________________________________________
void TGoodRunsList::Streamer(TBuffer &R__b)
{
   // Stream an object of class Root::TGoodRunsList.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Root::TGoodRunsList::Class(),this);
   } else {
      R__b.WriteClassBuffer(Root::TGoodRunsList::Class(),this);
   }
}

} // namespace Root
namespace ROOT {
   // Wrappers around operator new
   static void *new_RootcLcLTGoodRunsList(void *p) {
      return  p ? new(p) ::Root::TGoodRunsList : new ::Root::TGoodRunsList;
   }
   static void *newArray_RootcLcLTGoodRunsList(Long_t nElements, void *p) {
      return p ? new(p) ::Root::TGoodRunsList[nElements] : new ::Root::TGoodRunsList[nElements];
   }
   // Wrapper around operator delete
   static void delete_RootcLcLTGoodRunsList(void *p) {
      delete ((::Root::TGoodRunsList*)p);
   }
   static void deleteArray_RootcLcLTGoodRunsList(void *p) {
      delete [] ((::Root::TGoodRunsList*)p);
   }
   static void destruct_RootcLcLTGoodRunsList(void *p) {
      typedef ::Root::TGoodRunsList current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Root::TGoodRunsList

namespace ROOT {
   // Wrappers around operator new
   static void *new___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> > : new ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >;
   }
   static void *newArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >[nElements] : new ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >[nElements];
   }
   // Wrapper around operator delete
   static void delete___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(void *p) {
      delete ((::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >*)p);
   }
   static void deleteArray___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(void *p) {
      delete [] ((::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >*)p);
   }
   static void destruct___gnu_cxxcLcL__normal_iteratorlERootcLcLTGoodRunsListmUcOvectorlERootcLcLTGoodRunsListgRsPgR(void *p) {
      typedef ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >

namespace Root {
//______________________________________________________________________________
void TGRLCollection::Streamer(TBuffer &R__b)
{
   // Stream an object of class Root::TGRLCollection.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Root::TGRLCollection::Class(),this);
   } else {
      R__b.WriteClassBuffer(Root::TGRLCollection::Class(),this);
   }
}

} // namespace Root
namespace ROOT {
   // Wrappers around operator new
   static void *new_RootcLcLTGRLCollection(void *p) {
      return  p ? new(p) ::Root::TGRLCollection : new ::Root::TGRLCollection;
   }
   static void *newArray_RootcLcLTGRLCollection(Long_t nElements, void *p) {
      return p ? new(p) ::Root::TGRLCollection[nElements] : new ::Root::TGRLCollection[nElements];
   }
   // Wrapper around operator delete
   static void delete_RootcLcLTGRLCollection(void *p) {
      delete ((::Root::TGRLCollection*)p);
   }
   static void deleteArray_RootcLcLTGRLCollection(void *p) {
      delete [] ((::Root::TGRLCollection*)p);
   }
   static void destruct_RootcLcLTGRLCollection(void *p) {
      typedef ::Root::TGRLCollection current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Root::TGRLCollection

namespace ROOT {
   static TClass *vectorlERootcLcLTLumiBlockRangegR_Dictionary();
   static void vectorlERootcLcLTLumiBlockRangegR_TClassManip(TClass*);
   static void *new_vectorlERootcLcLTLumiBlockRangegR(void *p = 0);
   static void *newArray_vectorlERootcLcLTLumiBlockRangegR(Long_t size, void *p);
   static void delete_vectorlERootcLcLTLumiBlockRangegR(void *p);
   static void deleteArray_vectorlERootcLcLTLumiBlockRangegR(void *p);
   static void destruct_vectorlERootcLcLTLumiBlockRangegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Root::TLumiBlockRange>*)
   {
      vector<Root::TLumiBlockRange> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Root::TLumiBlockRange>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Root::TLumiBlockRange>", -2, "vector", 210,
                  typeid(vector<Root::TLumiBlockRange>), DefineBehavior(ptr, ptr),
                  &vectorlERootcLcLTLumiBlockRangegR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Root::TLumiBlockRange>) );
      instance.SetNew(&new_vectorlERootcLcLTLumiBlockRangegR);
      instance.SetNewArray(&newArray_vectorlERootcLcLTLumiBlockRangegR);
      instance.SetDelete(&delete_vectorlERootcLcLTLumiBlockRangegR);
      instance.SetDeleteArray(&deleteArray_vectorlERootcLcLTLumiBlockRangegR);
      instance.SetDestructor(&destruct_vectorlERootcLcLTLumiBlockRangegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Root::TLumiBlockRange> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<Root::TLumiBlockRange>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlERootcLcLTLumiBlockRangegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Root::TLumiBlockRange>*)0x0)->GetClass();
      vectorlERootcLcLTLumiBlockRangegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlERootcLcLTLumiBlockRangegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlERootcLcLTLumiBlockRangegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<Root::TLumiBlockRange> : new vector<Root::TLumiBlockRange>;
   }
   static void *newArray_vectorlERootcLcLTLumiBlockRangegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<Root::TLumiBlockRange>[nElements] : new vector<Root::TLumiBlockRange>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlERootcLcLTLumiBlockRangegR(void *p) {
      delete ((vector<Root::TLumiBlockRange>*)p);
   }
   static void deleteArray_vectorlERootcLcLTLumiBlockRangegR(void *p) {
      delete [] ((vector<Root::TLumiBlockRange>*)p);
   }
   static void destruct_vectorlERootcLcLTLumiBlockRangegR(void *p) {
      typedef vector<Root::TLumiBlockRange> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Root::TLumiBlockRange>

namespace ROOT {
   static TClass *vectorlERootcLcLTGoodRunsListgR_Dictionary();
   static void vectorlERootcLcLTGoodRunsListgR_TClassManip(TClass*);
   static void *new_vectorlERootcLcLTGoodRunsListgR(void *p = 0);
   static void *newArray_vectorlERootcLcLTGoodRunsListgR(Long_t size, void *p);
   static void delete_vectorlERootcLcLTGoodRunsListgR(void *p);
   static void deleteArray_vectorlERootcLcLTGoodRunsListgR(void *p);
   static void destruct_vectorlERootcLcLTGoodRunsListgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Root::TGoodRunsList>*)
   {
      vector<Root::TGoodRunsList> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Root::TGoodRunsList>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Root::TGoodRunsList>", -2, "vector", 210,
                  typeid(vector<Root::TGoodRunsList>), DefineBehavior(ptr, ptr),
                  &vectorlERootcLcLTGoodRunsListgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Root::TGoodRunsList>) );
      instance.SetNew(&new_vectorlERootcLcLTGoodRunsListgR);
      instance.SetNewArray(&newArray_vectorlERootcLcLTGoodRunsListgR);
      instance.SetDelete(&delete_vectorlERootcLcLTGoodRunsListgR);
      instance.SetDeleteArray(&deleteArray_vectorlERootcLcLTGoodRunsListgR);
      instance.SetDestructor(&destruct_vectorlERootcLcLTGoodRunsListgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Root::TGoodRunsList> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<Root::TGoodRunsList>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlERootcLcLTGoodRunsListgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Root::TGoodRunsList>*)0x0)->GetClass();
      vectorlERootcLcLTGoodRunsListgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlERootcLcLTGoodRunsListgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlERootcLcLTGoodRunsListgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<Root::TGoodRunsList> : new vector<Root::TGoodRunsList>;
   }
   static void *newArray_vectorlERootcLcLTGoodRunsListgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<Root::TGoodRunsList>[nElements] : new vector<Root::TGoodRunsList>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlERootcLcLTGoodRunsListgR(void *p) {
      delete ((vector<Root::TGoodRunsList>*)p);
   }
   static void deleteArray_vectorlERootcLcLTGoodRunsListgR(void *p) {
      delete [] ((vector<Root::TGoodRunsList>*)p);
   }
   static void destruct_vectorlERootcLcLTGoodRunsListgR(void *p) {
      typedef vector<Root::TGoodRunsList> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Root::TGoodRunsList>

namespace ROOT {
   static TClass *vectorlERootcLcLTGoodRungR_Dictionary();
   static void vectorlERootcLcLTGoodRungR_TClassManip(TClass*);
   static void *new_vectorlERootcLcLTGoodRungR(void *p = 0);
   static void *newArray_vectorlERootcLcLTGoodRungR(Long_t size, void *p);
   static void delete_vectorlERootcLcLTGoodRungR(void *p);
   static void deleteArray_vectorlERootcLcLTGoodRungR(void *p);
   static void destruct_vectorlERootcLcLTGoodRungR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Root::TGoodRun>*)
   {
      vector<Root::TGoodRun> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Root::TGoodRun>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Root::TGoodRun>", -2, "vector", 210,
                  typeid(vector<Root::TGoodRun>), DefineBehavior(ptr, ptr),
                  &vectorlERootcLcLTGoodRungR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Root::TGoodRun>) );
      instance.SetNew(&new_vectorlERootcLcLTGoodRungR);
      instance.SetNewArray(&newArray_vectorlERootcLcLTGoodRungR);
      instance.SetDelete(&delete_vectorlERootcLcLTGoodRungR);
      instance.SetDeleteArray(&deleteArray_vectorlERootcLcLTGoodRungR);
      instance.SetDestructor(&destruct_vectorlERootcLcLTGoodRungR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Root::TGoodRun> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<Root::TGoodRun>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlERootcLcLTGoodRungR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Root::TGoodRun>*)0x0)->GetClass();
      vectorlERootcLcLTGoodRungR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlERootcLcLTGoodRungR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlERootcLcLTGoodRungR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<Root::TGoodRun> : new vector<Root::TGoodRun>;
   }
   static void *newArray_vectorlERootcLcLTGoodRungR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<Root::TGoodRun>[nElements] : new vector<Root::TGoodRun>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlERootcLcLTGoodRungR(void *p) {
      delete ((vector<Root::TGoodRun>*)p);
   }
   static void deleteArray_vectorlERootcLcLTGoodRungR(void *p) {
      delete [] ((vector<Root::TGoodRun>*)p);
   }
   static void destruct_vectorlERootcLcLTGoodRungR(void *p) {
      typedef vector<Root::TGoodRun> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Root::TGoodRun>

namespace ROOT {
   static TClass *maplEintcORootcLcLTGoodRungR_Dictionary();
   static void maplEintcORootcLcLTGoodRungR_TClassManip(TClass*);
   static void *new_maplEintcORootcLcLTGoodRungR(void *p = 0);
   static void *newArray_maplEintcORootcLcLTGoodRungR(Long_t size, void *p);
   static void delete_maplEintcORootcLcLTGoodRungR(void *p);
   static void deleteArray_maplEintcORootcLcLTGoodRungR(void *p);
   static void destruct_maplEintcORootcLcLTGoodRungR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,Root::TGoodRun>*)
   {
      map<int,Root::TGoodRun> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,Root::TGoodRun>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,Root::TGoodRun>", -2, "map", 96,
                  typeid(map<int,Root::TGoodRun>), DefineBehavior(ptr, ptr),
                  &maplEintcORootcLcLTGoodRungR_Dictionary, isa_proxy, 4,
                  sizeof(map<int,Root::TGoodRun>) );
      instance.SetNew(&new_maplEintcORootcLcLTGoodRungR);
      instance.SetNewArray(&newArray_maplEintcORootcLcLTGoodRungR);
      instance.SetDelete(&delete_maplEintcORootcLcLTGoodRungR);
      instance.SetDeleteArray(&deleteArray_maplEintcORootcLcLTGoodRungR);
      instance.SetDestructor(&destruct_maplEintcORootcLcLTGoodRungR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,Root::TGoodRun> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<int,Root::TGoodRun>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcORootcLcLTGoodRungR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,Root::TGoodRun>*)0x0)->GetClass();
      maplEintcORootcLcLTGoodRungR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcORootcLcLTGoodRungR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcORootcLcLTGoodRungR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,Root::TGoodRun> : new map<int,Root::TGoodRun>;
   }
   static void *newArray_maplEintcORootcLcLTGoodRungR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<int,Root::TGoodRun>[nElements] : new map<int,Root::TGoodRun>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcORootcLcLTGoodRungR(void *p) {
      delete ((map<int,Root::TGoodRun>*)p);
   }
   static void deleteArray_maplEintcORootcLcLTGoodRungR(void *p) {
      delete [] ((map<int,Root::TGoodRun>*)p);
   }
   static void destruct_maplEintcORootcLcLTGoodRungR(void *p) {
      typedef map<int,Root::TGoodRun> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,Root::TGoodRun>

namespace ROOT {
   static TClass *maplETStringcOTStringgR_Dictionary();
   static void maplETStringcOTStringgR_TClassManip(TClass*);
   static void *new_maplETStringcOTStringgR(void *p = 0);
   static void *newArray_maplETStringcOTStringgR(Long_t size, void *p);
   static void delete_maplETStringcOTStringgR(void *p);
   static void deleteArray_maplETStringcOTStringgR(void *p);
   static void destruct_maplETStringcOTStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,TString>*)
   {
      map<TString,TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,TString>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,TString>", -2, "map", 96,
                  typeid(map<TString,TString>), DefineBehavior(ptr, ptr),
                  &maplETStringcOTStringgR_Dictionary, isa_proxy, 4,
                  sizeof(map<TString,TString>) );
      instance.SetNew(&new_maplETStringcOTStringgR);
      instance.SetNewArray(&newArray_maplETStringcOTStringgR);
      instance.SetDelete(&delete_maplETStringcOTStringgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOTStringgR);
      instance.SetDestructor(&destruct_maplETStringcOTStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const map<TString,TString>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOTStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,TString>*)0x0)->GetClass();
      maplETStringcOTStringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOTStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOTStringgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) map<TString,TString> : new map<TString,TString>;
   }
   static void *newArray_maplETStringcOTStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) map<TString,TString>[nElements] : new map<TString,TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOTStringgR(void *p) {
      delete ((map<TString,TString>*)p);
   }
   static void deleteArray_maplETStringcOTStringgR(void *p) {
      delete [] ((map<TString,TString>*)p);
   }
   static void destruct_maplETStringcOTStringgR(void *p) {
      typedef map<TString,TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,TString>

namespace {
  void TriggerDictionaryInitialization_GoodRunsListsCINT_Impl() {
    static const char* headers[] = {
"GoodRunsLists/DQHelperFunctions.h",
"GoodRunsLists/RegularFormula.h",
"GoodRunsLists/TGoodRunsListReader.h",
"GoodRunsLists/TGoodRunsListWriter.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/usr/include/libxml2",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace Root{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  RegularFormula;}
namespace Root{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  TGoodRunsListReader;}
namespace Root{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  TGoodRunsListWriter;}
namespace Root{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  TLumiBlockRange;}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace Root{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  TGoodRun;}
namespace std{template <class _T1, class _T2> struct __attribute__((annotate("$clingAutoload$string")))  pair;
}
namespace std{template <typename _Tp> struct __attribute__((annotate("$clingAutoload$string")))  less;
}
namespace std{template <typename _Tp> struct __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  _Rb_tree_iterator;
}
class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  TString;
namespace Root{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  TGoodRunsList;}
namespace Root{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/GoodRunsLists/Root/LinkDef.h")))  TGRLCollection;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "GoodRunsLists"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "GoodRunsLists/DQHelperFunctions.h"
#include "GoodRunsLists/RegularFormula.h"
#include "GoodRunsLists/TGoodRunsListReader.h"
#include "GoodRunsLists/TGoodRunsListWriter.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DQ::PassRunLB", payloadCode, "@",
"DQ::SetXMLFile", payloadCode, "@",
"Root::RegularFormula", payloadCode, "@",
"Root::TGRLCollection", payloadCode, "@",
"Root::TGoodRun", payloadCode, "@",
"Root::TGoodRunsList", payloadCode, "@",
"Root::TGoodRunsListReader", payloadCode, "@",
"Root::TGoodRunsListWriter", payloadCode, "@",
"Root::TLumiBlockRange", payloadCode, "@",
"_Rb_tree_iterator<pair<const int,Root::TGoodRun> >", payloadCode, "@",
"__gnu_cxx::__normal_iterator<Root::TGoodRun*,vector<Root::TGoodRun> >", payloadCode, "@",
"__gnu_cxx::__normal_iterator<Root::TGoodRunsList*,vector<Root::TGoodRunsList> >", payloadCode, "@",
"__gnu_cxx::__normal_iterator<Root::TLumiBlockRange*,vector<Root::TLumiBlockRange> >", payloadCode, "@",
"map<TString,TString>", payloadCode, "@",
"map<int,Root::TGoodRun>", payloadCode, "@",
"map<int,Root::TGoodRun>::iterator", payloadCode, "@",
"pair<TString,TString>", payloadCode, "@",
"pair<int,Root::TGoodRun>", payloadCode, "@",
"vector<Root::TGoodRun>", payloadCode, "@",
"vector<Root::TGoodRun>::iterator", payloadCode, "@",
"vector<Root::TGoodRunsList>", payloadCode, "@",
"vector<Root::TGoodRunsList>::iterator", payloadCode, "@",
"vector<Root::TLumiBlockRange>", payloadCode, "@",
"vector<Root::TLumiBlockRange>::iterator", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("GoodRunsListsCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_GoodRunsListsCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_GoodRunsListsCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_GoodRunsListsCINT() {
  TriggerDictionaryInitialization_GoodRunsListsCINT_Impl();
}

// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIJetResolutiondIobjdIJetResolution_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetResolution/JetResolution/JetResolutionDict.h"

// Header files passed via #pragma extra_include

namespace JER {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *JER_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("JER", 0 /*version*/, "JetResolution/JERDefs.h", 6,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &JER_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *JER_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *JERcLcLROOT6_OpenNamespaceWorkaround_Dictionary();
   static void JERcLcLROOT6_OpenNamespaceWorkaround_TClassManip(TClass*);
   static void *new_JERcLcLROOT6_OpenNamespaceWorkaround(void *p = 0);
   static void *newArray_JERcLcLROOT6_OpenNamespaceWorkaround(Long_t size, void *p);
   static void delete_JERcLcLROOT6_OpenNamespaceWorkaround(void *p);
   static void deleteArray_JERcLcLROOT6_OpenNamespaceWorkaround(void *p);
   static void destruct_JERcLcLROOT6_OpenNamespaceWorkaround(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JER::ROOT6_OpenNamespaceWorkaround*)
   {
      ::JER::ROOT6_OpenNamespaceWorkaround *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JER::ROOT6_OpenNamespaceWorkaround));
      static ::ROOT::TGenericClassInfo 
         instance("JER::ROOT6_OpenNamespaceWorkaround", "JetResolution/JERDefs.h", 9,
                  typeid(::JER::ROOT6_OpenNamespaceWorkaround), DefineBehavior(ptr, ptr),
                  &JERcLcLROOT6_OpenNamespaceWorkaround_Dictionary, isa_proxy, 0,
                  sizeof(::JER::ROOT6_OpenNamespaceWorkaround) );
      instance.SetNew(&new_JERcLcLROOT6_OpenNamespaceWorkaround);
      instance.SetNewArray(&newArray_JERcLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDelete(&delete_JERcLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDeleteArray(&deleteArray_JERcLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDestructor(&destruct_JERcLcLROOT6_OpenNamespaceWorkaround);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JER::ROOT6_OpenNamespaceWorkaround*)
   {
      return GenerateInitInstanceLocal((::JER::ROOT6_OpenNamespaceWorkaround*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JER::ROOT6_OpenNamespaceWorkaround*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JERcLcLROOT6_OpenNamespaceWorkaround_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JER::ROOT6_OpenNamespaceWorkaround*)0x0)->GetClass();
      JERcLcLROOT6_OpenNamespaceWorkaround_TClassManip(theClass);
   return theClass;
   }

   static void JERcLcLROOT6_OpenNamespaceWorkaround_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *IJERTool_Dictionary();
   static void IJERTool_TClassManip(TClass*);
   static void delete_IJERTool(void *p);
   static void deleteArray_IJERTool(void *p);
   static void destruct_IJERTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IJERTool*)
   {
      ::IJERTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IJERTool));
      static ::ROOT::TGenericClassInfo 
         instance("IJERTool", "JetResolution/IJERTool.h", 18,
                  typeid(::IJERTool), DefineBehavior(ptr, ptr),
                  &IJERTool_Dictionary, isa_proxy, 0,
                  sizeof(::IJERTool) );
      instance.SetDelete(&delete_IJERTool);
      instance.SetDeleteArray(&deleteArray_IJERTool);
      instance.SetDestructor(&destruct_IJERTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IJERTool*)
   {
      return GenerateInitInstanceLocal((::IJERTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::IJERTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IJERTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IJERTool*)0x0)->GetClass();
      IJERTool_TClassManip(theClass);
   return theClass;
   }

   static void IJERTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *JERTool_Dictionary();
   static void JERTool_TClassManip(TClass*);
   static void delete_JERTool(void *p);
   static void deleteArray_JERTool(void *p);
   static void destruct_JERTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JERTool*)
   {
      ::JERTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JERTool));
      static ::ROOT::TGenericClassInfo 
         instance("JERTool", "JetResolution/JERTool.h", 29,
                  typeid(::JERTool), DefineBehavior(ptr, ptr),
                  &JERTool_Dictionary, isa_proxy, 0,
                  sizeof(::JERTool) );
      instance.SetDelete(&delete_JERTool);
      instance.SetDeleteArray(&deleteArray_JERTool);
      instance.SetDestructor(&destruct_JERTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JERTool*)
   {
      return GenerateInitInstanceLocal((::JERTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JERTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JERTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JERTool*)0x0)->GetClass();
      JERTool_TClassManip(theClass);
   return theClass;
   }

   static void JERTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *IJERSmearingTool_Dictionary();
   static void IJERSmearingTool_TClassManip(TClass*);
   static void delete_IJERSmearingTool(void *p);
   static void deleteArray_IJERSmearingTool(void *p);
   static void destruct_IJERSmearingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IJERSmearingTool*)
   {
      ::IJERSmearingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IJERSmearingTool));
      static ::ROOT::TGenericClassInfo 
         instance("IJERSmearingTool", "JetResolution/IJERSmearingTool.h", 16,
                  typeid(::IJERSmearingTool), DefineBehavior(ptr, ptr),
                  &IJERSmearingTool_Dictionary, isa_proxy, 0,
                  sizeof(::IJERSmearingTool) );
      instance.SetDelete(&delete_IJERSmearingTool);
      instance.SetDeleteArray(&deleteArray_IJERSmearingTool);
      instance.SetDestructor(&destruct_IJERSmearingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IJERSmearingTool*)
   {
      return GenerateInitInstanceLocal((::IJERSmearingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::IJERSmearingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IJERSmearingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IJERSmearingTool*)0x0)->GetClass();
      IJERSmearingTool_TClassManip(theClass);
   return theClass;
   }

   static void IJERSmearingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *JERSmearingTool_Dictionary();
   static void JERSmearingTool_TClassManip(TClass*);
   static void delete_JERSmearingTool(void *p);
   static void deleteArray_JERSmearingTool(void *p);
   static void destruct_JERSmearingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JERSmearingTool*)
   {
      ::JERSmearingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JERSmearingTool));
      static ::ROOT::TGenericClassInfo 
         instance("JERSmearingTool", "JetResolution/JERSmearingTool.h", 29,
                  typeid(::JERSmearingTool), DefineBehavior(ptr, ptr),
                  &JERSmearingTool_Dictionary, isa_proxy, 0,
                  sizeof(::JERSmearingTool) );
      instance.SetDelete(&delete_JERSmearingTool);
      instance.SetDeleteArray(&deleteArray_JERSmearingTool);
      instance.SetDestructor(&destruct_JERSmearingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JERSmearingTool*)
   {
      return GenerateInitInstanceLocal((::JERSmearingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JERSmearingTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JERSmearingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JERSmearingTool*)0x0)->GetClass();
      JERSmearingTool_TClassManip(theClass);
   return theClass;
   }

   static void JERSmearingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_JERcLcLROOT6_OpenNamespaceWorkaround(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::JER::ROOT6_OpenNamespaceWorkaround : new ::JER::ROOT6_OpenNamespaceWorkaround;
   }
   static void *newArray_JERcLcLROOT6_OpenNamespaceWorkaround(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::JER::ROOT6_OpenNamespaceWorkaround[nElements] : new ::JER::ROOT6_OpenNamespaceWorkaround[nElements];
   }
   // Wrapper around operator delete
   static void delete_JERcLcLROOT6_OpenNamespaceWorkaround(void *p) {
      delete ((::JER::ROOT6_OpenNamespaceWorkaround*)p);
   }
   static void deleteArray_JERcLcLROOT6_OpenNamespaceWorkaround(void *p) {
      delete [] ((::JER::ROOT6_OpenNamespaceWorkaround*)p);
   }
   static void destruct_JERcLcLROOT6_OpenNamespaceWorkaround(void *p) {
      typedef ::JER::ROOT6_OpenNamespaceWorkaround current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JER::ROOT6_OpenNamespaceWorkaround

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IJERTool(void *p) {
      delete ((::IJERTool*)p);
   }
   static void deleteArray_IJERTool(void *p) {
      delete [] ((::IJERTool*)p);
   }
   static void destruct_IJERTool(void *p) {
      typedef ::IJERTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IJERTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JERTool(void *p) {
      delete ((::JERTool*)p);
   }
   static void deleteArray_JERTool(void *p) {
      delete [] ((::JERTool*)p);
   }
   static void destruct_JERTool(void *p) {
      typedef ::JERTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JERTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IJERSmearingTool(void *p) {
      delete ((::IJERSmearingTool*)p);
   }
   static void deleteArray_IJERSmearingTool(void *p) {
      delete [] ((::IJERSmearingTool*)p);
   }
   static void destruct_IJERSmearingTool(void *p) {
      typedef ::IJERSmearingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IJERSmearingTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JERSmearingTool(void *p) {
      delete ((::JERSmearingTool*)p);
   }
   static void deleteArray_JERSmearingTool(void *p) {
      delete [] ((::JERSmearingTool*)p);
   }
   static void destruct_JERSmearingTool(void *p) {
      typedef ::JERSmearingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JERSmearingTool

namespace {
  void TriggerDictionaryInitialization_JetResolution_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetResolution/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetResolution",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/JetResolution/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace JER{struct __attribute__((annotate("$clingAutoload$JetResolution/JERTool.h")))  ROOT6_OpenNamespaceWorkaround;}
class __attribute__((annotate("$clingAutoload$JetResolution/JERTool.h")))  IJERTool;
class __attribute__((annotate("$clingAutoload$JetResolution/JERTool.h")))  JERTool;
class __attribute__((annotate("$clingAutoload$JetResolution/JERSmearingTool.h")))  IJERSmearingTool;
class __attribute__((annotate("$clingAutoload$JetResolution/JERSmearingTool.h")))  JERSmearingTool;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "JetResolution"
#endif
#ifndef JETRES_STANDALONE
  #define JETRES_STANDALONE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef JETRESOLUTION_JETRESOLUTIONDICT_H
#define JETRESOLUTION_JETRESOLUTIONDICT_H

// Reflex dictionary generation
// Following instructions on the CP tools twiki:
// https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/DevelopingCPToolsForxAOD

// Special handling for Eigen vectorization
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#define EIGEN_DONT_VECTORIZE
#endif

// Tool headers
#include "JetResolution/JERTool.h"
#include "JetResolution/JERSmearingTool.h"
#include "JetResolution/JERDefs.h"

#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"IJERSmearingTool", payloadCode, "@",
"IJERTool", payloadCode, "@",
"JER::JetAlg", payloadCode, "@",
"JER::ROOT6_OpenNamespaceWorkaround", payloadCode, "@",
"JER::Uncert", payloadCode, "@",
"JERSmearingTool", payloadCode, "@",
"JERTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("JetResolution_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_JetResolution_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_JetResolution_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_JetResolution_Reflex() {
  TriggerDictionaryInitialization_JetResolution_Reflex_Impl();
}

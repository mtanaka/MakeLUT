#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIDiTauMassToolsdIobjdIDiTauMassToolsCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "DiTauMassTools/MissingMassCalculator.h"

// Header files passed via #pragma extra_include

namespace MMCCalibrationSet {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *MMCCalibrationSet_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("MMCCalibrationSet", 0 /*version*/, "DiTauMassTools/MissingMassCalculator.h", 30,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &MMCCalibrationSet_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *MMCCalibrationSet_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace WalkStrategy {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *WalkStrategy_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("WalkStrategy", 0 /*version*/, "DiTauMassTools/MissingMassCalculator.h", 47,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &WalkStrategy_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *WalkStrategy_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace MaxHistStrategy {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *MaxHistStrategy_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("MaxHistStrategy", 0 /*version*/, "DiTauMassTools/MissingMassCalculator.h", 52,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &MaxHistStrategy_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *MaxHistStrategy_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace HistInfo {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *HistInfo_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("HistInfo", 0 /*version*/, "DiTauMassTools/MissingMassCalculator.h", 58,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &HistInfo_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *HistInfo_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *MissingMassCalculator_Dictionary();
   static void MissingMassCalculator_TClassManip(TClass*);
   static void *new_MissingMassCalculator(void *p = 0);
   static void *newArray_MissingMassCalculator(Long_t size, void *p);
   static void delete_MissingMassCalculator(void *p);
   static void deleteArray_MissingMassCalculator(void *p);
   static void destruct_MissingMassCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MissingMassCalculator*)
   {
      ::MissingMassCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MissingMassCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("MissingMassCalculator", "DiTauMassTools/MissingMassCalculator.h", 68,
                  typeid(::MissingMassCalculator), DefineBehavior(ptr, ptr),
                  &MissingMassCalculator_Dictionary, isa_proxy, 0,
                  sizeof(::MissingMassCalculator) );
      instance.SetNew(&new_MissingMassCalculator);
      instance.SetNewArray(&newArray_MissingMassCalculator);
      instance.SetDelete(&delete_MissingMassCalculator);
      instance.SetDeleteArray(&deleteArray_MissingMassCalculator);
      instance.SetDestructor(&destruct_MissingMassCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MissingMassCalculator*)
   {
      return GenerateInitInstanceLocal((::MissingMassCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MissingMassCalculator*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MissingMassCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MissingMassCalculator*)0x0)->GetClass();
      MissingMassCalculator_TClassManip(theClass);
   return theClass;
   }

   static void MissingMassCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_MissingMassCalculator(void *p) {
      return  p ? new(p) ::MissingMassCalculator : new ::MissingMassCalculator;
   }
   static void *newArray_MissingMassCalculator(Long_t nElements, void *p) {
      return p ? new(p) ::MissingMassCalculator[nElements] : new ::MissingMassCalculator[nElements];
   }
   // Wrapper around operator delete
   static void delete_MissingMassCalculator(void *p) {
      delete ((::MissingMassCalculator*)p);
   }
   static void deleteArray_MissingMassCalculator(void *p) {
      delete [] ((::MissingMassCalculator*)p);
   }
   static void destruct_MissingMassCalculator(void *p) {
      typedef ::MissingMassCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MissingMassCalculator

namespace {
  void TriggerDictionaryInitialization_DiTauMassToolsCINT_Impl() {
    static const char* headers[] = {
"DiTauMassTools/MissingMassCalculator.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/DiTauMassTools/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/DiTauMassTools",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/DiTauMassTools/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/DiTauMassTools/Root/LinkDef.h")))  MissingMassCalculator;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "DiTauMassTools"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "DiTauMassTools/MissingMassCalculator.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"MissingMassCalculator", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("DiTauMassToolsCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_DiTauMassToolsCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_DiTauMassToolsCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_DiTauMassToolsCINT() {
  TriggerDictionaryInitialization_DiTauMassToolsCINT_Impl();
}

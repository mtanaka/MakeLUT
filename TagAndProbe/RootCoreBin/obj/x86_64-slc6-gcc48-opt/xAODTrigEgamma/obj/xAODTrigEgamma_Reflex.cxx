// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrigEgammadIobjdIxAODTrigEgamma_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigEgamma/xAODTrigEgamma/xAODTrigEgammaDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTrigPhoton_v1_Dictionary();
   static void xAODcLcLTrigPhoton_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigPhoton_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigPhoton_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigPhoton_v1(void *p);
   static void deleteArray_xAODcLcLTrigPhoton_v1(void *p);
   static void destruct_xAODcLcLTrigPhoton_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTrigPhoton_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TrigPhoton_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::TrigPhoton_v1* newObj = (xAOD::TrigPhoton_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
          m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigPhoton_v1*)
   {
      ::xAOD::TrigPhoton_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigPhoton_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigPhoton_v1", "xAODTrigEgamma/versions/TrigPhoton_v1.h", 30,
                  typeid(::xAOD::TrigPhoton_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigPhoton_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigPhoton_v1) );
      instance.SetNew(&new_xAODcLcLTrigPhoton_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigPhoton_v1);
      instance.SetDelete(&delete_xAODcLcLTrigPhoton_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigPhoton_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigPhoton_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TrigPhoton_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTrigPhoton_v1_0);
      rule->fCode        = "\n          m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigPhoton_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigPhoton_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigPhoton_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigPhoton_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigPhoton_v1*)0x0)->GetClass();
      xAODcLcLTrigPhoton_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigPhoton_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigPhoton_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigPhoton_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigPhoton_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigPhoton_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigPhoton_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigPhoton_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigPhoton_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigPhoton_v1>*)
   {
      ::DataVector<xAOD::TrigPhoton_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigPhoton_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigPhoton_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TrigPhoton_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigPhoton_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigPhoton_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigPhoton_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigPhoton_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigPhoton_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigPhoton_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigPhoton_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigPhoton_v1>","xAOD::TrigPhotonContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigPhoton_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigPhoton_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigPhoton_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigPhoton_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigPhoton_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigPhoton_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigPhoton_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","DA2CDAF5-B0E8-4502-89A3-E342DFA9C250");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigPhotonAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigPhotonAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigPhotonAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigPhotonAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigPhotonAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigPhotonAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigPhotonAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigPhotonAuxContainer_v1*)
   {
      ::xAOD::TrigPhotonAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigPhotonAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigPhotonAuxContainer_v1", "xAODTrigEgamma/versions/TrigPhotonAuxContainer_v1.h", 30,
                  typeid(::xAOD::TrigPhotonAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigPhotonAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigPhotonAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigPhotonAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigPhotonAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigPhotonAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigPhotonAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigPhotonAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigPhotonAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigPhotonAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigPhotonAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigPhotonAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigPhotonAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigPhotonAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigPhotonAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","36B8B9E3-8F5E-4356-9315-EF4FA6959624");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TrigPhoton_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TrigPhoton_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TrigPhoton_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TrigPhoton_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TrigPhoton_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TrigPhoton_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TrigPhoton_v1> >","DataLink<xAOD::TrigPhotonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TrigPhoton_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TrigPhoton_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrigPhoton_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrigPhoton_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrigPhoton_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrigPhoton_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrigPhoton_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrigPhoton_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrigPhoton_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrigPhoton_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrigPhoton_v1> >","ElementLink<xAOD::TrigPhotonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrigPhoton_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrigPhoton_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigPhoton_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigPhoton_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigElectron_v1_Dictionary();
   static void xAODcLcLTrigElectron_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigElectron_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigElectron_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigElectron_v1(void *p);
   static void deleteArray_xAODcLcLTrigElectron_v1(void *p);
   static void destruct_xAODcLcLTrigElectron_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLTrigElectron_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::TrigElectron_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::TrigElectron_v1* newObj = (xAOD::TrigElectron_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
          m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigElectron_v1*)
   {
      ::xAOD::TrigElectron_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigElectron_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigElectron_v1", "xAODTrigEgamma/versions/TrigElectron_v1.h", 31,
                  typeid(::xAOD::TrigElectron_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigElectron_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigElectron_v1) );
      instance.SetNew(&new_xAODcLcLTrigElectron_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigElectron_v1);
      instance.SetDelete(&delete_xAODcLcLTrigElectron_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigElectron_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigElectron_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::TrigElectron_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLTrigElectron_v1_0);
      rule->fCode        = "\n          m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigElectron_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigElectron_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigElectron_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigElectron_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigElectron_v1*)0x0)->GetClass();
      xAODcLcLTrigElectron_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigElectron_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigElectron_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigElectron_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigElectron_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigElectron_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigElectron_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigElectron_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigElectron_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigElectron_v1>*)
   {
      ::DataVector<xAOD::TrigElectron_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigElectron_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigElectron_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::TrigElectron_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigElectron_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigElectron_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigElectron_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigElectron_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigElectron_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigElectron_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigElectron_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigElectron_v1>","xAOD::TrigElectronContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigElectron_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigElectron_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigElectron_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigElectron_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigElectron_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigElectron_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigElectron_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","3492BB27-3ED8-45E3-9A5B-7266949CEDA9");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigElectronAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigElectronAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigElectronAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigElectronAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigElectronAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigElectronAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigElectronAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigElectronAuxContainer_v1*)
   {
      ::xAOD::TrigElectronAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigElectronAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigElectronAuxContainer_v1", "xAODTrigEgamma/versions/TrigElectronAuxContainer_v1.h", 31,
                  typeid(::xAOD::TrigElectronAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigElectronAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigElectronAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigElectronAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigElectronAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigElectronAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigElectronAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigElectronAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigElectronAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigElectronAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigElectronAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigElectronAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigElectronAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigElectronAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigElectronAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","173425E6-51BB-4015-B960-3F7F83F26B9E");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TrigElectron_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TrigElectron_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TrigElectron_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TrigElectron_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TrigElectron_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TrigElectron_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TrigElectron_v1> >","DataLink<xAOD::TrigElectronContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TrigElectron_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TrigElectron_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrigElectron_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrigElectron_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrigElectron_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrigElectron_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrigElectron_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrigElectron_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrigElectron_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrigElectron_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrigElectron_v1> >","ElementLink<xAOD::TrigElectronContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrigElectron_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrigElectron_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigElectron_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigElectron_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigPhoton_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigPhoton_v1 : new ::xAOD::TrigPhoton_v1;
   }
   static void *newArray_xAODcLcLTrigPhoton_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigPhoton_v1[nElements] : new ::xAOD::TrigPhoton_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigPhoton_v1(void *p) {
      delete ((::xAOD::TrigPhoton_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigPhoton_v1(void *p) {
      delete [] ((::xAOD::TrigPhoton_v1*)p);
   }
   static void destruct_xAODcLcLTrigPhoton_v1(void *p) {
      typedef ::xAOD::TrigPhoton_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigPhoton_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigPhoton_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigPhoton_v1> : new ::DataVector<xAOD::TrigPhoton_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigPhoton_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigPhoton_v1>[nElements] : new ::DataVector<xAOD::TrigPhoton_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigPhoton_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigPhoton_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigPhoton_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigPhoton_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigPhoton_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigPhoton_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigPhoton_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigPhotonAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigPhotonAuxContainer_v1 : new ::xAOD::TrigPhotonAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigPhotonAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigPhotonAuxContainer_v1[nElements] : new ::xAOD::TrigPhotonAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigPhotonAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigPhotonAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigPhotonAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigPhotonAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigPhotonAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigPhotonAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigPhotonAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TrigPhoton_v1> > : new ::DataLink<DataVector<xAOD::TrigPhoton_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TrigPhoton_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TrigPhoton_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TrigPhoton_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TrigPhoton_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TrigPhoton_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TrigPhoton_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrigPhoton_v1> > : new ::ElementLink<DataVector<xAOD::TrigPhoton_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrigPhoton_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrigPhoton_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrigPhoton_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrigPhoton_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrigPhoton_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrigPhoton_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigElectron_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigElectron_v1 : new ::xAOD::TrigElectron_v1;
   }
   static void *newArray_xAODcLcLTrigElectron_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigElectron_v1[nElements] : new ::xAOD::TrigElectron_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigElectron_v1(void *p) {
      delete ((::xAOD::TrigElectron_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigElectron_v1(void *p) {
      delete [] ((::xAOD::TrigElectron_v1*)p);
   }
   static void destruct_xAODcLcLTrigElectron_v1(void *p) {
      typedef ::xAOD::TrigElectron_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigElectron_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigElectron_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigElectron_v1> : new ::DataVector<xAOD::TrigElectron_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigElectron_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigElectron_v1>[nElements] : new ::DataVector<xAOD::TrigElectron_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigElectron_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigElectron_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigElectron_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigElectron_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigElectron_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigElectron_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigElectron_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigElectronAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigElectronAuxContainer_v1 : new ::xAOD::TrigElectronAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigElectronAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigElectronAuxContainer_v1[nElements] : new ::xAOD::TrigElectronAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigElectronAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigElectronAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigElectronAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigElectronAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigElectronAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigElectronAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigElectronAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TrigElectron_v1> > : new ::DataLink<DataVector<xAOD::TrigElectron_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TrigElectron_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TrigElectron_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TrigElectron_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TrigElectron_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TrigElectron_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TrigElectron_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrigElectron_v1> > : new ::ElementLink<DataVector<xAOD::TrigElectron_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrigElectron_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrigElectron_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrigElectron_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrigElectron_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrigElectron_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrigElectron_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > : new vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > : new vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > > : new vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigPhoton_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TrigElectron_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TrigElectron_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrigElectron_v1> > > : new vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigElectron_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TrigElectron_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTrigEgamma_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigEgamma/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigEgamma",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigEgamma/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigEgamma/TrigPhotonContainer.h")))  TrigPhoton_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigEgamma/TrigPhotonContainer.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@36B8B9E3-8F5E-4356-9315-EF4FA6959624)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigEgamma/versions/TrigPhotonAuxContainer_v1.h")))  TrigPhotonAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigEgamma/TrigElectronContainer.h")))  TrigElectron_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@173425E6-51BB-4015-B960-3F7F83F26B9E)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigEgamma/versions/TrigElectronAuxContainer_v1.h")))  TrigElectronAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigEgamma"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTrigEgammaDict.h 634417 2014-12-08 13:31:27Z krasznaa $
#ifndef XAODTRIGEGAMMA_XAODTRIGEGAMMADICT_H
#define XAODTRIGEGAMMA_XAODTRIGEGAMMADICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// STL include(s):
#include <vector>
	
// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

//Local include(s)
#include "xAODTrigEgamma/TrigPhotonContainer.h"
#include "xAODTrigEgamma/versions/TrigPhoton_v1.h"
#include "xAODTrigEgamma/versions/TrigPhotonContainer_v1.h"
#include "xAODTrigEgamma/versions/TrigPhotonAuxContainer_v1.h"

#include "xAODTrigEgamma/TrigElectronContainer.h"
#include "xAODTrigEgamma/versions/TrigElectron_v1.h"
#include "xAODTrigEgamma/versions/TrigElectronContainer_v1.h"
#include "xAODTrigEgamma/versions/TrigElectronAuxContainer_v1.h"

namespace{
   struct GCCXML_DUMMY_INSTANTIATION_XAODTRIGEGAMMA {
	
      xAOD::TrigElectronContainer_v1 electron_c1;
      DataLink< xAOD::TrigElectronContainer_v1 > electron_dl1;
      std::vector< DataLink< xAOD::TrigElectronContainer_v1 > > electron_dl2;
      ElementLink< xAOD::TrigElectronContainer_v1 > electron_el1;
      std::vector< ElementLink< xAOD::TrigElectronContainer_v1 > > electron_el2;
      std::vector< std::vector< ElementLink< xAOD::TrigElectronContainer_v1 > > > electron_el3;

      xAOD::TrigPhotonContainer_v1 photon_c1;
      DataLink< xAOD::TrigPhotonContainer_v1 > photon_dl1;
      std::vector< DataLink< xAOD::TrigPhotonContainer_v1 > > photon_dl2;
      ElementLink< xAOD::TrigPhotonContainer_v1 > photon_el1;
      std::vector< ElementLink< xAOD::TrigPhotonContainer_v1 > > photon_el2;
      std::vector< std::vector< ElementLink< xAOD::TrigPhotonContainer_v1 > > > photon_el3;

      // Declarations needed in order to generate the dictionaries of the
      // auxiliary containers correctly:
      ElementLink< xAOD::TrigEMClusterContainer > dummy1;
      std::vector< ElementLink< xAOD::TrigEMClusterContainer > > dummy2;
      ElementLink< xAOD::TrackParticleContainer > dummy3;
      std::vector< ElementLink< xAOD::TrackParticleContainer > > dummy4;

   };
} // private namespace

#endif // XAODTRIGEGAMMA_XAODTRIGEGAMMADICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::TrigElectron_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::TrigPhoton_v1> >", payloadCode, "@",
"DataLink<xAOD::TrigElectronContainer_v1>", payloadCode, "@",
"DataLink<xAOD::TrigPhotonContainer_v1>", payloadCode, "@",
"DataVector<xAOD::TrigElectron_v1>", payloadCode, "@",
"DataVector<xAOD::TrigPhoton_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrigElectron_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrigPhoton_v1> >", payloadCode, "@",
"ElementLink<xAOD::TrigElectronContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::TrigPhotonContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TrigElectron_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TrigPhoton_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::TrigElectronContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::TrigPhotonContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::TrigElectronContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::TrigPhotonContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrigElectronContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrigPhotonContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrigElectron_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrigPhoton_v1> > > >", payloadCode, "@",
"xAOD::TrigElectronAuxContainer_v1", payloadCode, "@",
"xAOD::TrigElectronContainer_v1", payloadCode, "@",
"xAOD::TrigElectron_v1", payloadCode, "@",
"xAOD::TrigPhotonAuxContainer_v1", payloadCode, "@",
"xAOD::TrigPhotonContainer_v1", payloadCode, "@",
"xAOD::TrigPhoton_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigEgamma_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigEgamma_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigEgamma_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigEgamma_Reflex() {
  TriggerDictionaryInitialization_xAODTrigEgamma_Reflex_Impl();
}

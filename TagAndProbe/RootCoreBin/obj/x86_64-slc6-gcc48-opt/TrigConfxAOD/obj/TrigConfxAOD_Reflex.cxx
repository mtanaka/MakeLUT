// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdITrigConfxAODdIobjdITrigConfxAOD_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigConfxAOD/TrigConfxAOD/TrigConfxAODDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *TrigConfcLcLxAODConfigTool_Dictionary();
   static void TrigConfcLcLxAODConfigTool_TClassManip(TClass*);
   static void *new_TrigConfcLcLxAODConfigTool(void *p = 0);
   static void *newArray_TrigConfcLcLxAODConfigTool(Long_t size, void *p);
   static void delete_TrigConfcLcLxAODConfigTool(void *p);
   static void deleteArray_TrigConfcLcLxAODConfigTool(void *p);
   static void destruct_TrigConfcLcLxAODConfigTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrigConf::xAODConfigTool*)
   {
      ::TrigConf::xAODConfigTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TrigConf::xAODConfigTool));
      static ::ROOT::TGenericClassInfo 
         instance("TrigConf::xAODConfigTool", "TrigConfxAOD/xAODConfigTool.h", 33,
                  typeid(::TrigConf::xAODConfigTool), DefineBehavior(ptr, ptr),
                  &TrigConfcLcLxAODConfigTool_Dictionary, isa_proxy, 0,
                  sizeof(::TrigConf::xAODConfigTool) );
      instance.SetNew(&new_TrigConfcLcLxAODConfigTool);
      instance.SetNewArray(&newArray_TrigConfcLcLxAODConfigTool);
      instance.SetDelete(&delete_TrigConfcLcLxAODConfigTool);
      instance.SetDeleteArray(&deleteArray_TrigConfcLcLxAODConfigTool);
      instance.SetDestructor(&destruct_TrigConfcLcLxAODConfigTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrigConf::xAODConfigTool*)
   {
      return GenerateInitInstanceLocal((::TrigConf::xAODConfigTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrigConf::xAODConfigTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigConfcLcLxAODConfigTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TrigConf::xAODConfigTool*)0x0)->GetClass();
      TrigConfcLcLxAODConfigTool_TClassManip(theClass);
   return theClass;
   }

   static void TrigConfcLcLxAODConfigTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrigConfcLcLxAODConfigTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::TrigConf::xAODConfigTool : new ::TrigConf::xAODConfigTool;
   }
   static void *newArray_TrigConfcLcLxAODConfigTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::TrigConf::xAODConfigTool[nElements] : new ::TrigConf::xAODConfigTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrigConfcLcLxAODConfigTool(void *p) {
      delete ((::TrigConf::xAODConfigTool*)p);
   }
   static void deleteArray_TrigConfcLcLxAODConfigTool(void *p) {
      delete [] ((::TrigConf::xAODConfigTool*)p);
   }
   static void destruct_TrigConfcLcLxAODConfigTool(void *p) {
      typedef ::TrigConf::xAODConfigTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrigConf::xAODConfigTool

namespace {
  void TriggerDictionaryInitialization_TrigConfxAOD_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigConfxAOD/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigConfxAOD/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace TrigConf{class __attribute__((annotate("$clingAutoload$TrigConfxAOD/xAODConfigTool.h")))  xAODConfigTool;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "TrigConfxAOD"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: TrigConfxAODDict.h 656622 2015-03-25 09:33:48Z krasznaa $
#ifndef TRIGCONFXAOD_TRIGCONFXAODDICT_H
#define TRIGCONFXAOD_TRIGCONFXAODDICT_H

// Local include(s):
#include "TrigConfxAOD/xAODConfigTool.h"

#endif // TRIGCONFXAOD_TRIGCONFXAODDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"TrigConf::xAODConfigTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TrigConfxAOD_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TrigConfxAOD_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TrigConfxAOD_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TrigConfxAOD_Reflex() {
  TriggerDictionaryInitialization_TrigConfxAOD_Reflex_Impl();
}

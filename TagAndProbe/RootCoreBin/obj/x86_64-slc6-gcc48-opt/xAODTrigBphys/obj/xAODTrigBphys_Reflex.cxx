// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrigBphysdIobjdIxAODTrigBphys_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigBphys/xAODTrigBphys/xAODTrigBphysDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTrigBphys_v1_Dictionary();
   static void xAODcLcLTrigBphys_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigBphys_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigBphys_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigBphys_v1(void *p);
   static void deleteArray_xAODcLcLTrigBphys_v1(void *p);
   static void destruct_xAODcLcLTrigBphys_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigBphys_v1*)
   {
      ::xAOD::TrigBphys_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigBphys_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigBphys_v1", "xAODTrigBphys/versions/TrigBphys_v1.h", 39,
                  typeid(::xAOD::TrigBphys_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigBphys_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigBphys_v1) );
      instance.SetNew(&new_xAODcLcLTrigBphys_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigBphys_v1);
      instance.SetDelete(&delete_xAODcLcLTrigBphys_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigBphys_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigBphys_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigBphys_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigBphys_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigBphys_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigBphys_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigBphys_v1*)0x0)->GetClass();
      xAODcLcLTrigBphys_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigBphys_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigBphys_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigBphys_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigBphys_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigBphys_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigBphys_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigBphys_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigBphys_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigBphys_v1>*)
   {
      ::DataVector<xAOD::TrigBphys_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigBphys_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigBphys_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigBphys_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigBphys_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigBphys_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigBphys_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigBphys_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigBphys_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigBphys_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigBphys_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigBphys_v1>","xAOD::TrigBphysContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigBphys_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigBphys_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigBphys_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigBphys_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigBphys_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigBphys_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigBphys_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","FD05137E-421B-40B5-AB7C-D119C5490784");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigBphysAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigBphysAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigBphysAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigBphysAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigBphysAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigBphysAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigBphysAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigBphysAuxContainer_v1*)
   {
      ::xAOD::TrigBphysAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigBphysAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigBphysAuxContainer_v1", "xAODTrigBphys/versions/TrigBphysAuxContainer_v1.h", 31,
                  typeid(::xAOD::TrigBphysAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigBphysAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigBphysAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigBphysAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigBphysAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigBphysAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigBphysAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigBphysAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigBphysAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigBphysAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigBphysAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigBphysAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigBphysAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigBphysAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigBphysAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C7246162-DB5D-4ACA-BF20-838A1B2BC4A3");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::TrigBphys_v1> >*)
   {
      ::DataLink<DataVector<xAOD::TrigBphys_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::TrigBphys_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::TrigBphys_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::TrigBphys_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::TrigBphys_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::TrigBphys_v1> >","DataLink<xAOD::TrigBphysContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::TrigBphys_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::TrigBphys_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrigBphys_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::TrigBphys_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrigBphys_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrigBphys_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrigBphys_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrigBphys_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrigBphys_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrigBphys_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrigBphys_v1> >","ElementLink<xAOD::TrigBphysContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrigBphys_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrigBphys_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigBphys_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigBphys_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigBphys_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigBphys_v1 : new ::xAOD::TrigBphys_v1;
   }
   static void *newArray_xAODcLcLTrigBphys_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigBphys_v1[nElements] : new ::xAOD::TrigBphys_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigBphys_v1(void *p) {
      delete ((::xAOD::TrigBphys_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigBphys_v1(void *p) {
      delete [] ((::xAOD::TrigBphys_v1*)p);
   }
   static void destruct_xAODcLcLTrigBphys_v1(void *p) {
      typedef ::xAOD::TrigBphys_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigBphys_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigBphys_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigBphys_v1> : new ::DataVector<xAOD::TrigBphys_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigBphys_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigBphys_v1>[nElements] : new ::DataVector<xAOD::TrigBphys_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigBphys_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigBphys_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigBphys_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigBphys_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigBphys_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigBphys_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigBphys_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigBphysAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigBphysAuxContainer_v1 : new ::xAOD::TrigBphysAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigBphysAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigBphysAuxContainer_v1[nElements] : new ::xAOD::TrigBphysAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigBphysAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigBphysAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigBphysAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigBphysAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigBphysAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigBphysAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigBphysAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::TrigBphys_v1> > : new ::DataLink<DataVector<xAOD::TrigBphys_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::TrigBphys_v1> >[nElements] : new ::DataLink<DataVector<xAOD::TrigBphys_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::TrigBphys_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::TrigBphys_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::TrigBphys_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::TrigBphys_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrigBphys_v1> > : new ::ElementLink<DataVector<xAOD::TrigBphys_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrigBphys_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrigBphys_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrigBphys_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrigBphys_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrigBphys_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrigBphys_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > : new vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::TrigBphys_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::TrigBphys_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrigBphys_v1> > > : new vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLTrigBphys_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::TrigBphys_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTrigBphys_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigBphys/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigBphys",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigBphys/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigBphys/TrigBphysContainer.h")))  TrigBphys_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@C7246162-DB5D-4ACA-BF20-838A1B2BC4A3)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigBphys/versions/TrigBphysAuxContainer_v1.h")))  TrigBphysAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigBphys"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTrigBphysDict.h 632753 2014-12-01 17:04:27Z jwalder $
#ifndef XAODTRIGBPHYS_AODTRIGBPHYSDICT_H
#define XAODTRIGBPHYS_AODTRIGBPHYSDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODTrigBphys/TrigBphysContainer.h"
#include "xAODTrigBphys/versions/TrigBphys_v1.h"
#include "xAODTrigBphys/versions/TrigBphysContainer_v1.h"
#include "xAODTrigBphys/versions/TrigBphysAuxContainer_v1.h"

namespace{
  struct GCCXML_DUMMY_INSTANTIATION_XAODTRIGBPHYS {

      xAOD::TrigBphysContainer_v1 bphys_l1;
      ElementLink< xAOD::TrigBphysContainer_v1 > bphys_l2;
      DataLink< xAOD::TrigBphysContainer_v1 > bphys_l3;
      std::vector< ElementLink< xAOD::TrigBphysContainer_v1 > > bphys_l4;
      std::vector< DataLink< xAOD::TrigBphysContainer_v1 > > bphys_l5;
      std::vector< std::vector< ElementLink< xAOD::TrigBphysContainer_v1 > > > bphys_l6;
      std::vector< std::vector< ElementLink< xAOD::TrackParticleContainer > > > bphys_17;

     // Instantiate the classes used by xAOD::xAODTrigBphys so that
     // Reflex would see them with their "correct type". Note that the
     // dictionary for these types comes from other places. This instantiation
     // is just needed for "Reflex related technical reasons"...
     ElementLink< xAOD::TrackParticleContainer > auxlink1;
     std::vector< ElementLink< xAOD::TrackParticleContainer > > auxlink2;
     std::vector< std::vector< ElementLink< xAOD::TrackParticleContainer > > > auxlink3;

     ElementLink< xAOD::IParticleContainer > auxlink4;
     std::vector< ElementLink< xAOD::IParticleContainer > > auxlink5;
     std::vector< std::vector< ElementLink< xAOD::IParticleContainer > > > auxlink6;
  };
}

#endif // XAODTRIGBPHYS_AODTRIGBPHYSDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::TrigBphys_v1> >", payloadCode, "@",
"DataLink<xAOD::TrigBphysContainer_v1>", payloadCode, "@",
"DataVector<xAOD::TrigBphys_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrigBphys_v1> >", payloadCode, "@",
"ElementLink<xAOD::TrigBphysContainer_v1>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::TrigBphys_v1> > >", payloadCode, "@",
"vector<DataLink<xAOD::TrigBphysContainer_v1> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::TrigBphysContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrigBphysContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrigBphys_v1> > > >", payloadCode, "@",
"xAOD::TrigBphysAuxContainer_v1", payloadCode, "@",
"xAOD::TrigBphysContainer_v1", payloadCode, "@",
"xAOD::TrigBphys_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigBphys_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigBphys_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigBphys_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigBphys_Reflex() {
  TriggerDictionaryInitialization_xAODTrigBphys_Reflex_Impl();
}

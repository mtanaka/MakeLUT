// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrigMuondIobjdIxAODTrigMuon_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/xAODTrigMuon/xAODTrigMuon/xAODTrigMuonDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLL2StandAloneMuon_v1_Dictionary();
   static void xAODcLcLL2StandAloneMuon_v1_TClassManip(TClass*);
   static void *new_xAODcLcLL2StandAloneMuon_v1(void *p = 0);
   static void *newArray_xAODcLcLL2StandAloneMuon_v1(Long_t size, void *p);
   static void delete_xAODcLcLL2StandAloneMuon_v1(void *p);
   static void deleteArray_xAODcLcLL2StandAloneMuon_v1(void *p);
   static void destruct_xAODcLcLL2StandAloneMuon_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLL2StandAloneMuon_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::L2StandAloneMuon_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::L2StandAloneMuon_v1* newObj = (xAOD::L2StandAloneMuon_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L2StandAloneMuon_v1*)
   {
      ::xAOD::L2StandAloneMuon_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L2StandAloneMuon_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L2StandAloneMuon_v1", "xAODTrigMuon/versions/L2StandAloneMuon_v1.h", 28,
                  typeid(::xAOD::L2StandAloneMuon_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLL2StandAloneMuon_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L2StandAloneMuon_v1) );
      instance.SetNew(&new_xAODcLcLL2StandAloneMuon_v1);
      instance.SetNewArray(&newArray_xAODcLcLL2StandAloneMuon_v1);
      instance.SetDelete(&delete_xAODcLcLL2StandAloneMuon_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL2StandAloneMuon_v1);
      instance.SetDestructor(&destruct_xAODcLcLL2StandAloneMuon_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::L2StandAloneMuon_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLL2StandAloneMuon_v1_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L2StandAloneMuon_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::L2StandAloneMuon_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L2StandAloneMuon_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL2StandAloneMuon_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L2StandAloneMuon_v1*)0x0)->GetClass();
      xAODcLcLL2StandAloneMuon_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL2StandAloneMuon_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLL2StandAloneMuon_v1gR_Dictionary();
   static void DataVectorlExAODcLcLL2StandAloneMuon_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::L2StandAloneMuon_v1>*)
   {
      ::DataVector<xAOD::L2StandAloneMuon_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::L2StandAloneMuon_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::L2StandAloneMuon_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::L2StandAloneMuon_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLL2StandAloneMuon_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::L2StandAloneMuon_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLL2StandAloneMuon_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLL2StandAloneMuon_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLL2StandAloneMuon_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLL2StandAloneMuon_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLL2StandAloneMuon_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::L2StandAloneMuon_v1>","xAOD::L2StandAloneMuonContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::L2StandAloneMuon_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::L2StandAloneMuon_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::L2StandAloneMuon_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLL2StandAloneMuon_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::L2StandAloneMuon_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLL2StandAloneMuon_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLL2StandAloneMuon_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","645BDBC3-44EE-486B-8783-96F93FA2550B");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL2StandAloneMuonAuxContainer_v1_Dictionary();
   static void xAODcLcLL2StandAloneMuonAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLL2StandAloneMuonAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLL2StandAloneMuonAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLL2StandAloneMuonAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLL2StandAloneMuonAuxContainer_v1(void *p);
   static void destruct_xAODcLcLL2StandAloneMuonAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L2StandAloneMuonAuxContainer_v1*)
   {
      ::xAOD::L2StandAloneMuonAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L2StandAloneMuonAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L2StandAloneMuonAuxContainer_v1", "xAODTrigMuon/versions/L2StandAloneMuonAuxContainer_v1.h", 27,
                  typeid(::xAOD::L2StandAloneMuonAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLL2StandAloneMuonAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L2StandAloneMuonAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLL2StandAloneMuonAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLL2StandAloneMuonAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLL2StandAloneMuonAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL2StandAloneMuonAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLL2StandAloneMuonAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L2StandAloneMuonAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::L2StandAloneMuonAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L2StandAloneMuonAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL2StandAloneMuonAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L2StandAloneMuonAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLL2StandAloneMuonAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL2StandAloneMuonAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","69F56941-D2B5-4C70-BFBD-605CB64A3DB8");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)
   {
      ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >","DataLink<xAOD::L2StandAloneMuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >","ElementLink<xAOD::L2StandAloneMuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL2StandAloneMuon_v2_Dictionary();
   static void xAODcLcLL2StandAloneMuon_v2_TClassManip(TClass*);
   static void *new_xAODcLcLL2StandAloneMuon_v2(void *p = 0);
   static void *newArray_xAODcLcLL2StandAloneMuon_v2(Long_t size, void *p);
   static void delete_xAODcLcLL2StandAloneMuon_v2(void *p);
   static void deleteArray_xAODcLcLL2StandAloneMuon_v2(void *p);
   static void destruct_xAODcLcLL2StandAloneMuon_v2(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLL2StandAloneMuon_v2_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::L2StandAloneMuon_v2");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::L2StandAloneMuon_v2* newObj = (xAOD::L2StandAloneMuon_v2*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L2StandAloneMuon_v2*)
   {
      ::xAOD::L2StandAloneMuon_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L2StandAloneMuon_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L2StandAloneMuon_v2", "xAODTrigMuon/versions/L2StandAloneMuon_v2.h", 28,
                  typeid(::xAOD::L2StandAloneMuon_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLL2StandAloneMuon_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L2StandAloneMuon_v2) );
      instance.SetNew(&new_xAODcLcLL2StandAloneMuon_v2);
      instance.SetNewArray(&newArray_xAODcLcLL2StandAloneMuon_v2);
      instance.SetDelete(&delete_xAODcLcLL2StandAloneMuon_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL2StandAloneMuon_v2);
      instance.SetDestructor(&destruct_xAODcLcLL2StandAloneMuon_v2);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::L2StandAloneMuon_v2";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLL2StandAloneMuon_v2_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L2StandAloneMuon_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::L2StandAloneMuon_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L2StandAloneMuon_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL2StandAloneMuon_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L2StandAloneMuon_v2*)0x0)->GetClass();
      xAODcLcLL2StandAloneMuon_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL2StandAloneMuon_v2_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLL2StandAloneMuon_v2gR_Dictionary();
   static void DataVectorlExAODcLcLL2StandAloneMuon_v2gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(void *p);
   static void destruct_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::L2StandAloneMuon_v2>*)
   {
      ::DataVector<xAOD::L2StandAloneMuon_v2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::L2StandAloneMuon_v2>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::L2StandAloneMuon_v2>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::L2StandAloneMuon_v2>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLL2StandAloneMuon_v2gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::L2StandAloneMuon_v2>) );
      instance.SetNew(&new_DataVectorlExAODcLcLL2StandAloneMuon_v2gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLL2StandAloneMuon_v2gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLL2StandAloneMuon_v2gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLL2StandAloneMuon_v2gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLL2StandAloneMuon_v2gR);

      ROOT::AddClassAlternate("DataVector<xAOD::L2StandAloneMuon_v2>","xAOD::L2StandAloneMuonContainer_v2");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::L2StandAloneMuon_v2>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::L2StandAloneMuon_v2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::L2StandAloneMuon_v2>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLL2StandAloneMuon_v2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::L2StandAloneMuon_v2>*)0x0)->GetClass();
      DataVectorlExAODcLcLL2StandAloneMuon_v2gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLL2StandAloneMuon_v2gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","6B02C486-CB3B-4762-89CA-60B210FC5AAF");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL2StandAloneMuonAuxContainer_v2_Dictionary();
   static void xAODcLcLL2StandAloneMuonAuxContainer_v2_TClassManip(TClass*);
   static void *new_xAODcLcLL2StandAloneMuonAuxContainer_v2(void *p = 0);
   static void *newArray_xAODcLcLL2StandAloneMuonAuxContainer_v2(Long_t size, void *p);
   static void delete_xAODcLcLL2StandAloneMuonAuxContainer_v2(void *p);
   static void deleteArray_xAODcLcLL2StandAloneMuonAuxContainer_v2(void *p);
   static void destruct_xAODcLcLL2StandAloneMuonAuxContainer_v2(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L2StandAloneMuonAuxContainer_v2*)
   {
      ::xAOD::L2StandAloneMuonAuxContainer_v2 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L2StandAloneMuonAuxContainer_v2));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L2StandAloneMuonAuxContainer_v2", "xAODTrigMuon/versions/L2StandAloneMuonAuxContainer_v2.h", 27,
                  typeid(::xAOD::L2StandAloneMuonAuxContainer_v2), DefineBehavior(ptr, ptr),
                  &xAODcLcLL2StandAloneMuonAuxContainer_v2_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L2StandAloneMuonAuxContainer_v2) );
      instance.SetNew(&new_xAODcLcLL2StandAloneMuonAuxContainer_v2);
      instance.SetNewArray(&newArray_xAODcLcLL2StandAloneMuonAuxContainer_v2);
      instance.SetDelete(&delete_xAODcLcLL2StandAloneMuonAuxContainer_v2);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL2StandAloneMuonAuxContainer_v2);
      instance.SetDestructor(&destruct_xAODcLcLL2StandAloneMuonAuxContainer_v2);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L2StandAloneMuonAuxContainer_v2*)
   {
      return GenerateInitInstanceLocal((::xAOD::L2StandAloneMuonAuxContainer_v2*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L2StandAloneMuonAuxContainer_v2*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL2StandAloneMuonAuxContainer_v2_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L2StandAloneMuonAuxContainer_v2*)0x0)->GetClass();
      xAODcLcLL2StandAloneMuonAuxContainer_v2_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL2StandAloneMuonAuxContainer_v2_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","39CA616A-16BF-4CCE-8885-3D68CE7BC3B0");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)
   {
      ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >","DataLink<xAOD::L2StandAloneMuonContainer_v2>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)
   {
      ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >","ElementLink<xAOD::L2StandAloneMuonContainer_v2>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL2IsoMuon_v1_Dictionary();
   static void xAODcLcLL2IsoMuon_v1_TClassManip(TClass*);
   static void *new_xAODcLcLL2IsoMuon_v1(void *p = 0);
   static void *newArray_xAODcLcLL2IsoMuon_v1(Long_t size, void *p);
   static void delete_xAODcLcLL2IsoMuon_v1(void *p);
   static void deleteArray_xAODcLcLL2IsoMuon_v1(void *p);
   static void destruct_xAODcLcLL2IsoMuon_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLL2IsoMuon_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::L2IsoMuon_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::L2IsoMuon_v1* newObj = (xAOD::L2IsoMuon_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L2IsoMuon_v1*)
   {
      ::xAOD::L2IsoMuon_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L2IsoMuon_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L2IsoMuon_v1", "xAODTrigMuon/versions/L2IsoMuon_v1.h", 25,
                  typeid(::xAOD::L2IsoMuon_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLL2IsoMuon_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L2IsoMuon_v1) );
      instance.SetNew(&new_xAODcLcLL2IsoMuon_v1);
      instance.SetNewArray(&newArray_xAODcLcLL2IsoMuon_v1);
      instance.SetDelete(&delete_xAODcLcLL2IsoMuon_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL2IsoMuon_v1);
      instance.SetDestructor(&destruct_xAODcLcLL2IsoMuon_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::L2IsoMuon_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLL2IsoMuon_v1_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L2IsoMuon_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::L2IsoMuon_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L2IsoMuon_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL2IsoMuon_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L2IsoMuon_v1*)0x0)->GetClass();
      xAODcLcLL2IsoMuon_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL2IsoMuon_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLL2IsoMuon_v1gR_Dictionary();
   static void DataVectorlExAODcLcLL2IsoMuon_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLL2IsoMuon_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLL2IsoMuon_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLL2IsoMuon_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLL2IsoMuon_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLL2IsoMuon_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::L2IsoMuon_v1>*)
   {
      ::DataVector<xAOD::L2IsoMuon_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::L2IsoMuon_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::L2IsoMuon_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::L2IsoMuon_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLL2IsoMuon_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::L2IsoMuon_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLL2IsoMuon_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLL2IsoMuon_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLL2IsoMuon_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLL2IsoMuon_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLL2IsoMuon_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::L2IsoMuon_v1>","xAOD::L2IsoMuonContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::L2IsoMuon_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::L2IsoMuon_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::L2IsoMuon_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLL2IsoMuon_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::L2IsoMuon_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLL2IsoMuon_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLL2IsoMuon_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","D3AD2358-29B9-48D1-B181-89D4DA39B5CC");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL2IsoMuonAuxContainer_v1_Dictionary();
   static void xAODcLcLL2IsoMuonAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLL2IsoMuonAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLL2IsoMuonAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLL2IsoMuonAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLL2IsoMuonAuxContainer_v1(void *p);
   static void destruct_xAODcLcLL2IsoMuonAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L2IsoMuonAuxContainer_v1*)
   {
      ::xAOD::L2IsoMuonAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L2IsoMuonAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L2IsoMuonAuxContainer_v1", "xAODTrigMuon/versions/L2IsoMuonAuxContainer_v1.h", 25,
                  typeid(::xAOD::L2IsoMuonAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLL2IsoMuonAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L2IsoMuonAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLL2IsoMuonAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLL2IsoMuonAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLL2IsoMuonAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL2IsoMuonAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLL2IsoMuonAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L2IsoMuonAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::L2IsoMuonAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L2IsoMuonAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL2IsoMuonAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L2IsoMuonAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLL2IsoMuonAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL2IsoMuonAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","09310901-41D9-49FB-A131-5AC73A9EA455");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::L2IsoMuon_v1> >*)
   {
      ::DataLink<DataVector<xAOD::L2IsoMuon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::L2IsoMuon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::L2IsoMuon_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::L2IsoMuon_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::L2IsoMuon_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::L2IsoMuon_v1> >","DataLink<xAOD::L2IsoMuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::L2IsoMuon_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::L2IsoMuon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::L2IsoMuon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::L2IsoMuon_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::L2IsoMuon_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::L2IsoMuon_v1> >","ElementLink<xAOD::L2IsoMuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL2CombinedMuon_v1_Dictionary();
   static void xAODcLcLL2CombinedMuon_v1_TClassManip(TClass*);
   static void *new_xAODcLcLL2CombinedMuon_v1(void *p = 0);
   static void *newArray_xAODcLcLL2CombinedMuon_v1(Long_t size, void *p);
   static void delete_xAODcLcLL2CombinedMuon_v1(void *p);
   static void deleteArray_xAODcLcLL2CombinedMuon_v1(void *p);
   static void destruct_xAODcLcLL2CombinedMuon_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLL2CombinedMuon_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      static TClassRef cls("xAOD::L2CombinedMuon_v1");
      static Long_t offset_m_p4Cached = cls->GetDataMemberOffset("m_p4Cached");
      bool& m_p4Cached = *(bool*)(target+offset_m_p4Cached);
      xAOD::L2CombinedMuon_v1* newObj = (xAOD::L2CombinedMuon_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
       m_p4Cached = false;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L2CombinedMuon_v1*)
   {
      ::xAOD::L2CombinedMuon_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L2CombinedMuon_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L2CombinedMuon_v1", "xAODTrigMuon/versions/L2CombinedMuon_v1.h", 33,
                  typeid(::xAOD::L2CombinedMuon_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLL2CombinedMuon_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L2CombinedMuon_v1) );
      instance.SetNew(&new_xAODcLcLL2CombinedMuon_v1);
      instance.SetNewArray(&newArray_xAODcLcLL2CombinedMuon_v1);
      instance.SetDelete(&delete_xAODcLcLL2CombinedMuon_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL2CombinedMuon_v1);
      instance.SetDestructor(&destruct_xAODcLcLL2CombinedMuon_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::L2CombinedMuon_v1";
      rule->fTarget      = "m_p4Cached";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLL2CombinedMuon_v1_0);
      rule->fCode        = "\n       m_p4Cached = false;\n    ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L2CombinedMuon_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::L2CombinedMuon_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L2CombinedMuon_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL2CombinedMuon_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L2CombinedMuon_v1*)0x0)->GetClass();
      xAODcLcLL2CombinedMuon_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL2CombinedMuon_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLL2CombinedMuon_v1gR_Dictionary();
   static void DataVectorlExAODcLcLL2CombinedMuon_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLL2CombinedMuon_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLL2CombinedMuon_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLL2CombinedMuon_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLL2CombinedMuon_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLL2CombinedMuon_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::L2CombinedMuon_v1>*)
   {
      ::DataVector<xAOD::L2CombinedMuon_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::L2CombinedMuon_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::L2CombinedMuon_v1>", "AthContainers/DataVector.h", 719,
                  typeid(::DataVector<xAOD::L2CombinedMuon_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLL2CombinedMuon_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::L2CombinedMuon_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLL2CombinedMuon_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLL2CombinedMuon_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLL2CombinedMuon_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLL2CombinedMuon_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLL2CombinedMuon_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::L2CombinedMuon_v1>","xAOD::L2CombinedMuonContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::L2CombinedMuon_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::L2CombinedMuon_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::L2CombinedMuon_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLL2CombinedMuon_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::L2CombinedMuon_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLL2CombinedMuon_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLL2CombinedMuon_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","91ECD1FA-3B9F-4F80-BBD1-698DCC7C9477");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLL2CombinedMuonAuxContainer_v1_Dictionary();
   static void xAODcLcLL2CombinedMuonAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLL2CombinedMuonAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLL2CombinedMuonAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLL2CombinedMuonAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLL2CombinedMuonAuxContainer_v1(void *p);
   static void destruct_xAODcLcLL2CombinedMuonAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::L2CombinedMuonAuxContainer_v1*)
   {
      ::xAOD::L2CombinedMuonAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::L2CombinedMuonAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::L2CombinedMuonAuxContainer_v1", "xAODTrigMuon/versions/L2CombinedMuonAuxContainer_v1.h", 29,
                  typeid(::xAOD::L2CombinedMuonAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLL2CombinedMuonAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::L2CombinedMuonAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLL2CombinedMuonAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLL2CombinedMuonAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLL2CombinedMuonAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLL2CombinedMuonAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLL2CombinedMuonAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::L2CombinedMuonAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::L2CombinedMuonAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::L2CombinedMuonAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLL2CombinedMuonAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::L2CombinedMuonAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLL2CombinedMuonAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLL2CombinedMuonAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","0A7A1CB4-A5D3-4447-BE1A-F4DA14D5891B");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_Dictionary();
   static void DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p = 0);
   static void *newArray_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p);
   static void deleteArray_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p);
   static void destruct_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >*)
   {
      ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("DataLink<DataVector<xAOD::L2CombinedMuon_v1> >", "AthLinks/DataLink.h", 37,
                  typeid(::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >), DefineBehavior(ptr, ptr),
                  &DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >) );
      instance.SetNew(&new_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);
      instance.SetDelete(&delete_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);

      ROOT::AddClassAlternate("DataLink<DataVector<xAOD::L2CombinedMuon_v1> >","DataLink<xAOD::L2CombinedMuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >*)
   {
      return GenerateInitInstanceLocal((::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >*)0x0)->GetClass();
      DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >","ElementLink<xAOD::L2CombinedMuonContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL2StandAloneMuon_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2StandAloneMuon_v1 : new ::xAOD::L2StandAloneMuon_v1;
   }
   static void *newArray_xAODcLcLL2StandAloneMuon_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2StandAloneMuon_v1[nElements] : new ::xAOD::L2StandAloneMuon_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL2StandAloneMuon_v1(void *p) {
      delete ((::xAOD::L2StandAloneMuon_v1*)p);
   }
   static void deleteArray_xAODcLcLL2StandAloneMuon_v1(void *p) {
      delete [] ((::xAOD::L2StandAloneMuon_v1*)p);
   }
   static void destruct_xAODcLcLL2StandAloneMuon_v1(void *p) {
      typedef ::xAOD::L2StandAloneMuon_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L2StandAloneMuon_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::L2StandAloneMuon_v1> : new ::DataVector<xAOD::L2StandAloneMuon_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::L2StandAloneMuon_v1>[nElements] : new ::DataVector<xAOD::L2StandAloneMuon_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(void *p) {
      delete ((::DataVector<xAOD::L2StandAloneMuon_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::L2StandAloneMuon_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLL2StandAloneMuon_v1gR(void *p) {
      typedef ::DataVector<xAOD::L2StandAloneMuon_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::L2StandAloneMuon_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL2StandAloneMuonAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2StandAloneMuonAuxContainer_v1 : new ::xAOD::L2StandAloneMuonAuxContainer_v1;
   }
   static void *newArray_xAODcLcLL2StandAloneMuonAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2StandAloneMuonAuxContainer_v1[nElements] : new ::xAOD::L2StandAloneMuonAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL2StandAloneMuonAuxContainer_v1(void *p) {
      delete ((::xAOD::L2StandAloneMuonAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLL2StandAloneMuonAuxContainer_v1(void *p) {
      delete [] ((::xAOD::L2StandAloneMuonAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLL2StandAloneMuonAuxContainer_v1(void *p) {
      typedef ::xAOD::L2StandAloneMuonAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L2StandAloneMuonAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > : new ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >[nElements] : new ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > : new ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL2StandAloneMuon_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2StandAloneMuon_v2 : new ::xAOD::L2StandAloneMuon_v2;
   }
   static void *newArray_xAODcLcLL2StandAloneMuon_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2StandAloneMuon_v2[nElements] : new ::xAOD::L2StandAloneMuon_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL2StandAloneMuon_v2(void *p) {
      delete ((::xAOD::L2StandAloneMuon_v2*)p);
   }
   static void deleteArray_xAODcLcLL2StandAloneMuon_v2(void *p) {
      delete [] ((::xAOD::L2StandAloneMuon_v2*)p);
   }
   static void destruct_xAODcLcLL2StandAloneMuon_v2(void *p) {
      typedef ::xAOD::L2StandAloneMuon_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L2StandAloneMuon_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::L2StandAloneMuon_v2> : new ::DataVector<xAOD::L2StandAloneMuon_v2>;
   }
   static void *newArray_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::L2StandAloneMuon_v2>[nElements] : new ::DataVector<xAOD::L2StandAloneMuon_v2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(void *p) {
      delete ((::DataVector<xAOD::L2StandAloneMuon_v2>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(void *p) {
      delete [] ((::DataVector<xAOD::L2StandAloneMuon_v2>*)p);
   }
   static void destruct_DataVectorlExAODcLcLL2StandAloneMuon_v2gR(void *p) {
      typedef ::DataVector<xAOD::L2StandAloneMuon_v2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::L2StandAloneMuon_v2>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL2StandAloneMuonAuxContainer_v2(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2StandAloneMuonAuxContainer_v2 : new ::xAOD::L2StandAloneMuonAuxContainer_v2;
   }
   static void *newArray_xAODcLcLL2StandAloneMuonAuxContainer_v2(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2StandAloneMuonAuxContainer_v2[nElements] : new ::xAOD::L2StandAloneMuonAuxContainer_v2[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL2StandAloneMuonAuxContainer_v2(void *p) {
      delete ((::xAOD::L2StandAloneMuonAuxContainer_v2*)p);
   }
   static void deleteArray_xAODcLcLL2StandAloneMuonAuxContainer_v2(void *p) {
      delete [] ((::xAOD::L2StandAloneMuonAuxContainer_v2*)p);
   }
   static void destruct_xAODcLcLL2StandAloneMuonAuxContainer_v2(void *p) {
      typedef ::xAOD::L2StandAloneMuonAuxContainer_v2 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L2StandAloneMuonAuxContainer_v2

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > : new ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >[nElements] : new ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > : new ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >[nElements] : new ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL2IsoMuon_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2IsoMuon_v1 : new ::xAOD::L2IsoMuon_v1;
   }
   static void *newArray_xAODcLcLL2IsoMuon_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2IsoMuon_v1[nElements] : new ::xAOD::L2IsoMuon_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL2IsoMuon_v1(void *p) {
      delete ((::xAOD::L2IsoMuon_v1*)p);
   }
   static void deleteArray_xAODcLcLL2IsoMuon_v1(void *p) {
      delete [] ((::xAOD::L2IsoMuon_v1*)p);
   }
   static void destruct_xAODcLcLL2IsoMuon_v1(void *p) {
      typedef ::xAOD::L2IsoMuon_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L2IsoMuon_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLL2IsoMuon_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::L2IsoMuon_v1> : new ::DataVector<xAOD::L2IsoMuon_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLL2IsoMuon_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::L2IsoMuon_v1>[nElements] : new ::DataVector<xAOD::L2IsoMuon_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLL2IsoMuon_v1gR(void *p) {
      delete ((::DataVector<xAOD::L2IsoMuon_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLL2IsoMuon_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::L2IsoMuon_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLL2IsoMuon_v1gR(void *p) {
      typedef ::DataVector<xAOD::L2IsoMuon_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::L2IsoMuon_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL2IsoMuonAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2IsoMuonAuxContainer_v1 : new ::xAOD::L2IsoMuonAuxContainer_v1;
   }
   static void *newArray_xAODcLcLL2IsoMuonAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2IsoMuonAuxContainer_v1[nElements] : new ::xAOD::L2IsoMuonAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL2IsoMuonAuxContainer_v1(void *p) {
      delete ((::xAOD::L2IsoMuonAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLL2IsoMuonAuxContainer_v1(void *p) {
      delete [] ((::xAOD::L2IsoMuonAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLL2IsoMuonAuxContainer_v1(void *p) {
      typedef ::xAOD::L2IsoMuonAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L2IsoMuonAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::L2IsoMuon_v1> > : new ::DataLink<DataVector<xAOD::L2IsoMuon_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::L2IsoMuon_v1> >[nElements] : new ::DataLink<DataVector<xAOD::L2IsoMuon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::L2IsoMuon_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::L2IsoMuon_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::L2IsoMuon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::L2IsoMuon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> > : new ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::L2IsoMuon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL2CombinedMuon_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2CombinedMuon_v1 : new ::xAOD::L2CombinedMuon_v1;
   }
   static void *newArray_xAODcLcLL2CombinedMuon_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2CombinedMuon_v1[nElements] : new ::xAOD::L2CombinedMuon_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL2CombinedMuon_v1(void *p) {
      delete ((::xAOD::L2CombinedMuon_v1*)p);
   }
   static void deleteArray_xAODcLcLL2CombinedMuon_v1(void *p) {
      delete [] ((::xAOD::L2CombinedMuon_v1*)p);
   }
   static void destruct_xAODcLcLL2CombinedMuon_v1(void *p) {
      typedef ::xAOD::L2CombinedMuon_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L2CombinedMuon_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLL2CombinedMuon_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::L2CombinedMuon_v1> : new ::DataVector<xAOD::L2CombinedMuon_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLL2CombinedMuon_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::L2CombinedMuon_v1>[nElements] : new ::DataVector<xAOD::L2CombinedMuon_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLL2CombinedMuon_v1gR(void *p) {
      delete ((::DataVector<xAOD::L2CombinedMuon_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLL2CombinedMuon_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::L2CombinedMuon_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLL2CombinedMuon_v1gR(void *p) {
      typedef ::DataVector<xAOD::L2CombinedMuon_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::L2CombinedMuon_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLL2CombinedMuonAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2CombinedMuonAuxContainer_v1 : new ::xAOD::L2CombinedMuonAuxContainer_v1;
   }
   static void *newArray_xAODcLcLL2CombinedMuonAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::L2CombinedMuonAuxContainer_v1[nElements] : new ::xAOD::L2CombinedMuonAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLL2CombinedMuonAuxContainer_v1(void *p) {
      delete ((::xAOD::L2CombinedMuonAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLL2CombinedMuonAuxContainer_v1(void *p) {
      delete [] ((::xAOD::L2CombinedMuonAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLL2CombinedMuonAuxContainer_v1(void *p) {
      typedef ::xAOD::L2CombinedMuonAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::L2CombinedMuonAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> > : new ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >;
   }
   static void *newArray_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >[nElements] : new ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p) {
      delete ((::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >*)p);
   }
   static void deleteArray_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p) {
      delete [] ((::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >*)p);
   }
   static void destruct_DataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p) {
      typedef ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataLink<DataVector<xAOD::L2CombinedMuon_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > : new ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > > : new vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)
   {
      vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > : new vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >[nElements] : new vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > : new vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > : new vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > : new vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)
   {
      vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > > : new vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >[nElements] : new vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v2gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > > : new vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2StandAloneMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > > : new vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2IsoMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >

namespace ROOT {
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_Dictionary();
   static void vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)
   {
      vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >", -2, "vector", 210,
                  typeid(vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >) );
      instance.SetNew(&new_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)0x0)->GetClass();
      vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > > : new vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >;
   }
   static void *newArray_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >[nElements] : new vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p) {
      delete ((vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)p);
   }
   static void deleteArray_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >*)p);
   }
   static void destruct_vectorlEDataLinklEDataVectorlExAODcLcLL2CombinedMuon_v1gRsPgRsPgR(void *p) {
      typedef vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTrigMuon_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/xAODTrigMuon/Root",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/xAODTrigMuon",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/xAODTrigMuon/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMuon/versions/L2StandAloneMuon_v1.h")))  L2StandAloneMuon_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMuon/L2StandAloneMuonContainer.h")))  IParticle;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@69F56941-D2B5-4C70-BFBD-605CB64A3DB8)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMuon/versions/L2StandAloneMuonAuxContainer_v1.h")))  L2StandAloneMuonAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMuon/L2StandAloneMuonContainer.h")))  L2StandAloneMuon_v2;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@39CA616A-16BF-4CCE-8885-3D68CE7BC3B0)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMuon/versions/L2StandAloneMuonAuxContainer_v2.h")))  L2StandAloneMuonAuxContainer_v2;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMuon/L2IsoMuonContainer.h")))  L2IsoMuon_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@09310901-41D9-49FB-A131-5AC73A9EA455)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMuon/versions/L2IsoMuonAuxContainer_v1.h")))  L2IsoMuonAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMuon/L2CombinedMuonContainer.h")))  L2CombinedMuon_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@0A7A1CB4-A5D3-4447-BE1A-F4DA14D5891B)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMuon/versions/L2CombinedMuonAuxContainer_v1.h")))  L2CombinedMuonAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigMuon"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODTrigMuonDict.h 698126 2015-10-02 12:59:53Z mishitsu $
#ifndef XAODTRIGMUON_XAODTRIGMUONDICT_H
#define XAODTRIGMUON_XAODTRIGMUONDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigMuon/L2IsoMuonContainer.h"
#include "xAODTrigMuon/L2CombinedMuonContainer.h"
#include "xAODTrigMuon/versions/L2StandAloneMuon_v1.h"
#include "xAODTrigMuon/versions/L2StandAloneMuonContainer_v1.h"
#include "xAODTrigMuon/versions/L2StandAloneMuonAuxContainer_v1.h"
#include "xAODTrigMuon/versions/L2StandAloneMuon_v2.h"
#include "xAODTrigMuon/versions/L2StandAloneMuonContainer_v2.h"
#include "xAODTrigMuon/versions/L2StandAloneMuonAuxContainer_v2.h"
#include "xAODTrigMuon/versions/L2IsoMuon_v1.h"
#include "xAODTrigMuon/versions/L2IsoMuonContainer_v1.h"
#include "xAODTrigMuon/versions/L2IsoMuonAuxContainer_v1.h"
#include "xAODTrigMuon/versions/L2CombinedMuon_v1.h"
#include "xAODTrigMuon/versions/L2CombinedMuonContainer_v1.h"
#include "xAODTrigMuon/versions/L2CombinedMuonAuxContainer_v1.h"

namespace{
   struct GCCXML_DUMMY_INSTANTIATION_XAODTRIGMUON {

      xAOD::L2StandAloneMuonContainer_v1 muonsa_c1;
      DataLink< xAOD::L2StandAloneMuonContainer_v1 > muonsa_dl1;
      std::vector< DataLink< xAOD::L2StandAloneMuonContainer_v1 > > muonsa_dl2;
      ElementLink< xAOD::L2StandAloneMuonContainer_v1 > muonsa_el1;
      std::vector< ElementLink< xAOD::L2StandAloneMuonContainer_v1 > > muonsa_el2;
      std::vector< std::vector< ElementLink< xAOD::L2StandAloneMuonContainer_v1 > > > muonsa_el3;

      xAOD::L2StandAloneMuonContainer_v2 muonsa_c2;
      DataLink< xAOD::L2StandAloneMuonContainer_v2 > muonsa_dl4;
      std::vector< DataLink< xAOD::L2StandAloneMuonContainer_v2 > > muonsa_dl5;
      ElementLink< xAOD::L2StandAloneMuonContainer_v2 > muonsa_el4;
      std::vector< ElementLink< xAOD::L2StandAloneMuonContainer_v2 > > muonsa_el5;
      std::vector< std::vector< ElementLink< xAOD::L2StandAloneMuonContainer_v2 > > > muonsa_el6;

      xAOD::L2IsoMuonContainer_v1 muoniso_c1;
      DataLink< xAOD::L2IsoMuonContainer_v1 > muoniso_dl1;
      std::vector< DataLink< xAOD::L2IsoMuonContainer_v1 > > muoniso_dl2;
      ElementLink< xAOD::L2IsoMuonContainer_v1 > muoniso_el1;
      std::vector< ElementLink< xAOD::L2IsoMuonContainer_v1 > > muoniso_el2;
      std::vector< std::vector< ElementLink< xAOD::L2IsoMuonContainer_v1 > > > muoniso_el3;

      xAOD::L2CombinedMuonContainer_v1 muoncb_c1;
      DataLink< xAOD::L2CombinedMuonContainer_v1 > muoncb_dl1;
      std::vector< DataLink< xAOD::L2CombinedMuonContainer_v1 > > muoncb_dl2;
      ElementLink< xAOD::L2CombinedMuonContainer_v1 > muoncb_el1;
      std::vector< ElementLink< xAOD::L2CombinedMuonContainer_v1 > > muoncb_el2;
      std::vector< std::vector< ElementLink< xAOD::L2CombinedMuonContainer_v1 > > > muoncb_el3;

   };
}

#endif // XAODTRIGMUON_XAODTRIGMUONDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataLink<DataVector<xAOD::L2CombinedMuon_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::L2IsoMuon_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::L2StandAloneMuon_v1> >", payloadCode, "@",
"DataLink<DataVector<xAOD::L2StandAloneMuon_v2> >", payloadCode, "@",
"DataLink<xAOD::L2CombinedMuonContainer_v1>", payloadCode, "@",
"DataLink<xAOD::L2IsoMuonContainer_v1>", payloadCode, "@",
"DataLink<xAOD::L2StandAloneMuonContainer_v1>", payloadCode, "@",
"DataLink<xAOD::L2StandAloneMuonContainer_v2>", payloadCode, "@",
"DataVector<xAOD::L2CombinedMuon_v1>", payloadCode, "@",
"DataVector<xAOD::L2IsoMuon_v1>", payloadCode, "@",
"DataVector<xAOD::L2StandAloneMuon_v1>", payloadCode, "@",
"DataVector<xAOD::L2StandAloneMuon_v2>", payloadCode, "@",
"ElementLink<DataVector<xAOD::L2CombinedMuon_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::L2IsoMuon_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> >", payloadCode, "@",
"ElementLink<xAOD::L2CombinedMuonContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::L2IsoMuonContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::L2StandAloneMuonContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::L2StandAloneMuonContainer_v2>", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::L2CombinedMuon_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::L2IsoMuon_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v1> > >", payloadCode, "@",
"vector<DataLink<DataVector<xAOD::L2StandAloneMuon_v2> > >", payloadCode, "@",
"vector<DataLink<xAOD::L2CombinedMuonContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::L2IsoMuonContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::L2StandAloneMuonContainer_v1> >", payloadCode, "@",
"vector<DataLink<xAOD::L2StandAloneMuonContainer_v2> >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > >", payloadCode, "@",
"vector<ElementLink<xAOD::L2CombinedMuonContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::L2IsoMuonContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::L2StandAloneMuonContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::L2StandAloneMuonContainer_v2> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::L2CombinedMuonContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::L2IsoMuonContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::L2StandAloneMuonContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::L2StandAloneMuonContainer_v2> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::L2CombinedMuon_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::L2IsoMuon_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::L2StandAloneMuon_v2> > > >", payloadCode, "@",
"xAOD::L2CombinedMuonAuxContainer_v1", payloadCode, "@",
"xAOD::L2CombinedMuonContainer_v1", payloadCode, "@",
"xAOD::L2CombinedMuon_v1", payloadCode, "@",
"xAOD::L2IsoMuonAuxContainer_v1", payloadCode, "@",
"xAOD::L2IsoMuonContainer_v1", payloadCode, "@",
"xAOD::L2IsoMuon_v1", payloadCode, "@",
"xAOD::L2StandAloneMuonAuxContainer_v1", payloadCode, "@",
"xAOD::L2StandAloneMuonAuxContainer_v2", payloadCode, "@",
"xAOD::L2StandAloneMuonContainer_v1", payloadCode, "@",
"xAOD::L2StandAloneMuonContainer_v2", payloadCode, "@",
"xAOD::L2StandAloneMuon_v1", payloadCode, "@",
"xAOD::L2StandAloneMuon_v2", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigMuon_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigMuon_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigMuon_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigMuon_Reflex() {
  TriggerDictionaryInitialization_xAODTrigMuon_Reflex_Impl();
}

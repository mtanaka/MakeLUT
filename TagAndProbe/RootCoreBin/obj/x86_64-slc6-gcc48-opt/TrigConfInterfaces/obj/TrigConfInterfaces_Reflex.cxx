// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdITrigConfInterfacesdIobjdITrigConfInterfaces_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigConfInterfaces/TrigConfInterfaces/TrigConfInterfacesDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *TrigConfcLcLIILVL1ConfigSvc_Dictionary();
   static void TrigConfcLcLIILVL1ConfigSvc_TClassManip(TClass*);
   static void delete_TrigConfcLcLIILVL1ConfigSvc(void *p);
   static void deleteArray_TrigConfcLcLIILVL1ConfigSvc(void *p);
   static void destruct_TrigConfcLcLIILVL1ConfigSvc(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrigConf::IILVL1ConfigSvc*)
   {
      ::TrigConf::IILVL1ConfigSvc *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TrigConf::IILVL1ConfigSvc));
      static ::ROOT::TGenericClassInfo 
         instance("TrigConf::IILVL1ConfigSvc", "TrigConfInterfaces/IILVL1ConfigSvc.h", 26,
                  typeid(::TrigConf::IILVL1ConfigSvc), DefineBehavior(ptr, ptr),
                  &TrigConfcLcLIILVL1ConfigSvc_Dictionary, isa_proxy, 0,
                  sizeof(::TrigConf::IILVL1ConfigSvc) );
      instance.SetDelete(&delete_TrigConfcLcLIILVL1ConfigSvc);
      instance.SetDeleteArray(&deleteArray_TrigConfcLcLIILVL1ConfigSvc);
      instance.SetDestructor(&destruct_TrigConfcLcLIILVL1ConfigSvc);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrigConf::IILVL1ConfigSvc*)
   {
      return GenerateInitInstanceLocal((::TrigConf::IILVL1ConfigSvc*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrigConf::IILVL1ConfigSvc*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigConfcLcLIILVL1ConfigSvc_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TrigConf::IILVL1ConfigSvc*)0x0)->GetClass();
      TrigConfcLcLIILVL1ConfigSvc_TClassManip(theClass);
   return theClass;
   }

   static void TrigConfcLcLIILVL1ConfigSvc_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrigConfcLcLIIHLTConfigSvc_Dictionary();
   static void TrigConfcLcLIIHLTConfigSvc_TClassManip(TClass*);
   static void delete_TrigConfcLcLIIHLTConfigSvc(void *p);
   static void deleteArray_TrigConfcLcLIIHLTConfigSvc(void *p);
   static void destruct_TrigConfcLcLIIHLTConfigSvc(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrigConf::IIHLTConfigSvc*)
   {
      ::TrigConf::IIHLTConfigSvc *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TrigConf::IIHLTConfigSvc));
      static ::ROOT::TGenericClassInfo 
         instance("TrigConf::IIHLTConfigSvc", "TrigConfInterfaces/IIHLTConfigSvc.h", 24,
                  typeid(::TrigConf::IIHLTConfigSvc), DefineBehavior(ptr, ptr),
                  &TrigConfcLcLIIHLTConfigSvc_Dictionary, isa_proxy, 0,
                  sizeof(::TrigConf::IIHLTConfigSvc) );
      instance.SetDelete(&delete_TrigConfcLcLIIHLTConfigSvc);
      instance.SetDeleteArray(&deleteArray_TrigConfcLcLIIHLTConfigSvc);
      instance.SetDestructor(&destruct_TrigConfcLcLIIHLTConfigSvc);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrigConf::IIHLTConfigSvc*)
   {
      return GenerateInitInstanceLocal((::TrigConf::IIHLTConfigSvc*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrigConf::IIHLTConfigSvc*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigConfcLcLIIHLTConfigSvc_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TrigConf::IIHLTConfigSvc*)0x0)->GetClass();
      TrigConfcLcLIIHLTConfigSvc_TClassManip(theClass);
   return theClass;
   }

   static void TrigConfcLcLIIHLTConfigSvc_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrigConfcLcLITrigConfigTool_Dictionary();
   static void TrigConfcLcLITrigConfigTool_TClassManip(TClass*);
   static void delete_TrigConfcLcLITrigConfigTool(void *p);
   static void deleteArray_TrigConfcLcLITrigConfigTool(void *p);
   static void destruct_TrigConfcLcLITrigConfigTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrigConf::ITrigConfigTool*)
   {
      ::TrigConf::ITrigConfigTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TrigConf::ITrigConfigTool));
      static ::ROOT::TGenericClassInfo 
         instance("TrigConf::ITrigConfigTool", "TrigConfInterfaces/ITrigConfigTool.h", 24,
                  typeid(::TrigConf::ITrigConfigTool), DefineBehavior(ptr, ptr),
                  &TrigConfcLcLITrigConfigTool_Dictionary, isa_proxy, 0,
                  sizeof(::TrigConf::ITrigConfigTool) );
      instance.SetDelete(&delete_TrigConfcLcLITrigConfigTool);
      instance.SetDeleteArray(&deleteArray_TrigConfcLcLITrigConfigTool);
      instance.SetDestructor(&destruct_TrigConfcLcLITrigConfigTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrigConf::ITrigConfigTool*)
   {
      return GenerateInitInstanceLocal((::TrigConf::ITrigConfigTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrigConf::ITrigConfigTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrigConfcLcLITrigConfigTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TrigConf::ITrigConfigTool*)0x0)->GetClass();
      TrigConfcLcLITrigConfigTool_TClassManip(theClass);
   return theClass;
   }

   static void TrigConfcLcLITrigConfigTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TrigConfcLcLIILVL1ConfigSvc(void *p) {
      delete ((::TrigConf::IILVL1ConfigSvc*)p);
   }
   static void deleteArray_TrigConfcLcLIILVL1ConfigSvc(void *p) {
      delete [] ((::TrigConf::IILVL1ConfigSvc*)p);
   }
   static void destruct_TrigConfcLcLIILVL1ConfigSvc(void *p) {
      typedef ::TrigConf::IILVL1ConfigSvc current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrigConf::IILVL1ConfigSvc

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TrigConfcLcLIIHLTConfigSvc(void *p) {
      delete ((::TrigConf::IIHLTConfigSvc*)p);
   }
   static void deleteArray_TrigConfcLcLIIHLTConfigSvc(void *p) {
      delete [] ((::TrigConf::IIHLTConfigSvc*)p);
   }
   static void destruct_TrigConfcLcLIIHLTConfigSvc(void *p) {
      typedef ::TrigConf::IIHLTConfigSvc current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrigConf::IIHLTConfigSvc

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TrigConfcLcLITrigConfigTool(void *p) {
      delete ((::TrigConf::ITrigConfigTool*)p);
   }
   static void deleteArray_TrigConfcLcLITrigConfigTool(void *p) {
      delete [] ((::TrigConf::ITrigConfigTool*)p);
   }
   static void destruct_TrigConfcLcLITrigConfigTool(void *p) {
      typedef ::TrigConf::ITrigConfigTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrigConf::ITrigConfigTool

namespace {
  void TriggerDictionaryInitialization_TrigConfInterfaces_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigConfInterfaces/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/TrigConfInterfaces/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace TrigConf{class __attribute__((annotate("$clingAutoload$TrigConfInterfaces/IILVL1ConfigSvc.h")))  IILVL1ConfigSvc;}
namespace TrigConf{class __attribute__((annotate("$clingAutoload$TrigConfInterfaces/IIHLTConfigSvc.h")))  IIHLTConfigSvc;}
namespace TrigConf{class __attribute__((annotate("$clingAutoload$TrigConfInterfaces/ITrigConfigTool.h")))  ITrigConfigTool;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "TrigConfInterfaces"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: TrigConfInterfacesDict.h 656610 2015-03-25 08:56:32Z krasznaa $
#ifndef TRIGCONFINTERFACES_TRIGCONFINTERFACESDICT_H
#define TRIGCONFINTERFACES_TRIGCONFINTERFACESDICT_H

// Local include(s):
#include "TrigConfInterfaces/IILVL1ConfigSvc.h"
#include "TrigConfInterfaces/IIHLTConfigSvc.h"
#include "TrigConfInterfaces/ITrigConfigTool.h"

#endif // TRIGCONFINTERFACES_TRIGCONFINTERFACESDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"TrigConf::IIHLTConfigSvc", payloadCode, "@",
"TrigConf::IILVL1ConfigSvc", payloadCode, "@",
"TrigConf::ITrigConfigTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TrigConfInterfaces_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TrigConfInterfaces_Reflex_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TrigConfInterfaces_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TrigConfInterfaces_Reflex() {
  TriggerDictionaryInitialization_TrigConfInterfaces_Reflex_Impl();
}

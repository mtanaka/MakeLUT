// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIAsgToolsdIobjdIAsgTools_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/AsgTools/AsgTools/AsgToolsDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *asgcLcLIAsgTool_Dictionary();
   static void asgcLcLIAsgTool_TClassManip(TClass*);
   static void delete_asgcLcLIAsgTool(void *p);
   static void deleteArray_asgcLcLIAsgTool(void *p);
   static void destruct_asgcLcLIAsgTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::asg::IAsgTool*)
   {
      ::asg::IAsgTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::asg::IAsgTool));
      static ::ROOT::TGenericClassInfo 
         instance("asg::IAsgTool", "AsgTools/IAsgTool.h", 35,
                  typeid(::asg::IAsgTool), DefineBehavior(ptr, ptr),
                  &asgcLcLIAsgTool_Dictionary, isa_proxy, 0,
                  sizeof(::asg::IAsgTool) );
      instance.SetDelete(&delete_asgcLcLIAsgTool);
      instance.SetDeleteArray(&deleteArray_asgcLcLIAsgTool);
      instance.SetDestructor(&destruct_asgcLcLIAsgTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::asg::IAsgTool*)
   {
      return GenerateInitInstanceLocal((::asg::IAsgTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::asg::IAsgTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *asgcLcLIAsgTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::asg::IAsgTool*)0x0)->GetClass();
      asgcLcLIAsgTool_TClassManip(theClass);
   return theClass;
   }

   static void asgcLcLIAsgTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *asgcLcLAsgTool_Dictionary();
   static void asgcLcLAsgTool_TClassManip(TClass*);
   static void delete_asgcLcLAsgTool(void *p);
   static void deleteArray_asgcLcLAsgTool(void *p);
   static void destruct_asgcLcLAsgTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::asg::AsgTool*)
   {
      ::asg::AsgTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::asg::AsgTool));
      static ::ROOT::TGenericClassInfo 
         instance("asg::AsgTool", "AsgTools/AsgTool.h", 46,
                  typeid(::asg::AsgTool), DefineBehavior(ptr, ptr),
                  &asgcLcLAsgTool_Dictionary, isa_proxy, 0,
                  sizeof(::asg::AsgTool) );
      instance.SetDelete(&delete_asgcLcLAsgTool);
      instance.SetDeleteArray(&deleteArray_asgcLcLAsgTool);
      instance.SetDestructor(&destruct_asgcLcLAsgTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::asg::AsgTool*)
   {
      return GenerateInitInstanceLocal((::asg::AsgTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::asg::AsgTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *asgcLcLAsgTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::asg::AsgTool*)0x0)->GetClass();
      asgcLcLAsgTool_TClassManip(theClass);
   return theClass;
   }

   static void asgcLcLAsgTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *asgcLcLAsgMetadataTool_Dictionary();
   static void asgcLcLAsgMetadataTool_TClassManip(TClass*);
   static void delete_asgcLcLAsgMetadataTool(void *p);
   static void deleteArray_asgcLcLAsgMetadataTool(void *p);
   static void destruct_asgcLcLAsgMetadataTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::asg::AsgMetadataTool*)
   {
      ::asg::AsgMetadataTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::asg::AsgMetadataTool));
      static ::ROOT::TGenericClassInfo 
         instance("asg::AsgMetadataTool", "AsgTools/AsgMetadataTool.h", 47,
                  typeid(::asg::AsgMetadataTool), DefineBehavior(ptr, ptr),
                  &asgcLcLAsgMetadataTool_Dictionary, isa_proxy, 0,
                  sizeof(::asg::AsgMetadataTool) );
      instance.SetDelete(&delete_asgcLcLAsgMetadataTool);
      instance.SetDeleteArray(&deleteArray_asgcLcLAsgMetadataTool);
      instance.SetDestructor(&destruct_asgcLcLAsgMetadataTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::asg::AsgMetadataTool*)
   {
      return GenerateInitInstanceLocal((::asg::AsgMetadataTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::asg::AsgMetadataTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *asgcLcLAsgMetadataTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::asg::AsgMetadataTool*)0x0)->GetClass();
      asgcLcLAsgMetadataTool_TClassManip(theClass);
   return theClass;
   }

   static void asgcLcLAsgMetadataTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *asgcLcLAsgMessaging_Dictionary();
   static void asgcLcLAsgMessaging_TClassManip(TClass*);
   static void delete_asgcLcLAsgMessaging(void *p);
   static void deleteArray_asgcLcLAsgMessaging(void *p);
   static void destruct_asgcLcLAsgMessaging(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::asg::AsgMessaging*)
   {
      ::asg::AsgMessaging *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::asg::AsgMessaging));
      static ::ROOT::TGenericClassInfo 
         instance("asg::AsgMessaging", "AsgTools/AsgMessaging.h", 36,
                  typeid(::asg::AsgMessaging), DefineBehavior(ptr, ptr),
                  &asgcLcLAsgMessaging_Dictionary, isa_proxy, 0,
                  sizeof(::asg::AsgMessaging) );
      instance.SetDelete(&delete_asgcLcLAsgMessaging);
      instance.SetDeleteArray(&deleteArray_asgcLcLAsgMessaging);
      instance.SetDestructor(&destruct_asgcLcLAsgMessaging);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::asg::AsgMessaging*)
   {
      return GenerateInitInstanceLocal((::asg::AsgMessaging*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::asg::AsgMessaging*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *asgcLcLAsgMessaging_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::asg::AsgMessaging*)0x0)->GetClass();
      asgcLcLAsgMessaging_TClassManip(theClass);
   return theClass;
   }

   static void asgcLcLAsgMessaging_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *asgcLcLToolStore_Dictionary();
   static void asgcLcLToolStore_TClassManip(TClass*);
   static void *new_asgcLcLToolStore(void *p = 0);
   static void *newArray_asgcLcLToolStore(Long_t size, void *p);
   static void delete_asgcLcLToolStore(void *p);
   static void deleteArray_asgcLcLToolStore(void *p);
   static void destruct_asgcLcLToolStore(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::asg::ToolStore*)
   {
      ::asg::ToolStore *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::asg::ToolStore));
      static ::ROOT::TGenericClassInfo 
         instance("asg::ToolStore", "AsgTools/ToolStore.h", 30,
                  typeid(::asg::ToolStore), DefineBehavior(ptr, ptr),
                  &asgcLcLToolStore_Dictionary, isa_proxy, 0,
                  sizeof(::asg::ToolStore) );
      instance.SetNew(&new_asgcLcLToolStore);
      instance.SetNewArray(&newArray_asgcLcLToolStore);
      instance.SetDelete(&delete_asgcLcLToolStore);
      instance.SetDeleteArray(&deleteArray_asgcLcLToolStore);
      instance.SetDestructor(&destruct_asgcLcLToolStore);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::asg::ToolStore*)
   {
      return GenerateInitInstanceLocal((::asg::ToolStore*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::asg::ToolStore*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *asgcLcLToolStore_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::asg::ToolStore*)0x0)->GetClass();
      asgcLcLToolStore_TClassManip(theClass);
   return theClass;
   }

   static void asgcLcLToolStore_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_asgcLcLIAsgTool(void *p) {
      delete ((::asg::IAsgTool*)p);
   }
   static void deleteArray_asgcLcLIAsgTool(void *p) {
      delete [] ((::asg::IAsgTool*)p);
   }
   static void destruct_asgcLcLIAsgTool(void *p) {
      typedef ::asg::IAsgTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::asg::IAsgTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_asgcLcLAsgTool(void *p) {
      delete ((::asg::AsgTool*)p);
   }
   static void deleteArray_asgcLcLAsgTool(void *p) {
      delete [] ((::asg::AsgTool*)p);
   }
   static void destruct_asgcLcLAsgTool(void *p) {
      typedef ::asg::AsgTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::asg::AsgTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_asgcLcLAsgMetadataTool(void *p) {
      delete ((::asg::AsgMetadataTool*)p);
   }
   static void deleteArray_asgcLcLAsgMetadataTool(void *p) {
      delete [] ((::asg::AsgMetadataTool*)p);
   }
   static void destruct_asgcLcLAsgMetadataTool(void *p) {
      typedef ::asg::AsgMetadataTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::asg::AsgMetadataTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_asgcLcLAsgMessaging(void *p) {
      delete ((::asg::AsgMessaging*)p);
   }
   static void deleteArray_asgcLcLAsgMessaging(void *p) {
      delete [] ((::asg::AsgMessaging*)p);
   }
   static void destruct_asgcLcLAsgMessaging(void *p) {
      typedef ::asg::AsgMessaging current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::asg::AsgMessaging

namespace ROOT {
   // Wrappers around operator new
   static void *new_asgcLcLToolStore(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::asg::ToolStore : new ::asg::ToolStore;
   }
   static void *newArray_asgcLcLToolStore(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::asg::ToolStore[nElements] : new ::asg::ToolStore[nElements];
   }
   // Wrapper around operator delete
   static void delete_asgcLcLToolStore(void *p) {
      delete ((::asg::ToolStore*)p);
   }
   static void deleteArray_asgcLcLToolStore(void *p) {
      delete [] ((::asg::ToolStore*)p);
   }
   static void destruct_asgcLcLToolStore(void *p) {
      typedef ::asg::ToolStore current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::asg::ToolStore

namespace {
  void TriggerDictionaryInitialization_AsgTools_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/AsgTools/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/AsgTools",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/AsgTools/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace asg{class __attribute__((annotate("$clingAutoload$AsgTools/IAsgTool.h")))  IAsgTool;}
namespace asg{class __attribute__((annotate("$clingAutoload$AsgTools/AsgTool.h")))  AsgTool;}
namespace asg{class __attribute__((annotate("$clingAutoload$AsgTools/AsgMetadataTool.h")))  AsgMetadataTool;}
namespace asg{class __attribute__((annotate("$clingAutoload$AsgTools/AsgTool.h")))  AsgMessaging;}
namespace asg{class __attribute__((annotate("$clingAutoload$AsgTools/AsgTool.h")))  ToolStore;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "AsgTools"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: AsgToolsDict.h 676552 2015-06-19 05:56:54Z krasznaa $
#ifndef ASGTOOLS_ASGTOOLSDICT_H
#define ASGTOOLS_ASGTOOLSDICT_H

#ifdef __GNUC__
# pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#endif

// Local include(s):
#include "AsgTools/IAsgTool.h"
#include "AsgTools/AsgTool.h"
#include "AsgTools/AsgMetadataTool.h"
#include "AsgTools/AsgMessaging.h"
#include "AsgTools/ToolStore.h"

// The following is only needed for standalone usage. In Athena the
// setProperty(...) function(s) come(s) from the AlgTool base class, with all
// the necessary dictionaries declared in GaudiKernel.
#ifdef ASGTOOL_STANDALONE

// Helper macro for declaring the setProperty functions to the dictionary:
#define SETPROPERTY_INSTAN( TYPE )                                            \
   template StatusCode asg::AsgTool::setProperty< TYPE >( const std::string&, \
                                                          const TYPE& )

// Declare all possible setProperty template instantiations to Reflex:
SETPROPERTY_INSTAN( bool );
SETPROPERTY_INSTAN( short );
SETPROPERTY_INSTAN( unsigned short );
SETPROPERTY_INSTAN( int );
SETPROPERTY_INSTAN( unsigned int );
SETPROPERTY_INSTAN( long );
SETPROPERTY_INSTAN( unsigned long );
SETPROPERTY_INSTAN( long long );
SETPROPERTY_INSTAN( unsigned long long );
SETPROPERTY_INSTAN( float );
SETPROPERTY_INSTAN( double );
SETPROPERTY_INSTAN( std::string );

SETPROPERTY_INSTAN( std::vector< bool > );
SETPROPERTY_INSTAN( std::vector< short > );
SETPROPERTY_INSTAN( std::vector< unsigned short > );
SETPROPERTY_INSTAN( std::vector< int > );
SETPROPERTY_INSTAN( std::vector< unsigned int > );
SETPROPERTY_INSTAN( std::vector< long > );
SETPROPERTY_INSTAN( std::vector< unsigned long > );
SETPROPERTY_INSTAN( std::vector< long long > );
SETPROPERTY_INSTAN( std::vector< unsigned long long > );
SETPROPERTY_INSTAN( std::vector< float > );
SETPROPERTY_INSTAN( std::vector< double > );
SETPROPERTY_INSTAN( std::vector< std::string > );

// Make the compiler forget about this macro now...
#undef SETPROPERTY_INSTAN

#endif // ASGTOOL_STANDALONE
#endif // not ASGTOOLS_ASGTOOLSDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"asg::AsgMessaging", payloadCode, "@",
"asg::AsgMetadataTool", payloadCode, "@",
"asg::AsgTool", payloadCode, "@",
"asg::IAsgTool", payloadCode, "@",
"asg::ToolStore", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("AsgTools_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_AsgTools_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_AsgTools_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_AsgTools_Reflex() {
  TriggerDictionaryInitialization_AsgTools_Reflex_Impl();
}

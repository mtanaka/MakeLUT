// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIMCTruthClassifierdIobjdIMCTruthClassifier_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *IMCTruthClassifier_Dictionary();
   static void IMCTruthClassifier_TClassManip(TClass*);
   static void delete_IMCTruthClassifier(void *p);
   static void deleteArray_IMCTruthClassifier(void *p);
   static void destruct_IMCTruthClassifier(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IMCTruthClassifier*)
   {
      ::IMCTruthClassifier *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IMCTruthClassifier));
      static ::ROOT::TGenericClassInfo 
         instance("IMCTruthClassifier", "MCTruthClassifier/IMCTruthClassifier.h", 36,
                  typeid(::IMCTruthClassifier), DefineBehavior(ptr, ptr),
                  &IMCTruthClassifier_Dictionary, isa_proxy, 0,
                  sizeof(::IMCTruthClassifier) );
      instance.SetDelete(&delete_IMCTruthClassifier);
      instance.SetDeleteArray(&deleteArray_IMCTruthClassifier);
      instance.SetDestructor(&destruct_IMCTruthClassifier);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IMCTruthClassifier*)
   {
      return GenerateInitInstanceLocal((::IMCTruthClassifier*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::IMCTruthClassifier*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IMCTruthClassifier_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IMCTruthClassifier*)0x0)->GetClass();
      IMCTruthClassifier_TClassManip(theClass);
   return theClass;
   }

   static void IMCTruthClassifier_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *MCTruthClassifier_Dictionary();
   static void MCTruthClassifier_TClassManip(TClass*);
   static void delete_MCTruthClassifier(void *p);
   static void deleteArray_MCTruthClassifier(void *p);
   static void destruct_MCTruthClassifier(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MCTruthClassifier*)
   {
      ::MCTruthClassifier *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MCTruthClassifier));
      static ::ROOT::TGenericClassInfo 
         instance("MCTruthClassifier", "MCTruthClassifier/MCTruthClassifier.h", 36,
                  typeid(::MCTruthClassifier), DefineBehavior(ptr, ptr),
                  &MCTruthClassifier_Dictionary, isa_proxy, 0,
                  sizeof(::MCTruthClassifier) );
      instance.SetDelete(&delete_MCTruthClassifier);
      instance.SetDeleteArray(&deleteArray_MCTruthClassifier);
      instance.SetDestructor(&destruct_MCTruthClassifier);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MCTruthClassifier*)
   {
      return GenerateInitInstanceLocal((::MCTruthClassifier*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MCTruthClassifier*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MCTruthClassifier_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MCTruthClassifier*)0x0)->GetClass();
      MCTruthClassifier_TClassManip(theClass);
   return theClass;
   }

   static void MCTruthClassifier_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *MCTruthPartClassifiercLcLParticleDef_Dictionary();
   static void MCTruthPartClassifiercLcLParticleDef_TClassManip(TClass*);
   static void *new_MCTruthPartClassifiercLcLParticleDef(void *p = 0);
   static void *newArray_MCTruthPartClassifiercLcLParticleDef(Long_t size, void *p);
   static void delete_MCTruthPartClassifiercLcLParticleDef(void *p);
   static void deleteArray_MCTruthPartClassifiercLcLParticleDef(void *p);
   static void destruct_MCTruthPartClassifiercLcLParticleDef(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MCTruthPartClassifier::ParticleDef*)
   {
      ::MCTruthPartClassifier::ParticleDef *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MCTruthPartClassifier::ParticleDef));
      static ::ROOT::TGenericClassInfo 
         instance("MCTruthPartClassifier::ParticleDef", "MCTruthClassifier/MCTruthClassifierDefs.h", 138,
                  typeid(::MCTruthPartClassifier::ParticleDef), DefineBehavior(ptr, ptr),
                  &MCTruthPartClassifiercLcLParticleDef_Dictionary, isa_proxy, 0,
                  sizeof(::MCTruthPartClassifier::ParticleDef) );
      instance.SetNew(&new_MCTruthPartClassifiercLcLParticleDef);
      instance.SetNewArray(&newArray_MCTruthPartClassifiercLcLParticleDef);
      instance.SetDelete(&delete_MCTruthPartClassifiercLcLParticleDef);
      instance.SetDeleteArray(&deleteArray_MCTruthPartClassifiercLcLParticleDef);
      instance.SetDestructor(&destruct_MCTruthPartClassifiercLcLParticleDef);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MCTruthPartClassifier::ParticleDef*)
   {
      return GenerateInitInstanceLocal((::MCTruthPartClassifier::ParticleDef*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MCTruthPartClassifier::ParticleDef*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MCTruthPartClassifiercLcLParticleDef_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MCTruthPartClassifier::ParticleDef*)0x0)->GetClass();
      MCTruthPartClassifiercLcLParticleDef_TClassManip(theClass);
   return theClass;
   }

   static void MCTruthPartClassifiercLcLParticleDef_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR_Dictionary();
   static void pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR_TClassManip(TClass*);
   static void *new_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(void *p = 0);
   static void *newArray_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(Long_t size, void *p);
   static void delete_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(void *p);
   static void deleteArray_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(void *p);
   static void destruct_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>*)
   {
      pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>));
      static ::ROOT::TGenericClassInfo 
         instance("pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>", "string", 96,
                  typeid(pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>), DefineBehavior(ptr, ptr),
                  &pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR_Dictionary, isa_proxy, 0,
                  sizeof(pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>) );
      instance.SetNew(&new_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR);
      instance.SetNewArray(&newArray_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR);
      instance.SetDelete(&delete_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR);
      instance.SetDeleteArray(&deleteArray_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR);
      instance.SetDestructor(&destruct_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR);
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>*)0x0)->GetClass();
      pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR_TClassManip(theClass);
   return theClass;
   }

   static void pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IMCTruthClassifier(void *p) {
      delete ((::IMCTruthClassifier*)p);
   }
   static void deleteArray_IMCTruthClassifier(void *p) {
      delete [] ((::IMCTruthClassifier*)p);
   }
   static void destruct_IMCTruthClassifier(void *p) {
      typedef ::IMCTruthClassifier current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IMCTruthClassifier

namespace ROOT {
   // Wrapper around operator delete
   static void delete_MCTruthClassifier(void *p) {
      delete ((::MCTruthClassifier*)p);
   }
   static void deleteArray_MCTruthClassifier(void *p) {
      delete [] ((::MCTruthClassifier*)p);
   }
   static void destruct_MCTruthClassifier(void *p) {
      typedef ::MCTruthClassifier current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MCTruthClassifier

namespace ROOT {
   // Wrappers around operator new
   static void *new_MCTruthPartClassifiercLcLParticleDef(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::MCTruthPartClassifier::ParticleDef : new ::MCTruthPartClassifier::ParticleDef;
   }
   static void *newArray_MCTruthPartClassifiercLcLParticleDef(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::MCTruthPartClassifier::ParticleDef[nElements] : new ::MCTruthPartClassifier::ParticleDef[nElements];
   }
   // Wrapper around operator delete
   static void delete_MCTruthPartClassifiercLcLParticleDef(void *p) {
      delete ((::MCTruthPartClassifier::ParticleDef*)p);
   }
   static void deleteArray_MCTruthPartClassifiercLcLParticleDef(void *p) {
      delete [] ((::MCTruthPartClassifier::ParticleDef*)p);
   }
   static void destruct_MCTruthPartClassifiercLcLParticleDef(void *p) {
      typedef ::MCTruthPartClassifier::ParticleDef current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MCTruthPartClassifier::ParticleDef

namespace ROOT {
   // Wrappers around operator new
   static void *new_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin> : new pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>;
   }
   static void *newArray_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>[nElements] : new pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>[nElements];
   }
   // Wrapper around operator delete
   static void delete_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(void *p) {
      delete ((pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>*)p);
   }
   static void deleteArray_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(void *p) {
      delete [] ((pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>*)p);
   }
   static void destruct_pairlEMCTruthPartClassifiercLcLParticleTypecOMCTruthPartClassifiercLcLParticleOrigingR(void *p) {
      typedef pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>

namespace {
  void TriggerDictionaryInitialization_MCTruthClassifier_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MCTruthClassifier/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MCTruthClassifier/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$MCTruthClassifier/IMCTruthClassifier.h")))  IMCTruthClassifier;
class __attribute__((annotate("$clingAutoload$MCTruthClassifier/MCTruthClassifier.h")))  MCTruthClassifier;
namespace MCTruthPartClassifier{struct __attribute__((annotate("$clingAutoload$MCTruthClassifier/IMCTruthClassifier.h")))  ParticleDef;}
namespace MCTruthPartClassifier{enum  __attribute__((annotate("$clingAutoload$MCTruthClassifier/IMCTruthClassifier.h"))) ParticleType : unsigned int;}
namespace MCTruthPartClassifier{enum  __attribute__((annotate("$clingAutoload$MCTruthClassifier/IMCTruthClassifier.h"))) ParticleOrigin : unsigned int;}
namespace std{template <class _T1, class _T2> struct __attribute__((annotate("$clingAutoload$string")))  pair;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "MCTruthClassifier"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef MCTRUTHCLASSIFIER_MCTRUTHCLASSIFIERDICT_H
#define MCTRUTHCLASSIFIER_MCTRUTHCLASSIFIERDICT_H
	
#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "MCTruthClassifier/MCTruthClassifier.h"
#include "MCTruthClassifier/MCTruthClassifierDefs.h"
	
 namespace MCTruthClassifierEnumsDict {
	
   struct tmp {
     std::pair<MCTruthPartClassifier::ParticleType,
       MCTruthPartClassifier::ParticleOrigin> m_1;
   };
 }
#endif // MCTRUTHCLASSIFIER_MCTRUTHCLASSIFIERDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"IMCTruthClassifier", payloadCode, "@",
"MCTruthClassifier", payloadCode, "@",
"MCTruthPartClassifier::ParticleDef", payloadCode, "@",
"MCTruthPartClassifier::ParticleOrigin", payloadCode, "@",
"MCTruthPartClassifier::ParticleOutCome", payloadCode, "@",
"MCTruthPartClassifier::ParticleType", payloadCode, "@",
"pair<MCTruthPartClassifier::ParticleType,MCTruthPartClassifier::ParticleOrigin>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("MCTruthClassifier_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_MCTruthClassifier_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_MCTruthClassifier_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_MCTruthClassifier_Reflex() {
  TriggerDictionaryInitialization_MCTruthClassifier_Reflex_Impl();
}

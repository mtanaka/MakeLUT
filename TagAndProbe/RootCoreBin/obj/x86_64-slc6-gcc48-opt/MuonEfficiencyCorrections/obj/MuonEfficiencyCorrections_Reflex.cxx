// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIMuonEfficiencyCorrectionsdIobjdIMuonEfficiencyCorrections_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonEfficiencyCorrections/MuonEfficiencyCorrections/MuonEfficiencyCorrectionsDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *CPcLcLMuonEfficiencyScaleFactors_Dictionary();
   static void CPcLcLMuonEfficiencyScaleFactors_TClassManip(TClass*);
   static void delete_CPcLcLMuonEfficiencyScaleFactors(void *p);
   static void deleteArray_CPcLcLMuonEfficiencyScaleFactors(void *p);
   static void destruct_CPcLcLMuonEfficiencyScaleFactors(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::MuonEfficiencyScaleFactors*)
   {
      ::CP::MuonEfficiencyScaleFactors *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CP::MuonEfficiencyScaleFactors));
      static ::ROOT::TGenericClassInfo 
         instance("CP::MuonEfficiencyScaleFactors", "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h", 23,
                  typeid(::CP::MuonEfficiencyScaleFactors), DefineBehavior(ptr, ptr),
                  &CPcLcLMuonEfficiencyScaleFactors_Dictionary, isa_proxy, 0,
                  sizeof(::CP::MuonEfficiencyScaleFactors) );
      instance.SetDelete(&delete_CPcLcLMuonEfficiencyScaleFactors);
      instance.SetDeleteArray(&deleteArray_CPcLcLMuonEfficiencyScaleFactors);
      instance.SetDestructor(&destruct_CPcLcLMuonEfficiencyScaleFactors);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::MuonEfficiencyScaleFactors*)
   {
      return GenerateInitInstanceLocal((::CP::MuonEfficiencyScaleFactors*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CP::MuonEfficiencyScaleFactors*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CPcLcLMuonEfficiencyScaleFactors_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CP::MuonEfficiencyScaleFactors*)0x0)->GetClass();
      CPcLcLMuonEfficiencyScaleFactors_TClassManip(theClass);
   return theClass;
   }

   static void CPcLcLMuonEfficiencyScaleFactors_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CPcLcLMuonEfficiencyScaleFactors(void *p) {
      delete ((::CP::MuonEfficiencyScaleFactors*)p);
   }
   static void deleteArray_CPcLcLMuonEfficiencyScaleFactors(void *p) {
      delete [] ((::CP::MuonEfficiencyScaleFactors*)p);
   }
   static void destruct_CPcLcLMuonEfficiencyScaleFactors(void *p) {
      typedef ::CP::MuonEfficiencyScaleFactors current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CP::MuonEfficiencyScaleFactors

namespace {
  void TriggerDictionaryInitialization_MuonEfficiencyCorrections_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonEfficiencyCorrections/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/MuonEfficiencyCorrections/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace CP{class __attribute__((annotate("$clingAutoload$MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h")))  MuonEfficiencyScaleFactors;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "MuonEfficiencyCorrections"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
/*
 * MuonEfficiencyCorrectionsDict.h
 *
 *  Created on: Nov 12, 2014
 *      Author: goblirsc
 */

#ifndef MUONEFFICIENCYCORRECTIONSDICT_H_
#define MUONEFFICIENCYCORRECTIONSDICT_H_

#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"



#endif /* MUONEFFICIENCYCORRECTIONSDICT_H_ */

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"CP::MuonEfficiencyScaleFactors", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("MuonEfficiencyCorrections_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_MuonEfficiencyCorrections_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_MuonEfficiencyCorrections_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_MuonEfficiencyCorrections_Reflex() {
  TriggerDictionaryInitialization_MuonEfficiencyCorrections_Reflex_Impl();
}

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIPATInterfacesdIobjdIPATInterfacesCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicsUtil.h"
#include "PATInterfaces/StreamTestWrapper.h"

// Header files passed via #pragma extra_include

namespace CP {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *CP_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("CP", 0 /*version*/, "PATInterfaces/Global.h", 18,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &CP_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *CP_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace SysStreamTest {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *SysStreamTest_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("SysStreamTest", 0 /*version*/, "PATInterfaces/StreamTestWrapper.h", 11,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &SysStreamTest_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *SysStreamTest_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *CPcLcLSystematicVariation_Dictionary();
   static void CPcLcLSystematicVariation_TClassManip(TClass*);
   static void *new_CPcLcLSystematicVariation(void *p = 0);
   static void *newArray_CPcLcLSystematicVariation(Long_t size, void *p);
   static void delete_CPcLcLSystematicVariation(void *p);
   static void deleteArray_CPcLcLSystematicVariation(void *p);
   static void destruct_CPcLcLSystematicVariation(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::SystematicVariation*)
   {
      ::CP::SystematicVariation *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CP::SystematicVariation));
      static ::ROOT::TGenericClassInfo 
         instance("CP::SystematicVariation", "PATInterfaces/SystematicVariation.h", 48,
                  typeid(::CP::SystematicVariation), DefineBehavior(ptr, ptr),
                  &CPcLcLSystematicVariation_Dictionary, isa_proxy, 4,
                  sizeof(::CP::SystematicVariation) );
      instance.SetNew(&new_CPcLcLSystematicVariation);
      instance.SetNewArray(&newArray_CPcLcLSystematicVariation);
      instance.SetDelete(&delete_CPcLcLSystematicVariation);
      instance.SetDeleteArray(&deleteArray_CPcLcLSystematicVariation);
      instance.SetDestructor(&destruct_CPcLcLSystematicVariation);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::SystematicVariation*)
   {
      return GenerateInitInstanceLocal((::CP::SystematicVariation*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CP::SystematicVariation*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CPcLcLSystematicVariation_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CP::SystematicVariation*)0x0)->GetClass();
      CPcLcLSystematicVariation_TClassManip(theClass);
   return theClass;
   }

   static void CPcLcLSystematicVariation_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CPcLcLSystematicSet_Dictionary();
   static void CPcLcLSystematicSet_TClassManip(TClass*);
   static void *new_CPcLcLSystematicSet(void *p = 0);
   static void *newArray_CPcLcLSystematicSet(Long_t size, void *p);
   static void delete_CPcLcLSystematicSet(void *p);
   static void deleteArray_CPcLcLSystematicSet(void *p);
   static void destruct_CPcLcLSystematicSet(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::SystematicSet*)
   {
      ::CP::SystematicSet *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CP::SystematicSet));
      static ::ROOT::TGenericClassInfo 
         instance("CP::SystematicSet", "PATInterfaces/SystematicSet.h", 29,
                  typeid(::CP::SystematicSet), DefineBehavior(ptr, ptr),
                  &CPcLcLSystematicSet_Dictionary, isa_proxy, 4,
                  sizeof(::CP::SystematicSet) );
      instance.SetNew(&new_CPcLcLSystematicSet);
      instance.SetNewArray(&newArray_CPcLcLSystematicSet);
      instance.SetDelete(&delete_CPcLcLSystematicSet);
      instance.SetDeleteArray(&deleteArray_CPcLcLSystematicSet);
      instance.SetDestructor(&destruct_CPcLcLSystematicSet);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::SystematicSet*)
   {
      return GenerateInitInstanceLocal((::CP::SystematicSet*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CP::SystematicSet*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CPcLcLSystematicSet_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CP::SystematicSet*)0x0)->GetClass();
      CPcLcLSystematicSet_TClassManip(theClass);
   return theClass;
   }

   static void CPcLcLSystematicSet_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *_Rb_tree_const_iteratorlECPcLcLSystematicVariationgR_Dictionary();
   static void _Rb_tree_const_iteratorlECPcLcLSystematicVariationgR_TClassManip(TClass*);
   static void *new__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(void *p = 0);
   static void *newArray__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(Long_t size, void *p);
   static void delete__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(void *p);
   static void deleteArray__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(void *p);
   static void destruct__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::_Rb_tree_const_iterator<CP::SystematicVariation>*)
   {
      ::_Rb_tree_const_iterator<CP::SystematicVariation> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::_Rb_tree_const_iterator<CP::SystematicVariation>));
      static ::ROOT::TGenericClassInfo 
         instance("_Rb_tree_const_iterator<CP::SystematicVariation>", "set", 228,
                  typeid(::_Rb_tree_const_iterator<CP::SystematicVariation>), DefineBehavior(ptr, ptr),
                  &_Rb_tree_const_iteratorlECPcLcLSystematicVariationgR_Dictionary, isa_proxy, 0,
                  sizeof(::_Rb_tree_const_iterator<CP::SystematicVariation>) );
      instance.SetNew(&new__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR);
      instance.SetNewArray(&newArray__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR);
      instance.SetDelete(&delete__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR);
      instance.SetDeleteArray(&deleteArray__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR);
      instance.SetDestructor(&destruct__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR);

      ROOT::AddClassAlternate("_Rb_tree_const_iterator<CP::SystematicVariation>","CP::SystematicSet::iterator");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::_Rb_tree_const_iterator<CP::SystematicVariation>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *_Rb_tree_const_iteratorlECPcLcLSystematicVariationgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::_Rb_tree_const_iterator<CP::SystematicVariation>*)0x0)->GetClass();
      _Rb_tree_const_iteratorlECPcLcLSystematicVariationgR_TClassManip(theClass);
   return theClass;
   }

   static void _Rb_tree_const_iteratorlECPcLcLSystematicVariationgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_SysStreamTestcLcLStreamTestWrapper(void *p = 0);
   static void *newArray_SysStreamTestcLcLStreamTestWrapper(Long_t size, void *p);
   static void delete_SysStreamTestcLcLStreamTestWrapper(void *p);
   static void deleteArray_SysStreamTestcLcLStreamTestWrapper(void *p);
   static void destruct_SysStreamTestcLcLStreamTestWrapper(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SysStreamTest::StreamTestWrapper*)
   {
      ::SysStreamTest::StreamTestWrapper *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SysStreamTest::StreamTestWrapper >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SysStreamTest::StreamTestWrapper", ::SysStreamTest::StreamTestWrapper::Class_Version(), "PATInterfaces/StreamTestWrapper.h", 16,
                  typeid(::SysStreamTest::StreamTestWrapper), DefineBehavior(ptr, ptr),
                  &::SysStreamTest::StreamTestWrapper::Dictionary, isa_proxy, 4,
                  sizeof(::SysStreamTest::StreamTestWrapper) );
      instance.SetNew(&new_SysStreamTestcLcLStreamTestWrapper);
      instance.SetNewArray(&newArray_SysStreamTestcLcLStreamTestWrapper);
      instance.SetDelete(&delete_SysStreamTestcLcLStreamTestWrapper);
      instance.SetDeleteArray(&deleteArray_SysStreamTestcLcLStreamTestWrapper);
      instance.SetDestructor(&destruct_SysStreamTestcLcLStreamTestWrapper);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SysStreamTest::StreamTestWrapper*)
   {
      return GenerateInitInstanceLocal((::SysStreamTest::StreamTestWrapper*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SysStreamTest::StreamTestWrapper*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace SysStreamTest {
//______________________________________________________________________________
atomic_TClass_ptr StreamTestWrapper::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *StreamTestWrapper::Class_Name()
{
   return "SysStreamTest::StreamTestWrapper";
}

//______________________________________________________________________________
const char *StreamTestWrapper::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SysStreamTest::StreamTestWrapper*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int StreamTestWrapper::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SysStreamTest::StreamTestWrapper*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *StreamTestWrapper::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SysStreamTest::StreamTestWrapper*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *StreamTestWrapper::Class()
{
   if (!fgIsA) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SysStreamTest::StreamTestWrapper*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace SysStreamTest
namespace ROOT {
   // Wrappers around operator new
   static void *new_CPcLcLSystematicVariation(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::CP::SystematicVariation : new ::CP::SystematicVariation;
   }
   static void *newArray_CPcLcLSystematicVariation(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::CP::SystematicVariation[nElements] : new ::CP::SystematicVariation[nElements];
   }
   // Wrapper around operator delete
   static void delete_CPcLcLSystematicVariation(void *p) {
      delete ((::CP::SystematicVariation*)p);
   }
   static void deleteArray_CPcLcLSystematicVariation(void *p) {
      delete [] ((::CP::SystematicVariation*)p);
   }
   static void destruct_CPcLcLSystematicVariation(void *p) {
      typedef ::CP::SystematicVariation current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CP::SystematicVariation

namespace ROOT {
   // Wrappers around operator new
   static void *new_CPcLcLSystematicSet(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::CP::SystematicSet : new ::CP::SystematicSet;
   }
   static void *newArray_CPcLcLSystematicSet(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::CP::SystematicSet[nElements] : new ::CP::SystematicSet[nElements];
   }
   // Wrapper around operator delete
   static void delete_CPcLcLSystematicSet(void *p) {
      delete ((::CP::SystematicSet*)p);
   }
   static void deleteArray_CPcLcLSystematicSet(void *p) {
      delete [] ((::CP::SystematicSet*)p);
   }
   static void destruct_CPcLcLSystematicSet(void *p) {
      typedef ::CP::SystematicSet current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CP::SystematicSet

namespace ROOT {
   // Wrappers around operator new
   static void *new__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::_Rb_tree_const_iterator<CP::SystematicVariation> : new ::_Rb_tree_const_iterator<CP::SystematicVariation>;
   }
   static void *newArray__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::_Rb_tree_const_iterator<CP::SystematicVariation>[nElements] : new ::_Rb_tree_const_iterator<CP::SystematicVariation>[nElements];
   }
   // Wrapper around operator delete
   static void delete__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(void *p) {
      delete ((::_Rb_tree_const_iterator<CP::SystematicVariation>*)p);
   }
   static void deleteArray__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(void *p) {
      delete [] ((::_Rb_tree_const_iterator<CP::SystematicVariation>*)p);
   }
   static void destruct__Rb_tree_const_iteratorlECPcLcLSystematicVariationgR(void *p) {
      typedef ::_Rb_tree_const_iterator<CP::SystematicVariation> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::_Rb_tree_const_iterator<CP::SystematicVariation>

namespace SysStreamTest {
//______________________________________________________________________________
void StreamTestWrapper::Streamer(TBuffer &R__b)
{
   // Stream an object of class SysStreamTest::StreamTestWrapper.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SysStreamTest::StreamTestWrapper::Class(),this);
   } else {
      R__b.WriteClassBuffer(SysStreamTest::StreamTestWrapper::Class(),this);
   }
}

} // namespace SysStreamTest
namespace ROOT {
   // Wrappers around operator new
   static void *new_SysStreamTestcLcLStreamTestWrapper(void *p) {
      return  p ? new(p) ::SysStreamTest::StreamTestWrapper : new ::SysStreamTest::StreamTestWrapper;
   }
   static void *newArray_SysStreamTestcLcLStreamTestWrapper(Long_t nElements, void *p) {
      return p ? new(p) ::SysStreamTest::StreamTestWrapper[nElements] : new ::SysStreamTest::StreamTestWrapper[nElements];
   }
   // Wrapper around operator delete
   static void delete_SysStreamTestcLcLStreamTestWrapper(void *p) {
      delete ((::SysStreamTest::StreamTestWrapper*)p);
   }
   static void deleteArray_SysStreamTestcLcLStreamTestWrapper(void *p) {
      delete [] ((::SysStreamTest::StreamTestWrapper*)p);
   }
   static void destruct_SysStreamTestcLcLStreamTestWrapper(void *p) {
      typedef ::SysStreamTest::StreamTestWrapper current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SysStreamTest::StreamTestWrapper

namespace ROOT {
   static TClass *vectorlECPcLcLSystematicSetgR_Dictionary();
   static void vectorlECPcLcLSystematicSetgR_TClassManip(TClass*);
   static void *new_vectorlECPcLcLSystematicSetgR(void *p = 0);
   static void *newArray_vectorlECPcLcLSystematicSetgR(Long_t size, void *p);
   static void delete_vectorlECPcLcLSystematicSetgR(void *p);
   static void deleteArray_vectorlECPcLcLSystematicSetgR(void *p);
   static void destruct_vectorlECPcLcLSystematicSetgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<CP::SystematicSet>*)
   {
      vector<CP::SystematicSet> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<CP::SystematicSet>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<CP::SystematicSet>", -2, "vector", 210,
                  typeid(vector<CP::SystematicSet>), DefineBehavior(ptr, ptr),
                  &vectorlECPcLcLSystematicSetgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<CP::SystematicSet>) );
      instance.SetNew(&new_vectorlECPcLcLSystematicSetgR);
      instance.SetNewArray(&newArray_vectorlECPcLcLSystematicSetgR);
      instance.SetDelete(&delete_vectorlECPcLcLSystematicSetgR);
      instance.SetDeleteArray(&deleteArray_vectorlECPcLcLSystematicSetgR);
      instance.SetDestructor(&destruct_vectorlECPcLcLSystematicSetgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<CP::SystematicSet> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<CP::SystematicSet>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlECPcLcLSystematicSetgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<CP::SystematicSet>*)0x0)->GetClass();
      vectorlECPcLcLSystematicSetgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlECPcLcLSystematicSetgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlECPcLcLSystematicSetgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<CP::SystematicSet> : new vector<CP::SystematicSet>;
   }
   static void *newArray_vectorlECPcLcLSystematicSetgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<CP::SystematicSet>[nElements] : new vector<CP::SystematicSet>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlECPcLcLSystematicSetgR(void *p) {
      delete ((vector<CP::SystematicSet>*)p);
   }
   static void deleteArray_vectorlECPcLcLSystematicSetgR(void *p) {
      delete [] ((vector<CP::SystematicSet>*)p);
   }
   static void destruct_vectorlECPcLcLSystematicSetgR(void *p) {
      typedef vector<CP::SystematicSet> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<CP::SystematicSet>

namespace ROOT {
   static TClass *setlECPcLcLSystematicVariationgR_Dictionary();
   static void setlECPcLcLSystematicVariationgR_TClassManip(TClass*);
   static void *new_setlECPcLcLSystematicVariationgR(void *p = 0);
   static void *newArray_setlECPcLcLSystematicVariationgR(Long_t size, void *p);
   static void delete_setlECPcLcLSystematicVariationgR(void *p);
   static void deleteArray_setlECPcLcLSystematicVariationgR(void *p);
   static void destruct_setlECPcLcLSystematicVariationgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const set<CP::SystematicVariation>*)
   {
      set<CP::SystematicVariation> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(set<CP::SystematicVariation>));
      static ::ROOT::TGenericClassInfo 
         instance("set<CP::SystematicVariation>", -2, "set", 90,
                  typeid(set<CP::SystematicVariation>), DefineBehavior(ptr, ptr),
                  &setlECPcLcLSystematicVariationgR_Dictionary, isa_proxy, 4,
                  sizeof(set<CP::SystematicVariation>) );
      instance.SetNew(&new_setlECPcLcLSystematicVariationgR);
      instance.SetNewArray(&newArray_setlECPcLcLSystematicVariationgR);
      instance.SetDelete(&delete_setlECPcLcLSystematicVariationgR);
      instance.SetDeleteArray(&deleteArray_setlECPcLcLSystematicVariationgR);
      instance.SetDestructor(&destruct_setlECPcLcLSystematicVariationgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Insert< set<CP::SystematicVariation> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const set<CP::SystematicVariation>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *setlECPcLcLSystematicVariationgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const set<CP::SystematicVariation>*)0x0)->GetClass();
      setlECPcLcLSystematicVariationgR_TClassManip(theClass);
   return theClass;
   }

   static void setlECPcLcLSystematicVariationgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_setlECPcLcLSystematicVariationgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) set<CP::SystematicVariation> : new set<CP::SystematicVariation>;
   }
   static void *newArray_setlECPcLcLSystematicVariationgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) set<CP::SystematicVariation>[nElements] : new set<CP::SystematicVariation>[nElements];
   }
   // Wrapper around operator delete
   static void delete_setlECPcLcLSystematicVariationgR(void *p) {
      delete ((set<CP::SystematicVariation>*)p);
   }
   static void deleteArray_setlECPcLcLSystematicVariationgR(void *p) {
      delete [] ((set<CP::SystematicVariation>*)p);
   }
   static void destruct_setlECPcLcLSystematicVariationgR(void *p) {
      typedef set<CP::SystematicVariation> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class set<CP::SystematicVariation>

namespace {
  void TriggerDictionaryInitialization_PATInterfacesCINT_Impl() {
    static const char* headers[] = {
"PATInterfaces/SystematicVariation.h",
"PATInterfaces/SystematicSet.h",
"PATInterfaces/SystematicsUtil.h",
"PATInterfaces/StreamTestWrapper.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PATInterfaces/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PATInterfaces/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace CP{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PATInterfaces/Root/LinkDef.h")))  SystematicVariation;}
namespace std{template <typename _Tp> struct __attribute__((annotate("$clingAutoload$string")))  less;
}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace CP{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PATInterfaces/Root/LinkDef.h")))  SystematicSet;}
namespace std{template <typename _Tp> struct __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PATInterfaces/Root/LinkDef.h")))  _Rb_tree_const_iterator;
}
namespace SysStreamTest{class __attribute__((annotate("$clingAutoload$/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/PATInterfaces/Root/LinkDef.h")))  StreamTestWrapper;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "PATInterfaces"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicsUtil.h"
#include "PATInterfaces/StreamTestWrapper.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"CP::SystematicSet", payloadCode, "@",
"CP::SystematicSet::iterator", payloadCode, "@",
"CP::SystematicVariation", payloadCode, "@",
"SysStreamTest::StreamTestWrapper", payloadCode, "@",
"_Rb_tree_const_iterator<CP::SystematicVariation>", payloadCode, "@",
"set<CP::SystematicVariation>", payloadCode, "@",
"vector<CP::SystematicSet>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("PATInterfacesCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_PATInterfacesCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_PATInterfacesCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_PATInterfacesCINT() {
  TriggerDictionaryInitialization_PATInterfacesCINT_Impl();
}

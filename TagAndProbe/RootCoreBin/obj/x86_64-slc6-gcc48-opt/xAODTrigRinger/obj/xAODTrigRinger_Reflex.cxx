// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrigRingerdIobjdIxAODTrigRinger_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigRinger/xAODTrigRinger/xAODTrigRingerDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTrigRingerRings_v1_Dictionary();
   static void xAODcLcLTrigRingerRings_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigRingerRings_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigRingerRings_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigRingerRings_v1(void *p);
   static void deleteArray_xAODcLcLTrigRingerRings_v1(void *p);
   static void destruct_xAODcLcLTrigRingerRings_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigRingerRings_v1*)
   {
      ::xAOD::TrigRingerRings_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigRingerRings_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigRingerRings_v1", "xAODTrigRinger/versions/TrigRingerRings_v1.h", 16,
                  typeid(::xAOD::TrigRingerRings_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigRingerRings_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigRingerRings_v1) );
      instance.SetNew(&new_xAODcLcLTrigRingerRings_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigRingerRings_v1);
      instance.SetDelete(&delete_xAODcLcLTrigRingerRings_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigRingerRings_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigRingerRings_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigRingerRings_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigRingerRings_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigRingerRings_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigRingerRings_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigRingerRings_v1*)0x0)->GetClass();
      xAODcLcLTrigRingerRings_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigRingerRings_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigRingerRings_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigRingerRings_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigRingerRings_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigRingerRings_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigRingerRings_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigRingerRings_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigRingerRings_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigRingerRings_v1>*)
   {
      ::DataVector<xAOD::TrigRingerRings_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigRingerRings_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigRingerRings_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigRingerRings_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigRingerRings_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigRingerRings_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigRingerRings_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigRingerRings_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigRingerRings_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigRingerRings_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigRingerRings_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigRingerRings_v1>","xAOD::TrigRingerRingsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigRingerRings_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigRingerRings_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigRingerRings_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigRingerRings_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigRingerRings_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigRingerRings_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigRingerRings_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","AEF63C18-1B19-4861-A909-FCAF11CFBFCE");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigRingerRingsAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigRingerRingsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigRingerRingsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigRingerRingsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigRingerRingsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigRingerRingsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigRingerRingsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigRingerRingsAuxContainer_v1*)
   {
      ::xAOD::TrigRingerRingsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigRingerRingsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigRingerRingsAuxContainer_v1", "xAODTrigRinger/versions/TrigRingerRingsAuxContainer_v1.h", 20,
                  typeid(::xAOD::TrigRingerRingsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigRingerRingsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigRingerRingsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigRingerRingsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigRingerRingsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigRingerRingsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigRingerRingsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigRingerRingsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigRingerRingsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigRingerRingsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigRingerRingsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigRingerRingsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigRingerRingsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigRingerRingsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigRingerRingsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","4016ADBB-830C-4523-97E2-D5FED349D98D");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrigRingerRings_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrigRingerRings_v1> >","ElementLink<xAOD::TrigRingerRingsContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigRNNOutput_v1_Dictionary();
   static void xAODcLcLTrigRNNOutput_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigRNNOutput_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigRNNOutput_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigRNNOutput_v1(void *p);
   static void deleteArray_xAODcLcLTrigRNNOutput_v1(void *p);
   static void destruct_xAODcLcLTrigRNNOutput_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigRNNOutput_v1*)
   {
      ::xAOD::TrigRNNOutput_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigRNNOutput_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigRNNOutput_v1", "xAODTrigRinger/versions/TrigRNNOutput_v1.h", 16,
                  typeid(::xAOD::TrigRNNOutput_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigRNNOutput_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigRNNOutput_v1) );
      instance.SetNew(&new_xAODcLcLTrigRNNOutput_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigRNNOutput_v1);
      instance.SetDelete(&delete_xAODcLcLTrigRNNOutput_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigRNNOutput_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigRNNOutput_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigRNNOutput_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigRNNOutput_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigRNNOutput_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigRNNOutput_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigRNNOutput_v1*)0x0)->GetClass();
      xAODcLcLTrigRNNOutput_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigRNNOutput_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigRNNOutput_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigRNNOutput_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigRNNOutput_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigRNNOutput_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigRNNOutput_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigRNNOutput_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigRNNOutput_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigRNNOutput_v1>*)
   {
      ::DataVector<xAOD::TrigRNNOutput_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigRNNOutput_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigRNNOutput_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigRNNOutput_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigRNNOutput_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigRNNOutput_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigRNNOutput_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigRNNOutput_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigRNNOutput_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigRNNOutput_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigRNNOutput_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigRNNOutput_v1>","xAOD::TrigRNNOutputContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigRNNOutput_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigRNNOutput_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigRNNOutput_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigRNNOutput_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigRNNOutput_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigRNNOutput_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigRNNOutput_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","E96CE49C-6A88-47A0-8DA0-F1D42E0813C8");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigRNNOutputAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigRNNOutputAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigRNNOutputAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigRNNOutputAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigRNNOutputAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigRNNOutputAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigRNNOutputAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigRNNOutputAuxContainer_v1*)
   {
      ::xAOD::TrigRNNOutputAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigRNNOutputAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigRNNOutputAuxContainer_v1", "xAODTrigRinger/versions/TrigRNNOutputAuxContainer_v1.h", 20,
                  typeid(::xAOD::TrigRNNOutputAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigRNNOutputAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigRNNOutputAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigRNNOutputAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigRNNOutputAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigRNNOutputAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigRNNOutputAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigRNNOutputAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigRNNOutputAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigRNNOutputAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigRNNOutputAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigRNNOutputAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigRNNOutputAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigRNNOutputAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigRNNOutputAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","5090A739-C400-4322-9F91-C49441C9141F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR_Dictionary();
   static void ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR_TClassManip(TClass*);
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(void *p = 0);
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(Long_t size, void *p);
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(void *p);
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(void *p);
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >*)
   {
      ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >", "AthLinks/ElementLink.h", 39,
                  typeid(::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >), DefineBehavior(ptr, ptr),
                  &ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >) );
      instance.SetNew(&new_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR);
      instance.SetNewArray(&newArray_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR);
      instance.SetDelete(&delete_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR);
      instance.SetDestructor(&destruct_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR);

      ROOT::AddClassAlternate("ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >","ElementLink<xAOD::TrigRNNOutputContainer_v1>");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >*)
   {
      return GenerateInitInstanceLocal((::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >*)0x0)->GetClass();
      ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigRingerRings_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigRingerRings_v1 : new ::xAOD::TrigRingerRings_v1;
   }
   static void *newArray_xAODcLcLTrigRingerRings_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigRingerRings_v1[nElements] : new ::xAOD::TrigRingerRings_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigRingerRings_v1(void *p) {
      delete ((::xAOD::TrigRingerRings_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigRingerRings_v1(void *p) {
      delete [] ((::xAOD::TrigRingerRings_v1*)p);
   }
   static void destruct_xAODcLcLTrigRingerRings_v1(void *p) {
      typedef ::xAOD::TrigRingerRings_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigRingerRings_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigRingerRings_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigRingerRings_v1> : new ::DataVector<xAOD::TrigRingerRings_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigRingerRings_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigRingerRings_v1>[nElements] : new ::DataVector<xAOD::TrigRingerRings_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigRingerRings_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigRingerRings_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigRingerRings_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigRingerRings_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigRingerRings_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigRingerRings_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigRingerRings_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigRingerRingsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigRingerRingsAuxContainer_v1 : new ::xAOD::TrigRingerRingsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigRingerRingsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigRingerRingsAuxContainer_v1[nElements] : new ::xAOD::TrigRingerRingsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigRingerRingsAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigRingerRingsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigRingerRingsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigRingerRingsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigRingerRingsAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigRingerRingsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigRingerRingsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> > : new ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrigRingerRings_v1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigRNNOutput_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigRNNOutput_v1 : new ::xAOD::TrigRNNOutput_v1;
   }
   static void *newArray_xAODcLcLTrigRNNOutput_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigRNNOutput_v1[nElements] : new ::xAOD::TrigRNNOutput_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigRNNOutput_v1(void *p) {
      delete ((::xAOD::TrigRNNOutput_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigRNNOutput_v1(void *p) {
      delete [] ((::xAOD::TrigRNNOutput_v1*)p);
   }
   static void destruct_xAODcLcLTrigRNNOutput_v1(void *p) {
      typedef ::xAOD::TrigRNNOutput_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigRNNOutput_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigRNNOutput_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigRNNOutput_v1> : new ::DataVector<xAOD::TrigRNNOutput_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigRNNOutput_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigRNNOutput_v1>[nElements] : new ::DataVector<xAOD::TrigRNNOutput_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigRNNOutput_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigRNNOutput_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigRNNOutput_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigRNNOutput_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigRNNOutput_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigRNNOutput_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigRNNOutput_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigRNNOutputAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigRNNOutputAuxContainer_v1 : new ::xAOD::TrigRNNOutputAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigRNNOutputAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigRNNOutputAuxContainer_v1[nElements] : new ::xAOD::TrigRNNOutputAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigRNNOutputAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigRNNOutputAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigRNNOutputAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigRNNOutputAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigRNNOutputAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigRNNOutputAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigRNNOutputAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(void *p) {
      return  p ? new(p) ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > : new ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >;
   }
   static void *newArray_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(Long_t nElements, void *p) {
      return p ? new(p) ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >[nElements] : new ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(void *p) {
      delete ((::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >*)p);
   }
   static void deleteArray_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(void *p) {
      delete [] ((::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >*)p);
   }
   static void destruct_ElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgR(void *p) {
      typedef ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >

namespace ROOT {
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR_Dictionary();
   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(void *p);
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >*)
   {
      vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >", -2, "vector", 210,
                  typeid(vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >), DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >) );
      instance.SetNew(&new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >*)0x0)->GetClass();
      vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > > : new vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >;
   }
   static void *newArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >[nElements] : new vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(void *p) {
      delete ((vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >*)p);
   }
   static void deleteArray_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(void *p) {
      delete [] ((vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >*)p);
   }
   static void destruct_vectorlEvectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgRsPgR(void *p) {
      typedef vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > : new vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigRingerRings_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >

namespace ROOT {
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR_Dictionary();
   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(void *p);
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(void *p);
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >*)
   {
      vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >", -2, "vector", 210,
                  typeid(vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >), DefineBehavior(ptr, ptr),
                  &vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >) );
      instance.SetNew(&new_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >*)0x0)->GetClass();
      vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > : new vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >;
   }
   static void *newArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >[nElements] : new vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(void *p) {
      delete ((vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >*)p);
   }
   static void deleteArray_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(void *p) {
      delete [] ((vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >*)p);
   }
   static void destruct_vectorlEElementLinklEDataVectorlExAODcLcLTrigRNNOutput_v1gRsPgRsPgR(void *p) {
      typedef vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >

namespace {
  void TriggerDictionaryInitialization_xAODTrigRinger_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigRinger/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigRinger",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigRinger/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigRinger/TrigRingerRingsContainer.h")))  TrigRingerRings_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@4016ADBB-830C-4523-97E2-D5FED349D98D)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigRinger/versions/TrigRingerRingsAuxContainer_v1.h")))  TrigRingerRingsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigRinger/TrigRNNOutputContainer.h")))  TrigRNNOutput_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@5090A739-C400-4322-9F91-C49441C9141F)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigRinger/versions/TrigRNNOutputAuxContainer_v1.h")))  TrigRNNOutputAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigRinger"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
#ifndef XAODTRIGRINGER_XAODTRIGRINGERDICT_H
#define XAODTRIGRINGER_XAODTRIGRINGERDICT_H

// STL include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODTrigRinger/TrigRingerRingsContainer.h"
#include "xAODTrigRinger/TrigRNNOutputContainer.h"
#include "xAODTrigRinger/versions/TrigRingerRings_v1.h"
#include "xAODTrigRinger/versions/TrigRingerRingsContainer_v1.h"
#include "xAODTrigRinger/versions/TrigRingerRingsAuxContainer_v1.h"
#include "xAODTrigRinger/versions/TrigRNNOutput_v1.h"
#include "xAODTrigRinger/versions/TrigRNNOutputContainer_v1.h"
#include "xAODTrigRinger/versions/TrigRNNOutputAuxContainer_v1.h"

namespace{
  struct GCCXML_DUMMY_INSTANTIATION_XAODRINGERRINGS {

    xAOD::TrigRingerRingsContainer_v1 c3;
    ElementLink< xAOD::TrigRingerRingsContainer_v1 > a1;
    std::vector< ElementLink< xAOD::TrigRingerRingsContainer_v1 > > a3;
    std::vector< std::vector< ElementLink< xAOD::TrigRingerRingsContainer_v1 > > > a4;
      
    xAOD::TrigRNNOutputContainer_v1 c4;
    ElementLink< xAOD::TrigRNNOutputContainer_v1 > b1;
    std::vector< ElementLink< xAOD::TrigRNNOutputContainer_v1 > > b3;
    std::vector< std::vector< ElementLink< xAOD::TrigRNNOutputContainer_v1 > > > b4;
  
  };
}

#endif // 

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataVector<xAOD::TrigRNNOutput_v1>", payloadCode, "@",
"DataVector<xAOD::TrigRingerRings_v1>", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrigRNNOutput_v1> >", payloadCode, "@",
"ElementLink<DataVector<xAOD::TrigRingerRings_v1> >", payloadCode, "@",
"ElementLink<xAOD::TrigRNNOutputContainer_v1>", payloadCode, "@",
"ElementLink<xAOD::TrigRingerRingsContainer_v1>", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > >", payloadCode, "@",
"vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > >", payloadCode, "@",
"vector<ElementLink<xAOD::TrigRNNOutputContainer_v1> >", payloadCode, "@",
"vector<ElementLink<xAOD::TrigRingerRingsContainer_v1> >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrigRNNOutputContainer_v1> > >", payloadCode, "@",
"vector<std::vector<ElementLink<xAOD::TrigRingerRingsContainer_v1> > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrigRNNOutput_v1> > > >", payloadCode, "@",
"vector<vector<ElementLink<DataVector<xAOD::TrigRingerRings_v1> > > >", payloadCode, "@",
"xAOD::TrigRNNOutputAuxContainer_v1", payloadCode, "@",
"xAOD::TrigRNNOutputContainer_v1", payloadCode, "@",
"xAOD::TrigRNNOutput_v1", payloadCode, "@",
"xAOD::TrigRingerRingsAuxContainer_v1", payloadCode, "@",
"xAOD::TrigRingerRingsContainer_v1", payloadCode, "@",
"xAOD::TrigRingerRings_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigRinger_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigRinger_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigRinger_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigRinger_Reflex() {
  TriggerDictionaryInitialization_xAODTrigRinger_Reflex_Impl();
}

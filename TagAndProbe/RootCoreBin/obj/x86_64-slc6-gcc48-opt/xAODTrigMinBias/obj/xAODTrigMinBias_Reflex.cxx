// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODTrigMinBiasdIobjdIxAODTrigMinBias_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigMinBias/xAODTrigMinBias/xAODTrigMinBiasDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLTrigSpacePointCounts_v1_Dictionary();
   static void xAODcLcLTrigSpacePointCounts_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigSpacePointCounts_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigSpacePointCounts_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigSpacePointCounts_v1(void *p);
   static void deleteArray_xAODcLcLTrigSpacePointCounts_v1(void *p);
   static void destruct_xAODcLcLTrigSpacePointCounts_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigSpacePointCounts_v1*)
   {
      ::xAOD::TrigSpacePointCounts_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigSpacePointCounts_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigSpacePointCounts_v1", "xAODTrigMinBias/versions/TrigSpacePointCounts_v1.h", 8,
                  typeid(::xAOD::TrigSpacePointCounts_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigSpacePointCounts_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigSpacePointCounts_v1) );
      instance.SetNew(&new_xAODcLcLTrigSpacePointCounts_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigSpacePointCounts_v1);
      instance.SetDelete(&delete_xAODcLcLTrigSpacePointCounts_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigSpacePointCounts_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigSpacePointCounts_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigSpacePointCounts_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigSpacePointCounts_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigSpacePointCounts_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigSpacePointCounts_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigSpacePointCounts_v1*)0x0)->GetClass();
      xAODcLcLTrigSpacePointCounts_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigSpacePointCounts_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigSpacePointCounts_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigSpacePointCounts_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigSpacePointCounts_v1>*)
   {
      ::DataVector<xAOD::TrigSpacePointCounts_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigSpacePointCounts_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigSpacePointCounts_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigSpacePointCounts_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigSpacePointCounts_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigSpacePointCounts_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigSpacePointCounts_v1>","xAOD::TrigSpacePointCountsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigSpacePointCounts_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigSpacePointCounts_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigSpacePointCounts_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigSpacePointCounts_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigSpacePointCounts_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigSpacePointCounts_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigSpacePointCounts_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","519EBE61-C135-46C7-AD7D-C661DC6FC4B5");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigSpacePointCountsAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigSpacePointCountsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigSpacePointCountsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigSpacePointCountsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigSpacePointCountsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigSpacePointCountsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigSpacePointCountsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigSpacePointCountsAuxContainer_v1*)
   {
      ::xAOD::TrigSpacePointCountsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigSpacePointCountsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigSpacePointCountsAuxContainer_v1", "xAODTrigMinBias/versions/TrigSpacePointCountsAuxContainer_v1.h", 10,
                  typeid(::xAOD::TrigSpacePointCountsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigSpacePointCountsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigSpacePointCountsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigSpacePointCountsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigSpacePointCountsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigSpacePointCountsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigSpacePointCountsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigSpacePointCountsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigSpacePointCountsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigSpacePointCountsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigSpacePointCountsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigSpacePointCountsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigSpacePointCountsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigSpacePointCountsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigSpacePointCountsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C682FD32-73CC-4773-ACBF-30B2657C5991");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigT2MbtsBits_v1_Dictionary();
   static void xAODcLcLTrigT2MbtsBits_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigT2MbtsBits_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigT2MbtsBits_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigT2MbtsBits_v1(void *p);
   static void deleteArray_xAODcLcLTrigT2MbtsBits_v1(void *p);
   static void destruct_xAODcLcLTrigT2MbtsBits_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigT2MbtsBits_v1*)
   {
      ::xAOD::TrigT2MbtsBits_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigT2MbtsBits_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigT2MbtsBits_v1", "xAODTrigMinBias/versions/TrigT2MbtsBits_v1.h", 10,
                  typeid(::xAOD::TrigT2MbtsBits_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigT2MbtsBits_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigT2MbtsBits_v1) );
      instance.SetNew(&new_xAODcLcLTrigT2MbtsBits_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigT2MbtsBits_v1);
      instance.SetDelete(&delete_xAODcLcLTrigT2MbtsBits_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigT2MbtsBits_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigT2MbtsBits_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigT2MbtsBits_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigT2MbtsBits_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigT2MbtsBits_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigT2MbtsBits_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigT2MbtsBits_v1*)0x0)->GetClass();
      xAODcLcLTrigT2MbtsBits_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigT2MbtsBits_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigT2MbtsBits_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigT2MbtsBits_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigT2MbtsBits_v1>*)
   {
      ::DataVector<xAOD::TrigT2MbtsBits_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigT2MbtsBits_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigT2MbtsBits_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigT2MbtsBits_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigT2MbtsBits_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigT2MbtsBits_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigT2MbtsBits_v1>","xAOD::TrigT2MbtsBitsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigT2MbtsBits_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigT2MbtsBits_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigT2MbtsBits_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigT2MbtsBits_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigT2MbtsBits_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigT2MbtsBits_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigT2MbtsBits_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","FF96BF06-1206-11E4-9605-02163E00A892");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigT2MbtsBitsAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigT2MbtsBitsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigT2MbtsBitsAuxContainer_v1*)
   {
      ::xAOD::TrigT2MbtsBitsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigT2MbtsBitsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigT2MbtsBitsAuxContainer_v1", "xAODTrigMinBias/versions/TrigT2MbtsBitsAuxContainer_v1.h", 10,
                  typeid(::xAOD::TrigT2MbtsBitsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigT2MbtsBitsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigT2MbtsBitsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigT2MbtsBitsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigT2MbtsBitsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigT2MbtsBitsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigT2MbtsBitsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigT2MbtsBitsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigT2MbtsBitsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigT2MbtsBitsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigT2MbtsBitsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigT2MbtsBitsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigT2MbtsBitsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigT2MbtsBitsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigT2MbtsBitsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","0055818E-1207-11E4-9C7B-02163E00A892");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigHisto2D_v1_Dictionary();
   static void xAODcLcLTrigHisto2D_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigHisto2D_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigHisto2D_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigHisto2D_v1(void *p);
   static void deleteArray_xAODcLcLTrigHisto2D_v1(void *p);
   static void destruct_xAODcLcLTrigHisto2D_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigHisto2D_v1*)
   {
      ::xAOD::TrigHisto2D_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigHisto2D_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigHisto2D_v1", "xAODTrigMinBias/versions/TrigHisto2D_v1.h", 19,
                  typeid(::xAOD::TrigHisto2D_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigHisto2D_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigHisto2D_v1) );
      instance.SetNew(&new_xAODcLcLTrigHisto2D_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigHisto2D_v1);
      instance.SetDelete(&delete_xAODcLcLTrigHisto2D_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigHisto2D_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigHisto2D_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigHisto2D_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigHisto2D_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigHisto2D_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigHisto2D_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigHisto2D_v1*)0x0)->GetClass();
      xAODcLcLTrigHisto2D_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigHisto2D_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigHisto2D_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigHisto2D_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigHisto2D_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigHisto2D_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigHisto2D_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigHisto2D_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigHisto2D_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigHisto2D_v1>*)
   {
      ::DataVector<xAOD::TrigHisto2D_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigHisto2D_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigHisto2D_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigHisto2D_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigHisto2D_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigHisto2D_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigHisto2D_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigHisto2D_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigHisto2D_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigHisto2D_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigHisto2D_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigHisto2D_v1>","xAOD::TrigHisto2DContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigHisto2D_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigHisto2D_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigHisto2D_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigHisto2D_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigHisto2D_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigHisto2D_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigHisto2D_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","0116E8A6-1207-11E4-8724-02163E00A892");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigHisto2DAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigHisto2DAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigHisto2DAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigHisto2DAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigHisto2DAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigHisto2DAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigHisto2DAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigHisto2DAuxContainer_v1*)
   {
      ::xAOD::TrigHisto2DAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigHisto2DAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigHisto2DAuxContainer_v1", "xAODTrigMinBias/versions/TrigHisto2DAuxContainer_v1.h", 10,
                  typeid(::xAOD::TrigHisto2DAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigHisto2DAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigHisto2DAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigHisto2DAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigHisto2DAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigHisto2DAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigHisto2DAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigHisto2DAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigHisto2DAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigHisto2DAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigHisto2DAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigHisto2DAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigHisto2DAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigHisto2DAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigHisto2DAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","05B28A1E-1207-11E4-84E2-02163E00A892");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigTrackCounts_v1_Dictionary();
   static void xAODcLcLTrigTrackCounts_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigTrackCounts_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigTrackCounts_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigTrackCounts_v1(void *p);
   static void deleteArray_xAODcLcLTrigTrackCounts_v1(void *p);
   static void destruct_xAODcLcLTrigTrackCounts_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigTrackCounts_v1*)
   {
      ::xAOD::TrigTrackCounts_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigTrackCounts_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigTrackCounts_v1", "xAODTrigMinBias/versions/TrigTrackCounts_v1.h", 8,
                  typeid(::xAOD::TrigTrackCounts_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigTrackCounts_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigTrackCounts_v1) );
      instance.SetNew(&new_xAODcLcLTrigTrackCounts_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigTrackCounts_v1);
      instance.SetDelete(&delete_xAODcLcLTrigTrackCounts_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigTrackCounts_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigTrackCounts_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigTrackCounts_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigTrackCounts_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigTrackCounts_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigTrackCounts_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigTrackCounts_v1*)0x0)->GetClass();
      xAODcLcLTrigTrackCounts_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigTrackCounts_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigTrackCounts_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigTrackCounts_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigTrackCounts_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigTrackCounts_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigTrackCounts_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigTrackCounts_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigTrackCounts_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigTrackCounts_v1>*)
   {
      ::DataVector<xAOD::TrigTrackCounts_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigTrackCounts_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigTrackCounts_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigTrackCounts_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigTrackCounts_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigTrackCounts_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigTrackCounts_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigTrackCounts_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigTrackCounts_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigTrackCounts_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigTrackCounts_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigTrackCounts_v1>","xAOD::TrigTrackCountsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigTrackCounts_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigTrackCounts_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigTrackCounts_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigTrackCounts_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigTrackCounts_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigTrackCounts_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigTrackCounts_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","0650DED0-1207-11E4-9CD1-02163E00A892");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigTrackCountsAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigTrackCountsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigTrackCountsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigTrackCountsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigTrackCountsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigTrackCountsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigTrackCountsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigTrackCountsAuxContainer_v1*)
   {
      ::xAOD::TrigTrackCountsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigTrackCountsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigTrackCountsAuxContainer_v1", "xAODTrigMinBias/versions/TrigTrackCountsAuxContainer_v1.h", 10,
                  typeid(::xAOD::TrigTrackCountsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigTrackCountsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigTrackCountsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigTrackCountsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigTrackCountsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigTrackCountsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigTrackCountsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigTrackCountsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigTrackCountsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigTrackCountsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigTrackCountsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigTrackCountsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigTrackCountsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigTrackCountsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigTrackCountsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","0701BEB0-1207-11E4-9D08-02163E00A892");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigVertexCounts_v1_Dictionary();
   static void xAODcLcLTrigVertexCounts_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigVertexCounts_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigVertexCounts_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigVertexCounts_v1(void *p);
   static void deleteArray_xAODcLcLTrigVertexCounts_v1(void *p);
   static void destruct_xAODcLcLTrigVertexCounts_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigVertexCounts_v1*)
   {
      ::xAOD::TrigVertexCounts_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigVertexCounts_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigVertexCounts_v1", "xAODTrigMinBias/versions/TrigVertexCounts_v1.h", 8,
                  typeid(::xAOD::TrigVertexCounts_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigVertexCounts_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigVertexCounts_v1) );
      instance.SetNew(&new_xAODcLcLTrigVertexCounts_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigVertexCounts_v1);
      instance.SetDelete(&delete_xAODcLcLTrigVertexCounts_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigVertexCounts_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigVertexCounts_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigVertexCounts_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigVertexCounts_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigVertexCounts_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigVertexCounts_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigVertexCounts_v1*)0x0)->GetClass();
      xAODcLcLTrigVertexCounts_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigVertexCounts_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigVertexCounts_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigVertexCounts_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigVertexCounts_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigVertexCounts_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigVertexCounts_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigVertexCounts_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigVertexCounts_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigVertexCounts_v1>*)
   {
      ::DataVector<xAOD::TrigVertexCounts_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigVertexCounts_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigVertexCounts_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigVertexCounts_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigVertexCounts_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigVertexCounts_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigVertexCounts_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigVertexCounts_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigVertexCounts_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigVertexCounts_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigVertexCounts_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigVertexCounts_v1>","xAOD::TrigVertexCountsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigVertexCounts_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigVertexCounts_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigVertexCounts_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigVertexCounts_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigVertexCounts_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigVertexCounts_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigVertexCounts_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","07EE6B72-1207-11E4-8B69-02163E00A892");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigVertexCountsAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigVertexCountsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigVertexCountsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigVertexCountsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigVertexCountsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigVertexCountsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigVertexCountsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigVertexCountsAuxContainer_v1*)
   {
      ::xAOD::TrigVertexCountsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigVertexCountsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigVertexCountsAuxContainer_v1", "xAODTrigMinBias/versions/TrigVertexCountsAuxContainer_v1.h", 10,
                  typeid(::xAOD::TrigVertexCountsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigVertexCountsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigVertexCountsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigVertexCountsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigVertexCountsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigVertexCountsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigVertexCountsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigVertexCountsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigVertexCountsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigVertexCountsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigVertexCountsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigVertexCountsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigVertexCountsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigVertexCountsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigVertexCountsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","70E7BDFE-1207-11E4-ACEF-02163E00A892");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigT2ZdcSignals_v1_Dictionary();
   static void xAODcLcLTrigT2ZdcSignals_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigT2ZdcSignals_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigT2ZdcSignals_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigT2ZdcSignals_v1(void *p);
   static void deleteArray_xAODcLcLTrigT2ZdcSignals_v1(void *p);
   static void destruct_xAODcLcLTrigT2ZdcSignals_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigT2ZdcSignals_v1*)
   {
      ::xAOD::TrigT2ZdcSignals_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigT2ZdcSignals_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigT2ZdcSignals_v1", "xAODTrigMinBias/versions/TrigT2ZdcSignals_v1.h", 8,
                  typeid(::xAOD::TrigT2ZdcSignals_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigT2ZdcSignals_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigT2ZdcSignals_v1) );
      instance.SetNew(&new_xAODcLcLTrigT2ZdcSignals_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigT2ZdcSignals_v1);
      instance.SetDelete(&delete_xAODcLcLTrigT2ZdcSignals_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigT2ZdcSignals_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigT2ZdcSignals_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigT2ZdcSignals_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigT2ZdcSignals_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigT2ZdcSignals_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigT2ZdcSignals_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigT2ZdcSignals_v1*)0x0)->GetClass();
      xAODcLcLTrigT2ZdcSignals_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigT2ZdcSignals_v1_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR_Dictionary();
   static void DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR_TClassManip(TClass*);
   static void *new_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(void *p = 0);
   static void *newArray_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(Long_t size, void *p);
   static void delete_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(void *p);
   static void deleteArray_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(void *p);
   static void destruct_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DataVector<xAOD::TrigT2ZdcSignals_v1>*)
   {
      ::DataVector<xAOD::TrigT2ZdcSignals_v1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DataVector<xAOD::TrigT2ZdcSignals_v1>));
      static ::ROOT::TGenericClassInfo 
         instance("DataVector<xAOD::TrigT2ZdcSignals_v1>", "AthContainers/DataVector.h", 1920,
                  typeid(::DataVector<xAOD::TrigT2ZdcSignals_v1>), DefineBehavior(ptr, ptr),
                  &DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR_Dictionary, isa_proxy, 0,
                  sizeof(::DataVector<xAOD::TrigT2ZdcSignals_v1>) );
      instance.SetNew(&new_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR);
      instance.SetNewArray(&newArray_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR);
      instance.SetDelete(&delete_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR);
      instance.SetDeleteArray(&deleteArray_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR);
      instance.SetDestructor(&destruct_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR);

      ROOT::AddClassAlternate("DataVector<xAOD::TrigT2ZdcSignals_v1>","xAOD::TrigT2ZdcSignalsContainer_v1");
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DataVector<xAOD::TrigT2ZdcSignals_v1>*)
   {
      return GenerateInitInstanceLocal((::DataVector<xAOD::TrigT2ZdcSignals_v1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigT2ZdcSignals_v1>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DataVector<xAOD::TrigT2ZdcSignals_v1>*)0x0)->GetClass();
      DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR_TClassManip(theClass);
   return theClass;
   }

   static void DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","8313279A-5E27-11E4-A635-02163E00A82C");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLTrigT2ZdcSignalsAuxContainer_v1_Dictionary();
   static void xAODcLcLTrigT2ZdcSignalsAuxContainer_v1_TClassManip(TClass*);
   static void *new_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(void *p = 0);
   static void *newArray_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(Long_t size, void *p);
   static void delete_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(void *p);
   static void deleteArray_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(void *p);
   static void destruct_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::TrigT2ZdcSignalsAuxContainer_v1*)
   {
      ::xAOD::TrigT2ZdcSignalsAuxContainer_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::TrigT2ZdcSignalsAuxContainer_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::TrigT2ZdcSignalsAuxContainer_v1", "xAODTrigMinBias/versions/TrigT2ZdcSignalsAuxContainer_v1.h", 10,
                  typeid(::xAOD::TrigT2ZdcSignalsAuxContainer_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLTrigT2ZdcSignalsAuxContainer_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::TrigT2ZdcSignalsAuxContainer_v1) );
      instance.SetNew(&new_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1);
      instance.SetNewArray(&newArray_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1);
      instance.SetDelete(&delete_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1);
      instance.SetDestructor(&destruct_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::TrigT2ZdcSignalsAuxContainer_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::TrigT2ZdcSignalsAuxContainer_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::TrigT2ZdcSignalsAuxContainer_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLTrigT2ZdcSignalsAuxContainer_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::TrigT2ZdcSignalsAuxContainer_v1*)0x0)->GetClass();
      xAODcLcLTrigT2ZdcSignalsAuxContainer_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLTrigT2ZdcSignalsAuxContainer_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","83B9F174-5E27-11E4-98C2-02163E00A82");
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigSpacePointCounts_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigSpacePointCounts_v1 : new ::xAOD::TrigSpacePointCounts_v1;
   }
   static void *newArray_xAODcLcLTrigSpacePointCounts_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigSpacePointCounts_v1[nElements] : new ::xAOD::TrigSpacePointCounts_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigSpacePointCounts_v1(void *p) {
      delete ((::xAOD::TrigSpacePointCounts_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigSpacePointCounts_v1(void *p) {
      delete [] ((::xAOD::TrigSpacePointCounts_v1*)p);
   }
   static void destruct_xAODcLcLTrigSpacePointCounts_v1(void *p) {
      typedef ::xAOD::TrigSpacePointCounts_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigSpacePointCounts_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigSpacePointCounts_v1> : new ::DataVector<xAOD::TrigSpacePointCounts_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigSpacePointCounts_v1>[nElements] : new ::DataVector<xAOD::TrigSpacePointCounts_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigSpacePointCounts_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigSpacePointCounts_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigSpacePointCounts_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigSpacePointCounts_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigSpacePointCounts_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigSpacePointCountsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigSpacePointCountsAuxContainer_v1 : new ::xAOD::TrigSpacePointCountsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigSpacePointCountsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigSpacePointCountsAuxContainer_v1[nElements] : new ::xAOD::TrigSpacePointCountsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigSpacePointCountsAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigSpacePointCountsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigSpacePointCountsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigSpacePointCountsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigSpacePointCountsAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigSpacePointCountsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigSpacePointCountsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigT2MbtsBits_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigT2MbtsBits_v1 : new ::xAOD::TrigT2MbtsBits_v1;
   }
   static void *newArray_xAODcLcLTrigT2MbtsBits_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigT2MbtsBits_v1[nElements] : new ::xAOD::TrigT2MbtsBits_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigT2MbtsBits_v1(void *p) {
      delete ((::xAOD::TrigT2MbtsBits_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigT2MbtsBits_v1(void *p) {
      delete [] ((::xAOD::TrigT2MbtsBits_v1*)p);
   }
   static void destruct_xAODcLcLTrigT2MbtsBits_v1(void *p) {
      typedef ::xAOD::TrigT2MbtsBits_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigT2MbtsBits_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigT2MbtsBits_v1> : new ::DataVector<xAOD::TrigT2MbtsBits_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigT2MbtsBits_v1>[nElements] : new ::DataVector<xAOD::TrigT2MbtsBits_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigT2MbtsBits_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigT2MbtsBits_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigT2MbtsBits_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigT2MbtsBits_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigT2MbtsBits_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigT2MbtsBitsAuxContainer_v1 : new ::xAOD::TrigT2MbtsBitsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigT2MbtsBitsAuxContainer_v1[nElements] : new ::xAOD::TrigT2MbtsBitsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigT2MbtsBitsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigT2MbtsBitsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigT2MbtsBitsAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigT2MbtsBitsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigT2MbtsBitsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigHisto2D_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigHisto2D_v1 : new ::xAOD::TrigHisto2D_v1;
   }
   static void *newArray_xAODcLcLTrigHisto2D_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigHisto2D_v1[nElements] : new ::xAOD::TrigHisto2D_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigHisto2D_v1(void *p) {
      delete ((::xAOD::TrigHisto2D_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigHisto2D_v1(void *p) {
      delete [] ((::xAOD::TrigHisto2D_v1*)p);
   }
   static void destruct_xAODcLcLTrigHisto2D_v1(void *p) {
      typedef ::xAOD::TrigHisto2D_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigHisto2D_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigHisto2D_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigHisto2D_v1> : new ::DataVector<xAOD::TrigHisto2D_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigHisto2D_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigHisto2D_v1>[nElements] : new ::DataVector<xAOD::TrigHisto2D_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigHisto2D_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigHisto2D_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigHisto2D_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigHisto2D_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigHisto2D_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigHisto2D_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigHisto2D_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigHisto2DAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigHisto2DAuxContainer_v1 : new ::xAOD::TrigHisto2DAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigHisto2DAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigHisto2DAuxContainer_v1[nElements] : new ::xAOD::TrigHisto2DAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigHisto2DAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigHisto2DAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigHisto2DAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigHisto2DAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigHisto2DAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigHisto2DAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigHisto2DAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigTrackCounts_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigTrackCounts_v1 : new ::xAOD::TrigTrackCounts_v1;
   }
   static void *newArray_xAODcLcLTrigTrackCounts_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigTrackCounts_v1[nElements] : new ::xAOD::TrigTrackCounts_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigTrackCounts_v1(void *p) {
      delete ((::xAOD::TrigTrackCounts_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigTrackCounts_v1(void *p) {
      delete [] ((::xAOD::TrigTrackCounts_v1*)p);
   }
   static void destruct_xAODcLcLTrigTrackCounts_v1(void *p) {
      typedef ::xAOD::TrigTrackCounts_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigTrackCounts_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigTrackCounts_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigTrackCounts_v1> : new ::DataVector<xAOD::TrigTrackCounts_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigTrackCounts_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigTrackCounts_v1>[nElements] : new ::DataVector<xAOD::TrigTrackCounts_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigTrackCounts_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigTrackCounts_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigTrackCounts_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigTrackCounts_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigTrackCounts_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigTrackCounts_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigTrackCounts_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigTrackCountsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigTrackCountsAuxContainer_v1 : new ::xAOD::TrigTrackCountsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigTrackCountsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigTrackCountsAuxContainer_v1[nElements] : new ::xAOD::TrigTrackCountsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigTrackCountsAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigTrackCountsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigTrackCountsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigTrackCountsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigTrackCountsAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigTrackCountsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigTrackCountsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigVertexCounts_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigVertexCounts_v1 : new ::xAOD::TrigVertexCounts_v1;
   }
   static void *newArray_xAODcLcLTrigVertexCounts_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigVertexCounts_v1[nElements] : new ::xAOD::TrigVertexCounts_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigVertexCounts_v1(void *p) {
      delete ((::xAOD::TrigVertexCounts_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigVertexCounts_v1(void *p) {
      delete [] ((::xAOD::TrigVertexCounts_v1*)p);
   }
   static void destruct_xAODcLcLTrigVertexCounts_v1(void *p) {
      typedef ::xAOD::TrigVertexCounts_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigVertexCounts_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigVertexCounts_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigVertexCounts_v1> : new ::DataVector<xAOD::TrigVertexCounts_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigVertexCounts_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigVertexCounts_v1>[nElements] : new ::DataVector<xAOD::TrigVertexCounts_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigVertexCounts_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigVertexCounts_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigVertexCounts_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigVertexCounts_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigVertexCounts_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigVertexCounts_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigVertexCounts_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigVertexCountsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigVertexCountsAuxContainer_v1 : new ::xAOD::TrigVertexCountsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigVertexCountsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigVertexCountsAuxContainer_v1[nElements] : new ::xAOD::TrigVertexCountsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigVertexCountsAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigVertexCountsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigVertexCountsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigVertexCountsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigVertexCountsAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigVertexCountsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigVertexCountsAuxContainer_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigT2ZdcSignals_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigT2ZdcSignals_v1 : new ::xAOD::TrigT2ZdcSignals_v1;
   }
   static void *newArray_xAODcLcLTrigT2ZdcSignals_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigT2ZdcSignals_v1[nElements] : new ::xAOD::TrigT2ZdcSignals_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigT2ZdcSignals_v1(void *p) {
      delete ((::xAOD::TrigT2ZdcSignals_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigT2ZdcSignals_v1(void *p) {
      delete [] ((::xAOD::TrigT2ZdcSignals_v1*)p);
   }
   static void destruct_xAODcLcLTrigT2ZdcSignals_v1(void *p) {
      typedef ::xAOD::TrigT2ZdcSignals_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigT2ZdcSignals_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(void *p) {
      return  p ? new(p) ::DataVector<xAOD::TrigT2ZdcSignals_v1> : new ::DataVector<xAOD::TrigT2ZdcSignals_v1>;
   }
   static void *newArray_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(Long_t nElements, void *p) {
      return p ? new(p) ::DataVector<xAOD::TrigT2ZdcSignals_v1>[nElements] : new ::DataVector<xAOD::TrigT2ZdcSignals_v1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(void *p) {
      delete ((::DataVector<xAOD::TrigT2ZdcSignals_v1>*)p);
   }
   static void deleteArray_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(void *p) {
      delete [] ((::DataVector<xAOD::TrigT2ZdcSignals_v1>*)p);
   }
   static void destruct_DataVectorlExAODcLcLTrigT2ZdcSignals_v1gR(void *p) {
      typedef ::DataVector<xAOD::TrigT2ZdcSignals_v1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DataVector<xAOD::TrigT2ZdcSignals_v1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigT2ZdcSignalsAuxContainer_v1 : new ::xAOD::TrigT2ZdcSignalsAuxContainer_v1;
   }
   static void *newArray_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::TrigT2ZdcSignalsAuxContainer_v1[nElements] : new ::xAOD::TrigT2ZdcSignalsAuxContainer_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(void *p) {
      delete ((::xAOD::TrigT2ZdcSignalsAuxContainer_v1*)p);
   }
   static void deleteArray_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(void *p) {
      delete [] ((::xAOD::TrigT2ZdcSignalsAuxContainer_v1*)p);
   }
   static void destruct_xAODcLcLTrigT2ZdcSignalsAuxContainer_v1(void *p) {
      typedef ::xAOD::TrigT2ZdcSignalsAuxContainer_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::TrigT2ZdcSignalsAuxContainer_v1

namespace {
  void TriggerDictionaryInitialization_xAODTrigMinBias_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigMinBias/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigMinBias",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODTrigMinBias/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigSpacePointCounts_v1.h")))  TrigSpacePointCounts_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@C682FD32-73CC-4773-ACBF-30B2657C5991)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigSpacePointCountsAuxContainer_v1.h")))  TrigSpacePointCountsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigT2MbtsBits_v1.h")))  TrigT2MbtsBits_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@0055818E-1207-11E4-9C7B-02163E00A892)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigT2MbtsBitsAuxContainer_v1.h")))  TrigT2MbtsBitsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigHisto2D_v1.h")))  TrigHisto2D_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@05B28A1E-1207-11E4-84E2-02163E00A892)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigHisto2DAuxContainer_v1.h")))  TrigHisto2DAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigTrackCounts_v1.h")))  TrigTrackCounts_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@0701BEB0-1207-11E4-9D08-02163E00A892)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigTrackCountsAuxContainer_v1.h")))  TrigTrackCountsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigVertexCounts_v1.h")))  TrigVertexCounts_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@70E7BDFE-1207-11E4-ACEF-02163E00A892)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigVertexCountsAuxContainer_v1.h")))  TrigVertexCountsAuxContainer_v1;}
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigT2ZdcSignals_v1.h")))  TrigT2ZdcSignals_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@83B9F174-5E27-11E4-98C2-02163E00A82)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODTrigMinBias/versions/TrigT2ZdcSignalsAuxContainer_v1.h")))  TrigT2ZdcSignalsAuxContainer_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODTrigMinBias"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef xAODTrigMinBias_xAODTrigMinBias_DICT_H
#define xAODTrigMinBias_xAODTrigMinBias_DICT_H

// Include all headers here that need to have dictionaries
// generated for them.

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

#include "xAODTrigMinBias/versions/TrigSpacePointCounts_v1.h"
#include "xAODTrigMinBias/versions/TrigSpacePointCountsContainer_v1.h"
#include "xAODTrigMinBias/versions/TrigSpacePointCountsAuxContainer_v1.h"

#include "xAODTrigMinBias/versions/TrigT2MbtsBits_v1.h"
#include "xAODTrigMinBias/versions/TrigT2MbtsBitsContainer_v1.h"
#include "xAODTrigMinBias/versions/TrigT2MbtsBitsAuxContainer_v1.h"

#include "xAODTrigMinBias/versions/TrigHisto2D_v1.h"
#include "xAODTrigMinBias/versions/TrigHisto2DContainer_v1.h"
#include "xAODTrigMinBias/versions/TrigHisto2DAuxContainer_v1.h"

#include "xAODTrigMinBias/versions/TrigVertexCounts_v1.h"
#include "xAODTrigMinBias/versions/TrigVertexCountsContainer_v1.h"
#include "xAODTrigMinBias/versions/TrigVertexCountsAuxContainer_v1.h"

#include "xAODTrigMinBias/versions/TrigTrackCounts_v1.h"
#include "xAODTrigMinBias/versions/TrigTrackCountsContainer_v1.h"
#include "xAODTrigMinBias/versions/TrigTrackCountsAuxContainer_v1.h"

#include "xAODTrigMinBias/versions/TrigT2ZdcSignals_v1.h"
#include "xAODTrigMinBias/versions/TrigT2ZdcSignalsContainer_v1.h"
#include "xAODTrigMinBias/versions/TrigT2ZdcSignalsAuxContainer_v1.h"

//i don't know what for
namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODTRIGMINBIAS {
      xAOD::TrigSpacePointCountsContainer_v1 c1;
      DataLink< xAOD::TrigSpacePointCountsContainer_v1 > dl1;
      std::vector< DataLink< xAOD::TrigSpacePointCountsContainer_v1 > > dl2;
      ElementLink< xAOD::TrigSpacePointCountsContainer_v1 > el1;
      std::vector< ElementLink< xAOD::TrigSpacePointCountsContainer_v1 > > el2;
      std::vector< std::vector< ElementLink< xAOD::TrigSpacePointCountsContainer_v1 > > > el3;
      
      xAOD::TrigT2MbtsBitsContainer_v1 ct1;
      DataLink< xAOD::TrigT2MbtsBitsContainer_v1 > dlt1;
      std::vector< DataLink< xAOD::TrigT2MbtsBitsContainer_v1 > > dlt2;
      ElementLink< xAOD::TrigT2MbtsBitsContainer_v1 > elt1;
      std::vector< ElementLink< xAOD::TrigT2MbtsBitsContainer_v1 > > elt2;
      std::vector< std::vector< ElementLink< xAOD::TrigT2MbtsBitsContainer_v1 > > > elt3;
      
      xAOD::TrigVertexCountsContainer_v1 cv1;
      DataLink< xAOD::TrigVertexCountsContainer_v1 > dlv1;
      std::vector< DataLink< xAOD::TrigVertexCountsContainer_v1 > > dlv2;
      ElementLink< xAOD::TrigVertexCountsContainer_v1 > elv1;
      std::vector< ElementLink< xAOD::TrigVertexCountsContainer_v1 > > elv2;
      std::vector< std::vector< ElementLink< xAOD::TrigVertexCountsContainer_v1 > > > elv3;
      
      xAOD::TrigTrackCountsContainer_v1 ctt1;
      DataLink< xAOD::TrigTrackCountsContainer_v1 > dltt1;
      std::vector< DataLink< xAOD::TrigTrackCountsContainer_v1 > > dltt2;
      ElementLink< xAOD::TrigTrackCountsContainer_v1 > eltt1;
      std::vector< ElementLink< xAOD::TrigTrackCountsContainer_v1 > > eltt2;
      std::vector< std::vector< ElementLink< xAOD::TrigTrackCountsContainer_v1 > > > eltt3;

      xAOD::TrigT2ZdcSignalsContainer_v1 czdct1;
      DataLink< xAOD::TrigT2ZdcSignalsContainer_v1 > dlzdct1;
      std::vector< DataLink< xAOD::TrigT2ZdcSignalsContainer_v1 > > dlzdct2;
      ElementLink< xAOD::TrigT2ZdcSignalsContainer_v1 > elzdct1;
      std::vector< ElementLink< xAOD::TrigT2ZdcSignalsContainer_v1 > > elzdct2;
      std::vector< std::vector< ElementLink< xAOD::TrigT2ZdcSignalsContainer_v1 > > > elzdct3;

   };
} // private namespace

#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"DataVector<xAOD::TrigHisto2D_v1>", payloadCode, "@",
"DataVector<xAOD::TrigSpacePointCounts_v1>", payloadCode, "@",
"DataVector<xAOD::TrigT2MbtsBits_v1>", payloadCode, "@",
"DataVector<xAOD::TrigT2ZdcSignals_v1>", payloadCode, "@",
"DataVector<xAOD::TrigTrackCounts_v1>", payloadCode, "@",
"DataVector<xAOD::TrigVertexCounts_v1>", payloadCode, "@",
"xAOD::TrigHisto2DAuxContainer_v1", payloadCode, "@",
"xAOD::TrigHisto2DContainer_v1", payloadCode, "@",
"xAOD::TrigHisto2D_v1", payloadCode, "@",
"xAOD::TrigSpacePointCountsAuxContainer_v1", payloadCode, "@",
"xAOD::TrigSpacePointCountsContainer_v1", payloadCode, "@",
"xAOD::TrigSpacePointCounts_v1", payloadCode, "@",
"xAOD::TrigT2MbtsBitsAuxContainer_v1", payloadCode, "@",
"xAOD::TrigT2MbtsBitsContainer_v1", payloadCode, "@",
"xAOD::TrigT2MbtsBits_v1", payloadCode, "@",
"xAOD::TrigT2ZdcSignalsAuxContainer_v1", payloadCode, "@",
"xAOD::TrigT2ZdcSignalsContainer_v1", payloadCode, "@",
"xAOD::TrigT2ZdcSignals_v1", payloadCode, "@",
"xAOD::TrigTrackCountsAuxContainer_v1", payloadCode, "@",
"xAOD::TrigTrackCountsContainer_v1", payloadCode, "@",
"xAOD::TrigTrackCounts_v1", payloadCode, "@",
"xAOD::TrigVertexCountsAuxContainer_v1", payloadCode, "@",
"xAOD::TrigVertexCountsContainer_v1", payloadCode, "@",
"xAOD::TrigVertexCounts_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODTrigMinBias_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODTrigMinBias_Reflex_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODTrigMinBias_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODTrigMinBias_Reflex() {
  TriggerDictionaryInitialization_xAODTrigMinBias_Reflex_Impl();
}

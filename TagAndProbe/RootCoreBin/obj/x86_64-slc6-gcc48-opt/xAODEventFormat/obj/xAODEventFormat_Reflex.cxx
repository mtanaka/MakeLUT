// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODEventFormatdIobjdIxAODEventFormat_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventFormat/xAODEventFormat/xAODEventFormatDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLEventFormatElement_Dictionary();
   static void xAODcLcLEventFormatElement_TClassManip(TClass*);
   static void *new_xAODcLcLEventFormatElement(void *p = 0);
   static void *newArray_xAODcLcLEventFormatElement(Long_t size, void *p);
   static void delete_xAODcLcLEventFormatElement(void *p);
   static void deleteArray_xAODcLcLEventFormatElement(void *p);
   static void destruct_xAODcLcLEventFormatElement(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EventFormatElement*)
   {
      ::xAOD::EventFormatElement *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EventFormatElement));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EventFormatElement", "xAODEventFormat/EventFormatElement.h", 32,
                  typeid(::xAOD::EventFormatElement), DefineBehavior(ptr, ptr),
                  &xAODcLcLEventFormatElement_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EventFormatElement) );
      instance.SetNew(&new_xAODcLcLEventFormatElement);
      instance.SetNewArray(&newArray_xAODcLcLEventFormatElement);
      instance.SetDelete(&delete_xAODcLcLEventFormatElement);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEventFormatElement);
      instance.SetDestructor(&destruct_xAODcLcLEventFormatElement);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EventFormatElement*)
   {
      return GenerateInitInstanceLocal((::xAOD::EventFormatElement*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EventFormatElement*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEventFormatElement_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EventFormatElement*)0x0)->GetClass();
      xAODcLcLEventFormatElement_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEventFormatElement_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLEventFormat_v1_Dictionary();
   static void xAODcLcLEventFormat_v1_TClassManip(TClass*);
   static void *new_xAODcLcLEventFormat_v1(void *p = 0);
   static void *newArray_xAODcLcLEventFormat_v1(Long_t size, void *p);
   static void delete_xAODcLcLEventFormat_v1(void *p);
   static void deleteArray_xAODcLcLEventFormat_v1(void *p);
   static void destruct_xAODcLcLEventFormat_v1(void *p);

   // Schema evolution read functions
   static void read_xAODcLcLEventFormat_v1_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      xAOD::EventFormat_v1* newObj = (xAOD::EventFormat_v1*)target;
      // Supress warning message.
      if (oldObj) {}

      if (newObj) {}

      //--- User's code ---
     
         newObj->toTransient();
      
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::EventFormat_v1*)
   {
      ::xAOD::EventFormat_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::EventFormat_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::EventFormat_v1", "xAODEventFormat/versions/EventFormat_v1.h", 33,
                  typeid(::xAOD::EventFormat_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLEventFormat_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::EventFormat_v1) );
      instance.SetNew(&new_xAODcLcLEventFormat_v1);
      instance.SetNewArray(&newArray_xAODcLcLEventFormat_v1);
      instance.SetDelete(&delete_xAODcLcLEventFormat_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLEventFormat_v1);
      instance.SetDestructor(&destruct_xAODcLcLEventFormat_v1);

      ROOT::TSchemaHelper* rule;

      // the io read rules
      std::vector<ROOT::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "xAOD::EventFormat_v1";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_xAODcLcLEventFormat_v1_0);
      rule->fCode        = "\n         newObj->toTransient();\n      ";
      rule->fVersion     = "[1-]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::EventFormat_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::EventFormat_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::EventFormat_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLEventFormat_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::EventFormat_v1*)0x0)->GetClass();
      xAODcLcLEventFormat_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLEventFormat_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","0EFE2D2C-9E78-441D-9A87-9EE2B908AC81");
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEventFormatElement(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventFormatElement : new ::xAOD::EventFormatElement;
   }
   static void *newArray_xAODcLcLEventFormatElement(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventFormatElement[nElements] : new ::xAOD::EventFormatElement[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEventFormatElement(void *p) {
      delete ((::xAOD::EventFormatElement*)p);
   }
   static void deleteArray_xAODcLcLEventFormatElement(void *p) {
      delete [] ((::xAOD::EventFormatElement*)p);
   }
   static void destruct_xAODcLcLEventFormatElement(void *p) {
      typedef ::xAOD::EventFormatElement current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EventFormatElement

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLEventFormat_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventFormat_v1 : new ::xAOD::EventFormat_v1;
   }
   static void *newArray_xAODcLcLEventFormat_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::EventFormat_v1[nElements] : new ::xAOD::EventFormat_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLEventFormat_v1(void *p) {
      delete ((::xAOD::EventFormat_v1*)p);
   }
   static void deleteArray_xAODcLcLEventFormat_v1(void *p) {
      delete [] ((::xAOD::EventFormat_v1*)p);
   }
   static void destruct_xAODcLcLEventFormat_v1(void *p) {
      typedef ::xAOD::EventFormat_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::EventFormat_v1

namespace {
  void TriggerDictionaryInitialization_xAODEventFormat_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventFormat/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODEventFormat/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate("$clingAutoload$xAODEventFormat/EventFormatElement.h")))  EventFormatElement;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@0EFE2D2C-9E78-441D-9A87-9EE2B908AC81)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODEventFormat/versions/EventFormat_v1.h")))  EventFormat_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODEventFormat"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODEventFormatDict.h 568357 2013-11-01 16:21:30Z krasznaa $
#ifndef XAODEVENTFORMAT_XAODEVENTFORMATDICT_H
#define XAODEVENTFORMAT_XAODEVENTFORMATDICT_H

// Includes for the dictionary generation:
#include "xAODEventFormat/EventFormatElement.h"
#include "xAODEventFormat/versions/EventFormat_v1.h"

#endif // XAODEVENTFORMAT_XAODEVENTFORMATDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xAOD::EventFormatElement", payloadCode, "@",
"xAOD::EventFormat_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODEventFormat_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODEventFormat_Reflex_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODEventFormat_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODEventFormat_Reflex() {
  TriggerDictionaryInitialization_xAODEventFormat_Reflex_Impl();
}

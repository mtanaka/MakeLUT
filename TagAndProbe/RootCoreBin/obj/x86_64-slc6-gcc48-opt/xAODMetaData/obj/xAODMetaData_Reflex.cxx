// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIdatadImaxi174dIzpdImtanakadIforYazawaProdIgitdIMakeLUTdITagAndProbedIRootCoreBindIobjdIx86_64mIslc6mIgcc48mIoptdIxAODMetaDatadIobjdIxAODMetaData_Reflex

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMetaData/xAODMetaData/xAODMetaDataDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xAODcLcLFileMetaData_v1_Dictionary();
   static void xAODcLcLFileMetaData_v1_TClassManip(TClass*);
   static void *new_xAODcLcLFileMetaData_v1(void *p = 0);
   static void *newArray_xAODcLcLFileMetaData_v1(Long_t size, void *p);
   static void delete_xAODcLcLFileMetaData_v1(void *p);
   static void deleteArray_xAODcLcLFileMetaData_v1(void *p);
   static void destruct_xAODcLcLFileMetaData_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::FileMetaData_v1*)
   {
      ::xAOD::FileMetaData_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::FileMetaData_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::FileMetaData_v1", "xAODMetaData/versions/FileMetaData_v1.h", 26,
                  typeid(::xAOD::FileMetaData_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLFileMetaData_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::FileMetaData_v1) );
      instance.SetNew(&new_xAODcLcLFileMetaData_v1);
      instance.SetNewArray(&newArray_xAODcLcLFileMetaData_v1);
      instance.SetDelete(&delete_xAODcLcLFileMetaData_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLFileMetaData_v1);
      instance.SetDestructor(&destruct_xAODcLcLFileMetaData_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::FileMetaData_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::FileMetaData_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::FileMetaData_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLFileMetaData_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::FileMetaData_v1*)0x0)->GetClass();
      xAODcLcLFileMetaData_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLFileMetaData_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","C87E3828-4A7A-480A-95DE-0339539F6A0F");
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *xAODcLcLFileMetaDataAuxInfo_v1_Dictionary();
   static void xAODcLcLFileMetaDataAuxInfo_v1_TClassManip(TClass*);
   static void *new_xAODcLcLFileMetaDataAuxInfo_v1(void *p = 0);
   static void *newArray_xAODcLcLFileMetaDataAuxInfo_v1(Long_t size, void *p);
   static void delete_xAODcLcLFileMetaDataAuxInfo_v1(void *p);
   static void deleteArray_xAODcLcLFileMetaDataAuxInfo_v1(void *p);
   static void destruct_xAODcLcLFileMetaDataAuxInfo_v1(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAOD::FileMetaDataAuxInfo_v1*)
   {
      ::xAOD::FileMetaDataAuxInfo_v1 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xAOD::FileMetaDataAuxInfo_v1));
      static ::ROOT::TGenericClassInfo 
         instance("xAOD::FileMetaDataAuxInfo_v1", "xAODMetaData/versions/FileMetaDataAuxInfo_v1.h", 25,
                  typeid(::xAOD::FileMetaDataAuxInfo_v1), DefineBehavior(ptr, ptr),
                  &xAODcLcLFileMetaDataAuxInfo_v1_Dictionary, isa_proxy, 0,
                  sizeof(::xAOD::FileMetaDataAuxInfo_v1) );
      instance.SetNew(&new_xAODcLcLFileMetaDataAuxInfo_v1);
      instance.SetNewArray(&newArray_xAODcLcLFileMetaDataAuxInfo_v1);
      instance.SetDelete(&delete_xAODcLcLFileMetaDataAuxInfo_v1);
      instance.SetDeleteArray(&deleteArray_xAODcLcLFileMetaDataAuxInfo_v1);
      instance.SetDestructor(&destruct_xAODcLcLFileMetaDataAuxInfo_v1);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAOD::FileMetaDataAuxInfo_v1*)
   {
      return GenerateInitInstanceLocal((::xAOD::FileMetaDataAuxInfo_v1*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAOD::FileMetaDataAuxInfo_v1*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xAODcLcLFileMetaDataAuxInfo_v1_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xAOD::FileMetaDataAuxInfo_v1*)0x0)->GetClass();
      xAODcLcLFileMetaDataAuxInfo_v1_TClassManip(theClass);
   return theClass;
   }

   static void xAODcLcLFileMetaDataAuxInfo_v1_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","BEE2BECF-A936-4078-9FDD-AD703C9ADF9F");
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLFileMetaData_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::FileMetaData_v1 : new ::xAOD::FileMetaData_v1;
   }
   static void *newArray_xAODcLcLFileMetaData_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::FileMetaData_v1[nElements] : new ::xAOD::FileMetaData_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLFileMetaData_v1(void *p) {
      delete ((::xAOD::FileMetaData_v1*)p);
   }
   static void deleteArray_xAODcLcLFileMetaData_v1(void *p) {
      delete [] ((::xAOD::FileMetaData_v1*)p);
   }
   static void destruct_xAODcLcLFileMetaData_v1(void *p) {
      typedef ::xAOD::FileMetaData_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::FileMetaData_v1

namespace ROOT {
   // Wrappers around operator new
   static void *new_xAODcLcLFileMetaDataAuxInfo_v1(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::FileMetaDataAuxInfo_v1 : new ::xAOD::FileMetaDataAuxInfo_v1;
   }
   static void *newArray_xAODcLcLFileMetaDataAuxInfo_v1(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::xAOD::FileMetaDataAuxInfo_v1[nElements] : new ::xAOD::FileMetaDataAuxInfo_v1[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAODcLcLFileMetaDataAuxInfo_v1(void *p) {
      delete ((::xAOD::FileMetaDataAuxInfo_v1*)p);
   }
   static void deleteArray_xAODcLcLFileMetaDataAuxInfo_v1(void *p) {
      delete [] ((::xAOD::FileMetaDataAuxInfo_v1*)p);
   }
   static void destruct_xAODcLcLFileMetaDataAuxInfo_v1(void *p) {
      typedef ::xAOD::FileMetaDataAuxInfo_v1 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAOD::FileMetaDataAuxInfo_v1

namespace {
  void TriggerDictionaryInitialization_xAODMetaData_Reflex_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMetaData/Root",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/data/maxi174/zp/mtanaka/forYazawaPro/git/MakeLUT/TagAndProbe/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.02.12-x86_64-slc6-gcc48-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/2.3.21/xAODMetaData/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@C87E3828-4A7A-480A-95DE-0339539F6A0F)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMetaData/versions/FileMetaData_v1.h")))  FileMetaData_v1;}
namespace xAOD{class __attribute__((annotate(R"ATTRDUMP(id@@@BEE2BECF-A936-4078-9FDD-AD703C9ADF9F)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$xAODMetaData/versions/FileMetaDataAuxInfo_v1.h")))  FileMetaDataAuxInfo_v1;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 23
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e3698_s2608_s2183_r6630_tid05352803_00/AOD.05352803._000242.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r6630"
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODMetaData"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Dear emacs, this is -*- c++ -*-
// $Id: xAODMetaDataDict.h 670157 2015-05-27 11:52:51Z krasznaa $
#ifndef XAODMETADATA_XAODMETADATADICT_H
#define XAODMETADATA_XAODMETADATADICT_H

// Local include(s):
#include "xAODMetaData/versions/FileMetaData_v1.h"
#include "xAODMetaData/versions/FileMetaDataAuxInfo_v1.h"

#endif // XAODMETADATA_XAODMETADATADICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xAOD::FileMetaDataAuxInfo_v1", payloadCode, "@",
"xAOD::FileMetaData_v1", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODMetaData_Reflex",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODMetaData_Reflex_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODMetaData_Reflex_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODMetaData_Reflex() {
  TriggerDictionaryInitialization_xAODMetaData_Reflex_Impl();
}

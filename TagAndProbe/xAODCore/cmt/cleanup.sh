# echo "cleanup xAODCore xAODCore-00-00-84-04 in /home/mtanaka/Event/xAOD"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtxAODCoretempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtxAODCoretempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=xAODCore -version=xAODCore-00-00-84-04 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory $* >${cmtxAODCoretempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=xAODCore -version=xAODCore-00-00-84-04 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory $* >${cmtxAODCoretempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtxAODCoretempfile}
  unset cmtxAODCoretempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtxAODCoretempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtxAODCoretempfile}
unset cmtxAODCoretempfile
return $cmtcleanupstatus


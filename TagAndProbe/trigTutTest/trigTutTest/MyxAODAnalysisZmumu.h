#ifndef MyAnalysis_MyxAODAnalysisZmumu_H
#define MyAnalysis_MyxAODAnalysisZmumu_H

#include <EventLoop/Algorithm.h>
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "trigTutTest/Util.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "TH1.h"
#include <iostream>
#include <map>
using namespace Trig;
using namespace TrigConf;
using namespace std;

class MyxAODAnalysisZmumu : public EL::Algorithm
{
  
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;
  xAOD::TEvent *m_event;  //!
  int m_eventCounter; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  std::string outputName;
  //tree output
  TTree *tree; //!
  float m_tag_offline_pt; //!
  float m_tag_offline_eta; //!
  float m_tag_offline_phi; //!
  float m_tag_offline_charge; //!
  float m_probe_offline_pt; //!
  float m_probe_offline_eta; //!
  float m_probe_offline_phi; //!
  float m_probe_offline_charge; //!
  float m_probe_roi_eta; //!
  float m_probe_roi_phi; //!
  int m_probe_roi_threshold; //!
  float m_probe_sa_pt; //!
  float m_probe_sa_eta; //!
  float m_probe_sa_phi; //!
  int m_probe_sa_charge; //!
  float m_probe_sa_alpha_pt; //!
  float m_probe_sa_beta_pt; //!
  float m_probe_sa_tgc_pt; //!
  float m_probe_sa_ec_radius_pt; //!
  int m_probe_sa_saddress; //!
  float m_probe_sa_ec_alpha; //!
  float m_probe_sa_ec_beta; //!
  float m_probe_sa_ec_radius; //!
  float m_probe_sa_br_radius; //!
  float m_probe_sa_etaMap; //!
  float m_probe_sa_phiMap; //!
  float m_probe_sa_tgcInn_eta; //!
  float m_probe_sa_tgcInn_phi; //!
  float m_probe_sa_tgcMid1_r; //!
  float m_probe_sa_tgcMid1_z; //!
  float m_probe_sa_tgcMid1_eta; //!
  float m_probe_sa_tgcMid1_phi; //!
  float m_probe_sa_tgcMid2_r; //!
  float m_probe_sa_tgcMid2_z; //!
  vector<float> m_sp_r; //!
  vector<float> m_sp_z; //!

  int nTagChain; //!
  vector<string> tagChain; //!
  vector<int> tagL1ThrNum; //!
  vector<int> tagThr; //!
  int nProbeChain; //!
  vector<string> probeChain; //!
  vector<int> probeThr; //!
  vector<int> probeL1ThrNum; //!
  bool runLocal; //!
  bool doDebug; //!
  bool useGRL; //!
  bool doMatching; //!

  //tool
  Trig::TrigDecisionTool *trigDecTool; //! 
  TrigConf::xAODConfigTool *configTool; //!

  GoodRunsListSelectionTool *m_grl; //!

  map<string,int> triggerCounts;

  // this is a standard constructor
  MyxAODAnalysisZmumu ();

  Util m_util; //!

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysisZmumu, 1);
  
};

#endif

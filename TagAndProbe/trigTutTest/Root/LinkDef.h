#include "trigTutTest/MyxAODAnalysisZmumu.h"
#include "trigTutTest/Util.h"
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class MyxAODAnalysisZmumu+;
#pragma link C++ class Util+;
#endif

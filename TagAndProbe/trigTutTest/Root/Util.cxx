#include <iostream>
#include "trigTutTest/Util.h"
#include "TTree.h"
#include "TH1.h"
#include "TLorentzVector.h"
using namespace std;

// this is needed to distribute the algorithm to the workers
//ClassImp(Util)

const double PI = 3.14159265258979;
double const PI_OVER_4 = PI/4.0;
double const PI_OVER_8 = PI/8.0;
double const PHI_RANGE = 12.0/PI_OVER_8;

Util :: Util ()
{
}

Util :: ~Util ()
{
}

int Util :: initialize ()
{
  cout << "Util initializing..." << endl;
  
  return 0;
}

float Util :: calcDr(float eta1, float eta2, float phi1, float phi2){
  float deta = eta1-eta2;
  float dphi = acos(cos(phi1-phi2));
  float dr = sqrt(deta*deta+dphi*dphi);
  return dr;
}

//////////////////////////////////////////
pair<int,int> Util :: GetBinNumber(float eta, float phi){

  int Octant = (int)(phi/PI_OVER_4);
  double PhiInOctant = fabs(phi - Octant*PI_OVER_4);
  if(PhiInOctant > PI_OVER_8) PhiInOctant = PI_OVER_4 - PhiInOctant;
  int phiBin = static_cast<int>(PhiInOctant*PHI_RANGE);
  int etaBin = static_cast<int>((fabs(eta)-1.)/0.05);

  if (etaBin < 0) etaBin =  0;
  if (etaBin > 29) etaBin = 29;

  if (etaBin < -0.5 || etaBin > 29.5 || phiBin < -0.5 || phiBin > 11.5) return make_pair(-1,-1);

  return make_pair(etaBin,phiBin);
}

////////////////////////////////////////////////
/*bool Util :: isSmall(float phi){
  int SL=1;//0:small 1:large
  if (fabs(phi)>0.196 && fabs(phi)<0.589) SL=0;
  if (fabs(phi)>0.982 && fabs(phi)<1.375) SL=0;
  if (fabs(phi)>1.767 && fabs(phi)<2.16) SL=0;
  if (fabs(phi)>2.553 && fabs(phi)<2.945) SL=0;
  if (fabs(phi)<0.196) SL=1;
  if (fabs(phi)>0.589 && fabs(phi)<0.982) SL=1;
  if (fabs(phi)>1.375 && fabs(phi)<1.767) SL=1;
  if (fabs(phi)>2.16 && fabs(phi)<2.553) SL=1;
  if (fabs(phi)>2.945) SL=1;
  bool isSmall= (SL==0)? true : false;
  return isSmall;

}*/

///////////////////////////////////////////////////
bool Util :: isSmall(float eez){
  bool small = false;
  if (fabs(eez)>10000 && fabs(eez)<10600) small =true;
  else  if(fabs(eez)>10600 && fabs(eez)<12000) small = false;
  return small;
}

///////////////////////////////////////////////////
bool Util :: isSmallInner(float brinnr){

  bool small = false;
  
  if (brinnr>1e-5) small = true;

  return small;

}

/////////////////////////////////////////////////
int Util :: GetPhiBinNumber(float phi, int sector){
  
  int phiBin24=-1;
  int Octant = (int)(phi/PI_OVER_4);
  double PhiInOctant = fabs(phi - Octant * PI_OVER_4);
  if (PhiInOctant > PI_OVER_8) PhiInOctant = PI_OVER_4 - PhiInOctant;
  if ( sector==0 ){//Small
    int OctantSmall = Octant;
    double PhiInOctantSmall = PhiInOctant;
    if(phi<0) PhiInOctantSmall = fabs(phi - (OctantSmall-1)*PI_OVER_4);
    phiBin24 = PhiInOctantSmall * PHI_RANGE;
  }
  else {//Large
    //phi = phi + PI_OVER_8;
    int OctantLarge = (int)(phi / PI_OVER_4);
    double PhiInOctantLarge = fabs(phi - OctantLarge * PI_OVER_4);
    if (phi<0) PhiInOctantLarge = fabs(phi - (OctantLarge-1)*PI_OVER_4);
    phiBin24 = PhiInOctantLarge * PHI_RANGE;
  }

  return phiBin24;
  
}

///////////////////////////////////////////////////////
float Util :: calcAlpha(float r1,float z1,float r2,float z2){
  float slope1 = r1/z1;
  float slope2 = (r2 - r1)/(z2 - z1);
  float alpha = 0;
  alpha = fabs(atan(slope1) - atan(slope2));
  return alpha;
}

///////////////////////////////////////////////////////
float Util :: calcIntercept(float r1,float z1,float r2,float z2){
  float slope = (r1-r2)/(z1-z2);
  float intercept = r1 - slope*z1;
  return intercept;
}

/////////////////////////////////////////////////////
bool Util :: isPassedSA(int thr, float eta, float phi, float pt){
  bool pass=false;
  float etaRegion[5] = {0, 1.05, 1.5, 2.0, 9.9};
  float SA_mu4[4] = {3.41, 3.21, 3.39, 3.53}; 
  float SA_mu6[4] = {5.04,  4.81,  5.01,  5.25}; 
  float SA_mu4_weakB[2] = {2.11, 2.97};
  float SA_mu6_weakB[2] = {4.37, 3.77};
  eta = fabs(eta);

  bool isWeakBFieldA=false;
  bool isWeakBFieldB=false;
  if(      ( 1.3 <= eta && eta < 1.45) &&
      ( (0                 <= fabs(phi) && fabs(phi) < PI/48. )     ||
        (PI*11./48. <= fabs(phi) && fabs(phi) < PI*13./48. ) ||
        (PI*23./48. <= fabs(phi) && fabs(phi) < PI*25./48. ) ||
        (PI*35./48. <= fabs(phi) && fabs(phi) < PI*37./48. ) ||
        (PI*47./48. <= fabs(phi) && fabs(phi) < PI )
      )
    ) isWeakBFieldA=true;

  else if( ( 1.5 <= eta && eta < 1.65 ) &&
      ( (PI*3./32.  <= fabs(phi) && fabs(phi) < PI*5./32. ) ||
        (PI*11./32. <= fabs(phi) && fabs(phi) < PI*13./32.) ||
        (PI*19./32. <= fabs(phi) && fabs(phi) < PI*21./32.) ||
        (PI*27./32. <= fabs(phi) && fabs(phi) < PI*29./32.)
      )
    ) isWeakBFieldB=true;

  if (isWeakBFieldA){
    if (thr==4 && pt>SA_mu4_weakB[0]) pass=true;
    else if (thr==6 && pt>SA_mu6_weakB[0]) pass=true;
  }
  else if (isWeakBFieldB){
    if (thr==4 && pt>SA_mu4_weakB[1]) pass=true;
    else if (thr==6 && pt>SA_mu6_weakB[1]) pass=true;
  }
  else {
    for (int i=0; i<4; i++){
      if (fabs(eta)>etaRegion[i] && fabs(eta)<etaRegion[i+1]){
        if (thr==4 && pt>SA_mu4[i]) pass = true;
        else if(thr==6 && pt>SA_mu6[i]) pass = true;
      }
    }
  }

  return pass;
}

/////////////////////////////////////////////////////
bool Util :: isPassedComb(int thr, float eta, float pt){
  bool pass=false;
  float etaRegion[5] = {0,1.05,1.5,2.0,9.9};
  float Comb_mu4[4] = {3.0, 2.5, 2.5, 2.5}; 
  float Comb_mu6[4] = {5.8, 5.8, 5.8, 5.6}; 
  
  for (int i=0; i<4; i++){
    if (fabs(eta)>etaRegion[i] && fabs(eta)<etaRegion[i+1]){
      if (thr==4 && pt>Comb_mu4[i]) pass = true;
      else if(thr==6 && pt>Comb_mu6[i]) pass = true;
    }
  }

  return pass;
}

/////////////////////////////////////////////////////
bool Util :: isPassedEF(int thr, float eta, float pt){
  bool pass=false;
  float etaRegion[5] = {0,1.05,1.5,2.0,9.9};
  float EF_mu4[4] = {3.94, 3.91, 3.77, 3.72}; 
  float EF_mu6[4] = {5.92,  5.86,  5.70,  5.64}; 
  float EF_mu10[4] = {9.84, 9.77, 9.54, 9.47}; 
  float EF_mu11[4] = {10.74, 10.64, 10.58, 10.53}; 
  float EF_mu14[4] = {13.75, 13.62, 13.38, 13.36}; 
  float EF_mu18[4] = {17.68, 17.51, 17.34, 17.34}; 
  float EF_mu20[4] = {19.65, 19.42, 19.16, 19.19}; 
  float EF_mu22[4] = {21.57, 21.32, 21.07, 21.11}; 
  float EF_mu24[4] = {23.53, 23.21, 22.99, 23.03}; 
  float EF_mu26[4] = {25.49, 25.15, 24.90, 24.95}; 
  float EF_mu28[4] = {27.23, 27.09, 27.07, 26.99}; 
  float EF_mu40[4] = {38.76, 38.54, 38.38, 38.31}; 
  float EF_mu50[4] = {45.00, 45.00, 45.00, 45.00};
  
  for (int i=0; i<4; i++){
    if (fabs(eta)>etaRegion[i] && fabs(eta)<etaRegion[i+1]){
      if (thr==4 && pt>EF_mu4[i]) pass = true;
      else if(thr==6 && pt>EF_mu6[i]) pass = true;
      else if(thr==10 && pt>EF_mu10[i]) pass = true;
      else if(thr==11 && pt>EF_mu11[i]) pass = true;
      else if(thr==14 && pt>EF_mu14[i]) pass = true;
      else if(thr==18 && pt>EF_mu18[i]) pass = true;
      else if(thr==20 && pt>EF_mu20[i]) pass = true;
      else if(thr==22 && pt>EF_mu22[i]) pass = true;
      else if(thr==24 && pt>EF_mu24[i]) pass = true;
      else if(thr==26 && pt>EF_mu26[i]) pass = true;
      else if(thr==28 && pt>EF_mu28[i]) pass = true;
      else if(thr==40 && pt>EF_mu40[i]) pass = true;
      else if(thr==50 && pt>EF_mu50[i]) pass = true;
    }
  }

  return pass;
}

///////////////////////////////////////////////////////
float Util :: whichSAPT(float alphapt,float betapt,float tgcpt,float innerspz,float middlespz,float outerspz, float tgcmid1z, float beta){

  bool useMDT=false;
  if (fabs(middlespz)>1e-5 && fabs(outerspz)>1e-5) useMDT=true;
  else{
    if (tgcpt>=8 || fabs(tgcmid1z)<1e-5){
      if(fabs(middlespz)>1e-5) useMDT=true;
    }
  }

  if (!useMDT) return tgcpt;
  
  float l2pt=alphapt;
  if (alphapt>10 && beta>1e-5){
    float ratio1 = fabs(betapt-alphapt)/alphapt;
    float ratio2 = fabs(tgcpt-alphapt)/alphapt;
    float ratio3 = fabs(tgcpt-betapt)/betapt;
    if (ratio1<0.5) l2pt=betapt;
    else if (fabs(outerspz)<1e-5){
      if (betapt>alphapt || ratio2>ratio3) l2pt=betapt;
    }
  }

  return l2pt;
}

/////////////////////////////////////////////////////////
float Util :: calcDrThrL1(float pt){
  float dr=-1;
  if (pt<10) dr = 0.6 - 0.03*pt ;
  else if (pt<30) dr = 0.4 - 0.01*pt;
  else dr = 0.1;
  return dr;
}

////////////////////////////////////////////////////
float Util :: calcDrThrL1LUT(float pt){
  float ans=0;
  if (fabs(pt)<10)
    ans = -0.035*pt + 0.5;
  else ans = 0.15;

  return ans;

}

//////////////////////////////////////////////////////////
float Util :: computeRadius3Points(float InnerZ, float InnerR,
                                    float EEZ, float EER,
                                    float MiddleZ, float MiddleR)
{
  float radius_EE;

  float a3;

  float m = 0.;
  float cost = 0.;
  float x0 = 0., y0 = 0., x2 = 0., y2 = 0., x3 = 0., y3 = 0.;
  float tm = 0.;

  a3 = ( MiddleZ - InnerZ ) / ( MiddleR - InnerR );

  m = a3;
  cost = cos(atan(m));
  x2 = EER - InnerR;
  y2 = EEZ - InnerZ;
  x3 = MiddleR - InnerR;
  y3 = MiddleZ - InnerZ;

  tm = x2;
  x2 = ( x2   + y2*m)*cost;
  y2 = (-tm*m + y2  )*cost;

  tm = x3;
  x3 = ( x3   + y3*m)*cost;
  y3 = (-tm*m + y3  )*cost;

  x0 = x3/2.;
  y0 = (y2*y2 + x2*x2 -x2*x3)/(2*y2);

  radius_EE = sqrt(x0*x0 + y0*y0);
  return radius_EE;
}

bool Util :: isBadPhi(int phibinall){
  //int badNum[18] = {67,68,69,70,76,77,78,113,114,115,118,119,120,167,168,172,173,174};//v1
  int badNum[19] = {68,69,70,71,75,76,77,78,113,114,115,118,119,120,167,168,169,173,174};//v2
  bool ans=false;
  for (int i=0;i<19;i++){
    if (phibinall==badNum[i])
      ans = true;
  }
  return ans;
}

int Util :: GetPhiBinAllNumber(float phi){
  if (phi<0) phi = phi + 2*PI;
  float phibin = (int) (phi * 96/PI);

  return phibin;
}

bool Util :: useSmall(int phibin){
  bool use=false;
  int smallbin[5] = {7,8,9,10,11};
  for (int i=0;i<5;i++){
    if (phibin==smallbin[i]) use=true;
  }
  return use;
}

float Util :: calcDistance(float x1,float y1,float x2,float y2,float x3,float y3){
  float xm1=(x1+x2)/2;
  float xm2=(x2+x3)/2;
  float ym1=(y1+y2)/2;
  float ym2=(y2+y3)/2;
  float a1=(x1-x2)/(y2-y1);
  float a2=(x2-x3)/(y3-y2);
  float x0=(a2*xm2-a1*xm1-ym2+ym1)/(a2-a1);//center of circle
  float y0=a1*(x0-xm1)+ym1;//center of circle
  float a = (x0-x1)/(y1-y0);//slope of sessen
  float b = y1+x1*(x1-x0)/(y1-y0);//intercept of sessen
  float d=fabs(b)/sqrt(a*a+1);
  return d;
}

double Util::dRL1byPt( double mupt ) {
  double dR = 0.15;
  if( mupt < 10. ) dR = 0.5 - 0.035*mupt;
  return dR;
}


//eof

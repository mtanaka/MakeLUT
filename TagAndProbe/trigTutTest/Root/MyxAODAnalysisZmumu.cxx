#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <AsgTools/ToolHandle.h>
#include "trigTutTest/MyxAODAnalysisZmumu.h"
#include <iostream>
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTrigMuon/L2CombinedMuonContainer.h"
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigMuon/TrigMuonDefs.h"
#include "xAODTrigger/MuonRoIContainer.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigDecisionTool/ChainGroupFunctions.h"
#include "TrigConfInterfaces/ITrigConfigTool.h"
#include "trigTutTest/Util.h"
#include "TTree.h"
#include "TH1.h"
#include "TLorentzVector.h"
#include "TSystem.h"
using namespace std;
using namespace Trig;
using namespace TrigConf;
using namespace xAOD;

#define PI 3.14159265258979

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysisZmumu)


MyxAODAnalysisZmumu :: MyxAODAnalysisZmumu ()
{
}

EL::StatusCode MyxAODAnalysisZmumu :: setupJob (EL::Job& job)
{

  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();
  xAOD::Init(); // call before opening first file


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisZmumu :: histInitialize ()
{

  tree = new TTree ("validationT", "validationT");
  tree->Branch("tag_offline_pt", &m_tag_offline_pt);
  tree->Branch("tag_offline_eta", &m_tag_offline_eta);
  tree->Branch("tag_offline_phi", &m_tag_offline_phi);
  tree->Branch("tag_offline_charge", &m_tag_offline_charge);
  tree->Branch("probe_offline_pt", &m_probe_offline_pt);
  tree->Branch("probe_offline_eta", &m_probe_offline_eta);
  tree->Branch("probe_offline_phi", &m_probe_offline_phi);
  tree->Branch("probe_offline_charge", &m_probe_offline_charge);
  tree->Branch("probe_roi_eta", &m_probe_roi_eta);
  tree->Branch("probe_roi_phi", &m_probe_roi_phi);
  tree->Branch("probe_roi_threshold", &m_probe_roi_threshold);
  tree->Branch("probe_sa_pt", &m_probe_sa_pt);
  tree->Branch("probe_sa_eta", &m_probe_sa_eta);
  tree->Branch("probe_sa_phi", &m_probe_sa_phi);
  tree->Branch("probe_sa_charge", &m_probe_sa_charge);
  tree->Branch("probe_sa_alpha_pt", &m_probe_sa_alpha_pt);
  tree->Branch("probe_sa_beta_pt", &m_probe_sa_beta_pt);
  tree->Branch("probe_sa_tgc_pt", &m_probe_sa_tgc_pt);
  tree->Branch("probe_sa_ec_radius_pt", &m_probe_sa_ec_radius_pt);
  tree->Branch("probe_sa_saddress", &m_probe_sa_saddress);
  tree->Branch("probe_sa_ec_alpha", &m_probe_sa_ec_alpha);
  tree->Branch("probe_sa_ec_beta", &m_probe_sa_ec_beta);
  tree->Branch("probe_sa_ec_radius", &m_probe_sa_ec_radius);
  tree->Branch("probe_sa_br_radius", &m_probe_sa_br_radius);
  tree->Branch("probe_sa_etaMap", &m_probe_sa_etaMap);
  tree->Branch("probe_sa_phiMap", &m_probe_sa_phiMap);
  tree->Branch("probe_sa_tgcInn_eta", &m_probe_sa_tgcInn_eta);
  tree->Branch("probe_sa_tgcInn_phi", &m_probe_sa_tgcInn_phi);
  tree->Branch("probe_sa_tgcMid1_r", &m_probe_sa_tgcMid1_r);
  tree->Branch("probe_sa_tgcMid1_z", &m_probe_sa_tgcMid1_z);
  tree->Branch("probe_sa_tgcMid1_eta", &m_probe_sa_tgcMid1_eta);
  tree->Branch("probe_sa_tgcMid1_phi", &m_probe_sa_tgcMid1_phi);
  tree->Branch("probe_sa_tgcMid2_r", &m_probe_sa_tgcMid2_r);
  tree->Branch("probe_sa_tgcMid2_z", &m_probe_sa_tgcMid2_z);
  tree->Branch("sp_r", &m_sp_r);
  tree->Branch("sp_z", &m_sp_z);

  tree->Print();

  wk()->addOutput(tree);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisZmumu :: fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisZmumu :: changeInput (bool firstFile)
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisZmumu :: initialize ()
{

  runLocal=true;
  doDebug=false;
  useGRL=true;
  doMatching=true;

  m_event = wk()->xaodEvent(); // you should have already added this as described before

  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", m_event->getEntries() ); // print long long int

  // count number of events
  m_eventCounter = 0;

  //xAODConfigTool configTool("xAODConfigTool");

  configTool = new xAODConfigTool("configTool");
  ToolHandle<TrigConf::ITrigConfigTool> configHandle(configTool);
  configHandle->initialize();

  trigDecTool = new TrigDecisionTool("TrigDecisionTool");
  trigDecTool->setProperty("ConfigTool",configHandle);
  trigDecTool->setProperty("TrigDecisionKey","xTrigDecision");
  trigDecTool->initialize();

  m_util.initialize();

  // GRL
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  const char*GRLFilePath;
  if(runLocal){//run local
    GRLFilePath = 
      "/afs/cern.ch/user/a/atlasdqm/grlgen/All_Good/data16_13TeV.periodAllYear_DetStatus-v81-pro20-09_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml";
  }
  else {//run grid
    GRLFilePath = 
      "__panda_rootCoreWorkDir/trigTutTest/share/data16_13TeV.periodAllYear_DetStatus-v81-pro20-09_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml";
  }
  const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(fullGRLFilePath);
  m_grl->setProperty( "GoodRunsListVec", vecStringGRL);
  m_grl->setProperty("PassThrough", false); // if true (default) will ignore result of GRL and will just pass all events
  bool scgrl = m_grl->initialize();
  if (!scgrl){
    cout << "ERROR: Following GRL is not found" << endl;
    cout << GRLFilePath << endl;
    return EL::StatusCode::FAILURE;
  }


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisZmumu :: execute ()
{

  // print every 100 events, so we know where we are:
  if( (m_eventCounter % 10000) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  auto chainGroup = trigDecTool->getChainGroup(".*");
  for(auto &trig : chainGroup->getListOfTriggers()) {
    auto cg = trigDecTool->getChainGroup(trig);
    if (cg->isPassed()) {
      triggerCounts[trig]++;
    } else {
      triggerCounts[trig] += 0;
    }
  }

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  if( ! m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
    Error("execute()", "Failed to retrieve event info collection. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  bool isMC = false;
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) isMC = true; // can do something with this later

  // if data check if event passes GRL
  if(useGRL){
    if(!isMC)// it's data!
      if(!m_grl->passRunLB(*eventInfo)) return EL::StatusCode::SUCCESS; // go to next event
  }

  const MuonContainer* offmuons=0;
  const MuonRoIContainer* L1muons=0;
  const L2StandAloneMuonContainer* SAmuons=0;
  const L2CombinedMuonContainer* COMBmuons=0;
  const MuonContainer* EFmuons=0;
  m_event->retrieve(offmuons, "Muons");
  m_event->retrieve(L1muons, "LVL1MuonRoIs");
  m_event->retrieve(SAmuons, "HLT_xAOD__L2StandAloneMuonContainer_MuonL2SAInfo");
  m_event->retrieve(COMBmuons, "HLT_xAOD__L2CombinedMuonContainer_MuonL2CBInfo");
  m_event->retrieve(EFmuons, "HLT_xAOD__MuonContainer_MuonEFInfo");
  MuonContainer::const_iterator offmuon1_itr = offmuons->begin();
  MuonContainer::const_iterator offmuon2_itr = offmuons->begin();
  MuonContainer::const_iterator offmuon_begin = offmuons->begin();
  MuonContainer::const_iterator offmuon_end = offmuons->end();

  float offmuon_pt[2], offmuon_eta[2], offmuon_phi[2], offmuon_charge[2];
  const float muon_mass = 0.1056;//GeV

  if (offmuons->size()<2) return EL::StatusCode::SUCCESS; 
  for ( ;offmuon1_itr<offmuon_end; ++offmuon1_itr){
    for (offmuon2_itr=offmuon_begin; offmuon2_itr<offmuon1_itr; ++offmuon2_itr){
      if ((*offmuon1_itr)->author()!=1 || (*offmuon2_itr)->author()!=1) continue;//Require MuidCo
      if ((*offmuon1_itr)->muonType()!=0 || (*offmuon2_itr)->muonType()!=0) continue;//Require Combined
      if ((*offmuon1_itr)->quality()>1.5 || (*offmuon2_itr)->quality()>1.5) continue;//Require tight or medium quality
      offmuon_pt[0] = (*offmuon1_itr)->pt()/1000.;//GeV
      offmuon_pt[1] = (*offmuon2_itr)->pt()/1000.;//GeV
      offmuon_eta[0] = (*offmuon1_itr)->eta();
      offmuon_eta[1] = (*offmuon2_itr)->eta();
      if (fabs(offmuon_eta[0])>2.5 || fabs(offmuon_eta[1])>2.5) continue; //eta cut
      offmuon_phi[0] = (*offmuon1_itr)->phi();
      offmuon_phi[1] = (*offmuon2_itr)->phi();
      float dphi = acos(cos(offmuon_phi[0]-offmuon_phi[1]));
      if(fabs(offmuon_eta[0])<1.05 && fabs(offmuon_eta[1]<1.05 && fabs(dphi-PI)<0.1)) continue;//aboid vias in barrel feet region
      offmuon_charge[0] = (*offmuon1_itr)->charge();
      offmuon_charge[1] = (*offmuon2_itr)->charge();
      if (offmuon_charge[0]*offmuon_charge[1]>0) continue;//opossite charge cut
      TLorentzVector muon[2];
      muon[0].SetPtEtaPhiM(offmuon_pt[0],offmuon_eta[0],offmuon_phi[0],muon_mass);
      muon[1].SetPtEtaPhiM(offmuon_pt[1],offmuon_eta[1],offmuon_phi[1],muon_mass);
      float mass = (muon[0]+muon[1]).M();
      if (mass<80 || mass>100) continue;//Z mass cut

      int ov_roi_number[2] = {-1,-1};
      for (int itag=0; itag<2; itag++){//tag is muon1 or muon2
        //////////////////check tag muon
        if(doDebug) cout << "tag offline pt=" << offmuon_pt[itag] << endl;
        auto fcTag = trigDecTool->features("HLT_mu26_ivarmedium",TrigDefs::alsoDeactivateTEs);
        auto fcTagSA = fcTag.containerFeature<xAOD::L2StandAloneMuonContainer>("",TrigDefs::alsoDeactivateTEs);
        auto fcTagComb = fcTag.containerFeature<xAOD::L2CombinedMuonContainer>("",TrigDefs::alsoDeactivateTEs);
        auto fcTagEF = fcTag.containerFeature<xAOD::MuonContainer>("",TrigDefs::alsoDeactivateTEs);
        bool passL1_tag = false;
        bool passSA_tag = false;
        bool passComb_tag = false;
        bool passEF_tag = false;
        int matchRoiNum_tag = -1;

        if (offmuon_pt[itag]<26) continue;

        //////L1
        MuonRoIContainer::const_iterator roi_itr = L1muons->begin();
        MuonRoIContainer::const_iterator roi_end = L1muons->end();
        float minDrL1_tag=1000.;
        int min_roi_thrnum_tag=-1;
        float matching_dr_tag = (doMatching)? m_util.dRL1byPt(offmuon_pt[itag]) : 1000000;
        for (;roi_itr!=roi_end;++roi_itr){
          float roi_eta = (*roi_itr)->eta();
          float  roi_phi = (*roi_itr)->phi();
          int roi_number = (*roi_itr)->getRoI();
          int roi_thrnum = (*roi_itr)->getThrNumber();
          float dr = m_util.calcDr(roi_eta,offmuon_eta[itag],roi_phi,offmuon_phi[itag]);
          if (dr<minDrL1_tag){
            matchRoiNum_tag = roi_number;
            minDrL1_tag = dr;
            min_roi_thrnum_tag=roi_thrnum;
          }
        }
        if (doDebug) cout << "matchRoiNum_tag=" << matchRoiNum_tag << endl;
        if (doDebug) cout << "min_roi_thrun_tag=" << min_roi_thrnum_tag << endl;
        if ((minDrL1_tag < matching_dr_tag) && min_roi_thrnum_tag>=6) passL1_tag=true;
        if(doMatching)
          if (!passL1_tag) continue;
        if(doDebug) cout << "tag L1 pass" << endl;
        ov_roi_number[itag] = matchRoiNum_tag;

        ////////SA
        for (auto &fcTagsa : fcTagSA){
          const HLT::TriggerElement* tesa = ( fcTagsa.te() );
          const L2StandAloneMuonContainer* contsa = fcTagsa.cptr();
          for (auto samuon : *contsa) {
            int sa_roinum = samuon->roiNumber();
            if (tesa->getActiveState() && sa_roinum==matchRoiNum_tag) passSA_tag=true;
          }
        }
        if(doMatching)
          if (!passSA_tag)continue;
        if(doDebug) cout << "tag SA pass" << endl;

        //////////Comb
        float minDrComb_tag=1000.;
        bool minPassComb_tag=false;
        for (auto &fcTagcomb : fcTagComb){
          const HLT::TriggerElement* tecomb = ( fcTagcomb.te() );
          const L2CombinedMuonContainer* contcomb = fcTagcomb.cptr();
          for (auto combmuon : *contcomb) {
            float comb_pt = fabs(combmuon->pt());
            float comb_eta = combmuon->eta();
            float comb_phi = combmuon->phi();
            float dr = m_util.calcDr(comb_eta,offmuon_eta[itag],comb_phi,offmuon_phi[itag]);
            if (dr<minDrComb_tag){
              minDrComb_tag=dr;
              minPassComb_tag=tecomb->getActiveState();
            }
          }
        }
        if(minDrComb_tag<0.01 && minPassComb_tag) passComb_tag=true;
        if(doMatching)
          if (!passComb_tag) continue;
        if(doDebug)cout << "tag Comb pass" << endl;

        ///////EF
        float minDrEF_tag=1000.;
        bool minPassEF_tag=false;
        for (auto &fcTagef : fcTagEF){
          const HLT::TriggerElement* teef = ( fcTagef.te() );
          const MuonContainer* contef = fcTagef.cptr();
          for (auto efmuon : *contef) {
            float ef_pt = efmuon->pt()/1000.;
            float ef_eta = efmuon->eta();
            float ef_phi = efmuon->phi();
            float dr = m_util.calcDr(ef_eta,offmuon_eta[itag],ef_phi,offmuon_phi[itag]);
            if (dr<minDrEF_tag){
              minDrEF_tag=dr;
              minPassEF_tag=m_util.isPassedEF(24,ef_eta,ef_pt);
            }
          }
        }
        if (minDrEF_tag<0.01 && minPassEF_tag) passEF_tag=true;
        if(doMatching)
          if (!passEF_tag) continue ;
        if(doDebug)cout << "tag EF pass" << endl; 

        if(doDebug) cout << "tag muon exist!" << endl;
        int iprobe = (itag==0)? 1 : 0;
        float probe_sa_pt=0.,probe_comb_pt=0.,probe_ef_pt=0.;
        float probe_sa_eta=0., probe_sa_phi=0., probe_comb_eta=0., probe_comb_phi=0., probe_ef_eta=0., probe_ef_phi=0.;
        float probe_roi_eta=0., probe_roi_phi=0.;
        int probe_roi_threshold=0;
        int probe_sa_charge=0, probe_comb_charge=0, probe_ef_charge=0,probe_sa_saddress=-2;
        float probe_sa_alpha_pt=0,probe_sa_beta_pt=0,probe_sa_tgc_pt=0,probe_sa_ec_radius_pt=0;
        float probe_sa_ec_alpha=0,probe_sa_ec_beta=0,probe_sa_ec_radius=0,probe_sa_br_radius=0;
        float probe_sa_etaMap=0,probe_sa_phiMap=0,probe_sa_tgcInn_eta=0,probe_sa_tgcInn_phi=0;
        float probe_sa_tgcMid1_eta=0,probe_sa_tgcMid1_phi=0,probe_sa_tgcMid1_r=0,probe_sa_tgcMid1_z=0;
        float probe_sa_tgcMid2_r=0,probe_sa_tgcMid2_z=0;
        vector<float> sp_r,sp_z;

        if(doDebug) cout << "probe offline pt=" << offmuon_pt[iprobe] << endl;
        auto fcProbe = trigDecTool->features("HLT_mu0_perf",TrigDefs::alsoDeactivateTEs);
        auto fcProbeSA = fcProbe.containerFeature<xAOD::L2StandAloneMuonContainer>("",TrigDefs::alsoDeactivateTEs);

        //L1
        int matchRoiNum_probe = -1, min_matchRoiNum_probe = -1, min_roi_thrNum_pr=0;
        float minDrL1_probe=1000., min_probe_roi_eta=1000., min_probe_roi_phi=1000.;
        float matching_dr_probe = (doMatching)? m_util.dRL1byPt(offmuon_pt[iprobe]) : 1000000; 
        MuonRoIContainer::const_iterator roi_pr_itr = L1muons->begin();
        for (;roi_pr_itr!=roi_end;++roi_pr_itr){
          float roi_eta = (*roi_pr_itr)->eta();
          float  roi_phi = (*roi_pr_itr)->phi();
          int roi_number = (*roi_pr_itr)->getRoI();
          int roi_thrnum = (*roi_pr_itr)->getThrNumber();
          float dr = m_util.calcDr(roi_eta,offmuon_eta[iprobe],roi_phi,offmuon_phi[iprobe]);
          if (dr<minDrL1_probe && roi_number!=matchRoiNum_tag){
            minDrL1_probe=dr;
            min_roi_thrNum_pr=roi_thrnum;
            min_matchRoiNum_probe = roi_number;
            min_probe_roi_eta = roi_eta;
            min_probe_roi_phi = roi_phi;
          }
        }
        if (minDrL1_probe < matching_dr_probe){
          matchRoiNum_probe = min_matchRoiNum_probe;
          probe_roi_eta = min_probe_roi_eta;
          probe_roi_phi = min_probe_roi_phi;
          probe_roi_threshold = min_roi_thrNum_pr;
        }

        ////////SA
        for (auto &fcProbesa : fcProbeSA){
          const HLT::TriggerElement* tesa = ( fcProbesa.te() );
          const L2StandAloneMuonContainer* contsa = fcProbesa.cptr();
          for (auto samuon : *contsa) {
            int sa_roinum = samuon->roiNumber();
            float sa_pt = fabs(samuon->pt());
            float sa_eta = samuon->eta();
            float sa_phi = samuon->phi();
            int sa_charge = (samuon->pt()>0)? 1 : -1;

            if (sa_roinum==matchRoiNum_probe) {
              probe_sa_pt = sa_pt;
              probe_sa_eta = sa_eta;
              probe_sa_phi = sa_phi;
              probe_sa_charge = sa_charge;
              probe_sa_alpha_pt = samuon->ptEndcapAlpha();
              probe_sa_beta_pt = samuon->ptEndcapBeta();
              probe_sa_tgc_pt = samuon->tgcPt();
              probe_sa_ec_radius_pt = samuon->ptEndcapRadius();
              probe_sa_saddress = samuon->sAddress();
              probe_sa_ec_alpha = samuon->endcapAlpha();
              probe_sa_ec_beta = samuon->endcapBeta();
              probe_sa_ec_radius = samuon->endcapRadius();
              probe_sa_br_radius = samuon->barrelRadius();
              probe_sa_etaMap = samuon->etaMap();
              probe_sa_phiMap = samuon->phiMap();
              probe_sa_tgcInn_eta = samuon->tgcInnEta();
              probe_sa_tgcInn_phi = samuon->tgcInnPhi();
              probe_sa_tgcMid1_r = samuon->tgcMid1R();
              probe_sa_tgcMid1_z = samuon->tgcMid1Z();
              probe_sa_tgcMid1_eta = samuon->tgcMid1Eta();
              probe_sa_tgcMid1_phi = samuon->tgcMid1Phi();
              probe_sa_tgcMid2_r = samuon->tgcMid2R();
              probe_sa_tgcMid2_z = samuon->tgcMid2Z();
              sp_r.clear();sp_z.clear();
              for(int ic=0;ic<10;ic++){
                sp_r.push_back(samuon->superPointR(ic));
                sp_z.push_back(samuon->superPointZ(ic));
              }
            }
          }
        }

        m_tag_offline_pt = offmuon_pt[itag];
        m_tag_offline_eta = offmuon_eta[itag];
        m_tag_offline_phi = offmuon_phi[itag];
        m_tag_offline_charge = offmuon_charge[itag];
        m_probe_offline_pt = offmuon_pt[iprobe];
        m_probe_offline_eta = offmuon_eta[iprobe];
        m_probe_offline_phi = offmuon_phi[iprobe];
        m_probe_offline_charge = offmuon_charge[iprobe];
        m_probe_roi_eta = probe_roi_eta;
        m_probe_roi_phi = probe_roi_phi;
        m_probe_roi_threshold = probe_roi_threshold;
        m_probe_sa_pt = probe_sa_pt;
        m_probe_sa_eta = probe_sa_eta;
        m_probe_sa_phi = probe_sa_phi;
        m_probe_sa_charge = probe_sa_charge;
        m_probe_sa_alpha_pt = probe_sa_alpha_pt;
        m_probe_sa_beta_pt = probe_sa_beta_pt;
        m_probe_sa_tgc_pt = probe_sa_tgc_pt;
        m_probe_sa_ec_radius_pt = probe_sa_ec_radius_pt;
        m_probe_sa_saddress = probe_sa_saddress;
        m_probe_sa_ec_alpha = probe_sa_ec_alpha;
        m_probe_sa_ec_beta = probe_sa_ec_beta;
        m_probe_sa_ec_radius = probe_sa_ec_radius;
        m_probe_sa_br_radius = probe_sa_br_radius;
        m_probe_sa_etaMap = probe_sa_etaMap;
        m_probe_sa_phiMap = probe_sa_phiMap;
        m_probe_sa_tgcInn_eta = probe_sa_tgcInn_eta;
        m_probe_sa_tgcInn_phi = probe_sa_tgcInn_phi;
        m_probe_sa_tgcMid1_eta = probe_sa_tgcMid1_eta;
        m_probe_sa_tgcMid1_phi = probe_sa_tgcMid1_phi;
        m_probe_sa_tgcMid1_r = probe_sa_tgcMid1_r;
        m_probe_sa_tgcMid1_z = probe_sa_tgcMid1_z;
        m_probe_sa_tgcMid2_r = probe_sa_tgcMid2_r;
        m_probe_sa_tgcMid2_z = probe_sa_tgcMid2_z;
        m_sp_r.clear();m_sp_z.clear();
        for(int ich=0;ich<sp_r.size();ich++){
          m_sp_r.push_back(sp_r[ich]);
          m_sp_z.push_back(sp_z[ich]);
        }
        tree->Fill();
      }//tag or probe
    }//loop of offline muon2
  }//loop of offline muon1

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisZmumu :: postExecute ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisZmumu :: finalize ()
{
  cout << "finalize" << endl;

  //cout << "Final trigger tally" << endl;
  //for (auto &i : triggerCounts) cout << "  " << i.first << ": " << i.second << " times" << endl;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysisZmumu :: histFinalize ()
{
  cout << "histFInalize" << endl;
  return EL::StatusCode::SUCCESS;
}


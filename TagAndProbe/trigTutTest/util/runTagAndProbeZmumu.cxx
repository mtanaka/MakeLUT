//
// Look at all the triggers that have fired in a app.
//

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"

#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/OutputStream.h"
//#include "EventLoopAlgs/NTupleSvc.h"
#include "EventLoop/Algorithm.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODMuon/MuonContainer.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#include "trigTutTest/MyxAODAnalysisZmumu.h"

#include "TSystem.h"

#include <iostream>

using namespace std;
using namespace Trig;
using namespace TrigConf;
using namespace xAOD;

// Config
const char* APP_NAME = "runTagAndProbeZmumu";
const char* OUTPUT_FILE = "output/aho.root";

int main( int argc, char* argv[] ) {

  // Initialize (as done for all xAOD standalone programs!)
  RETURN_CHECK (APP_NAME, xAOD::Init(APP_NAME));

  SH::SampleHandler sh;

  //data15
  //const char* inputFilePath = gSystem->ExpandPathName ("/home/mtanaka/WorkSpace/data15_13TeV_DAOD_MUON0/data15_13TeV.00282784.physics_Main.merge.DAOD_MUON0.r7562_p2521_p2510_tid07706227_00");
  //SH::ScanDir().sampleDepth(0).samplePattern("DAOD_MUON0.07706227._000001.pool.root.1").scan(sh, inputFilePath);

  //data16
  const char* inputFilePath = gSystem->ExpandPathName ("/home/mtanaka/WorkSpace/data/data16_13TeV/data16_13TeV.00298687.physics_Main.merge.DAOD_MUON0.f697_m1588_p2623_tid08384499_00");
  SH::ScanDir().sampleDepth(0).samplePattern("DAOD_MUON0.08384499._000001.pool.root.1").scan(sh, inputFilePath);

  //MC
  //const char* inputFilePath = gSystem->ExpandPathName ("/home/mtanaka/WorkSpace/mc/mc15_13TeV/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_MUON0.e3601_s2576_s2132_r7725_r7676_p2616_tid08199723_00");
  //SH::ScanDir().sampleDepth(0).samplePattern("DAOD_MUON0.08199723._000019.pool.root.1").scan(sh, inputFilePath);

  sh.setMetaString ( "nc_tree", "CollectionTree" );

  sh.print ();

  EL::Job job;
  job.sampleHandler ( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, 10000);

  MyxAODAnalysisZmumu* alg = new MyxAODAnalysisZmumu();
  job.algsAdd ( alg );
  
  EL::DirectDriver driver;
  driver.submit (job, "submitDirZmumu");

  return 0;
}

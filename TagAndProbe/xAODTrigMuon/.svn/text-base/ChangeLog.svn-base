2015-10-22 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Added variables for delta pT calculation.
	* Tagging as xAODTrigMuon-00-02-08

2015-10-22 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Modified type of TGC hit variables.
	* Tagging as xAODTrigMuon-00-02-07

2015-10-21 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Modified type of variables (bug fix).
	* Tagging as xAODTrigMuon-00-02-06

2015-10-19 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Modified the name and type of variables.
	* Tagging as xAODTrigMuon-00-02-05

2015-10-16 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Added RPC hit variables
	* Tagging as xAODTrigMuon-00-02-04

2015-10-14 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Added BEE and BME chambers to enum in TrigMuonDefs.cxx
	* Tagging as xAODTrigMuon-00-02-03

2015-10-08 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Added class ID to version 2 of L2StandAloneMuon
	* Tagging as xAODTrigMuon-00-02-02

2015-10-07 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Added CSC variables to L2StandAloneMuon_v2
	* Tagging as xAODTrigMuon-00-02-01

2015-10-02 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Added L2StandAloneMuon_v2
	* Tagging as xAODTrigMuon-00-02-00

2015-09-21 Masaki Ishitsuka <ishitsuka@hep.phys.titech.ac.jp>
	* Added CLID generation
	* Tagging as xAODTrigMuon-00-01-22

2015-06-10 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Fixed typo reported in JIRA ATR-11475
	* Tagging as xAODTrigMuon-00-01-21

2015-04-18 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Fixed typo in function and variable names
	* Tagging as xAODTrigMuon-00-01-20

2015-04-01  scott snyder  <snyder@bnl.gov>

	* Tagging xAODTrigMuon-00-01-19.
	* xAODTrigMuon/versions/L2CombinedMuon_v1.h,
	Root/L2CombinedMuon_v1.cxx: operator<< should be in the same
	namespace as its argument.

2015-03-09 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Remove extra space in variable name (bug fix)
	* Tagging as xAODTrigMuon-00-01-18

2015-01-29 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Add xAODTracking to Makefile.RootCore
	* Tagging as xAODTrigMuon-00-01-17

2014-12-08 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Made it possible to compile the package in standalone mode
	  as well. (Needed the "usual" Eigen tweak in the dictionary
	  generation.)
	* Tagging as xAODTrigMuon-00-00-16

2014-11-27  Gordon Watts  <gwatts@lxplus0227.cern.ch>

	* Make sure to use xAODCore instead of SGTools for CLASS DEF includes.
	* Tagged as xAODTrigMuon-00-00-15

2014-11-26  Gordon Watts  <gwatts@lxplus0105.cern.ch>

	* Changed to use versionless object names
	* Tagged as xAODTrigMuon-00-00-13
	* Removed XAOD_STANDALONE macro
	* Tagged as xAODTrigMuon-00-00-14

2014-11-14 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Add new variables in L2StandAloneMuon to store track positions
	* Tagged as xAODTrigMuon-00-00-11

2014-11-13 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Modified type of variables to be consistent in all codes
	* Tagged as xAODTrigMuon-00-00-10

2014-09-21 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Add deltaPt variable to store error on pT calculation
	* Tagged as xAODTrigMuon-00-00-09

2014-09-11 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Change the definition of road variables to be consistent with super point.
	3 station was assumed in previous definition but it is now extended to include
	CSC and EE chambers. Numbers are defined in enumerator in TrigMuonDefs.h.
	* Tagged as xAODTrigMuon-00-00-08

2014-09-10 Masaki Ishitsuka <masaki.ishitsuka@cern.ch>
	* Add TrigMuonDefs.h to define L2 muon parameters (enumerators)
	* Tagged as xAODTrigMuon-00-00-07

2014-09-06 David Quarrie <David.Quarrie@cern.ch>
	* cmt/requirements
		Specify the required ROOT components (cmake-specific)
	* Tagged as xAODTrigMuon-00-00-06

2014-08-28 Masaki Ishitsuka <ishitsuka@phys.titech.ac.jp>
	* Added pT from different algorithms in L2MuonSA to L2StandAloneMuon.
2014-08-19 Masaki Ishitsuka <ishitsuka@phys.titech.ac.jp>
	* Added remaining variables in MuonFeature and MuonFeatureDetails
	  to L2StandAloneMuon.
	* Tagging as xAODTrigMuon-00-00-05
2014-07-09 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* L2IsoMuon_v1::type() and L2CombinedMuon_v1::type() now return
	  the new, correct values coming from xAODBase.
	* Needs xAODBase-00-00-20 or newer.
	* Tagging as xAODTrigMuon-00-00-04

2014-07-09 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Making the code compile in standalone mode by using the
	  XAOD_STANDALONE definition correctly in the new headers.
	* Changing the type of "charge" to float in the new classes.
	* Modifying the capitalisation of the setter function names in
	  the new classes so that they adhere to the xAOD naming
	  convention.
	* Added functions to L2CombinedMuon that return bare pointers
	  for the child objects.
	* Tagging as xAODTrigMuon-00-00-03

2014-06-30 Stefano Giagu <stefano.gaigu@cern.ch>
	* Commit L2IsoMon and L2CombinedMuon classes & containers
	* Tagging as xAODTrigMuon-00-00-02

2014-04-11 Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
	* Cleaned up the code to actually behave as an xAOD EDM should.
	* Removed all the persistent variables from xAOD::L2StandAloneMuon_v1.
	* Changed the behaviour of the super-point handling functions a bit.
	* Changed the list of smart pointer dictionaries made, to reflect
	  what kind of types users should actually think about using.
	* Added an empty Doxygen main page that should be filled with
	  information.
	* Updated the build rules for the standalone compilation as well, but
	  didn't test that yet.
	* Tagging as xAODTrigMuon-00-00-01

2014-02-XX Masaki Ishitsuka <ishitsuk@phys.titech.ac.jp>
	* Put L2StandAloneMuon and container class with minimum variables.
	* Tagging as xAODTrigMuon-00-00-XX

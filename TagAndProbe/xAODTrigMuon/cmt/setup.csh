# echo "setup xAODTrigMuon xAODTrigMuon-00-02-08 in /home/mtanaka/WorkSpace/anal-new/Event/xAOD"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /afs/cern.ch/sw/contrib/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtxAODTrigMuontempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtxAODTrigMuontempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=xAODTrigMuon -version=xAODTrigMuon-00-02-08 -path=/home/mtanaka/WorkSpace/anal-new/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODTrigMuontempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=xAODTrigMuon -version=xAODTrigMuon-00-02-08 -path=/home/mtanaka/WorkSpace/anal-new/Event/xAOD  -quiet -without_version_directory -no_cleanup $* >${cmtxAODTrigMuontempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtxAODTrigMuontempfile}
  unset cmtxAODTrigMuontempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtxAODTrigMuontempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtxAODTrigMuontempfile}
unset cmtxAODTrigMuontempfile
exit $cmtsetupstatus


# echo "cleanup xAODMuon xAODMuon-00-17-04 in /home/mtanaka/Event/xAOD"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtxAODMuontempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtxAODMuontempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=xAODMuon -version=xAODMuon-00-17-04 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory $* >${cmtxAODMuontempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=xAODMuon -version=xAODMuon-00-17-04 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory $* >${cmtxAODMuontempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtxAODMuontempfile}
  unset cmtxAODMuontempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtxAODMuontempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtxAODMuontempfile}
unset cmtxAODMuontempfile
return $cmtcleanupstatus


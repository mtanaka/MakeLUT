/**
@mainpage xAODMuon package

@author Edward Moyse <Edward.Moyse@cern.ch>

$Revision:$
$Date:$

@section xAODMuonOverview Overview

This package holds the data model to describe the output of the
muon combined performance reconstruction for analysis users.

@section xAODMuonClasses Classes

The main classes of the package are the following:
  - xAOD::Muon: Class describing a chaged track.
  - xAOD::Segment: Class describing a neutral particle;

@htmlinclude used_packages.html

@include requirements
*/

# echo "cleanup xAODTracking xAODTracking-00-13-21 in /home/mtanaka/Event/xAOD"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtxAODTrackingtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtxAODTrackingtempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=xAODTracking -version=xAODTracking-00-13-21 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory $* >${cmtxAODTrackingtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=xAODTracking -version=xAODTracking-00-13-21 -path=/home/mtanaka/Event/xAOD  -quiet -without_version_directory $* >${cmtxAODTrackingtempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtxAODTrackingtempfile}
  unset cmtxAODTrackingtempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtxAODTrackingtempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtxAODTrackingtempfile}
unset cmtxAODTrackingtempfile
exit $cmtcleanupstatus


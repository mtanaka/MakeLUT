/**
   @mainpage xAODTracking package

   @author Edward Moyse <Edward.Moyse@cern.ch>
   @author Andreas Salzburger <Andreas.Salzburger@cern.ch>
   @author Markus Elsing <Markus.Elsing@cern.ch>
   @author Ruslan Mashinistov <Ruslan.Mashinistov@cern.ch>
   @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>

   $Revision$
   $Date$

   @section xAODTrackingOverview Overview

   This package holds the data model to describe the output of the
   tracking/vertexing reconstruction for analysis users.

   @section xAODTrackingClasses Classes

   The main classes of the package are the following:
     - xAOD::TrackParticle: Class describing a chaged track.
     - xAOD::NeutralParticle: Class describing a neutral particle;
     - xAOD::Vertex: Class describing a reconstructed vertex.

   @htmlinclude used_packages.html

   @include requirements
*/

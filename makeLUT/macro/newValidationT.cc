#define newValidationT_cxx
#include "macro/newValidationT.h"
#include <TH2.h>
#include "TF1.h"
#include "TGraphErrors.h"
#include "TLegend.h"
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

/////////////////////////////////////////////////
void newValidationT::Loop(string list,string output)
{
  TChain *chain = new TChain("validationT");
  ifstream finlist(list.c_str());
  string file_rec;
  while(finlist>>file_rec) chain->Add(file_rec.c_str());
  TTree *tree = static_cast<TTree*>(chain);
  Init(tree);

  TFile *fout = new TFile(output.c_str(),"recreate");
  
  ///Initialize

  ///////////////set region ///////////////////
  int phibinmax_ec = 12;
  int phibinmin_ec = 0;
  int phinumber = phibinmax_ec - phibinmin_ec + 1;
  int divetamax_ec = 30;
  int divetamin_ec = 0;
  int etanumber = divetamax_ec - divetamin_ec + 1;
  int phibinmax_br = 30;
  int divetamax_br = 30;

  gStyle->SetLabelSize(0.05,"x");
  gStyle->SetLabelSize(0.05,"y");
  gStyle->SetTitleSize(0.05,"x");
  gStyle->SetTitleSize(0.05,"y");
  gStyle->SetTitleOffset(1.5,"y");
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetOptStat(0);

  ////////////////////////////////////////////
  //endcap
  stringstream Alphadrawname,TgcAlphadrawname,Betadrawname;
  stringstream EndcapRadiusdrawname,EndcapRadiusNoSLdrawname,EndcapRadiusAllPhiNoQetadrawname;
  stringstream EndcapRadiusLineardrawname,EndcapRadiusAnormaldrawname,EndcapRadiusShiftdrawname;
  stringstream EndcapSagittaAnormaldrawname,BarrelSagittadrawname,LSSagittadrawname;
  stringstream iphi_ec,ieta_ec,iphi_ee,ieta_ee;
  //barrel
  TH2F *hMDT_invpTvsinvRadius[2][4][divetamax_br][phibinmax_br];

  stringstream BarrelRadiusdrawname;
  stringstream iphi_br,ieta_br;

    //barrel
    for(int k=0; k<2; k++){//charge
      for (int chamber=0; chamber<4; chamber++){//saddress
        for(int i=0; i<divetamax_br; i++){
          for(int j=0; j<phibinmax_br; j++){
            if(i < 10) ieta_br << "0" << i;
            else ieta_br << i;
            if(j < 10) iphi_br << "0" << j;
            else iphi_br << j;
            BarrelRadiusdrawname << "Radius" << k << chamber << ieta_br.str().c_str() << iphi_br.str().c_str();
            hMDT_invpTvsinvRadius[k][chamber][i][j] = new TH2F(BarrelRadiusdrawname.str().c_str(),"",500,0,500000,120,0,60);
            hMDT_invpTvsinvRadius[k][chamber][i][j]->SetMarkerStyle(8);
            hMDT_invpTvsinvRadius[k][chamber][i][j]->GetXaxis()->SetRangeUser(0,500000);
            hMDT_invpTvsinvRadius[k][chamber][i][j]->GetYaxis()->SetRangeUser(0,60);
            BarrelRadiusdrawname.str("");
            iphi_br.str("");
            ieta_br.str("");
          }
        }
      }
    }

  ////////////////////////////////////////////////
  float EtaMin[4] = {-1.145, -1.150, -1.050, -1.050};
  float PhiMin[4] = {-0.230, -0.233, -0.181, -0.181};
  float EtaMax[4] = {1.145, 1.150, 1.050, 1.050};
  float PhiMax[4] = {0.230, 0.233, 0.181, 0.181};
  float EtaStep[4], PhiStep[4];
  for (int i=0; i<4; i++){
    EtaStep[i] = (EtaMax[i]-EtaMin[i])/30;
    PhiStep[i] = (PhiMax[i]-PhiMin[i])/30;
  }

  /////////////////////////////////////
  int nentries = chain->GetEntries();
  cout << "Number of events is " << nentries << endl;
  for (int jentry=0; jentry<nentries;jentry++) {
    if (jentry%10000000==0) cout << "entry=" << jentry << endl;
    if(jentry==10000000) break;
    chain->GetEntry(jentry); 
    
    ////////////////////////////////////////

    float tgcalpha = (fabs(tgcMid1_z)>1e-5 && fabs(tgcMid2_z)>1e-5) ? m_util.calcAlpha(tgcMid1_r,tgcMid1_z,tgcMid2_r,tgcMid2_z) : 0;
    bool isEndcap = (L2_saddress<-0.5)? true : false;
    float center_phi = m_util.getChamberCenterPhi(tgcMid1_phi);
    float cosphidif = m_util.cosAminusB(tgcMid1_phi,center_phi);

    ////////////////////////////////////////////////////////////////////////
    if (isEndcap){ //endcap
    }
    else{//barrel
      int diveta_br = (int)((L2_etaMap - EtaMin[L2_saddress])/EtaStep[L2_saddress]);
      int phiBin_br = (int)((L2_phiMap - PhiMin[L2_saddress])/PhiStep[L2_saddress]);
      int icharge = (L2_charge>0)? 1 : 0;
      int iphi = (L2_phi < -1.5)? 0 : 1; 
      if(diveta_br<=-1) diveta_br = 0;
      if(diveta_br>=30) diveta_br = 29;
      if(phiBin_br<=-1) phiBin_br = 0;
      if(phiBin_br>=30) phiBin_br = 29;
      if (L2_br_radius > 1e-5) {
        hMDT_invpTvsinvRadius[icharge][L2_saddress][diveta_br][phiBin_br]->Fill(L2_br_radius, offline_pt);
      }
    }
  }
  fout->Write();
}


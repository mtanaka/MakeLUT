void runNewValidation(){
  gROOT->LoadMacro("macro/util.cxx++");
  gROOT->LoadMacro("macro/newValidationT.cc++");
  newValidationT t;
  
  const char *input_data = "datalist/inputValidationData16.list";
  const char *output_data = "inputLUT/input_lut.root";
  t.Loop(input_data,output_data); 
  
}
